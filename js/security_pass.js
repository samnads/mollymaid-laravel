$(document).ready(function () {

    // $('#pass_maid_name').change(function () {
    //     var selectedOption = $(this).find(':selected');
    //     var empType = selectedOption.data('emp-type');
    //     $('#emp_type').val(empType);
    // });
    // ***************************************************************
    // $('#pass_customer_name').change(function () {
    //     var selectedOption = $(this).find(':selected');
    //     var customerCode = selectedOption.data('customer-code');
    //     $('#customer_code').val(customerCode);
    // });

    // ************************************************************
    // date allowed to select today and future
    $(function () {
        $("#service_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        }).datepicker("setStartDate", new Date());
    });
    $(function () {
        $("#service_start_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        }).datepicker("setStartDate", new Date());
    });
    $(function () {
        $("#service_end_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        }).datepicker("setStartDate", new Date());
    });
    $.validator.addMethod("requiredFile", function(value, element) {
        return $(element).get(0).files.length > 0;
    }, "Please upload a file.")

    security_pass_form_validator = $('#security_pass_form').validate({
        ignore: [],
        focusInvalid: false,
        rules: {
            pass_customer_name: {
                required: true,
            },
            pass_maid_name: {
                required: true,
            },
            pass_type: {
                required: true,
            },
            service_date: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: true,
            },
            security_pass_image: {
                requiredFile: function () {
                    return !$("#edit_security_pass_form").length;
                }
            },
        },
        messages: {
            pass_customer_name: {
                required: "Please select a customer.",
            },
            pass_maid_name: {
                required: "Please select an employee.",
            },
            service_date: {
                required: "Please select a service date.",
            },
            pass_type: {
                required: "Please select a Pass Type.",
            },
            service_start_date: {
                required: "Please select a Service Start Date.",
            },
            service_end_date: {
                required: "Please select a Service End Date.",
            },
            security_pass_image: {
                requiredFile: "Please upload a pass image.",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "pass_customer_name") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "pass_maid_name") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_date") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "pass_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_start_date") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_end_date") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "security_pass_image") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
            console.log(element.attr("name"));
        },
    });
// *****************************************************************************
    toggledates();
    $('#pass_type').change(function(){
        toggledates();
        updatevalidation_rules()
    });

    function toggledates() {
        var passType = $('#pass_type').val();
        
        $('#service_start_date, #service_end_date').closest('.n-field-main').hide();

        if (passType === 'Daily') {

            $('#service_date').closest('.n-field-main').show();
        } else if (passType === 'Yearly') {
            $('#service_date').closest('.n-field-main').hide();
            $('#service_start_date, #service_end_date').closest('.n-field-main').show();
        }
    }
// ****************************************************************************
    function updatevalidation_rules() {
        var passType = $('#pass_type').val();
        var rules = {};

        if (passType === 'Daily') {
            rules['service_date'] = 'required';
            rules['service_start_date'] = false;
            rules['service_end_date'] = false;
        } else if (passType === 'Yearly') {
            rules['service_date'] = false;
            rules['service_start_date'] = 'required';
            rules['service_end_date'] = 'required';
        }

        // Update validation rules
        security_pass_form_validator.settings.rules = rules;
        edit_security_pass_form.settings.rules = rules;

        // Revalidate the form
        security_pass_form_validator.form();
        edit_security_pass_form.form();
    }

    // edit validation
    edit_security_pass_form = $('#edit_security_pass_form').validate({
        ignore: [],
        focusInvalid: false,
        rules: {
            pass_customer_name: {
                required: true,
            },
            pass_maid_name: {
                required: true,
            },
            pass_type: {
                required: true,
            },
            service_date: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: true,
            },
        },
        messages: {
            pass_customer_name: {
                required: "Please select a customer.",
            },
            pass_maid_name: {
                required: "Please select an employee.",
            },
            service_date: {
                required: "Please select a service date.",
                date: "Please Select a valid date",
            },
            pass_type: {
                required: "Please select a Pass Type.",
            },
            service_start_date: {
                required: "Please select a Service Start Date.",
            },
            service_end_date: {
                required: "Please select a Service End Date.",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "pass_customer_name") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "pass_maid_name") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_date") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "pass_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_start_date") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_end_date") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
            console.log(element.attr("name"));
        },
    });
    // *************************************************************************
    $("#security_pass_form #pass-save-btn").click(function () {
        security_pass_form_validator.resetForm();
        if ($("#security_pass_form").valid()) {
            loader(true);
            $('#security_pass_form-error').empty();
            $('#pass-save-btn').html('Saving...').attr("disabled", true);
            var employee_name = $('#pass_maid_name option:selected').data('employee-name');
            var employee_type = $('#pass_maid_name option:selected').data('emp-type');
            var customer_name = $('#pass_customer_name option:selected').data('customer-name');
            var formData = new FormData(document.getElementById("security_pass_form"));
            console.log(formData);
            formData.append('employee_name', employee_name);
            formData.append('employee_type', employee_type);
            formData.append('customer_name', customer_name);
            console.log(formData.get('security_pass_image'));
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "security_pass/save_pass",
                // data: $('#security_pass_form').serialize(),
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == "success") {
                        $('#pass-save-btn').html('Saved');
                        loader(false);
                        toast('success', data.message);
                        setTimeout(function () {
                            window.location.href = _base_url + "security_pass/pass_list";
                        }, 3000);
                    }
                    else {
                        $('#security_pass_form-error').html(data.message);
                        $('#pass-save-btn').html('Save').removeAttr("disabled");
                        loader(false);
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    $('#security_pass_form-error').html("An error occured!");
                    $('#pass-save-btn').html('Save').removeAttr("disabled");
                    loader(false);
                    toast('error', data.message);
                },
            });
        }
        else {
            $('#security_pass_form label.error').each(function () {
                if ($(this).css('display') != 'none') {
                    toast('warning', $(this).text());
                    return false;
                }
            });
        }
    });
    // **************************************************************************
    // update security pass
    $("#edit_security_pass_form #pass-save-btn").click(function () {
        edit_security_pass_form.resetForm();
        if ($("#edit_security_pass_form").valid()) {
            loader(true);
            $('#security_pass_form-error').empty();
            $('#pass-save-btn').html('Saving...').attr("disabled", true);
            var employee_name = $('#pass_maid_name option:selected').data('employee-name');
            var employee_type = $('#pass_maid_name option:selected').data('emp-type');
            var customer_name = $('#pass_customer_name option:selected').data('customer-name');
            var formData = new FormData(document.getElementById("edit_security_pass_form"));
            formData.append('employee_name', employee_name);
            formData.append('employee_type', employee_type);
            formData.append('customer_name', customer_name);
            console.log(formData.get('security_pass_image'));
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "security_pass/update_pass",
                // data: $('#edit_security_pass_form').serialize(),
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == "success") {
                        $('#pass-save-btn').html('updated');
                        loader(false);
                        toast('success', data.message);
                        setTimeout(function () {
                            window.location.href = _base_url + "security_pass/pass_list";
                        }, 3000);
                    }
                    else {
                        $('#edit_security_pass_form-error').html(data.message);
                        $('#pass-save-btn').html('Update').removeAttr("disabled");
                        loader(false);
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    $('#edit_security_pass_form-error').html("An error occured!");
                    $('#pass-save-btn').html('Update').removeAttr("disabled");
                    loader(false);
                    toast('error', data.message);
                },
            });
        }
        else {
            $('#edit_security_pass_form label.error').each(function () {
                if ($(this).css('display') != 'none') {
                    toast('warning', $(this).text());
                    return false;
                }
            });
        }
    });
    // *******************************************************************************************
    // view gate pass 
    $('[data-action="view-pass"]').click(function (event) {
        $("#view_pass_modal :input").prop("disabled", true);
        $('.mm-loader').show();
        $.ajax({
            type: "POST",
            url: _base_url + "security_pass/get_pass_details",
            data: {
                pass_id: $(this).attr("data-id")
            },
            dataType: "json",
            cache: false,
            success: function (result) {
                console.log(result);
                $('#customercode').val(result.customer_code)
                $('#customername').val(result.customer_name)
                $('#employee_name').val(result.maid_name)
                $('#employee_type').val(result.employee_type)
                $('#pass_type').val(result.pass_type)
                $('#service_date').val(formatdate(result.service_date));
                $('#service_start_date').val(formatdate(result.service_start_date));
                $('#service_end_date').val(formatdate(result.service_end_date));
                if (result.pass_type === 'Daily') {
                    $('#service_date').closest('.n-field-main').show();
                    $('#service_start_date, #service_end_date').closest('.n-field-main').css('display', 'none');
                } else if (result.pass_type === 'Yearly') {
                    $('#service_start_date, #service_end_date').closest('.n-field-main').show();
                    $('#service_date').closest('.n-field-main').css('display', 'none');
                }
                if (result.pass_status == 1) {
                    $("#pass_status").val("Active");
                    $("#pass_status").css("color", "green");
                } else {
                    $("#pass_status").val("Inactive");
                    $("#pass_status").css("color", "red");
                }
                if (result.file_name) {
                    var imagePath = _base_url + './uploads/gate_pass/' + result.file_name;
                    $('#pass_image').attr('src', imagePath);
                    $('#pass_image_link').attr('href', imagePath);
                    $('#security_pass_image_error').text('');
                } else {
                    $('#pass_image').attr('src', '');
                    $('#pass_image_link').attr('href', '#');
                    $('#security_pass_image_error').text('No image');
                }
                pass_fancybox_show('view_pass_modal', {
                    closeClick: false
                });
            },
            error: function (xhr, textStatus, errorThrown) {
                console.error("Error: " + errorThrown);
                console.error("Status: " + textStatus);
                console.error(xhr);
                toast('error', 'Something went wrong. Please try again.');
            }
        });
    });


    // *************************************************************************************
    // image upload
    $(document).ready(function () {
        $('#security_pass_image').on('change', function (e) {
            var selectedFile = e.target.files[0];
            var filename = selectedFile.name;

            // Show the selected filename in the associated custom file label
            var customFileLabel = document.querySelector('label[for="' + e.target.id + '"]');
            if (customFileLabel) {
                customFileLabel.textContent = filename;
            }

            // Check if the file type is allowed
            var extension = filename.split('.').pop().toLowerCase();
            var allowedTypes = ['jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'pdf'];
            var errorDiv = document.getElementById('security_pass_image_error');

            if (allowedTypes.indexOf(extension) === -1) {
                if (errorDiv) {
                    errorDiv.textContent = 'File type not allowed. Allowed types: jpg, jpeg, gif, png, doc, docx, pdf';
                    errorDiv.classList.add('error');
                }
                e.target.value = '';
            } else {
                if (errorDiv) {
                    errorDiv.textContent = '';
                    errorDiv.classList.remove('error');
                }

                // Display the file name in the image_name span
                var imageNameSpan = document.getElementById('image_name');
                if (imageNameSpan) {
                    imageNameSpan.textContent = filename;
                }
            }
        });
    });

// **********************************************************************************


  
// *********************************************************************************
function pass_fancybox_show(id, settings = {}) {
    width = settings.width || 450;
    height = settings.height || 'auto';
    closeClick = settings.closeClick || false;
    padding = settings.padding || 0;
    $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: true,
        openEffect: 'none',
        openSpeed: 1,
        closeSpeed: 1,
        autoSize: false,
        width: width,
        height: height,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgb(57 92 106 / 60%)'
                },
                closeClick: closeClick
            }
        },
        padding: 0,
        closeBtn: false,
        content: $('#' + id),
    });
    $('.mm-loader').hide();
}
// ****************************************************************
function formatdate(datestring) {

    var date = new Date(datestring);

    if (isNaN(date.getTime())) {
        return ""; 
    }

    var day = date.getDate();
    var month = date.getMonth() + 1; 
    var year = date.getFullYear() % 100; 

    day = day < 10 ? "0" + day : day;
    month = month < 10 ? "0" + month : month;

    return day + "-" + month + "-" + year;
}






























});