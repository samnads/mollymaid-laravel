daily_collection_form_validator = $('#daily_collection').validate({
    ignore: [],
    focusInvalid: false,
    focusCleanup: true,
    rules: {
        filter_date: {
            required: true,
        },
        filter_leader: {
            required: true,
        }, filter_maid: {
            required: function (element) {
                var leaderValue = $('#filter_leader').val();
                return leaderValue !== '';
            }
        },
    },
    messages: {
        filter_date: {
            required: "Please select a date.",
        },
        filter_leader: {
            required: "Please select a Leader.",
        }, filter_maid: {
            required: "Please select an Employee.",
        },
    },
    errorPlacement: function (error, element) {
        if (element.attr("name") == "filter_date" ||
         element.attr("name") == "filter_leader" || element.attr("name") == "filter_maid") {
            element.closest('li').append(error);
        } else {
            error.insertAfter(element);
        }
    },
});

$('#filter_date, #filter_leader, #filter_maid').on('focus', function() {
    var fieldName = $(this).attr('name');
    // Remove the 'error' class and hide the error message for this field
    $(this).removeClass('error');
    $("label[for='" + fieldName + "']").hide();
}); 

$("#printButnfordailycollection").click(function () {
    var divContents = $("#divForPrintCollection").html();

    var date = moment($('#filter_date').val(), 'YYYY-MM-DD').format('DD/MM/YYYY');
    // var enddate = $("#end-date").val();
    //var day = $("#day").val();
  
    var printWindow = window.open("", "", "height=400,width=800");
    // printWindow.document.write("<html><head><title></title>");
    printWindow.document.write("</head><body >");
    printWindow.document.write('<span align="text-right"><b>MOLLY MAID</b></span>');
    printWindow.document.write("<br />");
    printWindow.document.write('<span align="text-right"><b>P O Box 32903</b></span>');
    printWindow.document.write("<br />");
    printWindow.document.write('<span align="text-right"><b>Dubai - UAE</b></span>');
    printWindow.document.write("<br />");
    printWindow.document.write('<span align="text-right"><b>Daily Collection - Employee  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</b></span>');
    printWindow.document.write('<span align="text-left"><b>Date &nbsp;&nbsp;&nbsp; : &nbsp;</b></span>');
    printWindow.document.write(date);
    printWindow.document.write('<span style="margin-left:125px;">');
    // printWindow.document.write("Date Range &nbsp;&nbsp;&nbsp; : &nbsp;");
    // printWindow.document.write(startdate);
    printWindow.document.write(
    //   "&nbsp;&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;&nbsp;&nbsp;" + enddate
    );
    printWindow.document.write("&nbsp;&nbsp;&nbsp;&nbsp;");
    printWindow.document.write("</span>");
    //printWindow.document.write('<span style="margin-left:300px;">');
    //printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    //printWindow.document.write(zone);
    //printWindow.document.write('</span>');
    printWindow.document.write("<br /><br />");
    printWindow.document.write(divContents);
    printWindow.document.write("</body></html>");
    printWindow.document.close();
    printWindow.print();
  });
// *****************************************************************
// excel download
function downloadExcel() {
    var fileType = "xlsx";
    var tableId = "employee-collection-table-excel";
    var table = document.getElementById(tableId);

    var selectedDate = $("#filter_date").val();
    var selectedMaidName = $("#filter_maid option:selected").text();
    selectedMaidName = selectedMaidName.replace(/[^\w\s]/gi, '').replace(/\s+/g, '_');
    var fileName = "employee_collection_report_" + selectedMaidName + "_" + selectedDate;
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report" });
    const ws = wb.Sheets['Report'];

    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 20 },
        { wch: 25 },
        { wch: 25 },
        { wch: 25 },
        { wch: 25 },
        { wch: 25 },
        { wch: 25 },
    ];

    ws['!cols'] = wscols;
    return XLSX.writeFile(wb, fileName + "." + (fileType || "xlsx"));
}


// ********************************************************************************
// get maids under team leader 
$(document).ready(function () {
    $('#filter_leader').on('change', function () {
        var leaderId = $(this).val();
        // alert(leaderId);
        $.ajax({
            type: 'POST',
            url: _base_url + 'employee/get_leader_maid',
            data: { leader_id: leaderId },
            dataType: 'json',
            success: function (response) {
                var maidDropdown = $('#filter_maid');
                maidDropdown.empty().append('<option value="">-- Select Employee --</option>');

                if (response && response.length > 0) {
                    $.each(response, function (index, maid) {
                        maidDropdown.append('<option value="' + maid.maid_id + '">' + maid.maid_name + ' (' + maid.maid_id + ')</option>');
                    });
                }
            },
            error: function (xhr, status, error) {
                console.error('Ajax request failed:', status, error);
            }
        });
    });
});

