
/************************************************************************** */
$('#edit_driver_mapping_form select[name="edit_zone_id"]').change(function () {
    var zone_id = this.value;
    $('#edit_driver_mapping_form select[name="edit_area_id"]').html(``);
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/get_areas_by_zone",
        data: { zone_id: zone_id },
        success: function (areas) {
            var options = `<option value="">-- Select area --</option>`;
            $.each(areas, function (key, area) {
                options += `<option value="` + area.area_id + `">` + area.area_name + `</option>`;
            });
            $('#edit_driver_mapping_form select[name="edit_area_id"]').html(options);
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
/************************************************************************** */
$().ready(function () {
    $('li [data-tab="tab2"]').trigger('click');
    edit_driver_mapping_form_validator = $('#edit_driver_mapping_form').validate({
        ignore: [],
        rules: {
            edit_driver_id: {
                required: true,
            },
            edit_zone_id: {
                required: true,
            },
            edit_area_id: {
                required: true,
            },
            edit_date_from: {
                required: true,
            },
            edit_date_to: {
                required: true,
            },
        },
        messages: {
            edit_driver_id: "Select driver.",
            edit_zone_id: "Select zone from list.",
            edit_area_id: "Select area from list.",
            edit_date_from: "Pick a start date.",
            edit_date_to: "Pick a end date.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "edit_driver_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "edit_zone_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "edit_area_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
           // $('#edit_driver_mapping_form button[type="submit"]').html('Saving...').attr("disabled", true);
            var edit_driver_avalability_id = $('#edit_driver_avalability_id').val();
            var edit_driver_id = $('#edit_driver_id').val();
            var edit_zone_id = $('#edit_zone_id').val();
            var edit_area_id = $('#edit_area_id').val();
            var edit_date_from = $('#edit_date_from').val();
            var edit_date_to = $('#edit_date_to').val();
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "driver/driver_area_exist",
                data: {
                    edit_driver_avalability_id: edit_driver_avalability_id,
                    edit_driver_id: edit_driver_id,
                    edit_area_id: edit_area_id,
                    edit_date_from: edit_date_from,
                    edit_date_to: edit_date_to,
                  },
                  success: function(response) {
                    if (response.status == 'success') {
                        toast('error', response.message);
                        //$('.error-message').text(response1.message);
                    } else {

                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: _base_url + "driver/edit_driver",
                            data: {
                                edit_driver_avalability_id: edit_driver_avalability_id,
                                edit_driver_id: edit_driver_id,
                                edit_area_id: edit_area_id,
                                edit_zone_id: edit_zone_id,
                                edit_date_from: edit_date_from,
                                edit_date_to: edit_date_to,
                              },
                            success: function (data) {
                                if (data.status == true) {
                                    toast('success', data.message);
                                    close_fancybox();
                                    clickCurrentTab();
                                }
                                else {
                                    toast('error', data.message);
                                    $('.mm-loader').hide();
                                }
                                $('#edit_driver_mapping_form button[type="submit"]').html('Save').attr("disabled", false);
                            },
                            error: function (data) {
                                $('#edit_driver_mapping_form button[type="submit"]').html('Save').attr("disabled", false);
                                toast('error', data.statusText);
                            },
                        });
                    }
                },
                error: function() {
                    alert('Error');
                }
            });
           
        }
    });
});
$('#edit_driver_mapping_form .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});