/************************************************************************** */
/*                                                                          */
/*                                    
/*                                                                          */
/************************************************************************** */
// logic goes here...................
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#tb-slide-right').click(function () {
    var position = $('.time_line .time_slider').position();
    var max_left = time_grid_width - 1768 + 3 + 5;
    var left_pos = position.left - 71;
    left_pos = left_pos < max_left ? max_left : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -left_pos + 71 + 'px',
    }, 71);



    if ($(window).width() <= 1366) {

       //alert("hai");
        
        var max_left = time_grid_width - 1610 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        

    }
    if ($(window).width() <= 1300) {
        var max_left = time_grid_width - 1610 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        

    }
});









$('#tb-slide-left').click(function () {
    var position = $('.time_line .time_slider').position();
    var left_pos = position.left + 71;
    left_pos = left_pos > 0 ? 0 : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    //$('.maid_name').css('padding-left', -position.left + 'px');
    position.left = position.left == 0 ? -71 : position.left;
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -position.left + 'px',
    }, 71);
});
$('.selector').selectable({
    cancel: ".booked",
    //distance: 1,
    start: function (event, ui) {
        $('.ui-selected').removeClass('ui-selected');
    },
    stop: function (event, ui) {
        var maid_id = $(this).attr('data-maid');
        var maid_name = $(this).attr('data-maid-name');
        var week_day = $(this).attr('data-week');
        var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
        var time_from = $('.ui-selected').first().attr('data-time-from');
        var time_to = $('.ui-selected').last().attr('data-time-to');
        $('#new_booking_form input[name="service_start_date"]').datepicker('update', new Date());
        $('#new_booking_form input[name="service_end_date"]').val("").datepicker('setStartDate', moment().add(7 * 3, 'days').format('DD/MM/YYYY'));
        $('#new_booking_form input[name="service_week_day"]').val(week_day);
        $('#new_booking_form input[name="service_week_name"]').val(week_name);
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
        $('#new_booking_form input[name="maid_id"]').val(maid_id);
        $('#new_booking_form input[name="maid_name"]').val(maid_name);
        $('#new_booking_form select[name="time_from"]').select2().val(time_from).trigger("change.select2");
        $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(time_from)).select2().val(time_to).trigger("change.select2");
        $('#new_booking_form select[name="customer_id"]').val("").trigger("change.select2");
        $('#new_booking_form select[name="service_type_id"]').select2().val(6).trigger("change.select2");
        $('#new_booking_form input[name="cleaning_materials"]').prop('checked', false);
        $('#new_booking_form input[name="booking_id"]').val(""); // important
        $('#new_booking_form input[name="service_rate_per_hour"]').val("");
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val("");
        $('#new_booking_form .vat_percentage').html(service_vat_percentage.toFixed(2));
        $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val(10);
        $('#new_booking_form select[name="customer_id"]');
        $('#new_booking_form .modal-footer :input');
        reCalculateForm();
        /*********************************** */
        $('#new-booking-form-popup .title').html('New Booking');
        $('#new_booking_form select[name="customer_id"]').next().show();
        $('#new_booking_form input[name="customer_name"]').val("").hide();
        $("#new-booking-form-popup .modal-footer.new").show();
        $("#new-booking-form-popup .modal-footer.edit").hide();
        new_booking_form_validator.resetForm();
        // enable fields
        $('#new_booking_form select[name="service_type_id"]').prop("disabled", false);
        $('#new_booking_form select[name="time_from"]').prop("disabled", false);
        $('#new_booking_form select[name="time_to"]').prop("disabled", false);
        $('#new_booking_form input[name="service_start_date"]').prop("disabled", false);
        $('#new_booking_form input[name="service_end_date"]').prop("disabled", false);
        $('#new_booking_form input[name="service_rate_per_hour"]').prop("disabled", false);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').prop("disabled", false);
        $('#new_booking_form input[name="cleaning_materials"]').prop("disabled", false);
        $('#new_booking_form rf').show();
        //
        fancybox_show('new-booking-form-popup', { width: 750 });
        $('#customer-picked-address').html('');
        $('#customer-address-panel-user').hide()
    }
});
if ($(".booking-position").length > 0) {
    var stickyTop = $('.booking-position').offset().top;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= stickyTop) {
            $('.scroll-top-fix').addClass('box-fixed-top');
            $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
        } else {
            $('.scroll-top-fix').removeClass('box-fixed-top');
            $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
        }
    });
}
function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
function RefreshSomeEventListener() {
    $(".booked_bubble").off();
    $(".booked_bubble").on("click", function () {
        //alert('b');
    });
}
function renderBookings() {
    loader(true);
    // reset cells
    $('.booked_bubble').remove();
    $('.cell').removeClass("booked")
    //
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "laravel/booking/regular_schedules_list",
        // data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() || undefined },
        data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() ? (moment($('#filter_end_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined },
        success: function (data) {
            var booking_delete_list = data.booking_delete_data;
            var total_hours = data.total_hours;
            var training_hours = data.training_hours;
            var free_hours = data.free_hours;
            var total_hour_week = data.total_hour_week;
            var grand_total = data.grand_total;
            var grand_suspend_total = data.grand_suspend_total;
            var maid_count = data.maid_count;
            var emergency_leave_count = data.emergency_leave_count;
            var vacations_count = data.vacations_count;
            var holidays_count = data.holidays_count;
            var medical_leaves_count = data.medical_leaves_count;
            var total_maid_leave_count = data.total_maid_leave_count;
            var present_maid_count = data.present_maid_count;
            var total_sales = data.total_sales;
            var lost_sales = data.lost_sales;
            // maid leaves count
            $('#emergency_leave_count').html(emergency_leave_count);
            $('#vacations').html(vacations_count);
            $('#holidays').html(holidays_count);
            $('#medical_leaves').html(medical_leaves_count);
            $('#absents_count').html(total_maid_leave_count);
            $('#presents_count').html(present_maid_count);

            $('#grand_percentage').html('');
            $('#grand_total').html('');
            $('#gained_hours').html('');
            $('#lost_hours').html('');
            $('#total_hours').html('');
            $('#lost_hours').html('');
            $('#total_hours').html('');
            $('#training_hours').html('');
            $('#free_hours').html('');
            let booking_html = '';
            //
            var count = 1;
            var validBookings = []; // Array to store valid bookings
            var seenBookingIds = new Set();
            $.each(data.bookings, function (key, booking) {
                // Find the booking in the delete list
                let delete_list = booking_delete_list.find(delete_list => {
                    return delete_list.booking_id == booking.booking_id;
                });
            
                // Check if booking is not in delete list and delete_status is null
                if (delete_list === undefined && booking.delete_status === null) {
                    if (!seenBookingIds.has(booking.booking_id)) {
                        validBookings.push(booking);
                        seenBookingIds.add(booking.booking_id);
                    }
                    // Add the booking to the validBookings array
                    // validBookings.push(booking);
                }
            });
            
            // Pass the array of valid bookings to initializeDataTable
            initializeDataTable(validBookings);
            $.each(data.bookings, function (key, booking) {
            let delete_list = booking_delete_list.find(delete_list => {
                return delete_list.booking_id == booking.booking_id
            });
                var cells = booking.working_minutes / 30;
            
                
                $.each(total_hours, function (key1, total) {
                    var working_minutes1 = total.working_minutes / 60;
                    var max_time = 56;
                    var percentage = (working_minutes1 / (max_time / 100));
                    percentage = percentage.toFixed(2)
                    percentage =  percentage+`%`;
                    $('[data-maid-week-id="' + total.maid_id +'"]').html(working_minutes1);
                    $('[data-maid-id-week-id="' + total.maid_id +'"]').html(percentage);

                });

                $.each(total_hour_week, function (key, tot) {
                    var working_minutes1 = tot.working_minutes / 60;
                    var max_time = 8;
                    var percentage = (working_minutes1 / (max_time / 100));
                    percentage = percentage.toFixed(2)
                    percentage =  percentage+`%`;
                    $('[data-maid-id="'+ tot.maid_id +'"][data-week-id="' + tot.service_week_day + '"]').html(working_minutes1);
                    $('[data-maid-id-percentage="' + tot.maid_id +'"][data-week-id-percentage="' + tot.service_week_day + '"]').html(percentage);

                });

                //grand total percentage
                var total_maid_hours = maid_count * 56;
                var grantTotal = grand_total / 60;
                var grand_percentage = (grantTotal / (total_maid_hours / 100))
                grand_percentage = grand_percentage.toFixed(2)
                grand_percentage =  grand_percentage+`%`;
          
                // Grand Suspend total hours
                var suspend_total = grand_suspend_total / 60;
                $('#lost_hours').html(suspend_total+`hrs`);

                // Gained total hours 
                $('#gained_hours').html(grantTotal+`hrs`);

                //Regular schedule total Hrs
                var schedule_total = grantTotal + suspend_total;
                $('#total_hours').html(schedule_total+`hrs`);

                $('#grand_percentage').html(grand_percentage);
                $('#grand_total').html(grantTotal);
                $('#total_sales').html(total_sales);
                $('#lost_sales').html(lost_sales);

                // training_hours 
                var Training_hours = training_hours / 60;
                $('#training_hours').html(Training_hours+`hrs`);
                // free_hours
                var Free_hours = free_hours / 60;
                $('#free_hours').html(Free_hours+`hrs`);
                
            });
            // $('#booking_customer').html(booking_html);
            RefreshSomeEventListener();
            styleThisWeekCells();
            loader(false);
        },
        error: function (data) {
            toast('warning', "An error occured !");
            loader(false);
        },
    });
}
function styleThisWeekCells() {
    var currentTime = moment();
    var dow = currentTime.day();
    var time_rounded = moment(currentTime).format("H:00:00");
    $('.cell').removeClass("current-week-day");
    $('.cell').removeClass("current-time");
    $('.cell').removeAttr('style');
    $('.cell').each(function (index, option) {
        /******* same week day highlight  */
        if ($(this).attr("data-week") == dow) {
            $(this).addClass("current-week-day");
        }
        /******* current time highlight  */
        if ($(this).attr("data-time-from") == time_rounded) {
            //$(this).addClass("current-time");
            $(this).css({ "border-right": "1px solid rgb(255 150 150)" });
        }
    });
}
// Function to initialize DataTable
function initializeDataTable(dataArray) {
    console.log(dataArray)
    var dataTable; // Define dataTable as a local variable
    if ($.fn.DataTable.isDataTable('#package-list-table')) {
        $('#package-list-table').DataTable().destroy();
    }

    var tableData = [];

    // Populate tableData with data from dataArray
    if (Array.isArray(dataArray)) {
        tableData = dataArray.map(function(data, index) {
          
            var book_status = data.delete_status == null ? "Active" : data.delete_booking_type;
            if (data.customer_booktype == "0") {
                var customer_booktype = "One-off";
            } else if (data.customer_booktype == "1") {
                var customer_booktype = "Regular";
            }
           
            return {
                sl_no: index + 1, // Add serial number
                customer_code: data.customer_code,
                customer_name: data.customer_name,
                customer_type: customer_booktype,
                booking_id: data.booking_id,
                week_name: data.week_name,
                booking_type: data.booking_type,
                maid_name: data.maid_name,
                time_from: moment(data.time_from, 'HH:mm:ss').format('hh:mm A'),
                time_to: moment(data.time_to, 'HH:mm:ss').format('hh:mm A'),
                working_minutes: data.working_minutes / 60,
                service_rate_per_hour: data._service_rate_per_hour,
                total_amount: data._total_amount,
                booking_zone: data.booking_zone,
                area_name: data.area_name,
                book_status: book_status
            };
        });
    } else {
        console.error("dataArray is not an array.");
        return; // Exit function if dataArray is not an array
    }

    // Initialize DataTable
    $('#package-list-table').DataTable({
        data: tableData,
        columns: [
            { data: 'sl_no', name: 'sl_no' }, // Slno column
            { data: 'customer_code', name: 'customer_code' },
            { data: 'customer_name', name: 'customer_name' },
            { data: 'customer_type', name: 'customer_type' },
            { data: 'booking_id', name: 'booking_id' },
            { data: 'week_name', name: 'week_name' },
            { data: 'booking_type', name: 'booking_type' },
            { data: 'maid_name', name: 'maid_name' },
            { data: 'time_from', name: 'time_from' },
            { data: 'time_to', name: 'time_to' },
            { data: 'working_minutes', name: 'working_minutes' },
            { data: 'service_rate_per_hour', name: 'service_rate_per_hour' },
            { data: 'total_amount', name: 'total_amount' },
            { data: 'booking_zone', name: 'booking_zone' },
            { data: 'area_name', name: 'area_name' },
            { data: 'book_status', name: 'book_status' },
            // Add more columns as needed
        ]
    });

}
$(document).ready(function () {
    // var dataTable;

    // if ($('#package-list-table').length > 0) {
    //   dataTable = $('table#package-list-table').DataTable({
    //     'sPaginationType': 'full_numbers',
    //     'bSort': true,
    //     'iDisplayLength': 100,
    //     'scrollY': true,
    //     'orderMulti': false,
    //     'scrollX': true,
    //     'columnDefs': [{
    //       'targets': [-1],
    //       'orderable': false
    //     }]
    //   });
    // }
    renderBookings();
});

/********************************************************************************** */
// date picker
$('.datepicker').datepicker({ autoclose: true, todayHighlight: true, todayBtn: "linked" }).on('changeDate', function (event) {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var date_full = moment(date).format('DD/MM/YYYY');
    $('.hed-date-main-box').html(date_full);
    $('#date').val(date);
});
$('.hed-date-left-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(-1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('.hed-date-right-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
/***************************************************************************************** */
$('#new_booking_form select[name="time_from"]').on('change', function () {
    $('#new_booking_form input[name="working_hours"]').val(0);
    $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change.select2");
});
$('#new_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
/***************************************************************************************** */
function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#new_booking_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
/***************************************************************************************** */
$('#new_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_start_date"]').valid();
    var booking_date = $(this).val();
    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    $('#new_booking_form input[name="service_week_day"]').val(dow);
    $('#new_booking_form input[name="service_week_name"]').val(days[dow]);

    // alert(week_day)
    $('#new_booking_form input[name="service_end_date"]').datepicker('update', null).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").add(7 * 3, 'days').format('DD/MM/YYYY'));
});
$('#new_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_end_date"]').valid();
});
$('#new_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
}).on("change", function (e) {
    var cust_id = $(this).val();
    open_address_panel(cust_id)
    $(this).valid();
});
$('#new_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="time_to"]').select2().on("change", function (e) {
    $(this).valid();
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_week_day"]').on('change', function () {
    var week_day_id = this.value;
    var maid_id = $('#schedule-menu select[name="filter_maid_id"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
        $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"][data-week-maid-id-summary="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
            $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-maid-id-summary="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_maid_id"]').on('change', function () {
    var maid_id = this.value;
    var week_day_id = $('#schedule-menu select[name="filter_week_day"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"][data-week-maid-id-summary="' + maid_id + '"]').show();

        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"]').show();

        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
            $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-maid-id-summary="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
function maidHeads(show) {
    if (show == true) {
        $('#weeks .maid_name_week').show();
        $('.tb-slider .maid_name').show();
        $('#booking-summary-box .maids-weekly-summary').show();
    }
    else {
        $('#weeks .maid_name_week').hide();
        $('.tb-slider .maid_name').hide();
        $('#booking-summary-box .maids-weekly-summary').hide();

    }
}
function maidWeeks(show) {
    if (show == true) {
        $('#weeks .week').show();
        $('#booking-summary-box .week-day-summary').show();
    }
    else {
        $('#weeks .week').hide();
        $('#booking-summary-box .week-day-summary').hide();

    }
}
function slots(show) {
    if (show == true) {
        $('.tb-slider .slots').show();
    }
    else {
        $('.tb-slider .slots').hide();
    }
}
/***************************************************************************************** */
$().ready(function () {
    new_booking_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: false,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            },
            cleaning_material_rate_per_hour: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
            cleaning_material_rate_per_hour: "Enter materials per hour rate."
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "cleaning_material_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#new_booking_form").serializeArray();
            var service_start_date_i = values.map(function (o) { return o.name; }).indexOf("service_start_date");
            var service_end_date_i = values.map(function (o) { return o.name; }).indexOf("service_end_date");
            if (values[service_start_date_i]['value']) {
                values[service_start_date_i]['value'] = moment(values[service_start_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            if (values[service_end_date_i]['value']) {
                values[service_end_date_i]['value'] = moment(values[service_end_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/" + ($('#new_booking_form input[name="booking_id"]').val() == "" ? "save_booking" : "update_booking"),
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#customer-picked-address").html('');
                        // setTimeout(function () {
                        //     window.location.href = _base_url + "schedule/week_view";
                        // }, 1000);
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
/***************************************************************************************** */
var cleaning_material_rate_per_hour = 0;
function reCalculateForm() {
    cleaning_material_rate_per_hour = $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val();
    var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#new_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#new_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#new_booking_form input[name="material_fee"]').val(material_fee);
    $('#new_booking_form input[name="service_amount"]').val(service_amount);
    $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#new_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#new_booking_form input[name="service_discount"]').val(service_discount);
    //$('#new_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#new_booking_form input[name='cleaning_materials'],#new_booking_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $(this).valid();
});
$("#new_booking_form input[name='service_discount_rate_per_hour'],#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
$(function () {
    $('[data-toggle="popover"]').popover()
})
/***************************************************************************************** */
// page filter options
$('#filter_start_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    // startDate: "today"
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_start_date"]').valid();
    var end_date = moment(this.value, "DD.MM.YYYY");
    var end_date = end_date.add(5, 'days').format('DD/MM/YYYY');
    $('#filter_end_date').datepicker('update', end_date).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").format('DD/MM/YYYY'));
    renderBookings();
});
$('#filter_end_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_end_date"]').valid();
});

$('#schedulte_delete').click(function () {
    // var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    // var service_date = $('#new_booking_form input[name="service_date"]').val();
    // var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    fancybox_show('alert-popup', { width: 450 });
    //    $('#day_service_id').val(day_service_id);
    $('#booking_id_perment').val(booking_id);
    $('#booking_id_day').val(booking_id);
    //    $('#service_date').val(service_date);
    //    $('#day_service_status').val(service_status);
   
});

$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    // $('.n-delete-set-left').addClass('de-select');

    //$('.n-delete-set-right-cont').show(500);
     $('.n-delete-set-left-cont').show(500);
});

// $(".n-delete-set-left").click(function () {
//     $('.n-delete-set-left').removeClass('de-select');
//     $('.n-delete-set-right').addClass('de-select');

//     $('.n-delete-set-left-cont').show(500);
//     $('.n-delete-set-right-cont').hide(500);
// });

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    var booking_id = $('#booking_id_perment').val();
    if(remarks == "")
    {
        $("#delete_remark_perm").focus();
         $('#deleteremarks_book').css('display','block');
    } else {
        $('#deleteremarks_book').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#booking_id_day').val();
    if(remarks == "")
    {
        $("#delete_remark_day").focus();
         $('#deleteremarks_book_day').css('display','block');
    } else {
        $('#deleteremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

function open_address_panel(customer_id)
{
    $('#customer-address-id-user').val('');
    $('#customer-address-panel-user').hide();
    $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');
	
    if($.isNumeric(customer_id) && customer_id > 0)
    {	
        $('#customer-address-panel-user').slideDown();
        $.ajax({
            url: _base_url + 'schedule/get_customer_address_details',
            type: 'POST',
            data: {customer_id: customer_id},
            dataType: 'json',
            success: function(response) {
                // console.log(response)
                // $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response)
                // {
                if(response == 'refresh')
                {
                    $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                    });
                }
                else
                {
                    var adressdata = response.customer_address;	
                    // console.log(adressdata)			
                    var address_html = '<div class="table">';                                
                                    
                    $.each(adressdata, function(key, val) {
                        address_html += '<div class="row"><div class="cell1"><input type="hidden" id="address_location" value="'+ val.location_name +'"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                        
                    });				

                    address_html += '</div>';

                    $('#customer-address-panel-user .inner').html(address_html);
                }
            }
        });
    }
}

//End
$('body').on('click', '#customer-address-panel-user .close', function() {
	$('#customer-address-panel-user').slideUp( function() { $('#customer-picked-address').show(); } );
});


$('body').on('click', '.pick_customer_address', function() {
	var address_id = $(this).attr('id').replace('cadddress-', '');
	$('#customer-address-id-user').val(address_id);
	var addzone = $('#caddzone-' + address_id).html();
	var address = $('#cadd-' + address_id).html();
    // $('#customer-address').empty();
    // $('#customer-location').empty();
    // $('#customer-area-zone').empty();
    // $('#customer-sublocation').empty();
    $('#new_booking_form input[name="customer_address_id"]').val('');
    
	$('#customer-address-panel-user').slideUp(function() {
        // $('#customer-address').append(address);
        // $('#customer-area-zone').append(addzone);
        // $('#customer-location').append(response.customer_address[0]['location_name']);
        // $('#customer-sublocation').append(response.customer_address[0]['landmark_name']);
        $('#new_booking_form input[name="customer_address_id"]').val(address_id);
        $('#customer-picked-address').html('<div class="address"><strong>' + addzone + '</strong> - ' + address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
        $('#customer-picked-address').show();
        $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');

        var booking_id = $('#booking-id-user').val();
	});
});

$('body').on('click', '#chg-cust-address', function() {
	var _customer_id = $('#b-customer-id').val();
	$('#customer-picked-address').hide();
	open_address_panel(_customer_id);
});