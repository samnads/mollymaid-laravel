var new_validator, driver_assign_validator = null;
$('[data-action="new-vehicle"]').click(function (event) {
    new_validator.resetForm();
    $('#vehicle_id').val("");
    $('#vehicle-save-btn').html("Save");
    $('#new-vehicle-popup .modal-footer').show();
    $("#new-vehicle-popup :input").prop("disabled", false);
    $('#new-or-edit').html("New Vehicle");
    $('#new-vehicle-form')[0].reset();
    fancybox_show('new-vehicle-popup', { width: 850 });
});
$('[data-action="view-vehicle"]').click(function (event) {
    new_validator.resetForm();
    $('#vehicle_id').val("");
    $('#new-vehicle-popup .modal-footer').hide();
    $("#new-vehicle-popup :input").prop("disabled", true);
    $('#new-or-edit').html("View Vehicle");
    $('.mm-loader').show();
    $.ajax({
        type: "GET",
        url: _base_url + "vehicle/get_data",
        data: {
            id: $(this).attr("data-id")
        },
        dataType: "json",
        cache: false,
        success: function (data) {
            $('input[name=licence_number]').val(data.licence_number);
            $('input[name=brand_name]').val(data.brand_name);
            $('input[name=model_name]').val(data.model_name);
            $('input[name=color]').val(data.color);
            $('#model_year').val(data.model_year).trigger('change');
            $('input[name=engine_number]').val(data.engine_number);
            $('input[name=chassis_number]').val(data.chassis_number);
            $('input[name=fitness_start_date]').val(moment(data.fitness_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=fitness_expiry_date]').val(moment(data.fitness_expiry_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=horsepower]').val(data.horsepower);
            $('#fuel_type').val(data.fuel_type).trigger('change');
            $('input[name=insurance_number]').val(data.insurance_number);
            $('input[name=insurance_start_date]').val(moment(data.insurance_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=insurance_expiry_date]').val(moment(data.insurance_expiry_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=insurance_type]').val(data.insurance_type);
            $('input[name=insurance_file_name]').val(data.insurance_file_name);
            $('[name=insurance_note]').val(data.insurance_note);
            fancybox_show('new-vehicle-popup', { width: 850 });
        },
        error: function (data) {
            $('.mm-loader').hide();
        }
    });
});
$('[data-action="edit-vehicle"]').click(function (event) {
    new_validator.resetForm();
    $('#vehicle_id').val($(this).attr("data-id"));
    $('#vehicle-save-btn').html("Update");
    $('#new-vehicle-popup .modal-footer').show();
    $("#new-vehicle-popup :input").prop("disabled", false);
    $('#new-or-edit').html("Edit Vehicle");
    $('.mm-loader').show();
    $.ajax({
        type: "GET",
        url: _base_url + "vehicle/get_data",
        data: {
            id: $(this).attr("data-id")
        },
        dataType: "json",
        cache: false,
        success: function (data) {
            $('input[name=licence_number]').val(data.licence_number);
            $('input[name=brand_name]').val(data.brand_name);
            $('input[name=model_name]').val(data.model_name);
            $('input[name=color]').val(data.color);
            $('#model_year').val(data.model_year).trigger('change');
            $('input[name=engine_number]').val(data.engine_number);
            $('input[name=chassis_number]').val(data.chassis_number);
            $('input[name=fitness_start_date]').val(moment(data.fitness_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=fitness_expiry_date]').val(moment(data.fitness_expiry_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=horsepower]').val(data.horsepower);
            $('#fuel_type').val(data.fuel_type).trigger('change');
            $('input[name=insurance_number]').val(data.insurance_number);
            $('input[name=insurance_start_date]').val(moment(data.insurance_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=insurance_expiry_date]').val(moment(data.insurance_expiry_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
            $('input[name=insurance_type]').val(data.insurance_type);
            $('input[name=insurance_file_name]').val(data.insurance_file_name);
            $('[name=insurance_note]').val(data.insurance_note);
            fancybox_show('new-vehicle-popup', { width: 850 });
        },
        error: function (data) {
            $('.mm-loader').hide();
        }
    });
});
$('[data-action="assign-driver"]').click(function (event) {
    $('.mm-loader').show();
    $('#assign_vehicle_id').val($(this).attr("data-id"));
    $('#employee_id').html("");
    $('#driver_assign_note').val("");
    driver_assign_validator.resetForm();
    $.ajax({
        type: "GET",
        url: _base_url + "vehicle/get_data",
        data: {
            id: $(this).attr("data-id")
        },
        dataType: "json",
        cache: false,
        success: function (data) {
            $('.licence_number').html(data.licence_number);
            fancybox_show('driver-assign-popup');
        },
        error: function (data) {
            $('.mm-loader').hide();
        }
    });
});
$().ready(function () {
    new_validator = $('#new-vehicle-form').validate({
        ignore: [],
        rules: {
            brand_name: {
                required: true,
            },
            model_name: {
                required: true,
            },
            model_year: {
                required: true,
            },
            licence_number: {
                required: true,
            },
            engine_number: {
                required: true,
            },
            chassis_number: {
                required: true,
            },
            fitness_start_date: {
                required: true,
            },
            fitness_expiry_date: {
                required: true,
            },
            color: {
                required: true,
            },
            horsepower: {
                required: true,
            },
            fuel_type: {
                required: true,
            },
            insurance_number: {
                required: true,
            },
            insurance_start_date: {
                required: true,
            },
            insurance_expiry_date: {
                required: true,
            },
            insurance_type: {
                required: true,
            },
            insurance_note: {
                required: true,
            },
            insurance_file_name: {
                required: true,
            },
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "model_year") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "fuel_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "horsepower") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#new-vehicle-form-error').empty();
            $('#vehicle-save-btn').html('Saving...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "vehicle/save",
                data: $('#new-vehicle-form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        $('#vehicle-save-btn').html('Saved');
                        window.location.href = _base_url + "vehicles";
                    }
                    else {
                        $('#new-vehicle-form-error').html(data.message);
                        $('#vehicle-save-btn').html('Save').removeAttr("disabled");
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    $('#new-vehicle-form-error').html("An error occured!");
                    $('#vehicle-save-btn').html('Save').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
    driver_assign_validator = $('#driver-assign-form').validate({
        ignore: [],
        rules: {
            assign_vehicle_id: {
                required: true,
            },
            employee_id: {
                required: true,
            },
            driver_assign_note: {
                required: true,
            },
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "employee_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#driver-assign-error').empty();
            $('#driver-assign-btn').html('Assigning...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "vehicle/assign_driver",
                data: $('#driver-assign-form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        $('#driver-assign-btn').html('Assigned');
                        window.location.href = _base_url + "vehicles";
                    }
                    else {
                        $('#driver-assign-error').html(data.message);
                        $('#driver-assign-btn').html('Assign').removeAttr("disabled");
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    $('#driver-assign-error').html("Error : " + data.statusText);
                    $('#driver-assign-btn').html('Assign').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
    $("table#vehicle-list-table").dataTable({
        'sPaginationType': "full_numbers",
        "bSort": true,
        "iDisplayLength": 100,
        "scrollY": true,
        "orderMulti": false,
        "scrollX": true,
        'columnDefs': [{
            'targets': [-1],
            'orderable': false
        },]
    });
});
$("#employee_id").select2({
    ajax: {
        url: _base_url + "vehicle/get_drivers_for_assign",
        type: "GET",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                search: params.term,
            };
        },
        processResults: function (response) {
            return { results: response };
        },
        cache: true
    },
});
/************************************************************************** */
$('#new-vehicle-form input[name="fitness_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$('#new-vehicle-form input[name="fitness_expiry_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$('#new-vehicle-form input[name="insurance_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$('#new-vehicle-form input[name="insurance_expiry_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
/************************************************************************** */