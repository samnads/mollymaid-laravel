var _places = {};
var list_loader = `<li class="list-group-item active">Loading...</li>`;
function list_empty(message){
    return `<li class="list-group-item bg-warning text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;` + message +`</li>`;
}
function renderZones(zone_id_active = null) {
    var zones = _places.zones;
    zone_id_active = zone_id_active || zones[0].zone_id;
    var zone_html = ``;
    zones.forEach(function (zone, key) {
        edit_button = (zone.zone_id == zone_id_active) ? `<a href="#" onclick="return false;" data-action="edit-zone-popup" data-id="` + zone.zone_id + `"><span class="pull-right"><span><i class="fa fa-pencil" aria-hidden="true"></i></span></span></a>` : '';
        zone_html += `<li class="list-group-item ` + (zone.zone_id == zone_id_active ? 'active' : '') + `" data-id="` + zone.zone_id + `">` + zone.zone_name + edit_button + `</li>`;
    });
    $('#zones-holder').html(zone_html || list_empty('No Zones Available !'));
    renderAreas(null);
    /***************************************************** */
    $('#zones-holder .list-group-item').off();
    $('#zones-holder .list-group-item').click(function (event) {
        var zone_id = $(this).attr('data-id');
        renderZones(zone_id);
    });
    /***************************************************** */
    $('#zones-holder [data-action="edit-zone-popup"]').off();
    $('#zones-holder [data-action="edit-zone-popup"]').click(function (event) {
        var zone_id = $(this).attr('data-id');
        loader(true);
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: _base_url + "master/get_zone",
            data: { zone_id: zone_id },
            success: function (response) {
                $('#new_zone_form')[0].reset();
                let zone = response.zone;
                $('#new-zone-popup .title').html("Edit Zone");
                $('#new_zone_form [type="submit"]').html("Update");
                $('#new_zone_form input[name="zone_id"]').val(zone.zone_id);
                $('#new_zone_form input[name="name"]').val(zone.zone_name);
                $('#new_zone_form textarea[name="description"]').val(zone.description);
                loader(false);
                showZoneForm();
            },
            error: function (data) {
                loader(false);
                //toast('error', data.statusText);
            },
        });
    });
    /***************************************************** */
}
function renderAreas(area_id_active = null) {
    zone_id_active = $('#zones-holder li.active').attr('data-id');
    var areas = _places.areas;
    var area_html = ``;
    areas.forEach(function (area, key) {
        if (area.zone_id == zone_id_active) {
            area_id_active = area_id_active || area.area_id;
            edit_button = (area.area_id == area_id_active) ? `<a href="#" onclick="return false;" data-action="edit-area-popup" data-id="` + area.area_id + `"><span class="pull-right"><span><i class="fa fa-pencil" aria-hidden="true"></i></span></span></a>` : '';
            area_html += `<li class="list-group-item ` + (area.area_id == area_id_active ? 'active' : '') + `" data-id="` + area.area_id + `"  data-zone="` + area.zone_id + `">` + area.area_name + edit_button + `</li>`;
        }
    });
    $('#areas-holder').html(area_html || list_empty('No Areas Available !'));
    renderLocations(null);
    /***************************************************** */
    $('#areas-holder .list-group-item').off();
    $('#areas-holder .list-group-item').click(function (event) {
        var area_id = $(this).attr('data-id');
        renderAreas(area_id);
    });
    /***************************************************** */
    $('#areas-holder [data-action="edit-area-popup"]').off();
    $('#areas-holder [data-action="edit-area-popup"]').click(function (event) {
        var area_id = $(this).attr('data-id');
        loader(true);
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: _base_url + "master/get_area",
            data: { area_id: area_id },
            success: function (response) {
                $('#new_area_form')[0].reset();
                let area = response.area;
                $('#new-area-popup .title').html("Edit Area");
                $('#new_area_form [type="submit"]').html("Update");
                $('#new_area_form input[name="area_id"]').val(area.area_id);
                $('#new_area_form input[name="name"]').val(area.area_name);
                // $('#new_area_form input[name="min_booking_hour"]').val(area.min_booking_hour);
                $('#new_area_form textarea[name="description"]').val(area.description);
                loader(false);
                zone_id_active = $('#zones-holder li.active').attr('data-id');
                var zone_html = ``;
                _places.zones.forEach(function (zone, key) {
                    zone_html += `<option value="` + zone.zone_id + `">` + zone.zone_name + `</option>`;
                });
                $('#new_area_form select[name="zone_id"]').html(zone_html);
                // $('#new_area_form select[name="zone_id"]').select2().val(zone_id_active).trigger("change").prop('disabled', true);
                $('#new_area_form select[name="zone_id"]').select2().val(zone_id_active).trigger("change");
                showAreaForm();
            },
            error: function (data) {
                loader(false);
                //toast('error', data.statusText);
            },
        });
    });
    /***************************************************** */
}
function renderLocations(location_id_active = null) {
    zone_id_active = $('#zones-holder li.active').attr('data-id');
    area_id_active = $('#areas-holder li.active').attr('data-id');
    var locations = _places.locations;
    var location_html = ``;
    locations.forEach(function (location, key) {
        if (location.area_id == area_id_active) {
            location_id_active = location_id_active || location.location_id;
            edit_button = (location.location_id == location_id_active) ? `<a href="#" onclick="return false;" data-action="edit-location-popup" data-id="` + location.location_id + `"><span class="pull-right"><span><i class="fa fa-pencil" aria-hidden="true"></i></span></span></a>` : '';
            location_html += `<li class="list-group-item ` + (location.location_id == location_id_active ? 'active' : '') + `" data-id="` + location.location_id + `" data-area="` + location.area_id + `" data-zone="` + location.zone_id + `">` + location.location_name + edit_button + `</li>`;
        }
    });
    $('#locations-holder').html(location_html || list_empty('No Locations Available !'));
    renderLandmarks();
    /***************************************************** */
    $('#locations-holder .list-group-item').off();
    $('#locations-holder .list-group-item').click(function (event) {
        var location_id = $(this).attr('data-id');
        $("#locations-holder .list-group-item").removeClass("active")
        $(this).addClass("active");
        renderLocations(location_id);
        renderLandmarks();
    });
    /***************************************************** */
    $('#locations-holder [data-action="edit-location-popup"]').off();
    $('#locations-holder [data-action="edit-location-popup"]').click(function (event) {
        var location_id = $(this).attr('data-id');
        loader(true);
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: _base_url + "master/get_location",
            data: { location_id: location_id },
            success: function (response) {               
                $('#new_location_form')[0].reset();
                let location = response.location;
                $('#new-location-popup .title').html("Edit Location");
                $('#new_location_form [type="submit"]').html("Update");
                $('#new_location_form input[name="location_id"]').val(location.location_id);
                $('#new_location_form input[name="name"]').val(location.location_name);
                $('#new_location_form input[name="min_booking_hour"]').val(location.min_booking_hour);
                $('#new_location_form textarea[name="description"]').val(location.description);
                $('#new_location_form input[name="latitude"]').val(location.latitude);
                $('#new_location_form input[name="longitude"]').val(location.longitude);
                $('#new_location_form select[name="zone_id"]').html(`<option value="` + location.zone_id + `" selected>` + location.zone_name + `</option>>`);
                $('#new_location_form select[name="area_id"]').html(`<option value="` + location.area_id + `" selected>` + location.area_name + `</option>>`);
                $('#new_location_form select[name="zone_id"]').prop('disabled', true);
                $('#new_location_form select[name="area_id"]').prop('disabled', true);
                loader(false);
                showLocationForm();
            },
            error: function (data) {
                loader(false);
                //toast('error', data.statusText);
            },
        });
    });
    /***************************************************** */
}
function renderLandmarks() {
    zone_id_active = $('#zones-holder li.active').attr('data-id');
    area_id_active = $('#areas-holder li.active').attr('data-id');
    location_id_active = $('#locations-holder li.active').attr('data-id');
    var landmarks = _places.landmarks;
    var landmark_html = ``;
    landmarks.forEach(function (landmark, key) {
        if (landmark.location_id == location_id_active) {
            landmark_html += `<li class="list-group-item" data-id="` + landmark.landmark_id + `" data-location="` + landmark.location_id + `" data-area="` + landmark.area_id + `" data-zone="` + landmark.zone_id + `">` + landmark.landmark_name + `<a class="edit-btn" style="display:none" href="#" onclick="return false;" data-action="edit-landmark-popup" data-id="` + landmark.landmark_id + `"><span class="pull-right"><span><i class="fa fa-pencil" aria-hidden="true"></i></span></span></a></li>`;
        }
    });
    $('#landmarks-holder').html(landmark_html || list_empty('No Landmarks Available !'));
    /***************************************************** */
    $('#landmarks-holder .list-group-item').off();
    $('#landmarks-holder .list-group-item').click(function (event) {
        var landmark_id = $(this).attr('data-id');
        $("#landmarks-holder .list-group-item").removeClass("active")
        $(this).addClass("active");
        $('#landmarks-holder .list-group-item .edit-btn').hide();
        $('#landmarks-holder .list-group-item .edit-btn[data-id="' + landmark_id +'"]').show();
    });
    /***************************************************** */
    $('#landmarks-holder [data-action="edit-landmark-popup"]').off();
    $('#landmarks-holder [data-action="edit-landmark-popup"]').click(function (event) {
        var landmark_id = $(this).attr('data-id');
        loader(true);
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: _base_url + "master/get_landmark",
            data: { landmark_id: landmark_id },
            success: function (response) {
                $('#new_landmark_form')[0].reset();
                let landmark = response.landmark;
                $('#new-landmark-popup .title').html("Edit Landmark");
                $('#new_landmark_form [type="submit"]').html("Update");
                $('#new_landmark_form input[name="landmark_id"]').val(landmark.landmark_id);
                $('#new_landmark_form input[name="name"]').val(landmark.landmark_name);
                $('#new_landmark_form textarea[name="description"]').val(landmark.description);
                $('#new_landmark_form select[name="zone_id"]').html(`<option value="` + landmark.zone_id + `" selected>` + landmark.zone_name + `</option>>`);
                $('#new_landmark_form select[name="area_id"]').html(`<option value="` + landmark.area_id + `" selected>` + landmark.area_name + `</option>>`);
                $('#new_landmark_form select[name="location_id"]').html(`<option value="` + landmark.location_id + `" selected>` + landmark.location_name + `</option>>`);
                $('#new_landmark_form select[name="zone_id"]').prop('disabled', true);
                $('#new_landmark_form select[name="area_id"]').prop('disabled', true);
                $('#new_landmark_form select[name="location_id"]').prop('disabled', true);
                loader(false);
                showLandmarkForm();
            },
            error: function (data) {
                loader(false);
                //toast('error', data.statusText);
            },
        });
    });
    /***************************************************** */
}
var new_zone_form_validator = ";"
$(function () {
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "master/get_places",
        data: {},
        success: function (response) {
            if (response.status == true) {
                _places = response;
                renderZones();
            }
            else {
                toast('error', response.message);
            }
        },
        error: function (response) {
            toast('error', response.statusText);
        },
    });
    new_zone_form_validator = $('#new_zone_form').validate({
        rules: {
            name: {
                required: true
            },
        },
        messages: {
            name: {
                required: "Enter zone name."
            },
        },
        submitHandler: function (form) {
            loader(true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "master/" + ($('#new_zone_form input[name="zone_id"]').val() ? 'update_zone' : 'save_zone'),
                data: $('#new_zone_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        _places.zones = data.zones;
                        renderZones(data.id);
                        close_fancybox();
                    }
                    else {
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
    new_area_form_validator = $('#new_area_form').validate({
        rules: {
            zone_id: {
                required: true
            },
            name: {
                required: true
            },
        },
        messages: {
            zone_id: {
                required: "Select zone from list."
            },
            name: {
                required: "Enter area name."
            },
        },
        submitHandler: function (form) {
            if ($('#new_area_form input[name="area_id"]').val() != ''){
                var edit_areaid =  $('#new_area_form input[name="area_id"]').val();
                var edit_zone_id =  $('#new_area_form select[name="zone_id"]').select2().val();
               // alert(edit_zone_id);
                $.ajax({
                    url: _base_url + "/settings/get_zone_area",
                    type: "POST",
                    dataType: "json",
                    data: {
                        edit_areaid: edit_areaid,
                        edit_zone_id: edit_zone_id,
                    },
                    success: function(response1) {
                        if (response1.status == 'success') {
                            toast('error', response1.message);
                            //$('.error-message').text(response1.message);
                        } else {
                 
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                url: _base_url + "master/" + ($('#new_area_form input[name="area_id"]').val() ? 'update_area' : 'save_area'),
                                data: $('#new_area_form').serialize(),
                                success: function (data) {
                                    if (data.status == true) {
                                        toast('success', data.message);
                                        _places.areas = data.areas;
                                        renderAreas(data.id);
                                        close_fancybox();
                                    }
                                    else {
                                        toast('error', data.message);
                                    }
                                },
                                error: function (data) {
                                    toast('error', data.statusText);
                                },
                            });
                        }
                       
                    },
                    error: function() {
                        alert('Error');
                    }
                });
            } else {
                loader(true);
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: _base_url + "master/" + ($('#new_area_form input[name="area_id"]').val() ? 'update_area' : 'save_area'),
                    data: $('#new_area_form').serialize(),
                    success: function (data) {
                        if (data.status == true) {
                            toast('success', data.message);
                            _places.areas = data.areas;
                            renderAreas(data.id);
                            close_fancybox();
                        }
                        else {
                            toast('error', data.message);
                        }
                    },
                    error: function (data) {
                        toast('error', data.statusText);
                    },
                });
            }
        }
    });
    new_location_form_validator = $('#new_location_form').validate({
        rules: {
            zone_id: {
                required: true
            },
            area_id: {
                required: true
            },
            name: {
                required: true
            },
        },
        messages: {
            zone_id: {
                required: "Select zone from list."
            },
            area_id: {
                required: "Select area from list."
            },
            name: {
                required: "Enter location name."
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "area_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "master/" + ($('#new_location_form input[name="location_id"]').val() ? 'update_location' : 'save_location'),
                data: $('#new_location_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        _places.locations = data.locations;
                        renderLocations(data.id);
                        close_fancybox();
                    }
                    else {
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
    new_landmark_form_validator = $('#new_landmark_form').validate({
        rules: {
            zone_id: {
                required: true
            },
            area_id: {
                required: true
            },
            location_id: {
                required: true
            },
            name: {
                required: true
            },
        },
        messages: {
            zone_id: {
                required: "Select zone from list."
            },
            area_id: {
                required: "Select area from list."
            },
            location_id: {
                required: "Select location from list."
            },
            name: {
                required: "Enter landmark name."
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "zone_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "area_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "location_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "master/" + ($('#new_landmark_form input[name="landmark_id"]').val() ? 'update_landmark' : 'save_landmark'),
                data: $('#new_landmark_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        _places.landmarks = data.landmarks;
                        renderLandmarks();
                        close_fancybox();
                    }
                    else {
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
    $('[data-action="new-zone-popup"]').click(function (event) {
        $('#new_zone_form')[0].reset();
        $('#new_zone_form input[name="zone_id"]').val("");
        $('#new-zone-popup .title').html("New Zone");
        $('#new_zone_form [type="submit"]').html("Save");
        showZoneForm();
    });
    $('[data-action="new-area-popup"]').click(function (event) {
        $('#new_area_form')[0].reset();
        $('#new_area_form input[name="area_id"]').val("");
        $('#new-area-popup .title').html("New Area");
        $('#new_area_form [type="submit"]').html("Save");
        zone_id_active = $('#zones-holder li.active').attr('data-id');
        var zone_html = ``;
        _places.zones.forEach(function (zone, key) {
            zone_html += `<option value="` + zone.zone_id + `">` + zone.zone_name + `</option>`;
        });
        $('#new_area_form select[name="zone_id"]').html(zone_html);
        $('#new_area_form select[name="zone_id"]').select2().val(zone_id_active).trigger("change").prop('disabled', false);
        showAreaForm();
    });
    $('[data-action="new-location-popup"]').click(function (event) {
        $('#new_location_form')[0].reset();
        $('#new_location_form input[name="location_id"]').val("");
        $('#new-location-popup .title').html("New Location");
        $('#new_location_form [type="submit"]').html("Save");
        zone_id_active = $('#zones-holder li.active').attr('data-id');
        area_id_active = $('#areas-holder li.active').attr('data-id');
        var zone_html = ``;
        _places.zones.forEach(function (zone, key) {
            zone_html += `<option value="` + zone.zone_id + `">` + zone.zone_name + `</option>`;
        });
        $('#new_location_form select[name="zone_id"]').html(zone_html);
        var area_html = ``;
        _places.areas.forEach(function (area, key) {
            if (area.zone_id == zone_id_active) {
                area_html += `<option value="` + area.area_id + `">` + area.area_name + `</option>`;
            }
        });
        $('#new_location_form select[name="area_id"]').html(area_html);
        $('#new_location_form select[name="zone_id"]').select2().val(zone_id_active).trigger('change.select2').prop('disabled', false);
        $('#new_location_form select[name="area_id"]').select2().val(area_id_active).trigger('change.select2').prop('disabled', false);
        showLocationForm();
    });
    $('[data-action="new-landmark-popup"]').click(function (event) {
        $('#new_landmark_form')[0].reset();
        $('#new_landmark_form input[name="landmark_id"]').val("");
        $('#new-landmark-popup .title').html("New Landmark");
        $('#new_landmark_form [type="submit"]').html("Save");
        zone_id_active = $('#zones-holder li.active').attr('data-id');
        area_id_active = $('#areas-holder li.active').attr('data-id');
        location_id_active = $('#locations-holder li.active').attr('data-id');
        var zone_html = ``;
        _places.zones.forEach(function (zone, key) {
            zone_html += `<option value="` + zone.zone_id + `">` + zone.zone_name + `</option>`;
        });
        $('#new_landmark_form select[name="zone_id"]').html(zone_html);
        var area_html = ``;
        _places.areas.forEach(function (area, key) {
            if (area.zone_id == zone_id_active) {
                area_html += `<option value="` + area.area_id + `">` + area.area_name + `</option>`;
            }
        });
        $('#new_landmark_form select[name="area_id"]').html(area_html);
        var location_html = ``;
        _places.locations.forEach(function (location, key) {
            if (location.location_id == location_id_active) {
                location_html += `<option value="` + location.location_id + `">` + location.location_name + `</option>`;
            }
        });
        $('#new_landmark_form select[name="location_id"]').html(location_html);
        $('#new_landmark_form select[name="zone_id"]').select2().val(zone_id_active).trigger('change.select2').prop('disabled', false);
        $('#new_landmark_form select[name="area_id"]').select2().val(area_id_active).trigger('change.select2').prop('disabled', false);
        $('#new_landmark_form select[name="location_id"]').select2().val(location_id_active).trigger('change.select2').prop('disabled', false);
        showLandmarkForm();
    });
});
function showZoneForm() {
    new_zone_form_validator.resetForm();
    fancybox_show('new-zone-popup', { width: 400 })
}
function showAreaForm() {
    new_area_form_validator.resetForm();
    fancybox_show('new-area-popup', { width: 400 })
}
function showLocationForm() {
    new_location_form_validator.resetForm();
    fancybox_show('new-location-popup', { width: 400 })
}
function showLandmarkForm() {
    new_landmark_form_validator.resetForm();
    fancybox_show('new-landmark-popup', { width: 400 })
}
/***************************************************** */
$('#new_location_form select[name="zone_id"]').on('change', function () {
    var zone_id = this.value;
    var area_html = ``;
    _places.areas.forEach(function (area, key) {
        if (area.zone_id == zone_id) {
            area_html += `<option value="` + area.area_id + `">` + area.area_name + `</option>`;
        }
    });
    $('#new_location_form select[name="area_id"]').html(area_html);
    $('#new_location_form select[name="area_id"]').select2().val("").trigger('change.select2');
});
/***************************************************** */
$('#new_landmark_form select[name="zone_id"]').on('change', function () {
    var zone_id = this.value;
    var area_html = ``;
    _places.areas.forEach(function (area, key) {
        if (area.zone_id == zone_id) {
            area_html += `<option value="` + area.area_id + `">` + area.area_name + `</option>`;
        }
    });
    $('#new_landmark_form select[name="area_id"]').html(area_html);
    $('#new_landmark_form select[name="area_id"]').select2().val("").trigger('change.select2');
    $('#new_landmark_form select[name="location_id"]').html("").select2().val("").trigger('change.select2');
});
/***************************************************** */
$('#new_landmark_form select[name="area_id"]').on('change', function () {
    var area_id = this.value;
    var location_html = ``;
    _places.locations.forEach(function (location, key) {
        if (location.area_id == area_id) {
            location_html += `<option value="` + location.location_id + `">` + location.location_name + `</option>`;
        }
    });
    $('#new_landmark_form select[name="location_id"]').html(location_html).select2().val("").trigger('change.select2');
});
/***************************************************** */