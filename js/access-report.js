var validator = null;
var return_validator = null;
$("#access_type").select2({});
$(document.body).on("change", "#customer_id,#access_type", function () {
    get_available_accesses();
});
function get_available_accesses() {
    $('#access').html("").select2({});
    var customer_id = $('#customer_id').val();
    var access_type = $('#access_type').val();
    if (customer_id && access_type) {
        $('.mm-loader').show();
        $.ajax({
            type: "GET",
            url: _base_url + "access/get_available",
            data: {
                customer_id: customer_id,
                access_type: access_type
            },
            dataType: "json",
            cache: false,
            success: function (data) {
                var html = ""
                $.each(data, function (key, access) {
                    html += `<option value="` + access.id + `">` + access.code + `</option>`;
                });
                $('#access').html(html).select2({});
                $('.mm-loader').hide();
            },
            error: function (data) {
                $('.mm-loader').hide();
            }
        });
    }
}
$().ready(function () {
    
});
function closeFancy() {
    $.fancybox.close();
}
function view_access(id){
 $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "customer_access/view_access",
        data: {id: id },
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#view_code').val(value.code)
		 $('#view_c_name').val(value.customer_name)
		 $('#view_type').val(value.access_type)
		 $('#view_r_on').val(value.received_at)
		 $('#view_r_by').val(value.received_user)
		 $('#view_return_on').val(value.returned_at)
		 $('#view_return_by').val(value.returned_user)
		 $('#view_r_notes').val(value.received_notes)
		 $('#view_return_notes').val(value.returned_notes)
               
            });
  fancybox_show('view-popup',{ width: 850 });
  $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
   
}

$(document).on('change', '#access_type_filter', function(){
	var status = $(this).val();
	if(status != "")
	{
		window.location = _base_url + 'access/report/' + status;
	} else {
		window.location = _base_url + 'access/report';
	}
});


