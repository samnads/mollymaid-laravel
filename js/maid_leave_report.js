$('#start-date, #end-date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        //startDate: new Date()
    });
    
    
$('.da-table').dataTable( {
    "oLanguage": {
        "sEmptyTable":"<p style='color:red;text-align:center;'>No results</p>"
    }
} ); 

$('#maid_leave_extend .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    // startDate: "today",
    todayHighlight: true
});

$(document).on('click', '#add_leave_sub', function() {
    var leave_type=$("#leave_type").val();
    var leave_type_new=$("#leave_type_new").val();
    if(leave_type == '1') {
        var ltype= "Full Day"
    }
    else if(leave_type == '2') {
        var ltype= "Half Day"
    }
    
    if(leave_type_new == 'leave') {
        var ltypenew= "Leave"
    }
    else if(leave_type_new == 'emergency_leave') {
        var ltypenew= "Emergency Leaves"
    }else if(leave_type_new == 'vacations') {
        var ltypenew= "Vacations"
    }else if(leave_type_new == 'holidays') {
        var ltypenew= "Holidays"
    } else if(leave_type_new == 'medical_leaves') {
        var ltypenew= "Medical Leaves"
    }
    // else if(leave_type_new == 'absent') {
    //     var ltypenew= "Absent"
    // }else if(leave_type_new == 'dayoff') {
    //     var ltypenew= "Dayoff"
    // }else if(leave_type_new == 'offset') {
    //     var ltypenew= "Offset"
    // } else if(leave_type_new == 'sick leave') {
    //     var ltypenew= "Sick leave"
    // }
    // else if(leave_type_new == 'sm_replacement') {
    //     var ltypenew= "SM Replacement"
    // }
   
    
    var maidName=$("#search-maid-id option:selected").text();
$.fancybox.close(); 
 $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
		content : _alert_html = '<div id="alert-popup"><div class="head">Add Leave<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Do you want to mark '+ ltype +' leave for '+ maidName +' as '+ltypenew+' ?</div><div class="bottom"><input type="button" value="Yes" data-id="'+leave_type+'" data-bind="'+leave_type_new+'" class="leave_yes" style="background:#b2d157;border:1px solid" />&nbsp;&nbsp;<input type="button" value="No" class="leave_no pop_close"  /></div></div>',
		topRatio : 0.2,
		
	});   
 
    
});

$(document).on('click', '.leave_yes', function(){

    var maid_id=$("#search-maid-id").val();
    
    var start_date=$("#start-date").val();
    
    var end_date=$("#end-date").val();
    
    // var leave_type=$("#leave_type").val();
    var leave_type=$(this).attr('data-id');
    
    var leave_type_new=$(this).attr('data-bind');
    

 
    if(maid_id!=0 && start_date!="" && end_date!="" && leave_type!="" && leave_type_new!=""){
        $.fancybox.close(); 
        $("#LoadingImage").show();
        $.ajax({
            type: "POST",
            url: _base_url + "reports/maid-leave-report",
            data: {maid_id: maid_id,start_date:start_date,end_date:end_date,leave_type:leave_type,leave_type_new:leave_type_new,action:'add_leave'},
            dataType: "text",
            cache: false,
            success: function (result) {
                if(result=="success")
                {
                    $("#LoadingImage").hide();         
                    $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'none',
                        openSpeed : 1,
                        helpers : {
                            overlay : {
                                css : {
                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: true
                            }
                        },
                        padding : 0,
                        closeBtn : true,
                        content : _alert_html ='<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Leave marked successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>',
                        topRatio : 0.2,
                
                    });      
                }
                if(result=="error")
                {
                    $("#LoadingImage").hide();         
                    $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'none',
                        openSpeed : 1,
                        helpers : {
                            overlay : {
                                css : {
                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: true
                            }
                        },
                        padding : 0,
                        closeBtn : true,
                        content : _alert_html ='<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Current date leave available on this maid.</div><div class="bottom"><input type="button" value="Close" class="alert-popup-close cancel_btn pop_close" style="float:none;"  /></div></div>',
                        topRatio : 0.2,
                
                    });      
                }

            }
        });
    }
    else
    {
        $.fancybox.close();  
        alert("All fields are mandatory");  
      
    }
 
    $.fancybox.close();
 
});
 
 
$(document).on('click', '.pop_close', function(){
    $.fancybox.close();
 
});
 
 
 //Show / Hide add leave button based on maid display starts
$('#search-maid-id').change(function(){
   var maidVal = $(this).val();
   if(maidVal > 0) {
       $('#add_leave').show();
   } else {
       $('#add_leave').hide();
   }
});

$("#printButnforleave").click(function ()
{
    var divContents = $("#divForPrintLeave").html();
    var startdate = $("#start-date").val();
    var enddate = $("#end-date").val();
    //var day = $("#day").val();
  
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Maid Leave Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('<span style="margin-left:125px;">');
    printWindow.document.write('Date Range &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(startdate);
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;&nbsp;&nbsp;'+enddate);
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('</span>');
    //printWindow.document.write('<span style="margin-left:300px;">');
    //printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    //printWindow.document.write(zone);
    //printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});

  var maidVal = $('#search-maid-id').val();
   if(maidVal > 0) {
       $('#add_leave').show();
   } else {
       $('#add_leave').hide();
   }
//Show / Hide add leave button based on maid display ends

//Add leave type marking start
$(document).on('click', '#add_leave', function() {
    
 $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
		// content : _alert_html = '<div id="alert-popup"><div class="head">Enter Leave information<span class="alert-popup-close pop_close"></span></div><div class="content"  style="padding:0 20px;"> What type of leave do you prefer ?<br/><br/><select name="leave_type" id="leave_type" style="margin-bottom: 4px;" class="sel2"><option value="1">Full Day</option><option value="2">Half Day</option></select></div><div class="content" style="padding:5px 67px; text-align: left;"> Leave Type</div><div class="content"><select name="leave_type_new" id="leave_type_new" style="margin-bottom: 4px;" class="sel2"><option value="leave">Leave</option><option  value="sm_replacement">SM Replacement</option><option value="absent">Absent</option><option value="dayoff">Dayoff</option><option value="offset">Offset</option><option value="sick leave">Sick Leave</option></select></div><div class="bottom"><input type="button" value="Submit" id="add_leave_sub" style="background:#b2d157;border:1px solid" />&nbsp;</div></div>',
		content : _alert_html = '<div id="alert-popup"><div class="head">Enter Leave information<span class="alert-popup-close pop_close"></span></div><div class="content"  style="padding:0 20px;"> What type of leave do you prefer ?<br/><br/><select name="leave_type" id="leave_type" style="margin-bottom: 4px;" class="sel2"><option value="1">Full Day</option><option value="2">Half Day</option></select></div><div class="content" style="padding:5px 67px; text-align: left;"> Leave Type</div><div class="content"><select name="leave_type_new" id="leave_type_new" style="margin-bottom: 4px;" class="sel2"><option value="leave">Leave</option><option  value="emergency_leave">Emergency Leaves</option><option value="vacations">Vacations</option><option value="holidays">Holidays</option><option value="medical_leaves">Medical Leaves</option></select></div><div class="bottom"><input type="button" value="Submit" id="add_leave_sub" style="background:#b2d157;border:1px solid" />&nbsp;</div></div>',
		topRatio : 0.2,
		
	});   
 
    
});
//Add leave type marking end
$('#maid_leave_extend #leave-extend-btn').click(function () {
    $('.mm-loader').show();
    var formData = new FormData(document.getElementById("maid_leave_extend"));
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "reports/maid_leave_extend_data",
        data: $('#maid_leave_extend').serialize(),
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.status =="success") {
                toast('success', data.message);
                setTimeout(function () {
                    window.location.href = _base_url + "reports/maid-leave-report";
                }, 3000);
                $.fancybox.close();
            }
            else {
                $('#leave-extend-btn').html('Save').removeAttr("disabled");
                $.fancybox.close();
                toast('error', data.message);
            }
        },
        error: function (data) {
            $('#leave-extend-btn').html('Save').removeAttr("disabled");
            toast('error', data.message);
        },
    });
    
});
(function (a) {
    a(document).ready(function (b) {
        if(a('#da-ex-datatable-numberpaging').length > 0)
        {
        a("table#da-ex-datatable-numberpaging").dataTable({sPaginationType: "full_numbers", "bSort": false, "bDestroy": true, "iDisplayLength": 100});
        a("table#da-ex-datatable-default").dataTable({"iDisplayLength": 100});
    }
        
    });
    
})(jQuery);