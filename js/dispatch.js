/************************************************************************** */
/* Coder : Samnad. S                                                        */
/*                                                                          */
/* Last Updated by :                                                        */
/************************************************************************** */
// logic goes here...................
$('button[data-action="show-bookings-for-dispatch"]').click(function (event) {
    $('.mm-loader').show();
    var service_date = $(this).attr("data-dispatch-date");
    $('input[id="check_all"]').prop('checked', false);
    $('#bookings-dispatch-list .inner').html('<div class="inner">Loading booikings<span class="dots_loader"></span></div>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_bookings_for_dispatch",
        data: { 'service_date': service_date },
        success: function (bookings) {
            if (bookings.length == 0) {
                toast('warning', "No bookings available to dispatch.");
                return false;
            }
            var _maid_html = `<div class="table"><input type="hidden" name="dispatch_service_date" value="` + service_date + `">`;
            $.each(bookings, function (key, booking) {
                if (booking.day_service_id == null) {
                    _maid_html += `<div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                    <div class="col-sm-10 n-pick-maid-left pl-0">
                    <span id=""><strong>Booking No. &nbsp;</strong>` + booking.booking_id + `</span><br />
                    <span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;` + booking.customer_name + `</span>
                    <p><strong><i class="fa fa-clock-o" aria-hidden="true"></i></strong>&nbsp;` + moment(booking.time_from, ["HH:mm:ss"]).format("hh:mm A") + ` - ` + moment(booking.time_to, ["HH:mm:ss"]).format("hh:mm A") + ` ( ` + booking.maid_name + ` )</p>
                    </div>
                    <div class="col-sm-2 n-pick-maid-right pr-0">
                    <input type="checkbox" value="` + booking.booking_id + `" name="dispatch_booking_ids[]" id="dispatch_booking_id_` + key + `" ` + (booking.dispatch_status == 1 ? "disabled" : "") + `/>
                    <label for="dispatch_booking_id_` + key + `">
                    <span class="border-radius-3"></span>
                    </label>
                    <p class="text-success">`+ (booking.dispatch_status == 1 ? "Dispatched !" : "") + `</p>
                    </div></div>`;
                }
            });
            _maid_html += `</div>`;
            $('#bookings-dispatch-list .inner').html(_maid_html);
            fancybox_show('bookings-for-dispatch', { width: 600 });
        },
        error: function (data) {
            $('#alert-title').html('Error !');
            $('#alert-message').html('An error occured !');
            fancybox_show('alert-popup');
        },
    });
});
$('button[data-action="dispatch-now"]').click(function (event) {
    $('.mm-loader').show();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "dispatch/dispatch_bookings",
        data: $('#bookings-dispatch-form').serialize(),
        success: function (data) {
            if (data.status == true) {
                close_fancybox();
                toast('success', data.message);
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            close_fancybox();
            toast('error', 'An error occured !');
        },
    });
});
$(function () {
    $('#dispatch_service_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        //startDate: "today"
    }).on('changeDate', function (e) {
        var service_date = moment(e.format(), 'DD/MM/YYYY').format('YYYY-MM-DD');
        window.location.href = _base_url + "dispatch/list/" + service_date;
    });
    $('#bookings-for-dispatch #check_all').change(function () {
        if (this.checked) {
            $('input[name="dispatch_booking_ids[]"]').not(":disabled").prop("checked", true);
            if ($('input[name="dispatch_booking_ids[]"]:checked').length == 0) {
                toast('warning', "No booking available to dispatch.");
                $('#bookings-for-dispatch #check_all').prop("checked", false);
            }
        }
        else {
            $('input[name="dispatch_booking_ids[]"]').prop('checked', false);
        }
        $('input[name="dispatch_booking_ids[]"]').trigger("change");
    });
});
$(document).on('change', 'input[name="dispatch_booking_ids[]"]', function () {
    if ($('input[name="dispatch_booking_ids[]"]:checked').length == $('input[name="dispatch_booking_ids[]"]').not(":disabled").length) {
        $("#bookings-for-dispatch #check_all").prop("checked", true);
    }
    else {
        $("#bookings-for-dispatch #check_all").prop("checked", false);
    }
    if ($('input[name="dispatch_booking_ids[]"]:checked').length > 0) {
        $('button[data-action="dispatch-now"]').show();
    }
    else {
        $('button[data-action="dispatch-now"]').hide();
    }
})
/***************************************************************************************** */
$('button[data-action="dispatch-confirm"]').click(function (event) {
    $('.mm-loader').show();
    var service_date = moment($("#dispatch_service_date").val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
    $('#bookings-for-confirm-dispatch #check_all').prop('checked', false);
    $('#bookings-confirm-dispatch-list .inner').html('<div class="inner">Loading booikings<span class="dots_loader"></span></div>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_day_services_for_confirm_dispatch",
        data: { 'service_date': service_date },
        success: function (bookings) {
            if (bookings.length == 0) {
                toast('warning', "No dispatched bookings available to confirm.");
                return false;
            }
            var _maid_html = `<div class="table"><input type="hidden" name="dispatch_service_date" value="` + service_date + `">`;
            $.each(bookings, function (key, booking) {
                if (booking.dispatch_status == 1) {
                    // 1 means dispatched but not confirmed
                    _maid_html += `<div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                    <div class="col-sm-10 n-pick-maid-left pl-0">
                    <span id=""><strong>Booking Ref. &nbsp;</strong>` + booking.booking_reference_id + `</span><br />
                    <span id=""><strong>Schedule Ref. &nbsp;</strong>` + booking.day_service_reference_id + `</span><br />
                    <span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;` + booking.customer_name + `</span>
                    <p><strong><i class="fa fa-clock-o" aria-hidden="true"></i></strong>&nbsp;` + moment(booking.time_from, ["HH:mm:ss"]).format("hh:mm A") + ` - ` + moment(booking.time_to, ["HH:mm:ss"]).format("hh:mm A") + ` ( ` + booking.maid_name + ` )</p>
                    </div>
                    <div class="col-sm-2 n-pick-maid-right pr-0">
                    <input type="checkbox" value="` + booking.day_service_id + `" name="day_service_ids[]" id="day_service_id_` + key + `" ` + (booking.dispatch_status == 2 ? "disabled" : "") + `/>
                    <label for="day_service_id_` + key + `">
                    <span class="border-radius-3"></span>
                    </label>
                    <p class="text-success">`+ (booking.dispatch_status == 2 ? "Confirmed !" : "") + `</p>
                    </div></div>`;
                }
            });
            _maid_html += `</div>`;
            $('#bookings-confirm-dispatch-list .inner').html(_maid_html);
            fancybox_show('bookings-for-confirm-dispatch', { width: 600 });
        },
        error: function (data) {
            $('#alert-title').html('Error !');
            $('#alert-message').html('An error occured !');
            fancybox_show('alert-popup');
        },
    });
});
/***************************************************************************************** */
$('#bookings-for-confirm-dispatch #check_all').change(function () {
    $('input[name="day_service_ids[]"]').prop('checked', this.checked).trigger('change');
});
/***************************************************************************************** */
$(document).on('change', 'input[name="day_service_ids[]"]', function () {
    if ($('input[name="day_service_ids[]"]:checked').length == $('input[name="day_service_ids[]"]').not(":disabled").length) {
        $("#bookings-for-confirm-dispatch #check_all").prop("checked", true)
    }
    else {
        $("#bookings-for-confirm-dispatch #check_all").prop("checked", false)
    }
});
/***************************************************************************************** */
$('button[data-action="confirm-dispatch-now"]').click(function (event) {
    $('.mm-loader').show();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "dispatch/confirm_dispatched_services",
        data: $('#bookings-confirm-dispatch-form').serialize(),
        success: function (data) {
            if (data.status == true) {
                close_fancybox();
                toast('success', data.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            close_fancybox();
            toast('error', 'An error occured !');
        },
    });
});
/***************************************************************************************** */
$(document).on('change', 'input[name="day_service_ids[]"]', function () {
    if ($('input[name="day_service_ids[]"]:checked').length > 0) {
        $('button[data-action="confirm-dispatch-now"]').show();
    }
    else {
        $('button[data-action="confirm-dispatch-now"]').hide();
    }
});
/***************************************************************************************** */
(function (a) {
    a(document).ready(function (b) {
        if (a('#confirmed-dispatches-table').length > 0) {
            a("table#confirmed-dispatches-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,
                "orderMulti": false,
                "autoWidth": false,
                "iDisplayLength": 100,
                'columnDefs': [{
                    'targets': [-1],
                    'orderable': false
                },]
            });
        }
    });
})(jQuery);
/***************************************************************************************** */
(function (a) {
    a(document).ready(function (b) {
        if (a('#dispatches-table').length > 0) {
            a("table#dispatches-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,
                "iDisplayLength": 100,
                "scrollY": true,
                "orderMulti": false,
                "scrollX": true,
                'columnDefs': [{
                    'targets': [-1],
                    'orderable': false
                },]
            });
        }
    });
})(jQuery);
/***************************************************************************************** */
$('[data-tab]').click(function (event) {
    var tab_name = $(this).attr("data-tab");
    if (tab_name == "dispatched") {
        $('#dispatch-confirm-li').show();
        $('#dispatch-confirm-undo-li').hide();
    }
    else if (tab_name == "confirmed") {
        $('#dispatch-confirm-li').hide();
        $('#dispatch-confirm-undo-li').show();
    }
    else {
        $('#dispatch-confirm-li').hide();
        $('#dispatch-confirm-undo-li').hide();
    }
});
/***************************************************************************************** */
$('button[data-action="dispatch-confirm-undo"]').click(function (event) {
    $('.mm-loader').show();
    var service_date = moment($("#dispatch_service_date").val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
    $('#bookings-for-undo-confirm-dispatch #check_all').prop('checked', false);
    $('#bookings-confirm-dispatch-list .inner').html('<div class="inner">Loading booikings<span class="dots_loader"></span></div>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_day_services_for_undo_confirmed_dispatch",
        data: { 'service_date': service_date },
        success: function (bookings) {
            if (bookings.length == 0) {
                toast('warning', "No confirmed dispatches found.");
                return false;
            }
            var _maid_html = `<div class="table"><input type="hidden" name="dispatch_service_date" value="` + service_date + `">`;
            $.each(bookings, function (key, booking) {
                if (booking.dispatch_status == 2) {
                    // 2 means dispatch confirmed
                    _maid_html += `<div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                    <div class="col-sm-10 n-pick-maid-left pl-0">
                    <span id=""><strong>Booking Ref. &nbsp;</strong>` + booking.booking_reference_id + `</span><br />
                    <span id=""><strong>Schedule Ref. &nbsp;</strong>` + booking.day_service_reference_id + `</span><br />
                    <span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;` + booking.customer_name + `</span>
                    <p><strong><i class="fa fa-clock-o" aria-hidden="true"></i></strong>&nbsp;` + moment(booking.time_from, ["HH:mm:ss"]).format("hh:mm A") + ` - ` + moment(booking.time_to, ["HH:mm:ss"]).format("hh:mm A") + ` ( ` + booking.maid_name + ` )</p>
                    </div>
                    <div class="col-sm-2 n-pick-maid-right pr-0">
                    <input type="checkbox" value="` + booking.day_service_id + `" name="undo_day_service_ids[]" id="undo_day_service_id_` + key + `" ` + (booking.dispatch_status != 2 ? "disabled" : "") + `/>
                    <label for="undo_day_service_id_` + key + `">
                    <span class="border-radius-3"></span>
                    </label>
                    </div></div>`;
                }
            });
            _maid_html += `</div>`;
            $('#bookings-undo-confirm-dispatch-list .inner').html(_maid_html);
            fancybox_show('bookings-for-undo-confirm-dispatch', { width: 600 });
        },
        error: function (data) {
            $('#alert-title').html('Error !');
            $('#alert-message').html('An error occured !');
            fancybox_show('alert-popup');
        },
    });
});
/***************************************************************************************** */
$('#bookings-undo-confirm-dispatch-list #check_all_for_undo').change(function () {
    $('#bookings-undo-confirm-dispatch-list input[name="undo_day_service_ids[]"]').prop('checked', this.checked).trigger('change');
});
$(document).on('change', 'input[name="undo_day_service_ids[]"]', function () {
    if ($('input[name="undo_day_service_ids[]"]:checked').length == $('input[name="undo_day_service_ids[]"]').not(":disabled").length) {
        $("#bookings-undo-confirm-dispatch-list #check_all_for_undo").prop("checked", true);
    }
    else {
        $("#bookings-undo-confirm-dispatch-list #check_all_for_undo").prop("checked", false)
    }
});
$(document).on('change', 'input[name="undo_day_service_ids[]"]', function () {
    if ($('input[name="undo_day_service_ids[]"]:checked').length > 0) {
        $('button[data-action="confirm-undo-dispatch-now"]').show();
    }
    else {
        $('button[data-action="confirm-undo-dispatch-now"]').hide();
    }
});
/***************************************************************************************** */
$('button[data-action="confirm-undo-dispatch-now"]').click(function (event) {
    $('.mm-loader').show();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "dispatch/undo_confirmed_dispatched_services",
        data: $('#bookings-undo-confirm-dispatch-form').serialize(),
        success: function (data) {
            if (data.status == true) {
                close_fancybox();
                toast('success', data.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', 'An error occured !');
        },
    });
});
/***************************************************************************************** */
$('[data-action="print-this"]').click(function (event) {
    $("#" + $(this).attr("data-id")).printThis({});
});
/***************************************************************************************** */
$('#dispatch_edit_form select[name="time_from"]').on('change', function () {
    $('#dispatch_edit_form select[name="time_to"]').select2().val("").trigger("change");
    var from_time = this.value;
    var _time_to_options = '';
    $('#dispatch_edit_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#dispatch_edit_form select[name="time_to"]').html(_time_to_options);
    });
    $('#dispatch_edit_form select[name="time_to"]').select2().val("").trigger("change");//.select2('open');
});
/***************************************************************************************** */
// open dispatched schedule edit form
$('[data-action="edit-dispatched-schedule"]').click(function () {
    /********************************************************************* */
    $('#dispatch_edit_form select[name="time_from"]').prop("disabled", false);
    $('#dispatch_edit_form select[name="time_to"]').prop("disabled", false);
    $('#dispatch_edit_form input[name="service_rate_per_hour"]').prop("disabled", false);
    $('#dispatch_edit_form input[name="service_discount_rate_per_hour"]').prop("disabled", false);
    $('#dispatch_edit_form input[name="cleaning_materials"]').prop("disabled", false);
    $('#dispatch_edit_form .modal-footer').show();
    $('#edit-dispatched-schedule-popup .title').html("Edit Dispatched Schedule");
    /********************************************************************* */
    $('.mm-loader').show();
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_dispatched_schedule_for_edit",
        data: { day_service_id: $(this).attr("data-id") },
        success: function (response) {
            if (response.status == true) {
                var data = response.data;
                $('#dispatch_edit_form input[name="day_service_id"]').val(data.day_service_id);
                $('#dispatch_edit_form input[name="customer_name"]').val(data.customer_name);
                $('#dispatch_edit_form input[name="service_type_name"]').val(data.service_type_name);
                $('#dispatch_edit_form input[name="maid_name"]').val(data.maid_name);
                $('#dispatch_edit_form input[name="service_rate_per_hour"]').val(data.service_rate_per_hour);
                $('#dispatch_edit_form input[name="service_discount_rate_per_hour"]').val(data.service_discount_rate_per_hour);
                $('#dispatch_edit_form input[name="service_discount"]').val((data.service_rate_per_hour - data.service_discount_rate_per_hour));
                $('#dispatch_edit_form input[name="material_fee"]').val(data.material_fee);
                $('#dispatch_edit_form select[name="time_from"]').val(data.time_from).trigger('change.select2'); // this change event will be very specific to the select2
                var startTime = moment(data.time_from, 'HH:mm:ss');
                var endTime = moment(data.time_to, 'HH:mm:ss');
                var duration = moment.duration(endTime.diff(startTime));
                var hours = parseFloat(duration.asHours());
                $('#dispatch_edit_form input[name="working_hours"]').val(hours);
                $('#dispatch_edit_form input[name="service_amount"]').val((hours * data.service_discount_rate_per_hour) + data.material_fee);
                $('#dispatch_edit_form input[name="cleaning_materials"]').prop('checked', data.cleaning_material == "Y");
                $('#dispatch_edit_form input[name="service_vat_amount"]').val((service_vat_percentage / 100) * $('#dispatch_edit_form input[name="service_amount"]').val());
                $('#dispatch_edit_form input[name="taxed_total"]').val(data.total_fee);
                var _time_to_options = '';
                $('#dispatch_edit_form select[name="time_from"] option').each(function (index, option) {
                    if ($(option).val() > data.time_from) {
                        _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                    }
                    $('#dispatch_edit_form select[name="time_to"]').html(_time_to_options);
                });
                $('#dispatch_edit_form select[name="time_to"]').select2().val(data.time_to).trigger("change");
                $('#dispatch_edit_form input[name="service_date"]').val(moment(data.service_date).format("DD-MM-YYYY"));
                fancybox_show('edit-dispatched-schedule-popup', { width: 700 });
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
// open dispatched schedule view form
$('[data-action="view-dispatched-schedule"]').click(function () {
    /********************************************************************* */
    $('#dispatch_edit_form select[name="time_from"]').prop("disabled", true);
    $('#dispatch_edit_form select[name="time_to"]').prop("disabled", true);
    $('#dispatch_edit_form input[name="service_rate_per_hour"]').prop("disabled", true);
    $('#dispatch_edit_form input[name="service_discount_rate_per_hour"]').prop("disabled", true);
    $('#dispatch_edit_form input[name="cleaning_materials"]').prop("disabled", true);
    $('#dispatch_edit_form .modal-footer').hide();
    $('#edit-dispatched-schedule-popup .title').html("Dispatched Schedule");
    /********************************************************************* */
    $('.mm-loader').show();
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_dispatched_schedule_for_edit",
        data: { day_service_id: $(this).attr("data-id") },
        success: function (response) {
            if (response.status == true) {
                var data = response.data;
                $('#dispatch_edit_form input[name="day_service_id"]').val(data.day_service_id);
                $('#dispatch_edit_form input[name="customer_name"]').val(data.customer_name);
                $('#dispatch_edit_form input[name="service_type_name"]').val(data.service_type_name);
                $('#dispatch_edit_form input[name="maid_name"]').val(data.maid_name);
                $('#dispatch_edit_form input[name="service_rate_per_hour"]').val(data.service_rate_per_hour);
                $('#dispatch_edit_form input[name="service_discount_rate_per_hour"]').val(data.service_discount_rate_per_hour);
                $('#dispatch_edit_form input[name="service_discount"]').val((data.service_rate_per_hour - data.service_discount_rate_per_hour));
                $('#dispatch_edit_form input[name="material_fee"]').val(data.material_fee);
                $('#dispatch_edit_form select[name="time_from"]').val(data.time_from).trigger('change.select2'); // this change event will be very specific to the select2
                var startTime = moment(data.time_from, 'HH:mm:ss');
                var endTime = moment(data.time_to, 'HH:mm:ss');
                var duration = moment.duration(endTime.diff(startTime));
                var hours = parseFloat(duration.asHours());
                $('#dispatch_edit_form input[name="working_hours"]').val(hours);
                $('#dispatch_edit_form input[name="service_amount"]').val((hours * data.service_discount_rate_per_hour) + data.material_fee);
                $('#dispatch_edit_form input[name="cleaning_materials"]').prop('checked', data.cleaning_material == "Y");
                $('#dispatch_edit_form input[name="service_vat_amount"]').val((service_vat_percentage / 100) * $('#dispatch_edit_form input[name="service_amount"]').val());
                $('#dispatch_edit_form input[name="taxed_total"]').val(data.total_fee);
                var _time_to_options = '';
                $('#dispatch_edit_form select[name="time_from"] option').each(function (index, option) {
                    if ($(option).val() > data.time_from) {
                        _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                    }
                    $('#dispatch_edit_form select[name="time_to"]').html(_time_to_options);
                });
                $('#dispatch_edit_form select[name="time_to"]').select2().val(data.time_to).trigger("change");
                $('#dispatch_edit_form input[name="service_date"]').val(moment(data.service_date).format("DD-MM-YYYY"));
                fancybox_show('edit-dispatched-schedule-popup', { width: 700, closeClick: true });
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
/***************************************************************************************** */
$().ready(function () {
    dispatch_edit_form_validator = $('#dispatch_edit_form').validate({
        ignore: [],
        rules: {
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            }
        },
        messages: {
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_rate_per_hour: "Enter service rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#dispatch_edit_form #form_error').empty();
            $('#dispatch_edit_form button[type="submit"]').html('Saving...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "dispatch/update_dispatch",
                data: $('#dispatch_edit_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        close_fancybox();
                        $('#dispatch_edit_form button[type="submit"]').html('Save').attr("disabled", false);
                        setTimeout(function () {
                            location.reload();
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $('#dispatch_edit_form button[type="submit"]').html('Save').attr("disabled", false);
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    $('#dispatch_edit_form button[type="submit"]').html('Save').attr("disabled", false);
                    toast('error', data.statusText);
                },
            });
        }
    });
});
/***************************************************************************************** */
var cleaning_material_rate_per_hour = 10;
function reCalculateForm() {
    var startTime = moment($('#dispatch_edit_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#dispatch_edit_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#dispatch_edit_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#dispatch_edit_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#dispatch_edit_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#dispatch_edit_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#dispatch_edit_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#dispatch_edit_form input[name="material_fee"]').val(material_fee);
    $('#dispatch_edit_form input[name="service_amount"]').val(service_amount);
    $('#dispatch_edit_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#dispatch_edit_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#dispatch_edit_form input[name="service_discount"]').val(service_discount);
    $('#dispatch_edit_form input[name="service_rate_per_hour"]').valid();
    $('#dispatch_edit_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#dispatch_edit_form input[name='cleaning_materials'],#dispatch_edit_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#dispatch_edit_form input[name='service_discount_rate_per_hour'],#dispatch_edit_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});