$(document).ready(function () {
    re_assign_schedule_form_validator = $('#re_assign_schedule_form').validate({
        ignore: [],
        rules: {
            reason: {
                required: true,
            },
            
        },
        messages: {
            "reason": "Please enter reason",
        },
        errorPlacement: function (error, element) {
            
            // else {
                error.insertAfter(element);
           // }
        }
    });
});

$('#suspended_schedule_reassign_form #schedule-search-btn').click(function () {
    var formData = new FormData(document.getElementById("suspended_schedule_reassign_form"));
    $('.mm-loader').show();
    $.ajax({
        type: 'POST',
        dataType: "text",
        url: _base_url + "schedule_suspended/search_available_maid",
        data: $('#suspended_schedule_reassign_form').serialize(),
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {
            $('#maid_search').show();
            $('#maid_search').html(result);
            $('#maid_search').addClass('widget-content');
            $('.mm-loader').hide();
        },

        error: function (data) {
            $('#enquiry-save-btn').html('Save').removeAttr("disabled");
            toast('error', data.message);
        },
    });
    
});
$('#suspended_schedule_reassign_form .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    // startDate: "today",
    todayHighlight: true
});
function create_reassign_maid(maid_id,new_maid_name)
{
    var booking_id = $('#suspended_schedule_reassign_form input[name="booking_id"]').val();
    var delete_date_from = $('#suspended_schedule_reassign_form input[name="date_from"]').val();
    var delete_date_to = $('#suspended_schedule_reassign_form input[name="date_to"]').val();
    var time_to = $('#suspended_schedule_reassign_form select[name="time_to"]').val();
    var time_from = $('#suspended_schedule_reassign_form select[name="time_from"]').val();
    var maid_name = $('#suspended_schedule_reassign_form input[name="maid_name"]').val();
    $('#re_assign_schedule_form input[name="booking_id"]').val(booking_id);
    $('#re_assign_schedule_form input[name="maid_id"]').val(maid_id);
    $('#re_assign_schedule_form input[name="new_maid_name"]').val(new_maid_name);
    $('#re_assign_schedule_form input[name="maid_name"]').val(maid_name);
    $('#re_assign_schedule_form input[name="delete_date_from"]').val(delete_date_from);
    $('#re_assign_schedule_form input[name="delete_date_to"]').val(delete_date_to);
    $('#re_assign_schedule_form input[name="time_to"]').val(time_to);
    $('#re_assign_schedule_form input[name="time_from"]').val(time_from);
    // booking_on_change();
    fancybox_show("reassign-popup", {});
}

$('#re_assign_schedule_form #reassign-schedule-btn').click(function () {
    re_assign_schedule_form_validator.resetForm();
    if ($("#re_assign_schedule_form").valid()) {
        $('.mm-loader').show();
        // $('#enquiry-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("re_assign_schedule_form"));
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "schedule_suspended/re_assign_schedule",
            data: $('#re_assign_schedule_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status =="success") {
                    // $('#enquiry-save-btn').html('Saved');
                    toast('success', data.message);

                    setTimeout(function () {
                        window.location.href = _base_url + "schedule/suspended_view";
                    }, 3000);
                }
                else {
                    $('#enquiry-update-btn').html('Save').removeAttr("disabled");
                    toast('error', data.message);
                }
            },
            error: function (data) {
                $('#enquiry-update-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

$('#suspended_schedule_reassign_form select[name^="time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#suspended_schedule_reassign_form select[name="time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#suspended_schedule_reassign_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#suspended_schedule_reassign_form select[name="time_to"]').html(_time_to_options);
    });
    $('#suspended_schedule_reassign_form select[name="time_to"]').select2().val("").trigger("change");//.select2('open');
});
