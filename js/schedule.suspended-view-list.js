/************************************************************************** */
/*                                                                          */
/*                                                                          */
/************************************************************************** */
// logic goes here...................
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#tb-slide-right').click(function () {
    var position = $('.time_line .time_slider').position();
    var max_left = time_grid_width - 1342 + 3 + 5;
    var left_pos = position.left - 71;
    left_pos = left_pos < max_left ? max_left : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -left_pos + 71 + 'px',
    }, 71);

    if ($(window).width() <= 1366) {
        var max_left = time_grid_width - 1173 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        

    }
    if ($(window).width() <= 1280) {
        var max_left = time_grid_width - 1090 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        

    }
});

$('#tb-slide-left').click(function () {
    var position = $('.time_line .time_slider').position();
    var left_pos = position.left + 71;
    left_pos = left_pos > 0 ? 0 : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    //$('.maid_name').css('padding-left', -position.left + 'px');
    position.left = position.left == 0 ? -71 : position.left;
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -position.left + 'px',
    }, 71);
});

if ($(".booking-position").length > 0) {
    var stickyTop = $('.booking-position').offset().top;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= stickyTop) {
            $('.scroll-top-fix').addClass('box-fixed-top');
            $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
        } else {
            $('.scroll-top-fix').removeClass('box-fixed-top');
            $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
        }
    });
}
function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
function RefreshSomeEventListener() {
    $(".booked_bubble").off();
    $(".booked_bubble").on("click", function () {
        //alert('b');
    });
}
function renderBookings() {
    loader(true);
    // reset cells
    $('.booked_bubble').remove();
    $('.cell').removeClass("booked")
    //
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "laravel/booking/suspended_schedules_list",
        // data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() || undefined },
        data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() ? (moment($('#filter_end_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined },
        success: function (data) {
            var booking_delete_list = data.booking_delete_data;
            var suspend_count = data.suspend_count;
            var suspend_maid_absent_count = data.suspend_maid_absent_count;
            var suspend_by_customer_count = data.suspend_by_customer_count;
            var suspend_by_office_count = data.suspend_by_office_count;
            var unassigned_count = data.unassigned_count;
            var reassigned_count = data.reassigned_count;
            
            // var assigned_hours = data.assigned_hours;
            // var unassigned_hours = data.unassigned_hours;
            let booking_html = '';
            //
            // initializeDataTable(data.bookings);
            var deletedBookingsForDate = data.bookings.filter(function(booking) {
                // Add your condition here to filter deleted bookings for a particular date
                let delete_list = booking_delete_list.find(delete_list => {
                    return delete_list.booking_id == booking.booking_id;
                });
                return delete_list != null; // Include only deleted bookings
            });
            initializeDataTable(deletedBookingsForDate);
            var count = 1;
            $.each(data.bookings, function (key, booking) {
                
                
                let delete_list = booking_delete_list.find(delete_list => {
                    return delete_list.booking_id == booking.booking_id
                });
               
                // // Assigned hours
                // var total_assigned_hours = assigned_hours / 60;
                // $('#assigned_hours').html(total_assigned_hours);

                // // UnAssigned hours
                // var total_unassigned_hours = unassigned_hours / 60;
                // $('#unassigned_hours').html(total_unassigned_hours);

                //unassigned count
                $('#unassigned_count').html(unassigned_count);

                // Reassigned count
                $('#reassigned_count').html(reassigned_count);
                
                // Total Suspended Schedules
                $('#total_suspended_schedules').html(suspend_count);
                
                // Maid absent count 
                $('#maid_absent_count').html(suspend_maid_absent_count);

                //  by customer count
                $('#by_customer').html(suspend_by_customer_count);
                
                //  by customer count
                $('#by_office').html(suspend_by_office_count);

                // $new = data.deleted_booking_data.filter(booking.booking_id)

               
            });
            // $('#booking_customer').html(booking_html);
            RefreshSomeEventListener();
            styleThisWeekCells();
            loader(false);
        },
        error: function (data) {
            toast('warning', "An error occured !");
            loader(false);
        },
    });
}
function initializeDataTable(dataArray) {
    var dataTable; // Define dataTable as a local variable
    if ($.fn.DataTable.isDataTable('#package-list-table')) {
        $('#package-list-table').DataTable().destroy();
    }

    var tableData = [];

    // Populate tableData with data from dataArray
    if (Array.isArray(dataArray)) {
        tableData = dataArray.map(function(data, index) {
            if (data.reassign_status == null) {
                var book_status = data.delete_status == null ? "Active" : data.delete_booking_type;
            } else {
                var book_status = data.reassign_status;
            }
            if (data.customer_booktype == "0") {
                var customer_booktype = "One-off";
            } else if (data.customer_booktype == "1") {
                var customer_booktype = "Regular";
            }
            return {
                sl_no: index + 1, // Add serial number
                customer_code: data.customer_code,
                customer_name: data.customer_name,
                customer_type: customer_booktype,
                week_name: data.week_name,
                booking_type: data.booking_type,
                maid_name: data.maid_name,
                time_from: moment(data.time_from, 'HH:mm:ss').format('hh:mm A'),
                time_to: moment(data.time_to, 'HH:mm:ss').format('hh:mm A'),
                working_minutes: data.working_minutes / 60,
                service_rate_per_hour: data._service_rate_per_hour,
                total_amount: data._total_amount,
                booking_zone: data.booking_zone,
                area_name: data.area_name,
                book_status: book_status
            };
        });
    } else {
        console.error("dataArray is not an array.");
        return; // Exit function if dataArray is not an array
    }

    // Initialize DataTable
    $('#package-list-table').DataTable({
        data: tableData,
        columns: [
            { data: 'sl_no', name: 'sl_no' }, // Slno column
            { data: 'customer_code', name: 'customer_code' },
            { data: 'customer_name', name: 'customer_name' },
            { data: 'customer_type', name: 'customer_type' },
            { data: 'week_name', name: 'week_name' },
            { data: 'booking_type', name: 'booking_type' },
            { data: 'maid_name', name: 'maid_name' },
            { data: 'time_from', name: 'time_from' },
            { data: 'time_to', name: 'time_to' },
            { data: 'working_minutes', name: 'working_minutes' },
            { data: 'service_rate_per_hour', name: 'service_rate_per_hour' },
            { data: 'total_amount', name: 'total_amount' },
            { data: 'booking_zone', name: 'booking_zone' },
            { data: 'area_name', name: 'area_name' },
            { data: 'book_status', name: 'book_status' },
            // Add more columns as needed
        ]
    });

    // Adding slno to each row
    // $('#package-list-table').DataTable.on('order.dt search.dt', function () {
    //     $('#package-list-table').DataTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
    //         cell.innerHTML = i + 1;
    //     });
    // }).draw();
}
function styleThisWeekCells() {
    var currentTime = moment();
    var dow = currentTime.day();
    var time_rounded = moment(currentTime).format("H:00:00");
    $('.cell').removeClass("current-week-day");
    $('.cell').removeClass("current-time");
    $('.cell').removeAttr('style');
    $('.cell').each(function (index, option) {
        /******* same week day highlight  */
        if ($(this).attr("data-week") == dow) {
            $(this).addClass("current-week-day");
        }
        /******* current time highlight  */
        if ($(this).attr("data-time-from") == time_rounded) {
            //$(this).addClass("current-time");
            $(this).css({ "border-right": "1px solid rgb(255 150 150)" });
        }
    });
}
$(document).ready(function () {
    
    re_assign_schedule_form_validator = $('#re_assign_schedule_form').validate({
        ignore: [],
        rules: {
            reason: {
                required: true,
            },
            
        },
        messages: {
            "reason": "Please enter reason",
        },
        errorPlacement: function (error, element) {
            
            // else {
                error.insertAfter(element);
           // }
        }
    });
    renderBookings();
});

/********************************************************************************** */
// date picker
$('.datepicker').datepicker({ autoclose: true, todayHighlight: true, todayBtn: "linked" }).on('changeDate', function (event) {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var date_full = moment(date).format('DD/MM/YYYY');
    $('.hed-date-main-box').html(date_full);
    $('#date').val(date);
});
$('.hed-date-left-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(-1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('.hed-date-right-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
/***************************************************************************************** */
$('#new_booking_form select[name="time_from"]').on('change', function () {
    $('#new_booking_form input[name="working_hours"]').val(0);
    $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change.select2");
});
$('#new_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
/***************************************************************************************** */
function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#new_booking_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
/***************************************************************************************** */
$('#new_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_start_date"]').valid();
    var booking_date = $(this).val();
    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    $('#new_booking_form input[name="service_week_day"]').val(dow);
    $('#new_booking_form input[name="service_week_name"]').val(days[dow]);

    // alert(week_day)
    $('#new_booking_form input[name="service_end_date"]').datepicker('update', null).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").add(7 * 3, 'days').format('DD/MM/YYYY'));
});
$('#new_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_end_date"]').valid();
});
$('#new_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
}).on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="time_to"]').select2().on("change", function (e) {
    $(this).valid();
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_week_day"]').on('change', function () {
    var week_day_id = this.value;
    var maid_id = $('#schedule-menu select[name="filter_maid_id"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_maid_id"]').on('change', function () {
    var maid_id = this.value;
    var week_day_id = $('#schedule-menu select[name="filter_week_day"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
function maidHeads(show) {
    if (show == true) {
        $('#weeks .maid_name_week').show();
        $('.tb-slider .maid_name').show();
    }
    else {
        $('#weeks .maid_name_week').hide();
        $('.tb-slider .maid_name').hide();
    }
}
function maidWeeks(show) {
    if (show == true) {
        $('#weeks .week').show();
    }
    else {
        $('#weeks .week').hide();
    }
}
function slots(show) {
    if (show == true) {
        $('.tb-slider .slots').show();
    }
    else {
        $('.tb-slider .slots').hide();
    }
}
/***************************************************************************************** */
$().ready(function () {
    // var dataTable;

    // if ($('#package-list-table').length > 0) {
    //   dataTable = $('table#package-list-table').DataTable({
    //     'sPaginationType': 'full_numbers',
    //     'bSort': true,
    //     'iDisplayLength': 100,
    //     'scrollY': true,
    //     'orderMulti': false,
    //     'scrollX': true,
    //     'columnDefs': [{
    //       'targets': [-1],
    //       'orderable': false
    //     }]
    //   });
    // }
    new_booking_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: false,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            },
            cleaning_material_rate_per_hour: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
            cleaning_material_rate_per_hour: "Enter materials per hour rate."
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "cleaning_material_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#new_booking_form").serializeArray();
            var service_start_date_i = values.map(function (o) { return o.name; }).indexOf("service_start_date");
            var service_end_date_i = values.map(function (o) { return o.name; }).indexOf("service_end_date");
            if (values[service_start_date_i]['value']) {
                values[service_start_date_i]['value'] = moment(values[service_start_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            if (values[service_end_date_i]['value']) {
                values[service_end_date_i]['value'] = moment(values[service_end_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/" + ($('#new_booking_form input[name="booking_id"]').val() == "" ? "save_booking" : "update_booking"),
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
/***************************************************************************************** */
var cleaning_material_rate_per_hour = 0;
function reCalculateForm() {
    cleaning_material_rate_per_hour = $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val();
    var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#new_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#new_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#new_booking_form input[name="material_fee"]').val(material_fee);
    $('#new_booking_form input[name="service_amount"]').val(service_amount);
    $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#new_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#new_booking_form input[name="service_discount"]').val(service_discount);
    //$('#new_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#new_booking_form input[name='cleaning_materials'],#new_booking_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $(this).valid();
});
$("#new_booking_form input[name='service_discount_rate_per_hour'],#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
$(function () {
    $('[data-toggle="popover"]').popover()
})
/***************************************************************************************** */
// page filter options
$('#filter_start_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    // startDate: "today"
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_start_date"]').valid();
    var end_date = moment(this.value, "DD.MM.YYYY");
    var end_date = end_date.add(5, 'days').format('DD/MM/YYYY');
    $('#filter_end_date').datepicker('update', end_date).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").format('DD/MM/YYYY'));
    renderBookings();
});
$('#filter_end_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_end_date"]').valid();
});

$('#schedulte_delete').click(function () {
    // var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    // var service_date = $('#new_booking_form input[name="service_date"]').val();
    // var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    fancybox_show('alert-popup', { width: 450 });
    //    $('#day_service_id').val(day_service_id);
    $('#booking_id_perment').val(booking_id);
    $('#booking_id_day').val(booking_id);
    //    $('#service_date').val(service_date);
    //    $('#day_service_status').val(service_status);
   
});

$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    // $('.n-delete-set-left').addClass('de-select');

    //$('.n-delete-set-right-cont').show(500);
     $('.n-delete-set-left-cont').show(500);
});

// $(".n-delete-set-left").click(function () {
//     $('.n-delete-set-left').removeClass('de-select');
//     $('.n-delete-set-right').addClass('de-select');

//     $('.n-delete-set-left-cont').show(500);
//     $('.n-delete-set-right-cont').hide(500);
// });

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    var booking_id = $('#booking_id_perment').val();
    if(remarks == "")
    {
        $("#delete_remark_perm").focus();
         $('#deleteremarks_book').css('display','block');
    } else {
        $('#deleteremarks_book').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#booking_id_day').val();
    if(remarks == "")
    {
        $("#delete_remark_day").focus();
         $('#deleteremarks_book_day').css('display','block');
    } else {
        $('#deleteremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$('#schedule_re_assign').click(function () {
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var delete_from_date = $('#new_booking_form input[name="delete_from_date"]').val();
    var delete_to_date = $('#new_booking_form input[name="delete_to_date"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var maid_name =  $('#new_booking_form input[name="maid_name"]').val();
    // fancybox_show('reassign-popup', { width: 450 });
    // $('#new-booking-form-popup').hide();
    var maid_id =  $('#new_booking_form input[name="maid_id"]').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: _base_url + 'schedule_suspended/maid_availability',
        data: {time_from:time_from, time_to:time_to, booking_id:booking_id, delete_from_date:delete_from_date,
            delete_to_date:delete_to_date, maid_id:maid_id, maid_name:maid_name},
        // cache: false,
        success: function (data)
            {
                // window.location.href= data.redirect_url;
                // window.location.href="<?php echo base_url('test/'.data);";
                // if (data.status == "success") {
                //     // url: _base_url + "laravel/booking/suspended_schedules",
                window.location.href = _base_url + "schedule_suspended/maid_reassign?booking_id=" + data.booking_id + "&time_from=" +data.time_from+ 
                  "&time_to=" +data.time_to+ "&delete_to_date=" +data.delete_to_date+ "&delete_from_date=" +data.delete_from_date+ "&maid_id=" +data.maid_id+
                  "&maid_name=" +data.maid_name;
            // }
           
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
   
});

$('#re_assign_schedule_form input[name="delete_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#re_assign_schedule_form input[name="delete_date_to"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});