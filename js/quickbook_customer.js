var quickbookcustomerlisttable;
$(document).ready(function () {
    quickbookcustomerlisttable = $('#quickbookcustomerlisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 100,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'quickbook/list_ajax_customer_list',
            'data': function (data) {
                // if ($('#cust_from_date').length) {
                    // var regdate = $('#cust_from_date').val();
                    // data.regdate = regdate;
                // } else {
                    // data.regdate = '';
                // }
                // if ($('#cust_to_date').length) {
                    // var regdateto = $('#cust_to_date').val();
                    // data.regdateto = regdateto;
                // } else {
                    // data.regdateto = '';
                // }
                // data.paytype = $('#payment_type').val();
                // data.sourceval = $('#sort_source').val();
                // data.customertype = $('#sort_cust_type').val();
                // data.useractive = $('#all-customers').val();
                data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'name' },
            { data: 'mobile' },
            { data: 'addeddate' },
        ],
        // "columnDefs": [{
            // "targets": 1,
            // "visible": false
        // }]
    });    

    $("#keyword-search").keyup(function () {
        var vall = $('#keyword-search').val().length
        // if (vall >= 3) {
		quickbookcustomerlisttable.draw();
        // }
    });
});

function closeFancy() {
    $.fancybox.close();
}

function confirm_disable_enable_modal() {
    $.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#enable-popup'),
	});
}

function sync_customers() {
	$('.mm-loader').css('display','block');
    $.ajax({
        type: "POST",
        url: _base_url + "quickbook/sync_all_customer_quickbooks",
        data: { },
        dataType: "text",
        cache: false,
        success: function (result) {
			closeFancy();
			$('.mm-loader').css('display','none');
			var _resp = $.parseJSON(result);
            if(_resp.status == "error") {
                toastr.error(_resp.message);
				quickbookcustomerlisttable.draw();
            } else if(_resp.status == "success") {
				$("#sync-error-div").html(_resp.message);
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'none',
					openSpeed: 1,
					autoSize: false,
					width: 'auto',
					height: 'auto',
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding: 0,
					closeBtn: false,
					content: $('#disable-popup'),
				});
				quickbookcustomerlisttable.draw();
			} else {
               toastr.error("Something went wrong."); 
			   quickbookcustomerlisttable.draw();
            }
        }
    });
    
	// $('.mm-loader').css('display','block');
    // quickbookcustomerlisttable.draw();
}