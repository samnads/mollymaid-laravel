var validator = null;
var return_validator = null;
$('button[data-action="return-access"]').click(function (event) {
    $('.mm-loader').show();
    //return false;
    $('#return-form-error').html("");
    $('#return_access_id').val($(this).attr("data-id"));
    $.ajax({
        type: "GET",
        url: _base_url + "access/assigned_data",
        data: {
            id: $(this).attr("data-id")
        },
        dataType: "json",
        cache: false,
        success: function (data) {
            $('#customer_name').html(data.customer_name);
            $('.access_type').html(data.access_type);
            $('.access_code').html(data.code);
            $('#returned_notes').html(data.returned_notes);
            fancybox_show('return-access-popup', {});
        },
        error: function (data) {
            $('.mm-loader').hide();
        }
    });
});
$('[data-action="view-access"]').click(function (event) {
    $('.mm-loader').show();
    //return false;
    $('#return-form-error').html("");
    $('#return_access_id').val($(this).attr("data-id"));
    $.ajax({
        type: "GET",
        url: _base_url + "access/assigned_data",
        data: {
            id: $(this).attr("data-id")
        },
        dataType: "json",
        cache: false,
        success: function (data) {
            $('.customer_name').html(data.customer_name);
            $('.access_type').text(data.access_type);
            $('.access_code').html(data.code);
            $('.received_notes').html(data.received_notes);
            $('.returned_notes').html(data.returned_notes);
            fancybox_show('view-access-popup', { closeClick :true});
        },
        error: function (data) {
            $('.mm-loader').hide();
        }
    });
});
function assignAccessPopUp() {
    $('#access').html("").select2({});
    $("#customer_id").val('').change();
    $("#access_type").val('').change();
    $('#notes').val("").change();
    validator.resetForm();
    fancybox_show('new-popup', {});
}
$("#customer_id").select2({
    allowClear: true,
    ajax: {
        url: _base_url + "access/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
});
$('#access-return-form select[name="returned_by_employee_id"]').select2({
    allowClear: true,
    ajax: {
        url: _base_url + "customer_access/returned_by_employee_field_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            return { results: response };
        },
        cache: false
    },
});
$("#access_type").select2({});
$(document.body).on("change", "#customer_id,#access_type", function () {
    get_available_accesses();
});
function get_available_accesses() {
    $('#access').html("").select2({});
    var customer_id = $('#customer_id').val();
    var access_type = $('#access_type').val();
    if (customer_id && access_type) {
        $('.mm-loader').show();
        $.ajax({
            type: "GET",
            url: _base_url + "access/get_available",
            data: {
                customer_id: customer_id,
                access_type: access_type
            },
            dataType: "json",
            cache: false,
            success: function (data) {
                var html = ""
                $.each(data, function (key, access) {
                    html += `<option value="` + access.id + `">` + access.code + `</option>`;
                });
                $('#access').html(html).select2({});
                $('.mm-loader').hide();
            },
            error: function (data) {
                $('.mm-loader').hide();
            }
        });
    }
}
$().ready(function () {
    validator = $('#access-assign-form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: true,
            },
            access_type: {
                required: true,
            },
            access: {
                required: true,
            },
            received_notes: {
                required: false,
            },
        },
        messages: {
            customer_id: "Select customer.",
            access_type: "Select access type.",
            access: "Select access.",
            received_notes: "Add note.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("id") == "access_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("id") == "access") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            $('#access-assign-btn').html('Assigning...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "access/assign",
                data: $('#access-assign-form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        close_fancybox();
                        window.location.href = _base_url + "access/assign/list";
                    }
                    else {
                        $('#form-error').html(data.message);
                        $('#access-assign-btn').html('Assign').removeAttr("disabled");
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    alert("An error occured!");
                    $('#access-assign-btn').html('Assign').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
    return_validator = $('#access-return-form').validate({
        ignore: [],
        rules: {
            return_access_id: {
                required: true,
            },
            returned_notes: {
                required: false,
            },
            returned_by_employee_id: {
                required: true,
            },
        },
        messages: {
            returned_notes: "Add return note.",
            returned_by_employee_id:"Please Select a Returning Employee",
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#return-form-error').empty();
            $('#access-return-btn').html('Returning...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "access/return",
                data: $('#access-return-form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        close_fancybox();
                        window.location.href = _base_url + "access/assign/list";
                    }
                    else {
                        toast('error', data.message);
                        $('#access-return-btn').html('Return').removeAttr("disabled");
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    $('#return-form-error').html("An error occured!");
                    $('#access-return-btn').html('Return').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
});
function closeFancy() {
    $.fancybox.close();
}
function view_access(id){
 $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "customer_access/view_access",
        data: {id: id },
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#view_code').val(value.code)
		 $('#view_c_name').val(value.customer_name)
		 $('#view_type').val(value.access_type)
		 $('#view_r_on').val(value.received_at)
		 $('#view_r_by').val(value.received_user)
		 $('#view_return_on').val(value.returned_at)
		 $('#view_return_by').val(value.returned_user)
		 $('#view_r_notes').val(value.received_notes)
		 $('#view_return_notes').val(value.returned_notes)
               
            });
  fancybox_show('view-popup',{ width: 850 });
  $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
   
}

$(document).on('change', '#access_type_filter', function(){
	var status = $(this).val();
	if(status != "")
	{
		window.location = _base_url + 'access/assign/list/' + status;
	} else {
		window.location = _base_url + 'access/assign/list';
	}
});

function confirm_delete_modal(id){
    $('#delete_keyid').val(id);
 fancybox_show('delete-popup', { width: 450 });
}

function confirm_delete() {
    var keyId = $('#delete_keyid').val();

    // Check if 'returned_by_user' and 'returned_at' are null
    $.ajax({
        type: "POST",
        url: _base_url + "customer_access/check_returned",
        data: { key_id: keyId },
        dataType: "json",
        cache: false,
        success: function (response) {
            if (response.status == 'null') {
              toast("error","Cannot be deleted.It is being used by the customer");
	       $.fancybox.close();
            } else {
                $.ajax({
                    type: "POST",
                    url: _base_url + "customer_access/remove_access",
                    data: { key_id: keyId },
                    dataType: "text",
                    cache: false,
                    success: function (result) {
                        fancybox_show('delete-success-popup', { width: 450 });
                        location.reload();
                    },
                    error: function (data) {
                        alert(data.statusText);
                        console.log(data);
                    }
                });
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}