$().ready(function () {
    $('li[data-tab="booking"] a').trigger('click'); // active tab
    booking_on_change();
    booking_on_enquiry_change();
    $("#call_management_booking_form select[name='booking_type']").change(function () {
        // reCalculateForm();
    var booking_type =  $(this).val();
    // alert(booking_type)
        if (booking_type == 'WE') {
            $('#create_schedule_booking_form #booking_service_date').show();
        } else {
            $('#create_schedule_booking_form #booking_service_date').hide();
        }
    });

    $("#call_management_enquiry_form select[name='booking_type_enquiry']").change(function () {
        // reCalculateForm();
    var booking_type =  $(this).val();
    // alert(booking_type)
        if (booking_type == 'WE') {
            $('#create_enquiry_schedule_booking_form #booking_service_date_enquiry').show();
        } else {
            $('#create_enquiry_schedule_booking_form #booking_service_date_enquiry').hide();
        }
    });
});
$('#mytabs li').click(function () {
    var tab_id = $(this).attr("data-tab");
    $('.tab-panel').removeClass('active').css('display', 'none');
    $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
    if (tab_id == "booking")
    {
        var called_for = "1";
        $('#maid_search').show();
        $('#maid_enquiry_search').hide();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $("#booking_customer_re_active_details").hide();
        $('#booking_customer_details').hide();


    } 
    else if (tab_id == "enquiry")
    {
        var called_for = "2";
        $('#maid_search').hide();
        $('#maid_enquiry_search').show();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $("#booking_customer_re_active_details").hide();
        $('#booking_customer_details').hide();


    } 
    
    else if (tab_id == "complaints")
    {
        var called_for = "3";
        $('#maid_search').hide();
        $('#maid_enquiry_search').hide();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $('#booking_customer_details').hide();
        $("#booking_customer_re_active_details").hide();

    } 
    else if (tab_id == "re-activations")
    {
        var called_for = "5";
        $('#maid_search').hide();
        $('#maid_enquiry_search').hide();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $('#booking_customer_details').hide();
        $("#booking_customer_re_active_details").show();

    } 
    else if (tab_id == "suspend-service")
    {
        var called_for = "4";
        $('#maid_search').hide();
        $('#maid_enquiry_search').hide();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $("#booking_customer_re_active_details").hide();
        $('#booking_customer_details').show();


    } 
    else if (tab_id == "preferences")
    {
        var called_for = "6";
        $('#maid_search').hide();
        $('#maid_enquiry_search').hide();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $("#booking_customer_re_active_details").hide();
        $('#booking_customer_details').hide();


    } 
    else if (tab_id == "feedback")
    {
        var called_for = "9";
        $('#maid_search').hide();
        $('#maid_enquiry_search').hide();
        $("#called_for").select2();
        $("#called_for").val(called_for); // Select the option
        $("#called_for").trigger('change');
        $("#booking_customer_re_active_details").hide();
        $('#booking_customer_details').hide();

    } 
    else {
        $('#maid_search').hide();
        $('#maid_enquiry_search').hide();
        // ("#booking_customer_details").hide();

    }
    // booking
});

$('#call_management_enquiry_form select[name^="availability_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#call_management_enquiry_form select[name="availability_time_to[' + id + ']"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#call_management_enquiry_form select[name="availability_time_from[' + id + ']"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#call_management_enquiry_form select[name="availability_time_to[' + id + ']"]').html(_time_to_options);
    });
    $('#call_management_enquiry_form select[name="availability_time_to[' + id + ']"]').select2().val("").trigger("change");//.select2('open');
});
$('#call_management_enquiry_form select[name^="availability_time_to"]').on('change', function () {
    var name = $(this).attr("name");
    var to_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    var from_time = $('#call_management_enquiry_form select[name="availability_time_from[' + id + ']"]').val();

    if (from_time && to_time) {
        $('#call_management_enquiry_form input[name="working_hour[' + id + ']"]').val(hourDifference(from_time, to_time));
    } else {
        // Clear 'working_hour' field if from_time or to_time is not selected
        $('#call_management_enquiry_form input[name="working_hour[' + id + ']"]').val('');
    }
});

$('#call_management_enquiry_form select[name^="enquiry_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#call_management_enquiry_form select[name="enquiry_time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#call_management_enquiry_form select[name="enquiry_time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#call_management_enquiry_form select[name="enquiry_time_to"]').html(_time_to_options);
    });
    $('#call_management_enquiry_form select[name="enquiry_time_to"]').select2().val("").trigger("change");//.select2('open');
});

$('#create_schedule_booking_form select[name^="schedule_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#create_schedule_booking_form select[name="schedule_time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#create_schedule_booking_form select[name="schedule_time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#create_schedule_booking_form select[name="schedule_time_to"]').html(_time_to_options);
    });
    $('#create_schedule_booking_form select[name="schedule_time_to"]').select2().val("").trigger("change");//.select2('open');
});

$('#create_enquiry_schedule_booking_form select[name^="schedule_enquiry_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_to"]').html(_time_to_options);
    });
    $('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_to"]').select2().val("").trigger("change");//.select2('open');
});


$('#call_management_booking_form select[name^="availability_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#call_management_booking_form select[name="availability_time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#call_management_booking_form select[name="availability_time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#call_management_booking_form select[name="availability_time_to"]').html(_time_to_options);
    });
    $('#call_management_booking_form select[name="availability_time_to"]').select2().val("").trigger("change");//.select2('open');
});

$('#call_management_booking_form select[name^="booking_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#call_management_booking_form select[name="booking_time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#call_management_booking_form select[name="booking_time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#call_management_booking_form select[name="booking_time_to"]').html(_time_to_options);
    });
    $('#call_management_booking_form select[name="booking_time_to"]').select2().val("").trigger("change");//.select2('open');
});

$('#call_management_preferences_form select[name^="preference_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#call_management_preferences_form select[name="preference_time_to"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#call_management_preferences_form select[name="preference_time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#call_management_preferences_form select[name="preference_time_to"]').html(_time_to_options);
    });
    $('#call_management_preferences_form select[name="preference_time_to"]').select2().val("").trigger("change");//.select2('open');
});

$("#area_id").on('change', function(){
    var area_id = $(this).val();
    $.ajax({
       url: _base_url + 'call-management/getlocation',
       type: 'POST',
       data: {area_id: area_id},
       dataType: 'json',
       success: function(response) {
          // Clear the previous options
          $('#location_id').empty();
          

          // Add the new options based on the response
          $.each(response, function(index, location) {
            
             $('#location_id').append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
             
          });
       }
    });
 })
 $('input[name="booking_date"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
// }).on('changeDate', function (e) {
//     var booking_date = $(this).val();
//     const date = moment(booking_date, 'DD/MM/YYYY');
//     const dow = date.day();
//     var booking_type = $("#booking_type").val();
    
//     $('input[class="week_day"]').prop('checked', false)
//     if (booking_type == "OD") {
//         $('.weekly_booking_time').hide();
//         $('.od_booking_time').show();
//         $('input[name="availability_checked['+ dow +']"]').each(function(){                                   
//             $(this).prop("checked", true);                      
//         });
//         $("input.week_day").attr("disabled", true);

//     } else {
//         $('.weekly_booking_time').show();
//         $('.od_booking_time').hide();


//         $("input.week_day").attr("disabled", false);
//         // $('input.week_time').attr('disabled', false)
//     }
});

// $('input[name="complaint_date"]').datepicker({
//     format: 'dd/mm/yyyy',
//     autoclose: true,
// }).on('changeDate', function (e) {

// });


 $('#call_management_enquiry_form .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
// $('#call_management_re_activations_form .input-daterange').datepicker({
//     format: 'dd/mm/yyyy',
//     autoclose: true,
//     startDate: "today",
//     todayHighlight: true
// });

$('#call_management_preferences_form .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#call_management_suspend_service_form .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#call_management_suspend_service_form input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#call_management_enquiry_form input[name="enquiry_booking_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#call_management_re_activations_form input[name="re_activation_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#call_management_feedback_form input[name="feedback_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$().ready(function () {
    $("#customer_id").select2({
        language: {
            noResults: function() { return '<a href="javascript:void(0);" id="add-customer-popup" onclick="return add_customer()">Add Customer</a>'; }
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        allowClear: true,
        ajax: {
            url: _base_url + "call-management/customers_search",
            type: "post",
            dataType: 'json',
            delay: 150,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (response) {
                if (response.id === "") {

                } else {
                    return { results: response };
                }
            },
            cache: true
        },
    });

    validator = $('#customer-popup').validate({
        ignore: [],
        rules: {
            customer_name: {
                required: true,
            },
            customer_email: {
                required: true,
                remote: {
                    url: _base_url + "call-management/validateemail",
                    type: "post",
                    data: 
                    {
                        email: function(){ return $("#customer_email").val(); }
                    }
                }
            },
            customer_mobilenumber: {
                required: true,
                remote: {
                    url: _base_url + "call-management/validatemobilenumber",
                    type: "post",
                    data: 
                    {
                        phone: function(){ return $("#customer_mobilenumber").val(); }
                    }
                }
            },
            customer_residence_type: {
                required: true,
            },
            customer_address: {
                required: true,
            },
        },
        messages: {
            customer_name: "Please enter customer.",
            customer_email: {
                required: "Please enter email",
                remote: "Email already exist",
            },
            customer_mobilenumber: {
                required :"Please enter mobile number",
                remote: "Mobile number already exist"

            },
            customer_residence_type: "Select residence.",
            customer_address: "Please enter address.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("id") == "customer_residence_type") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            $('#customer-save-btn').html('Saving...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/customersave",
                data: $('#customer-popup').serialize(),
                success: function (data) {
                    if (data.status == 'success') {
                        toast('success', data.message);
                        close_fancybox();
                        window.location.href = _base_url + "call-management";
                    }
                    else {
                        $('#form-error').html(data.message);
                        $('#customer-save-btn').html('Saving...').attr("disabled", true);
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    alert("An error occured!");
                    $('#customer-save-btn').html('Saving...').attr("disabled", true);
                    $('.mm-loader').hide();
                },
            });
        }
    });
    call_management_booking_form_validator = $('#call_management_booking_form').validate({
        ignore: [],
        rules: {
            booking_type: {
                required: true,
            },
            booking_date: {
                required: true,
            },
            // booking_maid_id: {
            //     required: true,
            // }, 
            // booking_zone_id: {
            //     required: true,
            // },
            
        },
        messages: {
            "booking_type": "select booking type",
            "booking_date": "Please enter date",
            // "booking_maid_id": "select maid",
            "booking_zone_id": "Select Zone",  
            
        },
        errorPlacement: function (error, element) {
             if (element.attr("name") == "booking_zone_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "booking_maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "booking_type") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        }
    });

    call_management_enquiry_form_validator = $('#call_management_enquiry_form').validate({
        ignore: [],
        rules: {
            // date_from: {
            //     required: true,
            // },
            booking_type_enquiry:{
                required: true,
            },
            enquiry_booking_date:{
                required: true,
            },
            // enquiry_zone_id: {
            //     required: function(element) {
            //         return $('select[name="booking_type_enquiry"]').val() == "OD";
            //     }
            // },
            // date_to: {
            //     required: true,
            // },
            enquiry_time_from: {
                required: function(element) {
                    return $('select[name="booking_type_enquiry"]').val() == "OD";
                }
                // required: $('select[name="area_id"]').val() == "OD",
            }, 
            enquiry_time_to: {
                required: function(element) {
                    return $('select[name="booking_type_enquiry"]').val() == "OD";
                }
            },
            service_id: {
                required: true,
            },
            area_id: {
                required: function(element) {
                    return !$('#outside_service_check').is(':checked');
                }
            },
            location_id: {
                required: function(element) {
                    return !$('#outside_service_check').is(':checked');
                }
            },
            outside_new_area: { 
                required: function(element) {
                    return $('#outside_service_check').is(':checked') || $('select[name="area_id"]').val() === '';
                }
            },
        },
        messages: {
            // "date_from": "Please enter date",
            // "date_to": "Please enter date",
            "enquiry_time_from": "Please enter time",
            "enquiry_time_to": "Please enter time",
            "service_id": "Select service",
            "enquiry_zone_id": "Select zone",
            "booking_type_enquiry": "Select booking type",
            "enquiry_booking_date": "Select date",
            "area_id": "Select area",
            "location_id": "Select location",
            "outside_new_area": "Please enter new area & address",
            
            
        },
        errorPlacement: function (error, element) {
             if (element.attr("name") == "enquiry_time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "enquiry_time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "area_id") {
                error.insertAfter(element.parent());
            } 
            else if (element.attr("name") == "service_id") {
                error.insertAfter(element.parent());
            } 
            else if (element.attr("name") == "location_id") {
                error.insertAfter(element.parent());
            } 
            else {
                error.insertAfter(element);
            }
        }
    });

    call_management_complaints_form_validator = $('#call_management_complaints_form').validate({
        ignore: [],
        rules: {
            complaint_date: {
                required: true,
            },
            // complaint_time: {
            //     required: true,
            // }, 
            complaint_type: {
                required: true,
            },
            complaint_status: {
                required: true,
            },

        },
        messages: {
            "complaint_date": "Please enter date",
            // "complaint_time": "Please enter time",
            "complaint_type": "Select type",
            "complaint_status": "Select status",
            
        },
        errorPlacement: function (error, element) {
             if (element.attr("name") == "complaint_date") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "complaint_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "complaint_status") {
                error.insertAfter(element.parent());
            } 
            // else if (element.attr("name") == "complaint_time") {
            //     error.insertAfter(element.parent());
            // } 
            else {
                error.insertAfter(element);
            }
        }
    });

    call_management_preferences_form_validator = $('#call_management_preferences_form').validate({
        ignore: [],
        rules: {
            preference_booking_type: {
                required: true,
            },
            
            preference_date_from: {
                required: true,
            },
            preference_date_to: {
                required: true,
            },
            preference_time_from: {
                required: true,
            }, 
            preference_time_to: {
                required: true,
            },
            preference_service_id: {
                required: true,
            },
            preference_area_id: {
                required: true,
            },
            preference_location: {
                required: true,
            },
            
        },
        messages: {
            "preference_booking_type": "Select booking type",
            "preference_date_from": "Please enter date",
            "preference_date_to": "Please enter date",
            "preference_time_from": "Please enter time",
            "preference_time_to": "Please enter time",
            "preference_service_id": "Select service",
            "preference_area_id": "Select area",
            "preference_location": "Select location",
            
            
        },
        errorPlacement: function (error, element) {
             if (element.attr("name") == "preference_date_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "preference_date_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "preference_area_id") {
                error.insertAfter(element.parent());
            } 
            else if (element.attr("name") == "preference_service_id") {
                error.insertAfter(element.parent());
            } 
            else if (element.attr("name") == "preference_location") {
                error.insertAfter(element.parent());
            } 
            else {
                error.insertAfter(element);
            }
        }
    });

    call_management_suspend_service_form_validator = $('#call_management_suspend_service_form').validate({
        ignore: [],
        rules: {
            suspend_date_from: {
                required: true,
            },
            suspend_date_to: {
                required: true,
            },
            
        },
        messages: {
            "suspend_date_from": "Please enter date",
            "suspend_date_to": "Please enter date",
        },
        errorPlacement: function (error, element) {
            
            // else {
                error.insertAfter(element);
           // }
        }
    });

    call_management_re_activations_form_valdatior = $('#call_management_re_activations_form').validate({
        ignore: [],
        rules: {
            re_actvation_date_from: {
                required: true,
            },
            re_activation_date_from: {
                required: true,
            },
            
        },
        messages: {
            "re_actvation_date_from": "Please enter date",
            "re_activation_date_from": "Please enter date",
        },
        errorPlacement: function (error, element) {
            
            // else {
                error.insertAfter(element);
           // }
        }
    });

    call_management_feedback_form_validator = $('#call_management_feedback_form').validate({
        ignore: [],
        rules: {
            feedback_date: {
                required: true,
            },
            feedback_maid_id: {
                required: true,
            },
            
        },
        messages: {
            "feedback_date": "Please enter date",
            "feedback_maid_id": "Please select maid",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "feedback_maid_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
           }
        }
    });

    validator = $('#delete_day_schedule').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id = $('#booking_ids').val();
            // alert(booking_id)
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/schedule_delete",
                data: $('#delete_day_schedule').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // loader(true);
                        // window.location.href = _base_url + "call-management";
                        // $("#suspend_booking_id_"+booking_id).remove();
                        $("#delete_booking_id_"+booking_id).hide();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        $("#delete_day_schedule")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#delete_day_schedule")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#delete-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    create_schedule_booking_form_validator = $('#create_schedule_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#create_schedule_booking_form input[name="booking_id"]').val() == "";
                },
            },
            // service_week_day: {
            //     required: true,
            // },
            maid_id: {
                required: function () {
                    return $('#create_schedule_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            schedule_time_from: {
                required: true,
            },
            schedule_time_to: {
                required: true,
            },
            // service_start_date: {
            //     required: true,
            // },
            // service_end_date: {
            //     required: true,
            // },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            // service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            schedule_time_to: "Select service end time.",
            // service_start_date: "Select service start date.",
            // service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "schedule_time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "schedule_time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#create_schedule_booking_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/save_schedule",
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        $("#create_schedule_booking_form")[0].reset();
                        // window.location.href = _base_url + "call-management";
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });

    create_enquiry_schedule_booking_form_validator = $('#create_enquiry_schedule_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#create_enquiry_schedule_booking_form input[name="booking_id"]').val() == "";
                },
            },
            // service_week_day: {
            //     required: true,
            // },
            maid_id: {
                required: function () {
                    return $('#create_enquiry_schedule_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            schedule_enquiry_time_from: {
                required: true,
            },
            schedule_enquiry_time_to: {
                required: true,
            },
            // service_start_date: {
            //     required: true,
            // },
            // service_end_date: {
            //     required: true,
            // },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            // service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            schedule_enquiry_time_from: "Select service start time.",
            schedule_enquiry_time_to: "Select service end time.",
            // service_start_date: "Select service start date.",
            // service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "schedule_enquiry_time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "schedule_enquiry_time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#create_enquiry_schedule_booking_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/save_schedule_enquiry",
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        $("#create_enquiry_schedule_booking_form")[0].reset();
                        // window.location.href = _base_url + "call-management";
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
    suspend_one_day_schedule_od_validator = $('#suspend_one_day_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#suspend_one_day_schedule_od input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/suspend_one_day_schedule_od",
                data: $('#suspend_one_day_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).show();
                        $("#suspend_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_permanently_booking_id_"+booking_id).show();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        $("#suspend_one_day_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-one-day-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_one_day_schedule_we_validator = $('#suspend_one_day_schedule_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#suspend_one_day_schedule_we input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/suspend_one_day_schedule_we",
                data: $('#suspend_one_day_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).show();
                        $("#suspend_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_permanently_booking_id_"+booking_id).show();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        $("#suspend_one_day_schedule_we")[0].reset() 
                       
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-one-day-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_one_day_schedule_od_validator = $('#cancel_one_day_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#cancel_one_day_schedule_od input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/cancel_one_day_schedule_od",
                data: $('#cancel_one_day_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).show();
                        // $("#suspend_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_permanently_booking_id_"+booking_id).show();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        $("#cancel_one_day_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-one-day-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });


    cancel_one_day_schedule_we_validator = $('#cancel_one_day_schedule_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#cancel_one_day_schedule_we input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/cancel_one_day_schedule_we",
                data: $('#cancel_one_day_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).show();
                        // $("#suspend_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_permanently_booking_id_"+booking_id).show();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        $("#cancel_one_day_schedule_we")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_one_day_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-one-day-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
    suspend_date_range_schedule_od_validator = $('#suspend_date_range_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            var booking_id =  $('#suspend_date_range_schedule_od input[name="booking_id"]').val();
            // alert(booking_id);
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/suspend_date_range_schedule_od",
                data: $('#suspend_date_range_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).hide();
                        // $("#suspend_one_day_booking_id_"+booking_id).show();
                        $("#cancel_one_day_booking_id_"+booking_id).show();
                        $("#cancel_permanently_booking_id_"+booking_id).show();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        // $("#delete_day_schedule")[0].reset() 
                        $("#suspend_date_range_schedule_od")[0].reset() 

                        // setTimeout(function () {
                        //     window.location.href = _base_url + "schedule/day_view_list";
                        // }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_date_range_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-date-range-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_date_range_schedule_we_validator = $('#suspend_date_range_schedule_we').validate({
        ignore: [],
        rules: {
            delete_date_from: {
                required: true,
            },
            delete_date_to: {
                required: true,
            },
            remark: {
                required: true,
            },
           
        },
        messages: {
            delete_date_from: "Please enter date.",
            delete_date_to: "Please enter date.",
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#suspend_date_range_schedule_we input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/suspend_date_range_schedule_we",
                data: $('#suspend_date_range_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).hide();
                        // $("#suspend_one_day_booking_id_"+booking_id).show();
                        $("#cancel_one_day_booking_id_"+booking_id).show();
                        $("#cancel_permanently_booking_id_"+booking_id).show();
                        $("#reactivate_booking_id_"+booking_id).show();
                        var call_history_id = data.call_history_id;
                        $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                        // $("#delete_day_schedule")[0].reset() 
                        $("#suspend_date_range_schedule_we")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_date_range_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-date-range-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_permeantly_od_validator = $('#cancel_permeantly_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#cancel_permeantly_od input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/cancel_permeantly_od",
                data: $('#cancel_permeantly_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).hide();
                        // $("#suspend_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_permanently_booking_id_"+booking_id).hide();
                        $("#reactivate_booking_id_"+booking_id).hide();
                        var call_history_id = data.call_history_id;
                        $("#cancel_permeantly_od")[0].reset() 
                       
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_permeantly_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-permeantly-od-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_permeantly_we_validator = $('#cancel_permeantly_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var booking_id =  $('#cancel_permeantly_we input[name="booking_id"]').val();
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "call-management/cancel_permeantly_we",
                data: $('#cancel_permeantly_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_booking_id_"+booking_id).hide();
                        $("#suspend_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_one_day_booking_id_"+booking_id).hide();
                        $("#cancel_permanently_booking_id_"+booking_id).hide();
                        $("#reactivate_booking_id_"+booking_id).hide();
                        var call_history_id = data.call_history_id;
                        $("#cancel_permeantly_we")[0].reset() 
                       
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_permeantly_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-permeantly-we-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
});

function add_customer()
{
    var typed_cont = $(".select2-search__field").val();
    $('#customer-popup #customer_name').val(typed_cont);
    fancybox_show('customer-add-popup', {});
}

$("#customer_area_id").on('change', function(){
    var area_id = $(this).val();
    $.ajax({
       url: _base_url + 'call-management/getlocation',
       type: 'POST',
       data: {area_id: area_id},
       dataType: 'json',
       success: function(response) {
          // Clear the previous options
          $('#customer_location').empty();
          // Add the new options based on the response
          $.each(response, function(index, location) {
            
             $('#customer_location').append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
             
          });
       }
    });
})

$("#customer_location").on('change', function(){
    var location_id = $(this).val();
    $.ajax({
       url: _base_url + 'call-management/getlandmark',
       type: 'POST',
       data: {location_id: location_id},
       dataType: 'json',
       success: function(response) {
          // Clear the previous options
          $('#customer_landmark').empty();
          // Add the new options based on the response
          $.each(response, function(index, landmark) {
            
            $('#customer_landmark').append('<option value="' + landmark.landmark_id + '">' + landmark.landmark_name + '</option>');
             
          });
       }
    });
})

$("#preference_area_id").on('change', function(){
    var area_id = $(this).val();
    $.ajax({
       url: _base_url + 'call-management/getlocation',
       type: 'POST',
       data: {area_id: area_id},
       dataType: 'json',
       success: function(response) {
          // Clear the previous options
          $('#preference_location').empty();
          

          // Add the new options based on the response
          $.each(response, function(index, location) {
            
             $('#preference_location').append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
             
          });
       }
    });
 })

$("#customer_id").on('change', function()
{
  $('#view_customer').show();
   $('#customername').empty();
   $('#customer-mobile').empty();
   $('#customer-status').empty();
   $('#customer-type').empty();
   $('#customer-email').empty();
   $('#customer-address').empty();
   $('#customer-area-zone').empty();
   $('#customer-location').empty();
   $('#customer-sublocation').empty();
   $('#enquiry_customer_name').val('');
   $('#enquiry_customer_phoneno').val('');
   $('#enquiry_customer_email').val('');
   $('#enquiry_customer_id').val('');
   $('#complainer_phone').val('');
   $('#complainer_name').val('');
   $('#complainer_email').val('');
   $('#complainer_id').val('');
   $('#customer_preference_id').val('');
   $('#customer_preference_phone').val('');
   $('#booking_customer_phoneno').val('');
   $('#booking_customer_name').val('');
   $('#booking_customer_id').val('');
   $('#booking_customer_ids').val('');
   $('#suspend_customer_id').val('');
   $('#suspend_customer_name').val('');
   $('#suspend_customer_phoneno').val('');
   $('#suspend_customer_email').val('');
   $('#re_actvation_customer_phoneno').val('');
   $('#re_actvation_customer_id').val('');
   $('#re_actvation_customer_name').val('');
   $('#re_actvation_customer_email').val('');
   $('#re_activate_customer_id').val('');
   $('#re_activate_customer_phoneno').val('');
   $('#customer-picked-address-u').html('');
   $('#feedback_customer_id').val('');
   $('#feedback_customer_phone').val('');
   $('#booking_customer_address_id').val('');
   $('#enquiry_customer_address_id').val('');
    var cust_id = $(this).val();
     $('#customer_view').attr('href',_base_url + 'customer/view/'+cust_id);
    $.ajax({
        url: _base_url + 'call-management/get-customer-details',
        type: 'POST',
        data: {cust_id: cust_id},
        dataType: 'json',
        success: function(response) {
        //    console.log(response);
            if(response.customer_details != "") {
            $('#enquiry_customer_id').val(response.customer_details[0].customer_id);
            $('#re_activate_customer_id').val(response.customer_details[0].customer_id);
            $('#feedback_customer_id').val(response.customer_details[0].customer_id);
            $('#suspend_customer_id').val(response.customer_details[0].customer_id);
            $('#re_actvation_customer_id').val(response.customer_details[0].customer_id);
            $('#re_actvation_customer_phoneno').val(response.customer_details[0].mobile_number_1);
            $('#feedback_customer_phone').val(response.customer_details[0].mobile_number_1);
            $('#re_activate_customer_phoneno').val(response.customer_details[0].mobile_number_1);
            $('#suspend_customer_phoneno').val(response.customer_details[0].mobile_number_1);
            $('#booking_customer_phoneno').val(response.customer_details[0].mobile_number_1);
            $('#booking_customer_id').val(response.customer_details[0].customer_id);
            $('#booking_customer_name').val(response.customer_details[0].customer_name);
            $('#booking_customer_ids').val(response.customer_details[0].customer_id);
            $('#complainer_id').val(response.customer_details[0].customer_id);
            $('#customer_preference_id').val(response.customer_details[0].customer_id);
            $('#customername').append(response.customer_details[0].customer_name);
            $('#customer-mobile').append(response.customer_details[0].mobile_number_1);
            $('#customer_preference_phone').val(response.customer_details[0].mobile_number_1);
            if (response.customer_details[0].customer_status == "0")
            {
                var customer_status = "Inactive";
                // $('#customer-status').append(customer_status);
                
            } else {
                var customer_status = "Active";
                // $('#customer-status').append(customer_status);

            }
             
            if (response.customer_details[0].customer_booktype == "0")
            {
                var customer_type = "One-off";
            } else {
                var customer_type = "Regular";

            }

            $('#customer-status').append(customer_status);
            $('#customer-type').append(customer_type);
            
            $('#customer-email').append(response.customer_details[0].email_address);
            $('#enquiry_customer_name').val(response.customer_details[0].customer_name);
            $('#re_actvation_customer_name').val(response.customer_details[0].customer_name);
            $('#suspend_customer_name').val(response.customer_details[0].customer_name);
            $('#enquiry_customer_phoneno').val(response.customer_details[0].mobile_number_1);
            $('#suspend_customer_email').val(response.customer_details[0].email_address);
            $('#re_actvation_customer_email').val(response.customer_details[0].email_address);
            $('#enquiry_customer_email').val(response.customer_details[0].email_address);
            $('#complainer_phone').val(response.customer_details[0].mobile_number_1);
            $('#complainer_name').val(response.customer_details[0].customer_name);
            $('#complainer_email').val(response.customer_details[0].email_address);
            }
             
            if (response.customer_address != "") {
                $('#customer-address').append(response.customer_address[0]['customer_address']);
                $('#customer-area-zone').append(response.customer_address[0]['zone_name']);
                $('#customer-location').append(response.customer_address[0]['location_name']);
                $('#customer-sublocation').append(response.customer_address[0]['landmark_name']);
                $('#booking_customer_address_id').val(response.customer_address[0]['customer_address_id']);
                $('#enquiry_customer_address_id').val(response.customer_address[0]['customer_address_id']);

                if(response.no_of_address == 1)
                {
                    $.each(response.customer_address, function(key, val)
                    {
                        var address_id = val.customer_address_id;
                        $('#customer-address-id-user').val(address_id);
    
                        var addzone = '<strong>' + val.zone_name + ' - ' + val.area_name + '</strong>';
                        var address = val.customer_address;
                                             
                        $('#customer-address-panel-user').slideUp(function() 
                        {
                            $('#b-customer-area-cell').text(val.area_name);
                            $('#b-customer-zone-cell').text(val.zone_name);
                            $('#b-customer-address-cell').text(address);
                            $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');
                            // var booking_id = $('#booking-id-user').val();
                            
                        });
                    });
                }
                else
                {
                    open_address_panel(cust_id)
                }   
            }
             
        }
    });

    if (cust_id != "")
    {
        $.ajax({
            type: 'POST',
            dataType: "text",
            url: _base_url + "call-management/call_details",
            // data: $('#call_management_suspend_service_form').serialize(),
            data: {cust_id:cust_id},
            // contentType: false,
            // processData: false,
            success: function (result) {
                $('#call_details').show();
                $('.mm-loader').hide();
              
                $('#call_details').html(result);
               

            },
            error: function (data) {
                // $('#suspend-service-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }

    // $('#customer-picked-address-u').html('');
    // get_no_of_address(cust_id);
})
function open_address_panel(customer_id)
{
    $('#customer-address-id-user').val('');
    $('#customer-address-panel-user').hide();
    $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');
	
    if($.isNumeric(customer_id) && customer_id > 0)
    {	
        $('#customer-address-panel-user').slideDown();
        $.ajax({
            url: _base_url + 'call-management/get-customer-address-details',
            type: 'POST',
            data: {customer_id: customer_id},
            dataType: 'json',
            success: function(response) {
                // console.log(response)
                // $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response)
                // {
                if(response == 'refresh')
                {
                    $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                    });
                }
                else
                {
                    var adressdata = response.customer_address;			
                    // console.log(adressdata)			
                    var address_html = '<div class="table">';                                
                                    
                    $.each(adressdata, function(key, val) {
                        address_html += '<div class="row"><div class="cell1"><input type="hidden" id="address_location" value="'+ val.location_name +'"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span><span style="display:none;"id="caddlocation-' + val.customer_address_id + '">' + val.location_name + '</span><br /><span style="display:none;"id="caddlandmark-' + val.customer_address_id + '">' + val.landmark_name + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                        
                    });				

                    address_html += '</div>';

                    $('#customer-address-panel-user .inner').html(address_html);
                }
            }
        });
    }
}
// function get_no_of_address(customer_id)
// {

// }
$('body').on('click', '#customer-address-panel-user .close', function() {
	$('#customer-address-panel-user').slideUp( function() { $('#customer-picked-address').show(); } );
});
$('body').on('click', '.pick_customer_address', function() {
	var address_id = $(this).attr('id').replace('cadddress-', '');
	$('#customer-address-id-user').val(address_id);
	var addzone = $('#caddzone-' + address_id).html();
	var addlocation = $('#caddlocation-' + address_id).html();
	var addlandmark = $('#caddlandmark-' + address_id).html();
    // console.log(addlandmark)
	var address = $('#cadd-' + address_id).html();
    $('#customer-address').empty();
    $('#customer-location').empty();
    $('#customer-area-zone').empty();
    $('#customer-sublocation').empty();
    $('#booking_customer_address_id').val('');
    $('#enquiry_customer_address_id').val('');
    
	$('#customer-address-panel-user').slideUp(function() {
            $('#customer-address').append(address);
            $('#customer-area-zone').append(addzone);
            $('#customer-location').append(addlocation);
            $('#customer-sublocation').append(addlandmark);
            $('#booking_customer_address_id').val(address_id);
            $('#enquiry_customer_address_id').val(address_id);
            // $('#customer-location').append(response.customer_address[0]['location_name']);
            // $('#customer-sublocation').append(response.customer_address[0]['landmark_name']);
            $('#customer-picked-address-u').html('<div class="address"><strong>' + addzone + '</strong> - ' + address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
            $('#customer-picked-address-u').hide();
            $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');

            var booking_id = $('#booking-id-user').val();
	});
});
//caller type id
$("#called_for").on('change', function(){
    var caller_typ_id = $(this).val();
   $('#caller_enquiry_typ_id').val(caller_typ_id);
   $('#caller_complaints_typ_id').val(caller_typ_id);
   $('#caller_re_activations_typ_id').val(caller_typ_id);
   $('#caller_cancel_service_typ_id').val(caller_typ_id);
   $('#caller_suspend_service_typ_id').val(caller_typ_id);
   $('#caller_other_typ_id').val(caller_typ_id);
   $('#caller_preferences_typ_id').val(caller_typ_id);
   $('#caller_booking_typ_id').val(caller_typ_id);
   $('#caller_feedback_typ_id').val(caller_typ_id);
   
   if (caller_typ_id == "1")
   {
        var tab_id = "booking";
        $('li[data-tab="'+tab_id+'"]').addClass('active');
        $('li[data-tab="preferences"]').removeClass('active');
        $('li[data-tab="enquiry"]').removeClass('active');
        $('li[data-tab="complaints"]').removeClass('active');
        $('li[data-tab="suspend-service"]').removeClass('active');
        $('li[data-tab="re-activations"]').removeClass('active');
        $('li[data-tab="feedback"]').removeClass('active');
        $('.tab-panel').removeClass('active').css('display', 'none');
        $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');

    } else if (caller_typ_id == "2")
    {
        var tab_id = "enquiry";
        $('li[data-tab="'+tab_id+'"]').addClass('active');
        $('li[data-tab="booking"]').removeClass('active');
        $('li[data-tab="complaints"]').removeClass('active');
        $('li[data-tab="suspend-service"]').removeClass('active');
        $('li[data-tab="re-activations"]').removeClass('active');
        $('li[data-tab="preferences"]').removeClass('active');
        $('li[data-tab="feedback"]').removeClass('active');
        $('.tab-panel').removeClass('active').css('display', 'none');
        $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
        
    } else if(caller_typ_id == "3")
    {
        var tab_id = "complaints";
        $('li[data-tab="'+tab_id+'"]').addClass('active');
        $('li[data-tab="booking"]').removeClass('active');
        $('li[data-tab="enquiry"]').removeClass('active');
        $('li[data-tab="suspend-service"]').removeClass('active');
        $('li[data-tab="re-activations"]').removeClass('active');
        $('li[data-tab="preferences"]').removeClass('active');
        $('li[data-tab="feedback"]').removeClass('active');
        $('.tab-panel').removeClass('active').css('display', 'none');
        $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
    } else if(caller_typ_id == "4")
    {
        var tab_id = "suspend-service";
        $('li[data-tab="'+tab_id+'"]').addClass('active');
        $('li[data-tab="booking"]').removeClass('active');
        $('li[data-tab="enquiry"]').removeClass('active');
        $('li[data-tab="complaints"]').removeClass('active');
        $('li[data-tab="preferences"]').removeClass('active');
        $('li[data-tab="re-activations"]').removeClass('active');
        $('li[data-tab="feedback"]').removeClass('active');
        $('.tab-panel').removeClass('active').css('display', 'none');
        $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
    } else if(caller_typ_id == "5")
    {
        var tab_id = "re-activations";
        $('li[data-tab="'+tab_id+'"]').addClass('active');
        $('li[data-tab="booking"]').removeClass('active');
        $('li[data-tab="enquiry"]').removeClass('active');
        $('li[data-tab="complaints"]').removeClass('active');
        $('li[data-tab="suspend-service"]').removeClass('active');
        $('li[data-tab="preferences"]').removeClass('active');
        $('li[data-tab="feedback"]').removeClass('active');
        $('.tab-panel').removeClass('active').css('display', 'none');
        $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
    }

    // else if(caller_typ_id == "6")
    // {
    //     var tab_id = "preferences";
    //     $('li[data-tab="'+tab_id+'"]').addClass('active');
    //     $('li[data-tab="booking"]').removeClass('active');
    //     $('li[data-tab="enquiry"]').removeClass('active');
    //     $('li[data-tab="complaints"]').removeClass('active');
    //     $('li[data-tab="suspend-service"]').removeClass('active');
    //     $('li[data-tab="re-activations"]').removeClass('active');
    //     $('li[data-tab="feedback"]').removeClass('active');
    //     $('.tab-panel').removeClass('active').css('display', 'none');
    //     $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
    // }
 
    else if(caller_typ_id == "9")
    {
        var tab_id = "feedback";
        $('li[data-tab="'+tab_id+'"]').addClass('active');
        $('li[data-tab="booking"]').removeClass('active');
        $('li[data-tab="enquiry"]').removeClass('active');
        $('li[data-tab="complaints"]').removeClass('active');
        $('li[data-tab="suspend-service"]').removeClass('active');
        $('li[data-tab="re-activations"]').removeClass('active');
        $('.tab-panel').removeClass('active').css('display', 'none');
        $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
    }
});
$('#call_management_enquiry_form #enquiry-save-btn').click(function () {
    call_management_enquiry_form_validator.resetForm();
    if ($("#call_management_enquiry_form").valid()) {
        $('.mm-loader').show();
        // $('#enquiry-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_enquiry_form"));
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "call-management/save_enquiry",
            data: $('#call_management_enquiry_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status =="success") {
                    // $('#enquiry-save-btn').html('Saved');
                    toast('success', data.message);
                    $('#call_management_enquiry_form')[0].reset();
                    $("#call_management_enquiry_form select[name='booking_type_enquiry']").val("").trigger("change");
                    $("#call_management_enquiry_form select[name='maidId']").val("").trigger("change");
                    $("#call_management_enquiry_form select[name='service_id']").val('6').trigger("change");
                    $("#call_management_enquiry_form select[name='area_id']").val("").trigger("change");
                    $("#call_management_enquiry_form select[name='location_id']").val("").trigger("change");
                    $("#call_management_enquiry_form input[name='enquiry_booking_date']").val("");

                    // setTimeout(function () {
                    //     window.location.href = _base_url + "call-management";
                    // }, 3000);
                }
                else {
                    $('#enquiry-save-btn').html('Save').removeAttr("disabled");
                    toast('error', data.message);
                }
            },
            error: function (data) {
                $('#enquiry-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

$('#call_management_enquiry_form #enquiry-search-btn').click(function () {
    call_management_enquiry_form_validator.resetForm();
    if ($("#call_management_enquiry_form").valid()) {
        $('.mm-loader').show();
        // $('#enquiry-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_enquiry_form"));
        $.ajax({
            type: 'POST',
            dataType: "text",
            url: _base_url + "call-management/search_maid_availability_enquiry",
            data: $('#call_management_enquiry_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (result) {
                $('#maid_enquiry_search').show();
                $("#booking_customer_details").hide();
                $("#booking_customer_re_active_details").hide();
                $('#maid_enquiry_search').html(result);
                $('#maid_enquiry_search').addClass('widget-content');
                
                 $('.mm-loader').hide();
            },

            error: function (data) {
                $('#enquiry-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});


$("#employee_type_id").on('change', function(){
    var employee_type_id = $(this).val();
    $('#complaint_against_name').val('')
    $.ajax({
       url: _base_url + 'call-management/getemployeedetails',
       type: 'POST',
       data: {employee_type_id: employee_type_id},
       dataType: 'json',
       success: function(response) {
          // Clear the previous options
          $('#employee_name').empty();
          // Add the new options based on the response
            if(employee_type_id =='U') {
                $.each(response, function(index1, employee) {
                
                    $('#employee_name').append('<option value="' + employee.user_id + '">' + employee.user_fullname + '</option>');
                    $('#complaint_against_name').val(employee.user_fullname)
                    
                });
            } else {
                $.each(response, function(index, employee) {
                
                    $('#employee_name').append('<option value="' + employee.maid_id + '">' + employee.maid_name + '</option>');
                    $('#complaint_against_name').val(employee.maid_name)
                    
                });
            }  
        }
    });
});

(function(a) {
    a(document).ready(function(b) {
      if (a('#calls-list-table').length > 0) {
        a("table#calls-list-table").dataTable({
          'sPaginationType': "full_numbers",
          "bSort": true,
          "iDisplayLength": 100,
          "scrollY": true,
          "orderMulti": false,
          "scrollX": true,
          'columnDefs': [{
            'targets': [1, 4, 5],
            'orderable': false
          }, ]
        });
      }
    });
})(jQuery);

$('#call_management_complaints_form #complaint-save-btn').click(function () {
    call_management_complaints_form_validator.resetForm();
    if ($("#call_management_complaints_form").valid()) {
        $('.mm-loader').show();
        // $('#complaint-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_complaints_form"));
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "call-management/save_complaint",
            data: $('#call_management_complaints_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status =="success") {
                    // $('#complaint-save-btn').html('Saved');
                    toast('success', data.message);
                    // setTimeout(function () {
                    //     window.location.href = _base_url + "call-management";
                    // }, 3000);
                    // $("#call_management_complaints_form")[0].reset() 
                    // $("#complaint_type").val("").trigger("change")
                    $("#call_management_complaints_form select[name='complaint_type']").val("").trigger("change");
                    $("#call_management_complaints_form select[name='existing_customer']").val("").trigger("change");
                    $("#call_management_complaints_form select[name='complaint_against']").val("").trigger("change");
                    $("#call_management_complaints_form select[name='employee_name']").val("").trigger("change");
                    $("#call_management_complaints_form select[name='complaint_status']").val("").trigger("change");
                    $("#call_management_complaints_form select[name='complaint_time']").val("").trigger("change");
                    $("#call_management_complaints_form select[name='complaint_time']").val("").trigger("change");
                    $("#call_management_complaints_form input[name='complaint_date']").val("");
                    $("#call_management_complaints_form textarea[name='complaint']").val("");
                    $("#call_management_complaints_form textarea[name='action_taken']").val("");
                    
                }
                else {
                    $('#complaint-save-btn').html('Save').removeAttr("disabled");
                    toast('error', data.message);
                }
            },
            error: function (data) {
                $('#complaint-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

$('#call_management_preferences_form #preferences-save-btn').click(function () {
    call_management_preferences_form_validator.resetForm();
    if ($("#call_management_preferences_form").valid()) {
        $('.mm-loader').show();
        // $('#preferences-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_preferences_form"));
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "call-management/save_preference",
            data: $('#call_management_preferences_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status =="success") {
                    // $('#preferences-save-btn').html('Saved');
                    toast('success', data.message);
                    // $("#call_management_preferences_form")[0].reset() 
                    $("#call_management_preferences_form select[name='preference_time_from']").val("").trigger("change");
                    $("#call_management_preferences_form select[name='preference_time_to']").val("").trigger("change");
                    $("#call_management_preferences_form select[name='preference_booking_type']").val("").trigger("change");
                    $("#call_management_preferences_form select[name='preference_maidId']").val("").trigger("change");
                    $("#call_management_preferences_form select[name='preference_service_id']").val(6).trigger("change");
                    $("#call_management_preferences_form select[name='preference_area_id']").val("").trigger("change");
                    $("#call_management_preferences_form select[name='preference_location']").val("").trigger("change");
                    $("#call_management_preferences_form input[name='preference_date_from']").val("");
                    $("#call_management_preferences_form input[name='preference_date_to']").val("");


                    // setTimeout(function () {
                    //     window.location.href = _base_url + "call-management";
                    // }, 3000);
                }
                else {
                    $('#preferences-save-btn').html('Save').removeAttr("disabled");
                    toast('error', data.message);
                }
            },
            error: function (data) {
                $('#preferences-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

$('#call_management_feedback_form #feedback-save-btn').click(function () {
    call_management_feedback_form_validator.resetForm();
    if ($("#call_management_feedback_form").valid()) {
        $('.mm-loader').show();
        // $('#feedback-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_feedback_form"));
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "call-management/save_feedback",
            data: $('#call_management_feedback_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status =="success") {
                    // $('#feedback-save-btn').html('Saved');
                    toast('success', data.message);
                    // setTimeout(function () {
                    //     window.location.href = _base_url + "call-management";
                    // }, 3000);
                    $("#call_management_feedback_form")[0].reset() 
                    $("#call_management_feedback_form select[name='feedback_maid_id']").val("").trigger("change");


                }
                else {
                    $('#feedback-save-btn').html('Save').removeAttr("disabled");
                    toast('error', data.message);
                }
            },
            error: function (data) {
                $('#feedback-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

$("#employee_name").on('change', function(){
    var _aganstName = $("#employee_name option:selected").text();
    $('#complaint_against_name').val(_aganstName);
      
});
$("#call_management_booking_form select[name='booking_zone_id']").change(function () {
    call_management_booking_form_validator.resetForm();
    if ($("#call_management_booking_form").valid()) {
        $('.mm-loader').show();
        // $('#booking-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_booking_form"));
        $.ajax({
            type: 'POST',
            dataType: "text",
            url: _base_url + "call-management/search_booking_maid",
            data: $('#call_management_booking_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (result) {
                $('#maid_search').show();
                $("#booking_customer_details").hide();
                $("#booking_customer_re_active_details").hide();
                $('#maid_search').html(result);
                $('#maid_search').addClass('widget-content');
                
                $('.mm-loader').hide();
              
            },
            error: function (data) {
                // $('#booking-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});
$("#call_management_booking_form select[name='booking_maid_id']").change(function () {
    call_management_booking_form_validator.resetForm();
    if ($("#call_management_booking_form").valid()) {
        $('.mm-loader').show();
        // $('#booking-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_booking_form"));
        $.ajax({
            type: 'POST',
            dataType: "text",
            url: _base_url + "call-management/search_booking_maid",
            data: $('#call_management_booking_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (result) {
                $('#maid_search').show();
                $("#booking_customer_details").hide();
                $("#booking_customer_re_active_details").hide();
                $('#maid_search').html(result);
                $('#maid_search').addClass('widget-content');
                
                $('.mm-loader').hide();
              
            },
            error: function (data) {
                $('#booking-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});
$('#call_management_booking_form #booking-save-btn').click(function () {
    call_management_booking_form_validator.resetForm();
    if ($("#call_management_booking_form").valid()) {
        $('.mm-loader').show();
        // $('#booking-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_booking_form"));
        $.ajax({
            type: 'POST',
            dataType: "text",
            url: _base_url + "call-management/search_booking_maid",
            data: $('#call_management_booking_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (result) {
                // console.log(result)
                $('#maid_search').show();
                $("#booking_customer_details").hide();
                $("#booking_customer_re_active_details").hide();
                $('#maid_search').html(result);
                $('#maid_search').addClass('widget-content');
                // $("#LoadingImage").hide();
                // // console.log(data);
                // $('#booking-availability').show();
                 $('.mm-loader').hide();
                // let maid_availability_html = '';
                // if (data.get_available_data.length === 0) {
                //     $('#booking-maid-availability').html('No schedules found on')
                //     // toast('info', "No schedules found on ");
                // } else {
                //     var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
                //     var count = 1;
                //     $.each(data.get_available_data, function (key, get_booking_data) {
                //         // var d = new Date();
                //         var week_name = weekday[get_booking_data.service_week_day];
                //             maid_availability_html += `<tr><td align="center" valign="middle" style="width:50px;">
                //                                             <p>`+ count +`</p>
                //                                         </td>
                //                                         <td valign="middle">
                //                                             <p>` + week_name + `
                //                                         </td>
                //                                         <td valign="middle">
                //                                             <p>` + get_booking_data.maid_name + `
                //                                         </td>
                                                
                //                                         <td align="center" valign="middle">
                //                                             <p>`+ get_booking_data.working_minutes / 60 +`</p>
                //                                         </td>
                //                                         <td align="center" valign="middle">
                //                                             <p><a href="javascript:void(0);" class="slot1 slot-sml" onclick="return create_schedule(`+ get_booking_data.customer_id+`)">Book Now</a></p>
                //                                         </td>
                //                                     </tr>`;
                //         count++;

                //     });
                //     $('#booking-maid-availability').html(maid_availability_html);

                // }
                // if (data.status =="success") {
                    // $('#booking-save-btn').html('Saved');
                    // toast('success', data.message);
                    // setTimeout(function () {
                    //     window.location.href = _base_url + "call-management";
                    // }, 3000);
                // }
                // else {
                //     $('#booking-save-btn').html('Save').removeAttr("disabled");
                //     toast('error', data.message);
                // }
            },
            error: function (data) {
                $('#booking-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

// $("#booking_type").on('change', function(){
//     var booking_type = $(this).val();
//     if (booking_type == "OD") {
//         $("input.week_day").attr("disabled", true);
//         $('.weekly_booking_time').hide();
//         $('.od_booking_time').show();
//     } else {
//         $("input.week_day").attr("disabled", false);
//         $('.weekly_booking_time').show();
//         $('.od_booking_time').hide();
//     }
//     // alert(booking_type)
// });

$('#create_schedule_booking_form select[name="customer_ids"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "get",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            return { results: response };
        },
        cache: true
    },
}).on("change", function (e) {
    $(this).valid();
});
$('#call_management_suspend_service_form #suspend-service-save-btn').click(function () {
    call_management_suspend_service_form_validator.resetForm();
    if ($("#call_management_suspend_service_form").valid()) {
        $('.mm-loader').show();
        // $('#suspend-service-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_suspend_service_form"));
        $.ajax({
            type: 'POST',
            dataType: "text",
            url: _base_url + "call-management/search_booking",
            data: $('#call_management_suspend_service_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (result) {
                $('#booking_customer_details').show();
                // $("#booking_customer_details").hide();
                $("#booking_customer_re_active_details").hide();
                $('.mm-loader').hide();
                // if (data.status =="success") {
                    // $('.mm-loader').hide();
                    // $("#booking_customer_details").hide();
                // $("#booking_customer_re_active_details").hide();
                $('#booking_customer_details').html(result);
                $('#booking_customer_details').addClass('widget-content');
                    // let booking_html = '';
                    // var count = 1;
                    // if (data.schedules.length === 0) {
                    //     $('#booking-customer').html('No schedules found on')
                    //     // toast('info', "No schedules found on ");
                    // } else {
                    //     var service_date = data.service_date;
                    //     // console.log(data.schedules);
                    //     $.each(data.schedules, function (key, booking) {
                    //         console.log(booking.key.booking_type)
                    //         // var d = new Date();
                    //         booking_html += `<tr><td align="center" valign="middle" style="width:50px;">
                    //                             <p>`+ count +`</p>
                    //                             </td>
                    //                             <td valign="middle">
                    //                                 <p>` + booking.customer_name + `
                    //                             </td>
                    //                             <td valign="middle">
                    //                             <p>` + booking.booking_id + `
                    //                             </td>
                    //                             <td valign="middle">
                    //                                 <p>` + booking.maid_name + `
                    //                             </td>
                    //                             <td valign="middle">
                    //                             <p>` + booking.booking_type + `
                    //                             </td>
                    //                             <td align="center" valign="middle">
                    //                             <p><span class="slot2">    
                    //                             `+ moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') +`       
                    //                             </span></p>
                    //                             </td>    
                    //                             <td align="center" valign="middle">
                    //                                 <p>`+ booking.working_minutes / 60 +`</p>
                    //                             </td>
                    //                             <td align="center" valign="middle">
                    //                             <p><a href="javascript:void(0);" class="slot1 slot-sml" onclick="return delete_schedule('`+ booking.booking_type+`', '`+ booking.booking_id+`', '`+ service_date +`', '`+ booking.day_service_status+`', '`+ booking.day_service_id +`')">Delete</a></p>
                    //                         </td>
                                                
                    //                         </tr>`;
                    //         count++;

                    //     });
                    //     $('#booking-customer').html(booking_html);
                    // }

            },
            error: function (data) {
                $('#suspend-service-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

$('#call_management_re_activations_form #re-activations-save-btn').click(function () {
    call_management_re_activations_form_valdatior.resetForm();
    if ($("#call_management_re_activations_form").valid()) {
        $('.mm-loader').show();
        // $('#suspend-service-save-btn').html('Saving...').attr("disabled", true);
        var formData = new FormData(document.getElementById("call_management_re_activations_form"));
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "call-management/re_activation_booking",
            data: $('#call_management_re_activations_form').serialize(),
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#booking_customer_re_active_details').show();
                $("#booking_customer_details").hide();
                // $("#booking_customer_re_active_details").hide();
                $('.mm-loader').hide();
                // if (data.status =="success") {
                    $('.mm-loader').hide();
                    let booking_html = '';
                    var count = 1;
                    if (data.schedules.length === 0) {
                        $('#booking_re_activation_customer').html('No schedules found on')
                        // toast('info', "No schedules found on ");
                    } else {
                        var service_date = data.service_date;
                        // console.log(service_date);
                        $.each(data.schedules, function (key, booking) {
                            // console.log(booking.booking_type)
                            // var d = new Date();
                            booking_html += `<tr  id="re_activation_booking_id_`+ booking.booking_id+ `"><td align="center" valign="middle" style="width:50px;">
                                                <p>`+ count +`</p>
                                                </td>
                                                <td valign="middle">
                                                    <p>` + booking.customer_name + `
                                                </td>
                                                <td valign="middle">
                                                <p>` + booking.booking_id + `
                                                </td>
                                                <td valign="middle">
                                                <p>` + booking.maid_name + `
                                                </td>
                                                <td valign="middle">
                                                    <p>` + booking.booking_type + `
                                                </td>
                                                <td valign="middle">
                                                <p>` + booking.delete_from_date + `
                                                </td>
                                                <td valign="middle">
                                                <p>` + booking.delete_to_date + `
                                                </td>
                                                <td align="center" valign="middle">
                                                <p><a href="javascript:void(0);" class="slot-sml" onclick="return re_activate_schedule('`+ booking.booking_type+`', '`+ booking.booking_id+`', '`+ booking.delete_from_date +`','`+ booking.booking_delete_id +`')"><strong>Reactivate</strong></a></p>
                                            </td>
                                                
                                            </tr>`;
                            count++;

                        });
                        $('#booking_re_activation_customer').html(booking_html);
                    }

            },
            error: function (data) {
                $('#re-activations-save-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            },
        });
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});

function delete_schedule(booking_type, booking_id, service_date, service_status, day_service_id)
{
    // alert(booking_id)
    if (booking_type == 'OD') { 
        fancybox_show('delete-popup', {});
        $('#day_service_id').val(day_service_id || "");
      
        $('#day_service_status').val(service_status || "");
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#booking_customer_id').val(customer_id);
        $('#customer_phoneno').val(mobile_number);
        $('#booking_ids').val(booking_id);
        $('#customer_service_date').val(service_date);
        $('#caller_service_typ_id').val(caller_suspend_service_typ_id);

    } else {
        fancybox_show('delete-regular-schedule-popup', {});
        $('#booking_id_perment').val(booking_id);
        $('#booking_id_day').val(booking_id);
    }
   
}

function schedule_suspend_date_range(booking_type,booking_id,day_service_id,service_date1,service_date,service_status){
    
    if (booking_type == 'OD') {
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#suspend_date_range_schedule_od input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#suspend_date_range_schedule_od input[name="booking_customer_id"]').val(customer_id);
        $('#suspend_date_range_schedule_od input[name="customer_phoneno"]').val(mobile_number);
        $('#suspend_date_range_schedule_od input[name="customer_service_date"]').val(service_date);
        $('#suspend_date_range_schedule_od input[name="booking_id"]').val(booking_id);
        $('#suspend_date_range_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#suspend_date_range_schedule_od input[name="service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#suspend_date_range_schedule_od input[name="delete_date_from"]').val(service_date);
        $('#suspend_date_range_schedule_od input[name="delete_date_to"]').val(service_date);
        $('#suspend_date_range_schedule_od input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-date-range-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
       
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#suspend_date_range_schedule_we input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#suspend_date_range_schedule_we input[name="booking_customer_id"]').val(customer_id);
        $('#suspend_date_range_schedule_we input[name="customer_phoneno"]').val(mobile_number);
        $('#suspend_date_range_schedule_we input[name="customer_service_date"]').val(service_date);
        $('#suspend_date_range_schedule_we input[name="booking_id"]').val(booking_id);
        $('#suspend_date_range_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#suspend_date_range_schedule_we input[name="service_date"]').val(service_date);
        $('#suspend_date_range_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-date-range-we-popup', { width: 450});

    }
}

$('#suspend_date_range_schedule_od .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

function schedule_suspend_one_day(booking_type,booking_id,day_service_id,service_date1,service_date,service_status) {
   
    if (booking_type == 'OD') {
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#suspend_one_day_schedule_od input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#suspend_one_day_schedule_od input[name="booking_customer_id"]').val(customer_id);
        $('#suspend_one_day_schedule_od input[name="customer_phoneno"]').val(mobile_number);
        $('#suspend_one_day_schedule_od input[name="customer_service_date"]').val(service_date);
        $('#suspend_one_day_schedule_od input[name="booking_id"]').val(booking_id);
        $('#suspend_one_day_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#suspend_one_day_schedule_od input[name="service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#suspend_one_day_schedule_od input[name="day_service_status"]').val(service_status);
      
        fancybox_show('suspend-one-day-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
       
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#suspend_one_day_schedule_we input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#suspend_one_day_schedule_we input[name="booking_customer_id"]').val(customer_id);
        $('#suspend_one_day_schedule_we input[name="customer_phoneno"]').val(mobile_number);
        $('#suspend_one_day_schedule_we input[name="customer_service_date"]').val(service_date);
        $('#suspend_one_day_schedule_we input[name="booking_id"]').val(booking_id);
        $('#suspend_one_day_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#suspend_one_day_schedule_we input[name="service_date"]').val(service_date);
        $('#suspend_one_day_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-one-day-we-popup', { width: 450});

    }
}

function schedule_cancel_one_day(booking_type,booking_id,day_service_id,service_date1,service_date,service_status) {
   
    if (booking_type == 'OD') {
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#cancel_one_day_schedule_od input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#cancel_one_day_schedule_od input[name="booking_customer_id"]').val(customer_id);
        $('#cancel_one_day_schedule_od input[name="customer_phoneno"]').val(mobile_number);
        $('#cancel_one_day_schedule_od input[name="customer_service_date"]').val(service_date);
        $('#cancel_one_day_schedule_od input[name="booking_id"]').val(booking_id);
        $('#cancel_one_day_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#cancel_one_day_schedule_od input[name="service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#cancel_one_day_schedule_od input[name="day_service_status"]').val(service_status);
      
        fancybox_show('cancel-one-day-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
       
        var customer_id = $('#suspend_customer_id').val();
        var mobile_number = $('#suspend_customer_phoneno').val();
        var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
        $('#cancel_one_day_schedule_we input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#cancel_one_day_schedule_we input[name="booking_customer_id"]').val(customer_id);
        $('#cancel_one_day_schedule_we input[name="customer_phoneno"]').val(mobile_number);
        $('#cancel_one_day_schedule_we input[name="customer_service_date"]').val(service_date);
        $('#cancel_one_day_schedule_we input[name="booking_id"]').val(booking_id);
        $('#cancel_one_day_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#cancel_one_day_schedule_we input[name="service_date"]').val(service_date);
        $('#cancel_one_day_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-one-day-we-popup', { width: 450});

    }
}

$('#cancel_one_day_schedule_we input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
});

$('#suspend_one_day_schedule_we input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
});

function cancel_permanently(booking_type,booking_id,day_service_id,service_date1,service_date,service_status) {
    var customer_id = $('#suspend_customer_id').val();
    var mobile_number = $('#suspend_customer_phoneno').val();
    var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
    if (booking_type == 'OD') {
        $('#cancel_permeantly_od input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#cancel_permeantly_od input[name="booking_customer_id"]').val(customer_id);
        $('#cancel_permeantly_od input[name="customer_phoneno"]').val(mobile_number);
        $('#cancel_permeantly_od input[name="customer_service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#cancel_permeantly_od input[name="booking_id"]').val(booking_id);
        $('#cancel_permeantly_od input[name="day_service_id"]').val(day_service_id);
        $('#cancel_permeantly_od input[name="service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#cancel_permeantly_od input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-permeantly-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {

        $('#cancel_permeantly_we input[name="caller_service_typ_id"]').val(caller_suspend_service_typ_id);
        $('#cancel_permeantly_we input[name="booking_customer_id"]').val(customer_id);
        $('#cancel_permeantly_we input[name="customer_phoneno"]').val(mobile_number);
        $('#cancel_permeantly_we input[name="customer_service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#cancel_permeantly_we input[name="booking_id"]').val(booking_id);
        $('#cancel_permeantly_we input[name="day_service_id"]').val(day_service_id);
        $('#cancel_permeantly_we input[name="service_date"]').val(moment(service_date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
        $('#cancel_permeantly_we input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-permeantly-we-popup', { width: 450});

    }
}


function re_activate_schedule(booking_type, booking_id, service_date, booking_delete_id){
        fancybox_show('re-activate-popup', {});
   
        $('#booking_id').val(booking_id);
        $('#service_date').val(service_date);
        var caller_re_activations_typ_id = $('#caller_re_activations_typ_id').val();
        $('#booking_delete_id').val(booking_delete_id);
        $('#caller_re_activate_typ_id').val(caller_re_activations_typ_id);

  
}

$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    $('.n-delete-set-left').addClass('de-select');

    $('.n-delete-set-right-cont').show(500);
    $('.n-delete-set-left-cont').hide(500);
});

$(".n-delete-set-left").click(function () {
    $('.n-delete-set-left').removeClass('de-select');
    $('.n-delete-set-right').addClass('de-select');

    $('.n-delete-set-left-cont').show(500);
    $('.n-delete-set-right-cont').hide(500);
});

$('.n-delete-set-left-cont .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#delete_from_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
});
$('#suspend_date_range_schedule_we .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#create_schedule_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
}).on('changeDate', function (e) {
    $('#create_schedule_booking_form input[name="service_start_date"]').valid();
    var booking_date = $(this).val();
    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    $('#create_schedule_booking_form input[name="service_week_name"]').val(days[dow]);
    $('#create_schedule_booking_form input[name="service_end_date"]').datepicker('update', null).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").add(7 * 3, 'days').format('DD/MM/YYYY'));
});
$('#create_schedule_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    $('#create_schedule_booking_form input[name="service_end_date"]').valid();
});

$('#create_enquiry_schedule_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
}).on('changeDate', function (e) {
    $('#create_enquiry_schedule_booking_form input[name="service_start_date"]').valid();
    var booking_date = $(this).val();
    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    $('#create_enquiry_schedule_booking_form input[name="service_week_name"]').val(days[dow]);
    $('#create_enquiry_schedule_booking_form input[name="service_end_date"]').datepicker('update', null).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").add(7 * 3, 'days').format('DD/MM/YYYY'));
});
$('#create_enquiry_schedule_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    $('#create_enquiry_schedule_booking_form input[name="service_end_date"]').valid();
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#booking_id_day').val();
    var delete_date_from = $('#delete_date_from').val();
    var delete_date_to = $('#delete_date_to').val();
    var customer_id = $('#suspend_customer_id').val();
    // var mobile_number = $('#suspend_customer_phoneno').val();
    var mobile_number = $('#suspend_customer_phoneno').val();
    var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();

    // alert(customer_id)
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');
    } else if (delete_date_from == "")
    {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');   
    } else if (delete_date_to == "") {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block'); 
    }else {
        $('#deleteremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'call-management/booking_delete_one_day',
			data: {remarks: remarks,booking_id: booking_id, delete_date_from: delete_date_from, delete_date_to: delete_date_to, customer_id:customer_id, mobile_number:mobile_number, caller_suspend_service_typ_id:caller_suspend_service_typ_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    toast('success', data.message);
                    close_fancybox();
                    // $("#suspend_booking_id_"+booking_id).remove();
                    // / window.location.href = _base_url + "call-management";
                    $("#delete_booking_id_"+booking_id).hide();
                   $("#reactivate_booking_id_"+booking_id).show();
                   var call_history_id = data.call_history_id;
                   $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                    
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    var booking_id = $('#booking_id_perment').val();
    var customer_id = $('#suspend_customer_id').val();
    // var mobile_number = $('#suspend_customer_phoneno').val();
    var mobile_number = $('#suspend_customer_phoneno').val();
    var caller_suspend_service_typ_id = $('#caller_suspend_service_typ_id').val();
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#delete_remark_perm").focus();
         $('#deleteremarks_book').css('display','block');
    } else {
        $('#deleteremarks_book').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'call-management/permeant_booking_delete_schedule',
			data: {remarks: remarks,booking_id: booking_id, customer_id:customer_id, mobile_number:mobile_number, caller_suspend_service_typ_id:caller_suspend_service_typ_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    toast('success', data.message);
                    close_fancybox();
                    // $("#suspend_booking_id_"+booking_id).remove();
                    $("#delete_booking_id_"+booking_id).hide();
                    $("#reactivate_booking_id_"+booking_id).show();
                    var call_history_id = data.call_history_id;
                    $('#reactivate_booking_id_'+booking_id).attr('onclick', "schedule_reactivate("+booking_id+", "+call_history_id+")");
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});


$("#re-activate-schedule-btn").click(function () {
  
    var booking_id = $('#booking_id').val();
    var service_date = $('#service_date').val();
    var booking_delete_id = $('#booking_delete_id').val();
    var customer_id = $('#re_activate_customer_id').val();
    // var mobile_number = $('#suspend_customer_phoneno').val();
    var mobile_number = $('#re_activate_customer_phoneno').val();
    var caller_re_activate_typ_id = $('#caller_re_activate_typ_id').val();
   
    $.ajax({
        type: "POST",
        url: _base_url + 'call-management/booking_re_activate',
        data: {booking_id: booking_id, service_date: service_date, booking_delete_id: booking_delete_id, customer_id:customer_id, mobile_number:mobile_number, caller_re_activate_typ_id:caller_re_activate_typ_id},
        cache: false,
        success: function (data)
        {
            if (data.status == "success") {
                toast('success', data.message);
                close_fancybox();
                $("#re_activation_booking_id_"+booking_id).remove();
               
                // window.location.href = _base_url + "call-management";
                
            }
            else {
                toast('error', data.message);
                loader(false);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
    
});


// service suspenion 
function schedule_reactivate(booking_id,call_history_id){
    fancybox_show('schedule_reactivate-popup', {});

    $('#booking_id').val(booking_id);
    
    $('#call_history_id').val(call_history_id);
}

$("#schedule_reactivate_btn").click(function () {
  
    var booking_id = $('#booking_id').val();
    // var service_date = $('#service_date').val();
    var call_history_id = $('#call_history_id').val();
    // var customer_id = $('#re_activate_customer_id').val();
    // var mobile_number = $('#suspend_customer_phoneno').val();
    // var mobile_number = $('#re_activate_customer_phoneno').val();
    // var caller_re_activate_typ_id = $('#caller_re_activate_typ_id').val();
   
    $.ajax({
        type: "POST",
        url: _base_url + 'call-management/booking_schedule_re_activate',
        data: {booking_id: booking_id, call_history_id: call_history_id},
        cache: false,
        success: function (data)
        {
            if (data.status == "success") {
                toast('success', data.message);
                close_fancybox();
                // $("#re_activation_booking_id_"+booking_id).remove();
                // $("#delete_booking_id_"+booking_id).show();
                // $("#suspend_one_day_booking_id_"+booking_id).show();
                $("#cancel_one_day_booking_id_"+booking_id).show();
                $("#suspend_date_booking_id_"+booking_id).show();
                $("#cancel_permanently_booking_id_"+booking_id).show();
                $("#reactivate_booking_id_"+booking_id).hide();
                // window.location.href = _base_url + "call-management";
                
            }
            else {
                toast('error', data.message);
                loader(false);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
    
});
function create_schedule(customer_id, maid_name, maid_id, customer_name, customer_phoneno, caller_type, booking_customer_address_id)
{
    
    // var Timefrom = $('#call_management_booking_form select[name="availability_time_from"]').val();
    var booking_type = $('#call_management_booking_form select[name="booking_type"]').val();
    // var startTime = moment($('#call_management_booking_form select[name="availability_time_from"]').val(), 'HH:mm:ss');
    $('#create_schedule_booking_form select[name="booking_type"]').val(booking_type);

    var booking_date = $('#call_management_booking_form input[name="booking_date"]').val();
    // $("#schedule_time_from").select2().select2('val',Timefrom);

    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    // alert (days[dow]);
    $('#create_schedule_booking_form input[name="service_week_name"]').val(days[dow]);
    $('#create_schedule_booking_form input[name="caller_booking_typ_id"]').val(caller_type);
    $('#create_schedule_booking_form input[name="booking_customer_address_ids"]').val(booking_customer_address_id);
    $('#create_schedule_booking_form input[name="customer_booking_phone_no"]').val(customer_phoneno);
    $('#create_schedule_booking_form input[name="service_start_date1"]').val(booking_date);
    $('#create_schedule_booking_form input[name="service_end_date1"]').val(booking_date);
    $('#create_schedule_booking_form input[name="customer_name"]').val(customer_name);
    $('#create_schedule_booking_form input[name="maid_id"]').val(maid_id);
    $('#create_schedule_booking_form input[name="maid_name"]').val(maid_name);
    $('#create_schedule_booking_form input[name="customer_ids"]').val(customer_id);

    var fromTime = '08:00:00'; // Default start time
    var toTime = '08:30:00';   // Default end time (30 minutes after fromTime)
    $('#create_schedule_booking_form select[name="schedule_time_from"]').val(fromTime).trigger('change'); // Set "From Time" to 8:30 AM
    $('#create_schedule_booking_form select[name="schedule_time_to"]').val(toTime).trigger('change');   // Set "To Time" to 9:00 AM
   
    booking_on_change();
    fancybox_show('create-schedule-popup', {});
}

var cleaning_material_rate_per_hour = 0;
function reCalculateForm() {
    cleaning_material_rate_per_hour = $('#create_schedule_booking_form input[name="cleaning_material_rate_per_hour"]').val();
    var startTime = moment($('#create_schedule_booking_form select[name="schedule_time_from"]').val(), 'HH:mm:ss');
    if ($('#create_schedule_booking_form select[name="schedule_time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#create_schedule_booking_form select[name="schedule_time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#create_schedule_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#create_schedule_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#create_schedule_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#create_schedule_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#create_schedule_booking_form input[name="material_fee"]').val(material_fee);
    $('#create_schedule_booking_form input[name="service_amount"]').val(service_amount);
    $('#create_schedule_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#create_schedule_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#create_schedule_booking_form input[name="service_discount"]').val(service_discount);
    //$('#new_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#create_schedule_booking_form input[name='cleaning_materials'],#create_schedule_booking_form select[name='schedule_time_to']").change(function () {
    reCalculateForm();
});
$("#create_schedule_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#create_schedule_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#create_schedule_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#create_schedule_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $(this).valid();
});
$("#create_schedule_booking_form input[name='service_discount_rate_per_hour'],#create_schedule_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
function booking_on_change() {
    $("#create_schedule_booking_form select[name='booking_type']").change(function () {
        // reCalculateForm();
    var booking_type =  $(this).val();
        if (booking_type == 'WE') {
            $('#booking_service_date').show();
        } else {
            $('#booking_service_date').hide();
        }
    });
}

$("#booking_type_enquiry").on('change', function(){
    var booking_type_enquiry = $(this).val();
    if (booking_type_enquiry =='OD') {
        $('#date_range').hide();
        $('#enquiry_time').show();
        $('#enquiry_save').hide();
        $('#week_available').hide();
        $('#enquiry_search').show();
        $("#maid_enquiry_search").show();
        $('#enquiry_zone').show();
        
    } else {
        $('#date_range').show();
        $('#enquiry_time').hide();
        $('#week_available').show();
        $('#enquiry_save').show();
        $('#enquiry_search').hide();
        $('#enquiry_zone').hide();
        $("#maid_enquiry_search").hide();


    }

});

function create_enquiry_schedule(customer_id, maid_name, maid_id, customer_name, customer_phoneno, caller_type, enquiry_customer_address_id)
{
    
    // var Timefrom = $('#call_management_booking_form select[name="availability_time_from"]').val();
    var booking_type = $('#call_management_enquiry_form select[name="booking_type_enquiry"]').val();
    // var startTime = moment($('#call_management_booking_form select[name="availability_time_from"]').val(), 'HH:mm:ss');
    $('#create_enquiry_schedule_booking_form select[name="booking_type"]').val(booking_type);

    var booking_date = $('#call_management_enquiry_form input[name="enquiry_booking_date"]').val();
    // $("#schedule_time_from").select2().select2('val',Timefrom);

    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    // alert (days[dow]);
    $('#create_enquiry_schedule_booking_form input[name="service_week_name"]').val(days[dow]);
    $('#create_enquiry_schedule_booking_form input[name="caller_enquiry_typ_ids"]').val(caller_type);
    $('#create_enquiry_schedule_booking_form input[name="enquiry_customer_address_ids"]').val(enquiry_customer_address_id);
    $('#create_enquiry_schedule_booking_form input[name="customer_enquiry_phone_no"]').val(customer_phoneno);
    $('#create_enquiry_schedule_booking_form input[name="service_start_date1"]').val(booking_date);
    $('#create_enquiry_schedule_booking_form input[name="service_end_date1"]').val(booking_date);
    $('#create_enquiry_schedule_booking_form input[name="customer_enquiryname"]').val(customer_name);
    $('#create_enquiry_schedule_booking_form input[name="customer_enquiryname1"]').val(customer_name);
    $('#create_enquiry_schedule_booking_form input[name="maid_id"]').val(maid_id);
    $('#create_enquiry_schedule_booking_form input[name="maid_name"]').val(maid_name);
    $('#create_enquiry_schedule_booking_form input[name="customer_enquiryids"]').val(customer_id);

    if($('#outside_service_check').is(':checked'))  
    {  
        var outside_service_checked =   $('#call_management_enquiry_form input[name="outside_service_checked"]').val();  
    }  
    
    var enquiry_date = $('#call_management_enquiry_form input[name="enquiry_booking_date"]').val();
    // var service_type_id = $('#call_management_enquiry_form select[name="service_id"]').val();
    var area_id = $('#call_management_enquiry_form select[name="area_id"]').val();
    var enquiry_notes = $('#call_management_enquiry_form textarea#enquiry_notes').val();
    var location_id = $('#call_management_enquiry_form select[name="location_id"]').val();
    var enquirysourceId = $('#call_management_enquiry_form select[name="enquiry_source_id"]').val();
    var outside_new_area = $('#call_management_enquiry_form input[name="outside_new_area"]').val();
    var outside_service_area = $('#call_management_enquiry_form input[name="outside_service_area"]').val();
    // var outside_new_area = $('#call_management_enquiry_form input[name="outside_new_area"]').val();
    // var outside_service_checked = $('#call_management_enquiry_form input[name="outside_service_checked"]').val();
    var enquiry_customer_email = $('#call_management_enquiry_form input[name="enquiry_customer_email"]').val();
    $('#create_enquiry_schedule_booking_form input[name="enquiry_customer_email1"]').val(enquiry_customer_email);
    $('#create_enquiry_schedule_booking_form input[name="outside_service_area1"]').val(outside_service_area);
    $('#create_enquiry_schedule_booking_form input[name="outside_new_area1"]').val(outside_new_area);
    $('#create_enquiry_schedule_booking_form input[name="locationId"]').val(location_id);
    $('#create_enquiry_schedule_booking_form input[name="areaId"]').val(area_id);
    $('#create_enquiry_schedule_booking_form input[name="enquirynote"]').val(enquiry_notes);
    $('#create_enquiry_schedule_booking_form input[name="enquirynote"]').val(enquiry_notes);
    $('#create_enquiry_schedule_booking_form input[name="enquirysourceId"]').val(enquirysourceId);
    $('#create_enquiry_schedule_booking_form input[name="outside_service_checked1"]').val(outside_service_checked);

    var fromTime = '08:00:00'; // Default start time
    var toTime = '08:30:00';   // Default end time (30 minutes after fromTime)
    $('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_from"]').val(fromTime).trigger('change'); // Set "From Time" to 8:30 AM
    $('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_to"]').val(toTime).trigger('change');   // Set "To Time" to 9:00 AM
   
    booking_on_enquiry_change();
    fancybox_show('create-enquiry-schedule-popup', {});
}

var cleaning_material_rate_per_hour = 0;
function reCalculateODForm() {
    cleaning_material_rate_per_hour = $('#create_enquiry_schedule_booking_form input[name="cleaning_material_rate_per_hour"]').val();
    var startTime = moment($('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_from"]').val(), 'HH:mm:ss');
    if ($('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#create_enquiry_schedule_booking_form select[name="schedule_enquiry_time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#create_enquiry_schedule_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#create_enquiry_schedule_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#create_enquiry_schedule_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#create_enquiry_schedule_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#create_enquiry_schedule_booking_form input[name="material_fee"]').val(material_fee);
    $('#create_enquiry_schedule_booking_form input[name="service_amount"]').val(service_amount);
    $('#create_enquiry_schedule_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#create_enquiry_schedule_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#create_enquiry_schedule_booking_form input[name="service_discount"]').val(service_discount);
    //$('#new_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#create_enquiry_schedule_booking_form input[name='cleaning_materials'],#create_enquiry_schedule_booking_form select[name='schedule_enquiry_time_to']").change(function () {
    reCalculateODForm();
});
$("#create_enquiry_schedule_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#create_enquiry_schedule_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#create_enquiry_schedule_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateODForm();
});
$("#create_enquiry_schedule_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $(this).valid();
});
$("#create_enquiry_schedule_booking_form input[name='service_discount_rate_per_hour'],#create_enquiry_schedule_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateODForm();
});
function booking_on_enquiry_change() {
    $("#create_enquiry_schedule_booking_form select[name='booking_type']").change(function () {
        // reCalculateForm();
    var booking_type =  $(this).val();
        if (booking_type == 'WE') {
            $('#booking_service_date_enquiry').show();
        } else {
            $('#booking_service_date_enquiry').hide();
        }
    });
}

function enquiry_save(customer_id, maid_name, maid_id, customer_name, customer_phoneno, caller_type) {

    if($('#outside_service_check').is(':checked'))  
    {  
        var outside_service_checked =   $('#call_management_enquiry_form input[name="outside_service_checked"]').val();  
    }  
    
    var enquiry_date = $('#call_management_enquiry_form input[name="enquiry_booking_date"]').val();
    var booking_type = $('#call_management_enquiry_form select[name="booking_type_enquiry"]').val();
    var service_type_id = $('#call_management_enquiry_form select[name="service_id"]').val();
    var area_id = $('#call_management_enquiry_form select[name="area_id"]').val();
    var location_id = $('#call_management_enquiry_form select[name="location_id"]').val();
    var enquiry_source_id = $('#call_management_enquiry_form select[name="enquiry_source_id"]').val();
    var outside_new_area = $('#call_management_enquiry_form select[name="outside_new_area"]').val();
    var outside_service_area = $('#call_management_enquiry_form input[name="outside_service_area"]').val();
    var outside_new_area = $('#call_management_enquiry_form input[name="outside_new_area"]').val();
    // var outside_service_checked = $('#call_management_enquiry_form input[name="outside_service_checked"]').val();
    var enquiry_customer_email = $('#call_management_enquiry_form input[name="enquiry_customer_email"]').val();
    var enquiry_time_from = $('#call_management_enquiry_form select[name="enquiry_time_from"]').val();
    var enquiry_time_to = $('#call_management_enquiry_form select[name="enquiry_time_to"]').val();
    var enquiry_notes = $('#call_management_enquiry_form textarea#enquiry_notes').val();    
    var working_hours = $('#call_management_enquiry_form input[name="working_hours"]').val();    
    $.ajax({
        type: "POST",
        url: _base_url + 'call-management/customer_enquiry_save',
        data: {customer_id: customer_id, maid_id: maid_id, customer_name:customer_name, customer_phoneno:customer_phoneno, caller_type:caller_type, enquiry_customer_email:enquiry_customer_email,
            outside_service_checked:outside_service_checked, outside_new_area:outside_new_area, outside_service_area:outside_service_area, location_id:location_id,
            area_id:area_id, service_type_id:service_type_id, enquiry_date:enquiry_date, enquiry_time_from:enquiry_time_from, enquiry_time_to:enquiry_time_to, booking_type:booking_type, enquiry_notes:enquiry_notes, enquiry_source_id:enquiry_source_id, working_hours:working_hours},
        cache: false,
        success: function (data)
        {
            if (data.status == "success") {
                toast('success', data.message);
                close_fancybox();
                // $("#re_activation_booking_id_"+booking_id).remove();
                // window.location.href = _base_url + "call-management";
                
            }
            else {
                toast('error', data.message);
                loader(false);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
    
}

$('#call_management_enquiry_form select[name="enquiry_time_to"]').on('change', function () {
    var time_to = $('#call_management_enquiry_form select[name="enquiry_time_to"]').val();
    var time_from = $('#call_management_enquiry_form select[name="enquiry_time_from"]').val();
    if (time_to) {
        $('#call_management_enquiry_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});

function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
// ***********************************************************


$(document).ready(function() {
    var outside_service_check = $('#outside_service_check');
    var area_div = $('.area_div');
    var location_div = $('.loc_div');
    var outside_new = $('#outside_new');

    outside_service_check.on('change', function() {
        if (this.checked) {
            area_div.hide();
            location_div.hide();
            outside_new.show();
        } else {
            area_div.show();
            location_div.show();
            outside_new.hide();
        }
    });
});


