/************************************************************************** */
/*                                                                          */

/*                                                                          */
/************************************************************************** */
var cleaning_material_rate_per_hour = 10;
var service_vat_percentage = 5;
// logic goes here...................
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#tb-slide-right').click(function () {
    var position = $('.time_line .time_slider').position();
    var max_left = time_grid_width - 3415 + 3 + 5;
    var left_pos = position.left - 71;
    left_pos = left_pos < max_left ? max_left : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -left_pos + 71 + 'px',
    }, 71);
});

$('#tb-slide-left').click(function () {
    var position = $('.time_line .time_slider').position();
    var left_pos = position.left + 71;
    left_pos = left_pos > 0 ? 0 : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    //$('.maid_name').css('padding-left', -position.left + 'px');
    position.left = position.left == 0 ? -71 : position.left;
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -position.left + 'px',
    }, 71);
});
$('.selector').selectable({
    cancel: ".booked",
    //distance: 1,
    start: function (event, ui) {
        $('.ui-selected').removeClass('ui-selected');
    },
    stop: function (event, ui) {
        var maid_id = $(this).attr('data-maid');
        var maid_name = $(this).attr('data-maid-name');
        var week_day = $(this).attr('data-week');
        var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
        var time_from = $('.ui-selected').first().attr('data-time-from');
        var time_to = $('.ui-selected').last().attr('data-time-to');
        $('#od_booking_form input[name="service_start_date"]').val(moment().format('DD/MM/YYYY'));
        $('#od_booking_form input[name="service_end_date"]').val("");
        $('#od_booking_form input[name="service_week_day"]').val(week_day);
        $('#od_booking_form input[name="service_week_name"]').val(week_name);
        $('#od_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
        $('#od_booking_form input[name="maid_id"]').val(maid_id);
        $('#od_booking_form input[name="maid_name"]').val(maid_name);
        $('#od_booking_form select[name="time_from"]').select2().val(time_from).trigger("change.select2");
        $('#od_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(time_from)).select2().val(time_to).trigger("change.select2");
        $('#od_booking_form select[name="customer_id"]').val("").trigger("change.select2");
        $('#od_booking_form select[name="service_type_id"]').select2().val(6).trigger("change.select2");
        $('#od_booking_form input[name="cleaning_materials"]').prop('checked', false);
        $('#od_booking_form input[name="booking_id"]').val(""); // important
        $('#od_booking_form input[name="service_rate_per_hour"]').val("");
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val("");
        $('#od_booking_form .vat_percentage').html(_service_vat_percentage.toFixed(2));
        $('#od_booking_form input[name="service_start_date"]').val($('#date').val());
        $('#od_booking_form input[name="service_end_date"]').val($('#date').val());
        reCalculateForm();
        /*********************************** */
        $('#od-booking-form-popup .title').html('New One Time Schedule');
        $('#od_booking_form select[name="customer_id"]').next().show();
        $('#od_booking_form input[name="customer_name"]').val("").hide();
        $("#od-booking-form-popup .modal-footer.new").show();
        $("#od-booking-form-popup .modal-footer.edit").hide();
        new_booking_form_validator.resetForm();
        fancybox_show('od-booking-form-popup', { width: 750 });
    }
});
if ($(".booking-position").length > 0) {
    var stickyTop = $('.booking-position').offset().top;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= stickyTop) {
            $('.scroll-top-fix').addClass('box-fixed-top');
            $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
        } else {
            $('.scroll-top-fix').removeClass('box-fixed-top');
            $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
        }
    });
}
function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
function RefreshSomeEventListener() {
    $(".booked_bubble").off();
    $(".booked_bubble").on("click", function () {
        //alert('b');
    });
}
function renderCurrentWeekDay() {
    maidWeeks(false);
    slots(false);
    $('#weeks .week[data-id="' + service_week_day + '"]').show();
    $('.tb-slider .slots[data-week="' + service_week_day + '"]').show();
}
function renderBookings() {
    window.history.pushState({}, "", "day_view_list?service_date=" + $('#date').val());
    loader(true);
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "schedule_data/day_schedules_list",
        data: { service_date: $('#date').val() },
        success: function (data) {
            /************************************************************** */
            // show current day only
            renderCurrentWeekDay();
            /************************************************************** */
            // $('#booking_details').show();
            //toastr.clear();
            if (data.schedules.length === 0) {
                toast('info', "No schedules found on " + moment($('#date').val()).format('DD-MM-YYYY') + '.');
            }
            var grand_total = data.grand_total;
            var maid_count = data.maid_count;
            var training_hours = data.training_hours;
            var free_hours = data.free_hours;
            var emergency_leave_count = data.emergency_leave_count;
            var vacations_count = data.vacations_count;
            var holidays_count = data.holidays_count;
            var medical_leaves_count = data.medical_leaves_count;
            var total_maid_leave_count = data.total_maid_leave_count;
            var present_maid_count = data.present_maid_count;
            var grand_suspend_total = data.grand_suspend_total;
            var leave_maid_types = data.leave_maid_types;
            // maid leaves count
            var counting_emergency_hours = emergency_leave_count * 8;
            $('#emergency_leave_count').html(emergency_leave_count +' - '+ counting_emergency_hours + `hrs`);
            var counting_vacations_hours = vacations_count * 8;
            $('#vacations').html(vacations_count +' - '+ counting_vacations_hours + `hrs`);
            var counting_holidays_hours = holidays_count * 8;
            $('#holidays').html(holidays_count +' - '+ counting_holidays_hours + `hrs`);
            var counting_medical_leaves_hours = medical_leaves_count * 8;
            $('#medical_leaves').html(medical_leaves_count +' - '+ counting_medical_leaves_hours + `hrs`);
            $('#absents_count').html(total_maid_leave_count);
            $('#presents_count').html(present_maid_count);
            // updateTotalHourWeek(data.total_hour_week);
            $('#grand_percentage').html('');
            $('#grand_total').html('');
            $('#gained_hours').html('');
            $('#lost_hours').html('');
            $('#total_hours').html('');
            $('#free_hours').html('');
            $('#training_hours').html('');
            // reset cells
            $('.booked_bubble').remove();
            $('.cell').removeClass("booked");
            // let booking_html = '';
            //
            // var count = 1;
            var total_maid_hours = maid_count * 8;
            var grantTotal = grand_total / 60;
            var grand_percentage = (grantTotal / (total_maid_hours / 100))
            grand_percentage = grand_percentage.toFixed(2)
            grand_percentage =  grand_percentage+`%`;
            
            $('#grand_percentage').html(grand_percentage);
            $('#grand_total').html(grantTotal);

            // Gained total hours 
            $('#gained_hours').html(grantTotal+`hrs`);

            // Grand Suspend total hours
            var suspend_total = grand_suspend_total / 60;
            $('#lost_hours').html(suspend_total+`hrs`);

            //Day schedule total Hrs
            var schedule_total = grantTotal + suspend_total;
            $('#total_hours').html(schedule_total+`hrs`);

            // training_hours 
            var Training_hours = training_hours / 60;
            $('#training_hours').html(Training_hours+`hrs`);

            // Free Hours
            var Free_hours = free_hours / 60;
            $('#free_hours').html(Free_hours+`hrs`);
            
            initializeDataTable(data.schedules);
            $.each(data.schedules, function (key, booking) {
                // var cells = booking.working_minutes / 30;
               
                if (booking.delete_status == null) {
                    var book_status = "Active";
                } else if (booking.delete_status == "suspend_date"){
                    var book_status = "Suspend";

                }else if (booking.delete_status == "cancel_one_day"){
                    var book_status = "Cancel";

                }

            });
            // $('#booking_customer').html(booking_html);

            RefreshSomeEventListener();
            styleThisWeekCells();
            loader(false);
        },
        error: function (data) {
            toast('warning', "An error occured !");
            $('.mm-loader').hide();
            loader(false);
        },
    });
}
// function updateTotalHourWeek(total_hour_week) {
//     // Clear previous data
//     $('[data-maid-id]').html('');
//     $('[data-maid-id-percentage]').html('');
//     $('[data-maid-week-id]').html('');
//     $('[data-maid-id-week-id]').html('');

//     // Update HTML content with new data
//     $.each(total_hour_week, function (key, tot) {
//         var working_minutes1 = tot.working_minutes / 60;
//         var max_time = 8;
//         var percentage = (working_minutes1 / (max_time / 100)).toFixed(2) + "%";
        
//         $('[data-maid-id="' + tot.maid_id + '"]').html(working_minutes1);
//         $('[data-maid-id-percentage="' + tot.maid_id + '"]').html(percentage);
//         $('[data-maid-week-id="' + tot.maid_id + '"]').html(working_minutes1);
//         $('[data-maid-id-week-id="' + tot.maid_id + '"]').html(percentage);
//     });
// }
function styleThisWeekCells() {
    var currentTime = moment();
    var dow = currentTime.day();
    var time_rounded = moment(currentTime).format("H:00:00");
    $('.cell').removeClass("current-time");
    $('.cell').removeAttr('style');
    $('.cell').each(function (index, option) {
        /******* current time highlight  */
        if ($(this).attr("data-time-from") == time_rounded) {
            //$(this).addClass("current-time");
            $(this).css({ "border-right": "1px solid rgb(255 150 150)" });
        }
    });
}

function schedule_reactivate(booking_type,booking_id,day_service_id,service_date1,service_date,service_status){
    fancybox_show('schedule_reactivate-popup', {});
    $('#schedule_reactivate input[name="booking_id"]').val(booking_id);

    // $('#booking_id').val(booking_id);
    
}

$("#schedule_reactivate_btn").click(function () {
  
    var booking_id =  $('#schedule_reactivate input[name="booking_id"]').val();
   
    $.ajax({
        type: "POST",
        url: _base_url + 'schedule/booking_schedule_re_activate',
        data: {booking_id: booking_id},
        cache: false,
        success: function (data)
        {
            if (data.status == "success") {
                toast('success', data.message);
                close_fancybox();
                setTimeout(function () {
                    window.location.href = _base_url + "schedule/day_view_list";
                }, 3000);
                // window.location.href = _base_url + "call-management";
                
            }
            else {
                toast('error', data.message);
                loader(false);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
    
});

function schedule_suspend_one_day(booking_type,booking_id,day_service_id,service_date1,service_date,service_status) {
    // alert("helooo");
    // var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    // var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    // var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    // var service_date = $('#new_booking_form input[name="service_date"]').val();
    // var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        $('#suspend_one_day_schedule_od input[name="booking_id"]').val(booking_id);
        $('#suspend_one_day_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#suspend_one_day_schedule_od input[name="service_date"]').val(service_date);
        $('#suspend_one_day_schedule_od input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('suspend-one-day-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        // $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        // $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#suspend_one_day_schedule_we input[name="booking_id"]').val(booking_id);
        $('#suspend_one_day_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#suspend_one_day_schedule_we input[name="service_date"]').val(service_date);
        $('#suspend_one_day_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-one-day-we-popup', { width: 450});

    }
}

function schedule_suspend_date_range(booking_type,booking_id,day_service_id,service_date1,service_date,service_status){
    // alert(service_date);
    // var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    // var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    // var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    // var service_date = $('#new_booking_form input[name="service_date"]').val();
    // var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        
        $('#suspend_date_range_schedule_od input[name="booking_id"]').val(booking_id);
        $('#suspend_date_range_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#suspend_date_range_schedule_od input[name="service_date"]').val(service_date);
        $('#suspend_date_range_schedule_od input[name="delete_date_from"]').val(moment(service_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#suspend_date_range_schedule_od input[name="delete_date_to"]').val(moment(service_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#suspend_date_range_schedule_od input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('suspend-date-range-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        // $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        // $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#suspend_date_range_schedule_we input[name="booking_id"]').val(booking_id);
        $('#suspend_date_range_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#suspend_date_range_schedule_we input[name="service_date"]').val(service_date);
        $('#suspend_date_range_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-date-range-we-popup', { width: 450});

    }
}
function cancel_permanently(booking_type,booking_id,day_service_id,service_date1,service_date,service_status) {
   
    if (booking_type == 'OD') {
        
        $('#cancel_permeantly_od input[name="booking_id"]').val(booking_id);
        $('#cancel_permeantly_od input[name="day_service_id"]').val(day_service_id);
        $('#cancel_permeantly_od input[name="service_date"]').val(service_date);
        $('#cancel_permeantly_od input[name="day_service_status"]').val(service_status);
       
        fancybox_show('cancel-permeantly-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
       
        $('#cancel_permeantly_we input[name="booking_id"]').val(booking_id);
        $('#cancel_permeantly_we input[name="day_service_id"]').val(day_service_id);
        $('#cancel_permeantly_we input[name="service_date"]').val(service_date);
        $('#cancel_permeantly_we input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-permeantly-we-popup', { width: 450});

    }
}

function schedule_cancel_one_day(booking_type,booking_id,day_service_id,service_date1,service_date,service_status) {

    if (booking_type == 'OD') {
        $('#cancel_one_day_schedule_od input[name="booking_id"]').val(booking_id);
        $('#cancel_one_day_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#cancel_one_day_schedule_od input[name="service_date"]').val(service_date);
        $('#cancel_one_day_schedule_od input[name="day_service_status"]').val(service_status);
      
        fancybox_show('cancel-one-day-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
       
        $('#cancel_one_day_schedule_we input[name="booking_id"]').val(booking_id);
        $('#cancel_one_day_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#cancel_one_day_schedule_we input[name="service_date"]').val(service_date);
        $('#cancel_one_day_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-one-day-we-popup', { width: 450});

    }
}

// Function to initialize DataTable
function initializeDataTable(dataArray) {
    // console.log(dataArray)
    var dataTable; // Define dataTable as a local variable
    if ($.fn.DataTable.isDataTable('#package-list-table')) {
        $('#package-list-table').DataTable().destroy();
    }

    var tableData = [];

    // Populate tableData with data from dataArray
    if (Array.isArray(dataArray)) {
        tableData = dataArray.map(function(data, index) {
          
            var book_status = data.delete_status == null ? "Active" : data.delete_booking_type;
            // var customer_booktype = data.customer_booktype == "0" ? "One-off" : "Regular";
            if (data.customer_booktype == "0") {
                var customer_booktype = "One-off";
            } else if (data.customer_booktype == "1") {
                var customer_booktype = "Regular";
            }
            if (data.delete_status == "suspend_date") {
                var suspend_date ="";  
                var cancel_one_day = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_cancel_one_day('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Cancel One Day</strong></a>`;
                // var suspend_date = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_suspend_date_range('`+ booking.booking_type+`', '`+ booking.booking_id+`', '`+ booking.day_service_id +`','`+ booking.service_date +`','`+ booking.service_start_date +`','`+ booking.day_service_status +`')"><strong>Suspend Date</strong></a>`;
                // var suspend_one_day = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_suspend_one_day('`+ booking.booking_type+`', '`+ booking.booking_id+`', '`+ booking.day_service_id +`','`+ booking.service_date +`', '`+ booking.service_start_date +`','`+ booking.day_service_status +`')"><strong>Suspend One Day</strong></a>`;
                // var cancel_one_day = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_cancel_one_day('`+ booking.booking_type+`', '`+ booking.booking_id+`', '`+ booking.day_service_id +`','`+ booking.service_date +`', '`+ booking.service_start_date +`','`+ booking.day_service_status +`')"><strong>Cancel One Day</strong></a>`;
                var cancel = `<a href="javascript:void(0);" class="slot-sml" onclick="return cancel_permanently('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Cancel Permanently</strong></a>`;
                if (data.reassign_status == null) {
                    var re_activate = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_reactivate('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Reactivate</strong></a>`;
                } else {
                    var re_activate = "";
                }
            } else if (data.delete_status == "cancel_one_day") {
                var cancel_one_day ="";  
                var suspend_date = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_suspend_date_range('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`','`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Suspend Date</strong></a>`;
                var cancel = `<a href="javascript:void(0);" class="slot-sml" onclick="return cancel_permanently('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Cancel Permanently</strong></a>`;
                if (data.reassign_status == null) {
                    var re_activate = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_reactivate('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Reactivate</strong></a>`;
                } else {
                    var re_activate = "";
                }
            } else if (data.delete_status == "cancel_permeantly") {
                var suspend_date ="";  
                var cancel_one_day ="";  
                var cancel ="";  
                var re_activate = "";
            }

            else {
                var suspend_date = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_suspend_date_range('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`','`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Suspend Date</strong></a>`;
                // var suspend_one_day = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_suspend_one_day('`+ booking.booking_type+`', '`+ booking.booking_id+`', '`+ booking.day_service_id +`','`+ booking.service_date +`', '`+ booking.service_start_date +`','`+ booking.day_service_status +`')"><strong>Suspend One Day</strong></a>`;
                var cancel_one_day = `<a href="javascript:void(0);" class="slot-sml" onclick="return schedule_cancel_one_day('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Cancel One Day</strong></a>`;
                var cancel = `<a href="javascript:void(0);" class="slot-sml" onclick="return cancel_permanently('`+ data.booking_type+`', '`+ data.booking_id+`', '`+ data.day_service_id +`','`+ data.service_date +`', '`+ data.service_start_date +`','`+ data.day_service_status +`')"><strong>Cancel Permanently</strong></a>`;
                var re_activate = "";
            }
           
            return {
                sl_no: index + 1, // Add serial number
                customer_code: data.customer_code,
                customer_name: data.customer_name,
                customer_type: customer_booktype,
                // booking_id: data.booking_id,
                service_start_date: data.service_start_date,
                booking_type: data.booking_type,
                maid_name: data.maid_name,
                time_from: moment(data.time_from, 'HH:mm:ss').format('hh:mm A'),
                time_to: moment(data.time_to, 'HH:mm:ss').format('hh:mm A'),
                working_minutes: data.working_minutes / 60,
                service_rate_per_hour: data._service_rate_per_hour,
                total_amount: data._total_amount,
                booking_zone: data.booking_zone,
                area_name: data.area_name,
                book_status: book_status,
                action: suspend_date +` <br>`+ cancel_one_day +`<br> `+cancel+`<br>`+ re_activate,
            };
        });
    } else {
        console.error("dataArray is not an array.");
        return; // Exit function if dataArray is not an array
    }

    // Initialize DataTable
    $('#package-list-table').DataTable({
        data: tableData,
        columns: [
            { data: 'sl_no', name: 'sl_no' }, // Slno column
            { data: 'customer_code', name: 'customer_code' },
            { data: 'customer_name', name: 'customer_name' },
            { data: 'customer_type', name: 'customer_type' },
            // { data: 'booking_id', name: 'booking_id' },
            { data: 'service_start_date', name: 'service_start_date' },
            { data: 'booking_type', name: 'booking_type' },
            { data: 'maid_name', name: 'maid_name' },
            { data: 'time_from', name: 'time_from' },
            { data: 'time_to', name: 'time_to' },
            { data: 'working_minutes', name: 'working_minutes' },
            { data: 'service_rate_per_hour', name: 'service_rate_per_hour' },
            { data: 'total_amount', name: 'total_amount' },
            { data: 'booking_zone', name: 'booking_zone' },
            { data: 'area_name', name: 'area_name' },
            { data: 'book_status', name: 'book_status' },
            { data: 'action', name: 'action' },
            // Add more columns as needed
        ]
    });

}

$('#pay_customer1').on('change', function() {
    if (this.checked) {
       $("#pay_amount").show();
    } else {
        $("#pay_amount").hide();
    }
});

$('#pay_customer').on('change', function() {
    if (this.checked) {
       $("#pay_amount1").show();
    } else {
        $("#pay_amount1").hide();
    }
});

$(document).ready(function () {
    $('#cal_his').DataTable();

    // var dataTable;

    // if ($('#package-list-table').length > 0) {
    //   dataTable = $('table#package-list-table').DataTable({
    //     'sPaginationType': 'full_numbers',
    //     'bSort': true,
    //     'iDisplayLength': 100,
    //     'scrollY': true,
    //     'orderMulti': false,
    //     'scrollX': true,
    //     'columnDefs': [{
    //       'targets': [-1],
    //       'orderable': false
    //     }]
    //   });
    // }
    renderBookings();
    suspend_one_day_schedule_od_validator = $('#suspend_one_day_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_one_day_schedule_od",
                data: $('#suspend_one_day_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-one-day-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_one_day_schedule_we_validator = $('#suspend_one_day_schedule_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_one_day_schedule_we",
                data: $('#suspend_one_day_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-one-day-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_date_range_schedule_od_validator = $('#suspend_date_range_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_date_range_schedule_od",
                data: $('#suspend_date_range_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_range_schedule_od")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_date_range_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-date-range-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_date_range_schedule_we_validator = $('#suspend_date_range_schedule_we').validate({
        ignore: [],
        rules: {
            delete_date_from: {
                required: true,
            },
            delete_date_to: {
                required: true,
            },
            remark: {
                required: true,
            },
           
        },
        messages: {
            delete_date_from: "Please enter date.",
            delete_date_to: "Please enter date.",
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_date_range_schedule_we",
                data: $('#suspend_date_range_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#suspend_date_range_schedule_we")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_date_range_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-date-range-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_permeantly_od_validator = $('#cancel_permeantly_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_permeantly_od",
                data: $('#cancel_permeantly_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#cancel_permeantly_od")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_permeantly_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-permeantly-od-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_permeantly_we_validator = $('#cancel_permeantly_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_permeantly_we",
                data: $('#cancel_permeantly_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#cancel_permeantly_we")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_permeantly_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-permeantly-we-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    validator = $('#delete_day_schedule').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/delete_schedule",
                data: $('#delete_day_schedule').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#delete_day_schedule")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#delete_day_schedule")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#delete-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_validator = $('#cancel_day_schedule').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_schedule",
                data: $('#cancel_day_schedule').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#cancel_day_schedule")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_day_schedule")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_one_day_schedule_we_validator = $('#cancel_one_day_schedule_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_one_day_schedule_we",
                data: $('#cancel_one_day_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#cancel_one_day_schedule_we")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_one_day_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-one-day-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_one_day_schedule_od_validator = $('#cancel_one_day_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_one_day_schedule_od",
                data: $('#cancel_one_day_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        // renderBookings();
                        $("#cancel_one_day_schedule_od")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view_list";
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-one-day-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
});

/********************************************************************************** */
// date picker
$('.datepicker').datepicker({ autoclose: true, todayHighlight: true, todayBtn: "linked" }).on('changeDate', function (event) {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var date_full = moment(date).format('DD/MM/YYYY');
    $('.hed-date-main-box').html(date_full);
    $('#date').val(date);
    service_week_day = moment(date).day();
    renderBookings();
    window.history.pushState({}, "", "day_view_list?service_date=" + date); // just change url without reload, useful for link sharing (with date)
});
$('.hed-date-left-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(-1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('.hed-date-right-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('#suspend_one_day_schedule_we input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
});

$('#suspend_date_range_schedule_od .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#suspend_date_range_schedule_we .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#cancel_one_day_schedule_we input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
});
/***************************************************************************************** */
$('#new_booking_form select[name="time_from"]').on('change', function () {
    $('#new_booking_form input[name="working_hours"]').val(0);
    $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change");
});
$('#new_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
/***************************************************************************************** */
function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#new_booking_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
/***************************************************************************************** */
$('#new_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_start_date"]').valid();
});
$('#new_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_end_date"]').valid();
});
$('#new_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
}).on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="time_to"]').select2().on("change", function (e) {
    $(this).valid();
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_week_day"]').on('change', function () {
    var week_day_id = this.value;
    var maid_id = $('#schedule-menu select[name="filter_maid_id"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_maid_id"]').on('change', function () {
    var maid_id = this.value;
    var week_day_id = $('#schedule-menu select[name="filter_week_day"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
    renderCurrentWeekDay();
});
/***************************************************************************************** */
function maidHeads(show) {
    if (show == true) {
        $('#weeks .maid_name_week').show();
        $('.tb-slider .maid_name').show();
    }
    else {
        $('#weeks .maid_name_week').hide();
        $('.tb-slider .maid_name').hide();
    }
}
function maidWeeks(show) {
    if (show == true) {
        $('#weeks .week').show();
    }
    else {
        $('#weeks .week').hide();
    }
}
function slots(show) {
    if (show == true) {
        $('.tb-slider .slots').show();
    }
    else {
        $('.tb-slider .slots').hide();
    }
}
/***************************************************************************************** */
$().ready(function () {
    new_booking_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: true,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#new_booking_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/save_schedule",
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
/***************************************************************************************** */
function reCalculateForm() {
    var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#new_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#new_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#new_booking_form input[name="material_fee"]').val(material_fee);
    $('#new_booking_form input[name="service_amount"]').val(service_amount);
    $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#new_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#new_booking_form input[name="service_discount"]').val(service_discount);
    $('#new_booking_form input[name="service_rate_per_hour"]').valid();
    $('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#new_booking_form input[name='cleaning_materials'],#new_booking_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#new_booking_form input[name='service_discount_rate_per_hour'],#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
$('button[data-action="show-bookings-for-dispatch"]').click(function (event) {
    $('.mm-loader').show();
    var service_date = $(this).attr("data-dispatch-date");
    $('input[id="check_all"]').prop('checked', false);
    $('#bookings-dispatch-list .inner').html('<div class="inner">Loading booikings<span class="dots_loader"></span></div>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_bookings_for_dispatch",
        data: { 'service_date': service_date },
        success: function (bookings) {
            if (bookings.length == 0) {
                toast('warning', "No pending schedule available to dispatch.");
                return false;
            }
            var _maid_html = `<div class="table"><input type="hidden" name="dispatch_service_date" value="` + service_date + `">`;
            $.each(bookings, function (key, booking) {
                //if (booking.day_service_id == null) {
                _maid_html += `<div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                    <div class="col-sm-10 n-pick-maid-left pl-0">
                    <span id=""><strong>Booking Ref. &nbsp;</strong>` + booking.reference_id + `</span><br />
                    <span id=""><strong>Schedule Ref. &nbsp;</strong>` + (booking.day_service_reference_id || '<i>NIL</i>') + `</span><br />
                    <span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;` + booking.customer_name + `</span>
                    <p><strong><i class="fa fa-clock-o" aria-hidden="true"></i></strong>&nbsp;` + moment(booking.time_from, ["HH:mm:ss"]).format("hh:mm A") + ` - ` + moment(booking.time_to, ["HH:mm:ss"]).format("hh:mm A") + ` ( ` + booking.maid_name + ` )</p>
                    </div>
                    <div class="col-sm-2 n-pick-maid-right pr-0">
                    <input type="checkbox" value="` + booking.booking_id + `" name="dispatch_booking_ids[]" id="dispatch_booking_id_` + key + `" ` + (booking.dispatch_status == 1 ? "disabled" : "") + `/>
                    <label for="dispatch_booking_id_` + key + `">
                    <span class="border-radius-3"></span>
                    </label>
                    <p class="text-success">`+ (booking.dispatch_status == 1 ? "Dispatched !" : "") + `</p>
                    </div></div>`;
                //}
            });
            _maid_html += `</div>`;
            $('#bookings-dispatch-list .inner').html(_maid_html);
            fancybox_show('bookings-for-dispatch', { width: 600 });
        },
        error: function (data) {
            $('#alert-title').html('Error !');
            $('#alert-message').html('An error occured !');
            fancybox_show('alert-popup');
        },
    });
});
$(document).on('change', 'input[name="dispatch_booking_ids[]"]', function () {
    if ($('input[name="dispatch_booking_ids[]"]:checked').length == $('input[name="dispatch_booking_ids[]"]').not(":disabled").length) {
        $("#bookings-for-dispatch #check_all").prop("checked", true);
    }
    else {
        $("#bookings-for-dispatch #check_all").prop("checked", false);
    }
    if ($('input[name="dispatch_booking_ids[]"]:checked').length > 0) {
        $('button[data-action="dispatch-now"]').show();
    }
    else {
        $('button[data-action="dispatch-now"]').hide();
    }
});
$('#bookings-for-dispatch #check_all').change(function () {
    if (this.checked) {
        $('input[name="dispatch_booking_ids[]"]').not(":disabled").prop("checked", true);
        if ($('input[name="dispatch_booking_ids[]"]:checked').length == 0) {
            toast('warning', "No booking available to dispatch.");
            $('#bookings-for-dispatch #check_all').prop("checked", false);
        }
    }
    else {
        $('input[name="dispatch_booking_ids[]"]').prop('checked', false);
    }
    $('input[name="dispatch_booking_ids[]"]').trigger("change");
});
$(document).on('change', 'input[name="day_service_ids[]"]', function () {
    if ($('input[name="day_service_ids[]"]:checked').length == $('input[name="day_service_ids[]"]').not(":disabled").length) {
        $("#bookings-for-confirm-dispatch #check_all").prop("checked", true)
    }
    else {
        $("#bookings-for-confirm-dispatch #check_all").prop("checked", false)
    }
});
$('button[data-action="dispatch-now"]').click(function (event) {
    $('.mm-loader').show();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "dispatch/dispatch_bookings",
        data: $('#bookings-dispatch-form').serialize(),
        success: function (data) {
            if (data.status == true) {
                close_fancybox();
                toast('success', data.message);
            }
            else {
                toast('error', data.message);
            }
            renderBookings();
        },
        error: function (data) {
            close_fancybox();
            toast('error', 'An error occured !');
            renderBookings();
        },
    });
});
/*************************************************************************************************************************** */
$().ready(function () {
    new_od_booking_form_validator = $('#od_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#od_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#od_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            },
            cleaning_material_rate_per_hour: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
            cleaning_material_rate_per_hour: "Enter materials per hour rate."
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "cleaning_material_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#od_booking_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/" + ($('#od_booking_form input[name="booking_id"]').val() == "" ? "save_booking" : "update_booking"),
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
function reCalculateFormOD() {
    var startTime = moment($('#od_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#od_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#od_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#od_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#od_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#od_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#od_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#od_booking_form input[name="material_fee"]').val(material_fee);
    $('#od_booking_form input[name="service_amount"]').val(service_amount);
    $('#od_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#od_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#od_booking_form input[name="service_discount"]').val(service_discount);
    //$('#od_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#od_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$('#od_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "get",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            return { results: response };
        },
        cache: true
    },
}).on("change", function (e) {
    $(this).valid();
});
$('#od_booking_form select[name="time_from"]').on('change', function () {
    $('#od_booking_form input[name="working_hours"]').val(0);
    $('#od_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change");
});
$('#od_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#od_booking_form select[name="time_to"]').val();
    var time_from = $('#od_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#od_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
$("#od_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#od_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateFormOD();
});
$("#od_booking_form input[name='service_discount_rate_per_hour'],#od_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $('#od_booking_form input[name="service_rate_per_hour"]').valid();
    reCalculateFormOD();
});
$("#od_booking_form input[name='cleaning_materials'],#od_booking_form select[name='time_to']").change(function () {
    reCalculateFormOD();
});
$('#od_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});

$('#schedulte_delete').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        $('#delete_day_schedule input[name="booking_id"]').val(booking_id);
        $('#delete_day_schedule input[name="day_service_id"]').val(day_service_id);
        $('#delete_day_schedule input[name="service_date"]').val(service_date);
        $('#delete_day_schedule input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('delete-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        // $('#booking_id_perment').val(booking_id);
        // $('#booking_id_day').val(booking_id);
        fancybox_show('delete-regular-schedule-popup', { width: 450});

    }
});
$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    $('.n-delete-set-left').addClass('de-select');

    $('.n-delete-set-right-cont').show(500);
    $('.n-delete-set-left-cont').hide(500);
});

$(".n-delete-set-left").click(function () {
    $('.n-delete-set-left').removeClass('de-select');
    $('.n-delete-set-right').addClass('de-select');

    $('.n-delete-set-left-cont').show(500);
    $('.n-delete-set-right-cont').hide(500);
});

$('.n-delete-set-left-cont .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#delete_from_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#delete_one_schedule input[name="booking_id_day"]').val();
    var delete_date_from = $('#delete_one_schedule input[name="delete_date_from"]').val();
    var delete_date_to = $('#delete_one_schedule input[name="delete_date_to"]').val();
    // var booking_id = $('#booking_id_day').val();
    // var delete_date_from = $('#delete_date_from').val();
    // var delete_date_to = $('#delete_date_to').val();
   // alert(delete_date_from)
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');
    } else if (delete_date_from == "")
    {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');   
    } else if (delete_date_to == "") {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block'); 
    }else {
        $('#deleteremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id, delete_date_from: delete_date_from, delete_date_to: delete_date_to},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    // var booking_id = $('#booking_id_perment').val();
    var booking_id = $('#delete_permeant_schedule input[name="booking_id_perment"]').val();
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#delete_remark_perm").focus();
         $('#deleteremarks_book').css('display','block');
    } else {
        $('#deleteremarks_book').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$('#schedule_cancel').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        $('#cancel_day_schedule input[name="booking_id"]').val(booking_id);
        $('#cancel_day_schedule input[name="day_service_id"]').val(day_service_id);
        $('#cancel_day_schedule input[name="service_date"]').val(service_date);
        $('#cancel_day_schedule input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('cancel-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        $('#cancel_permanent_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#cancel_one_schedule input[name="booking_id_day"]').val(booking_id);  
        // $('#booking_id_perment').val(booking_id);
        // $('#booking_id_day').val(booking_id);
        fancybox_show('cancel-regular-schedule-popup', { width: 450});

    }
});

$(".n-cancel-set-right").click(function () {
    $('.n-cancel-set-right').removeClass('de-select');
    $('.n-cancel-set-left').addClass('de-select');

    $('.n-cancel-set-right-cont').show(500);
    $('.n-cancel-set-left-cont').hide(500);
});

$(".n-cancel-set-left").click(function () {
    $('.n-cancel-set-left').removeClass('de-select');
    $('.n-cancel-set-right').addClass('de-select');

    $('.n-cancel-set-left-cont').show(500);
    $('.n-cancel-set-right-cont').hide(500);
});

$('.n-cancel-set-left-cont .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$(".cancel_yes_book_day").click(function () {
    var remarks = $.trim($('#cancel_remark_day').val());
    var booking_id = $('#cancel_one_schedule input[name="booking_id_day"]').val();
    var delete_date_from = $('#cancel_one_schedule input[name="delete_date_from"]').val();
    var delete_date_to = $('#cancel_one_schedule input[name="delete_date_to"]').val();
   // alert(delete_date_from)
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#cancel_remark_day").focus();
        $('#cancelremarks_book_day').css('display','block');
    } else if (delete_date_from == "")
    {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#cancelremarks_book_day').css('display','block');   
    } else if (delete_date_to == "") {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#cancelremarks_book_day').css('display','block'); 
    }else {
        $('#cancelremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/cancel_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id, delete_date_from: delete_date_from, delete_date_to: delete_date_to},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".cancel_yes_book").click(function () {
    var remarks = $.trim($('#cancel_remark_perm').val());
    // var booking_id = $('#booking_id_perment').val();
    var booking_id = $('#cancel_permanent_schedule input[name="booking_id_perment"]').val();
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#cancel_remark_perm").focus();
         $('#cancelremarks_book_day').css('display','block');
    } else {
        $('#cancelremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/cancel_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

