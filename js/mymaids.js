
$(function(){
    
   $('#per-page').change(function(){
       
      //window.location = _base_url + 'customers/' + $('#per-page').val() + '/' + $('#all-customers').val(); 
      window.location = _base_url + 'customers/' + $('#per-page').val();
   });
//   $('#all-customers').change(function(){
//      window.location = _base_url + 'customers/' + $('#per-page').val() + '/' + $('#all-customers').val(); 
//   });
    $('#all-maids').change(function () {
        //window.location = _base_url + 'customers/' + $('#per-page').val() + '/' + $('#all-customers').val(); 
        window.location = _base_url + 'maids/' + $('#all-maids').val();
    });
    $('#keyword-search').keyup(function (e) {

        var _keyword = $('#keyword-search').val();
        $.ajax({
            type: "POST",
            url: _base_url + 'customer/search',
            data: {search_keyword: _keyword},
            dataType: 'text',
            cache: false,
            success: function (response)
            {
                $('#customer-list .Row').html('');

                $(response).insertAfter('#customer-list .table-head');
                $('.widget-content').next('p').remove();
            }

        });
    });



    var bookId = $('#booking_ID').val();
    var areaId = $('#area_justmop').val();
    if (bookId > 0) {
        $(".booking-tab a")[0].click();
        if(areaId > 0)
        {
            $('#btn-search-maid').click();
        }
    }


   $('#cust_from_date,#cust_to_date,#payment_type,#all-customers,#sort_custmer,#sort_source,#sort_cust_type').on('change', function (e) {
	   $('.mm-loader').css('display','block');
       if($('#checkdate:checked').val() == 1)
       {
           var checkval = $('#checkdate:checked').val();
       } else {
           var checkval = 0;
       }
        var dataString = "";
        if ($('#from_date').val() != "")
            dataString+= "from_date=" + $('#cust_from_date').val() + "&";
        if ($('#to_date').val() != "")
            dataString+= "to_date=" + $('#cust_to_date').val() + "&";
        if($('#payment_type').val() != "")
            dataString+= "payment_type=" + $('#payment_type').val() + "&";
        if($('#all-customers').val() != "")
            dataString+= "all_customers=" + $('#all-customers').val() + "&";
        if($('#sort_custmer').val() != "")
            dataString+= "sort_custmer=" + $('#sort_custmer').val() + "&";
        if($('#sort_source').val() != "")
            dataString+= "sort_source=" + $('#sort_source').val() + "&";
        if($('#checkdate').val() != "")
            dataString+= "checkdate=" + checkval + "&";
        if($('#sort_cust_type').val() != "")
            dataString+= "cust_type=" +  $('#sort_cust_type').val() + "&";
        if (dataString != "") {
            //alert(dataString);
            $("#LoadingImage").show();
            $.ajax({
                type: "POST",
                url: _base_url + 'customer/search_by_date',
                data: dataString,
                dataType: 'text',
                cache: false,
                success: function (response)
                {
                    if (response) {
						$('.mm-loader').css('display','none');
                        if(response == "no")
                        {
                            $("#LoadingImage").hide();
                            alert("Please Remove any one of the date you entered and Try again.");
                        } else {
                            //alert(response);
                        $("#LoadingImage").hide();
                        //$('#customer-list #tests').html(response);
                        $('#customer-list .Row').html('');

                        $(response).insertAfter('#customer-list .table-head');
                        $('.widget-content').next('p').remove();
                    }
                    }
                }

            });
        }
    });
    
    $('#doj').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        //startDate: new Date()
    });

        
       
  
});

$(function () {
    $('#passport_expiry, #visa_expiry, #paymentissuedate, #invoiceduedate, #labour_expiry, #emirates_expiry, #zone_date,#cust_from_date,#cust_to_date, #schedule_date, #vehicle_date,#vehicle_date_to, #payment_date, #b-date-from, #b-date-to,#b-date-from-job, #b-date-to-job,#invoice-b-date-from-job, #invoice-b-date-to-job,#OneDayDate,#ActFromDate,#ActToDate,#invoice_date,#search_date_from,#search_date_to,#cust_state_frmdt,#cust_state_todt,#collect_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        //startDate: new Date()
    });
    if($('#customers_vh_rep').length){
        //$('#customers_vh_rep').selectize({sortField: 'text'});
       
       $("#customers_vh_rep").select2({
                            ajax: { 
                             url: _base_url+"reports/report_srch_usr",
                             type: "post",
                             dataType: 'json',
                             delay: 150,
                             data: function (params) {
                                    return {
                                      searchTerm: params.term, // search term
                                    };
                                  },
                             processResults: function (response) {return {results: response};},
                             cache: true
                            },
                           });
    }
    $('#invoiceissuedate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    
    $('#cstatemnet-date-from').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
	
	$('#service_date_val').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
		startDate: new Date()
    });
    
    $('#cstatemnet-date-to').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    
     //$("#booking_date").val(moment().format('DD/MM/YYYY'));
//     var nowFrom = moment().format('h:00 a');
//     $("#book-from-time").val(nowFrom);
 $('.end_datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date()
			});

//Setting from time as 8 Am default - customer booking module starts

//$("#book-from-time option").each(function() {
//    if($(this).text() == "8:00 am") {
//       $("#book-from-time").val($(this).val());
//    }                        
//});

//Setting from time as 8 Am default - customer booking module ends

//Setting to time as 12 Pm default - customer booking module starts

//$("#book-to-time option").each(function() {
//    if($(this).text() == "12:00 pm") {
//       $("#book-to-time").val($(this).val());
//    }                        
//});

//Setting to time as 12 Pm default - customer booking module ends


$("#booking_date").datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: new Date() ,
});

if($('.statement-customer-sec').length){$('.statement-customer-sec').select2();}


if($("table#job-table").length){
  // Handle form submission event
  //Jobs table

var table = $("table#job-table").dataTable({
            sPaginationType: "full_numbers",
            "bSort": true,
            "bInfo": false,
            "bLengthChange": false,
            //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
//            'columnDefs': [
//                {
//                   'targets': 0,
//                   'checkboxes': {
//                      'selectRow': true
//                   }
//                }
//            ],
//            'select': {
//               'style': 'multi'
//            },
            //'order': [[1, 'asc']],
            "iDisplayLength": 100
        });
        $('#tbl-btn').on('click', function(e){
       
            var selectedIds = table.columns().checkboxes.selected()[0];
            //console.log(selectedIds)

            selectedIds.forEach(function(selectedId) {
                alert(selectedId);
            });
        });
   //Ends
}

if($("table#invoice-job-table").length)
    {
       var invoicetable = $("table#invoice-job-table").dataTable({
            sPaginationType: "full_numbers",
            "bSort": true,
            "bInfo": false,
            "bLengthChange": false,
            "bFilter": false,
            //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
//            'columnDefs': [
//                {
//                   'targets': 0,
//                   'checkboxes': {
//                      'selectRow': true
//                   }
//                }
//            ],
//            'select': {
//               'style': 'multi'
//            },
            //'order': [[1, 'asc']],
            "iDisplayLength": 100
        }); 
    }


if($("table#statement-content-table").length){        
//Customer Statement table
var statementtable = $("table#statement-content-table").dataTable({
            sPaginationType: "full_numbers",
            "bSort": false,
            "bInfo": false,
            "bLengthChange": false,
            "bFilter": false,
            //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
//            'columnDefs': [
//                {
//                   'targets': 0,
//                   'checkboxes': {
//                      'selectRow': true
//                   }
//                }
//            ],
//            'select': {
//               'style': 'multi'
//            },
            //'order': [[1, 'asc']],
            "iDisplayLength": 100
        });
   //Ends
} 
   
});




if($('.sel2').length > 0) $('.sel2').select2();

(function (a) {
    a(document).ready(function (b) {
        if(a('#da-ex-datatable-numberpaging').length > 0)
        {
        a("table#da-ex-datatable-numberpaging").dataTable({sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100,"scrollY": true,
        "scrollX": true});
        a("table#da-ex-datatable-default").dataTable({"iDisplayLength": 100});
    }
        
    });
    
})(jQuery);



function change_status(maid_id,status)
{
    if(status === 1)
    {
        if (confirm('Are you sure you want to Disable this Maid'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "maid/change_status",
                data: {maid_id: maid_id,status:status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    
                    window.location.assign(_base_url + 'maids');
                }
            });
        }
    }
    else if(status === 0)
    {
        if (confirm('Are you sure you want to Activate this Maid'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "maid/change_status",
                data: {maid_id: maid_id,status:status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'maids');
                }
            });
        }
    }
    else if(status==2)
    {
        if (confirm('Are you sure you want to Delete this Maid'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "maid/change_status",
                data: {maid_id: maid_id,status:status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'maids');
                }
            });
        }
        
    }
    
}
$(document).ready(function () {
    $('#changetabbutton').click(function(){
        var next = $('#mytabs li.active').next()
        next.length?
        next.find('a').click():
        $('#mytabs li a')[0].click();
    })
//$('#changetabbutton').click(function(e){
//            e.preventDefault();
//            var next = $('#mytabs li.active').next()
//            next.length?
//              next.find('a').click():
//              $('#mytabs li a')[0].click();
//        });
    });
$(document).ready(function () {


    var max_fields = 10; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
        var areas = $('#area').html();
        //alert(areas);

        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div style="margin-top:20px;"><div class="n-field-main"><p>Area</p><div class="n-field-box"><select name="area[]" id="area" class="">' + areas + '</select></div></div><div class="n-field-main"><p>Apartment</p><div class="n-field-box"><input id="apartment_no'+x+'" type="text" name="apartment_no[]" class=""/></div></div><div class="n-field-main"><p>Address</p><div class="n-field-box"><input id="address'+x+'" type="text" name="address[]" onkeyup="loadLocationField(\'address'+x+'\','+x+');" class="" />\n\
            <input type="hidden" name="address_id[]" value="" /><input type="hidden" name="lat[]" id="latitude_'+x+'" value=""/><input type="hidden" name="lng[]" id="longitude_'+x+'" value=""/></div></div><div class="n-field-main"><p class="text-right"><a href="#" class="remove_field red-btn n-btn-sml" data-xvalue="' + x + '" class="n-btn-sml red-btn">Remove</a></p></div></div>'); //add input box
            $(wrapper).append('<div id="map_canvas_'+x+'"></div>');
        }
    });
    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        var xval=$(this).attr('data-xvalue');
        $("#map_canvas_"+xval).remove();
        x--;
    });
	


});

$(document).ready(function() {
    $('input[type=radio][name=company]').change(function() {
        if (this.value == 'Y') {
            $('#company_source').css('display','block');
        }
        else if (this.value == 'N') {
            $('#company_source').css('display','none');
        }
    });
    $('#exTab2 .nav-tabs a[data-toggle=tab]').click(function(){
        $('.mm-loader').css('display','block');
        var tabid = this.id;
        var ser_date = $('#formatted-date-job').val();
        if(tabid == 'tab1')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'unscheduled', servicedate : ser_date}, function(response) {
                $('#tabtbody1').html('');
                $('#tabtbody1').html(response);
                var table = $("table#job-table").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "bInfo": false,
                    "bLengthChange": false,
            //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
//            'columnDefs': [
//                {
//                   'targets': 0,
//                   'checkboxes': {
//                      'selectRow': true
//                   }
//                }
//            ],
//            'select': {
//               'style': 'multi'
//            },
            //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
  
                $('.mm-loader').css('display','none');
            });
        } else if(tabid == 'tab2')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'scheduled', servicedate : ser_date}, function(response) {
                $('#tabtbody2').html('');
                $('#tabtbody2').html(response);
                var table2 = $("table#job-table2").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
            });
        } else if(tabid == 'tab3')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'inprogress', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#tabtbody3').html('');
                $('#tabtbody3').html(response);
                var table3 = $("table#job-table3").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                $('#tbl-btn3').on('click', function(e){

                   var selectedIds = table.columns().checkboxes.selected()[0];
                console.log(selectedIds)

                selectedIds.forEach(function(selectedId) {
                    alert(selectedId);
                });
                });
            });
        } else if(tabid == 'tab4')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'finished', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#tabtbody4').html('');
                $('#tabtbody4').html(response);
                
                var table4 = $("table#job-table4").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                $('#tbl-btn4').on('click', function(e){

                   var selectedIds = table.columns().checkboxes.selected()[0];
                console.log(selectedIds)

                selectedIds.forEach(function(selectedId) {
                    alert(selectedId);
                });
                });
            });
        } else if(tabid == 'tab5')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'recurring', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#tabtbody5').html('');
                $('#tabtbody5').html(response);
                var table5 = $("table#job-table5").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                $('#tbl-btn5').on('click', function(e){

                   var selectedIds = table.columns().checkboxes.selected()[0];
                console.log(selectedIds)

                selectedIds.forEach(function(selectedId) {
                    alert(selectedId);
                });
                });
            });
        } else if(tabid == 'tab6')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'missed', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#tabtbody6').html('');
                $('#tabtbody6').html(response);
                var table6 = $("table#job-table6").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                $('#tbl-btn6').on('click', function(e){

                   var selectedIds = table.columns().checkboxes.selected()[0];
                console.log(selectedIds)

                selectedIds.forEach(function(selectedId) {
                    alert(selectedId);
                });
                });
            });
        } else if(tabid == 'tab7')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'cancelled', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#tabtbody7').html('');
                $('#tabtbody7').html(response);
                var table7 = $("table#job-table7").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                $('#tbl-btn7').on('click', function(e){

                   var selectedIds = table.columns().checkboxes.selected()[0];
                console.log(selectedIds)

                selectedIds.forEach(function(selectedId) {
                    alert(selectedId);
                });
                });
            });
        } else if(tabid == 'tab8')
        {
            
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','block');
            $('#vehicle_date,#gohide').css('display','none');
            var start_date = $('#b-date-from-job').val();
            var end_date = $('#b-date-to-job').val();
            $.post( _page_url, { action: 'all', startdate : start_date ,enddate : end_date}, function(response) {
                //alert(response);
                $('#tabtbody8').html('');
                $('#tabtbody8').html(response);
                var table8 = $("table#job-table8").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                $('#tbl-btn8').on('click', function(e){

                   var selectedIds = table.columns().checkboxes.selected()[0];
                console.log(selectedIds)

                selectedIds.forEach(function(selectedId) {
                    alert(selectedId);
                });
                });
            });
        } else if(tabid == 'tab9')
        {
            $('#b-date-from-job,#b-date-to-job,#job-search').css('display','none');
            $('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'delayed', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#tabtbody9').html('');
                $('#tabtbody9').html(response);
                var table9 = $("table#job-table9").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
            });
        }
    });
    
    //Invoice Page
    $('#invoice-exTab2 .nav-tabs a[data-toggle=tab]').click(function(){
        $('.mm-loader').css('display','block');
        var tabid = this.id;
        var ser_date = $('#invoicedateformat').val();
        if(tabid == 'invoice-tab1')
        {
            $('#invoice-b-date-from-job,#invoice-b-date-to-job,#invoice-search').css('display','none');
            //$('#vehicle_date,#gohide').css('display','block');
            $('#invoice_date,#invoicego').css('display','block');
            $.post( _page_url, { action: 'new', servicedate : ser_date}, function(response) {
                $('#invoice-tabtbody1').html('');
                $('#invoice-tabtbody1').html(response);
                var table = $("table#invoice-job-table").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
            //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
//            'columnDefs': [
//                {
//                   'targets': 0,
//                   'checkboxes': {
//                      'selectRow': true
//                   }
//                }
//            ],
//            'select': {
//               'style': 'multi'
//            },
            //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
  
                $('.mm-loader').css('display','none');
            });
        } else if(tabid == 'invoice-tab2')
        {
            $('#invoice_date,#invoicego').css('display','none');
            $('#invoice-b-date-from-job,#invoice-b-date-to-job,#invoice-search').css('display','none');
            //$('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'due', servicedate : ser_date}, function(response) {
                $('#invoice-tabtbody2').html('');
                $('#invoice-tabtbody2').html(response);
                var table2 = $("table#invoice-job-table2").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bFilter": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
            });
        } else if(tabid == 'invoice-tab3')
        {
            $('#invoice_date,#invoicego').css('display','none');
            $('#invoice-b-date-from-job,#invoice-b-date-to-job,#invoice-search').css('display','none');
            //$('#vehicle_date,#gohide').css('display','block');
            $.post( _page_url, { action: 'paid', servicedate : ser_date}, function(response) {
                //alert(response);
                $('#invoice-tabtbody3').html('');
                $('#invoice-tabtbody3').html(response);
                var table3 = $("table#invoice-job-table3").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bFilter": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
                
            });
        } else if(tabid == 'invoice-tab4')
        {
            $('#invoice_date,#invoicego').css('display','none');
            $('#invoice-b-date-from-job,#invoice-b-date-to-job,#invoice-search').css('display','block');
            //$('#vehicle_date,#gohide').css('display','block');
            var start_date = $('#invoice-b-date-from-job').val();
            var end_date = $('#invoice-b-date-to-job').val();
            $.post( _page_url, { action: 'all', startdate : start_date ,enddate : end_date}, function(response) {
                $('#invoice-tabtbody4').html('');
                $('#invoice-tabtbody4').html(response);
                
                var table4 = $("table#invoice-job-table4").dataTable({
                    destroy: true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "iDisplayLength": 100
                });
                $('.mm-loader').css('display','none');
            });
        }
    });
    
    //ENds
});
//$(document).on('click', '#btn-book-maid', function(){
$(document).on('click', '.book_maid_list', function(){
  
var id_maid_data = $(this).attr('id');
var id_maid = id_maid_data.split('maid_');
//alert(id_maid[1]);
var _maid_name = $.trim($('#name_'+id_maid[1]).html());


$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: true
			}
		},
		padding : 0,
		closeBtn : true,
		content : _alert_html = '<div id="alert-popup"><div class="head">Book<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure want to continue?</div><div class="bottom"><input type="button" value="Yes" class="pop_close book_yes" style="background:#b2d157;border:1px solid" id="'+id_maid[1]+'"  />&nbsp;&nbsp;<input type="button" value="No" class="book_no pop_close"  /></div></div>',
		topRatio : 0.2,
		
	});                              
                                        
                                        
                                        
});
//all jobs
$('#job-search').on('click', function(){
    var start_date = $('#b-date-from-job').val();
    var end_date = $('#b-date-to-job').val();
    $('.mm-loader').css('display','block');
    $.post( _page_url, { action: 'all', startdate : start_date ,enddate : end_date}, function(response) {
        
        
        $("table#job-table8").dataTable().fnDestroy();
        $('#tabtbody8').empty();
        $('#tabtbody8').html('');
        $('#job-table8 #tabtbody8').html(response);
        $("table#job-table8").DataTable({
                    "bDestroy": true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
        $('.mm-loader').css('display','none');
        //alert(response);
    });
});

//all invoices
$('#invoice-search').on('click', function(){
    var start_date = $('#invoice-b-date-from-job').val();
    var end_date = $('#invoice-b-date-to-job').val();
    $('.mm-loader').css('display','block');
    $.post( _page_url, { action: 'all', startdate : start_date ,enddate : end_date}, function(response) {
        
        
        $("table#invoice-job-table4").dataTable().fnDestroy();
        $('#invoice-tabtbody4').empty();
        $('#invoice-tabtbody4').html('');
        $('#invoice-job-table4 #invoice-tabtbody4').html(response);
        $("table#invoice-job-table4").DataTable({
                    "bDestroy": true,
                    sPaginationType: "full_numbers",
                    "bSort": true,
                    "autoWidth": false,
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        //            'columnDefs': [
        //                {
        //                   'targets': 0,
        //                   'checkboxes': {
        //                      'selectRow': true
        //                   }
        //                }
        //            ],
        //            'select': {
        //               'style': 'multi'
        //            },
                    //'order': [[1, 'asc']],
                    "iDisplayLength": 100
                });
        $('.mm-loader').css('display','none');
        //alert(response);
    });
});

//$('#b-date-from-job').on('changeDate', function(ev){ 
//	var _date = new Date(ev.date);
//	var _new_date = ('0' + _date.getDate()).slice(-2) + '/' + ('0' + (_date.getMonth()+1)).slice(-2) + '/' + _date.getFullYear();
//	//window.location = _base_url + 'booking/' + _new_date;
//        var start_date = _new_date;
//            var end_date = $('#b-date-to-job').val();
//            $('.mm-loader').css('display','block');
//            //alert(_page_url);
//            $.post( _page_url, { action: 'all', startdate : start_date ,enddate : end_date}, function(response) {
//                //alert(response);
//                $('#tabtbody8').html('');
//                $('#job-table8 #tabtbody8').html(response);
//                var table8 = $("table#job-table8").dataTable({
//                    destroy: true,
//                    sPaginationType: "full_numbers",
//                    "bSort": true,
//                    "autoWidth": false,
//                    "bInfo": false,
//                    "bLengthChange": false,
//                    //"lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
//        //            'columnDefs': [
//        //                {
//        //                   'targets': 0,
//        //                   'checkboxes': {
//        //                      'selectRow': true
//        //                   }
//        //                }
//        //            ],
//        //            'select': {
//        //               'style': 'multi'
//        //            },
//                    //'order': [[1, 'asc']],
//                    "iDisplayLength": 100
//                });
//                $('.mm-loader').css('display','none');
////                $('#tbl-btn8').on('click', function(e){
////
////                   var selectedIds = table.columns().checkboxes.selected()[0];
////                console.log(selectedIds)
////
////                selectedIds.forEach(function(selectedId) {
////                    alert(selectedId);
////                });
////                });
//            });
//});
//Ends

$(document).on('click', '.book_yes', function(){
 $.fancybox.close();
$("#LoadingImage").show();											  
var _maid_id = $(this).attr('id');

var _booking_ID = $.trim($('#booking_ID').val()); 
var _action='book-maid';
if(_booking_ID!="")
{
 var _action='assign-maid';   
}
var _service_date = $('#booking_date').val();
var _total_amt = $('#c_total_amt').val();
var _customer_id = $.trim($('#cust_id').val());
var _customer_address_id = $.trim($('#cust_add_id').val());
var _from_time = $.trim($('#book-from-time').val());
var _to_time = $.trim($('#book-to-time').val());
var _booking_type = $.trim($('#book-booking-type').val());

if($('#c-cleaning-materials').prop('checked') === true){
   var _clean_mat = 'Y';
}else{
	 var _clean_mat = 'N';
}

   
//var _clean_mat = $('#c-cleaning-materials').val();
var _b_notes = $('#c-booking-note').val();

var _repeat_end = '';
var _repeat_end_date = '';
	if(_booking_type == 'WE')
	{	
		
		  
		 		  
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			 
		  }
	}
	
	if(_booking_type == 'BW')
	{	
		
		  
		 		  
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			 
		  }
	}

$.post( _page_url, { action: _action, booking_id:_booking_ID, customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_date:_service_date, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date,total_amt: _total_amt,clean_mat: _clean_mat,b_notes: _b_notes}, function(response) {
		_bpop_open = false;
		//refresh_grid();
		var _alert_html = '';
			
		if(response == 'refresh')
		{
              $("#LoadingImage").hide();  
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="alert-popup-close cancel_btn pop_close" style="float:none;"  /></div></div>';
			_refresh_page = true;
		}
		else
		{
              
//alert('ESTUS-'+_resp.status)
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$("#LoadingImage").hide();
				_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close alert-popup-close" style="float:none;" /></div></div>';
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				 $("#LoadingImage").hide();
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">'+ _resp.message+'</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
				_refresh_page = true
			}
			else
			{
				 $("#LoadingImage").hide();
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
				_refresh_page = true
			}
		}
//alert('--alertHtml'+_alert_html);		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});	  
    
    
});

$(document).on('click', '.book_no,.alert-popup-close', function(){
											  
$.fancybox.close();
											  
});

$(document).on('click', '.jobclose', function(){
											  
$.fancybox.close();
window.location.assign(_base_url + 'activity/jobs');
											  
});

$(document).on('click', '#deletejobbutton', function(){
    var _booking_id = $('#hiddenbookingid').val();
    var _booking_type = $('#hiddenbookingtype').val();
    var _alert_html = '';
    if(_booking_type == 'OD')
    {
        _alert_html = '<div id="alert-popup"><div class="head">Delete <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure you want to delete this booking?</div><div class="bottom"><input type="button" id="delete-permanent-job" value="Yes" class=""  /><input type="button" value="No" class="cancel_btn pop_close"  /></div></div>';
        //$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>');
    }
    else
    {
        _alert_html = '<div id="alert-popup"><div class="head">Delete <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure you want to delete this booking?</div><div class="bottom"><input type="button" id="delete-one-day-job" value="Delete One Day" class=""  /><input type="button" id="delete-permanent-job" value="Delete Permanently" class=""  /></div></div>';
        //$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-one-day" value="Delete One Day" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>');
    }
    if(_alert_html  != '')
    {
        $.fancybox.open({
            autoCenter : true,
            fitToView : false,
            scrolling : false,
            openEffect : 'fade',
            openSpeed : 100,
            helpers : {
                    overlay : {
                            css : {
                                    'background' : 'rgba(0, 0, 0, 0.3)'
                            },
                            closeClick: false
                    }
            },
            padding : 0,
            closeBtn : false,
            content: _alert_html
        });
    }
});

$(document).on('click', '#delete-permanent-job', function(){
    var _booking_id = $('#hiddenbookingid').val();
    var _service_date = $('#hiddenbookingdate').val();
    $.post(_page_url, { action: 'delete-booking-permanent-job', booking_id: _booking_id, service_date: _service_date }, function(response) {
        var _alert_html = '';
        if(response == 'success')
        {
                _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="jobclose"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="jobclose" /></div></div>';
                //_refresh_page = false
        }
        else
        {
                if(response == 'locked')
                {
                        _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                        //_refresh_page = false;
                }
                else if(response == 'start')
                {
                        _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Service already started...</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                        //_refresh_page = true;
                } else {
                        _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                        //_refresh_page = true;
                }
        }

        if(_alert_html  != '')
        {
                $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'fade',
                        openSpeed : 100,
                        helpers : {
                                overlay : {
                                        css : {
                                                'background' : 'rgba(0, 0, 0, 0.3)'
                                        },
                                        closeClick: false
                                }
                        },
                        padding : 0,
                        closeBtn : false,
                        content: _alert_html
                });
        }
    });
});
    
$(document).on('click', '#delete-one-day-job', function(){
    var _booking_id = $('#hiddenbookingid').val();
    var _service_date = $('#hiddenbookingdate').val();
    $.post(_page_url, { action: 'delete-booking-one-day-job', booking_id: _booking_id, service_date: _service_date }, function(response) {
        var _alert_html = '';
        if(response == 'success')
        {
                _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="jobclose"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="jobclose" /></div></div>';
                //_refresh_page = false
        }
        else
        {
                if(response == 'locked')
                {
                        _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                        //_refresh_page = false;
                }
                else if(response == 'start')
                {
                        _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Service already started...</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                        //_refresh_page = true;
                } else {
                        _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                        //_refresh_page = true;
                }
        }

        if(_alert_html  != '')
        {
                $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'fade',
                        openSpeed : 100,
                        helpers : {
                                overlay : {
                                        css : {
                                                'background' : 'rgba(0, 0, 0, 0.3)'
                                        },
                                        closeClick: false
                                }
                        },
                        padding : 0,
                        closeBtn : false,
                        content: _alert_html
                });
        }
    });
});
/*image uploader*/

$(function () {
    var btnUpload = $('#me');
    var mestatus = $('#mestatus');
    var files = $('#files');
    var call_method = $('#call_method').val();
    var img_fold = $('#img_fold').val();
    var fileObj = $('input[type="file"]');
    var up_archive = new AjaxUpload(btnUpload, {
        action: _base_url + call_method,
        name: 'uploadfile',
        onSubmit: function (file, ext) {
            if (!(ext && /^(jpg|png|jpeg)$/.test(ext))) {
                alert('Only JPG,PNG files are allowed');
                mestatus.text('Only PNG files are allowed');
                return false;
            }

        },
        onChange: function (file, ext) {
            if (up_archive._input.files[0].size >= 1048576) { //1MB
                alert('Selected file is bigger than 1MB.');
                return false;
            }
        },
        onComplete: function (file, response) {
            //On completion clear the status
            mestatus.text('');
            //On completion clear the status
            $('#me span').html('<img src="' + _base_url + 'images/ajax-loader.gif" style="width:100px !important;height:100px !important">');
            //Add uploaded file to list
            if (response == "error") {
                $('<li style="list-style: none;margin-left: 0px;"></li>').appendTo('#me span').text(file).addClass('error');
            } else {
                $('#me span').html('');
                $('#img_name_resp').val(response);
                $('<li style="list-style: none;margin-left: 0px;"></li>').appendTo('#me span').html('<img style="width:100px !important;height:100px !important" src="' + _base_url + '' + img_fold + '/' + response + '"/><br />').addClass('success');
            }
        }
    });

});

function add_zone()
{
    $('#add_zone').show();
    $('#edit_zone').hide();
}
function hideadd_zone()
{
    $('#add_zone').hide();
}
function add_team()
{
    $('#add_team').show();
    $('#edit_team').hide();
}
function hideadd_team()
{
    $('#add_team').hide();
}
function edit_team(team_id)
{
    //alert(zone_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_team",
        data: {team_id: team_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_teamid').val(value.team_id)
                $('#edit_teamname').val(value.team_name)
                //$('#edit_drivername').val(value.driver_name)
                //$('input[value="' + value.spare_zone + '"]').prop('checked', true);

            });
            $('#add_team').hide();
            $('#edit_team').show();

        }
    });
}
function add_area()
{
    $('#add_area').show();
    $('#edit_area').hide();
    check_hide_status();
}
function hideadd_area()
{
    $('#add_area').hide();
}
function add_flat()
{
    $('#add_flat').show();
    $('#edit_flat').hide();
}
function hideadd_flat()
{
    $('#add_flat').hide();
}
function add_services()
{
    $('#add_services').show();
    $('#edit_services').hide();
}
function add_souq_price()
{
    $('#add_souqprice').show();
    $('#edit_add_souqprice').hide();
}
function hideadd_souq_price()
{
    $('#add_souqprice').hide();
}
function hideadd_services()
{
    $('#add_services').hide();
}
function add_tablet()
{
    $('#add_tablet').show();
    $('#edit_tablet').hide();
}
function hideadd_tablet()
{
    $('#add_tablet').hide();
}
function hideedit_zone()
{
    $('#edit_zone').hide();
}
function hideedit_team()
{
    $('#edit_team').hide();
}
function edit_zone(zone_id)
{
    //alert(zone_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_zone",
        data: {zone_id: zone_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_zoneid').val(value.zone_id)
                $('#edit_zonename').val(value.zone_name)
                $('#edit_drivername').val(value.driver_name)
                $('input[value="' + value.spare_zone + '"]').prop('checked', true);

            });
            $('#add_zone').hide();
            $('#edit_zone').show();

        }
    });
}
function delete_zone(zone_id)
{
    if (confirm('Are you sure you want to delete this Zone?'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_zone",
            data: {zone_id: zone_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                if (result == 1)
                {
                    alert("Cannot Delete! This Zones has Areas");
                }
                else
                {
                    alert("Zone Deleted Successfully");
                }
                window.location.assign(_base_url + 'settings/');
            }
        });

    }
}
function hideedit_area()
{
    $('#edit_area').hide();
}
function edit_area(area_id)
{
    //alert(zone_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_area",
        data: {area_id: area_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                //alert(value.zone_id);
                $('#edit_areaid').val(value.area_id)
                $('#edit_areaname').val(value.area_name)
                $('#edit_zone_id option[value="' + value.zone_id + '"]').prop('selected', true);

            });
            $('#add_area').hide();
            $('#edit_area').show();


        }
    });
}
function delete_area(area_id)
{
    if (confirm('Are you sure you want to delete this Area'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_area",
            data: {area_id: area_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'settings/area');
            }
        });
    }
}
function hideedit_flat()
{
    $('#edit_flat').hide();
}
function edit_flat(flat_id)
{
    //alert(flat_id);
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_flat",
        data: {flat_id: flat_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_flatid').val(value.flat_id);
                $('#edit_flatname').val(value.flat_name);
                $('#edit_tablet_imei').val(value.tablet_imei);

            });
            $('#add_flat').hide();
            $('#edit_flat').show();


        }
    });
}
function delete_flat(flat_id)
{
    if (confirm('Are you sure you want to delete this Flat'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_flat",
            data: {flat_id: flat_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'settings/flats');
            }
        });
    }
}
function hideedit_services()
{
    $('#edit_services').hide();
}
function hideedit_souq_price()
{
    $('#edit_add_souqprice').hide();
}
function edit_services(service_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_services",
        data: {service_id: service_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_service_type_id').val(value.service_type_id);
                $('#edit_service_type_name').val(value.service_type_name);
                $('#edit_service_rate').val(value.service_rate);

            });

            $('#add_services').hide();
            $('#edit_services').show();

        }
    });
}

$('.show_web_tog').change(function(){
	
	var serviceid=$(this).data("serid");
	var status='';
	if ($(this).is(':checked')) {status=1;}
	else {status=0;}
	
	$.ajax({
        type: "POST",
        url: _base_url + "settings/update_service_webshow_status",
        data: {serviceid: serviceid,webshow_status:status},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            

        }
    });
	
});


function edit_souq_price(price_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_souqprice",
        data: {price_id: price_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_service_offer_id').val(value.id);
                $('#edit_service_date_val').val(value.offer_date);
                $('#edit_service_rate').val(value.offer_price);
            });

            $('#add_souqprice').hide();
            $('#edit_add_souqprice').show();

        }
    });
}
function delete_services(service_id)
{
    if (confirm('Are you sure you want to delete this Services'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_services",
            data: {service_id: service_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'settings/services');
            }
        });
    }
}
function hideedit_tablet()
{
    $('#edit_tablet').hide();
}
function edit_tablet(tablet_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_tablet",
        data: {tablet_id: tablet_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                //alert(value.zone_id);
                $('#edit_tabletid').val(value.tablet_id)
                $('#edit_access_code').html(value.access_code)
                $('#edit_imei').val(value.imei)
                $('#editdrivernameval').val(value.tablet_driver_name)
                $('#edit_username').val(value.tablet_username=='0'?'':value.tablet_username)
                $('#edit_password').val(value.tablet_password=='0'?'':value.tablet_password)
                $('#edit_zone_id option[value="' + value.zone_id + '"]').prop('selected', true);

            });
            $('#add_tablet').hide();
            $('#edit_tablet').show();
        }
    });
}
function tablet_status(tablet_id, status)
{
    if (status === 1)
    {
        if (confirm('Are you sure you want to Diasble this Tablet'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/tablet_status",
                data: {tablet_id: tablet_id, status: status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'tablets');
                }
            });
        }
    }
    else if (status === 0)
    {
        if (confirm('Are you sure you want to Activate this Tablet'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/tablet_status",
                data: {tablet_id: tablet_id, status: status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'tablets');
                }
            });
        }
    }

}
function delete_maid(maid_id)
{
    if (confirm('Are you sure you want to delete this Maid'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "maid/remove_maid",
            data: {maid_id: maid_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'maid/maid_list');
            }
        });
    }
}
function view_maid(maid_id)
{
    var gender;
    var p_date;
    var p_date_str;
    var v_date;
    var v_date_str;
    var l_date;
    var l_date_str;
    var e_date;
    var e_date_str;

    $.ajax({
        type: "POST",
        url: _base_url + "maid/view_maid",
        data: {maid_id: maid_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            console.log(result);
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (view, value) {

                if (value.maid_visa_expiry_date == "0000-00-00" || value.maid_visa_expiry_date == null)
                {
                    p_date_str = "";
                }
                else {
                    p_date = new Date(value.maid_passport_expiry_date);
                    p_date_str = moment(p_date).format("DD/MM/YYYY");
                }
                if (value.maid_visa_expiry_date == "0000-00-00" || value.maid_visa_expiry_date === null)
                {
                    v_date_str = "";
                }
                else {
                    v_date = new Date(value.maid_visa_expiry_date);
                    v_date_str = moment(v_date).format("DD/MM/YYYY");
                }
                if (value.maid_labour_card_expiry_date == "0000-00-00" ||value.maid_labour_card_expiry_date ===null)
                {
                    l_date_str = "";
                }
                else {
                    l_date = new Date(value.maid_labour_card_expiry_date);
                    l_date_str = moment(l_date).format("DD/MM/YYYY");
                }

                if (value.maid_emirates_expiry_date == "0000-00-00" || value.maid_emirates_expiry_date === null)
                {
                    e_date_str = "";
                }
                else {
                    e_date = new Date(value.maid_emirates_expiry_date);
                    e_date_str = moment(e_date).format("DD/MM/YYYY");
                }

                if (value.maid_gender == "F")
                {
                    gender = "Female";
                }
                else
                {
                    gender = "Male";
                }
                /*if (value.maid_photo_file == null || value.maid_photo_file == "")
                {
                    photo = _base_url + "img/default_avatar.jpg";
                }
                else
                {
                    photo = _base_url + "upload/maid_avatars/" + value.maid_photo_file;
                }*/
                photo = value.maid_photo_file; // full image link



                $('#maid_photo').attr('src', photo);
                $('#maid_name').html(value.maid_name);
                $('#maid_gender').html(gender);
                $('#maid_nationality').html(value.maid_nationality);
                $('#maid_present_address').html(value.maid_present_address);
                $('#maid_permanent_address').html(value.maid_permanent_address);
                $('#maid_mobile_1').html(value.maid_mobile_1 || '-');
                $('#maid_mobile_2').html(value.maid_mobile_2 || '-');
                $('#maid_whatsapp_no_1').html(value.maid_whatsapp_no_1 || '-');
                $('#flat_name').html(value.flat_name || '-');
                $('#maid_passport_number').html(value.maid_passport_number || '-');
                $('#maid_passport_expiry_date').html(p_date_str || '-');
                $('#maid_visa_number').html(value.maid_visa_number || '-');
                $('#maid_visa_expiry_date').html(v_date_str || '-');
                $('#maid_labour_card_number').html(value.maid_labour_card_number || '-');
                $('#maid_labour_card_expiry_date').html(l_date_str || '-');
                $('#maid_emirates_id').html(value.maid_emirates_id || '-');
                $('#maid_emirates_expiry_date').html(e_date_str || '-');
                $('#maid_notes').html(value.maid_notes || '-');

            });
            $.fancybox.open({
                autoCenter: true,
                fitToView: false,
                scrolling: false,
                openEffect: 'none',
                openSpeed: 1,
                autoSize: false,
                width: 450,
                height: 'auto',
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.3)'
                        },
                        closeClick: false
                    }
                },
                padding: 0,
                closeBtn: false,
                content: $('#info-popup'),
            });
        }
    });
    //$('#myModal').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
}
function view_customer(customer_id)
{
    var j = 0;
    var customer_type;
    var payment_type;
    $.ajax({
        type: "POST",
        url: _base_url + "customer/view_customer",
        data: {customer_id: customer_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);                
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (view, value) {
                if (value.customer_photo_file == "")
                {
                    photo = _base_url + "img/no_image.jpg";
                }
                else
                {
                    photo = _base_url + "customer_img/" + value.customer_photo_file;
                }
                if (value.customer_type == "HO")
                {
                    customer_type = "Home";
                }
                else if (value.customer_type == "OF")
                {
                    customer_type = "Office";
                }
                else if (value.customer_type == "WH")
                {
                    customer_type = "Warehouse";
                }
                if (value.payment_type == "D")
                {
                    payment_type = "Daily Paying";
                }
                else if (value.payment_type == "W")
                {
                    payment_type = "Weekly Paying";
                }
                else if (value.payment_type == "M")
                {
                    payment_type = "Monthly Paying";
                }
                $('#customer_photo').attr('src', photo);
                $('#customer_name').html(value.customer_name);
                $('#customer_nick_name').html(value.customer_nick_name);
                $('#mobile_number_1').html(value.mobile_number_1);
                $('#mobile_number_2').html(value.mobile_number_2);
                $('#mobile_number_3').html(value.mobile_number_3);
                $('#phone_number').html(value.phone_number);
                $('#fax_number').html(value.fax_number);
                $('#email_address').html(value.email_address);
                $('#website_url').html(value.website_url);
                $('#customer_type').html(customer_type);
                $('#contact_person').html(value.contact_person);
                $('#payment_type').html(payment_type);
                $('#payment_mode').html(value.payment_mode);
                $('#price_hourly').html(value.price_hourly);
                $('#price_extra').html(value.price_extra);
                $('#price_weekend').html(value.price_weekend);
                $('#customer_notes').html(value.customer_notes);

                $("#cust_add").html("");
                $.each(value.address, function (see, val) {
                    //alert(val.customer_address);
                    $('#cust_add').append('<span><b>Address</b> &nbsp;' + (++j) + '&nbsp;:&nbsp;' + val.customer_address + '</span><br><span>Area (' + val.area_name + ')</span><br>');

//                        alert(val.customer_address_id);
//                        alert(val.customer_address);
//                        alert(val.area_name);
                });
            });
        }
    });
    $('#myModal').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
}
function cust_address(cust_id)
{
    //alert(cust_id);
    $.ajax({
        type: "POST",
        url: _base_url + "customer/view_customer_address",
        data: {cust_id: cust_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);                
            var obj = jQuery.parseJSON(result);
            //alert(obj[0].customer_address);
            $.each(obj, function (view, value) {
                $('#cust_name').html(value.customer_name);
                $('#customer_add').html(value.customer_address);
            });
        }
    });
    $('#cust_address').modal({backdrop: true, keyboard: true, show: true, width: "1000"});
}
function remove_address(address_id, customer_id)
{
    if (confirm('Are you sure you want to delete this Address'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "customer/remove_customer_address",
            data: {address_id: address_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'customer/edit/' + customer_id);
            }
        });
    }
}
function delete_customer($this, customer_id, customer_status)
{
    var _lblstatus = customer_status == 1 ? 'disable' : 'enable';
    if (confirm('Are you sure you want to ' + _lblstatus + ' this customer'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "customer/remove_customer",
            data: {customer_id: customer_id, customer_status : customer_status},
            dataType: "text",
            cache: false,
            success: function (result) {
                //window.location.assign(_base_url + 'customers');
                if(result == 1)
                {
                    $($this).attr('class', 'btn btn-success btn-small');
                    $($this).html('<i class="btn-icon-only icon-ok"></i>');
                }
                else
                {
                    if(result == 'exist_bookings') // Edited by Geethu
                    {
                        alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                        result = 0;
                    }
                    else
                    {
                        $($this).attr('class', 'btn btn-danger btn-small');
                        $($this).html('<i class="btn-icon-only icon-remove"> </i>');
                    }
                    
                }
                $($this).attr('onclick', 'delete_customer(this, ' + customer_id +', ' + result +')');
            }
        });
    }
}

function validate_maid()
{
    if ($('#maid_name').val() == '')
    {
        alert('Please Enter Name');
        return false;
    }
    if($('#nationality').val() == '')
    {
        alert('Please Enter Nationality');
        return false;
    }
    if($('#present_address').val() == '')   
    {
        alert('Please Enter Present Address');
        return false;
    }
    if($('#permanent_address').val() == '')
    {
        alert('Please Enter Permanent Address');
        return false;
    }
    if(($('#mobile1').val() == '') || (!$.isNumeric($('#mobile1').val())))
    {
        alert('Please Enter a Valid Mobile Number');
        return false;
    }
    
    if(!($('.services').is(':checked')))
    {
        alert('Please Select Services');
        return false;
    }
    return true;
}
function validate_customer()
{
    if ($('#customer_name').val() === '')
    {
        alert('Please Enter Customer Name');
        return false;
    }
    if($('#customer_nick').val() === '')
    {
        alert('Please Enter Customer Nick Name');
        return false;
    }
      // if($('#email').val() === '')
    // {
        // alert('Please Enter Email');
        // return false;
    // }
    if($('#area').val() === '')
    {
        alert('Please Enter Area');
        return false;
    }
    if($('#address').val() === '')
    {
        alert('Please Enter Address');
        return false;
    }
    if($('#contact_person').val() === '')
    {
        alert('Please Enter Contact Person');
        return false;
    }
    if(($('#mobile_number1').val() === '') || (!$.isNumeric($('#mobile_number1').val())))
    {
        alert('Please Enter a Valid Mobile Number');
        return false;
    }
//    if($('#user_name').val() === '')
//    {
//        alert('Please Enter User Name');
//        return false;
//    }
    if($('#email').val() !== '')
    {
        //var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        var pattern = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$/;
        if (!pattern.test($('#email').val())){                
                
            alert('Please Enter a Valid Email Address');
            return false;
        } 
        
    }
//    if($('#password1').val() === '')
//    {
//        alert('Please Enter Password');
//        return false;
//    }
    if(($('#hourly').val() === '') || (!$.isNumeric($('#hourly').val())))
    {
        alert('Please Enter a Valid Hourly Price');
        return false;
    }
    // if(($('#extra').val() === '') || (!$.isNumeric($('#extra').val())))
    // {
        // alert('Please Enter a Valid Extra Price');
        // return false;
    // }
    // if(($('#weekend').val() === '') || (!$.isNumeric($('#weekend').val())))
    // {
        // alert('Please Enter a Valid Weekend Price');
        // return false;
    // }
    
    return true;
}
$("#printButton").click(function ()
{
    var divContents = $("#divToPrint").html();
    var date = $("#zone_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
   
    
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Zone Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;('+day+')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});
$("#printinvoice").click(function ()
{
    var divContents = $("#divToPrint").html();
    var date = $("#invoice_date").val();

   
    
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">InvoiceReport</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});


$("#printBtn").click(function ()
{
    var divContents = $("#divPrint").html();
    var date = $("#vehicle_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
  
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Vehicle Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;('+day+')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});


$("#printButn").click(function ()
{
    var divContents = $("#divForPrint").html();
    var date = $("#payment_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
  
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Payment Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;('+day+')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});



$('#btn-search-booking').click(function(){
    $.post(_page_url, {action : 'search-booking', date_from : $('#b-date-from').val(), date_to : $('#b-date-to').val()}, function(response)
    {
        $('#pause-booking table > tbody').html(response);
    })
    
});

$('#btn-search-schedule-booking').click(function(){
    $.post(_page_url, {action : 'search-schedule', date_from : $('#b1-date-from').val(), date_to : $('#b1-date-to').val()}, function(response)
    {
        $('#schedule-booking table > tbody').html(response);
    })
    
});
//Edited by Aparna

function deleteOnedayCancel($this, id) {
    if (confirm("This booking will no longer be in canceled list.Are you sre to delete this?")) {
        $.ajax({
            url: _base_url + 'reports/delete_onedaycancel',
            type: 'POST',
            data: {id: id},
            success: function (result) {
                if (result == "success")
                    window.location.assign(_base_url + 'reports/one_day_cancel');
                else
                    alert(result);
            }
        });
    }
}
$("#EmpWorkPrint").click(function ()
{
    var divContents = $("#employeeWorkPrint").html();
    var maid = $('#maid option:selected').text();
    var month = $('#month').val();
    var year = $('#year').val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Employee Work Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Employee</b>&nbsp;: &nbsp;');
    printWindow.document.write(maid);
    printWindow.document.write('&nbsp;&nbsp;<b>Month</b>&nbsp;: &nbsp;');
    printWindow.document.write(month);
    printWindow.document.write('&nbsp;&nbsp;<b>Year</b>&nbsp;&nbsp;: &nbsp;');
    printWindow.document.write(year);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});
$("#OneDayPrint").click(function ()
{
    var divContents = $("#OneDayReportPrint").html();
    var date = $('#OneDayDate').val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">One Day Cancel Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Date</b>&nbsp;: &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});
$("#ActivityPrint").click(function ()
{
    var divContents = $("#ActivityReportPrint").html();
    var from_date = $('#ActFromDate').val();
    var to_date = $('#ActToDate').val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Activity Summary Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>From</b>&nbsp;: &nbsp;');
    printWindow.document.write(from_date);
    printWindow.document.write('&nbsp;&nbsp;<b>To</b>&nbsp;: &nbsp;');
    printWindow.document.write(to_date);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});

function pause_booking(booking_id, service_date, booking_type)
{
    if(confirm('Are you sure want to pause this booking?'))
    {
        $.post(_page_url, {action : 'pause-booking', booking_id : booking_id, service_date : service_date, booking_type : booking_type}, function(response){

            if(response == 'error')
            {
                alert('Unexpected error!');
            }
            else if(response == 'locked')
            {
                alert('Booking Locked by another user!');
            }
            else
            {
                
                $('#btn-search-booking').click();
            }

        });
    }
}

function start_booking(booking_id, service_date, booking_type)
{
    if(confirm('Are you sure want to start this booking?'))
    {
        $.post(_page_url, {action : 'start-booking', booking_id : booking_id, service_date : service_date, booking_type : booking_type}, function(response){

            if(response == 'error')
            {
                alert('Unexpected error!');
            }
            else if(response == 'locked')
            {
                alert('Booking Locked by another user!');
            }
            else
            {
                $('#btn-search-booking').click();
            }

        });
    }
}

$('#customer-id').change(function(){
    $.post(_base_url + 'settings/add_backpayment', {action : 'get-balance-amount', customer_id : $('#customer-id').val()}, function(response)
    {
        var _json = $.parseJSON(response);
        if(_json.status == 'success')
        {
            $('#requested-amount').val(_json.balance);
            $('#req-balance-amount').show();
        }
        else
        {
            alert(_json.message);
        }
    })
    
});

$('body').on('change', '#pcustomer-id', function() {
    $.post(_base_url + 'customer/add_payment', {action : 'get-outstanding-amount', customer_id : $('#pcustomer-id').val()}, function(response)
    {
        var _json = $.parseJSON(response);
        if(_json.status == 'success')
        {
            $('#requested-amount').val(_json.balance);
            $('#quickbook_id').val(_json.quickbookid);
            $('#req-balance-amount').show();
        }
        else
        {
            alert(_json.message);
        }
    })
    
});

$('body').on('change', '#customer_source', function() {
	if($("#customer_source").val() == 'Others')
	{
		$("#customer_source_others").show();
	} else {
		$("#customer_source_others_val").val('');
		$("#customer_source_others").hide();
	}
});




$('body').on('click', '#save-booking', function() {
//    alert('sdf');
	var _service_date = $('#service-date').val();
	var _maid_id = $.trim($('#maid-id').val());
	var _customer_id = $.trim($('#b-customer-id').val());
	var _customer_address_id = $.trim($('#customer-address-id').val());
	var _service_type_id = $.trim($('#b-service-type-id').val());
	var _from_time = $.trim($('#b-from-time').val());
	var _to_time = $.trim($('#b-to-time').val());
        var _lock_booking = $('#lock-booking').is(':checked') ? 1 : 0;
	var _booking_type = $.trim($('#b-booking-type').val());
	var _pending_amount = $.trim($('#b-pending_amount').val());
        var _discount = $.trim($('#b-discount').val());
	var _note = $.trim($('#booking-note').val());
	
	$('#b-error').text('');
	
	if($.isNumeric(_customer_id) == false)
	{
		$('#b-error').text('Select customer');
		return false;
	}
	
	if($.isNumeric(_customer_address_id) == false)
	{
		$('#b-error').text('Pick customer address');
		open_address_panel(_customer_id);
		return false;
	}
	
	if($.isNumeric(_service_type_id) == false)
	{
		$('#b-error').text('Select service type');
		return false;
	}
	
	if(_from_time == '' || _to_time == '')
	{
		$('#b-error').text('Select booking time');
		return false;
	}
	
	if(_booking_type == '')
	{
		$('#b-error').text('Select repeat type');
		return false;
	}
	
	var _repeat_days = [];
	var _repeat_end = '';
	var _repeat_end_date = '';
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{	
		_repeat_days = $('input[id^="repeat-on-"]:checked').map(function() {
			return this.value;
		  }).get();
		  
		  if(_repeat_days.length == 0)
		  {
			  $('#b-error').text('Select repeat days');
			  return false;
		  }
		  
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			  if(_repeat_end_date == '')
			  {
				  $('#b-error').text('Enter an end date');
				return false;
			  }
		  }
	  }
	  
	_refresh_page = false;
	$('#save-booking').attr('id', 'saving-booking');
	$('#saving-booking').val('Please wait...');

	$.post( _page_url, { action: 'book-maid', customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_type_id: _service_type_id, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, is_locked: _lock_booking, pending_amount: _pending_amount, booking_note: _note, discount : _discount }, function(response) {
		_bpop_open = false;
		//refresh_grid();
		var _alert_html = '';
			
		if(response == 'refresh')
		{
                
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = true;
		}
		else
		{
              
//alert('ESTUS-'+_resp.status)
			var _resp = $.parseJSON(response);
			if(typeof _resp.status && _resp.status == 'success')
			{	
				$('#saving-booking').val('Done');
				_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
				_refresh_page = false
			}
			else if(typeof _resp.status && _resp.status == 'error')
			{
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else
			{
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true
			}
		}
//alert('--alertHtml'+_alert_html);		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});
});





$('body').on('click', '#copy-booking', function() {
	var _booking_id = $('#booking-id').val();
	//$('#customer-picked-address').hide();
	open_maid_panel(_booking_id);
});
function open_maid_panel(_booking_id)
{
	//$('#customer-address-id').val('');
	//$('#customer-address-panel').hide();
	//$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');
	
	if($.isNumeric(_booking_id) && _booking_id > 0)
	{	
		$('#maids-panel').slideDown();
		$('input[name="same_zone"]').attr('onclick', 'open_maid_panel(' + _booking_id + ')');
                
		$.post(_page_url, { action: 'get-free-maids', booking_id: _booking_id, same_zone : $('input[name="same_zone"]:checked').val() }, function(response) {
			if(response == 'refresh')
			{
				$.fancybox.open({
					autoCenter : true,
					fitToView : false,
					scrolling : false,
					openEffect : 'fade',
					openSpeed : 100,
					helpers : {
						overlay : {
							css : {
								'background' : 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding : 0,
					closeBtn : false,
					content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});
				
				_refresh_page = true;
			}
			else
			{
                            
				var _resp = $.parseJSON(response);
                                if(typeof _resp.status && _resp.status == 'error')
                                {
                                        //$('#b-error').text(_resp.message);
                                        //$('#saving-booking').attr('id', 'save-booking');
                                        //$('#save-booking').val('Save');
                                        $('#maids-panel .inner').css("overflow","hidden");
                                        $('#maids-panel .inner').html('<div id="c-error">' + _resp.message + '</div>');
                                        
                                } 
                                else
                                {
                                    var _maid_html = '<div class="table">';

                                    var i = 0;
                                    $.each(_resp, function(key, val) {
                                        //if(i == 0)
                                        //{
                                            _maid_html += '<div class="row"><div class="cell1"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="cell2"><input type="button" value="Book &raquo;" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid"  /></div></div>';
                                            //++i;
                                        //}
                                        //else if(i == 1)
                                        //{
                                            //_maid_html += '<div class="cell1"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="cell2"><input type="button" value="Book &raquo;" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid"  /></div></div>';
                                            //i = 0;                                        
                                        //}

                                    });				

                                    _maid_html += '</div>';

                                    $('#maids-panel .inner').html(_maid_html);
                                }
			}
		});
	}
}




$('body').on('click', '#delete-booking', function() {
	var _booking_id = $('#booking-id').val();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_type = _all_bookings[_booking_id].booking_type;
	
	if(_booking_type == 'OD')
	{
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>');
	}
	else
	{
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-one-day" value="Delete One Day" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>');
	}
	
	$('#booking-action-confirm-panel').slideDown();
});

$('body').on('change', '#b-booking-type', function() {
	var _booking_type = $(this).val();
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{
		$('#repeat-days').css('display', 'table-row');
		$('#repeat-ends').css('display', 'table-row');
	}
	else
	{
		$('#repeat-days').hide();
		$('#repeat-ends').hide();
	}
});
$('body').on('click', '#btn-snd-mail', function() {
    var amount=$("#send_amount").val();
    var description=$("#send_description").val();
    var cust_id=$("#custtid").val();
    if(amount == "" || !$.isNumeric(amount))
	{
            alert("Amount invalid");
            return false;		
	}
        
    if(description=="")
	{
            alert("Please enter description");
            return false;		
	}
        
    $('.mm-loader').css('display','block');
    $.ajax({
        type: "POST",
        url: _base_url + "customer/senddueemail",
        data: {balance: amount,customer_id: cust_id,description:description},
        dataType: "text",
        cache: false,
        success: function (result) {
            $('.mm-loader').css('display','none');
            var _alert_html = "";
            if(result == "success")
            {
                $("#send_amount").val('');
                $("#send_description").val('');
                _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Email has been sent successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close alert-popup-close" style="float:none;" /></div></div>';
            } else {
                _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">No email found. Try again...</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
            }
            if(_alert_html  != '')
            {
                $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'fade',
                    openSpeed : 100,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                            }
                    },
                    padding : 0,
                    closeBtn : false,
                    content: _alert_html
                });
            }
            //window.location.assign(_base_url + 'customer/edit/' + customer_id);
        }
    });
    
    
});
$('body').on('click', '#btn-search-maid , .location', function() {
	
   
    
    
    var book_date=$("#booking_date").val();
	var from_time=$("#book-from-time").val();
	var to_time=$("#book-to-time").val();
	var book_type=$("#book-booking-type").val();
	var total_amtt=$("#c_total_amt").val();
	var cleaning_mat=$("#c-cleaning-materials").val();
	var b_notes = $("#c-booking-note").val();

        var custId=$("#cust_id").val();
        var zone=$("#cust_zone_id").val();
	var area=$("#cust_area_id").val();
	var province=$("#cust_prov_id").val();
        var first_area = $("#area_justmop").val();
        if(first_area == 0)
        {
            var justmoparea = $("#just_address_area").val();
        } else {
            var justmoparea = $("#area_justmop").val();
        }
     var cust_address_id = $("#cust_add_id").val();
     
        
    var location_filter_type = $(this).val();
    if(location_filter_type == 'Search') {
        location_filter_type = 'area';
    }
//        alert(location_filter_type);
    var loc_type , loc_val ;
        loc_type = location_filter_type;
    if(location_filter_type == 'zone') {
        loc_val = zone;
    }
    if(location_filter_type == 'area') {
        loc_val = area;
    }
    if(location_filter_type == 'province') {
        loc_val = province;
    }
    if((location_filter_type == 'all') || (location_filter_type == 'free')) {
        loc_val = 0;
    }
    
    
	if(book_date=="")
	{
	 alert("Please select the date field");
     return false;	 
		
	}
	
	if(total_amtt=="")
	{
	 alert("Please enter total amount");
     return false;	 
		
	}
        if(justmoparea==0)
	{
	 alert("Please select the Area");
     return false;	 
		
	}
	if(from_time=="")
	{
	 alert("Please select the time from field");
     return false;	 
		
	}
	if(to_time=="")
	{
	 alert("Please select the time to field ");
     return false;	 
		
	}
    if(book_type=="")
	{
	 alert("Please select the Repeat field");
     return false;	 
		
	}
      var _repeat_end = '';
     var _repeat_end_date = '';  
        if(book_type == 'WE')
	{	
		
		  _repeat_end = $('input[name="repeat_end"]:checked').val();
		  if(_repeat_end == 'ondate')
		  {
			  _repeat_end_date = $('#repeat-end-date').val();
			  if(_repeat_end_date == '')
			  {
				  alert('Enter an end date');
				return false;
			  }
		  }
	  }
        $("#LoadingImage").show();
        
        $('#maid_search').hide();
        $.ajax({
            type    : "POST",
            url     : _base_url + "customer/maids_booking",
            data    : {book_date    : book_date,
                        from_time   :from_time,
                        to_time     :to_time,
                        book_type   :book_type,
                        location_type   :loc_type,
                        location_value: loc_val,
                        cust_address_id: cust_address_id,
                        area_id     :area,
                        zone_id     :zone,
						total_amt : total_amtt,
						cleaning_mat : cleaning_mat,
						b_notes : b_notes,
                        province_id :province,
                        jm_area :justmoparea,
                        customer_id :custId,
						repeat_end : _repeat_end,
						repeat_end_date: _repeat_end_date},
            dataType: "text",
            cache   : false,
            success : function (result) {
                 $('#maid_search').show();
                $('#maid_search').html(result);
                $('#maid_search').addClass('widget-content');
                $("#LoadingImage").hide();
                $("input[name=location_filter][value=" + location_filter_type + "]").attr('checked', 'checked');
            }
        });
	
});






$('body').on('change', '#book-booking-type', function() {
	var _booking_type = $(this).val();
	if(_booking_type == 'WE' || _booking_type == 'BW')
	{
              
//		$('#repeat-days-search').css('display', 'table-row');
		$('#repeat-ends-search').css('display', 'table-row');
                $('#repeat-end-never').attr('checked',  true);
	}
	else
	{
//		$('#repeat-days-search').hide();
		$('#repeat-ends-search').hide();
	}
});
$('body').on('change', '#cust_address_area', function() {
	var areaId = $(this).val();
	var cust_add_id=$(this).find(':selected').attr('data-id');
        $('#cust_area_id').val(areaId);
        $('#cust_add_id').val(cust_add_id);
           $.ajax({
            type    : "POST",
            url     : _base_url + "customer/get_zone_province",
            data    : {area_id : areaId},
            dataType: "text",
            cache   : false,
            success : function (result) {
                var _resp = $.parseJSON(result);
                $('#cust_zone_id').val(_resp.zone_id);
                $('#cust_prov_id').val(_resp.province_id);
		
                
            }
        });
	
});

$('body').on('change', 'input[name="repeat_end"]', function() {
	if($(this).val() == 'ondate')
	{
		$('#repeat-end-date').removeAttr('disabled'); 
	}
	else
	{
		$('#repeat-end-date').attr('disabled', 'disabled'); 
	}
});

$('body').on('click', '#delete-cancel', function() {
	//$('#booking-action-confirm-panel').slideUp(function() { $('#booking-action-confirm-panel').remove(); });
	$.fancybox.close();
});

$('body').on('click', '#delete-one-day', function() {
	var _booking_id = $('#boooking-id').val();
	//alert(_booking_id);
	$.post(_page_url, { action: 'delete-booking-one-day', booking_id: _booking_id }, function(response) {
		alert(response);
		_bpop_open = false;
		//refresh_grid();
		var _alert_html = '';
		if(response == 'success')
		{
			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false
		}
		else
		{
                        if(response == 'locked')
                        {
                                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                _refresh_page = true;
                        }
                        else
                        {
                                _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                _refresh_page = true;
                        }
			
		}
		
		if(_alert_html  != '')
		{
			$.fancybox.open({
				autoCenter : true,
				fitToView : false,
				scrolling : false,
				openEffect : 'fade',
				openSpeed : 100,
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding : 0,
				closeBtn : false,
				content: _alert_html
			});
		}
	});
});

// $('body').on('click', '#delete-permanent', function() {
// 	var _booking_id = $('#boooking-id').val();
// 	$.post(_page_url, { action: 'delete-booking-permanent', booking_id: _booking_id }, function(response) {
// 		_bpop_open = false;
// 		refresh_grid();
// 		var _alert_html = '';
// 		if(response == 'success')
// 		{
// 			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
// 			_refresh_page = false;
// 			window.location.reload();
// 		}
// 		else
// 		{
//                         if(response == 'locked')
//                         {
//                                 _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
//                                 _refresh_page = false;
//                         }
//                         else
//                         {
//                                 _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
//                                 _refresh_page = true;
//                         }
// 		}
		
// 		if(_alert_html  != '')
// 		{
// 			$.fancybox.open({
// 				autoCenter : true,
// 				fitToView : false,
// 				scrolling : false,
// 				openEffect : 'fade',
// 				openSpeed : 100,
// 				helpers : {
// 					overlay : {
// 						css : {
// 							'background' : 'rgba(0, 0, 0, 0.3)'
// 						},
// 						closeClick: false
// 					}
// 				},
// 				padding : 0,
// 				closeBtn : false,
// 				content: _alert_html
// 			});
// 		}
// 	});
// });

// function deletebookingservice(bookingid,bookingtype)
// {
// 	//var bookingid = $(this).attr('rel');
// 	//alert(bookingid);
// 	//var bookingtype = $(this).attr('rel');
// 	//alert(bookingtype);
// 	if(bookingtype == 'OD')
// 	{
// 		$.fancybox.open({
// 			autoCenter : true,
// 			fitToView : false,
// 			scrolling : false,
// 			openEffect : 'fade',
// 			openSpeed : 100,
// 			helpers : {
// 				overlay : {
// 					css : {
// 						'background' : 'rgba(0, 0, 0, 0.3)'
// 					},
// 					closeClick: false
// 				}
// 			},
// 			padding : 0,
// 			closeBtn : false,
// 			content:  '<div id="booking-action-confirm-panel1"><p style="padding: 20px; text-align: center; font-size:16px;">Do you want to delete this booking?</p><input type="hidden" id="boooking-id" value="'+bookingid+'" /><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>'
// 		});
		
		
		
// 		// $('#myModal').modal('show');
// 		// $('#myModal').on('shown.bs.modal', function() {
// 			// $('#myModal').find('.modal-body').append('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
// 		// });
// 		//$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
// 	}
// 	else
// 	{
// 		$.fancybox.open({
// 			autoCenter : true,
// 			fitToView : false,
// 			autoDimensions : true,
// 			scrolling : false,
// 			openEffect : 'fade',
// 			openSpeed : 100,
// 			helpers : {
// 				overlay : {
// 					css : {
// 						'background' : 'rgba(0, 0, 0, 0.3)'
// 					},
// 					closeClick: false
// 				}
// 			},
// 			padding : 0,
// 			closeBtn : false,
// 			content:  '<div id="booking-action-confirm-panel1"><p style="padding: 20px; text-align: center; font-size:16px;">Do you want to delete this booking?</p><input type="hidden" id="boooking-id" value="'+bookingid+'" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>'
// 		});
// 		//$('#booking-action-confirm-panel').slideDown();
		
		
// 		// $('#myModal').modal('show');
// 		// $('#myModal').on('shown.bs.modal', function() {
// 			// $('#myModal').find('.modal-body').append('<div id="booking-action-confirm-panel"><input type="button" id="update-one-day" value="Change One Day" /><input type="button" id="update-permanent" value="Change Permanently" /><input type="button" id="update-cancel" value="Cancel"></div>');
// 		// });
// 		//$(this).parent().before('');
// 	}
// 	//$("#myModal .modal-body").html()
// 	//$('#booking-action-confirm-panel').append('#myModal .modal-body');
// 	//$("#myModal").modal(); 
// 	///console.log(bookingtype);
// }
 
$('body').on('click', '#delete-permanent', function() {
    var _booking_id = $('#boooking-id').val();
$.fancybox.open({
        autoCenter : true,
        fitToView : false,
        scrolling : false,
        openEffect : 'none',
        openSpeed : 1,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.3)'
                },
                closeClick: true
            }
        },
        padding : 0,
        closeBtn : true,
        content : _alert_html = '<div id="alert-popup"><div class="head">Delete<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:20px 0 5px !important;">Are you sure want to continue?</div><div class="content"><span id="deleteremarks_book" style="color:red; display:none;">Please enter remarks.</span><textarea name="remark" placeholder="Remarks" id="remarkss_book"></textarea></div><div class="bottom"><input type="button" value="Yes" data-bookID="'+_booking_id+'" class="delete_yes_book" style="background:#b2d157;border:1px solid" />&nbsp;&nbsp;<input type="button" value="No" class="assign_no pop_close" /></div></div>',
        topRatio : 0.2,
        
    }); 
   
});
$('body').on('click', '.delete_yes_book', function() {
   var remarks = $.trim($('#remarkss_book').val());
    var _booking_id=$(this).attr('data-bookID');


  if(remarks == "")
  {
       $('#deleteremarks_book').css('display','block');
  } else {
       $('#deleteremarks_book').css('display','none');
       $.fancybox.close();
       $.post(_page_url, { action: 'delete-booking-permanent', booking_id: _booking_id ,remarks : remarks }, function(response) {
           _bpop_open = false;
           //refresh_grid();
           var _alert_html = '';
           if(response == 'success')
           {
               _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
               _refresh_page = false;
               window.location.reload();
           }
           else
           {
                           if(response == 'locked')
                           {
                                   _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                   _refresh_page = false;
                           }
                           else
                           {
                                   _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
                                   _refresh_page = true;
                           }
           }
           
           if(_alert_html  != '')
           {
               $.fancybox.open({
                   autoCenter : true,
                   fitToView : false,
                   scrolling : false,
                   openEffect : 'fade',
                   openSpeed : 100,
                   helpers : {
                       overlay : {
                           css : {
                               'background' : 'rgba(0, 0, 0, 0.3)'
                           },
                           closeClick: false
                       }
                   },
                   padding : 0,
                   closeBtn : false,
                   content: _alert_html
               });
           }
       });
  }
});
function deletebookingservice(bookingid,bookingtype)
{
    //var bookingid = $(this).attr('rel');
    //alert(bookingid);
    //var bookingtype = $(this).attr('rel');
    //alert(bookingtype);
    if(bookingtype == 'OD')
    {
        $.fancybox.open({
            autoCenter : true,
            fitToView : false,
            scrolling : false,
            openEffect : 'fade',
            openSpeed : 100,
            helpers : {
                overlay : {
                    css : {
                        'background' : 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding : 0,
            closeBtn : false,
            content:  '<div id="booking-action-confirm-panel1"><p style="padding: 20px; text-align: center; font-size:16px;">Do you want to delete this booking?</p><input type="hidden" id="boooking-id" value="'+bookingid+'" /><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>'
        });
        
        
        
        // $('#myModal').modal('show');
        // $('#myModal').on('shown.bs.modal', function() {
            // $('#myModal').find('.modal-body').append('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
        // });
        //$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
    }
    else
    {
        $.fancybox.open({
            autoCenter : true,
            fitToView : false,
            autoDimensions : true,
            scrolling : false,
            openEffect : 'fade',
            openSpeed : 100,
            helpers : {
                overlay : {
                    css : {
                        'background' : 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding : 0,
            closeBtn : false,
            content:  '<div id="booking-action-confirm-panel1"><p style="padding: 20px; text-align: center; font-size:16px;">Do you want to delete this booking?</p><input type="hidden" id="boooking-id" value="'+bookingid+'" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>'
        });
        //$('#booking-action-confirm-panel').slideDown();
        
        
        // $('#myModal').modal('show');
        // $('#myModal').on('shown.bs.modal', function() {
            // $('#myModal').find('.modal-body').append('<div id="booking-action-confirm-panel"><input type="button" id="update-one-day" value="Change One Day" /><input type="button" id="update-permanent" value="Change Permanently" /><input type="button" id="update-cancel" value="Cancel"></div>');
        // });
        //$(this).parent().before('');
    }
    //$("#myModal .modal-body").html()
    //$('#booking-action-confirm-panel').append('#myModal .modal-body');
    //$("#myModal").modal(); 
    ///console.log(bookingtype);
}
function refresh_grid()
{
	if(_bpop_open != true)
	{
		$.post( _page_url, { action: 'refresh-grid' }, function(response) {
			response = $.parseJSON(response);
			$('#schedule-grid-rows').html(response.grid);
			$('#schedule-report').html(response.report);
			setTimeout('refresh_grid()', 5000);
			if(_bpop_open != true)
			{
				apply_selectable();
			}
		});
	}
	else
	{
		setTimeout('refresh_grid()', 5000);
	}
}

function loadLocationField(id,count)
    {
       
        if(document.getElementById(id).value!=""){
     
        var input = document.getElementById(id);
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
       // document.getElementById(id).value = place.formatted_address;
        $("#map_canvas_"+count).css("height","200px");
        var locationRio = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
        var map = new google.maps.Map(document.getElementById("map_canvas_"+count), {
          zoom: 15,
          center: locationRio,
         
        });
    
      document.getElementById("latitude_"+count).value = place.geometry.location.lat();
      document.getElementById("longitude_"+count).value = place.geometry.location.lng();
      
      var marker = new google.maps.Marker({
       map: "map_canvas_"+count,
       draggable: true,
       animation: google.maps.Animation.DROP,
       position: locationRio,
      

  });
   marker.setMap(map);
        
        //console.log(place.geometry.location.lat());
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
           
          if (componentForm[addressType]) {
             
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(id).value = val;

             
          }
        }
       
       
        });

    }
    else
    {
      $("#map_canvas_"+count).html('');
      document.getElementById("latitude_"+count).value ='';
      document.getElementById("longitude_"+count).value ='';
    }
   
    }
  


function edit_hourly_prices(ps_id)
{ 
   $.ajax({
       type: "POST",
       url: _base_url + "settings/edit_hourly_price",
       data: {ps_id: ps_id},
       dataType: "text",
       cache: false,
       success: function (result) {
//   alert(result);
           var obj = jQuery.parseJSON(result);
           $.each($.parseJSON(result), function (edit, value) {
//    alert('ADFD'+value.ps_id);
           $('#edit_price').show();
               $('#edit_ps_id').val(value.ps_id);
               $('#edit_hourly_rate').val(value.price);
//var res = value.hours.split("-");
               $('#edit_from_hrly_name').val(value.from_hr);
               $('#edit_to_hrly_name').val(value.to_hr);
           });

           $('#add_hourly_price').hide();

       }
   });
}


function delete_hourly_price(service_id)
{
    if (confirm('Are you sure you want to delete this hourly charge'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_hrly_charge",
            data: {ps_id: service_id},
            dataType: "text",
            cache: false,
            success: function (result) {
                window.location.assign(_base_url + 'payment-settings');
            }
        });
    }
}
function add_hourly_price()
{
    $('#add_hourly_price').show();
    $('#edit_price').hide();
}

function hideadd_hrly_price()
{
    $('#add_hourly_price').hide();
}

function hideedit_hrly_price()
{
    $('#edit_price').hide();
}


$('body').on('change', '#from_hrly_name', function () {
    $("#to_hrly_name").html('');
    var _selected_index = $("#from_hrly_name")[0].selectedIndex;

    var _time_to_options;
    var _i = 0;
    var _last_index;
    $('#from_hrly_name option').each(function (index, option) {
        if (index > _selected_index)
        {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
            _last_index = index;
            _i++;
        }
    });


    if (_i == 0)
    {
        _time_to_options += '<option value="">No Time</option>';
    }

    $('#to_hrly_name').html(_time_to_options);
 
});

$('body').on('change', '#edit_from_hrly_name', function () {
    $("#edit_to_hrly_name").html('');
    var _selected_index = $("#edit_from_hrly_name")[0].selectedIndex;

    var _time_to_options;
    var _i = 0;
    var _last_index;
    $('#edit_from_hrly_name option').each(function (index, option) {
        if (index > _selected_index)
        {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
            _last_index = index;
            _i++;
        }
    });


    if (_i == 0)
    {
        _time_to_options += '<option value="">No Time</option>';
    }

    $('#edit_to_hrly_name').html(_time_to_options);
 
});
$('body').on('click', '#clickfirst', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #thirdli').removeClass('active');
    $('#mytabs #secondli').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#account').removeClass('active').css('display','none');
    $('#address-details').addClass('active').css('display','block');
    //$('#mytabs a[href="#address-details"]').tabs('show');
});

$('body').on('click', '#clicksecond', function () {
    $('#mytabs #secondli').removeClass('active');
    $('#mytabs #thirdli').addClass('active');
    $('#mytabs #firstli').removeClass('active');
    $('#address-details').removeClass('active').css('display','none');
    $('#personal').removeClass('active').css('display','none');
    $('#account').addClass('active').css('display','block');
    //$('#mytabs a[href="#account"]').tabs('show');
});
$('body').on('click', '#firstli', function () {
    $('#mytabs #firstli').addClass('active');
    $('#mytabs #thirdli').removeClass('active');
    $('#mytabs #secondli').removeClass('active');
    
    $('#personal').addClass('active').css('display','block');
    $('#account').removeClass('active').css('display','none');
    $('#address-details').removeClass('active').css('display','none');
});
$('body').on('click', '#secondli', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #thirdli').removeClass('active');
    $('#mytabs #secondli').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#account').removeClass('active').css('display','none');
    $('#address-details').addClass('active').css('display','block');
});
$('body').on('click', '#thirdli', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #thirdli').addClass('active');
    $('#mytabs #secondli').removeClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#account').addClass('active').css('display','block');
    $('#address-details').removeClass('active').css('display','none');
});


 function edit_sms_settings(ps_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_sms_settings",
        data: {sms_id: ps_id},
        dataType: "text",
        cache: false,
        success: function (result) {
//            alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_sms_id').val(value.id);
                $('#edit_sender_id').val(value.sender_id);
                $('#edit_api').val(value.api_url);
                $('#edit_user').val(value.user);
                $('#edit_pass').val(value.pass);

            });

//            $('#add_hourly_price').hide();
            $('#edit_sms_setting').show();

        }
    });
}

function hideedit_sms_setting()
{
    $('#edit_sms_setting').hide();
}
function hideedit_email_setting()
{
    $('#edit_email_setting').hide();
}
function hideedit_tax_setting()
{
    $('#edit_tax_setting').hide();
}

function edit_email_settings(ps_id)
{
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_email_settings",
        data: {e_id: ps_id},
        dataType: "text",
        cache: false,
        success: function (result) {
//            alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_email_id').val(value.id);
                $('#edit_header_name').val(value.header_name);
                $('#edit_subject').val(value.subject);
                $('#edit_from').val(value.from_address);

            });

//            $('#add_hourly_price').hide();
            $('#edit_email_setting').show();

        }
    });
}

//Add Complaint
function addcomplaint(bookingid,date_new)
{
    $.fancybox.open({
        autoCenter : true,
        fitToView : false,
        scrolling : false,
        openEffect : 'none',
        openSpeed : 1,
        helpers : {
                overlay : {
                        css : {
                                'background' : 'rgba(0, 0, 0, 0.3)'
                        },
                        closeClick: true
                }
        },
        padding : 0,
        closeBtn : true,
        content: _alert_html = '<form id="myForm" enctype="multipart/form-data"><div id="alert-popup"><div class="col-md-12 col-sm-12 green-popup-head"><span id="b-maid-name">Add Complaint</span><span class="pop_close n-close-btn">&nbsp;</span></div><div class="col-sm-12" style="padding: 20px 30px;"><div class="row m-0 n-field-main"><span id="addcompliants" style="color:red; display:none; padding-bottom: 10px;">Please fill all fields.</span><p>Complaint Type</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="type" placeholder="Enter Complaint" id="type" onChange=showHide()><option value="">Select Complaint Type</option><option value="service quality">Service quality</option><option value="damage">Damage</option><option value="theft or missing">Theft/Missing</option><option value="no show">No show</option><option value="lateness">Lateness</option><option value="other" id="other">Other</option><span id="" style="color:red; display:none; padding-bottom: 10px;"><textarea style="width:100%;display:none" name="othercomment" id="othercomment" placeholder=""></textarea></span></br></select></div></div><div class="row m-0 n-field-main"><p>Complaint or Customer Feedback</p><div class="col-sm-12 p-0 n-field-box"><textarea style="width:100%;" name="complaints" placeholder="" id="complaintss"></textarea></div></div><div class="row m-0 n-field-main"><p>Cleaner / Driver / Staff Comments</p><div class="col-sm-12 p-0 n-field-box"><textarea style="width:100%;" name="cleanercmt" id="cleanercmt" placeholder=""></textarea></div></div><div class="row m-0 n-field-main"><span id="adddeduction" style="color:red; display:none; padding-bottom: 10px;">Please enter digits only.</span><p>Deduction Amount</p><div class="col-sm-12 p-0 n-field-box"><input type="text" style="width:100%;height:30px;" name="deduction" placeholder="" id="deduction"/></div></div><div class="row m-0 n-field-main"><p>Select Image</p><div class="col-sm-12 p-0 n-field-box"><input type="file" style="width:100%;opacity: 1;" name="image" id="image"/></div></div><div class="row m-0 n-field-main pb-4"><p>Complaint Against</p><div class="col-sm-12 p-0 n-field-box"> <div class="row m-0"> <div class="col-sm-4 n-field-box p-0"> <div class="n-end n-pick-maids-set"> <input id="cleaner" name="role" type="radio" value="cleaner" class="" onclick="inputSelect(this.value)"> <label for="cleaner"> <span class="border-radius-3"></span><p>Cleaner</p></label> </div> </div> <div class="col-sm-4 n-field-box p-0"> <div class="n-end n-pick-maids-set"> <input id="driver" name="role" type="radio" value="driver" class="" onclick="inputSelect(this.value)"> <label for="driver"> <span class="border-radius-3"></span><p>Driver</p></label> </div> </div> <div class="col-sm-4 n-field-box p-0"> <div class="n-end n-pick-maids-set"> <input id="user" name="role" type="radio" value="user" class="" onclick="inputSelect(this.value)"> <label for="user"> <span class="border-radius-3"></span><p>User</p></label> </div> </div> </div></div></div><div class="row m-0 n-field-main" id="staff-dropdown" style="display:none;"><p>Select Driver Name</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="staff_name" placeholder="" id="staff_name" class="form-control select2" ><option value="">Select Driver Name</option></select></div></div><div class="row m-0 n-field-main" id="cleaner-dropdown" style="display: none;"><p>Select Cleaner Name</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="cleaner_name" placeholder="Select Cleaner Name" id="cleaner_name"><option value="">Select Cleaner Name</option></select></div></div><div class="row m-0 n-field-main" id="user-dropdown" style="display: none;"><p>Select User Name</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="user_name" placeholder="Select User Name" id="user_name"><option value="">Select User Name</option></select></div></div><div class="row m-0 n-field-main"><div class="col-sm-12 p-0 "><input type="button" value="submit" data-id="' + date_new + '" data-bookID="' + bookingid +'" class="n-btn mb-0 complaint_yes" /></div></div></div></div></form>',
        topRatio : 0.2,

    }); 
}


function send_delay_sms(mob,time)
{

    
    $.fancybox.open({
        autoCenter : true,
        fitToView : false,
        scrolling : false,
        openEffect : 'none',
        openSpeed : 1,
        helpers : {
                overlay : {
                        css : {
                                'background' : 'rgba(0, 0, 0, 0.3)'
                        },
                        closeClick: true
                }
        },
        padding : 0,
        closeBtn : true,
        content : _alert_html = '<div id="alert-popup"><div class="head">Send SMS<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:0px !important;"></div><div class="content" style="padding:20px 20px 20px 20px;"><span id="sendsms" style="color:red; display:none; padding-bottom: 10px;">Send SMS.</span><textarea style="width:100%;height:100px;" name="sms_msg" placeholder="Enter message" id="sms_msg">We are expecting a small delay for dropping the cleaner.The cleaner will reach your place at approximately '+ time + '. For further assistance please call 800 258. Thank You</textarea></div><div class="bottom"><input type="button" value="Submit" data-mobid="'+mob+'" class="report_sms" style="background:#b2d157;border:1px solid" /><p id="msg_success" style="text-align:center;"></p></div></div>',
        topRatio : 0.2,

    }); 
}
$(document).on('click', '.report_sms', function(){
$("#msg_success").text("");
 var mob=$(this).attr("data-mobid");
 var sms_message=$("#sms_msg").val();
 if(mob!="" && sms_message!="")
 {
    $('.mm-loader').css('display','block');
    $.post(_page_url, {action : 'sms_send', mobile : mob, message :sms_message}, function(response){
            
            if(response==1)
            {
             $('.mm-loader').css('display','none');  
             $("#msg_success").text("Message has been sent !").css("color","green");       
               
            }

      
    });
 }


});

$(document).on('click', '.complaint_yes', function(){
   $('.mm-loader').css('display','block');
   var complaints = $.trim($('#complaintss').val());
    var type = $.trim($('#type').val());
     var comments = $.trim($('#comments').val());
     var cleaner = $.trim($('#cleanercmt').val());
     var othercomment = $.trim($('#othercomment').val());
     var deduction = $.trim($('#deduction').val());
     var radioVal = $('input[name="role"]:checked').val();
     var selectedCleanerName = $('#cleaner_name').val();
     var selectedStaffName = $('#staff_name').val();
     var selectedUserName = $('#user_name').val();
     var cname = $('#cleaner_name option:selected').text();
     var dname = $('#staff_name option:selected').text();
     var uname = $('#user_name option:selected').text();

	

   if(complaints == "")
   {
        $('.mm-loader').css('display','none');
        $('#addcompliants').css('display','block');
   } 
	else if(isNaN(deduction)){
	 $('.mm-loader').css('display','none');
        $('#adddeduction').css('display','block');
	}
	else {
        $('#addcompliants').css('display','none');
        $.fancybox.close(); 
        var s_date = $(this).attr('data-id');
        var BookId = $(this).attr('data-bookID');
 		var formData = new FormData()
	formData.append('action', 'add-complaint-by-id');
	formData.append('booking_id', BookId);
        formData.append('service_date', s_date);
        formData.append('complaint', complaints);
        formData.append('complaint_type', type);
        formData.append('customer_comments', comments);
        formData.append('cleaner_comments', cleaner);
        formData.append('other_comment', othercomment);
	formData.append('deduction', deduction);
	var image = document.getElementById('image');
	formData.append('image', image.files[0]);
	formData.append('radio_button_value', radioVal);
	formData.append('CleanerName', selectedCleanerName);
	formData.append('DriverName', selectedStaffName);
	formData.append('CustomerName', selectedUserName);
	formData.append('cname', cname);
	formData.append('dname', dname);
	formData.append('uname', uname);
	$.ajax({
        	url: _base_url + "activity/jobs",
        	type: 'POST',
        	data: formData,
        	processData: false,
        	contentType: false,
        	success: function(response) {
            		var _resp = $.parseJSON(response);
 if(_resp.status == 'success')
            {
                $('.'+BookId+'-'+s_date+'.complaintclass').html('');
                $('.'+BookId+'-'+s_date+'complaintclass').html('<span class="btn edit-complaint-job" onclick="editcomplaint('+_resp.complaint_id+')"><i class="fa fa-pencil"></i></span><span class="btn view-complaint-job" onclick="viewcomplaint('+_resp.complaint_id+')"><i class="fa fa-eye"></i></span>');
                $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Success<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Complaint added successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });
                $('.mm-loader').css('display','none');
            }
            else
            {
                $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Some thing went wrong. Please try again...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });
                $('.mm-loader').css('display','none');
            }

        	}
	});



   
        //$.post(_page_url, {action : 'add-complaint-by-id', booking_id : BookId, service_date : s_date, complaint : complaints, complaint_type : type, customer_comments : comments, cleaner_comments : cleaner, other_comment : othercomment}, function(response){
           // var _resp = $.parseJSON(response);
           // if(_resp.status == 'success')
           // {
              //  $('.'+BookId+'-'+s_date+'.complaintclass').html('');
              //  $('.'+BookId+'-'+s_date+'complaintclass').html('<span class="btn edit-complaint-job" onclick="editcomplaint('+_resp.complaint_id+')"><i class="fa fa-pencil"></i></span><span class="btn view-complaint-job" onclick="viewcomplaint('+_resp.complaint_id+')"><i class="fa fa-eye"></i></span>');
                
                
                /*$.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Success<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Complaint added successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });*/
               // $('#alert-title').html('Success !');
               // $('#alert-message').html('Complaint added successfully.');
              //  fancybox_show('alert-popup');
              //  $('.mm-loader').css('display','none');
          //  }
          //  else
           // {
                /*$.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Some thing went wrong. Please try again...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });*/
               // $('#alert-title').html('Error !');
               // $('#alert-message').html('Some thing went wrong. Please try again...');
              //  fancybox_show('alert-popup');
              //  $('.mm-loader').css('display','none');
           // }
      //  });
    }
});

//View Complaint
function viewcomplaint(complaint_id)
{
    $('.mm-loader').css('display','block');
    $.post(_page_url, {action : 'view-complaint-by-id', complaint_id : complaint_id}, function(response){
        var _resp = $.parseJSON(response);
        if(_resp.status == 'success')
        {
            $.fancybox.open({
                autoCenter : true,
                fitToView : false,
                scrolling : false,
                openEffect : 'none',
                openSpeed : 1,
                helpers : {
                        overlay : {
                                css : {
                                        'background' : 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: true
                        }
                },
                padding : 0,
                closeBtn : true,
                content : _alert_html = '<div id="alert-popup"><div class="head">Complaint for the job<span class="alert-popup-close pop_close2"></span></div><div class="content padd20" style="text-align:left;">Complaint or Customer Feedback:- '+_resp.complaint+'<br></div><div class="content padd20" style="text-align:left;">Complaint Type:- '+_resp.complaint_type+'<br></div><div class="content padd20" style="text-align:left;">Other Complaint Type:- '+_resp.complaint_other_comment+'<br></div><div class="content padd20" style="text-align:left;">Cleaner/Driver/Staff comments:- '+_resp.complaint_cleaner_comments+'<br></div><div class="content padd20" style="text-align:left;">Status:- '+_resp.statuss+'<br></div><div class="content padd20" style="text-align:left;">Action taken:- '+_resp.action+'<br></div><div class="content padd20" style="text-align:left;">Deduction Amount:- '+_resp.deduction_amount+'<br></div><div class="content padd20" style="text-align:left;">Image:-<a href="'+_resp.image+'" target="_blank"> '+_resp.image+'</a><br></div><div class="content padd20" style="text-align:left;">Complaint Against:- '+_resp.complaint_against+'<br></div><div class="content padd20" style="text-align:left;">Name:- '+_resp.name+'<br></div><div class="pull-right"><br><i class="fa fa-user"> </i>'+_resp.added_by+'<br><i style="font-size:10px;">'+_resp.added_date+'  '+_resp.added_time+'</i><br><br></div> <div class="bottom"></div></div></div>',
                topRatio : 0.2,

            });
            $('.mm-loader').css('display','none');
        } else {
            $.fancybox.open({
                autoCenter : true,
                fitToView : false,
                scrolling : false,
                openEffect : 'none',
                openSpeed : 1,
                helpers : {
                        overlay : {
                                css : {
                                        'background' : 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: true
                        }
                },
                padding : 0,
                closeBtn : true,
                content : _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Some thing went wrong. Please try again...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                topRatio : 0.2,

            });
            $('.mm-loader').css('display','none');
        }
    });
}

//Edit Complaint
function editcomplaint(complaint_id)
{
   // $('.mm-loader').css('display','block');
    $.post(_page_url, {action : 'edit-complaint-by-id', complaint_id : complaint_id}, function(response){
        var _resp = $.parseJSON(response);
	
        if(_resp.status == 'success')
        {

                $.fancybox.open({
                autoCenter : true,
                fitToView : false,
                scrolling : false,
                openEffect : 'none',
                openSpeed : 1,
                helpers : {
                        overlay : {
                                css : {
                                        'background' : 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: true
                        }
                },
                padding : 0,
                closeBtn : true,
                content : _alert_html = '<form id="myForm" enctype="multipart/form-data"><div id="alert-popup"><div class="head">Edit Complaint<span class="alert-popup-close pop_close"></span></div><div class="content padd20" style="padding:0px !important;"></div><div class="content" style="padding:20px 20px 20px 20px;"><span id="editcompliants" style="color:red; display:none; padding-bottom: 10px;">Please fill all fields.</span><label>Select Complaint Type</label><select style="width:100%;" name="type" placeholder="" onChange=showHide() id="type" required><option>'+_resp.complaint_type+'</option><option value="Service quality">Service Quality</option><option value="Damage">Damage</option><option value="Theft or missing"> Theft or Missing</option><option value="No Show">No Show</option><option value="Lateness">Lateness</option><option value="other">other</option><textarea style="width:100%;display:none;" name="othercomment" id="othercomment">'+_resp.complaint_other_comment+'</textarea></select><label>Complaint or Customer Feedback</label><textarea style="width:100%;" name="edit-complaints" placeholder="Enter Complaint" id="edit-complaintss">'+_resp.complaint+'</textarea><label>Cleaner/Driver/Staff Comments</label><textarea style="width:100%;" name="edit-cleaner" id="edit-cleaner">'+_resp.complaint_cleaner_comments+'</textarea><label>Select Status</label><select name="statuss" id="statuss" style="width:100%;"><option>'+_resp.statuss+'</option><option value="Pending">Pending</option><option value="Investigating">Investigating</option><option value="Resolved">Resolved</option><option value="Invalid Complaint">Invalid Complaint</option></select><label>Select Action Taken</label><select style="width:100%;" name="actionn" id="actionn"><option>'+_resp.action+'</option><option value="No Action needed">No Action Needed</option><option value="Deduction">Deduction</option><option value="Warning">Warning</option><option value="Retouch">Retouch</option><option value="Retraining">Retraining</option></select><span id="editded" style="color:red; display:none; padding-bottom: 10px;">Please enter digits only.</span><label>Deduction Amount</label><input type="text" style="width:100%;" name="deduction" placeholder="Enter Deduction Amount" id="deduction"  value="'+_resp.deduction_amount+'"/><label> Select Image</label><p>previous upload:</p><a href="'+_resp.image+'"target="_blank">'+_resp.image+'</a> <input type="file" style="width:100%;opacity: 1;" name="image"  id="image"/><label>Complaint Against</label><p><input type="radio" name="role" id="cleaner" value="cleaner" onclick="inputSelect(this.value)"> Cleaner</p><br><p><input name="role" type="radio"  value="driver" id="driver" onclick="inputSelect(this.value)">Driver</p><br><p><input type="radio" name="role" id="user" value="user" onclick="inputSelect(this.value)" >User</p><div class="row m-0 n-field-main" id="staff-dropdown" style="display:none;"><p>Select Driver Name</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="staff_name" placeholder="Select Driver Name" id="staff_name"><option value="">Select Driver Name</option> </select></div></div><div class="row m-0 n-field-main" id="cleaner-dropdown" style="display: none;"><p>Select Cleaner Name</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="cleaner_name" placeholder="Select Cleaner Name" id="cleaner_name"> <option value="">Select Cleaner Name</option></select></div></div><div class="row m-0 n-field-main" id="user-dropdown" style="display: none;"><p>Select User Name</p><div class="col-sm-12 p-0 n-field-box"><select style="width:100%;" name="user_name" placeholder="Select User Name" id="user_name"> <option value="">Select User Name</option></select></div></div><div class="bottom"><input type="button" value="Submit" data-id="'+complaint_id+'" class="complaint_edit_yes" style="background:#b2d157;border:1px solid" /></div></div></div></form>',
    topRatio : 0.2,

            });
	if(_resp.complaint_against === "user") {
	 $('#user').prop('checked', true);
	  $('#user-dropdown').css('display', 'block');
           $.ajax({
            url: _base_url + "activity/get_users", 
           type: "GET",
           dataType: "json",
           success: function(data) {
               var options = '<option value="">Select User</option>';
               $.each(data, function(i, staff) {
 		var selected = "";
   		 if (staff.customer_id === _resp.cid) {
      		selected = "selected";
   		 }
    		options += '<option value="' + staff.customer_id + '" ' + selected + '>' + staff.customer_name + '</option>';
 	 	});
                 $('#user_name').html(options);
       
               // display the staff dropdown
		$('#staff-dropdown').css('display', 'none');
                $('#cleaner-dropdown').css('display', 'none');
                $('#user-dropdown').css('display', 'block');
		 $('input[type=radio][name=role]').not('#'+value).prop('checked', false);
             },
           error: function(xhr, status, error) {
             console.log("Error: " + error);
           }
         });
	}
	if(_resp.complaint_against === "driver") {
	 $('#driver').prop('checked', true);
	$('#staff-dropdown').css('display', 'block');
	$.ajax({
         url: _base_url + "activity/get_drivers", 
        type: "GET",
        dataType: "json",
        success: function(data) {
            var options = '<option value="">Select Driver</option>';
            $.each(data, function(i, staff) {
	    var selected = "";
   		 if (staff.tablet_id === _resp.cid) {
      		selected = "selected";
   		 }
    		options += '<option value="' + staff.tablet_id + '" ' + selected + '>' + staff.tablet_driver_name + ' - ' + staff.zone_name  + '</option>';
 	 	});        
            $('#staff_name').html(options);
    
            // display the staff dropdown
	     $('#cleaner-dropdown').css('display', 'none');
             $('#user-dropdown').css('display', 'none');
             $('#staff-dropdown').css('display', 'block');
	       $('input[type=radio][name=role]').not('#'+value).prop('checked', false);
          },
        error: function(xhr, status, error) {
          console.log("Error: " + error);
        }
      });

	}
	if(_resp.complaint_against === "cleaner") {
	 $('#cleaner').prop('checked', true);
	$('#cleaner-dropdown').css('display', 'block');
        $.ajax({
        url: _base_url + "activity/get_cleaners", 
       type: "GET",
       dataType: "json",
       success: function(data) {
           var options = '<option value="">Select Cleaner</option>';
           $.each(data, function(i, staff) {
           var selected = "";
   		 if (staff.maid_id === _resp.cid) {
      		selected = "selected";
   		 }
    		options += '<option value="' + staff.maid_id + '" ' + selected + '>' + staff.maid_name +  '</option>';
 	 	});
           $('#cleaner_name').html(options);
   
           // display the staff dropdown
	     $('#staff-dropdown').css('display', 'none');
            $('#user-dropdown').css('display', 'none');
            $('#cleaner-dropdown').css('display', 'block');
	     $('input[type=radio][name=role]').not('#'+value).prop('checked', false);
         },
       error: function(xhr, status, error) {
         console.log("Error: " + error);
       }
     });
	}


            $('.mm-loader').css('display','none');
        }
    });
}

$(document).on('click', '.complaint_edit_yes', function(){
  // $('.mm-loader').css('display','block');
   var complaints = $.trim($('#edit-complaintss').val());
   var complaints_type = $.trim($('#type').val());
   var customer = $.trim($('#edit-comments').val());
   var cleaner = $.trim($('#edit-cleaner').val());
   var othercomment = $.trim($('#othercomment').val());
   var statuss = $.trim($('#statuss').val());
   var actionn = $.trim($('#actionn').val());
   var maidname = $.trim($('#maidname').val());
   var deduction = $.trim($('#deduction').val());
   var radioVal = $('input[name="role"]:checked').val();
   var selectedCleanerName = $('#cleaner_name').val();
   var selectedStaffName = $('#staff_name').val();
   var selectedUserName = $('#user_name').val();
   var cname = $('#cleaner_name option:selected').text();
   var dname = $('#staff_name option:selected').text();
   var uname = $('#user_name option:selected').text();	

   if(complaints == "")
   {
        $('.mm-loader').css('display','none');
        $('#editcompliants').css('display','block');
   }
  else if(isNaN(deduction)){
	 $('.mm-loader').css('display','none');
        $('#editded').css('display','block');
	}
  else {
        $('#editcompliants').css('display','none');
        $.fancybox.close();
        var complaintids = $(this).attr('data-id');
        var formData = new FormData()
	formData.append('action', 'update-complaint-by-id');
	formData.append('complaint_id', complaintids);
        formData.append('complaints_type', complaints_type);
        formData.append('complaint', complaints);
        formData.append('customer_comments', customer);
        formData.append('cleaner_comments', cleaner);
        formData.append('other_comment', othercomment);
	formData.append('statuss', statuss );
	formData.append('actionn', actionn );
	formData.append('maidname', maidname );
   	formData.append('deduction', deduction );
	var image = document.getElementById('image');
	formData.append('image', image.files[0]);
	formData.append('radio_button_value', radioVal);
	formData.append('CleanerName', selectedCleanerName);
	formData.append('DriverName', selectedStaffName);
	formData.append('CustomerName', selectedUserName);
	formData.append('cname', cname);
	formData.append('dname', dname);
	formData.append('uname', uname);
	$.ajax({
        	url: _base_url + "activity/jobs",
        	type: 'POST',
        	data: formData,
        	processData: false,
        	contentType: false,
        	success: function(response) {
            		var _resp = $.parseJSON(response);
			 if(_resp.status == 'success')
                         {
               
                   $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Success<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Complaint updated successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });
                $('.mm-loader').css('display','none');
            }
            else
            {
                $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'none',
                    openSpeed : 1,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                            }
                    },
                    padding : 0,
                    closeBtn : true,
                    content : _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Some thing went wrong. Please try again...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                    topRatio : 0.2,

                });
                $('.mm-loader').css('display','none');
            }

           }
    })
}
 });


	// $.post(_page_url, {action : 'update-complaint-by-id', complaint : complaints, complaint_id : complaintids, complaints_type : complaints_type, customer_comments :customer, cleaner_comments : cleaner, other_comment : othercomment, statuss : statuss, actionn : actionn, maidname : maidname }, function(response){
          //  var _resp = $.parseJSON(response);
          //  if(_resp.status == 'success')
           // {
                //$('.complaintclass').html('');
                //$('.'+BookId+'complaintclass').html('<span class="btn edit-complaint-job" onclick="editcomplaint('+_resp.complaint_id+')"><i class="fa fa-pencil"></i></span><span class="btn view-complaint-job" onclick="viewcomplaint('+_resp.complaint_id+')"><i class="fa fa-eye"></i></span>');
               // $.fancybox.open({
                 //   autoCenter : true,
                 //   fitToView : false,
                 //   scrolling : false,
                 //   openEffect : 'none',
                  //  openSpeed : 1,
                  //  helpers : {
                        //    overlay : {
                                   // css : {
                                          //  'background' : 'rgba(0, 0, 0, 0.3)'
                                   // },
                                  //  closeClick: true
                           // }
                   // },
                   // padding : 0,
                   // closeBtn : true,
                   // content : _alert_html = '<div id="alert-popup"><div class="head">Success<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Complaint updated successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                  //  topRatio : 0.2,

                //});
              //  $('.mm-loader').css('display','none');
          //  }
          //  else
           // {
             //   $.fancybox.open({
               //     autoCenter : true,
                   // fitToView : false,
                  //  scrolling : false,
                  //  openEffect : 'none',
                  //  openSpeed : 1,
                  //  helpers : {
                          //  overlay : {
                                  //  css : {
                                         //   'background' : 'rgba(0, 0, 0, 0.3)'
                                  //  },
                                  //  closeClick: true
                         //   }
                  //  },
                   // padding : 0,
                   // closeBtn : true,
                   // content : _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Some thing went wrong. Please try again...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                   // topRatio : 0.2,

               // });
               // $('.mm-loader').css('display','none');
           // }

        //});
    //}
//});

function edit_tax_settings(tx_id)
{
    $('.mm-loader').css('display','block');
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_tax_settings",
        data: {t_id: tx_id},
        dataType: "text",
        cache: false,
        success: function (result) {
//            alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_taxt_id').val(value.tax_id);
                $('#edit_percentage').val(value.percentage);
            });

//            $('#add_hourly_price').hide();
            $('#edit_tax_setting').show();
            $('.mm-loader').css('display','none');
        }
    });
}

$("#invoiceaddform").validate({
        rules: {
                invoiceissuedate: "required",
                invoiceduedate: "required"
        },
        messages: {
                invoiceissuedate: "Please enter invoice issue date",
                invoiceduedate: "Please enter invoice due date"
        }
});

$(document).on('change', '#tabletstatus', function(){
	var status = $(this).val();
	if(status != "")
	{
		window.location = _base_url + 'tablets/' + status;
	} else {
		window.location = _base_url + 'tablets';
	}
});



function showHide() {
    let travelhistory = document.getElementById('type')
    if (travelhistory.value == 'other') {
        document.getElementById('othercomment').style.display = 'block'
    } else {
        document.getElementById('othercomment').style.display = 'none'
    }
}
$('.sel2').select2({ dropdownAutoWidth: true, width: 'resolve' });



function inputSelectt(value)
      {
       if(document.getElementById("cleaner").checked===true)
       {
           $('#cleaner-dropdown').css('display', 'block');
       }
      }

function inputSelect(value) {

    if (document.getElementById("driver").checked === true) {
      //alert("You clicked on Driver radio button");
  
     
$.ajax({
    url: _base_url + "activity/get_drivers", 
    type: "GET",
    dataType: "json",
    success: function(data) {
        var options = '<option value="">Search Driver</option>';
        //options += '<option value="" disabled>Search Driver</option>';
        $.each(data, function(i, staff) {
            options += '<option value="' + staff.tablet_id + '">' + staff.tablet_driver_name + ' - ' + staff.zone_name   + '</option>';
        });
        $('#staff_name').html(options);
        
        // display the staff dropdown
        $('#cleaner-dropdown').css('display', 'none');
        $('#user-dropdown').css('display', 'none');
        $('#staff-dropdown').css('display', 'block');
        $('input[type=radio][name=role]').not('#'+value).prop('checked', false);
        
        // Add search functionality to the dropdown
       $('.search-box').select2({
                placeholder: 'Search Driver',
                allowClear: true,
         
                ajax: {
                    url: _base_url + 'activity/search_drivers',
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
    },
    error: function(xhr, status, error) {
        console.log("Error: " + error);
    }
});
}
   
  
else if (value === 'cleaner') {
  //alert("You clicked on Cleaner radio button");
$.ajax({
        url: _base_url + "activity/get_cleaners", 
       type: "GET",
       dataType: "json",
       success: function(data) {
           var options = '<option value="">Select Cleaner</option>';
           $.each(data, function(i, staff) {
             options += '<option value="' + staff.maid_id + '">' + staff.maid_name + '</option>';
           });
           $('#cleaner_name').html(options);
   
           // display the staff dropdown
	     $('#staff-dropdown').css('display', 'none');
            $('#user-dropdown').css('display', 'none');
            $('#cleaner-dropdown').css('display', 'block');
	     $('input[type=radio][name=role]').not('#'+value).prop('checked', false);
         },
       error: function(xhr, status, error) {
         console.log("Error: " + error);
       }
     });
}
else if (value === 'user') {
  //alert("You clicked on User radio button");
  $.ajax({
            url: _base_url + "activity/get_users", 
           type: "GET",
           dataType: "json",
           success: function(data) {
               var options = '<option value="">Select User</option>';
               $.each(data, function(i, staff) {
                 options += '<option value="' + staff.customer_id + '">' + staff.customer_name + '</option>';
               });
               $('#user_name').html(options);
       
               // display the staff dropdown
		$('#staff-dropdown').css('display', 'none');
                $('#cleaner-dropdown').css('display', 'none');
                $('#user-dropdown').css('display', 'block');
		 $('input[type=radio][name=role]').not('#'+value).prop('checked', false);
             },
           error: function(xhr, status, error) {
             console.log("Error: " + error);
           }
         });
}
else {
      
  
    }
  }









