/************************************************************************** */
/*                                                                          */
/*                                                                          */
/************************************************************************** */
// logic goes here...................
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#tb-slide-right').click(function () {
    var position = $('.time_line .time_slider').position();
    var max_left = time_grid_width - 1739 + 3 + 5; //1342
    var left_pos = position.left - 71;
    left_pos = left_pos < max_left ? max_left : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -left_pos + 71 + 'px',
    }, 71);

    if ($(window).width() <= 1600) {
        var max_left = time_grid_width - 1875 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        
    }
    if ($(window).width() <= 1366) {
        var max_left = time_grid_width - 1795 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        

    }
    if ($(window).width() <= 1280) {
        var max_left = time_grid_width - 1645 + 3 + 5;
        //var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
        

    }
});

$('#tb-slide-left').click(function () {
    var position = $('.time_line .time_slider').position();
    var left_pos = position.left + 71;
    left_pos = left_pos > 0 ? 0 : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    //$('.maid_name').css('padding-left', -position.left + 'px');
    position.left = position.left == 0 ? -71 : position.left;
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -position.left + 'px',
    }, 71);
});
// $('.selector').selectable({
//     cancel: ".booked",
//     //distance: 1,
//     start: function (event, ui) {
//         $('.ui-selected').removeClass('ui-selected');
//     },
    // stop: function (event, ui) {
    //     var maid_id = $(this).attr('data-maid');
    //     var maid_name = $(this).attr('data-maid-name');
    //     var week_day = $(this).attr('data-week');
    //     var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
    //     var time_from = $('.ui-selected').first().attr('data-time-from');
    //     var time_to = $('.ui-selected').last().attr('data-time-to');
    //     $('#new_booking_form input[name="service_start_date"]').datepicker('update', new Date());
    //     $('#new_booking_form input[name="service_end_date"]').val("").datepicker('setStartDate', moment().add(7 * 3, 'days').format('DD/MM/YYYY'));
    //     $('#new_booking_form input[name="service_week_day"]').val(week_day);
    //     $('#new_booking_form input[name="service_week_name"]').val(week_name);
    //     $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    //     $('#new_booking_form input[name="maid_id"]').val(maid_id);
    //     $('#new_booking_form input[name="maid_name"]').val(maid_name);
    //     $('#new_booking_form select[name="time_from"]').select2().val(time_from).trigger("change.select2");
    //     $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(time_from)).select2().val(time_to).trigger("change.select2");
    //     $('#new_booking_form select[name="customer_id"]').val("").trigger("change.select2");
    //     $('#new_booking_form select[name="service_type_id"]').select2().val(6).trigger("change.select2");
    //     $('#new_booking_form input[name="cleaning_materials"]').prop('checked', false);
    //     $('#new_booking_form input[name="booking_id"]').val(""); // important
    //     $('#new_booking_form input[name="service_rate_per_hour"]').val("");
    //     $('#new_booking_form input[name="service_discount_rate_per_hour"]').val("");
    //     $('#new_booking_form .vat_percentage').html(service_vat_percentage.toFixed(2));
    //     $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val(10);
    //     $('#new_booking_form select[name="customer_id"]');
    //     $('#new_booking_form .modal-footer :input');
    //     reCalculateForm();
    //     /*********************************** */
    //     $('#new-booking-form-popup .title').html('New Booking');
    //     $('#new_booking_form select[name="customer_id"]').next().show();
    //     $('#new_booking_form input[name="customer_name"]').val("").hide();
    //     $("#new-booking-form-popup .modal-footer.new").show();
    //     $("#new-booking-form-popup .modal-footer.edit").hide();
    //     new_booking_form_validator.resetForm();
    //     // enable fields
    //     $('#new_booking_form select[name="service_type_id"]').prop("disabled", false);
    //     $('#new_booking_form select[name="time_from"]').prop("disabled", false);
    //     $('#new_booking_form select[name="time_to"]').prop("disabled", false);
    //     $('#new_booking_form input[name="service_start_date"]').prop("disabled", false);
    //     $('#new_booking_form input[name="service_end_date"]').prop("disabled", false);
    //     $('#new_booking_form input[name="service_rate_per_hour"]').prop("disabled", false);
    //     $('#new_booking_form input[name="service_discount_rate_per_hour"]').prop("disabled", false);
    //     $('#new_booking_form input[name="cleaning_materials"]').prop("disabled", false);
    //     $('#new_booking_form rf').show();
    //     //
    //     fancybox_show('new-booking-form-popup', { width: 750 });
    // }
// });
if ($(".booking-position").length > 0) {
    var stickyTop = $('.booking-position').offset().top;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= stickyTop) {
            $('.scroll-top-fix').addClass('box-fixed-top');
            $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
        } else {
            $('.scroll-top-fix').removeClass('box-fixed-top');
            $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
        }
    });
}
function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
function RefreshSomeEventListener() {
    $(".booked_bubble").off();
    $(".booked_bubble").on("click", function () {
        //alert('b');
    });
}
function renderBookings() {
    loader(true);
    // reset cells
    $('.booked_bubble').remove();
    $('.cell').removeClass("booked")
    //
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "laravel/booking/suspended_schedules",
        // data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() || undefined },
        data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() ? (moment($('#filter_end_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined },
        success: function (data) {
            var booking_delete_list = data.booking_delete_data;
            var suspend_count = data.suspend_count;
            var suspend_maid_absent_count = data.suspend_maid_absent_count;
            var suspend_by_customer_count = data.suspend_by_customer_count;
            var suspend_by_office_count = data.suspend_by_office_count;
            var unassigned_count = data.unassigned_count;
            var reassigned_count = data.reassigned_count;
            var maid_leaveData = data.maid_leaveData;
            // var assigned_hours = data.assigned_hours;
            // var unassigned_hours = data.unassigned_hours;
            
            $.each(data.bookings, function (key, booking) {
                // console.log(booking_delete_list.booking_id);
                // var new1 = data.booking_delete_data.filter(booking.booking_id)
                // console.log(new1)
                let delete_list = booking_delete_list.find(delete_list => {
                    return delete_list.booking_id == booking.booking_id
                });
                if (booking.booking_type == 'WE') {
                    var css_class = "bg-we";
                } else {
                    var css_class = "bg-od";
                }
                var cells = booking.working_minutes / 30;
                var tooldata = `Suspended(` + booking.delete_date_range + `)&#13;&#10;` + `Remarks(` + booking.remarks + `)`;
                if (delete_list != null) {
                    if (booking.reassign_status == null) {
                        var bubble = `<div class="deleted_bubble" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="`+ tooldata +`">
                        <p class="n_booking_customer_name"><i class="fa fa-user" aria-hidden="true"></i>` + booking.customer_name + `</p>
                        <p class="n_booking_time"><i class="fa fa-clock-o" aria-hidden="true"></i>` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours</p>
                        <p class="n_booking_location"><i class="fa fa-map-marker" aria-hidden="true"></i>` + booking.booking_location +  ` ~  `+ booking.booking_zone +`</p>
                        <p class="n_booking_maid_name">Suspended(` + booking.delete_date_range + `)</p>
                        <p class="n_booking_maid_name">Reason(` + booking.cancel_reason + `)</p>
                        <p class="n_booking_remark">Remarks(` + booking.remarks + `)</p></div>`;
                    } else {
                        if (booking.new_parent_maid_name == null) {
                            var maid_name = "";
                            var suspend = `<p class="n_booking_maid_name">Suspended(` + booking.delete_date_range + `)</p>`;
                        } else {
                            var maid_name = `<p class="n_booking_maid_name">` + booking.new_parent_maid_name + `&nbsp;Reassign(` + booking.delete_date_range + `).</p>`;
                            var suspend = "";
                        }
                        var bubble = `<div class="deleted_bubble" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="`+ tooldata +`">
                        <p class="n_booking_customer_name"><i class="fa fa-user" aria-hidden="true"></i>` + booking.customer_name + `</p>
                        <p class="n_booking_time"><i class="fa fa-clock-o" aria-hidden="true"></i>` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours</p>
                        <p class="n_booking_location"><i class="fa fa-map-marker" aria-hidden="true"></i>` + booking.booking_location +  ` ~  `+ booking.booking_zone +`</p>
                        `+ suspend +`
                        <p class="n_booking_maid_name">Reason(` + booking.cancel_reason + `)</p>
                        `+ maid_name + `
                        <p class="n_booking_maid_name">Remarks(` + booking.remarks + `)</p></div>`;  
                        // var bubble = `<div class="booked_bubble `+ css_class + `" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="`+ tooldata +`">
                        // <p class="n_booking_customer_name"><i class="fa fa-user" aria-hidden="true"></i>` + booking.customer_name + `</p>
                        // <p class="n_booking_time"><i class="fa fa-clock-o" aria-hidden="true"></i>` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours</p>
                        // <p class="n_booking_location"><i class="fa fa-map-marker" aria-hidden="true"></i>` + booking.booking_location +  ` ~  `+ booking.booking_zone +`</p>
                        // `+ suspend +`
                        // <p class="n_booking_maid_name">Reason(` + booking.cancel_reason + `)</p>
                        // `+ maid_name + `
                        // <p class="n_booking_maid_name">Remarks(` + booking.remarks + `)</p></div>`;  
                    }
                } else {
                    var bubble = null;
                }
                // // Assigned hours
                // var total_assigned_hours = assigned_hours / 60;
                // $('#assigned_hours').html(total_assigned_hours);

                // // UnAssigned hours
                // var total_unassigned_hours = unassigned_hours / 60;
                // $('#unassigned_hours').html(total_unassigned_hours);

                //unassigned count
                $('#unassigned_count').html(unassigned_count);

                // Reassigned count
                $('#reassigned_count').html(reassigned_count);
                
                // Total Suspended Schedules
                $('#total_suspended_schedules').html(suspend_count);
                
                // Maid absent count 
                $('#maid_absent_count').html(suspend_maid_absent_count);

                //  by customer count
                $('#by_customer').html(suspend_by_customer_count);
                
                //  by customer count
                $('#by_office').html(suspend_by_office_count);

                // $new = data.deleted_booking_data.filter(booking.booking_id)

                var element = $('[data-maid-week="' + booking.maid_id + "-" + booking.service_week_day + '"] [data-time-from="' + booking.time_from + '"]');
                $(element).html(bubble).addClass("booked").off().on("click", function () {
                    $('.mm-loader').show();
                    $.ajax({
                        type: 'GET',
                        dataType: "json",
                        url: _base_url + "laravel/booking/get_suspended_booking_data",
                        data: { booking_id: booking.booking_id },
                        success: function (data) {
                            var booking_data = data.booking_data;
                            var maid_id = booking_data.maid_id;
                            var week_day = booking_data.service_week_day;
                            var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
                            $('#new_booking_form input[name="service_week_day"]').val(week_day);
                            $('#new_booking_form input[name="maid_id"]').val(maid_id);
                            $('#new_booking_form input[name="customer_name"]').val(booking_data.customer_name);
                            $('#new_booking_form input[name="maid_name"]').val(booking_data.maid_name);
                            $('#new_booking_form select[name="service_type_id"]').select2().val(booking_data.service_type_id).trigger("change.select2");
                            $('#new_booking_form select[name="time_from"]').select2().val(booking_data.time_from).trigger("change.select2");
                            $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(booking_data.time_from)).select2().val(booking_data.time_to).trigger("change.select2");
                            $('#new_booking_form input[name="working_hours"]').val(hourDifference(booking_data.time_from, booking_data.time_to));
                            $('#new_booking_form input[name="service_week_name"]').val(week_name);
                            $('#new_booking_form input[name="service_start_date"]').val(moment(booking_data.service_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                            $('#new_booking_form input[name="service_end_date"]').val(booking_data.delete_to_date ? moment(booking_data.delete_to_date, 'YYYY-MM-DD').format('DD/MM/YYYY') : undefined);
                            $('#new_booking_form input[name="service_rate_per_hour"]').val(booking_data.price_per_hr);
                            $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(booking_data._service_discount_rate_per_hour);
                            $('#new_booking_form input[name="cleaning_materials"]').prop('checked', booking_data._cleaning_material == "Y");
                            $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val(booking_data._cleaning_material_rate_per_hour);
                            $('#new_booking_form .vat_percentage').html(booking_data._vat_percentage);
                            $('#new_booking_form input[name="booking_type"]').val(booking.booking_type);
                            $('#new_booking_form input[name="service_date"]').val(booking.service_start_date);
                            $('#new_booking_form input[name="booking_id"]').val(booking.booking_id); // need for edit
                            $('#new_booking_form input[name="delete_from_date"]').val(booking.delete_from_date); // need for edit
                            $('#new_booking_form input[name="delete_to_date"]').val(booking.delete_to_date); // need for edit
                            $('#new_booking_form input[name="delete_date_range"]').val(booking.delete_date_range); // need for edit
                            reCalculateForm();
                            /*********************************** */
                            $('#new-booking-form-popup .title').html('Booking ~ <i>' + booking_data.reference_id + "</i>");
                            $('#new_booking_form select[name="customer_id"]').next().hide();
                            $('#new_booking_form input[name="customer_name"]').show();
                            $("#new-booking-form-popup .modal-footer.new").hide();
                            $("#new-booking-form-popup .modal-footer.edit").show();
                            if (maid_leaveData.length > 0) {
                                $("#new-booking-form-popup #extend_leave").show();
                            } else {
                                $("#new-booking-form-popup #extend_leave").hide();
                            }
                            if (booking.reassign_status == null) {
                                $("#new-booking-form-popup #schedule_re_assign").show();
                                $("#new-booking-form-popup #extend_suspend").show();
                                $("#new-booking-form-popup #reactivate_suspend").show();
                            } else {
                                $("#new-booking-form-popup #schedule_re_assign").hide();
                                $("#new-booking-form-popup #extend_suspend").hide();
                                $("#new-booking-form-popup #reactivate_suspend").hide();
                            }
                            // $("#new-booking-form-popup #extend_suspend").show();
                            new_booking_form_validator.resetForm();
                            // disable fields
                            $('#new_booking_form select[name="service_type_id"]').prop("disabled", true);
                            $('#new_booking_form select[name="time_from"]').prop("disabled", true);
                            $('#new_booking_form select[name="time_to"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_start_date"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_end_date"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_rate_per_hour"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_discount_rate_per_hour"]').prop("disabled", true);
                            $('#new_booking_form input[name="cleaning_materials"]').prop("disabled", true);
                            $('#new_booking_form rf').hide();
                            //
                            fancybox_show('new-booking-form-popup', { width: 750 });
                        },
                        error: function (data) {
                            toast('warning', "An error occured !");
                            $('.mm-loader').hide();
                        },
                    });
                });
            });
            RefreshSomeEventListener();
            styleThisWeekCells();
            loader(false);
        },
        error: function (data) {
            toast('warning', "An error occured !");
            loader(false);
        },
    });
}
function styleThisWeekCells() {
    var currentTime = moment();
    var dow = currentTime.day();
    var time_rounded = moment(currentTime).format("H:00:00");
    $('.cell').removeClass("current-week-day");
    $('.cell').removeClass("current-time");
    $('.cell').removeAttr('style');
    $('.cell').each(function (index, option) {
        /******* same week day highlight  */
        if ($(this).attr("data-week") == dow) {
            $(this).addClass("current-week-day");
        }
        /******* current time highlight  */
        if ($(this).attr("data-time-from") == time_rounded) {
            //$(this).addClass("current-time");
            $(this).css({ "border-right": "1px solid rgb(255 150 150)" });
        }
    });
}
$(document).ready(function () {
    re_assign_schedule_form_validator = $('#re_assign_schedule_form').validate({
        ignore: [],
        rules: {
            reason: {
                required: true,
            },
            
        },
        messages: {
            "reason": "Please enter reason",
        },
        errorPlacement: function (error, element) {
            
            // else {
                error.insertAfter(element);
           // }
        }
    });
    renderBookings();
    extend_suspend_date_range_schedule_od = $('#extend_suspend_date_range_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule_suspended/extend_suspend_date_range_schedule_od",
                data: $('#extend_suspend_date_range_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/suspended_view";
                        }, 1000);
                        $("#extend_suspend_date_range_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#extend_suspend_date_range_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#extend-suspend-date-range-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
    extend_suspend_date_range_schedule_we = $('#extend_suspend_date_range_schedule_we').validate({
        ignore: [],
        rules: {
            delete_date_from: {
                required: true,
            },
            delete_date_to: {
                required: true,
            },
            remark: {
                required: true,
            },
           
        },
        messages: {
            delete_date_from: "Please enter date.",
            delete_date_to: "Please enter date.",
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule_suspended/extend_suspend_date_range_schedule_we",
                data: $('#extend_suspend_date_range_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/suspended_view";
                        }, 1000);
                        $("#extend_suspend_date_range_schedule_we")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#extend_suspend_date_range_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#extend-suspend-date-range-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
});

/********************************************************************************** */
// date picker
$('.datepicker').datepicker({ autoclose: true, todayHighlight: true, todayBtn: "linked" }).on('changeDate', function (event) {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var date_full = moment(date).format('DD/MM/YYYY');
    $('.hed-date-main-box').html(date_full);
    $('#date').val(date);
});
$('.hed-date-left-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(-1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('.hed-date-right-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('#maid_leave_extend .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#extend_suspend_date_range_schedule_od .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#extend_suspend_date_range_schedule_we .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
/***************************************************************************************** */
$('#new_booking_form select[name="time_from"]').on('change', function () {
    $('#new_booking_form input[name="working_hours"]').val(0);
    $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change.select2");
});
$('#new_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
/***************************************************************************************** */
function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#new_booking_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
/***************************************************************************************** */
$('#new_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_start_date"]').valid();
    var booking_date = $(this).val();
    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
    $('#new_booking_form input[name="service_week_day"]').val(dow);
    $('#new_booking_form input[name="service_week_name"]').val(days[dow]);

    // alert(week_day)
    $('#new_booking_form input[name="service_end_date"]').datepicker('update', null).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").add(7 * 3, 'days').format('DD/MM/YYYY'));
});
$('#new_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_end_date"]').valid();
});
$('#new_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
}).on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="time_to"]').select2().on("change", function (e) {
    $(this).valid();
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_week_day"]').on('change', function () {
    var week_day_id = this.value;
    var maid_id = $('#schedule-menu select[name="filter_maid_id"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_maid_id"]').on('change', function () {
    var maid_id = this.value;
    var week_day_id = $('#schedule-menu select[name="filter_week_day"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
function maidHeads(show) {
    if (show == true) {
        $('#weeks .maid_name_week').show();
        $('.tb-slider .maid_name').show();
    }
    else {
        $('#weeks .maid_name_week').hide();
        $('.tb-slider .maid_name').hide();
    }
}
function maidWeeks(show) {
    if (show == true) {
        $('#weeks .week').show();
    }
    else {
        $('#weeks .week').hide();
    }
}
function slots(show) {
    if (show == true) {
        $('.tb-slider .slots').show();
    }
    else {
        $('.tb-slider .slots').hide();
    }
}
/***************************************************************************************** */
$().ready(function () {
    new_booking_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: false,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            },
            cleaning_material_rate_per_hour: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
            cleaning_material_rate_per_hour: "Enter materials per hour rate."
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "cleaning_material_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#new_booking_form").serializeArray();
            var service_start_date_i = values.map(function (o) { return o.name; }).indexOf("service_start_date");
            var service_end_date_i = values.map(function (o) { return o.name; }).indexOf("service_end_date");
            if (values[service_start_date_i]['value']) {
                values[service_start_date_i]['value'] = moment(values[service_start_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            if (values[service_end_date_i]['value']) {
                values[service_end_date_i]['value'] = moment(values[service_end_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/" + ($('#new_booking_form input[name="booking_id"]').val() == "" ? "save_booking" : "update_booking"),
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
/***************************************************************************************** */
var cleaning_material_rate_per_hour = 0;
function reCalculateForm() {
    cleaning_material_rate_per_hour = $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val();
    var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#new_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#new_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#new_booking_form input[name="material_fee"]').val(material_fee);
    $('#new_booking_form input[name="service_amount"]').val(service_amount);
    $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#new_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#new_booking_form input[name="service_discount"]').val(service_discount);
    //$('#new_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#new_booking_form input[name='cleaning_materials'],#new_booking_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $(this).valid();
});
$("#new_booking_form input[name='service_discount_rate_per_hour'],#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
$(function () {
    $('[data-toggle="popover"]').popover()
})
/***************************************************************************************** */
// page filter options
$('#filter_start_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    // startDate: "today"
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_start_date"]').valid();
    var end_date = moment(this.value, "DD.MM.YYYY");
    var end_date = end_date.add(5, 'days').format('DD/MM/YYYY');
    $('#filter_end_date').datepicker('update', end_date).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").format('DD/MM/YYYY'));
    renderBookings();
});
$('#filter_end_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_end_date"]').valid();
});

$('#schedulte_delete').click(function () {
    // var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    // var service_date = $('#new_booking_form input[name="service_date"]').val();
    // var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    fancybox_show('alert-popup', { width: 450 });
    //    $('#day_service_id').val(day_service_id);
    $('#booking_id_perment').val(booking_id);
    $('#booking_id_day').val(booking_id);
    //    $('#service_date').val(service_date);
    //    $('#day_service_status').val(service_status);
   
});

$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    // $('.n-delete-set-left').addClass('de-select');

    //$('.n-delete-set-right-cont').show(500);
     $('.n-delete-set-left-cont').show(500);
});

// $(".n-delete-set-left").click(function () {
//     $('.n-delete-set-left').removeClass('de-select');
//     $('.n-delete-set-right').addClass('de-select');

//     $('.n-delete-set-left-cont').show(500);
//     $('.n-delete-set-right-cont').hide(500);
// });

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    var booking_id = $('#booking_id_perment').val();
    if(remarks == "")
    {
        $("#delete_remark_perm").focus();
         $('#deleteremarks_book').css('display','block');
    } else {
        $('#deleteremarks_book').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#booking_id_day').val();
    if(remarks == "")
    {
        $("#delete_remark_day").focus();
         $('#deleteremarks_book_day').css('display','block');
    } else {
        $('#deleteremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});
function formatDate(inputDate) {
    if (!inputDate) return ''; // Return empty string if inputDate is falsy

    var parts = inputDate.split('-'); // Split 'yyyy-mm-dd'
    if (parts.length !== 3) return ''; // Ensure there are 3 parts

    return parts[2] + '/' + parts[1] + '/' + parts[0]; // Return 'dd/mm/yyyy'
}
$('#extend_leave').click(function () {
    var maid_id =  $('#new_booking_form input[name="maid_id"]').val();  
    var delete_from_date = $('#new_booking_form input[name="delete_from_date"]').val();
    var delete_to_date = $('#new_booking_form input[name="delete_to_date"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var delete_date_range = $('#new_booking_form input[name="delete_date_range"]').val();
    // $('#new-booking-form-popup').hide();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: _base_url + 'schedule_suspended/maid_leave',
        data: {delete_from_date:delete_from_date,
            delete_to_date:delete_to_date, maid_id:maid_id, delete_date_range:delete_date_range},
        // cache: false,
        success: function (data)
            {
            fancybox_show('leave-extend-popup', { width: 450 });
            // var formattedDeleteFromDate = formatDate(data.delete_from_date);
            // var formattedDeleteToDate = formatDate(data.delete_to_date);
            var selectedLeaveType = $('#leave_type').val();
            var maid_leave_data = data.maid_leave_data;
            maid_leave_data.forEach(function (leave) {
                var selectedValue = '';
                var selectedText = '';

            if (leave.leave_type === '1') {
                selectedValue = '1';
                selectedText = 'Full Day';
            } else if (leave.leave_type === '2') {
                selectedValue = '2';
                selectedText = 'Half Day';
            }
            var selectedValue1 = '';
            var selectedText1 = '';
            if (leave.typeleaves ==='vaccations') {
                selectedValue1 = 'vaccations';
                selectedText1 = 'Vacations';
            }  else if(leave.typeleaves === 'leave') {
                selectedValue1 = 'leave';
                selectedText1 = 'Leave';
            } else if(leave.typeleaves === 'holidays') {
                selectedValue1 = 'holidays';
                selectedText1 = 'Holidays';
            } else if(leave.typeleaves === 'medical_leaves') {
                selectedValue1 = 'medical_leaves';
                selectedText1 = 'Medical Leaves';
            }
            // Set the selected value
            var selectElement = document.getElementById('leave_type');
            selectElement.value = leave.leave_type;
            // var selectedText = selectElement.options[selectElement.selectedIndex].text;

            var selectElement1 = document.getElementById('leave_type_new');
            selectElement1.value = leave.typeleaves;

            // var selectedText1 = selectElement1.options[selectElement1.selectedIndex1].text;
            });
            var leaveData = data.maid_leave_data;
                if (leaveData.length > 0) {
                    var firstLeaveDate = leaveData[0].leave_date;
                    var lastLeaveDate = leaveData[leaveData.length - 1].leave_date;
                    var lastLeaveDateObj = new Date(lastLeaveDate);
                    var extendedLastLeaveDateObj = addDays(lastLeaveDateObj, 0);
                    var extendedLastLeaveDate = formatDate1(extendedLastLeaveDateObj);
                    $('#maid_leave_extend input[name="delete_date_from"]').val(formatDate(firstLeaveDate));
                    $('#maid_leave_extend input[name="delete_date_to"]').val(formatDate(extendedLastLeaveDate));
                }
           
            $('#maid_leave_extend input[name="maid_id"]').val(data.maid_id);
            $('#maid_leave_extend input[name="booking_id"]').val(booking_id);
            $('#maid_leave_extend input[name="booking_type"]').val(booking_type);
            $('#maid_leave_extend input[name="delete_from_date1"]').val(delete_from_date);
            $('#maid_leave_extend input[name="delete_to_date1"]').val(delete_to_date);
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
   
   
    
});
function formatDate1(date) {
    var d = new Date(date);
    var year = d.getFullYear();
    var month = ("0" + (d.getMonth() + 1)).slice(-2); // Months are zero-based, so add 1
    var day = ("0" + d.getDate()).slice(-2);
    return year + "-" + month + "-" + day;
}

// Function to add days to a date
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
$('#schedule_re_assign').click(function () {
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var delete_from_date = $('#new_booking_form input[name="delete_from_date"]').val();
    var delete_to_date = $('#new_booking_form input[name="delete_to_date"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var maid_name =  $('#new_booking_form input[name="maid_name"]').val();
    var delete_date_range = $('#new_booking_form input[name="delete_date_range"]').val();
    // fancybox_show('reassign-popup', { width: 450 });
    // $('#new-booking-form-popup').hide();
    var maid_id =  $('#new_booking_form input[name="maid_id"]').val();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: _base_url + 'schedule_suspended/maid_availability',
        data: {time_from:time_from, time_to:time_to, booking_id:booking_id, delete_from_date:delete_from_date,
            delete_to_date:delete_to_date, maid_id:maid_id, maid_name:maid_name, delete_date_range:delete_date_range},
        // cache: false,
        success: function (data)
            {
                // window.location.href= data.redirect_url;
                // window.location.href="<?php echo base_url('test/'.data);";
                // if (data.status == "success") {
                //     // url: _base_url + "laravel/booking/suspended_schedules",
                window.location.href = _base_url + "schedule_suspended/maid_reassign?booking_id=" + data.booking_id + "&time_from=" +data.time_from+ 
                  "&time_to=" +data.time_to+ "&delete_to_date=" +data.delete_to_date+ "&delete_from_date=" +data.delete_from_date+ "&maid_id=" +data.maid_id+
                  "&maid_name=" +data.maid_name;
            // }
           
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
   
});

$('#re_assign_schedule_form input[name="delete_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});
$('#re_assign_schedule_form input[name="delete_date_to"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#maid_leave_extend #leave-extend-btn').click(function () {
    $('.mm-loader').show();
    var formData = new FormData(document.getElementById("maid_leave_extend"));
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "schedule_suspended/maid_leave_extend",
        data: $('#maid_leave_extend').serialize(),
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.status =="success") {
                toast('success', data.message);
                setTimeout(function () {
                    window.location.href = _base_url + "schedule/suspended_view";
                }, 3000);
            }
            else {
                $('#leave-extend-btn').html('Save').removeAttr("disabled");
                toast('error', data.message);
            }
        },
        error: function (data) {
            $('#leave-extend-btn').html('Save').removeAttr("disabled");
            toast('error', data.message);
        },
    });
    
});

$('#extend_suspend').click(function () {
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var delete_date_range = $('#new_booking_form input[name="delete_date_range"]').val();
    // $('#new-booking-form-popup').hide();
    if (booking_type == "WE") {
        $('#extend_suspend_date_range_schedule_we input[name="booking_id"]').val(booking_id);
        $('#extend_suspend_date_range_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#extend_suspend_date_range_schedule_we input[name="service_date"]').val(service_date);
        $('#extend_suspend_date_range_schedule_we input[name="day_service_status"]').val(service_status);
        $('#extend_suspend_date_range_schedule_we input[name="delete_date_range"]').val(delete_date_range);
       fancybox_show('suspend-extend-date-range-we-popup', { width: 450 });
    }
    else {
        $('#extend_suspend_date_range_schedule_od input[name="booking_id"]').val(booking_id);
        $('#extend_suspend_date_range_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#extend_suspend_date_range_schedule_od input[name="service_date"]').val(service_date);
        $('#extend_suspend_date_range_schedule_od input[name="delete_date_from"]').val(moment(service_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#extend_suspend_date_range_schedule_od input[name="delete_date_to"]').val(moment(service_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#extend_suspend_date_range_schedule_od input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-extend-date-range-od-popup', { width: 450 });

    }
          
    
});

$('#reactivate_suspend').click(function () {
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    fancybox_show('schedule_reactivate-popup', {});
    $('#schedule_reactivate input[name="booking_id"]').val(booking_id);
    
});

$("#schedule_reactivate_btn").click(function () {
  
    var booking_id =  $('#schedule_reactivate input[name="booking_id"]').val();
   
    $.ajax({
        type: "POST",
        url: _base_url + 'schedule/booking_schedule_re_activate',
        data: {booking_id: booking_id},
        cache: false,
        success: function (data)
        {
            if (data.status == "success") {
                toast('success', data.message);
                close_fancybox();
                
                // window.location.href = _base_url + "call-management";
                
            }
            else {
                toast('error', data.message);
                loader(false);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
    
});
