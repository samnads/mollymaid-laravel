/************************************************************************** */
/* Coder : Samnad. S                                                        */
/*                                                                          */
/* Last Updated by :                                                        */
/************************************************************************** */
// logic goes here...................
/************************************************************************** */
$('[data-tab]').click(function (event) {
    var tab_name = $(this).attr("data-tab");
    if (tab_name == "aaaa") {
    }
    else {
    }
});
/************************************************************************** */
$('#edit_employee_form input[name="date_of_joining"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
}).on('changeDate', function (e) {

});
/************************************************************************** */
$('#edit_employee_form select[name^="availability_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#edit_employee_form select[name="availability_time_to[' + id + ']"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#edit_employee_form select[name="availability_time_from[' + id + ']"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#edit_employee_form select[name="availability_time_to[' + id + ']"]').html(_time_to_options);
    });
    $('#edit_employee_form select[name="availability_time_to[' + id + ']"]').select2().val("").trigger("change");//.select2('open');
});
function skillRenderHtml(skill_id, skill, skill_rating_id, rating_level, notes) {
    console.log(notes);
    return `<div class="col-sm-3" id="skill_` + skill_id + `">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">` + skill + `<span class="pull-right">` + rating_level + `</span></h3>
                    </div>
                    <div class="panel-body">
                        <p>Notes</p>
                        <input type="hidden" name="skill_ids[]" value="` + skill_id + `"/>
                        <input type="hidden" name="skill_rating_level_ids[]" value="` + skill_rating_id + `"/>
                        <textarea style="width:100%" name="skill_notes[]">` +(notes ? notes : '') + `</textarea>
                    </div>
                    <div class="panel-footer text-right"><button type="button" class="btn btn-danger" data-id="` + skill_id + `" data-action="remove-skill"><i class="fa fa-trash" aria-hidden="true"></i></button></div>
                </div>
            </div>`;
}
/************************************************************************** */
$().ready(function () {
    $('li [data-tab="tab1"]').trigger('click'); // active tab
    /****************************************************************** */
    // hide some to times based on from time :D
    $('#edit_employee_form select[name^="availability_time_from"]').each(function () {
        if (this.value) {
            var name = $(this).attr("name");
            var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
            var from_time = this.value;
            var to_time = $('#edit_employee_form select[name="availability_time_to[' + id + ']"]').val();
            var _time_to_options = '';
            $('#edit_employee_form select[name="availability_time_from[' + id + ']"] option').each(function (index, option) {
                if ($(option).val() > from_time) {
                    _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
                }
                $('#edit_employee_form select[name="availability_time_to[' + id + ']"]').html(_time_to_options);
            });
            $('#edit_employee_form select[name="availability_time_to[' + id + ']"]').select2().val(to_time).trigger("change");//.select2('open');
        }
    });
    /****************************************************************** */
    // employee skills render
    var skillHtml = ``;
    employee_skills.forEach(function (skill) {
        skillHtml += skillRenderHtml(skill.skill_id, skill.skill, skill.rating_level_id, skill.rating_level, skill.notes);
    });
    $('#edit_employee_form #skills-holder').html(skillHtml);
    $('#edit_employee_form [data-action="remove-skill"]').off();
    $('#edit_employee_form [data-action="remove-skill"]').click(function (event) {
        var skill_id = $(this).attr("data-id");
        $(this).closest('div#skill_' + skill_id).remove();
    });
    /****************************************************************** */
    // Mobile number max limit to 12
    $.validator.addMethod("validAddressMobileNumber", function(value, element) {
        var validLength = value.replace(/[^\d]/g, "").length <= 12;
        return this.optional(element) || validLength;
    }, "Mobile number must be a maximum of 12 digits.");

    // **************************************************************************

    edit_employee_form_validator = $('#edit_employee_form').validate({
        ignore: [],
        rules: {
            employee_id: {
                required: true,
            },
            employee_type_id: {
                required: true,
            },
            maid_leader_id: {
                required: '#is_maid_leader:unchecked'
            },
            employee_name: {
                required: true,
            },
            nationality: {
                required: true,
            },
            present_address: {
                required: true,
            },
            date_of_joining: {
                required: true,
            },
            mobile_number_1: {
                required: true,
                validAddressMobileNumber: true,
                remote: {
                    url: _base_url + "employee/editvalidatemobilenumber",
                    type: "post",
                    data: 
                    {
                        id:$("[name='employee_id").val(),
                        phone: function(){ return $("[name='mobile_number_1").val();}
                    }
                }
            },
            /************************************************** */
            "availability_location[0]": {
                required: '#week_day_0:checked'
            },
            "availability_location[1]": {
                required: '#week_day_1:checked'
            },
            "availability_location[2]": {
                required: '#week_day_2:checked'
            },
            "availability_location[3]": {
                required: '#week_day_3:checked'
            },
            "availability_location[4]": {
                required: '#week_day_4:checked'
            },
            "availability_location[5]": {
                required: '#week_day_5:checked'
            },
            "availability_location[6]": {
                required: '#week_day_6:checked'
            },
            "availability_time_from[0]": {
                required: '#week_day_0:checked'
            },
            "availability_time_from[1]": {
                required: '#week_day_1:checked'
            },
            "availability_time_from[2]": {
                required: '#week_day_2:checked'
            },
            "availability_time_from[3]": {
                required: '#week_day_3:checked'
            },
            "availability_time_from[4]": {
                required: '#week_day_4:checked'
            },
            "availability_time_from[5]": {
                required: '#week_day_5:checked'
            },
            "availability_time_from[6]": {
                required: '#week_day_6:checked'
            },
            "availability_time_to[0]": {
                required: '#week_day_0:checked'
            },
            "availability_time_to[1]": {
                required: '#week_day_1:checked'
            },
            "availability_time_to[2]": {
                required: '#week_day_2:checked'
            },
            "availability_time_to[3]": {
                required: '#week_day_3:checked'
            },
            "availability_time_to[4]": {
                required: '#week_day_4:checked'
            },
            "availability_time_to[5]": {
                required: '#week_day_5:checked'
            },
            "availability_time_to[6]": {
                required: '#week_day_6:checked'
            },
            /************************************************** */
            "services_ids[]": {
                required: 'input[id="employee_type_1"]:checked',
                minlength: 1
            },
            "address_type_contact_name[0]": {
                required: true,
            },
            "address_type_contact_name[1]": {
                required: true,
            },
            // "address_type_contact_name[2]": {
            //     required: true,
            // },
            // "address_type_contact_name[3]": {
            //     required: true,
            // },
            "address_type_mobile_no_1[0]": {
                required: true,
                validAddressMobileNumber: true,
                remote: {
                    url: _base_url + "employee/edit_validate_adress_type_mobilenumber",
                    type: "post",
                    data: 
                    {
                        id:$("[name='employee_id").val(),
                        phone: function(){ return $("[name='address_type_mobile_no_1[0]']").val(); }
                    }  
                }
            },
            "address_type_mobile_no_1[1]": {
                required: true,
                validAddressMobileNumber: true,
                remote: {
                    url: _base_url + "employee/edit_validate_adress_type_mobilenumber",
                    type: "post",
                    data: 
                    {
                        id:$("[name='employee_id").val(),
                        phone: function(){ return $("[name='address_type_mobile_no_1[1]']").val(); }
                    }  
                },
            },
            "address_type_mobile_no_1[2]": {
                validAddressMobileNumber: true,
            },
            "address_type_mobile_no_1[3]": {
                validAddressMobileNumber: true,
            },
            "address_type_country_id[0]": {
                required: true,
            },
            "address_type_country_id[1]": {
                required: true,
            },
            // "address_type_country_id[2]": {
            //     required: true,
            // },
            // "address_type_country_id[3]": {
            //     required: true,
            // },
        },
        messages: {
            "services_ids[]": "Select atleast one service.",
            "maid_leader_id": "Select team leader",
            "employee_name": "Please enter employee name",
            "nationality": "Select nationality",
            "present_address": "Please enter present address",
            "date_of_joining": "Please enter date of joining",
            "mobile_number_1": {
                required: "Please enter mobile number",
                remote: "Mobile number already exist"
            },
            "address_type_contact_name[0]": "Please enter local address contact name",
            "address_type_contact_name[1]": "Please enter permanent address contact name",
            // "address_type_contact_name[2]": "Please enter emergency local contact name",
            // "address_type_contact_name[3]": "Please enter emergency home country contact name",
            "address_type_mobile_no_1[0]": {
                requird: "Please enter local address mobile number",
                remote: "Mobile number already exist"
            },
            "address_type_mobile_no_1[1]": {
                required: "Please enter permanent address mobile number",
                remote: "Mobile number already exist"
            },
            // "address_type_mobile_no_1[2]": "Please enter emergency local mobile number",
            // "address_type_mobile_no_1[3]": "Please enter emergency home country mobile number",
            "address_type_country_id[0]": "Select country",
            "address_type_country_id[1]": "Select country",
            // "address_type_country_id[2]": "Select country",
            // "address_type_country_id[3]": "Select country"
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "maid_leader_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "nationality") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "gender_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "services_ids[]") {
                error.insertAfter(element.parent().parent().parent());
            }
            else if (element.attr("name") == "address_type_country_id[0]") {
                error.insertAfter(element.parent().parent());
            }
            else if (element.attr("name") == "address_type_country_id[1]") {
                error.insertAfter(element.parent().parent());
            }
            else if (element.attr("name") == "address_type_country_id[2]") {
                error.insertAfter(element.parent().parent());
            }
            else if (element.attr("name") == "address_type_country_id[3]") {
                error.insertAfter(element.parent().parent());
            }
            /************************************************** */
            else if (element.attr("name").startsWith("availability_location")) {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name").startsWith("availability_time_from")) {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name").startsWith("availability_time_to")) {
                error.insertAfter(element.parent());
            }
            /************************************************** */
            else {
                error.insertAfter(element);
            }
        }
    });
    $('#edit_employee_form #employee-save-btn').click(function () {
        edit_employee_form_validator.resetForm();
        if ($("#edit_employee_form").valid()) {
            $('.mm-loader').show();
            $('#employee-save-btn').html('Saving...').attr("disabled", true);
            var formData = new FormData(document.getElementById("edit_employee_form"));
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "employee/update",
                data: $('#edit_employee_form').serialize(),
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == true) {
                        $('#employee-save-btn').html('Updated');
                        //$('#employee-save-btn').html('Save').removeAttr("disabled");
                        toast('success', data.message);
                        setTimeout(function () {
                            window.location.href = _base_url + "employee/list";
                        }, 3000);
                    }
                    else {
                        $('#employee-save-btn').html('Save').removeAttr("disabled");
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    $('#employee-save-btn').html('Save').removeAttr("disabled");
                    toast('error', data.statusText);
                },
            });
        }
        else {
            $('#edit_employee_form label.error').each(function () {
                if ($(this).css('display') != 'none') {
                    var tab_id = $(this).closest('.tab-pane').attr("id");
                    $('li [data-tab="' + tab_id + '"]').trigger('click');
                    //toast('warning', "Fill required fields to continue.");
                    toast('warning', $(this).text());
                    return false;
                }
            });
        }
    });
});
/************************************************************************** */
$('#edit_employee_form [data-action="select-skill"]').click(function (event) {
    var skill_id = $('#edit_employee_form select[id="skill_id"]').val();
    var skill = $('#edit_employee_form select[id="skill_id"]').select2('data')[0]['text'];
    var rating_level = $('#edit_employee_form select[id="skill_rating_id"]').select2('data')[0]['text'];
    var skill_rating_id = $('#edit_employee_form select[id="skill_rating_id"]').val();
    if (skill_id && skill_rating_id) {
        if (document.getElementById("skill_" + skill_id)) {
            toast('info', "Skill already selected.");
            return false;
        }
        $('#edit_employee_form #skills-holder').prepend(skillRenderHtml(skill_id, skill, skill_rating_id, rating_level, ""));
        $('#edit_employee_form [data-action="remove-skill"]').off();
        $('#edit_employee_form [data-action="remove-skill"]').click(function (event) {
            var skill_id = $(this).attr("data-id");
            $(this).closest('div#skill_' + skill_id).remove();
        });
    }
});
/************************************************************************** */
$('input[name="employee_type_id"]').click(function (event) {
    if (this.value == 1) {
        // maid employee
        $('[data-tab="tab4"]').closest('li').show();
        $("#is-maid-leader").show();
        $("#maid-leader").show();
        $("#service-types").show();
    }
    else {
        $('[data-tab="tab4"]').closest('li').hide();
        $('#is_maid_leader').prop('checked', false);
        $("#is-maid-leader").hide();
        $("#maid-leader").hide();
        $("#service-types").hide();
        $('#edit_employee_form input[name="services_ids[]"]').prop("checked", false)
    }
});
/************************************************************************** */
$('#is_maid_leader').change(function () {
    if (this.checked) {
        $('select[name="maid_leader_id"]').select2().val("").trigger("change");
        $("#maid-leader").hide();
    }
    else {
        $("#maid-leader").show();
    }
});
/************************************************************************** */

// image upload show img name in input
document.querySelectorAll('input[name^="document_image["]').forEach(function (inputElement) {
    inputElement.addEventListener('change', function (e) {
        var selectedFile = e.target.files[0];
        var filename = selectedFile.name;

        // Show the selected filename in the associated custom file label
        var customFileLabel = document.querySelector('label[for="' + e.target.id + '"]');
        if (customFileLabel) {
            customFileLabel.textContent = filename;
        }

        // Check if the file type is allowed
        var extension = filename.split('.').pop();
        var allowedTypes = ['jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'pdf'];
        var inputId = e.target.id.replace(/\D/g, '');
        var errorDiv = document.getElementById('image_error[' + inputId + ']');

        if (allowedTypes.indexOf(extension.toLowerCase()) === -1) {
            if (errorDiv) {
                errorDiv.textContent = 'File type not allowed. Allowed types: jpg, jpeg, gif, png, doc, docx, pdf';
                errorDiv.classList.add('error');
            }
            e.target.value = '';
            if (customFileLabel) {
                customFileLabel.textContent = 'Choose file';
            }
        } else {
            if (errorDiv) {
                errorDiv.textContent = '';
                errorDiv.classList.remove('error');
            }
        }
    });
});