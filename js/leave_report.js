$('body').on('change', '#dayselect', function() {
	var todate = $('#dayselect').val();
        //window.location = _base_url + 'apinew/view_maid_reports/' + todate;
        $.ajax({
            type: "POST",
            url: _base_url + "apinew/get_maid_reports",
            data: {todate: todate},
            success: function (result) {
                
                $('#da-ex-datatable-numberpaging').html(result);
               //location.reload();
            }
        });
});

function delete_maidleaves($this,leave_id, leave_status)
{
    var _lblstatus = leave_status == 1 ? 'disable' : 'enable';
    if (confirm('Are you sure you want to ' + _lblstatus + ' this leave'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "reports/remove_leave",
            data: {leave_id: leave_id, leave_status : leave_status},
            // dataType: "text",
            // cache: false,
            success: function (result) {
                //window.location.assign(_base_url + 'customers');
               if(result == 1)
               {
                   //$($this).attr('class', 'btn btn-success btn-small');
                   $($this).html('<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"></i>');
               }
               else
               {
                   //$($this).attr('class', 'btn btn-danger btn-small');
                   $('#searchgo').trigger('click');
                   //$($this).html('<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"> </i>');
               }
               $($this).attr('onclick', 'delete_maidleaves(this, ' + leave_id +', ' + result +')');
            }
        });
    }
}