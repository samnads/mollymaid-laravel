$('.datepicker').datepicker().on('changeDate', function(ev){ 
	var _date = new Date(ev.date);
	var _new_date = ('0' + _date.getDate()).slice(-2) + '-' + ('0' + (_date.getMonth()+1)).slice(-2) + '-' + _date.getFullYear();
	window.location = _base_url + 'booking/' + _new_date;
});

$('#leave_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        //startDate: new Date()
    });
$('body').on('click', '.pop_close', function() {
	_bpop_open = false;
	parent.$.fancybox.close();
	$(".ui-selected").removeClass("ui-selected");
	if(_refresh_page)
	{
		window.location = _page_url;
	}
});   
$(function(){
  
   $('#leave_date').unbind('change').bind('change', function () {
      
        
        var leave_date = $('#leave_date').val();
        
        $.ajax({
            type: "POST",
            url: _base_url + 'maid/search_maid_leave_by_date',
            data: {leave_date: leave_date},
            success: function (response)
            {
                
                if (response) {
                    $('#maid-leave-list tbody').html(response);
                    $('.widget-content').next('p').remove();
                }
            }

        });
        
    });
});   

function undo_leave(leave_id)
{
    
    var _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Confirm<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure you want to cancel this Leave?</div><div class="bottom"><input type="button" value="Ok" onclick="cancel_leave('+ leave_id +');"/>&nbsp;<input type="button" value="Cancel" id="cancel" class="pop_close"/></div></div>';
     
    
    $.fancybox.open({
            autoCenter : true,
            fitToView : false,
            scrolling : false,
            openEffect : 'fade',
            openSpeed : 100,
            helpers : {
                    overlay : {
                            css : {
                                    'background' : 'rgba(0, 0, 0, 0.3)'
                            },
                            closeClick: false
                    }
            },
            padding : 0,
            closeBtn : false,
            content: _alert_html
    });
}
function cancel_leave(leave_id)
{
    var leave_date = $('#leave_date').val();
    $.fancybox.close();
    $.ajax({
            type: "POST",
            url: _base_url + 'maid/cancel_maid_leave_by_id',
            data: {leave_id: leave_id, leave_date: leave_date},
            success: function (response)
            {
                //alert(response);
                if (response) {
                    $('#maid-leave-list tbody').html(response);
                    $('.widget-content').next('p').remove();
                }
            }
        });
}