$().ready(function () {
    $('li[data-tab="personal"] a').trigger('click'); // active tab
});
var address_html = null;
$(function () {
    /************************************************************* */
    $("#address_zone_id_0").on('change', function () {
        zoneId = $(this).val();
        $.ajax({
            url: _base_url + 'customer/getarea',
            type: 'POST',
            data: { zoneId: zoneId },
            dataType: 'json',
            success: function (response) {
                // Clear the previous options
                $('#address_area_id_0').empty();

                // $('#address_area_id_0').html('<option value="">-- Select area --</option>').select2();
                // Add the new options based on the response
                $.each(response, function (index, area) {
                    $('#address_area_id_0').append('<option value="' + area.area_id + '">' + area.area_name + '</option>');
                });
                $('#address_area_id_0').select2();
            }
        });
    })
    add_address();
    //copy address click
    $("#copy_address_id_0").click(function () {
        var copyText = document.getElementById("address_addresses_id_0");
        copyText.select();
        copyText.setSelectionRange(0, 99999); // For mobile devices
    
        // Copy the text inside the text field
        navigator.clipboard.writeText(copyText.value);
    });
    /************************************************************* */
    address_html = $('#address-single').prop('outerHTML');
    $("select[name='address_zone_ids[]']").select2();
    $("select[name='address_area_ids[]']").select2();
    $("select[name='address_location_ids[]']").select2();
    $("select[name='address_landmark_ids[]']").select2();
    if (customer_id) {
        $('#address-single').remove();

        $("[name='copy_address[]']").each(function (index, element) {
            $(this).attr({
                id: 'copy_address_id_' + index,
            });  
        });

        $("[name='address_area_ids[]']").each(function (index, element) {
            $(this).off('change');
            $(this).attr({
                id: 'address_area_id_' + index,
            });
            $(this).select2();
        });  
        /********************************************* */
        $("[name='address_location_ids[]']").each(function (index, element) {
            $(this).off('change');
            $(this).attr({
                id: 'address_location_id_' + index,
            });
            $(this).select2();
        });
        /********************************************* */
        $("[name='address_landmark_ids[]']").each(function (index, element) {
            $(this).off('change');
            $(this).attr({
                id: 'address_landmark_id_' + index,
            });
            $(this).select2();
        });

        $("[name='address_zone_ids[]']").each(function (index, element) {
            $(this).off('change');
            $(this).attr({
                id: 'address_zone_id_' + index,
            });
            $(this).select2();

            //Zone select2 on change
            $("#address_zone_id_" + index).on('change', function () {
                $.ajax({
                    url: _base_url + 'customer/getarea',
                    type: 'POST',
                    data: { zoneId: $(this).val() },
                    dataType: 'json',
                    success: function (response) {
                        // Clear the previous options
                        //$('#address_area_id_' + index).empty();
                        $('#address_area_id_' + index).html('<option value="">-- Select area --</option>').select2();
                        // Add the new options based on the response
                        $.each(response, function (index1, area) {

                            $('#address_area_id_' + index).append('<option value="' + area.area_id + '">' + area.area_name + '</option>');
                        });
                        $('#address_area_id_' + index).select2();
                    }
                });
            });

            // Area select2 on change
            $("#address_area_id_" + index).on('change', function () {
                $.ajax({
                    url: _base_url + 'customer/getlocation',
                    type: 'POST',
                    data: { area_id: $(this).val() },
                    dataType: 'json',
                    success: function (response) {
                        // Clear the previous options
                        //$('#address_location_id_' + index).empty();
                        $('#address_location_id_' + index).html('<option value="">-- Select location --</option>').select2();
                        // Add the new options based on the response
                        $.each(response, function (index2, location) {
                            $('#address_location_id_' + index).append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
                        });
                        $('#address_location_id' + index).select2();
                    }
                });
            })

            // Location select2 on change
            $("#address_location_id_" + index).on('change', function () {
                $.ajax({
                    url: _base_url + 'customer/getlandmark',
                    type: 'POST',
                    data: { location_id: $(this).val() },
                    dataType: 'json',
                    success: function (response) {
                        // Clear the previous options
                        // $('#address_landmark_id_' + index).empty();
                        $('#address_landmark_id_' + index).html('<option value="">-- Select location --</option>').select2();
                        // Add the new options based on the response
                        $.each(response, function (index3, landmark) {
                            $('#address_landmark_id_' + index).append('<option value="' + landmark.landmark_id + '">' + landmark.landmark_name + '</option>');
                        });
                        $('#address_landmark_id_' + index).select2();
                    }
                });
            })

            //copy address click
            $("#copy_address_id_" + index).click(function () {
                var copyText = document.getElementById("address_addresses_id_" + index);
                copyText.select();
                copyText.setSelectionRange(0, 99999); // For mobile devices
            
                // Copy the text inside the text field
                navigator.clipboard.writeText(copyText.value);
            });
        });
       
       add_address();
    }
    else {

    }
    // **********************************************************************************
    // Custom validation method for text characters only
    $.validator.addMethod("textOnly", function (value, element) {
        return value === '' || /^[A-Za-z\s]+$/.test(value);
    },);
    // **********************************************************************************

    customer_new_form_validator = $('#new_customer_form').validate({
        ignore: [],
        focusInvalid: false,
        rules: {
            is_company: {
                required: true,
            },
            customer_company_id: {
                required: '#is_company_2:checked',
            },
            customer_dealer_id: {
                required: '#is_company_3:checked',
            },
            customer_name: {
                required: true,
            },
            customer_code: {
                remote: {
                    url: _base_url + "customer/validate_customer_code",
                    type: "post",
                    data: {
                        form: 'new',
                        id: $("[name='customer_id']").val(),
                        customer_code: function () { return $("[name='customer_code']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            mobile_number_1: {
                maxlength: 12,
                required:true,
                remote: {
                    url: _base_url + "customer/editvalidatemobilenumber",
                    type: "post",
                    data: {
                        form: 'new',
                        id: $("[name='customer_id']").val(),
                        mobile: function () { return $("[name='mobile_number_1']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            whatsapp_no_1: {
                remote: {
                    url: _base_url + "customer/editvalidatewhatsnum",
                    type: "post",
                    data: {
                        form: 'new',
                        id: $("[name='customer_id']").val(),
                        whatsapp: function () { return $("[name='whatsapp_no_1']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            email: {
                email: true,
                remote: {
                    url: _base_url + "customer/editvalidateemail",
                    type: "post",
                    data: {
                        form: 'new',
                        id: $("[name='customer_id']").val(),
                        email_id: function () { return $("[name='email']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            mobile_number_2: {
                number: true,
                minlength: 9,
                maxlength: 12,
            },
            payment_frequency_id: {
                required: true,
            },
            payment_mode_id: {
                required: true,
            },
            is_flagged: {
                required: true,
            },
            pet: {
                required: true,
            },
            contact_person: {
                required: false,
                textOnly: true,
            },
            makkani_number: {
                required: true,
            },

            refer_source_id: {
                required: false,
            },
            residence_type: {
                required: true,
            },
            area_id: {
                required: true,
            },
            location_id: {
                required: true,
            },
            // initial_balance: {
            //     required: true,
            // },

            'address_zone_ids[]': {
                required: true,
            },
            'address_area_ids[]': {
                required: true,
            },
            'address_location_ids[]': {
                required: true,
            },
            'address_residence_type_ids[]': {
                required: true,
            },
            'address_apartment_nos[]': {
                required: true,
            },
            'address_addresses[]': {
                required: true,
            },
            'address_makkani_numbers[]': {
                required: false,
            },
        },
        messages: {
            mobile_number_1: {
                remote: "Mobile Number already exist"
            },
            whatsapp_no_1: {
                remote: "Whatsapp Number already exist"
            },
            email: {
                email: 'Please enter a valid email address.',
                remote: "Email already exist"
            },
            contact_person: {
                textOnly: "Please enter only text characters.",
            },
            customer_code: {
                remote: "Customer Code Already Exists",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "gender_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "customer_company_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "payment_frequency_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "payment_mode_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "refer_source_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "mobile_number_1") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "mobile_number_2") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "whatsapp_no_1") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "email") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("customer_code") === "customer_code") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
            console.log(element.attr("name"));
        }
    });
    // ********************************************************************************
    customer_edit_form_validator = $('#edit_customer_form').validate({
        ignore: [],
        focusInvalid: false,
        rules: {
            is_company: {
                required: true,
            },
            customer_company_id: {
                required: '#is_company_2:checked',
            },
            customer_dealer_id: {
                required: '#is_company_3:checked',
            },
            // gender_id: {
            //     required: false,
            // },
            customer_name: {
                required: true,
            },
            payment_frequency_id: {
                required: true,
            },
            payment_mode_id: {
                required: true,
            },
            is_flagged: {
                required: true,
            },
            pet: {
                required: true,
            },
            contact_person: {
                textOnly: true,
            },
            mobile_number_2: {
                number: true,
                minlength: 9,
                maxlength: 12,
            },
            makkani_number: {
                required: true,
            },
            customer_code: {
                remote: {
                    url: _base_url + "customer/validate_customer_code",
                    type: "post",
                    data: {
                        form: 'edit',
                        id: $("[name='customer_id']").val(),
                        customer_code: function () { return $("[name='customer_code']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            mobile_number_1: {
                maxlength: 12,
                remote: {
                    url: _base_url + "customer/editvalidatemobilenumber",
                    type: "post",
                    data: {
                        form: 'edit',
                        id: $("[name='customer_id']").val(),
                        mobile: function () { return $("[name='mobile_number_1']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            whatsapp_no_1: {
                remote: {
                    url: _base_url + "customer/editvalidatewhatsnum",
                    type: "post",
                    data: {
                        form: 'edit',
                        id: $("[name='customer_id']").val(),
                        whatsapp: function () { return $("[name='whatsapp_no_1']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },
            email: {
                email: true,
                remote: {
                    url: _base_url + "customer/editvalidateemail",
                    type: "post",
                    data: {
                        form: 'edit',
                        id: $("[name='customer_id']").val(),
                        email_id: function () { return $("[name='email']").val(); }
                    },
                    dataFilter: function (data) {
                        return (data === 'false') ? 'false' : 'true';
                    }
                }
            },

            // refer_source_id: {
            //     required: false,
            // },
            refer_source_note: {
                required: false,
            },
            residence_type: {
                required: true,
            },
            area_id: {
                required: true,
            },
            location_id: {
                required: true,
            },
            // initial_balance: {
            //     required: true,
            // },

            'address_location_ids[]': {
                required: true,
            },
            'address_residence_type_ids[]': {
                required: true,
            },
            'address_apartment_nos[]': {
                required: true,
            },
            'address_addresses[]': {
                required: true,
            },
            'address_makkani_numbers[]': {
                required: false,
            },
            'address_zip_codes[]': {
                required: false,
            },
        },
        messages: {
            email: {
                email: 'Please enter a valid email address.',
                remote: "Email already exist"
            },
            mobile_number_1: {
                remote: "Mobile Number already exist"
            },
            whatsapp_no_1: {
                remote: "Whatsapp Number already exist"
            },
            contact_person: {
                textOnly: "Please enter only text characters.",
            },
            customer_code: {
                remote: "Customer Code Already Exist",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "model_year") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "fuel_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "horsepower") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "mobile_number_1") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "mobile_number_2") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "whatsapp_no_1") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "email") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "contact_person") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") === "customer_code") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
            console.log(element.attr("name"));
        }
    });
    // ****************************************************************************************************
    // $('#edit_customer_form input').blur(function () {
    //     customer_edit_form_validator.element(this);
    // });
    // $('#new_customer_form input').blur(function () {
    //     customer_new_form_validator.element(this);
    // });
    // *****************************************************************************************************
    $("#new_customer_form #customer-save-btn").click(function () {
        customer_new_form_validator.resetForm();
        if ($("#new_customer_form").valid()) {
            loader(true);
            $('#new_customer_form-error').empty();
            $('#customer-save-btn').html('Saving...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "customer/save",
                data: $('#new_customer_form').serialize(),
                success: function (data) {
                    if (data.status == "success") {
                        $('#customer-save-btn').html('Saved');
                        loader(false);
                        toast('success', data.message);
                        window.location.href = _base_url + "customer/list";
                    }
                    else {
                        $('#new_customer_form-error').html(data.message);
                        $('#customer-save-btn').html('Save').removeAttr("disabled");
                        loader(false);
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    $('#new_customer_form-error').html("An error occured!");
                    $('#customer-save-btn').html('Save').removeAttr("disabled");
                    loader(false);
                    toast('error', data.message);
                },
            });
        }
        else {
            $('label.error').each(function () {
                if ($(this).css('display') != 'none') {
                    var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                    $('#mytabs li').removeClass('active');
                    $('#' + tab_id + '-li').addClass('active');
                    $('.tab-pane').removeClass('active').css('display', 'none');
                    $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                    //toast('warning', $(this).text());
                    return false;
                }
            });
        }
    });
    $("#edit_customer_form #customer-save-btn").click(function () {
        customer_edit_form_validator.resetForm();
        if ($("#edit_customer_form").valid()) {
            loader(true);
            $('#edit_customer_form #customer-save-btn').html('Updating...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "customer/update",
                data: $('#edit_customer_form').serialize(),
                success: function (data) {
                    if (data.status == "success") {
                        $('#edit_customer_form #customer-save-btn').html('Updated');
                        //$('#edit_customer_form #customer-save-btn').html('Update').removeAttr("disabled");
                        loader(false);
                        toast('success', data.message);
                        setTimeout(function () {
                            window.location.href = _base_url + "customer/list";
                        }, 3000);
                    }
                    else {
                        $('#edit_customer_form #customer-save-btn').html('Update').removeAttr("disabled");
                        loader(false);
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    $('#edit_customer_form #customer-save-btn').html('Update').removeAttr("disabled");
                    loader(false);
                    toast('error', data.statusText);
                },
            });
        }
        else {
            $('label.error').each(function () {
                if ($(this).css('display') != 'none') {
                    var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                    $('#mytabs li').removeClass('active');
                    $('#' + tab_id + '-li').addClass('active');
                    $('.tab-pane').removeClass('active').css('display', 'none');
                    $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                    toast('warning', "Fill required fields to continue.");
                    return false;
                }
            });
        }
    });
});



$('#mytabs li').click(function () {
    var tab_id = $(this).attr("data-tab");
    $('.tab-panel').removeClass('active').css('display', 'none');
    $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
});
$('[data-action="next"]').click(function () {
    var current_active = $('#mytabs li.active');
    $('#mytabs li').removeClass('active');
    $(current_active).next('li').addClass('active').click();
});
$("[name='is_flagged']").click(function () {
    if ($(this).val() == "Y") {
        $('.flag_reason').show(250);
    }
    else {
        $('#flag_reason').val("");
        $('.flag_reason').hide(250);
    }
});
$("[name='customer_type_id']").change(function () {
    $('#customer_company_id').val("");
    $('#customer_dealer_id').val("");
    $('#customer_company').hide(250);
    $('#customer_dealer').hide(250);
    if ($(this).val() == 2) {
        $('#customer_company').show(250);
    }
    else if ($(this).val() == 3) {
        $('#customer_dealer').show(250);
    }
    else {
        $('#customer_company_id').val("");
        $('#customer_dealer_id').val("");
        $('#customer_company').hide(250);
        $('#customer_dealer').hide(250);
    }
});
$("[name='is_allowed_new_booking']").click(function () {
    if ($(this).val() != "YES") {
        $('.blocked_new_booking_reason').show(250);
    }
    else {
        $('#blocked_new_booking_reason').val("");
        $('.blocked_new_booking_reason').hide(250);
    }
});

$('button[data-action="add-new-address"]').click(function () {
    var adress_count = $('[name="address_zone_ids[]"]').length;
    var last_sel_zone = $("#address_zone_id_" + (adress_count - 1)).select2().val();
    //area select2
    var adress_area_count = $('[name="address_area_ids[]"]').length;
    var last_sel_area = $("#address_area_id_" + (adress_area_count - 1)).select2().val();

    //location select2
    var adress_location_count = $('[name="address_location_ids[]"]').length;
    var last_sel_location = $("#address_location_id_" + (adress_location_count - 1)).select2().val();

    //landmark select2
    var adress_landmark_count = $('[name="address_landmark_ids[]"]').length;
    var last_sel_landmark = $("#address_landmark_id_" + (adress_landmark_count - 1)).select2().val();

    $('#address-list').append(address_html);
    /********************************************* */
    $("[name='address_area_ids[]']").each(function (index, element) {

        $(this).off('change');
        $(this).attr({
            id: 'address_area_id_' + index,
        });

        if (index == adress_area_count) {
            $("#address_area_id_" + index).select2();
            $("#address_area_id_" + adress_area_count).val(last_sel_area); // Select the option
            $("#address_area_id_" + adress_area_count).trigger('change'); // Notify any JS components that the value changed
        }

    });
    /********************************************* */
    $("[name='address_location_ids[]']").each(function (index, element) {
        $(this).off('change');
        $(this).attr({
            id: 'address_location_id_' + index,
        });
        if (index == adress_location_count) {
            $("#address_location_id_" + index).select2();
            $("#address_location_id_" + adress_location_count).val(last_sel_location); // Select the option
            $("#address_location_id_" + adress_location_count).trigger('change'); // Notify any JS components that the value changed
        }

    });
    /********************************************* */
    $("[name='address_landmark_ids[]']").each(function (index, element) {
        $(this).off('change');
        $(this).attr({
            id: 'address_landmark_id_' + index,
        });
        if (index == adress_landmark_count) {
            $("#address_landmark_id_" + index).select2();
            $("#address_landmark_id_" + adress_landmark_count).val(last_sel_landmark); // Select the option
            $("#address_landmark_id_" + adress_landmark_count).trigger('change'); // Notify any JS components that the value changed
        }

    });
    /********************************************* */
    // $("[name='address_status[]']").each(function (index, element) {
    //     $(this).attr({
    //         id: 'address_status_' + index,
    //     });
    // });
    /********************************************* */
    $("[name='address_zone_ids[]']").each(function (index, element) {
        //$(this).off('change');
        $(this).rules("add", { required: true });
        $(this).attr({
            id: 'address_zone_id_' + index,
        });

        if (index == adress_count) {
            $("#address_zone_id_" + index).select2();
            $("#address_zone_id_" + adress_count).val(last_sel_zone); // Select the option
            $("#address_zone_id_" + adress_count).trigger('change'); // Notify any JS components that the value changed
        }
        //Zone select2 on change
        $("#address_zone_id_" + index).on('change', function () {
            $.ajax({
                url: _base_url + 'customer/getarea',
                type: 'POST',
                data: { zoneId: $(this).val() },
                dataType: 'json',
                success: function (response) {
                    // Clear the previous options
                    //$('#address_area_id_' + index).empty();
                    $('#address_area_id_' + index).html('<option value="">-- Select area --</option>').select2();
                    // Add the new options based on the response
                    $.each(response, function (index1, area) {

                        $('#address_area_id_' + index).append('<option value="' + area.area_id + '">' + area.area_name + '</option>');
                    });
                    $('#address_area_id_' + index).select2();
                }
            });
        })

        // Area select2 on change
        $("#address_area_id_" + index).on('change', function () {
            $.ajax({
                url: _base_url + 'customer/getlocation',
                type: 'POST',
                data: { area_id: $(this).val() },
                dataType: 'json',
                success: function (response) {
                    // Clear the previous options
                    //$('#address_location_id_' + index).empty();
                    $('#address_location_id_' + index).html('<option value="">-- Select location --</option>').select2();
                    // Add the new options based on the response
                    $.each(response, function (index2, location) {
                        $('#address_location_id_' + index).append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
                    });
                    $('#address_location_id' + index).select2();
                }
            });
        })

        // Location select2 on change
        $("#address_location_id_" + index).on('change', function () {
            $.ajax({
                url: _base_url + 'customer/getlandmark',
                type: 'POST',
                data: { location_id: $(this).val() },
                dataType: 'json',
                success: function (response) {
                    // Clear the previous options
                    // $('#address_landmark_id_' + index).empty();
                    $('#address_landmark_id_' + index).html('<option value="">-- Select location --</option>').select2();
                    // Add the new options based on the response
                    $.each(response, function (index3, landmark) {
                        $('#address_landmark_id_' + index).append('<option value="' + landmark.landmark_id + '">' + landmark.landmark_name + '</option>');
                    });
                    $('#address_landmark_id_' + index).select2();
                }
            });
        })

    });
    add_address();
    copy_address();
});

function add_address() {
    $("[name='add_address[]']").each(function (index, element) {
        $(this).attr({
            id: 'add_address_id_' + index,
        });
      
        $("#add_address_id_"+ index).click(function () {
            
            var count1 = $('[name="address_addresses[]"]').length;
            $("[name='address_addresses[]']").each(function (index1, element) {

                $(this).off('change');
                $(this).attr({
                    id: 'address_addresses_id_' + index1,
                });
            });

            $("[name='address_building[]']").each(function (index2, element) {

                $(this).off('change');
                $(this).attr({
                    id: 'address_building_id_' + index2,
                });
            });

            $("[name='address_unit_nos[]']").each(function (index3, element) {

                $(this).off('change');
                $(this).attr({
                    id: 'address_unit_nos_id_' + index3,
                });
            });

            $("[name='address_residence_type_ids[]']").each(function (index4, element) {

                $(this).off('change');
                $(this).attr({
                    id: 'address_residence_id_' + index4,
                });
            });
            $("[name='address_status[]']").each(function (index5, element) {

                $(this).off('change');
                $(this).attr({
                    id: 'address_status_id_' + index5,
                });
            });

            // var area =$("#address_area_id_" + (index)).select2().val();

            var area_id = "#address_area_id_" + index;
            var area = $(area_id + ' option:selected').text();
            var location_id = "#address_location_id_" + index;
            var location = $(location_id + ' option:selected').text();
            var landmark_id = "#address_landmark_id_" + index;
            var landmark = $(landmark_id + ' option:selected').text();
            var residence_id = "#address_residence_id_" + index;
            var residence = $(residence_id + ' option:selected').text();
            var building = $("#address_building_id_" + index).val();
            var unit_no = $("#address_unit_nos_id_" + index).val();
            if (building == "") {
                $("#address_addresses_id_" + index).val(residence + ',' + unit_no +',' + landmark + ',' + location + ',' + area);
            } else if (unit_no == ""){
                $("#address_addresses_id_" + index).val(residence + ',' + building +',' + landmark + ',' + location + ',' + area);
                 
            } else {
                $("#address_addresses_id_" + index).val(residence + ',' + building + ',' + unit_no +',' + landmark + ',' + location + ',' + area);
            }

        });

    });
}

function copy_address() {
    $("[name='copy_address[]']").each(function (index, element) {
        $(this).attr({
            id: 'copy_address_id_' + index,
        });
      
        $("#copy_address_id_"+ index).click(function () {
            
            $("[name='address_addresses[]']").each(function (index1, element) {

              $(this).off('change');
                $(this).attr({
                    id: 'address_addresses_id_' + index1,
                });
            });
            var copyText = document.getElementById("address_addresses_id_" + index);
            copyText.select();
            copyText.setSelectionRange(0, 99999); // For mobile devices
          
            // Copy the text inside the text field
            navigator.clipboard.writeText(copyText.value);
        });

    });
}
/************************************************************************** */
$('#new_customer_form input[name="initial_balance_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$('#edit_customer_form input[name="initial_balance_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
/************************************************************************************************************************************ */
$('[data-action="new-access-popup"]').click(function () {
    new_access_validator.resetForm();
    $('#new-access-form select[name="access_type"],#new-access-form select[name="access"]').select2().val("").trigger("change.select2");
    $('#new-access-form select[name="access"]').html("").select2({});
    fancybox_show('new-access-popup', { width: 350 });
});
$('#new-access-form select[name="access_type"]').select2({});
$(document.body).on("change", '#new-access-form select[name="access_type"]', function () {
    $(this).valid();
    get_available_accesses();
});
$(document.body).on("change", '#new-access-form select[name="access"]', function () {
    $(this).valid();
});
function get_available_accesses() {
    $('#new-access-form select[name="access"]').html("").select2({});
    var access_type = $('#new-access-form select[name="access_type"]').val();
    if (access_type) {
        loader(true);
        $.ajax({
            type: "GET",
            url: _base_url + "access/get_available",
            data: {
                access_type: access_type
            },
            dataType: "json",
            cache: false,
            success: function (data) {
                var html = ""
                $.each(data, function (key, access) {
                    if ($('input[id="access_' + access.id + '"').length > 0) {
                        // exist in form
                    }
                    else {
                        html += `<option value="` + access.id + `">` + access.code + `</option>`;
                    }
                });
                $('#new-access-form select[name="access"]').html(html).select2({});
                $('#new-access-form select[name="access"]').valid();
                loader(false);
            },
            error: function (data) {
                loader(false);
            }
        });
    }
}
$().ready(function () {
    new_access_validator = $('#new-access-form').validate({
        ignore: [],
        rules: {
            access_type: {
                required: true,
            },
            access: {
                required: true,
            },
            received_notes: {
                required: false,
            }
        },
        messages: {
            access_type: "Select access type.",
            access: "Select access.",
            received_notes: "Enter receive notes.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "access_type") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "access") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            let access_id = $(form).find('select[name="access"]').val();
            let access_code = $(form).find('select[name="access"]').select2('data')[0].text;
            var html = `<input id="access_` + access_id + `" type="checkbox" value="` + access_id + `" name="access[]" checked>
                                    <label for="access_` + access_id + `">
                                       <span class="border-radius-3"></span>
                                       <p>` + access_code + `</p>
                                    </label>`;
            $('#access-holder').append(html);
            close_fancybox();
        }
    });
});
// *********************************************************

function petselection(value) {
    var petTypeField = document.getElementById('pettype');

    if (value === 'Y') {
        petTypeField.style.display = 'block';
    } else {
        petTypeField.style.display = 'none';
    }
}
// ******************************************************



