/************************************* Coder : Samnad. S ****************************** */
// This js file is used in the entire site
/************************************* HIGHLIGHT ACTIVE MENU *************************** */
$(function () {
    var current = window.location.href;
    $('#primary_nav_wrap li a').each(function () {
        var $this = $(this);
        // if the current path is like this link, make it active
        if ($this.attr('href') === current) {
            $this.addClass('active');
        }
    })
});
function fancybox_show(id, settings = {}) {
    width = settings.width || 450;
    height = settings.height || 'auto';
    closeClick = settings.closeClick || false;
    padding = settings.padding || 0;
    $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        closeSpeed: 1,
        autoSize: false,
        width: width,
        height: height,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgb(57 92 106 / 60%)'
                },
                closeClick: closeClick
            }
        },
        padding: 0,
        closeBtn: false,
        content: $('#' + id),
    });
    $('.mm-loader').hide();
}
function loader(show) {
    if (show == true) {
        $('.mm-loader').show();
    }
    else {
        $('.mm-loader').hide();
    }
}
function toast(type, message, options = {}) {
    loader(false);
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    if (type == 'error') {
        toastr.error(message);
    }
    else if (type == 'success') {
        toastr.success(message);
    }
    else if (type == 'warning') {
        toastr.warning(message);
    }
    else if (type == 'info') {
        toastr.info(message);
    }
}

if ($('.sel2').length > 0) $('.sel2').select2();
$('[data-action="close-fancybox"]').click(function (event) {
    $.fancybox.close();
});
function close_fancybox() {
    $('.mm-loader').hide();
    $.fancybox.close();
}