$('.datepicker').datepicker().on('changeDate', function (ev) {
	var _date = new Date(ev.date);
	var _new_date = ('0' + _date.getDate()).slice(-2) + '-' + ('0' + (_date.getMonth() + 1)).slice(-2) + '-' + _date.getFullYear();
	window.location = _base_url + 'booking/' + _new_date;
});

var _maids = $.parseJSON($('#all-maids').val());
var _time_slots = $.parseJSON($('#time-slots').val());
var _bpop_open = false;

$('#start-date, #end-date').datepicker({
	format: 'dd/mm/yyyy',
	autoclose: true,
	//startDate: new Date()
});

function apply_selectable() {
	$('.selectable').selectable({
		start: function (event, ui) {
			$('.ui-selected').removeClass('ui-selected');
			_bpop_open = true;
		},
		stop: function (event, ui) {
			var _maid_id = $('#search-maid-id').val();
			var _service_date = $(this).parent().attr('id').replace('maid-', '');
			var d = new Date();
			var month = d.getMonth() + 1;
			var day = d.getDate();
			var strDate = d.getFullYear() + '-' + (('' + month).length < 2 ? '0' : '') + month + '-' + (('' + day).length < 2 ? '0' : '') + day;
			//var twoDigitMonth = ((d.getMonth().length+1) === 1)? (d.getMonth()+1) : '0' + (d.getMonth()+1);
			//var strDate = d.getFullYear() + "-" + twoDigitMonth + "-" + d.getDate();
			//alert(strDate);
			var _time_from_index = $('.ui-selected').first().attr('id').replace('t-', '');
			var _time_to_index = $('.ui-selected').last().attr('id').replace('t-', '');
			_time_to_index = typeof _time_slots['t-' + parseInt(parseInt(_time_to_index) + 1)] != 'undefined' ? parseInt(_time_to_index) + 1 : 0;
			var _schedule_type = 'new';
			if (_maid_id == 0) {
				$('#alert-title').html('Error !');
				$('#alert-message').html('Please select maid to continue !');
				fancybox_show('alert-popup');
			} else {
				if (_service_date < strDate) {
					$('#alert-title').html('Error !');
					$('#alert-message').html('Booking not allowed for previous days !');
					fancybox_show('alert-popup');
				} else {
					open_bpop(_service_date, _maid_id, _time_from_index, _time_to_index, _schedule_type);
				}
			}
		}
	});
}
var screen_width = $('body').innerWidth();
//var scheduler_width = screen_width - 20;
//var time_grid_width = scheduler_width - 185 - 3;

var scheduler_width = screen_width - 20 + 17;
var time_grid_width = scheduler_width - 195 - 3 + 8;

//$('#schedule').css('width', scheduler_width + 'px');
//$('#schedule .time_line').css('width', (scheduler_width - 145 - 20 - 34 - 10) + 'px');
//$('#schedule .time_grid').css('width', time_grid_width + 'px');

$('#schedule').css('width', scheduler_width + 'px');
$('#schedule .time_line').css({'width' : (scheduler_width - 145 - 20 - 34 - 20 -3) + 'px'});
$('#schedule .time_grid').css('width', time_grid_width + 'px');


/*
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 20 + 'px';
var time_grid_width = '85%'//scheduler_width - 185 - 3 + 20;//1118px
//$('#schedule').css('width', scheduler_width );
$('#schedule').css('width', '100%' );
$('#schedule .time_line').css('width', time_grid_width);
//$('#schedule .time_line').css('width', (scheduler_width - 145 - 20 - 34 - 10 + 47-306) + 'px');
//$('#schedule .time_grid').css('width', time_grid_width + 'px');
$('#schedule .time_grid').css({'width': time_grid_width, 'float' : 'left'});
*/

var _booking_popup = $('#booking-popup').html();
$('#booking-popup').remove();
$('select.sel2').select2();

function open_bpop(service_date, maid_id, time_from_index, time_to_index, schedule_type) {
	_bpop_open = true;
	var _booking_popup_parsed = $.parseHTML(_booking_popup);
	var _maid_id = maid_id;

	if (typeof _maids['m-' + _maid_id] == 'undefined') {
		window.location = _page_url;
	}

	$(_booking_popup_parsed).find('#b-maid-name').text(_maids['m-' + _maid_id]);
	$(_booking_popup_parsed).find('#b-time-slot').text('(' + _time_slots['t-' + time_from_index].display + ' to ' + _time_slots['t-' + time_to_index].display + ')');
	$(_booking_popup_parsed).find('#maid-id').val(_maid_id);
	$(_booking_popup_parsed).find('#service-date').val(service_date);

	if (schedule_type == 'new') {
		$(_booking_popup_parsed).find('#copy-booking, #delete-booking').remove();
	}

	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 830,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: _booking_popup_parsed,
		topRatio: 0.2,
		beforeShow: function () {
			$('#b-from-time').val(_time_slots['t-' + time_from_index].stamp);
			refresh_to_time();
			$('#b-to-time').val(_time_slots['t-' + time_to_index].stamp);
			$('select.sel2').select2();
			$("#b-customer-id").select2({
				ajax: {
					url: _base_url + "customer/report_srch_usr_news",
					type: "post",
					dataType: 'json',
					delay: 150,
					data: function (params) {
						return {
							searchTerm: params.term, // search term
						};
					},
					processResults: function (response) {
						if (response.id === "") {

						} else {
							return { results: response };
						}
					},
					cache: true
				},
			});
			$('.fancybox-skin').css({
				'border-radius': '20px'
			});
			$('.end_datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date($('#repeate-end-start').val())
			});
		}
	});
}

$('body').on('change', '#b-booking-type', function () {
	var _booking_type = $(this).val();
	if (_booking_type == 'WE' || _booking_type == 'BW') {
		$('#repeat-days').css('display', 'block');
		$('#repeat-ends').css('display', 'block');
	}
	else {
		$('#repeat-days').hide();
		$('#repeat-ends').hide();
	}
});

$('body').on('change', 'input[name="repeat_end"]', function () {
	if ($(this).val() == 'ondate') {
		$('#repeat-end-date').removeAttr('disabled');
	}
	else {
		$('#repeat-end-date').attr('disabled', 'disabled');
	}
});

$('#schedule-grid-rows').on('click', '.schedule_bubble', function () {
	_bpop_open = true;
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_id = $(this).attr('id').replace('booking-', '');
	var _service_date = $(this).attr('data-bind');
	var _booking_popup_parsed = $.parseHTML(_booking_popup);
	$(_booking_popup_parsed).find('#b-maid-name').text(_maids['m-' + _all_bookings[_booking_id].maid_id]);
	$(_booking_popup_parsed).find('#b-time-slot').text('(' + _all_bookings[_booking_id].time_from + ' to ' + _all_bookings[_booking_id].time_to + ')');
	var _address_change_html = '';
	$.post(_page_url, { action: 'get-no-of-customer-address', customer_id: _all_bookings[_booking_id].customer_id }, function (response) {
		if (response == 'refresh') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
			});

			_refresh_page = true;
		}
		else {
			var _resp = $.parseJSON(response);

			$.ajax({
				type: "POST",
				//url: _base_url + 'settings/check_hourly_price',
				url: _base_url + 'settings/check_hourly_price_new',
				data: { customer_id: _all_bookings[_booking_id].customer_id },
				cache: false,
				success: function (response) {
					if (response == '') {
						response = '';
					}
					if (response != null) {

						$('#b-rate_per_hr').val(response);
					}

				}

			});

			if (_resp.no_of_address > 1) {
				_address_change_html = '<span id="chg-cust-address">Change</span>';

			}
			$(_booking_popup_parsed).find('#customer-picked-address').html('<div class="address"><strong>' + _all_bookings[_booking_id].customer_zone + ' - ' + _all_bookings[_booking_id].customer_area + '</strong> - ' + _all_bookings[_booking_id].customer_address + '</div><div class="action">' + _address_change_html + '</div><div class="clear"></div>');



		}
	});


	$(_booking_popup_parsed).find('#save-booking').remove();

	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		autoSize: false,
		width: 800,
		height: 'auto',
		openSpeed: 1,
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: _booking_popup_parsed,
		beforeShow: function () {
			$('#b-customer-id').parent().prepend(_all_bookings[_booking_id].customer_nick_name + " - " + _all_bookings[_booking_id].mobile_number_1);
			//$('#b-customer-id').parent().prepend($('#b-customer-id option[value="' + _all_bookings[_booking_id].customer_id + '"]').text());
			$('#b-customer-id').remove();
			$('#b-customer-id-cell').append('<input type="hidden" id="b-customer-id" value="' + _all_bookings[_booking_id].customer_id + '" />');
			$('#customer-address-id').val(_all_bookings[_booking_id].customer_address_id);

			$('#b-service-type-id').parent().prepend($('#b-service-type-id option[value="' + _all_bookings[_booking_id].service_type_id + '"]').text());
			$('#b-service-type-id').remove();

			$('#b-vaccum-cleaning').parent().parent().prepend(_all_bookings[_booking_id].cleaning_type);
			$('#b-vaccum-cleaning').parent().remove();

			$('#b-pending-amount').parent().prepend(_all_bookings[_booking_id].pending_amount);
			$('#b-pending-amount').remove();

			$('#b-discount').parent().prepend(_all_bookings[_booking_id].discount);
			$('#b-discount').remove();

			$('#b-from-time').val(_all_bookings[_booking_id].time_from_stamp);
			refresh_to_time();
			$('#b-to-time').val(_all_bookings[_booking_id].time_to_stamp);
			$('#tot_amout').val(_all_bookings[_booking_id].total_amount);
			if (_all_bookings[_booking_id].is_locked == 1) {
				$('#lock-booking').prop('checked', true);
			}

			if (_all_bookings[_booking_id].cleaning_type == 'Y') {
				$('#b-cleaning-materials').prop('checked', true);
			}

			if (_all_bookings[_booking_id].booking_type == 'OD') {
				$('#b-booking-type').parent().prepend($('#b-booking-type option[value="' + _all_bookings[_booking_id].booking_type + '"]').text() + ' (' + _all_bookings[_booking_id].service_end_date + ')');
			}
			else {
				$('#b-booking-type').parent().prepend($('#b-booking-type option[value="' + _all_bookings[_booking_id].booking_type + '"]').text() + ' (' + _all_bookings[_booking_id].service_week_day + 's)');
			}
			$('#b-booking-type').remove();

			if (_all_bookings[_booking_id].booking_type != 'OD') {
				$('#repeat-ends').css('display', 'block');
				if (_all_bookings[_booking_id].service_end == 1) {
					$('#repeat-end-ondate').attr('checked', 'checked');
					$('#repeat-end-date').removeAttr('disabled');
					$('#repeat-end-date').val(_all_bookings[_booking_id].service_end_date);
				}
				else {
					$('#repeat-end-never').attr('checked', 'checked');
				}
			}


			$('#booking-note').val(_all_bookings[_booking_id].booking_note);
			$('#booking-id').val(_booking_id);
			$('#service-date').val(_service_date)
			$('select.sel2').select2();
			$('.fancybox-skin').css({
				'border-radius': '20px'
			});
			$('.end_datepicker').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				startDate: new Date($('#repeate-end-start').val())
			});
		}
	});
});

$('body').on('click', '#delete-booking', function () {
	var _booking_id = $('#booking-id').val();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_type = _all_bookings[_booking_id].booking_type;

	if (_booking_type == 'OD') {
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-permanent" value="Delete Booking" /><input type="button" id="delete-cancel" value="Cancel"></div>');
	}
	else {
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="delete-one-day" value="Delete One Day" /><input type="button" id="delete-permanent" value="Delete Permanently" /><input type="button" id="delete-cancel" value="Cancel"></div>');
	}

	$('#booking-action-confirm-panel').slideDown();
});

$('body').on('click', '#update-booking', function () {
	var _booking_id = $('#booking-id').val();
	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_type = _all_bookings[_booking_id].booking_type;

	if (_booking_type == 'OD') {
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-permanent" value="Modify this Booking" /><input type="button" id="update-cancel" value="Cancel"></div>');
	}
	else {
		$(this).parent().before('<div id="booking-action-confirm-panel"><input type="button" id="update-one-day" value="Change One Day" /><input type="button" id="update-permanent" value="Change Permanently" /><input type="button" id="update-cancel" value="Cancel"></div>');
	}

	$('#booking-action-confirm-panel').slideDown();
});

$('body').on('click', '#delete-cancel, #update-cancel', function () {
	$('#booking-action-confirm-panel').slideUp(function () { $('#booking-action-confirm-panel').remove(); });
});

$('body').on('click', '#delete-permanent', function () {
	var _booking_id = $('#booking-id').val();
	$.post(_page_url, { action: 'delete-booking-permanent', service_date: $('#service-date').val(), booking_id: _booking_id, service_date: $('#service-date').val() }, function (response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if (response == 'success') {
			$('#alert-title').html('Success !');
			$('#alert-message').html('Booking has been deleted successfully.');
			fancybox_show('alert-popup');
			//_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false
		}
		else {
			if (response == 'locked') {
				$('#alert-title').html('Error !');
				$('#alert-message').html('This booking is locked by another user.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = false;
			} else if (response == 'odoorefresh') {
				$('#alert-title').html('Error !');
				$('#alert-message').html('Delete Failed. Booking alreday synched to odoo.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Delete Failed. Booking alreday synched to odoo.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = false;
			}
			else {
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true;
			}
		}

		/*if (_alert_html != '') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: _alert_html
			});
		}*/
	});
});

$('body').on('click', '#delete-one-day', function () {
	//alert();
	//return;
	var _booking_id = $('#booking-id').val();
	$.post(_page_url, { action: 'delete-booking-one-day', booking_id: _booking_id, service_date: $('#service-date').val() }, function (response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';
		if (response == 'success') {
			$('#alert-title').html('Success !');
			$('#alert-message').html('Booking has been deleted successfully.');
			fancybox_show('alert-popup');
			//_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been deleted successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
			_refresh_page = false
		}
		else {
			if (response == 'locked') {
				$('#alert-title').html('Error !');
				$('#alert-message').html('This booking is locked by another user.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true;
			} else if (response == 'odoorefresh') {
				$('#alert-title').html('Error !');
				$('#alert-message').html('Delete failed. Booking already synched to odoo.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Delete failed. Booking already synched to odoo.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true;
			}
			else {
				$('#alert-title').html('Error !');
				$('#alert-message').html('An unexpected error. Please try again.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true;
			}

		}

		/*if (_alert_html != '') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: _alert_html
			});
		}*/
	});
});
$('#tb-slide-right').click(function () {
	var position = $('.time_line .time_slider').position();
	//var max_left = time_grid_width - 2400 + 3;
	//var max_left = time_grid_width - 4800 + 3;
	//var left_pos = position.left - 200;
	
	var max_left = time_grid_width - 2775 + 3 + 5;
	var left_pos = position.left - 200 + 5;
	
	left_pos = left_pos < max_left ? max_left : left_pos;
	//$('.time_line .time_slider').animate({left: left_pos}, 800);
	$('.time_line .time_slider').animate({ left: left_pos }, 200);
	//$('.grids').animate({left: left_pos }, 800);
	$('.grids').animate({ left: left_pos }, 200);
});

//$('#tb-slide-left').click(function () {
//	var position = $('.time_line .time_slider').position();
//	var left_pos = position.left + 200;
//	left_pos = left_pos > 0 ? 0 : left_pos;
//	//$('.time_line .time_slider').animate({left: left_pos }, 800);
//	//$('.grids').animate({left: left_pos }, 800);
//	$('.time_line .time_slider').animate({ left: left_pos }, 10);
//	$('.grids').animate({ left: left_pos }, 10);
//});


$('#tb-slide-left').click(function() {
	var position = $('.time_line .time_slider').position();
	var left_pos = position.left + 200 + 5;
	left_pos = left_pos > 0 ? 0 : left_pos;
	//$('.time_line .time_slider').animate({left: left_pos }, 100);
	$('.time_line .time_slider').animate({left: left_pos }, 200);
	$('.grids').animate({left: left_pos }, 200);
	//$('.grids').animate({left: left_pos }, 100);
});


/*
$('#tb-slide-right').click(function() {
	var position = $('.time_line .time_slider').position();
	var max_left = time_grid_width - 4800 + 3;
	var left_pos = position.left - 200;
	left_pos = left_pos < max_left ? max_left : left_pos;
	$('.time_line .time_slider').animate({left: left_pos}, 800);
	$('.grids').animate({left: left_pos }, 800);
});

$('#tb-slide-left').click(function() {
	var position = $('.time_line .time_slider').position();
	var left_pos = position.left + 200;
	left_pos = left_pos > 0 ? 0 : left_pos;
	$('.time_line .time_slider').animate({left: left_pos }, 800);
	$('.grids').animate({left: left_pos }, 800);
});
*/

$('body').on('change', '#b-from-time', function () {
	refresh_to_time();
	$('#b-time-slot').text('');
});

function refresh_to_time() {
	//$("#b-to-time").select2("val", "");
	$("#b-to-time").val('');
	var _selected_index = $("#b-from-time")[0].selectedIndex;

	var _time_to_options = '<option></option>';
	var _i = 0;
	var _last_index;
	$('#b-from-time option').each(function (index, option) {
		if (index > _selected_index) {
			_time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
			_last_index = index;
			_i++;
		}
	});


	if (_i == 0) {
		_time_to_options += '<option value="">No Time</option>';
	}

	$('#b-to-time').html(_time_to_options);
}

$('body').on('change', '#b-to-time', function () {
	if ($("#b-from-time").val() != '' && $("#b-to-time").val() != '') {
		$('#b-time-slot').text('(' + $("#b-from-time option:selected").text() + ' to ' + $("#b-to-time option:selected").text() + ')');

		var _booking_id = $('#booking-id').val();

		if (_booking_id != '') {
			chk_booking_chg(_booking_id);
		}
	}
	else {
		$('#b-time-slot').text('');
	}
});

$('body').on('change', '#b-customer-id', function () {
	var _customer_id = $(this).val();
	$('#customer-picked-address').html('');
	get_no_of_address(_customer_id);
	if (_customer_id > 0) {
		$.ajax({
			type: "POST",
			//url: _base_url + 'settings/check_hourly_price',
			url: _base_url + 'settings/check_hourly_price_new',
			data: { customer_id: _customer_id },
			cache: false,
			success: function (response) {
				if (response == '') {
					response = '';
				}
				if (response != null) {
					$('#b-rate_per_hr').val(response);
				}

			}
		});
	} else {
		$('#b-rate_per_hr').val();
	}
	//open_address_panel(_customer_id);
});
function get_no_of_address(customer_id) {
	$.post(_page_url, { action: 'get-no-of-customer-address', customer_id: customer_id }, function (response) {
		if (response == 'refresh') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
			});

			_refresh_page = true;
		}
		else {
			var _resp = $.parseJSON(response);
			var _address_html = '<div class="table">';

			if (_resp.no_of_address == 1) {
				$.each(_resp.address, function (key, val) {
					var _address_id = val.customer_address_id;
					$('#customer-address-id').val(_address_id);
					var _addzone = '<strong>' + val.zone_name + ' - ' + val.area_name + '</strong>';
					var _address = val.customer_address;
					$('#customer-address-panel').slideUp(function () {
						$('#customer-picked-address').html('<div class="address"><strong>' + _addzone + '</strong> - ' + _address + '</div><div class="action"></div><div class="clear"></div>');
						$('#customer-picked-address').show();
						$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');

						var _booking_id = $('#booking-id').val();

						if (_booking_id != '') {
							chk_booking_chg(_booking_id);
						}
					});
				});
			}
			else {
				open_address_panel(customer_id)
			}



		}
	});
}
function open_address_panel(customer_id) {
	$('#customer-address-id').val('');
	$('#customer-address-panel').hide();
	$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');

	if ($.isNumeric(customer_id) && customer_id > 0) {
		$('#customer-address-panel').slideDown();

		$.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function (response) {
			if (response == 'refresh') {
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'fade',
					openSpeed: 100,
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding: 0,
					closeBtn: false,
					content: '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});

				_refresh_page = true;
			}
			else {
				var _resp = $.parseJSON(response);
				var _address_html = '<div class="table">';

				$.each(_resp, function (key, val) {
					_address_html += '<div class="row n-pick-maid-c-main m-0"><div class="col-sm-8 n-pick-maid-left pl-0"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="col-sm-4 n-pick-maid-right pr-0"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
				});

				_address_html += '</div>';

				$('#customer-address-panel .inner').html(_address_html);




			}
		});
	}
}


$('body').on('click', '.pick_customer_address', function () {
	var _address_id = $(this).attr('id').replace('cadddress-', '');
	$('#customer-address-id').val(_address_id);
	var _addzone = $('#caddzone-' + _address_id).html();
	var _address = $('#cadd-' + _address_id).html();
	$('#customer-address-panel').slideUp(function () {
		$('#customer-picked-address').html('<div class="address"><strong>' + _addzone + '</strong> - ' + _address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
		$('#customer-picked-address').show();
		$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');

		var _booking_id = $('#booking-id').val();

		if (_booking_id != '') {
			chk_booking_chg(_booking_id);
		}
	});
});

$('body').on('click', '#lock-booking', function () {
	var _booking_id = $('#booking-id').val();

	if (_booking_id != '') {
		chk_booking_chg(_booking_id);
	}
});

$('body').on('click', '#b-cleaning-materials', function () {
	
	var _booking_id = $('#booking-id').val();

	if (_booking_id != '') {
		chk_booking_chg(_booking_id);
	}
});

$('body').on('keyup', '#booking-note', function () {
	var _booking_id = $('#booking-id').val();

	if (_booking_id != '') {
		chk_booking_chg(_booking_id);
	}
});

$('body').on('keyup', '#tot_amout', function () {
	var _booking_id = $('#booking-id').val();

	if (_booking_id != '') {
		chk_booking_chg(_booking_id);
	}
});

$('body').on('change', 'input[name="repeat_end"]', function () {
	if ($('#repeat-end-never').is(':checked')) {
		$('#repeat-end-date').val('');
	}

	var _booking_id = $('#booking-id').val();

	if (_booking_id != '') {
		chk_booking_chg(_booking_id);
	}
});

$('body').on('change', '#repeat-end-date', function () {
	var _booking_id = $('#booking-id').val();

	if (_booking_id != '') {
		chk_booking_chg(_booking_id);
	}
});

function chk_booking_chg(booking_id) {
	if ($.isNumeric(booking_id) && booking_id > 0) {
		var _all_bookings = $.parseJSON($('#all-bookings').val());
		var _booking_locked = $('#lock-booking').is(':checked') ? 1 : 0;
		var _pending_amount = $.trim($('#b-pending-amount').val());
		var _discount = $.trim($('#b-discount').val());
		var _booking_note = $.trim($('#booking-note').val());

		if (_all_bookings[booking_id].booking_type != 'OD') {
			var _repeat_end = $('#repeat-end-never').is(':checked') ? 0 : 1;
			if (_repeat_end == 1) {
				var _repeat_end_date = $('#repeat-end-date').val();
			}
		}

		if (_all_bookings[booking_id].customer_address_id != $('#customer-address-id').val() || _all_bookings[booking_id].time_from_stamp != $('#b-from-time').val() || _all_bookings[booking_id].is_locked != _booking_locked || _all_bookings[booking_id].time_to_stamp != $('#b-to-time').val() || _all_bookings[booking_id].pending_amount != _pending_amount || _all_bookings[booking_id].discount != _discount || _all_bookings[booking_id].booking_note != _booking_note || (typeof _repeat_end != 'undefined' && _all_bookings[booking_id].service_end != _repeat_end) || (typeof _repeat_end_date != 'undefined' && _all_bookings[booking_id].service_end_date != _repeat_end_date)) {
			if ($('#update-booking').length == 0) {
				$('.pop-main-button').prepend('<input type="button" class="save-but n-btn purple-btn" id="update-booking" value="Update" />');
				//$('.pop-main-button').find('#copy-booking').remove(); //Updated by Geethu
			}
			if ($('#copy-booking').length == 0)//else
			{
				$('.pop-main-button').prepend('<input type="button" class="copy-but n-btn" id="copy-booking" value="Copy" />');//Updated by Geethu
			}
		}
		else {
			$('#update-booking').remove();
			//Updated by Geethu
			if ($('#copy-booking').length == 0) {
				$('.pop-main-button').prepend('<input type="button" class="copy-but n-btn" id="copy-booking" value="Copy" />');
			}
			//End
		}
	}
}

$('body').on('click', '#chg-cust-address', function () {
	var _customer_id = $('#b-customer-id').val();
	$('#customer-picked-address').hide();
	open_address_panel(_customer_id);
});
//Edited by geethu
$('body').on('click', '#copy-booking', function () {
	var _booking_id = $('#booking-id').val();
	//$('#customer-picked-address').hide();
	open_maid_panel(_booking_id);
});
function open_maid_panel(_booking_id) {
	//$('#customer-address-id').val('');
	//$('#customer-address-panel').hide();
	//$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');

	if ($.isNumeric(_booking_id) && _booking_id > 0) {
		$('#maids-panel').slideDown();
		$('input[name="same_zone"]').attr('onclick', 'open_maid_panel(' + _booking_id + ')');

		$.post(_page_url, { action: 'get-free-maids', booking_id: _booking_id, same_zone: $('input[name="same_zone"]:checked').val() }, function (response) {
			if (response == 'refresh') {
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'fade',
					openSpeed: 100,
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding: 0,
					closeBtn: false,
					content: '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
				});

				_refresh_page = true;
			}
			else {

				var _resp = $.parseJSON(response);
				if (typeof _resp.status && _resp.status == 'error') {
					//$('#b-error').text(_resp.message);
					//$('#saving-booking').attr('id', 'save-booking');
					//$('#save-booking').val('Save');
					$('#maids-panel .inner').css("overflow", "hidden");
					$('#maids-panel .inner').html('<div id="c-error">' + _resp.message + '</div>');

				}
				else {
					var _maid_html = '<div class="table">';

					var i = 0;
					$.each(_resp, function (key, val) {
						//if(i == 0)
						//{
						_maid_html += '<div class="row n-pick-maid-c-main m-0"><div class="col-sm-8 n-pick-maid-left pl-0"> <span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br> <span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="col-sm-4 n-pick-maid-right pr-0"><input type="button" value="Book" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid" /></div></div>';
						//++i;
						//}
						//else if(i == 1)
						//{
						//_maid_html += '<div class="cell1"><span id="mcopyadd-' + val.maid_id + '"><strong>' + val.maid_name + '</strong></span><br /><span id="mcopy-' + val.maid_id + '">' + val.maid_nationality + '</span></div><div class="cell2"><input type="button" value="Book &raquo;" id="mcopy_' + val.maid_id + '_' + _booking_id + '_' + val.service_start_date + '" class="copy_maid"  /></div></div>';
						//i = 0;                                        
						//}

					});

					_maid_html += '</div>';

					$('#maids-panel .inner').html(_maid_html);
				}
			}
		});
	}
}

$('body').on('click', '#maids-panel .close', function () {
	$('#maids-panel').slideUp(function () { $('#customer-copied-maid').show(); });
});
$('body').on('click', '.copy_maid', function () {
	var _booking_details = $(this).attr('id').replace('mcopy_', '').split('_');
	var _maid_id = $.trim(_booking_details[0]);
	var _booking_id = $.trim(_booking_details[1]);

	_refresh_page = false;
	$('#save-booking').attr('id', 'copying-booking');
	$('#saving-booking').val('Please wait...');

	$.post(_page_url, { action: 'copy-maid', booking_id: _booking_id, maid_id: _maid_id }, function (response) {
		_bpop_open = false;

		var _alert_html = '';

		if (response == 'refresh') {
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = true;
		}
		else if (response == 'locked') {
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = false;
		}
		else {
			var _resp = $.parseJSON(response);
			if (typeof _resp.status && _resp.status == 'success') {
				$('#saving-booking').val('Done');
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been done successfully.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
				_refresh_page = false
			}
			else if (typeof _resp.status && _resp.status == 'error') {
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else {
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true
			}
		}
		$('#maids-panel').slideUp(function () {

			//$('#customer-address-panel .inner').html('Loading<span class="dots_loader"></span>');                       

			if (_booking_id != '') {
				chk_booking_chg(_booking_id);
			}
		});

		if (_alert_html != '') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: _alert_html
			});
			refresh_grid();
		}
	});
});
//End
$('body').on('click', '#customer-address-panel .close', function () {
	$('#customer-address-panel').slideUp(function () { $('#customer-picked-address').show(); });
});

var _refresh_page = false;
$('body').on('click', '#save-booking', function () {
	$('.field-error').text('');
	var _service_date = $('#service-date').val();
	var _maid_id = $.trim($('#maid-id').val());
	var _customer_id = $.trim($('#b-customer-id').val());
	var _customer_address_id = $.trim($('#customer-address-id').val());
	var _service_type_id = $.trim($('#b-service-type-id').val());
	var _from_time = $.trim($('#b-from-time').val());
	var _to_time = $.trim($('#b-to-time').val());
	var _lock_booking = $('#lock-booking').is(':checked') ? 1 : 0;
	var _booking_type = $.trim($('#b-booking-type').val());
	var _hrly_amount = $.trim($('#b-rate_per_hr').val());
	var _pending_amount = $.trim($('#b-pending_amount').val());
	var _discount = $.trim($('#b-discount').val());
	var _note = $.trim($('#booking-note').val());
	var _tot_amt = $.trim($('#tot_amout').val());
	var _cleaning_material = $('#b-cleaning-materials').is(':checked') ? 'Y' : 'N';

	$('#b-error').text('');

	if ($.isNumeric(_customer_id) == false) {
		$('#b-customer-id').parent().append('<label class="field-error text-danger">Select a customer !</label>');
		return false;
	}
	else if ($.isNumeric(_customer_address_id) == false) {
		$('#b-error').text('Pick one customer address !');
		open_address_panel(_customer_id);
		return false;
	}
	else if ($.isNumeric(_service_type_id) == false) {
		$('#b-error').text('Select service type');
		return false;
	}
	else if (_from_time == '' || _to_time == '') {
		$('#b-from-time').parent().append('<label class="field-error text-danger">Select booking time !</label>');
		return false;
	}
	else if (_booking_type == '') {
		$('#b-booking-type').parent().append('<label class="field-error text-danger">Select repeat type !</label>');
		return false;
	}
	else if (_tot_amt == '') {
		$('#tot_amout').parent().parent().append('<label class="field-error text-danger">Enter total amount !</label>');
		return false;
	}

	var _repeat_days = [];
	var _repeat_end = '';
	var _repeat_end_date = '';
	if (_booking_type == 'WE' || _booking_type == 'BW') {
		_repeat_days = $('input[id^="repeat-on-"]:checked').map(function () {
			return this.value;
		}).get();

		if (_repeat_days.length == 0) {
			$('#repeat-days').append('<label class="field-error text-danger">Select repeat days !</label>');
			return false;
		}

		_repeat_end = $('input[name="repeat_end"]:checked').val();
		if (_repeat_end == 'ondate') {
			_repeat_end_date = $('#repeat-end-date').val();
			if (_repeat_end_date == '') {
				$('#repeat-ends').append('<label class="field-error text-danger">Select service end date !</label>');
				return false;
			}
		}
	}

	_refresh_page = false;
	$('#save-booking').attr('id', 'saving-booking');
	$('#saving-booking').val('Please wait...');

	$.post(_page_url, { action: 'book-maid', service_date: _service_date, customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_type_id: _service_type_id, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, is_locked: _lock_booking, pending_amount: _pending_amount, booking_note: _note, discount: _discount, tot_amt: _tot_amt, cleaning_material: _cleaning_material }, function (response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';

		if (response == 'refresh') {
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = true;
		}
		else {
			var _resp = $.parseJSON(response);
			if (typeof _resp.status && _resp.status == 'success') {
				$('#saving-booking').val('Done');
				$('#alert-title').html('Success !');
				$('#alert-message').html('Booking has been done successfully.');
				fancybox_show('alert-popup');
				//_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
				_refresh_page = false
			}
			else if (typeof _resp.status && _resp.status == 'error') {
				$('#b-error').text(_resp.message);
				$('#saving-booking').attr('id', 'save-booking');
				$('#save-booking').val('Save');
			}
			else {
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true
			}
		}

		if (_alert_html != '') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: _alert_html
			});
		}
	});
});

$('body').on('click', '#update-permanent, #update-one-day', function () {
	var _update_type = $(this).attr('id').replace('update-', '');

	var _all_bookings = $.parseJSON($('#all-bookings').val());
	var _booking_id = $('#booking-id').val();

	if ($.isNumeric(_booking_id) == false || typeof _all_bookings[_booking_id] == 'undefined') {
		window.location = _page_url;
	}

	var _customer_address_id = $.trim($('#customer-address-id').val());
	var _from_time = $.trim($('#b-from-time').val());
	var _to_time = $.trim($('#b-to-time').val());
	var _pending_amount = $.trim($('#b-pending-amount').val());
	var _discount = $.trim($('#b-discount').val());
	var _total_amt = $.trim($('#tot_amout').val());
	var _lock_booking = $('#lock-booking').is(':checked') ? 1 : 0;
	var _cleaning_material = $('#b-cleaning-materials').is(':checked') ? 'Y' : 'N';
	var _note = $.trim($('#booking-note').val());

	if (_from_time == '' || _to_time == '') {
		$('#b-error').text('Select booking time');
		return false;
	}

	var _repeat_end = '';
	var _repeat_end_date = '';
	if (_all_bookings[_booking_id].booking_type != 'OD') {
		_repeat_end = $('input[name="repeat_end"]:checked').val();
		if (_repeat_end == 'ondate') {
			_repeat_end_date = $('#repeat-end-date').val();
			if (_repeat_end_date == '') {
				$('#b-error').text('Enter an end date');
				return false;
			}
		}
	}

	_refresh_page = false;
	$('#update-cancel').hide();
	var _update_label = $('#update-permanent').val();
	$('#update-permanent').attr('id', 'updating-permanent');
	$('#updating-permanent').val('Please wait...');

	$.post(_page_url, { action: 'update-booking', booking_id: _booking_id, service_date: $('#service-date').val(), update_type: _update_type, customer_address_id: _customer_address_id, time_from: _from_time, time_to: _to_time, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, pending_amount: _pending_amount, discount: _discount, total_amt: _total_amt, booking_note: _note, is_locked: _lock_booking, cleaning_material: _cleaning_material }, function (response) {
		_bpop_open = false;
		refresh_grid();
		var _alert_html = '';

		if (response == 'refresh') {
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = true;
		}
		else if (response == 'locked') {
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">This booking is locked by another user.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = false;
		} else if (response == 'odoorefresh') {
			_alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Update failed. Booking already synched to odoo.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
			_refresh_page = false;
		}
		else {

			var _resp = $.parseJSON(response);
			if (typeof _resp.status && _resp.status == 'success') {
				$('#saving-booking').val('Done');
				_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Booking has been updated successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close" /></div></div>';
				_refresh_page = false
			}
			else if (typeof _resp.status && _resp.status == 'error') {
				$('#b-error').text(_resp.message);
				$('#update-cancel').show();
				$('#updating-permanent').attr('id', 'update-permanent');
				$('#update-permanent').val(_update_label);
			}
			else {
				_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';
				_refresh_page = true
			}
		}

		if (_alert_html != '') {
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'fade',
				openSpeed: 100,
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: _alert_html
			});
		}
	});
});

$('body').on('click', '.pop_close', function () {
	_bpop_open = false;
	parent.$.fancybox.close();
	$(".ui-selected").removeClass("ui-selected");
	if (_refresh_page) {
		window.location = _page_url;
	}
});
$('#search-zone-id').change(function () {
	refresh_grid();
});
$('#search-keyword').keyup(function () {
	refresh_grid();
});
function refresh_grid() {
	if (_bpop_open != true) {
		$('.time_grid').addClass('loading');
		$.post(_page_url, {
			action: 'refresh-grid',
			search_maid_schedule: '1',
			start_date: $('#start-date').val(),
			end_date: $('#end-date').val(),
			maid_id: $('#search-maid-id').val()
			//zone_id : $('#search-zone-id').val(), search_key : $('#search-keyword').val()
		}, function (response) {
			$('.time_grid').removeClass('loading');
			$('.mm-loader').hide();
			response = $.parseJSON(response);
			$('#schedule-grid-rows').html(response.grid);
			$('#schedule-report').html(response.report);
			$('#schedule .maids').html(response.days); // update left days columns
			//setTimeout('refresh_grid()', 5000);
			if (_bpop_open != true) {
				apply_selectable();
			}
		});
	} else {
		//setTimeout('refresh_grid()', 5000);
	}
}




$('#schedule').show();

$('#print-schedule-report').click(function () {
	var _print_ontents = $('#schedule-report').html();
	var _original_contents = $('body').html();

	$('body').html(_print_ontents);
	$('#schedule-report').show();

	window.print();

	$('body').html(_original_contents);

	window.location.reload();
});
/* Edited By Betsy Bernard */
$('#mark_maid_leave').click(function () {
	var select_maid = $('#search-maid-id').val();
	if (select_maid != 0) {
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'fade',
			openSpeed: 100,
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: '<div id="alert-popup" class=""><div class="popup-main-box"><div class="green-popup-head">Mark Leave<span id="b-time-slot"></span><span class="pop_close n-close-btn">&nbsp;</span></div><div class="content padd20">Are you sure, want to mark this maid as Leave ?</div><div class="bottom"><input type="button" value="Cancel" class="pop_close n-btn red-btn mb-0" />&nbsp;<input type="button" class="n-btn mb-0" value="Proceed" onclick="mark_leave();" /></div></div></div>'
		});
	} else {
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'fade',
			openSpeed: 100,
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: '<div id="alert-popup"><div class="head">Warning!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Please select a maid to continue...</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
		});
	}
});
function mark_leave() {
	$.fancybox.close();
	var _alert_html = '';
	var maid_id = $('#search-maid-id').val();
	var start_date = $('#start-date').val();
	var end_date = $('#end-date').val();

	$.post(_page_url, { action: 'maid-leave', maid_id: maid_id, start_date: start_date, end_date: end_date }, function (result) {

		if (result == "success") {
			_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Successfully Marked Leave!!</div><div class="bottom"><input type="button" value="Ok" class="pop_close" /></div></div>';

		}
		else if (result == "exists") {
			_alert_html = '<div id="alert-popup"><div class="head">Warning!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Not able to mark leave. Booking Exists for Current Dates.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';

		}
		else if (result == "Leave") {
			_alert_html = '<div id="alert-popup"><div class="head">Warning!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">Already Leave Marked for one of these Date.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>';

		}
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'fade',
			openSpeed: 100,
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: _alert_html
		});
	});
}



$('body').on('change', '#schedule-top #search-maid-id', function () {
	$('.mm-loader').show();
	refresh_grid();
	if ($(this).val() > 0) {
		$('#mark_maid_leave').show();
	} else {
		$('#mark_maid_leave').hide();

	}
});
$('body').on('change', '#schedule-top #start-date,#schedule-top #end-date', function () {
	$('.mm-loader').show();
	refresh_grid();
});
$('.sel2').select2({ dropdownAutoWidth: true, width: 'resolve' });
refresh_grid();
$(function () {
	setInterval(refresh_grid, 5000);
});