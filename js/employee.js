/************************************************************************** */
/* Coder : Samnad. S                                                        */
/*                                                                          */
/* Last Updated by :                                                        */
/************************************************************************** */
// logic goes here...................
/************************************************************************** */
$('[data-tab]').click(function (event) {
    var tab_name = $(this).attr("data-tab");
    if (tab_name == "aaaa") {
    }
    else {
    }
});
/************************************************************************** */
$('input[name="date_of_joining"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
}).on('changeDate', function (e) {

});
/************************************************************************** */
$('#new_employee_form select[name^="availability_time_from"]').on('change', function () {
    var name = $(this).attr("name");
    var from_time = this.value;
    var id = name.substring(name.indexOf("[") + 1, name.lastIndexOf("]"));
    $('#new_employee_form select[name="availability_time_to[' + id + ']"]').select2().val("").trigger("change");
    var _time_to_options = '';
    $('#new_employee_form select[name="availability_time_from[' + id + ']"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }
        $('#new_employee_form select[name="availability_time_to[' + id + ']"]').html(_time_to_options);
    });
    $('#new_employee_form select[name="availability_time_to[' + id + ']"]').select2().val("").trigger("change");//.select2('open');
});
/************************************************************************** */
$().ready(function () {
    $('li [data-tab="tab1"]').trigger('click');

  // Mobile number max limit to 12
    $.validator.addMethod("validAddressMobileNumber", function(value, element) {
        var validLength = value.replace(/[^\d]/g, "").length <= 12;
        return this.optional(element) || validLength;
    }, "Mobile number must be a maximum of 12 digits.");

    new_employee_form_validator = $('#new_employee_form').validate({
        ignore: [],
        rules: {
            employee_type_id: {
                required: true,
            },
            // maid_leader_id: {
            //  required: '#is_maid_leader:unchecked'
            // },
            'maid_leader_id': {
                required: function (element) {
                    return ($('#is_maid_leader').is(':unchecked') && $('#employee_type_1').is(':checked'));
                }
            },
            employee_name: {
                required: true,
            },
            nationality: {
                required: true,
            },
            present_address: {
                required: true,
            },
            date_of_joining: {
                required: true,
            },
            mobile_number_1: {
                required: true,
                validAddressMobileNumber: true,
                remote: {
                    url: _base_url + "employee/validatemobilenumber",
                    type: "post",
                    data: 
                    {
                        phone: function(){ return $("#mobile_number_1").val(); }
                    }
                }
            },
            /************************************************** */
            "availability_zone[0]": {
                required: '#week_day_0:checked'
            },
            "availability_zone[1]": {
                required: '#week_day_1:checked'
            },
            "availability_zone[2]": {
                required: '#week_day_2:checked'
            },
            "availability_zone[3]": {
                required: '#week_day_3:checked'
            },
            "availability_zone[4]": {
                required: '#week_day_4:checked'
            },
            "availability_zone[5]": {
                required: '#week_day_5:checked'
            },
            "availability_zone[6]": {
                required: '#week_day_6:checked'
            },
            "availability_time_from[0]": {
                required: '#week_day_0:checked'
            },
            "availability_time_from[1]": {
                required: '#week_day_1:checked'
            },
            "availability_time_from[2]": {
                required: '#week_day_2:checked'
            },
            "availability_time_from[3]": {
                required: '#week_day_3:checked'
            },
            "availability_time_from[4]": {
                required: '#week_day_4:checked'
            },
            "availability_time_from[5]": {
                required: '#week_day_5:checked'
            },
            "availability_time_from[6]": {
                required: '#week_day_6:checked'
            },
            "availability_time_to[0]": {
                required: '#week_day_0:checked'
            },
            "availability_time_to[1]": {
                required: '#week_day_1:checked'
            },
            "availability_time_to[2]": {
                required: '#week_day_2:checked'
            },
            "availability_time_to[3]": {
                required: '#week_day_3:checked'
            },
            "availability_time_to[4]": {
                required: '#week_day_4:checked'
            },
            "availability_time_to[5]": {
                required: '#week_day_5:checked'
            },
            "availability_time_to[6]": {
                required: '#week_day_6:checked'
            },
            /************************************************** */
            "services_ids[]": {
                required: 'input[id="employee_type_1"]:checked',
                minlength: 1
            },
            "address_type_contact_name[0]": {
                required: true,
            },
            "address_type_contact_name[1]": {
                required: true,
            },
            // "address_type_contact_name[2]": {
            //     required: true,
            // },
            // "address_type_contact_name[3]": {
            //     required: true,
            // },
            "address_type_mobile_no_1[0]": {
                
                required: true,
                validAddressMobileNumber: true,
                remote: {
                    url: _base_url + "employee/validate_adress_type_mobilenumber",
                    type: "post",
                    data: 
                    {
                        phone: function(){ return $("[name='address_type_mobile_no_1[0]']").val(); }
                    }  
                }
            },
            "address_type_mobile_no_1[1]": {
                required: true,
                validAddressMobileNumber: true,
                remote: {
                    url: _base_url + "employee/validate_adress_type_mobilenumber",
                    type: "post",
                    data: 
                    {
                        phone: function(){ return $("[name='address_type_mobile_no_1[1]']").val(); }
                    }  
                }
            },
            "address_type_mobile_no_1[2]": {
                validAddressMobileNumber: true,
            },
            "address_type_mobile_no_1[3]": {
                validAddressMobileNumber: true,
            },
            "address_type_country_id[0]": {
                required: true,
            },
            "address_type_country_id[1]": {
                required: true,
            },
            // "address_type_country_id[2]": {
            //     required: true,
            // },
            // "address_type_country_id[3]": {
            //     required: true,
            // },

            // *********************************************************
            // error for documents tab
            // "document_number[0]": {
            //     required: true,
            // },
            // "document_expiry_date[0]": {
            //     required: true,
            // },
            // "grace_period[0]": {
            //     required: true,
            // },
            // "document_number[1]": {
            //     required: true,
            // },
            // "document_expiry_date[1]": {
            //     required: true,
            // },
            // "grace_period[1]": {
            //     required: true,
            // },
            // "document_number[2]": {
            //     required: true,
            // },
            // "document_expiry_date[2]": {
            //     required: true,
            // },
            // "grace_period[2]": {
            //     required: true,
            // },
            // "document_image": {
            //     required: true,
            // },
        },
        messages: {
            "services_ids[]": "Select atleast one service.",
            "maid_leader_id": "Select team leader",
            "employee_name": "Please enter employee name",
            "nationality": "Select nationality",
            "present_address": "Please enter present address",
            "date_of_joining": "Please enter date of joining",
            "mobile_number_1": {
                required: "Please enter mobile number",
                remote: "Mobile number already exist"
            },
            "address_type_contact_name[0]": "Please enter local address contact name",
            "address_type_contact_name[1]": "Please enter permanent address contact name",
            // "address_type_contact_name[2]": "Please enter emergency local contact name",
            // "address_type_contact_name[3]": "Please enter emergency home country contact name",
            "address_type_mobile_no_1[0]":
            { 
                required: "Please enter local address mobile number",
                remote: "Mobile number already exist"
            },
            "address_type_mobile_no_1[1]": {
                required: "Please enter permanent address mobile number",
                remote : "Mobile number already exist"
            },
            // "address_type_mobile_no_1[2]": "Please enter emergency local mobile number",
            // "address_type_mobile_no_1[3]": "Please enter emergency home country mobile number",
            "address_type_country_id[0]": "Select country",
            "address_type_country_id[1]": "Select country",
            // "address_type_country_id[2]": "Select country",
            // "address_type_country_id[3]": "Select country",
            // **********************************************
            // error for documents tab
            // "document_number[0]": "Please enter Passport Number",
            // "document_expiry_date[0]": "Please enter Passport Expiry Date",
            // "grace_period[0]": "Please enter Passport Grace Period",
            // "document_number[1]": "Please enter Visa Number",
            // "document_expiry_date[1]": "Please enter Visa Expiry Date",
            // "grace_period[1]": "Please enter Visa Grace Period",
            // "document_number[2]": "Please enter Labour Card Number",
            // "document_expiry_date[2]": "Please enter Labour Card Expiry Date",
            // "grace_period[2]": "Please enter Labour Card Grace Period",
            // "document_image": "Please select a Image",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "maid_leader_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "nationality") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "gender_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "services_ids[]") {
                error.insertAfter(element.parent().parent().parent());
            }
            else if (element.attr("name") == "address_type_country_id[0]") {
                error.insertAfter(element.parent().parent());
            }
            else if (element.attr("name") == "address_type_country_id[1]") {
                error.insertAfter(element.parent().parent());
            }
            // else if (element.attr("name") == "address_type_country_id[2]") {
            //     error.insertAfter(element.parent().parent());
            // }
            // else if (element.attr("name") == "address_type_country_id[3]") {
            //     error.insertAfter(element.parent().parent());
            // }
            /************************************************** */
            else if (element.attr("name").startsWith("availability_zone")) {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name").startsWith("availability_time_from")) {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name").startsWith("availability_time_to")) {
                error.insertAfter(element.parent());
            }
            // ******************************************************
            // error for documents tab
            else if (element.attr("name").startsWith("document_image")) {
                var imageElements = $('[name^="document_image"]');
                imageElements.each(function () {
                    error.clone().insertAfter($(this));
                });
            }
            //  else if (element.attr("name") == "document_number[0]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "document_number[1]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "document_number[2]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "document_expiry_date[0]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "document_expiry_date[1]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "document_expiry_date[2]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "grace_period[0]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "grace_period[1]") {
            //     error.insertAfter(element);
            // }
            //  else if (element.attr("name") == "grace_period[2]") {
            //     error.insertAfter(element);
            // }
            /************************************************** */
            else {
                error.insertAfter(element);
            }
        }
    });
    $('#new_employee_form #employee-save-btn').click(function () {
        new_employee_form_validator.resetForm();
        if ($("#new_employee_form").valid()) {
            $('.mm-loader').show();
            $('#employee-save-btn').html('Saving...').attr("disabled", true);
            var formData = new FormData(document.getElementById("new_employee_form"));
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "employee/save",
                data: $('#new_employee_form').serialize(),
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == true) {
                        $('#employee-save-btn').html('Saved');
                        toast('success', data.message);
                        setTimeout(function () {
                            window.location.href = _base_url + "employee/list";
                        }, 3000);
                    }
                    else {
                        $('#employee-save-btn').html('Save').removeAttr("disabled");
                        toast('error', data.message);
                    }
                },
                error: function (data) {
                    $('#employee-save-btn').html('Save').removeAttr("disabled");
                    toast('error', data.statusText);
                },
            });
        }
        else {
            $('#new_employee_form label.error').each(function () {
                if ($(this).css('display') != 'none') {
                    var tab_id = $(this).closest('.tab-pane').attr("id");
                    $('li [data-tab="' + tab_id + '"]').trigger('click');
                    //toast('warning', "Fill required fields to continue.");
                    toast('warning', $(this).text());
                    return false;
                }
            });
        }
    });
});
/************************************************************************** */
$('[data-action="select-skill"]').click(function (event) {
    var skill_id = $('select[id="skill_id"]').val();
    var skill = $('select[id="skill_id"]').select2('data')[0]['text'];
    var skill_rating_id = $('select[id="skill_rating_id"]').val();
    if (skill_id && skill_rating_id) {
        if (document.getElementById("skill_" + skill_id)) {
            toast('info', "Skill already selected.");
            return false;
        }
        var html = `<div class="col-sm-3" id="skill_` + skill_id + `">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">` + skill + `<span class="pull-right">Poor</span></h3>
                            </div>
                            <div class="panel-body">
                                <p>Notes</p>
                                <input type="hidden" name="skill_ids[]" value="` + skill_id + `"/>
                                <input type="hidden" name="skill_rating_level_ids[]" value="` + skill_rating_id + `"/>
                                <textarea style="width:100%" name="skill_notes[]"></textarea>
                            </div>
                            <div class="panel-footer text-right"><button type="button" class="btn btn-danger" data-id="` + skill_id + `" data-action="remove-skill"><i class="fa fa-trash" aria-hidden="true"></i></button></div>
                        </div>
                    </div>`;
        $('#skills-holder').append(html);
        $('[data-action="remove-skill"]').off();
        $('[data-action="remove-skill"]').click(function (event) {
            var skill_id = $(this).attr("data-id");
            $(this).closest('div#skill_' + skill_id).remove();
        });
    }
});
/************************************************************************** */
$('input[name="employee_type_id"]').click(function (event) {
    if (this.value == 1) {
        // maid employee
        $('[data-tab="tab4"]').closest('li').show();
        $("#is-maid-leader").show();
        $("#maid-leader").show();
        $("#service-types").show();
    }
    else {
        $('[data-tab="tab4"]').closest('li').hide();
        $('#is_maid_leader').prop('checked', false);
        $("#is-maid-leader").hide();
        $("#maid-leader").hide();
        $("#service-types").hide();
        $('#new_employee_form input[name="services_ids[]"]').prop("checked", false)
    }
});
/************************************************************************** */
$('#is_maid_leader').change(function () {
    if (this.checked) {
        $('select[name="maid_leader_id"]').select2().val("").trigger("change");
        $("#maid-leader").hide();
    }
    else {
        $("#maid-leader").show();
    }
});
/************************************************************************** */
// image upload show img name in input
document.querySelectorAll('input[name^="document_image["]').forEach(function (inputElement) {
    inputElement.addEventListener('change', function (e) {
        var selectedFile = e.target.files[0];
        var filename = selectedFile.name;

        // Show the selected filename in the associated custom file label
        var customFileLabel = document.querySelector('label[for="' + e.target.id + '"]');
        if (customFileLabel) {
            customFileLabel.textContent = filename;
        }

        // Check if the file type is allowed
        var extension = filename.split('.').pop();
        var allowedTypes = ['jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'pdf'];
        var inputId = e.target.id.replace(/\D/g, '');
        var errorDiv = document.getElementById('image_error[' + inputId + ']');

        if (allowedTypes.indexOf(extension.toLowerCase()) === -1) {
            if (errorDiv) {
                errorDiv.textContent = 'File type not allowed. Allowed types: jpg, jpeg, gif, png, doc, docx, pdf';
                errorDiv.classList.add('error');
            }
            e.target.value = '';
            if (customFileLabel) {
                customFileLabel.textContent = 'Choose file';
            }
        } else {
            if (errorDiv) {
                errorDiv.textContent = '';
                errorDiv.classList.remove('error');
            }
        }
    });
});