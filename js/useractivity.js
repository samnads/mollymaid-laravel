$(document).ready(function(){
	var activityreportlist = $('#activityreportlist').DataTable({
        'bFilter' : false,
        'bLengthChange': false,
        'pageLength' : 25,
        'processing': "true",
        'serverSide': true,
        'bSort' : false,
        'serverMethod': 'post',
        'ajax': {
            'url':_base_url+'reports/list_ajax_useractivity_list',
            'data': function(data){
                if($('#act_from_date').length)
                {
                    var regdate = $('#act_from_date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
				if($('#act_to_date').length)
                {
                    var regdateto = $('#act_to_date').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
              data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function(json, type) {
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'type' },
            { data: 'bookingtype' },
            { data: 'shift' },
            { data: 'action' },
            { data: 'doneby' },
            { data: 'time' },
        ]
    });
	
	$('#act_from_date,#act_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
	
	$('#act_from_date').change(function(){
        activityreportlist.draw();
    });
	
	$('#act_to_date').change(function(){
        activityreportlist.draw();
    });
	$("#keyword-search").keyup(function(){
		var vall = $('#keyword-search').val().length
		if(vall > 0)
		{
			activityreportlist.draw();
		}
    });
	
	
	$('#customerexcelbtn').click(function(){
		$("#customerexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
		var status = $('#all-customers').val();
		var fromdate = $('#cust_from_date').val();
		var todate = $('#cust_to_date').val();
		var payment_type = $('#payment_type').val();
		var source = $('#sort_source').val();
		var sort_cust_type = $('#sort_cust_type').val();
		var search = $('#keyword-search').val();
		
		$.ajax({
			type: "POST",
			url: _base_url + 'customerexcel/customerexportexcel',
			data: {fromdate: fromdate,todate: todate,status: status,payment_type:payment_type,source:source,sort_cust_type:sort_cust_type,search: search},
			cache: false,
			success: function (response)
			{
				$("#customerexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
				window.location = response;
			}
		});
	});
});