/************************************************************************** */
$('[data-action="open-new-driver-mapping"]').click(function (event) {
    new_driver_mapping_form_validator.resetForm();
    $('#new_driver_mapping_form select[name="driver_id"]').val('').change();
    $('#new_driver_mapping_form select[name="zone_id"]').val('').change();
    $('#new_driver_mapping_form select[name="area_id"]').val('').change();
    $('#new_driver_mapping_form input[name="date_from"]').val('').change();
    $('#new_driver_mapping_form input[name="date_to"]').val('').change();
    fancybox_show('new-driver-mapping-popup', { width: 600 });
});
/************************************************************************** */
$('#new_driver_mapping_form select[name="zone_id"]').change(function () {
    var zone_id = this.value;
    $('#new_driver_mapping_form select[name="area_id[]"]').html(``);
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/get_areas_by_zone",
        data: { zone_id: zone_id },
        success: function (areas) {
            var options = `<option value="">-- Select area --</option>`;
            $.each(areas, function (key, area) {
                options += `<option value="` + area.area_id + `">` + area.area_name + `</option>`;
            });
            $('#new_driver_mapping_form select[name="area_id[]"]').html(options);
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
/************************************************************************** */
$().ready(function () {
    $('li [data-tab="tab2"]').trigger('click');
    new_driver_mapping_form_validator = $('#new_driver_mapping_form').validate({
        ignore: [],
        rules: {
            driver_id: {
                required: true,
            },
            zone_id: {
                required: true,
            },
            area_id: {
                required: true,
            },
            date_from: {
                required: true,
            },
            date_to: {
                required: true,
            },
        },
        messages: {
            driver_id: "Select driver.",
            zone_id: "Select zone from list.",
            area_id: "Select area from list.",
            date_from: "Pick a start date.",
            date_to: "Pick a end date.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "driver_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "zone_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "area_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#new_driver_mapping_form button[type="submit"]').html('Saving...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "driver/save_driver_availability",
                data: $('#new_driver_mapping_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        close_fancybox();
                        clickCurrentTab();
                    }
                    else {
                        toast('error', data.message);
                        $('.mm-loader').hide();
                    }
                    $('#new_driver_mapping_form button[type="submit"]').html('Save').attr("disabled", false);
                },
                error: function (data) {
                    $('#new_driver_mapping_form button[type="submit"]').html('Save').attr("disabled", false);
                    toast('error', data.statusText);
                },
            });
        }
    });
});
$('#new_driver_mapping_form .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});