$(document).ready(function(){
	var requestlistDataTable = $('#requestlisttable').DataTable({
        'bFilter' : false,
        'bLengthChange': false,
        'pageLength' : 50,
        'processing': true,
        'serverSide': true,
        'bSort' : false,
        'serverMethod': 'post',
        'ajax': {
            'url':_base_url+'booking/list_ajax_delete_approval',
            'data': function(data){
                if($('#cust_from_date').length)
                {
                    var regdate = $('#cust_from_date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
				if($('#cust_to_date').length)
                {
                    var regdateto = $('#cust_to_date').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.useractive = $('#all-request').val();
                data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function(json, type) {
                // var _resp = $.parseJSON(json.responseText);
            }
        },
        
        'columns': [
            { data: 'slno' },
            { data: 'name' },
            { data: 'servicedate' },
            { data: 'shift' },
            { data: 'maidname' },
            { data: 'reason' },
            { data: 'requestedby' },
            { data: 'approvedby' },
            { data: 'status' },
            { data: 'action' },
        ]
    });
	
	$('#cust_from_date,#cust_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
	
	$('#cust_from_date').change(function(){
        requestlistDataTable.draw();
    });
	
	$('#cust_to_date').change(function(){
        requestlistDataTable.draw();
    });
	
	$('#all-request').change(function(){
        requestlistDataTable.draw();
    });
	
	$("#keyword-search").keyup(function(){
		var vall = $('#keyword-search').val().length
		if(vall >= 3)
		{
			requestlistDataTable.draw();
		}
    });
	
	$('#requestexcelbtn').click(function(){
		$("#requestexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
		var status = $('#all-request').val();
		var fromdate = $('#cust_from_date').val();
		var todate = $('#cust_to_date').val();
		var search = $('#keyword-search').val();
		
		$.ajax({
			type: "POST",
			url: _base_url + 'customerexcel/requestexportexcel',
			data: {fromdate: fromdate,todate: todate,status: status,search: search},
			cache: false,
			success: function (response)
			{
				$("#requestexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
				requestlistDataTable.draw();
				// window.location = response;
			}
		});
	});
});

function approve_request(request_id, userid)
{
    if (confirm('Are you sure you want to approve this request?'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "booking/approve_delete_request",
            data: {request_id: request_id, userid : userid},
            dataType: "text",
            cache: false,
            success: function (result) {
                if(result == 'success')
                {
                    location.reload();
                }
                else
                {
                    location.reload();
                    
                }
            }
        });
    }
}

function reject_request(request_id, userid)
{
    if (confirm('Are you sure you want to reject this request?'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "booking/reject_delete_request",
            data: {request_id: request_id, userid : userid},
            dataType: "text",
            cache: false,
            success: function (result) {
                if(result == 'success')
                {
                    location.reload();
                }
                else
                {
                    location.reload();
                    
                }
            }
        });
    }
}