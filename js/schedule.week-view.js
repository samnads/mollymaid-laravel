/************************************************************************** */
/*                                                                          */
/*                          Coder : Samnad. S                               */
/*                                                                          */
/************************************************************************** */
/**
 * Test
 */
var screen_width = parseFloat($('body').innerWidth());
let head_width = parseFloat(document.querySelectorAll('#booking-calender-section .head')[0].offsetWidth);
let head_last_width = parseFloat(document.querySelectorAll('#booking-calender-section .head-last')[0].offsetWidth);
document.getElementById('booking-calender-section').setAttribute("style", "width:" + screen_width + "px");
document.getElementsByClassName('justify-content-between top-summary-section')[0].setAttribute("style", "width:" + screen_width + "px");
let time_line_width = screen_width - (head_width + head_last_width);
document.querySelectorAll('#schedule .time_line')[0].setAttribute("style", "width:" + time_line_width + "px;margin-left:" + head_width + "px");
let box_width = 71; // width of time box
let box_count = document.querySelectorAll('#schedule .time_slider div > div').length; // total number time voxes
let box_total_width = box_width * box_count;
let time_slider_width = (box_width * 24) + head_last_width;
let time_slider_visible_width = screen_width - (head_width + head_last_width);
document.querySelectorAll('#schedule .time_slider div')[0].setAttribute("style", "width:" + time_slider_width + "px");
document.getElementById('schedule').setAttribute("style", "display: block;width:" + screen_width + "px");
document.querySelectorAll('#schedule .time_grid')[0].setAttribute("style", "width:" + (time_line_width) + "px");
window.onresize = function () {
    location.reload();
}
/*************************************************************** */
// logic goes here...................
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#tb-slide-right').click(function () {
    var position = $('.time_line .time_slider').position();
    //var max_left = time_grid_width - 1768 + 3 + 5;
    var max_left = time_slider_visible_width - box_total_width;
    var left_pos = position.left - 71;
    left_pos = left_pos < max_left ? max_left : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -left_pos + 71 + 'px',
    }, 71);



    /*if ($(window).width() <= 1366) {
        var max_left = time_grid_width - 1870 + 3 + 5;
        var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
    }

    if ($(window).width() <= 1300) {
        var max_left = time_grid_width - 1510 + 3 + 5;  //1865 -for Fixed
        var left_pos = position.left - 71;
        left_pos = left_pos < max_left ? max_left : left_pos;
        $('.time_line .time_slider').animate({ left: left_pos }, 71);
        $('.grids').animate({ left: left_pos }, 71);
    }*/
});












$('#tb-slide-left').click(function () {
    var position = $('.time_line .time_slider').position();
    var left_pos = position.left + 71;
    left_pos = left_pos > 0 ? 0 : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    //$('.maid_name').css('padding-left', -position.left + 'px');
    position.left = position.left == 0 ? -71 : position.left;
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -position.left + 'px',
    }, 71);
});
$('.selector').selectable({
    cancel: ".booked",
    //distance: 1,
    start: function (event, ui) {
        $('.ui-selected').removeClass('ui-selected');
    },
    stop: function (event, ui) {
        var maid_id = $(this).attr('data-maid');
        var maid_name = $(this).attr('data-maid-name');
        var week_day = $(this).attr('data-week');
        var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
        var time_from = $('.ui-selected').first().attr('data-time-from');
        var time_to = $('.ui-selected').last().attr('data-time-to');
        $('#new_booking_form input[name="service_start_date"]').datepicker('update', new Date());
        $('#new_booking_form input[name="service_end_date"]').val("").datepicker('setStartDate', moment().add(7 * 3, 'days').format('DD/MM/YYYY'));
        $('#new_booking_form input[name="service_week_day"]').val(week_day);
        $('#new_booking_form input[name="service_week_name"]').val(week_name);
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
        $('#new_booking_form input[name="maid_id"]').val(maid_id);
        $('#new_booking_form input[name="maid_name"]').val(maid_name);
        $('#new_booking_form select[name="time_from"]').select2().val(time_from).trigger("change.select2");
        $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(time_from)).select2().val(time_to).trigger("change.select2");
        $('#new_booking_form select[name="customer_id"]').val("").trigger("change.select2");
        $('#new_booking_form select[name="service_type_id"]').select2().val(6).trigger("change.select2");
        $('#new_booking_form input[name="cleaning_materials"]').prop('checked', false);
        $('#new_booking_form input[name="booking_id"]').val(""); // important
        $('#new_booking_form input[name="service_rate_per_hour"]').val("");
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val("");
        $('#new_booking_form .vat_percentage').html(service_vat_percentage.toFixed(2));
        $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val(10);
        $('#new_booking_form select[name="customer_id"]');
        $('#new_booking_form .modal-footer :input');
        reCalculateForm();
        /*********************************** */
        $('#new-booking-form-popup .title').html('New Booking');
        $('#new_booking_form select[name="customer_id"]').next().show();
        $('#new_booking_form input[name="customer_name"]').val("").hide();
        $("#new-booking-form-popup .modal-footer.new").show();
        $("#new-booking-form-popup .modal-footer.edit").hide();
        new_booking_form_validator.resetForm();
        // enable fields
        $('#new_booking_form select[name="service_type_id"]').prop("disabled", false);
        $('#new_booking_form select[name="time_from"]').prop("disabled", false);
        $('#new_booking_form select[name="time_to"]').prop("disabled", false);
        $('#new_booking_form input[name="service_start_date"]').prop("disabled", false);
        $('#new_booking_form input[name="service_end_date"]').prop("disabled", false);
        $('#new_booking_form input[name="service_rate_per_hour"]').prop("disabled", false);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').prop("disabled", false);
        $('#new_booking_form input[name="cleaning_materials"]').prop("disabled", false);
        $('#new_booking_form rf').show();
        //
        fancybox_show('new-booking-form-popup', { width: 750 });
        $('#customer-picked-address').html('');
        $('#customer-address-panel-user').hide()
    }
});
if ($(".booking-position").length > 0) {
    var stickyTop = $('.booking-position').offset().top;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= stickyTop) {
            $('.scroll-top-fix').addClass('box-fixed-top');
            $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
            $('#schedule').addClass('schedule_topfix');
        } else {
            $('.scroll-top-fix').removeClass('box-fixed-top');
            $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
            $('#schedule').removeClass('schedule_topfix');
        }
    });
}
function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
function RefreshSomeEventListener() {
    $(".booked_bubble").off();
    $(".booked_bubble").on("click", function () {
        //alert('b');
    });
}
function renderBookings() {
    loader(true);
    // reset cells
    $('.booked_bubble').remove();
    $('.cell').removeClass("booked")
    //
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "laravel/booking/regular_schedules",
        // data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() || undefined },
        data: { start_date: $('#filter_start_date').val() ? (moment($('#filter_start_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined, end_date: $('#filter_end_date').val() ? (moment($('#filter_end_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')) : undefined },
        success: function (data) {
            var booking_delete_list = data.booking_delete_data;
            var total_hours = data.total_hours;
            var total_hour_week = data.total_hour_week;
            var grand_total = data.grand_total;
            var training_hours = data.training_hours;
            var free_hours = data.free_hours;
            var grand_suspend_total = data.grand_suspend_total;
            var maid_count = data.maid_count;
            var emergency_leave_count = data.emergency_leave_count;
            var vacations_count = data.vacations_count;
            var holidays_count = data.holidays_count;
            var medical_leaves_count = data.medical_leaves_count;
            var total_maid_leave_count = data.total_maid_leave_count;
            var present_maid_count = data.present_maid_count;
            var vacations_week_count = data.vacations_week_count;
            var holidays_week_count = data.holidays_week_count;
            var medical_leaves_week_count = data.medical_leaves_week_count;
            var emergency_leave_week_count = data.emergency_leave_week_count;
            var total_sales = data.total_sales;
            var lost_sales = data.lost_sales;
            var maid_leaveData = data.maid_leaveData;
            // maid leaves count
            var counting_emergency_hours = emergency_leave_week_count * 8;
            $('#emergency_leave_count').html(emergency_leave_week_count +' - '+ counting_emergency_hours + `hrs`);
            var counting_vacations_hours = vacations_week_count * 8;
            $('#vacations').html(vacations_week_count +' - '+ counting_vacations_hours + `hrs`);
            var counting_holidays_hours = holidays_week_count * 8;
            $('#holidays').html(holidays_week_count +' - '+ counting_holidays_hours + `hrs`);
            var counting_medical_leaves_hours = medical_leaves_week_count * 8;
            $('#medical_leaves').html(medical_leaves_week_count +' - '+ counting_medical_leaves_hours + `hrs`);
            $('#absents_count').html(total_maid_leave_count);
            $('#presents_count').html(present_maid_count);

            updateTotalHourWeek(data.total_hour_week);
            updateLeaveVisibility(data.maid_leaveData);
            updateTotalHour(data.total_hours);
            $('#grand_percentage').html('');
            $('#grand_total').html('');
            $('#training_hours').html('');
            $('#free_hours').html('');
            $('#total_sales').html('');
            $('#lost_sales').html('');
            $('#gained_hours').html('');
            $('#lost_hours').html('');
            $('#total_hours').html('');
            $.each(data.bookings, function (key, booking) {
                // var new1 = data.booking_delete_data.filter(booking.booking_id)
                let delete_list = booking_delete_list.find(delete_list => {
                    return delete_list.booking_id == booking.booking_id
                });
                let maid_list = maid_leaveData.find(maid_list => {
                    // return maid_leave_data.some(leave => leave.maid_id === maidId && leave.leave_date === date);
                    return maid_list.maid_id  == booking.maid_id && maid_list.service_week_day ==  booking.service_week_day 
                });

                var color = maid_list ? 'leave_data_bubble' : 'booked_bubble';
                var cells = booking.working_minutes / 30;
                if (delete_list == null) {
                    if (booking.parent_maid_name != null) {
                        var maid_name = `<p class="n_booking_maid_name">` + booking.parent_maid_name + `&nbsp;&nbsp;Temporary till..&nbsp;&nbsp;` + booking.service_start_date + ` - ` + booking.service_actual_end_date + `-` + `Reason(` + booking.booking_reassign_reason + `)</p>`;
                        var tooldata = booking.parent_maid_name + `&nbsp;&nbsp;Temporary till(` + booking.service_start_date + ` - ` + booking.service_actual_end_date + `)`;
                    } else {
                        var maid_name = "";
                        var tooldata = "";
                    }
                    var bubble = `<div class="`+ color +`" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="` + tooldata + `">
                    <p class="n_booking_customer_name"><i class="fa fa-user" aria-hidden="true"></i>` + booking.customer_name + `</p>
                    <p class="n_booking_time"><i class="fa fa-clock-o" aria-hidden="true"></i>` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours</p>
                    <p class="n_booking_location"><i class="fa fa-map-marker" aria-hidden="true"></i>` + booking.booking_location + ` ~  ` + booking.booking_zone + `</p>
                    `+ maid_name + `</div>`;
                }
                else {
                    var bubble = `<div class="deleted_bubble" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="">
                     <p class="n_booking_customer_name"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;` + booking.customer_name + `</p>
                  
                    <p class="n_booking_time"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours</p>
                    <p class="n_booking_location"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;` + booking.booking_location + ` ~ ` + booking.booking_zone + `</p>
                     <p class="n_booking_maid_name"> Suspended(` + booking.delete_date_range + `)</p>
                      <p class="n_booking_maid_name">Reason(` + booking.cancel_reason + `)</p>
                    <p class="n_booking_maid_name">Remarks(` + booking.remarks + `)</p></div>`

                }

                // $.each(total_hours, function (key1, total) {
                //     var working_minutes1 = total.working_minutes / 60;
                //     var max_time = 56;
                //     var percentage = (working_minutes1 / (max_time / 100));
                //     percentage = percentage.toFixed(2)
                //     percentage =  percentage+`%`;
                //     $('[data-maid-week-id="' + total.maid_id +'"]').html(working_minutes1);
                //     $('[data-maid-id-week-id="' + total.maid_id +'"]').html(percentage);

                // });

                // $.each(total_hour_week, function (key, tot) {
                //     var working_minutes1 = tot.working_minutes / 60;
                //     var max_time = 8;
                //     var percentage = (working_minutes1 / (max_time / 100));
                //     percentage = percentage.toFixed(2)
                //     percentage =  percentage+`%`;
                //     $('[data-maid-id="'+ tot.maid_id +'"][data-week-id="' + tot.service_week_day + '"]').html(working_minutes1);
                //     $('[data-maid-id-percentage="' + tot.maid_id +'"][data-week-id-percentage="' + tot.service_week_day + '"]').html(percentage);

                // });

                //grand total percentage
                var total_maid_hours = maid_count * 56;
                var grantTotal = grand_total / 60;
                var grand_percentage = (grantTotal / (total_maid_hours / 100))
                grand_percentage = grand_percentage.toFixed(2)
                grand_percentage = grand_percentage + `%`;

                // Grand Suspend total hours
                var suspend_total = grand_suspend_total / 60;
                $('#lost_hours').html(suspend_total + `hrs`);

                // Gained total hours 
                $('#gained_hours').html(grantTotal + `hrs`);
                //Regular schedule total Hrs
                var schedule_total = grantTotal + suspend_total;
                $('#total_hours').html(schedule_total + `hrs`);

                $('#grand_percentage').html(grand_percentage);
                $('#grand_total').html(grantTotal);

                // training_hours 
                var Training_hours = training_hours / 60;
                $('#training_hours').html(Training_hours+`hrs`);
                // free_hours
                var Free_hours = free_hours / 60;
                $('#free_hours').html(Free_hours+`hrs`);

                $('#total_sales').html(total_sales);
                $('#lost_sales').html(lost_sales);
                // else {
                //     var bubble = `<div class="deleted_bubble" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title=""><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;` + booking.customer_name + `
                //     <br>
                //     <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours
                //     <p style="color:#294C5A;font-weight:bold;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;` + booking.booking_location + `</p>
                //     <p style="color:#294C5A;font-weight:bold;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;` + booking.booking_zone + `</p>
                //     <p style="color:#294C5A;">&nbsp; Suspended(` + booking.delete_date_range + `)</p></div>
                //     <p style="color:#294C5A;">&nbsp; Remarks(` + booking.remarks + `)</p></div>`
                // <p class="n_booking_remark"><i class="fa fa-map-marker" aria-hidden="true"></i>test6767676565656</p>
                // <p class="n_booking_maid_name"><i class="fa fa-map-marker" aria-hidden="true"></i>testdummmyygff6</p>;

                // }
                // $new = data.deleted_booking_data.filter(booking.booking_id)

                var element = $('[data-maid-week="' + booking.maid_id + "-" + booking.service_week_day + '"] [data-time-from="' + booking.time_from + '"]');
                $(element).html(bubble).addClass("booked").off().on("click", function () {
                    $('.mm-loader').show();
                    $.ajax({
                        type: 'GET',
                        dataType: "json",
                        url: _base_url + "laravel/booking/get_booking_data",
                        data: { booking_id: booking.booking_id },
                        success: function (data) {
                            var booking_data = data.booking_data;
                            var maid_id = booking_data.maid_id;
                            var week_day = booking_data.service_week_day;
                            var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
                            $('#new_booking_form input[name="service_week_day"]').val(week_day);
                            $('#new_booking_form input[name="maid_id"]').val(maid_id);
                            $('#new_booking_form input[name="customer_name"]').val(booking_data.customer_name);
                            $('#new_booking_form input[name="maid_name"]').val(booking_data.maid_name);
                            $('#new_booking_form select[name="service_type_id"]').select2().val(booking_data.service_type_id).trigger("change.select2");
                            $('#new_booking_form select[name="time_from"]').select2().val(booking_data.time_from).trigger("change.select2");
                            $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(booking_data.time_from)).select2().val(booking_data.time_to).trigger("change.select2");
                            $('#new_booking_form input[name="working_hours"]').val(hourDifference(booking_data.time_from, booking_data.time_to));
                            $('#new_booking_form input[name="service_week_name"]').val(week_name);
                            $('#new_booking_form input[name="service_start_date"]').val(moment(booking_data.service_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                            $('#new_booking_form input[name="service_end_date"]').val(booking_data.service_end_date ? moment(booking_data.service_end_date, 'YYYY-MM-DD').format('DD/MM/YYYY') : undefined);
                            $('#new_booking_form input[name="service_rate_per_hour"]').val(booking_data.price_per_hr);
                            $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(booking_data._service_discount_rate_per_hour);
                            $('#new_booking_form input[name="cleaning_materials"]').prop('checked', booking_data._cleaning_material == "Y");
                            $('#new_booking_form input[name="charge_type1"][value="' + booking_data.charge_type + '"]').prop('checked', true);
                            if (booking_data._cleaning_material == "Y") {
                                $('#new_booking_form #material').show();
                                $('#new_booking_form input[name="material_cost"][value="' + booking_data.material_cost + '"]').prop('checked', true);
                            } else {
                                $('#new_booking_form #material').hide();
                            }
                            $('#new_booking_form input[name="cleaning_material_rate_per_hour1"]').val(booking_data._cleaning_material_rate_per_hour);
                            $('#new_booking_form .vat_percentage').html(booking_data._vat_percentage);
                            $('#new_booking_form input[name="booking_id"]').val(booking.booking_id); // need for edit
                            reCalculateForm();
                            /*********************************** */
                            $('#new-booking-form-popup .title').html('Booking ~ <i>' + booking_data.reference_id + "</i>");
                            $('#new_booking_form select[name="customer_id"]').next().hide();
                            $('#new_booking_form input[name="customer_name"]').show();
                            $("#new-booking-form-popup .modal-footer.new").hide();
                            $("#new-booking-form-popup .modal-footer.edit").hide();
                            new_booking_form_validator.resetForm();
                            // disable fields
                            $('#new_booking_form select[name="service_type_id"]').prop("disabled", true);
                            $('#new_booking_form select[name="time_from"]').prop("disabled", true);
                            $('#new_booking_form select[name="time_to"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_start_date"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_end_date"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_rate_per_hour"]').prop("disabled", true);
                            $('#new_booking_form input[name="service_discount_rate_per_hour"]').prop("disabled", true);
                            $('#new_booking_form input[name="cleaning_materials"]').prop("disabled", true);
                            $('#new_booking_form rf').hide();
                            //
                            fancybox_show('new-booking-form-popup', { width: 750 });
                        },
                        error: function (data) {
                            toast('warning', "An error occured !");
                            $('.mm-loader').hide();
                        },
                    });
                });
            });
            RefreshSomeEventListener();
            styleThisWeekCells();
            loader(false);
        },
        error: function (data) {
            toast('warning', "An error occured !");
            loader(false);
        },
    });
}

function updateTotalHourWeek(total_hour_week) {
    // Clear previous data
    $('[data-maid-id]').html('');
    $('[data-maid-id-percentage]').html('');
    // $('[data-maid-week-id]').html('');
    // $('[data-maid-id-week-id]').html('');

    // Update HTML content with new data
    $.each(total_hour_week, function (key, tot) {
        var working_minutes1 = tot.working_minutes / 60;
        var max_time = 8;
        var percentage = (working_minutes1 / (max_time / 100));
        percentage = percentage.toFixed(2)
        percentage = percentage + `%`;
        $('[data-maid-id="' + tot.maid_id + '"][data-week-id="' + tot.service_week_day + '"]').html(working_minutes1);
        $('[data-maid-id-percentage="' + tot.maid_id + '"][data-week-id-percentage="' + tot.service_week_day + '"]').html(percentage);
    });
}
function updateLeaveVisibility(maid_leaveData) {
    // console.log('Leave Data:', maid_leaveData);
    $('.leavetypes').hide();
    
    maid_leaveData.forEach(function(leave) {
        var maidId = leave.maid_id;
        var weekDayId = leave.service_week_day;
        var leaveType = leave.maid_type;

        // Find the corresponding day-column element
        var dayColumn = $(`[data-maid-column="${maidId}"][data-week-column="${weekDayId}"]`);
        dayColumn.addClass('day-column');

        // Create the new leave HTML
        var newLeaveHtml = `
            <div class="leavetypes slot dayoff" data-maid-leave="${maidId}" data-week-leave="${weekDayId}">
                <div class="leaves">${leaveType.replace(/_/g, ' ')}</div>
            </div>
        `;

        // Append the new leave HTML after the day-column element
        dayColumn.after(newLeaveHtml);
    });
}

function updateTotalHour(total_hours) {
    // Clear previous data
    $('[data-maid-week-id]').html('');
    $('[data-maid-id-week-id]').html('');

    // Update HTML content with new data
    $.each(total_hours, function (key1, total) {
        var working_minutes1 = total.working_minutes / 60;
        var max_time = 56;
        var percentage = (working_minutes1 / (max_time / 100));
        percentage = percentage.toFixed(2)
        percentage = percentage + `%`;
        $('[data-maid-week-id="' + total.maid_id + '"]').html(working_minutes1);
        $('[data-maid-id-week-id="' + total.maid_id + '"]').html(percentage);

    });
}
function styleThisWeekCells() {
    var currentTime = moment();
    var dow = currentTime.day();
    var time_rounded = moment(currentTime).format("H:00:00");
    $('.cell').removeClass("current-week-day");
    $('.cell').removeClass("current-time");
    $('.cell').removeAttr('style');
    $('.cell').each(function (index, option) {
        /******* same week day highlight  */
        if ($(this).attr("data-week") == dow) {
            $(this).addClass("current-week-day");
        }
        /******* current time highlight  */
        if ($(this).attr("data-time-from") == time_rounded) {
            //$(this).addClass("current-time");
            $(this).css({ "border-right": "1px solid rgb(255 150 150)" });
        }
    });
}
$(document).ready(function () {
    renderBookings();
});

/********************************************************************************** */
// date picker
$('.datepicker').datepicker({ autoclose: true, todayHighlight: true, todayBtn: "linked" }).on('changeDate', function (event) {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var date_full = moment(date).format('DD/MM/YYYY');
    $('.hed-date-main-box').html(date_full);
    $('#date').val(date);
});
$('.hed-date-left-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(-1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('.hed-date-right-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
/***************************************************************************************** */
$('#new_booking_form select[name="time_from"]').on('change', function () {
    $('#new_booking_form input[name="working_hours"]').val(0);
    $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change.select2");
});
$('#new_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
/***************************************************************************************** */
function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#new_booking_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
/***************************************************************************************** */
$('#new_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_start_date"]').valid();
    var booking_date = $(this).val();
    const date = moment(booking_date, 'DD/MM/YYYY');
    const dow = date.day();
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    $('#new_booking_form input[name="service_week_day"]').val(dow);
    $('#new_booking_form input[name="service_week_name"]').val(days[dow]);

    // alert(week_day)
    $('#new_booking_form input[name="service_end_date"]').datepicker('update', null).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").add(7 * 3, 'days').format('DD/MM/YYYY'));
});
$('#new_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_end_date"]').valid();
});
$('#new_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
}).on("change", function (e) {
    var cust_id = $(this).val();
    open_address_panel(cust_id)
    $(this).valid();
});
$('#new_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="time_to"]').select2().on("change", function (e) {
    $(this).valid();
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_week_day"]').on('change', function () {
    var week_day_id = this.value;
    var maid_id = $('#schedule-menu select[name="filter_maid_id"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    leaves(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
        $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"][data-week-maid-id-summary="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            // $('.tb-slider .leavetypes[data-week-leave="' + week_day_id + '"][data-maid-leave="' + maid_id + '"]').show();
            $('.render_leave').each(function () {
                var maidColumn = $(this);
                var maidColumnId =  maid_id;
                var weekColumnId = week_day_id;
                // var weekColumnId = maidColumn.data('week-column');
    
                // Check for leave data based on maid_id and week_day_id
                var leaveData = $(`.leavetypes[data-maid-leave="${maidColumnId}"][data-week-leave="${weekColumnId}"]`);
                // console.log(`Checking maidColumnId: ${maidColumnId}, WeekColumnId: ${weekColumnId}, LeaveData Length: ${leaveData.length}`);
    
                // If leave data exists, show the day-column
                if (leaveData.length > 0) {
                    maidColumn.addClass('day-column');
                    $(`.tb-slider .leavetypes[data-week-leave="${weekColumnId}"][data-maid-leave="${maidColumnId}"]`).show();

                } else {
                    // Otherwise, hide the day-column
                    maidColumn.removeClass('day-column');
            $(`.tb-slider .leavetypes[data-week-leave="${weekColumnId}"][data-maid-leave="${maidColumnId}"]`).hide();

                }
            });
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
            $('.tb-slider .leavetypes[data-week-leave="' + week_day_id + '"]').show();
            $('.render_leave').each(function () {
                var maidColumn = $(this);
                var maidColumnId =  maidColumn.data('maid-column');
                var weekColumnId = week_day_id;
                // var weekColumnId = maidColumn.data('week-column');
    
                // Check for leave data based on maid_id and week_day_id
                var leaveData = $(`.leavetypes[data-week-leave="${weekColumnId}"][data-maid-leave="${maidColumnId}"]`);
                // console.log(`Checking maidColumnId: ${maidColumnId}, WeekColumnId: ${weekColumnId}, LeaveData Length: ${leaveData.length}`);
    
                // If leave data exists, show the day-column
                if (leaveData.length > 0) {
                    maidColumn.addClass('day-column');
                } else {
                    // Otherwise, hide the day-column
                    maidColumn.removeClass('day-column');
                }
            });
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .leavetypes[data-maid-leave="' + maid_id + '"]').show();
            $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-maid-id-summary="' + maid_id + '"]').show();
            $('.render_leave').each(function () {
                var maidColumn = $(this);
                var maidColumnId =  maid_id
                var weekColumnId = maidColumn.data('week-column');
    
                // Check for leave data based on maid_id and week_day_id
                var leaveData = $(`.leavetypes[data-maid-leave="${maidColumnId}"][data-week-leave="${weekColumnId}"]`);
                // console.log(`Checking maidColumnId: ${maidColumnId}, WeekColumnId: ${weekColumnId}, LeaveData Length: ${leaveData.length}`);
    
                // If leave data exists, show the day-column
                if (leaveData.length > 0) {
                    maidColumn.addClass('day-column');
                } else {
                    // Otherwise, hide the day-column
                    maidColumn.removeClass('day-column');
                }
            });
        }
        else {
            maidWeeks(true);
            slots(true);
            leaves(true);
        }
    }
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_maid_id"]').on('change', function () {
    var maid_id = this.value;
    var week_day_id = $('#schedule-menu select[name="filter_week_day"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    leaves(false);
    if (maid_id) {
        // console.log("hiii")
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .leavetypes[data-maid-leave="' + maid_id + '"]').show();
        $('.render_leave').each(function () {
            var maidColumn = $(this);
            var maidColumnId = maid_id;
            var weekColumnId = maidColumn.data('week-column');

            // Check for leave data based on maid_id and week_day_id
            var leaveData = $(`.leavetypes[data-maid-leave="${maidColumnId}"][data-week-leave="${weekColumnId}"]`);
            // console.log(`Checking maidColumnId: ${maidColumnId}, WeekColumnId: ${weekColumnId}, LeaveData Length: ${leaveData.length}`);

            // If leave data exists, show the day-column
            if (leaveData.length > 0) {
                maidColumn.addClass('day-column');
            } else {
                // Otherwise, hide the day-column
                maidColumn.removeClass('day-column');
            }
        });
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        console.log("hellooo")
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"][data-week-maid-id-summary="' + maid_id + '"]').show();
            $('.tb-slider .leavetypes[data-week-leave="' + week_day_id + '"][data-maid-leave="' + maid_id + '"]').show();
            $('.render_leave').each(function () {
                var maidColumn = $(this);
                var maidColumnId = maid_id;
                var weekColumnId = week_day_id;
                // var weekColumnId = maidColumn.data('week-column');
    
                // Check for leave data based on maid_id and week_day_id
                var leaveData = $(`.leavetypes[data-week-leave="${weekColumnId}"][data-maid-leave="${maidColumnId}"]`);
                // console.log(`Checking maidColumnId: ${maidColumnId}, WeekColumnId: ${weekColumnId}, LeaveData Length: ${leaveData.length}`);
    
                // If leave data exists, show the day-column
                if (leaveData.length > 0) {
                    maidColumn.addClass('day-column');
                } else {
                    // Otherwise, hide the day-column
                    maidColumn.removeClass('day-column');
                }
            });
        }
        else {
            // console.log("welcome")
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
            $('.tb-slider .leavetypes[data-week-leave="' + week_day_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-day-summary="' + week_day_id + '"]').show();
            $('.render_leave').each(function () {
                var maidColumn = $(this);
                var maidColumnId =  maidColumn.data('maid-column');;
                var weekColumnId = week_day_id;
                // var weekColumnId = maidColumn.data('week-column');
    
                // Check for leave data based on maid_id and week_day_id
                var leaveData = $(`.leavetypes[data-week-leave="${weekColumnId}"][data-maid-leave="${maidColumnId}"]`);
                // console.log(`Checking maidColumnId: ${maidColumnId}, WeekColumnId: ${weekColumnId}, LeaveData Length: ${leaveData.length}`);
    
                // If leave data exists, show the day-column
                if (leaveData.length > 0) {
                    maidColumn.addClass('day-column');
                } else {
                    // Otherwise, hide the day-column
                    maidColumn.removeClass('day-column');
                }
            });

        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .leavetypes[data-maid-leave="' + maid_id + '"]').show();
            $('#booking-summary-box .maids-weekly-summary[data-maid-id-summary="' + maid_id + '"]').show();
            $('#booking-summary-box .week-day-summary[data-week-maid-id-summary="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
            leaves(true);
        }
    }
});
/***************************************************************************************** */
function maidHeads(show) {
    if (show == true) {
        $('#weeks .maid_name_week').show();
        $('.tb-slider .maid_name').show();
        $('#booking-summary-box .maids-weekly-summary').show();
    }
    else {
        $('#weeks .maid_name_week').hide();
        $('.tb-slider .maid_name').hide();
        $('#booking-summary-box .maids-weekly-summary').hide();

    }
}
function maidWeeks(show) {
    if (show == true) {
        $('#weeks .week').show();
        $('#booking-summary-box .week-day-summary').show();
    }
    else {
        $('#weeks .week').hide();
        $('#booking-summary-box .week-day-summary').hide();

    }
}
function slots(show) {
    if (show == true) {
        $('.tb-slider .slots').show();
    }
    else {
        $('.tb-slider .slots').hide();
    }
}

function leaves(show) {
    if (show == true) {
        $('.tb-slider .leavetypes').show();
    }
    else {
        $('.tb-slider .leavetypes').hide();
    }
}
/***************************************************************************************** */
$().ready(function () {
    new_booking_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: false,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            },
            cleaning_material_rate_per_hour: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
            cleaning_material_rate_per_hour: "Enter materials per hour rate."
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "cleaning_material_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#new_booking_form").serializeArray();
            var service_start_date_i = values.map(function (o) { return o.name; }).indexOf("service_start_date");
            var service_end_date_i = values.map(function (o) { return o.name; }).indexOf("service_end_date");
            if (values[service_start_date_i]['value']) {
                values[service_start_date_i]['value'] = moment(values[service_start_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            if (values[service_end_date_i]['value']) {
                values[service_end_date_i]['value'] = moment(values[service_end_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/" + ($('#new_booking_form input[name="booking_id"]').val() == "" ? "save_booking" : "update_booking"),
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#customer-picked-address").html('');
                        // setTimeout(function () {
                        //     window.location.href = _base_url + "schedule/week_view";
                        // }, 1000);
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
/***************************************************************************************** */
var cleaning_material_rate_per_hour = 0;
function reCalculateForm() {
    cleaning_material_rate_per_hour = $('#new_booking_form input[name="cleaning_material_rate_per_hour1"]').val();
    var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#new_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#new_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#new_booking_form input[name="material_fee"]').val(material_fee);
    $('#new_booking_form input[name="service_amount"]').val(service_amount);
    $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#new_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#new_booking_form input[name="service_discount"]').val(service_discount);
    //$('#new_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#new_booking_form input[name='cleaning_material_rate_per_hour1'],#new_booking_form select[name='time_to']").change(function () {
    // $("#new_booking_form input[name='cleaning_materials'],#new_booking_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $(this).valid();
});
$("#new_booking_form input[name='service_discount_rate_per_hour'],#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
// $('body').on('change', '#new_booking_form #free-service', function () {
$('body').on('change', '#new_booking_form input[name="cleaning_materials"]', function () {

    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        $('#new_booking_form #material').show();
    }else {
        $('#new_booking_form #material').hide();
    }
});
$('body').on('change', '#new_booking_form input[name="material_cost"]', function () {
    if ($(this).val() == "0") {
        $('#new_booking_form input[name="cleaning_material_rate_per_hour1"]').attr('readonly', true);
        $('#new_booking_form input[name="cleaning_material_rate_per_hour1"]').val(10);
    } else if ($(this).val() == "1") {

    $('#new_booking_form input[name="cleaning_material_rate_per_hour1"]').attr('readonly', false);
        $('#new_booking_form input[name="cleaning_material_rate_per_hour1"]').val('');
    } 
});	
$('body').on('change', '#new_booking_form input[name="charge_type1"]', function () {
    if ($(this).val() == "0") {
        $('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', false);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', false);
        $('#new_booking_form input[name="service_rate_per_hour"]').val('');
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val('');
    } else if ($(this).val() == "1") {
        // if ($('#od_booking_form #free-service').is(':checked') == true) {

        $('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="service_rate_per_hour"]').val(0);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(0);
    } else if($(this).val() == "2") {
        $('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="service_rate_per_hour"]').val(0);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(0);
    }
});
$(function () {
    $('[data-toggle="popover"]').popover()
})
/***************************************************************************************** */
// page filter options
$('#filter_start_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    // startDate: "today"
}).on('changeDate', function (e) {
    //$('#new_booking_form input[name="service_start_date"]').valid();
    var end_date = moment(this.value, "DD.MM.YYYY");
    var end_date = end_date.add(5, 'days').format('DD/MM/YYYY');
    $('#filter_end_date').datepicker('update', end_date).datepicker('setStartDate', moment(this.value, "DD/MM/YYYY").format('DD/MM/YYYY'));
    renderBookings();
});
$('#filter_end_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true,
}).on('changeDate', function (e) {
     renderBookings();
    //$('#new_booking_form input[name="service_end_date"]').valid();
});

$('#schedulte_delete').click(function () {
    // var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    // var service_date = $('#new_booking_form input[name="service_date"]').val();
    // var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    fancybox_show('alert-popup', { width: 450 });
    //    $('#day_service_id').val(day_service_id);
    $('#booking_id_perment').val(booking_id);
    $('#booking_id_day').val(booking_id);
    //    $('#service_date').val(service_date);
    //    $('#day_service_status').val(service_status);

});

$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    // $('.n-delete-set-left').addClass('de-select');

    //$('.n-delete-set-right-cont').show(500);
    $('.n-delete-set-left-cont').show(500);
});

// $(".n-delete-set-left").click(function () {
//     $('.n-delete-set-left').removeClass('de-select');
//     $('.n-delete-set-right').addClass('de-select');

//     $('.n-delete-set-left-cont').show(500);
//     $('.n-delete-set-right-cont').hide(500);
// });

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    var booking_id = $('#booking_id_perment').val();
    if (remarks == "") {
        $("#delete_remark_perm").focus();
        $('#deleteremarks_book').css('display', 'block');
    } else {
        $('#deleteremarks_book').css('display', 'none');
        $.ajax({
            type: "POST",
            url: _base_url + 'schedule/delete_permeant_booking_schedule',
            data: { remarks: remarks, booking_id: booking_id },
            cache: false,
            success: function (data) {
                if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
        });
    }
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#booking_id_day').val();
    if (remarks == "") {
        $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display', 'block');
    } else {
        $('#deleteremarks_book_day').css('display', 'none');
        $.ajax({
            type: "POST",
            url: _base_url + 'schedule/delete_booking_one_day',
            data: { remarks: remarks, booking_id: booking_id },
            cache: false,
            success: function (data) {
                if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
        });
    }
});

function open_address_panel(customer_id) {
    $('#customer-address-id-user').val('');
    $('#customer-address-panel-user').hide();
    $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');

    if ($.isNumeric(customer_id) && customer_id > 0) {
        $('#customer-address-panel-user').slideDown();
        $.ajax({
            url: _base_url + 'schedule/get_customer_address_details',
            type: 'POST',
            data: { customer_id: customer_id },
            dataType: 'json',
            success: function (response) {
                // console.log(response)
                // $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response)
                // {
                if (response == 'refresh') {
                    $.fancybox.open({
                        autoCenter: true,
                        fitToView: false,
                        scrolling: false,
                        openEffect: 'fade',
                        openSpeed: 100,
                        helpers: {
                            overlay: {
                                css: {
                                    'background': 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: false
                            }
                        },
                        padding: 0,
                        closeBtn: false,
                        content: '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                    });
                }
                else {
                    var adressdata = response.customer_address;
                    // console.log(adressdata)			
                    var address_html = '<div class="table">';

                    $.each(adressdata, function (key, val) {
                        address_html += '<div class="row"><div class="cell1"><input type="hidden" id="address_location" value="' + val.location_name + '"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';

                    });

                    address_html += '</div>';

                    $('#customer-address-panel-user .inner').html(address_html);
                }
            }
        });
    }
}

//End
$('body').on('click', '#customer-address-panel-user .close', function () {
    $('#customer-address-panel-user').slideUp(function () { $('#customer-picked-address').show(); });
});


$('body').on('click', '.pick_customer_address', function () {
    var address_id = $(this).attr('id').replace('cadddress-', '');
    $('#customer-address-id-user').val(address_id);
    var addzone = $('#caddzone-' + address_id).html();
    var address = $('#cadd-' + address_id).html();
    // $('#customer-address').empty();
    // $('#customer-location').empty();
    // $('#customer-area-zone').empty();
    // $('#customer-sublocation').empty();
    $('#new_booking_form input[name="customer_address_id"]').val('');

    $('#customer-address-panel-user').slideUp(function () {
        // $('#customer-address').append(address);
        // $('#customer-area-zone').append(addzone);
        // $('#customer-location').append(response.customer_address[0]['location_name']);
        // $('#customer-sublocation').append(response.customer_address[0]['landmark_name']);
        $('#new_booking_form input[name="customer_address_id"]').val(address_id);
        $('#customer-picked-address').html('<div class="address"><strong>' + addzone + '</strong> - ' + address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
        $('#customer-picked-address').show();
        $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');

        var booking_id = $('#booking-id-user').val();
    });
});

$('body').on('click', '#chg-cust-address', function () {
    var _customer_id = $('#b-customer-id').val();
    $('#customer-picked-address').hide();
    open_address_panel(_customer_id);
});