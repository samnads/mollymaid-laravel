/************************************************************************** */
/* Coder : Samnad. S                                                        */
/*                                                                          */
/* Last Updated by :                                                        */
/************************************************************************** */
// logic goes here...................
/************************************************************************** */
$('[data-tab]').click(function (event) {
    $('.widget-header li[class^="tab"]').hide();
    var tab_name = $(this).attr("data-tab");
    $('.widget-header li.' + tab_name + '').show();
    if (tab_name == "tab4") {
        renderMappedDriverDataAll();
    }
    else if (tab_name == "tab1") {
        renderMappedDriverData();
    }
    else if (tab_name == "tab2") {
        renderUnconfirmedDriverData();
        // renderUnconfirmedDriverPrintData();
    }
    else if (tab_name == "tab3") {
        renderConfirmedDriverData();
        renderConfirmedDriverPrintData();
    }
    else {
        $('.widget-header button').hide();
    }
});
/************************************************************************** */
$('[data-action="confirm-driver-mapping-popup"]').click(function (event) {
    $('.mm-loader').show();
    $('#confirm_driver_mapping_popup_form button[type="submit"]').hide();
    $("#confirm_driver_mapping_popup_form #check_all").prop("checked", false);
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/day_services_driver_unconfirmed",
        data: {
            service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')
        },
        success: function (data) {
            var data = data.data;
            var html = ``;
            $.each(data, function (key, schedule) {
                if (schedule.driver_id) {
                    html += `
                    <div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                    <div class="col-sm-10 n-pick-maid-left pl-0">
                        <span id=""><strong>Service No. &nbsp;</strong>`+ schedule.day_service_id + `</span><br>
                        <span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;`+ schedule.customer_name + `</span>
                        <p><strong><i class="fa fa-clock-o" aria-hidden="true"></i></strong>&nbsp;` + moment(schedule.time_from, ["HH:mm:ss"]).format("hh:mm A") + ` - ` + moment(schedule.time_to, ["HH:mm:ss"]).format("hh:mm A") + ` ( ` + schedule.maid_name + ` )</p>
                    </div>
                    <div class="col-sm-2 n-pick-maid-right pr-0">
                        <input type="checkbox" value="`+ schedule.day_service_id + `" name="day_service_ids[]" id="day_service_id_` + key + `">
                        <label for="day_service_id_` + key + `">
                        <span class="border-radius-3"></span>
                        </label>
                        <p class="text-success"></p>
                    </div>
                </div>`;
                }
            });
            if (html == "") {
                toast('warning', "No dispatches available to confirm.");
                return false;
            }
            fancybox_show('confirm-driver-mapping-popup', { width: 600 });
            $('#confirm_driver_mapping_popup_form .inner').html(html);
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
/************************************************************************** */

$('li select[name="filter_driver_id"]').change(function () {
    var current_tab = $('.tab-pane.active').attr('id');
    $('li [data-tab="' + current_tab + '"]').trigger('click');
});

$('li select[name="filter_zone_id"]').change(function () {
    var current_tab = $('.tab-pane.active').attr('id');
    $('li [data-tab="' + current_tab + '"]').trigger('click');
});

$('li select[name="filter_maid_id"]').change(function () {
    var current_tab = $('.tab-pane.active').attr('id');
    $('li [data-tab="' + current_tab + '"]').trigger('click');
});
/************************************************************************** */
function renderMappedDriverData() {
    $('#mapped-drivers-holder').html(`<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>`)
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/mapping_data",
        data: { service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') },
        success: function (data) {
            var data = data.data;
            if (data.length == 0) {
                toast('warning', "No areas found, add areas to continue");
                return false;
            }
            $('#mapped-drivers-holder').html(``)
            $.each(data, function (key, driver) {
                var table = ``;
                var trs = ``;
                $.each(driver.availabilities, function (key2, availability) {
                    trs += `<tr>
                                <td width="10%">
                                    <div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                                        <div class="col-sm-2 n-pick-maid-right pl-0">
                                            <input type="checkbox" value="`+ availability.driver_availability_id + `" name="mapped_driver_ids[]" id="mapped_id_` + availability.driver_availability_id + `">
                                            <label for="mapped_id_` + availability.driver_availability_id + `">
                                            <span class="border-radius-3"></span>
                                            </label>
                                            <p class="text-success"></p>
                                        </div>
                                    </div>
                                </th>
                                <td width="10%">→</th>
                                <td width="20%">`  + availability.zone_name + `</td>
                                <td width="20%">` + availability.area_name + ` </td>
                                <td width="20%">`+ moment(availability.date_from).format("DD-MM-YYYY") + `</td>
                                <td width="20%">`+ moment(availability.date_to).format("DD-MM-YYYY") + `</td>
                                <td width="20%">`+ (availability.date_to == availability.date_from ? 0 : moment(availability.date_to).diff(moment(), 'days') + 1) + `</td>
                                <td width="10%">
                                <a class="n-btn-icon purple-btn" href="javascript:void(0)" title="Edit">
                                <i class="btn-icon-only icon-pencil"  onclick="driver_availabity_modal(`+ availability.driver_availability_id +`)"> </i>
                                </a>
                                <a class="n-btn-icon red-btn delete-icon" href="javascript:void(0)" title="Delete">
                                <i class="btn-icon-only icon-trash" onclick="confirm_delete_driver_availability(` + availability.driver_availability_id + `)"></i>
                                </a>
                               </td>
                            </tr>`;
                });
                if (trs == "") {
                    trs += `<tr>
                                <td colspan="5" class="text-center text-danger">No area mapped for <b>`+ driver.driver.driver_name + `</b> on ` + moment($('#service_date').val(), 'DD/MM/YYYY').format('DD-MM-YYYY') + `.</th>
                            </tr>`;
                }
                table += `<table class="table table-bordered table-hover" width="100%">
                            <thead>
                            <tr style="background-color: #758b93 !important;">
                            <th colspan="8"><img class="img-thumbnail" style="width:30px" src="`+ _base_url + `uploads/images/default/maid-avatar.png"/>&nbsp;&nbsp;&nbsp;` + driver.driver.driver_name + `</th>
                            </tr>
                            </thead>
                            `+ trs + `
                        </table>`;
                $('#mapped-drivers-holder').append(table);
            });
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
}
$(document).on('change', 'input[name="mapped_driver_ids[]"]', function () {
    // if ($('input[name="driver_ids[]"]:checked').length == $('input[name="driver_ids[]"]').not(":disabled").length) {
    //     $("#bookings-for-dispatch #check_all").prop("checked", true);
    // }
    // else {
    //     $("#bookings-for-dispatch #check_all").prop("checked", false);
    // }
    if ($('input[name="mapped_driver_ids[]"]:checked').length > 0) {
        $('button[data-action="mapped-delete-now"]').show();
    }
    else {
        $('button[data-action="mapped-delete-now"]').hide();
    }
});

$('#tab1 #check_all1').change(function () {
    // alert("hiii");
    
    if (this.checked) {
        $('input[name="mapped_driver_ids[]"]').not(":disabled").prop("checked", true);
        if ($('input[name="mapped_driver_ids[]"]:checked').length == 0) {
            toast('warning', "No area mapped driver.");
            $('#tab1 #check_all1').prop("checked", false);
        }
    }
    else {
        $('input[name="mapped_driver_ids[]"]').prop('checked', false);
    }
    $('input[name="mapped_driver_ids[]"]').trigger("change");
});
$('button[data-action="mapped-delete-now"]').click(function (event) {
    var driver_availability_ids = $('input[name="mapped_driver_ids[]"]:checked').map(function() {
        return this.value;
    }).get();
    // Check if any driver IDs are selected
    if (driver_availability_ids.length === 0) {
        toast('error', 'Please select at least one driver.');
        return;
    }
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "driver/delete_multiple_driver_availability",
        data: { driver_availability_ids: driver_availability_ids },
        success: function (data) {
            toast('success', data.message);
            close_fancybox();
            clickCurrentTab();
            $('button[data-action="mapped-delete-now"]').hide();
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
function renderMappedDriverDataAll() {
    $('#all-mapped-drivers-holder').html(`<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>`)
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "laravel/driver/mapping_data_all_under_driver",
        data: { from_date: moment($('#service_date1').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            to_date: moment($('#service_date2').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            zone_id: !isNaN($('select[name="filter_zone_id"]').val()) ? $('select[name="filter_zone_id"]').val() : undefined,
            driver_id: !isNaN($('select[name="filter_driver_id"]').val()) ? $('select[name="filter_driver_id"]').val() : undefined},
        success: function (data) {
            var data = data.data;
            if (data.length == 0) {
                toast('warning', "No areas found, add areas to continue");
                return false;
            }
            $('#all-mapped-drivers-holder').html(``);
            $.each(data, function (key, driver) {
                var table = ``;
                var trs = ``;
                $.each(driver.availabilities, function (key2, availability) {
                    trs += `<tr>
                                <td width="10%">
                                    <div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                                        <div class="col-sm-2 n-pick-maid-right pl-0">
                                            <input type="checkbox" value="`+ availability.driver_availability_id + `" name="driver_ids[]" id="driver_id_` + availability.driver_availability_id + `">
                                            <label for="driver_id_` + availability.driver_availability_id + `">
                                            <span class="border-radius-3"></span>
                                            </label>
                                            <p class="text-success"></p>
                                        </div>
                                    </div>
                                </th>
                                <td width="10%">→</th>
                                <td width="20%">` + availability.zone_name + `</td>
                                <td width="20%">` + availability.area_name + ` </td>
                                <td width="20%">`+ moment(availability.date_from).format("DD-MM-YYYY") + `</td>
                                <td width="20%">`+ moment(availability.date_to).format("DD-MM-YYYY") + `</td>
                                <td width="20%">`+ (availability.date_to == availability.date_from ? 0 : moment(availability.date_to).diff(moment(), 'days') + 1) + `</td>
                               
                                <td width="10%">
                                <a class="n-btn-icon purple-btn" href="javascript:void(0)" title="Edit">
                                <i class="btn-icon-only icon-pencil"  onclick="driver_availabity_modal(`+ availability.driver_availability_id +`)"> </i>
                                </a>
                                <a class="n-btn-icon red-btn delete-icon" href="javascript:void(0)" title="Delete">
                                <i class="btn-icon-only icon-trash" onclick="confirm_delete_driver_availability(` + availability.driver_availability_id + `)"></i>
                                </a>
                               </td>
                            </tr>`;
                });
                if (trs == "") {
                    trs += `<tr>
                                <td colspan="5" class="text-center text-danger">No area mapped for <b>`+ driver.driver.driver_name + `</b>.</th>
                            </tr>`;
                }
                table += `<table class="table table-bordered table-hover" width="100%">
                            <thead>
                            <tr style="background-color: #758b93 !important;">
                            <th colspan="8"><img class="img-thumbnail" style="width:30px" src="`+ _base_url + `uploads/images/default/maid-avatar.png"/>&nbsp;&nbsp;&nbsp;` + driver.driver.driver_name + `</th>
                            </tr>
                            </thead>
                            `+ trs + `
                        </table>`;
                $('#all-mapped-drivers-holder').append(table);
            });
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
}
$(document).on('change', 'input[name="driver_ids[]"]', function () {
    // if ($('input[name="driver_ids[]"]:checked').length == $('input[name="driver_ids[]"]').not(":disabled").length) {
    //     $("#bookings-for-dispatch #check_all").prop("checked", true);
    // }
    // else {
    //     $("#bookings-for-dispatch #check_all").prop("checked", false);
    // }
    if ($('input[name="driver_ids[]"]:checked').length > 0) {
        $('button[data-action="delete-now"]').show();
    }
    else {
        $('button[data-action="delete-now"]').hide();
    }
});
$('#tab4 #check_all2').change(function () {
    // alert("hiii");
    if (this.checked) {
        $('input[name="driver_ids[]"]').not(":disabled").prop("checked", true);
        if ($('input[name="driver_ids[]"]:checked').length == 0) {
            toast('warning', "No area mapped driver.");
            $('#tab4 #check_all2').prop("checked", false);
        }
    }
    else {
        $('input[name="driver_ids[]"]').prop('checked', false);
    }
    $('input[name="driver_ids[]"]').trigger("change");
});
$('button[data-action="delete-now"]').click(function (event) {
    var driver_availability_ids = $('input[name="driver_ids[]"]:checked').map(function() {
        return this.value;
    }).get();
    // Check if any driver IDs are selected
    if (driver_availability_ids.length === 0) {
        toast('error', 'Please select at least one driver.');
        return;
    }
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "driver/delete_multiple_driver_availability",
        data: { driver_availability_ids: driver_availability_ids },
        success: function (data) {
            toast('success', data.message);
            close_fancybox();
            clickCurrentTab();
            $('button[data-action="delete-now"]').hide();
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
function renderUnconfirmedDriverData() {
    var loading_html = `<i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>`;
    $('#schedules-with-unconfirmed-drivers tbody').html(`tr><td colspan="9" class="text-center">` + loading_html + `</tr>`)
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/day_services_driver_unconfirmed",
        // url: _base_url + "driver_schedule/day_services_driver_unconfirmed",
        data: {
            service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            driver_id: !isNaN($('select[name="filter_driver_id"]').val()) ? $('select[name="filter_driver_id"]').val() : undefined,
            zone_id: !isNaN($('select[name="filter_zone_id"]').val()) ? $('select[name="filter_zone_id"]').val() : undefined,
            maid_id: !isNaN($('select[name="filter_maid_id"]').val()) ? $('select[name="filter_maid_id"]').val() : undefined
        },
        success: function (data) {
            var data = data.data;
            if (data.length == 0) {
                $('#schedules-with-unconfirmed-drivers tbody').html(`<tr><td colspan="9" class="text-center text-danger">No schedules found.</td></tr>`)
                return false;
            }
            var trs = ``;
            $.each(data, function (key, schedule) {
                trs += `<tr data-id="` + schedule.day_service_id + `" data-index-id ="`+(key + 1)+`">
                        <input type="hidden" name="maid_id" value="`+schedule.maid_id+`">
                        <input type="hidden" name="customer_id" value="`+schedule.customer_id+`">
                        <input type="hidden" name="unconfirmed_schedule_id" value="`+(schedule.unconfirmed_schedule_id ? schedule.unconfirmed_schedule_id : `""`)+`">
                            <!-- Only show the checkbox if schedule.deleted_at is null -->
                            ${schedule.deleted_at == null ? `
                                <td width="10%" class="editable">
                                    
                                    <div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                                        <div class="col-sm-2 n-pick-maid-right pl-0">
                                            <input type="checkbox" value="`+ schedule.day_service_id + `" name="schedule_ids[]" id="schedule_id_` + schedule.day_service_id +`_`+ (key + 1) + `">
                                            <label for="schedule_id_`+ schedule.day_service_id +`_`+ (key + 1) +`">
                                            <span class="border-radius-3"></span>
                                            </label>
                                            <p class="text-success"></p>
                                        </div>
                                    </div>
                                </th>
                             ` : ' <td width="10%" class="editable"> </th>'} 
                            <td class="editable non-editable" data-field="index" data-id="` + schedule.day_service_id + `">` + (key + 1) + `</th>
                            <td class="editable non-editable" data-field="day_service_id" data-id="` + schedule.day_service_id + `">` + schedule.day_service_id + `</th>
                            <td class="editable non-editable" data-field="customer_name" data-id="` + schedule.day_service_id + `">` + schedule.customer_name + `</td>
                            <td class="editable" data-field="maid_name" data-id="` + schedule.day_service_id + `">` + schedule.maid_name + `</td>
                            <td class="editable" data-field="pick_up_time" data-id="` + schedule.day_service_id + `"> `+(schedule.pick_time ? schedule.pick_time :`06:45 AM`)+`</td>
                            <td class="editable" data-field="pick_address" data-id="` + schedule.day_service_id + `">`+(schedule.pick_address ? schedule.pick_address : `-`)+`</td>
                            <td class="editable" data-field="drop_time" data-id="` + schedule.day_service_id + `"> `+ (schedule.drop_time ?  moment(schedule.drop_time, 'hh:mm').format('hh:mm A') : moment(schedule.time_from, 'hh:mm').format('hh:mm A')) +` </td>
                            <td class="editable" data-field="customer_address" data-id="` + schedule.day_service_id + `">`+ (schedule.drop_address ? schedule.drop_address : schedule.customer_address ) +`</td>
                            <td class="editable  non-editable" data-field="zone_name" data-id="` + schedule.day_service_id + `">` + schedule.zone_name + `</td>
                            <td class="editable  non-editable" data-field="area_name" data-id="` + schedule.day_service_id + `">` + schedule.area_name + `</td>
                            <td class="text-success editable" data-field="driver_name" data-id="` + schedule.day_service_id + `">` + (schedule.driver_name ? schedule.driver_name : '<p class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;No Driver Selected</p>') + `</td>`
                           if (schedule.deleted_at == null) {
                            trs += `
                                    <td class="editable non-editable"  data-field="edit">
                                        <button class="edit-btn">Edit</button>
                                        <a class="n-btn-icon blue-btn" data-id="` + schedule.day_service_id + `" data-driver="` + (schedule.driver_id ? schedule.driver_id : "") + `" data-mapped-driver="` + (schedule.driver_availability_driver_id ? schedule.driver_availability_driver_id : "") + `" data-action="change-driver" title="Change Driver"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <button class="copy-btn" data-id="` + schedule.day_service_id + `">Copy</button>
                                    </td>`;
                            }else {
                                trs += `<td class="editable non-editable text-danger"  data-field="edit">
                                <a class="n-btn-icon blue-btn" data-undo-id="` + schedule.day_service_id + `"  data-action="undo-unconfirm-driver" title="Rollback"><i class="fa fa-undo" aria-hidden="true"></i></a>Deleted</td>`;
                            }
                            
                            trs += `</tr>`;
                // $('#schedules-with-unconfirmed-drivers tbody').html(trs);
            });
            $('#schedules-with-unconfirmed-drivers tbody').html(trs);
            $('#schedules-with-unconfirmed-drivers tbody tr').each(function (index) {
                $(this).find('td[data-field="index"]').text(index + 1); // Update the serial number
            });
            // Handle the click event for the "Copy" button
            // $('.copy-btn').click(function () {
            //     var row = $(this).closest('tr'); // Get the closest row
            //     var newRow = row.clone(); // Duplicate the row
                
            //     // Reset the values for the new row if needed (for example, reset ID, driver, etc.)
            //     newRow.find('input[type="checkbox"]').prop('checked', false); // Uncheck the checkbox
            //     // newRow.find('td[data-field="driver_name"]').html('<p class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;No Driver Selected</p>');
                
            //     // Append the new row to the table
            //     $('#schedules-with-unconfirmed-drivers tbody').append(newRow);
            // });
            $('.copy-btn').click(function () {
                var row = $(this).closest('tr'); // Get the closest row (the row where the button was clicked)
                var newRow = row.clone(); // Clone the row
                
                // Reset the checkbox on the new row (uncheck it)
                newRow.find('input[type="checkbox"]').prop('checked', false);
                newRow.find('input[name="unconfirmed_schedule_id"]').val("");
                
                // Update the checkbox id and label 'for' attribute
                newRow.find('input[type="checkbox"]').each(function (index) {
                    var checkbox = $(this);
            
                    // Get the original scheduleId directly from the data-id attribute of the row
                    var scheduleId = checkbox.closest('tr').data('id'); // Get the correct schedule ID from the row
                    
                    // Generate a new unique id based on the scheduleId and row index
                    var newIndex = $('#schedules-with-unconfirmed-drivers tbody tr').length + 1; // New row index
                    var newId = `schedule_id_${scheduleId}_${newIndex}`; // New unique checkbox id
                    var newValue = `${scheduleId}_${newIndex}`; // New unique checkbox value
            
                    // Update checkbox id and value
                    var label = newRow.find('label[for="' + checkbox.attr('id') + '"]');
            
                    if (label.length > 0) {
                        label.attr('for', newId); // Update the label's 'for' attribute to match the new checkbox id
                        checkbox.attr('id', newId);
                    } else {
                        console.error('Label not found for checkbox ID: ' + checkbox.attr('id'));
                    }
                });
            
                // Insert the new row right after the current row (not at the end)
                row.after(newRow);  // This will insert the copied row directly below the original row
            
                // Recalculate the serial numbers for all rows after the new row is added
                $('#schedules-with-unconfirmed-drivers tbody tr').each(function (index) {
                    // Update the serial number in the "index" column
                    $(this).find('td[data-field="index"]').text(index + 1); // Serial number starts from 1
                });
                newRow.find('input.flatpickr').flatpickr({
                    enableTime: true,
                    noCalendar: true,
                    dateFormat: 'h:i K', // Ensure it uses AM/PM format
                    time_24hr: false,
                });
            });
            
            
            $('[data-action="undo-unconfirm-driver"]').off();
            $('[data-action="undo-unconfirm-driver"]').click(function (event) {
                var unconfirmed_schedule_id = $(this).closest('tr').find('input[name="unconfirmed_schedule_id"]').val();
                $('.mm-loader').show();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: _base_url + "driver/undo_unconfirm_driver",
                    data: { day_service_id: $(this).attr("data-undo-id"), unconfirmed_schedule_id:unconfirmed_schedule_id },
                    success: function (data) {
                        if (data.status == true) {
                            toast('success', data.message);
                            // renderConfirmedDriverData();
                            clickCurrentTab();
                        }
                        else {
                            toast('error', data.message);
                        }
                    },
                    error: function (data) {
                        toast('error', data.statusText);
                    },
                });
            });
            $('[data-action="change-driver"]').off();
            $('[data-action="change-driver"]').click(function (event) {
                var driver_availability_driver_id = $(this).attr("data-mapped-driver");
                var driver_id = $(this).attr("data-driver");
                $('#schedule_driver_change_form select[name="driver_availability_driver_id"]').select2().val(driver_availability_driver_id).trigger("change");
                $('#schedule_driver_change_form select[name="driver_id"]').select2().val(driver_id).trigger("change");
                $('#schedule_driver_change_form input[name="day_service_id"]').val($(this).attr("data-id"));
                schedule_driver_update_validator.resetForm();
                fancybox_show('schedule-driver-change-popup', { width: 380 });
            });
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
}

$('#tab2 #check_all4').change(function () {
    // alert("hiii");
    if (this.checked) {
        $('input[name="schedule_ids[]"]').not(":disabled").prop("checked", true);
        if ($('input[name="schedule_ids[]"]:checked').length == 0) {
            toast('warning', "No area mapped driver.");
            $('#tab2 #check_all4').prop("checked", false);
        }
    }
    else {
        $('input[name="schedule_ids[]"]').prop('checked', false);
    }
    $('input[name="schedule_ids[]"]').trigger("change");
});
$('button[data-action="confirm-mapping"]').click(function (event) {
    var $confirmButton = $(this);
    // $confirmButton.prop('disabled', true);
    var schedulesData = [];

    // Loop through each row and collect the data
    // $('#schedules-with-unconfirmed-drivers tbody tr').each(function() {
        $('input[name="schedule_ids[]"]:checked').each(function() {
            var row = $(this).closest('tr');
            var driver_id = row.find('a[data-action="change-driver"]').data('driver');
            if (!driver_id) {
                toast('warning', "please select driver.");
                return;
            }
            var scheduleData = {
            // schedule_id: $(this).data('id'),
            // customer_name: $(this).find('td[data-field="customer_name"]').text(),
            // maid_name: $(this).find('td[data-field="maid_name"]').text(),
            // pick_up_time: $(this).find('td[data-field="pick_up_time"]').text(),
            // pick_address: $(this).find('td[data-field="pick_address"]').text(),
            // drop_time: $(this).find('td[data-field="drop_time"]').text(),
            // drop_address: $(this).find('td[data-field="drop_address"]').text(),
            // zone_name: $(this).find('td[data-field="zone_name"]').text(),
            // area_name: $(this).find('td[data-field="area_name"]').text(),
            // driver_name: $(this).find('td[data-field="driver_name"]').text(),
            // maid_id: $(this).find('input[name="maid_id"]').val(),
            // customer_id: $(this).find('input[name="customer_id"]').val(),
            // driver_id: $(this).find('a[data-action="change-driver"]').data('driver')
            schedule_id: row.data('id'),
            customer_name: row.find('td[data-field="customer_name"]').text(),
            maid_name: row.find('td[data-field="maid_name"]').text(),
            // pick_up_time: row.find('td[data-field="pick_up_time"]').text(),
            pick_up_time: row.find('td[data-field="pick_up_time"] input').length > 0 
                ? row.find('td[data-field="pick_up_time"] input').val() 
                : row.find('td[data-field="pick_up_time"]').text(),
            drop_time: row.find('td[data-field="drop_time"] input').length > 0 
                ? row.find('td[data-field="drop_time"] input').val()  
                : row.find('td[data-field="drop_time"]').text(),
            // pick_address: row.find('td[data-field="pick_address"]').text(),
            pick_address: row.find('td[data-field="pick_address"] textarea').length > 0 
            ? row.find('td[data-field="pick_address"] textarea').val()
            : row.find('td[data-field="pick_address"]').text(),
            drop_address: row.find('td[data-field="customer_address"] textarea').length > 0 
            ? row.find('td[data-field="customer_address"] textarea').val()
            : row.find('td[data-field="customer_address"]').text(),
            // drop_time: row.find('td[data-field="drop_time"]').text(),
            // drop_address: row.find('td[data-field="customer_address"]').text(),
            zone_name: row.find('td[data-field="zone_name"]').text(),
            area_name: row.find('td[data-field="area_name"]').text(),
            driver_name: row.find('td[data-field="driver_name"]').text(),
            maid_id: row.find('input[name="maid_id"]').val(),
            customer_id: row.find('input[name="customer_id"]').val(),
            unconfirmed_schedule_id: row.find('input[name="unconfirmed_schedule_id"]').val(),
            driver_id: driver_id,
            is_selected: true 
        };

        schedulesData.push(scheduleData);
    });
// console.log(schedulesData)
    if (schedulesData.length === 0) {
        toast('warning', 'No schedules to confirm.');
        return;
    }

    // Send the collected data to the server via AJAX
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: _base_url + 'driver/confirm_mapping', // PHP script URL to handle confirmation
        data: {
            schedules: schedulesData,
            service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            // driver_id: $('select[name="filter_driver_id"]').val(),
            // zone_id: $('select[name="filter_zone_id"]').val(),
            // maid_id: $('select[name="filter_maid_id"]').val()
        },
        success: function (data) {
            if (data.status == "success") {
                toast('success', data.message);
                // $(this).replaceWith('<button class="edit-btn">Edit</button>');
            
                renderUnconfirmedDriverData();
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        }
    });
});
$('button[data-action="delete-unconfirmed"]').click(function (event) {
    var schedulesData = [];
    $('input[name="schedule_ids[]"]:checked').each(function() {
        var row = $(this).closest('tr');
        var driver_id = row.find('a[data-action="change-driver"]').data('driver');
        // if (!driver_id) {
        //     toast('warning', "please select driver.");
        //     return;
        // }
        var scheduleData = {
            schedule_id: row.data('id'),
            customer_name: row.find('td[data-field="customer_name"]').text(),
            maid_name: row.find('td[data-field="maid_name"]').text(),
            pick_up_time: row.find('td[data-field="pick_up_time"]').text(),
            pick_address: row.find('td[data-field="pick_address"]').text(),
            drop_time: row.find('td[data-field="drop_time"]').text(),
            drop_address: row.find('td[data-field="customer_address"]').text(),
            zone_name: row.find('td[data-field="zone_name"]').text(),
            area_name: row.find('td[data-field="area_name"]').text(),
            driver_name: row.find('td[data-field="driver_name"]').text(),
            maid_id: row.find('input[name="maid_id"]').val(),
            customer_id: row.find('input[name="customer_id"]').val(),
            unconfirmed_schedule_id: row.find('input[name="unconfirmed_schedule_id"]').val(),
            driver_id: driver_id,
            is_selected: true  // Mark this schedule as selected
        };

        schedulesData.push(scheduleData);
        row.remove();
    });
    if (schedulesData.length === 0) {
        toast('warning', 'No schedules to confirm.');
        return;
    }

    // Send the collected data to the server via AJAX
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: _base_url + 'driver/delete_unonfirmed_schedule', // PHP script URL to handle confirmation
        data: {
            schedules: schedulesData,
            service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            // driver_id: $('select[name="filter_driver_id"]').val(),
            // zone_id: $('select[name="filter_zone_id"]').val(),
            // maid_id: $('select[name="filter_maid_id"]').val()
        },
        success: function (data) {
            if (data.status == "success") {
                toast('success', data.message);
                // $(this).replaceWith('<button class="edit-btn">Edit</button>');
            
                renderUnconfirmedDriverData();
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        }
    });
   
});
$('button[data-action="reset-unconfirmed"]').click(function (event) {
    // $('button[data-action="rollback_confirm_driver"]').click(function (event) {
        var day_service_ids = $('input[name="schedule_ids[]"]:checked').map(function() {
            return this.value;
        }).get();
        // Check if any driver IDs are selected
        if (day_service_ids.length === 0) {
            toast('error', 'Please select at least one driver.');
            return;
        }
        // var confirmed_schedule_ids = [];
        var schedulesData = [];
        // Loop through each selected checkbox and get the confirmed_schedule_id from the closest row
        $('input[name="schedule_ids[]"]:checked').each(function() {
            // var confirmed_schedule_id = $(this).closest('tr').find('input[name="confirmed_schedule_id"]').val();
             var row = $(this).closest('tr');
              var scheduleData = {
                unconfirmed_schedule_id: row.find('input[name="unconfirmed_schedule_id"]').val(),
            };
    
            schedulesData.push(scheduleData);
            // confirmed_schedule_ids.push(confirmed_schedule_id);
        });
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "driver/reset_unconfirmed_schedule",
            data: { day_service_ids: day_service_ids,schedulesData: schedulesData },
            success: function (data) {
                if (data.status == true) {
                    toast('success', data.message);
                    renderUnconfirmedDriverData();
                    // $('button[data-action="reset-unconfirmed"]').hide();
    
                }
                else {
                    toast('error', data.message);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
        });
    // });
   
});
$('#schedules-with-unconfirmed-drivers tbody').on('click', '.edit-btn', function() {
    var $row = $(this).closest('tr');
    var $cells = $row.find('.editable');
    
    $cells.each(function() {
        var $cell = $(this);
        var field = $cell.data('field');
        var id = $cell.data('id');
        var originalValue = $cell.text().trim();

        if (field === 'index' || field === 'customer_name' || field === 'day_service_id' || field === 'maid_name' || field === 'zone_name' || field === 'area_name' || field === 'edit') {
            return; 
        }
        // Only make editable if it's not already an input or select
        if (!$cell.find('input').length && !$cell.find('select').length) {
            
            if (field === 'pick_up_time') {
                var input = $('<input>', {
                    type: 'text',
                    value: originalValue === ' - ' ? '' : moment(originalValue, 'hh:mm A').format('hh:mm A'), // Ensures 12-hour format
                    class: 'editable-input flatpickr',
                    'data-field': field,
                    'data-id': id
                });

                $cell.empty().append(input);

                input.flatpickr({
                    enableTime: true, 
                    noCalendar: true, 
                    dateFormat: 'h:i K', // Ensure it uses AM/PM format
                    defaultDate: originalValue === ' - ' ? '' : moment(originalValue, 'hh:mm A').format('hh:mm A'),
                    time_24hr: false, 
                });

            } else if (field === 'drop_time') {
                var input = $('<input>', {
                    type: 'text',
                    value: originalValue === ' - ' ? '' : moment(originalValue, 'hh:mm A').format('hh:mm A'), // Ensures 12-hour format
                    class: 'editable-input flatpickr',
                    'data-field': field,
                    'data-id': id
                });

                $cell.empty().append(input);

                input.flatpickr({
                    enableTime: true, 
                    noCalendar: true, 
                    dateFormat: 'h:i K', // Ensure it uses AM/PM format
                    defaultDate: originalValue === ' - ' ? '' : moment(originalValue, 'hh:mm A').format('hh:mm A'),
                    time_24hr: false, // Use 24-hour format (optional)
                });
            } else if (field === 'pick_address') {
                var textarea = $('<textarea>', {
                    class: 'editable-textarea',
                    'data-field': field,
                    'data-id': id,
                    text: originalValue === ' - ' ? '' : originalValue
                });

                $cell.empty().append(textarea);
            } else if (field === 'customer_address') {
                var textarea = $('<textarea>', {
                    class: 'editable-textarea',
                    'data-field': field,
                    'data-id': id,
                    text: originalValue === ' - ' ? '' : originalValue
                });

                $cell.empty().append(textarea);
            } 

            else if (field === 'driver_name') {
                var select = $('<select>', {
                    class: 'editable-select',
                    'data-field': field,
                    'data-id': id
                });

                // Add default option
                select.append('<option value="">-- Select driver --</option>');

    
                drivers.forEach(function(driver) {
                    var option = $('<option>', {
                        value: driver.driver_id,
                        text: driver.driver_name
                    });

                    // Set the selected option based on the current value
                    if (driver.driver_name === originalValue) {
                        option.attr('selected', 'selected');
                    }

                    select.append(option);
                });

                // Replace the cell content with the select dropdown
                $cell.empty().append(select);
            }

               
            else {
                // Replace other fields with a text input
                var inputValue = originalValue === ' - ' ? '' : originalValue;
                var input = $('<input>', {
                    type: 'text',
                    value: inputValue,
                    class: 'editable-input',
                    'data-field': field,
                    'data-id': id
                });

                // Replace the cell content with the input field
                $cell.empty().append(input);
            }
        }
    });

    $(this).replaceWith('<button class="save-btn">Save</button>'); 
});

// Handle "Save" button click
$('#schedules-with-unconfirmed-drivers tbody').on('click', '.save-btn', function() {
    var $row = $(this).closest('tr');
    var $cells = $row.find('.editable');
    var updatedValues = {};
    var unconfirmed_schedule_id = $row.find('input[name="unconfirmed_schedule_id"]').val(); 
    // Collect the updated values from each editable cell
    $cells.each(function() {
        var $cell = $(this);
        var $input = $cell.find('input');
        var $select = $cell.find('select');
        var $textarea = $cell.find('textarea');
        var newValue;
        var field = $cell.data('field'); 
        var day_service_id = $cell.data('id'); 
        if ($input.length) {
            newValue = $input.val();
        } else if ($select.length) {
            newValue = $select.val();
        } else if ($textarea.length) {
            newValue = $textarea.val();
        }

        // Ensure the updatedValues object is structured by the `id`
        if (day_service_id !== undefined) {
            if ($input.length) {
                newValue = $input.val();
            } else if ($select.length) {
                newValue = $select.val();
            } else if ($textarea.length) {
                newValue = $textarea.val();
            }

            // Add the updated value under the correct day_service_id
            if (!updatedValues[day_service_id]) {
                updatedValues[day_service_id] = {};
            }

            updatedValues[day_service_id][field] = newValue;
            // updatedValues[day_service_id]['unconfirmed_schedule_id'] = unconfirmed_schedule_id;
            // if (unconfirmed_schedule_id && unconfirmed_schedule_id !== '') {
                // updatedValues.unconfirmed_schedule_id = unconfirmed_schedule_id;
                updatedValues[day_service_id]['unconfirmed_schedule_id'] = unconfirmed_schedule_id;
            // } 
          
        }

    });
 
    // updatedValues.unconfirmed_schedule_id = unconfirmed_schedule_id;
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: _base_url + "driver/insert_unconfirmed_schedule_driver",
        data: JSON.stringify(updatedValues), 
        success: function (data) {
            if (data.status == "success") {
                toast('success', data.message);
                // $(this).replaceWith('<button class="edit-btn">Edit</button>');
                // $.each(updatedValues, function(day_service_id, fields) {
                //     var $row = $('#schedules-with-unconfirmed-drivers tbody tr[data-id="' + day_service_id + '"]');
                //     var $cells = $row.find('.editable');
                var unconfirmed_schedule_id = data.unconfirmed_schedule_id
                   var driver = data.driver_name[0].driver_name;
                   var driver_id = data.driver_name[0].driver_id;
                //     $cells.each(function() {
                //         var $cell = $(this);
                //         var field = $cell.data('field');
                //         if (fields[field] !== undefined) {
                //             var newValue = fields[field];
                //             if (field === 'pick_up_time' || field === 'drop_time') {
                //                 $cell.text(moment(newValue, 'hh:mm A').format('hh:mm A'));
                //             } else if (field === 'pick_address' || field === 'customer_address') {
                //                 $cell.text(newValue);
                //             } else if (field === 'driver_name') {
                //                 var driverName = driver || '<p class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;No Driver Selected</p>';
                //                 $cell.html(driverName);
                                $row.find('a[data-action="change-driver"]').data('driver', driver_id);
                                $row.find('input[name="unconfirmed_schedule_id"]').val(unconfirmed_schedule_id);
                //             } else {
                //                 $cell.text(newValue);
                //             }
                //         }
                //     });
                    $row.find('.save-btn').replaceWith('<button class="edit-btn">Edit</button>');
                // });
                // renderUnconfirmedDriverData();
            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        }
    });
});

/************************************************************************** */
function renderConfirmedDriverData() {
    var loading_html = `<i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>`;
    $('#schedules-with-confirmed-drivers tbody').html(`tr><td colspan="9" class="text-center">` + loading_html + `</tr>`)
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/day_services_driver_confirmed",
        data: {
            service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            driver_id: !isNaN($('select[name="filter_driver_id"]').val()) ? $('select[name="filter_driver_id"]').val() : undefined,
            zone_id: !isNaN($('select[name="filter_zone_id"]').val()) ? $('select[name="filter_zone_id"]').val() : undefined,
            maid_id: !isNaN($('select[name="filter_maid_id"]').val()) ? $('select[name="filter_maid_id"]').val() : undefined
        },
        success: function (data) {
            var data = data.data;
            if (data.length == 0) {
                $('#schedules-with-confirmed-drivers tbody').html(`<tr><td colspan="9" class="text-center text-danger">No confirmed driver dispatches found.</td></tr>`)
                return false;
            }
            var trs = ``;
            $.each(data, function (key, schedule) {
                trs += `<tr>
                            <input type="hidden" name="confirmed_schedule_id" value="`+schedule.confirmed_schedule_id+`">
                            <input type="hidden" name="unconfirmed_schedule_id" value="`+schedule.unconfirmed_schedule_id+`">
                            <td width="10%" class="editable">
                                <div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                                    <div class="col-sm-2 n-pick-maid-right pl-0">
                                        <input type="checkbox" value="`+ schedule.day_service_id + `" name="schedule_confirmed_ids[]" id="schedule_confirmed_id_` + schedule.day_service_id +`_`+schedule.confirmed_schedule_id + `">
                                        <label for="schedule_confirmed_id_` + schedule.day_service_id +`_`+schedule.confirmed_schedule_id + `">
                                        <span class="border-radius-3"></span>
                                        </label>
                                        <p class="text-success"></p>
                                    </div>
                                </div>
                            </th>
                            <td>` + (key + 1) + `</th>`
                            // <td>` + schedule.day_service_id + `</th>
                            +`<td class="text-success">` + (schedule.driver_name ? schedule.driver_name : '<p class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;No Driver Selected</p>') + `</td>`
                            // <td>` + schedule.customer_name + `</td>
                            +`<td>` + schedule.maid_name + `</td>
                             <td> `+(schedule.pick_time ? moment(schedule.pick_time, 'hh:mm A').format('hh:mm A') :`-`)+`</td>
                            <td>`+(schedule.pick_address ? schedule.pick_address :`-`)+`</td>
                            <td> `+ (schedule.drop_time ? moment(schedule.drop_time, 'hh:mm A').format('hh:mm A') :`-`) +` </td>
                            <td>`+ (schedule.drop_address ? schedule.drop_address : `-` ) +`</td>`
                            // <td>` + schedule.zone_name + `</td>
                            // <td>` + schedule.area_name + `</td>
                            +`<td>
                                <a class="n-btn-icon blue-btn" data-id="` + schedule.day_service_id + `"  data-action="undo-confirm-driver" title="Rollback"><i class="fa fa-undo" aria-hidden="true"></i></a>
                            </td>
                        </tr>`;
                $('#schedules-with-confirmed-drivers tbody').html(trs);
            });
            $('[data-action="undo-confirm-driver"]').off();
            $('[data-action="undo-confirm-driver"]').click(function (event) {
                var confirmed_schedule_id = $(this).closest('tr').find('input[name="confirmed_schedule_id"]').val();
                var unconfirmed_schedule_id = $(this).closest('tr').find('input[name="unconfirmed_schedule_id"]').val();
                $('.mm-loader').show();
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: _base_url + "driver/undo_confirm_driver",
                    data: { day_service_id: $(this).attr("data-id"),confirmed_schedule_id:confirmed_schedule_id, unconfirmed_schedule_id:unconfirmed_schedule_id },
                    success: function (data) {
                        if (data.status == true) {
                            toast('success', data.message);
                            renderConfirmedDriverData();
                        }
                        else {
                            toast('error', data.message);
                        }
                    },
                    error: function (data) {
                        toast('error', data.statusText);
                    },
                });
            });
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
}

$(document).on('change', 'input[name="schedule_confirmed_ids[]"]', function () {
    // if ($('input[name="driver_ids[]"]:checked').length == $('input[name="driver_ids[]"]').not(":disabled").length) {
    //     $("#bookings-for-dispatch #check_all").prop("checked", true);
    // }
    // else {
    //     $("#bookings-for-dispatch #check_all").prop("checked", false);
    // }
    if ($('input[name="schedule_confirmed_ids[]"]:checked').length > 0) {
        $('button[data-action="rollback_confirm_driver"]').show();
    }
    else {
        $('button[data-action="rollback_confirm_driver"]').hide();
    }
});
$('#tab3 #check_all3').change(function () {
    // alert("hiii");
    if (this.checked) {
        $('input[name="schedule_confirmed_ids[]"]').not(":disabled").prop("checked", true);
        if ($('input[name="schedule_confirmed_ids[]"]:checked').length == 0) {
            toast('warning', "No area mapped driver.");
            $('#tab3 #check_all3').prop("checked", false);
        }
    }
    else {
        $('input[name="schedule_confirmed_ids[]"]').prop('checked', false);
    }
    $('input[name="schedule_confirmed_ids[]"]').trigger("change");
});
$('button[data-action="rollback_confirm_driver"]').click(function (event) {
    var day_service_ids = $('input[name="schedule_confirmed_ids[]"]:checked').map(function() {
        return this.value;
    }).get();
    // Check if any driver IDs are selected
    if (day_service_ids.length === 0) {
        toast('error', 'Please select at least one driver.');
        return;
    }
    var confirmed_schedule_ids = [];
    var schedulesData = [];
    // Loop through each selected checkbox and get the confirmed_schedule_id from the closest row
    $('input[name="schedule_confirmed_ids[]"]:checked').each(function() {
        var confirmed_schedule_id = $(this).closest('tr').find('input[name="confirmed_schedule_id"]').val();
         var row = $(this).closest('tr');
          var scheduleData = {
            unconfirmed_schedule_id: row.find('input[name="unconfirmed_schedule_id"]').val(),
        };

        schedulesData.push(scheduleData);
        confirmed_schedule_ids.push(confirmed_schedule_id);
    });
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "driver/undo_confirm_driver_multiple",
        data: { day_service_ids: day_service_ids, confirmed_schedule_ids:confirmed_schedule_ids,schedulesData:schedulesData },
        success: function (data) {
            if (data.status == true) {
                toast('success', data.message);
                renderConfirmedDriverData();
                $('button[data-action="rollback_confirm_driver"]').hide();

            }
            else {
                toast('error', data.message);
            }
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
});
/************************************************************************** */
$().ready(function () {
    $('li [data-tab="tab2"]').trigger('click');
    schedule_driver_update_validator = $('#schedule_driver_change_form').validate({
        ignore: [],
        rules: {
            driver_id: {
                required: true,
            },
        },
        messages: {
            driver_id: "Select driver.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "driver_id") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#schedule_driver_change_form button[type="submit"]').html('Updating...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "driver/update_schedule_driver",
                data: $('#schedule_driver_change_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        close_fancybox();
                        renderUnconfirmedDriverData();
                    }
                    else {
                        toast('error', data.message);
                    }
                    $('#schedule_driver_change_form button[type="submit"]').html('Update').attr("disabled", false);
                },
                error: function (data) {
                    $('#schedule_driver_change_form button[type="submit"]').html('Update').attr("disabled", false);
                    toast('error', data.statusText);
                },
            });
        }
    });
    confirm_driver_mapping_popup_form = $('#confirm_driver_mapping_popup_form').validate({
        ignore: [],
        rules: {
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#confirm_driver_mapping_popup_form button[type="submit"]').html('Confirming...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "driver/confirm_drivers_for_schedule",
                data: $('#confirm_driver_mapping_popup_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        toast('success', data.message);
                        close_fancybox();
                        renderUnconfirmedDriverData();
                    }
                    else {
                        toast('error', data.message);
                    }
                    $('#confirm_driver_mapping_popup_form button[type="submit"]').html('Confirm').attr("disabled", false);
                },
                error: function (data) {
                    $('#confirm_driver_mapping_popup_form button[type="submit"]').html('Confirm').attr("disabled", false);
                    toast('error', data.statusText);
                },
            });
        }
    });
    $('#confirm_driver_mapping_popup_form #check_all').change(function () {
        if (this.checked) {
            $('#confirm_driver_mapping_popup_form input[name="day_service_ids[]"]').not(":disabled").prop("checked", true);
            if ($('#confirm_driver_mapping_popup_form input[name="day_service_ids[]"]:checked').length == 0) {
                toast('warning', "No dispatches available to confirm.");
                $('#confirm_driver_mapping_popup_form #check_all').prop("checked", false);
            }
        }
        else {
            $('#confirm_driver_mapping_popup_form input[name="day_service_ids[]"]').prop('checked', false);
        }
        $('#confirm_driver_mapping_popup_form input[name="day_service_ids[]"]').trigger("change");
    });
});
$(document).on('change', '#confirm_driver_mapping_popup_form input[name="day_service_ids[]"]', function () {
    if ($('input[name="day_service_ids[]"]:checked').length == $('input[name="day_service_ids[]"]').not(":disabled").length) {
        $("#confirm_driver_mapping_popup_form #check_all").prop("checked", true);
    }
    else {
        $("#confirm_driver_mapping_popup_form #check_all").prop("checked", false);
    }
    if ($('#confirm_driver_mapping_popup_form input[name="day_service_ids[]"]:checked').length > 0) {
        $('#confirm_driver_mapping_popup_form button[type="submit"]').show();
    }
    else {
        $('#confirm_driver_mapping_popup_form button[type="submit"]').hide();
    }
})
/************************************************************************** */
$('#service_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    //startDate: "today"
}).on('changeDate', function (e) {
    clickCurrentTab();
});
$('#service_date1').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    //startDate: "today"
}).on('changeDate', function (e) {
    clickCurrentTab();
});
$('#service_date2').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    //startDate: "today"
}).on('changeDate', function (e) {
    clickCurrentTab();
});
function clickCurrentTab() {
    var current_tab = $('.tab-pane.active').attr('id');
    $('li [data-tab="' + current_tab + '"]').trigger('click');
}

function driver_availabity_modal(driver_availability_id) { 
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "driver/edit_driver_availability",
        data: { driver_availability_id: driver_availability_id },
        success: function (driver_data) {
            $.each(driver_data, function (key, driver) {
                $('#edit_driver_avalability_id').val(driver.driver_availability_id)
                $('#edit_driver_id option[value="' + driver.driver_id + '"]').prop('selected', true);
                $('#edit_driver_id').trigger('change');
                $('#edit_zone_id option[value="' + driver.zone_id + '"]').prop('selected', true);
                $('#edit_zone_id').trigger('change');
                // $('#edit_area_id[] option[value="' + driver.area_id + '"]').prop('selected', true);
                // $('#edit_area_id[]').trigger('change');
                var selected_areas = driver.area_id.split(','); 
                $('#edit_area_id').val(selected_areas).trigger('change');
                var date_from = moment(driver.date_from).format("DD/MM/YYYY")
                var date_to = moment(driver.date_to).format("DD/MM/YYYY")
                $('#edit_driver_mapping_form input[name="edit_date_from"]').val(date_from);
                $('#edit_driver_mapping_form input[name="edit_date_to"]').val(date_to);  
               
            });
            edit_driver_mapping_form_validator.resetForm();
            fancybox_show('edit-driver-mapping-popup', { width: 600 });
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
 }

//  Notes
//delete popup is in driver-edit-availability-popup

// delete driver availability popup
    function confirm_delete_driver_availability(driver_availability_id) {
        
        $('#delete_driver_mapping_form input[name="driver_availability_id"]').val(driver_availability_id);

        fancybox_show('delete-driver-mapping-popup', { 
            width: 600 ,height:'50%'
        });
    }


    function delete_driver_availability() {
        var driver_availability_id = $('#delete_driver_mapping_form input[name="driver_availability_id"]').val();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: _base_url + "driver/delete_driver_availability",
            data: { driver_availability_id: driver_availability_id },
            success: function (data) {
                toast('success', data.message);
                close_fancybox();
                clickCurrentTab();
            },
            error: function (data) {
                toast('error', data.statusText);
            },
        });
    }

$("#printButnforschedule").click(function ()
{
    var divContents = $("#divForPrintSchedule").html();
    // var startdate = $("#start-date").val();
    // var enddate = $("#end-date").val();
    //var day = $("#day").val();

    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');  
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Driver scheules</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('<span style="margin-left:125px;">');
    // printWindow.document.write('Date Range &nbsp;&nbsp;&nbsp; : &nbsp;');
    // printWindow.document.write(startdate);
    // printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;&nbsp;&nbsp;'+enddate);
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('</span>');
    //printWindow.document.write('<span style="margin-left:300px;">');
    //printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    //printWindow.document.write(zone);
    //printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

});
function renderConfirmedDriverPrintData() {
    var loading_html = `<i class="fa fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>`;
    $('#divForPrintSchedule tbody').html(`tr><td colspan="9" class="text-center">` + loading_html + `</tr>`)
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "driver/day_services_driver_confirmed",
        data: {
            service_date: moment($('#service_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
            driver_id: !isNaN($('select[name="filter_driver_id"]').val()) ? $('select[name="filter_driver_id"]').val() : undefined,
            zone_id: !isNaN($('select[name="filter_zone_id"]').val()) ? $('select[name="filter_zone_id"]').val() : undefined,
            maid_id: !isNaN($('select[name="filter_maid_id"]').val()) ? $('select[name="filter_maid_id"]').val() : undefined
        },
        success: function (data) {
            var data = data.data;
            if (data.length == 0) {
                $('#divForPrintSchedule tbody').html(`<tr><td colspan="9" class="text-center text-danger">No confirmed driver dispatches found.</td></tr>`)
                return false;
            }
            var trs = ``;
            $.each(data, function (key, schedule) {
                trs += `<tr>
                            <input type="hidden" name="confirmed_schedule_id" value="`+schedule.confirmed_schedule_id+`">
                            <td>` + (key + 1) + `</th>`
                            // <td>` + schedule.day_service_id + `</th>
                            +`<td class="text-success">` + (schedule.driver_name ? schedule.driver_name : '<p class="text-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;No Driver Selected</p>') + `</td>`
                            // <td>` + schedule.customer_name + `</td>
                            +`<td>` + schedule.maid_name + `</td>
                             <td> `+(schedule.pick_time ? moment(schedule.pick_time, 'hh:mm A').format('hh:mm A') :`-`)+`</td>
                            <td>`+(schedule.pick_address ? schedule.pick_address :`-`)+`</td>
                            <td> `+ (schedule.drop_time ? moment(schedule.drop_time, 'hh:mm A').format('hh:mm A') :`-`) +` </td>
                            <td>`+ (schedule.drop_address ? schedule.drop_address : `-` ) +`</td>`
                            // <td>` + schedule.zone_name + `</td>
                            // <td>` + schedule.area_name + `</td>
                            +`
                        </tr>`;
                $('#divForPrintSchedule tbody').html(trs);
            });
        },
        error: function (data) {
            toast('error', data.statusText);
        },
    });
}


