/************************************************************************** */
/*                                                                          */
/*                          Coder : Samnad. S                               */
/*                                                                          */
/************************************************************************** */
var cleaning_material_rate_per_hour = 10;
var service_vat_percentage = 5;
/**
 * Test
 */
var screen_width = parseFloat($('body').innerWidth());
let head_width = parseFloat(document.querySelectorAll('#booking-calender-section .head')[0].offsetWidth);
let head_last_width = parseFloat(document.querySelectorAll('#booking-calender-section .head-last')[0].offsetWidth);
document.getElementById('booking-calender-section').setAttribute("style", "width:" + screen_width + "px");
document.getElementsByClassName('justify-content-between top-summary-section')[0].setAttribute("style", "width:" + screen_width + "px");
let time_line_width = screen_width - (head_width + head_last_width);
document.querySelectorAll('#schedule .time_line')[0].setAttribute("style", "width:" + time_line_width + "px;margin-left:" + head_width + "px");
let box_width = 71; // width of time box
let box_count = document.querySelectorAll('#schedule .time_slider div > div').length; // total number time voxes
let box_total_width = box_width * box_count;
let time_slider_width = (box_width * 24) + head_last_width;
let time_slider_visible_width = screen_width - (head_width + head_last_width);
document.querySelectorAll('#schedule .time_slider div')[0].setAttribute("style", "width:" + time_slider_width + "px");
document.getElementById('schedule').setAttribute("style", "display: block;width:" + screen_width + "px");
document.querySelectorAll('#schedule .time_grid')[0].setAttribute("style", "width:" + (time_line_width) + "px");
window.onresize = function () {
    location.reload();
}
// logic goes here...................
// var screen_width = $('body').innerWidth();
// var scheduler_width = screen_width - 12;
// var time_grid_width = scheduler_width - 195 - 3 + 8;
// $('#tb-slide-right').click(function () {
//     var position = $('.time_line .time_slider').position();
//     var max_left = time_grid_width - 1342 + 3 + 5;
//     var left_pos = position.left - 71;
//     left_pos = left_pos < max_left ? max_left : left_pos;
//     $('.time_line .time_slider').animate({ left: left_pos }, 71);
//     $('.grids').animate({ left: left_pos }, 71);
//     $(".maid_name").animate({
//         'padding-top': 0,
//         'padding-right': 0,
//         'padding-bottom': 0,
//         'padding-left': -left_pos + 71 + 'px',
//     }, 71);
var screen_width = $('body').innerWidth();
var scheduler_width = screen_width - 12;
var time_grid_width = scheduler_width - 195 - 3 + 8;
$('#tb-slide-right').click(function () {
    var position = $('.time_line .time_slider').position();
    //var max_left = time_grid_width - 1768 + 3 + 5;
    var max_left = time_slider_visible_width - box_total_width;
    var left_pos = position.left - 71;
    left_pos = left_pos < max_left ? max_left : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -left_pos + 71 + 'px',
    }, 71);

    // if ($(window).width() <= 1366) {
    //     var max_left = time_grid_width - 1173 + 3 + 5;
    //     //var left_pos = position.left - 71;
    //     left_pos = left_pos < max_left ? max_left : left_pos;
    //     $('.time_line .time_slider').animate({ left: left_pos }, 71);
    //     $('.grids').animate({ left: left_pos }, 71);
        

    // }
    // if ($(window).width() <= 1280) {
    //     var max_left = time_grid_width - 1090 + 3 + 5;
    //     //var left_pos = position.left - 71;
    //     left_pos = left_pos < max_left ? max_left : left_pos;
    //     $('.time_line .time_slider').animate({ left: left_pos }, 71);
    //     $('.grids').animate({ left: left_pos }, 71);
        

    // }


});



$('#tb-slide-left').click(function () {
    var position = $('.time_line .time_slider').position();
    var left_pos = position.left + 71;
    left_pos = left_pos > 0 ? 0 : left_pos;
    $('.time_line .time_slider').animate({ left: left_pos }, 71);
    $('.grids').animate({ left: left_pos }, 71);
    //$('.maid_name').css('padding-left', -position.left + 'px');
    position.left = position.left == 0 ? -71 : position.left;
    $(".maid_name").animate({
        'padding-top': 0,
        'padding-right': 0,
        'padding-bottom': 0,
        'padding-left': -position.left + 'px',
    }, 71);
});
$('.selector').selectable({
    cancel: ".booked",
    //distance: 1,
    start: function (event, ui) {
        $('.ui-selected').removeClass('ui-selected');
    },
    stop: function (event, ui) {
        var maid_id = $(this).attr('data-maid');
        var maid_name = $(this).attr('data-maid-name');
        var week_day = $(this).attr('data-week');
        var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
        var time_from = $('.ui-selected').first().attr('data-time-from');
        var time_to = $('.ui-selected').last().attr('data-time-to');
        $('#od_booking_form input[name="service_start_date"]').val(moment().format('DD/MM/YYYY'));
        $('#od_booking_form input[name="service_end_date"]').val("");
        $('#od_booking_form input[name="service_week_day"]').val(week_day);
        $('#od_booking_form input[name="service_week_name"]').val(week_name);
        $('#od_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
        $('#od_booking_form input[name="maid_id"]').val(maid_id);
        $('#od_booking_form input[name="maid_name"]').val(maid_name);
        $('#od_booking_form select[name="time_from"]').select2().val(time_from).trigger("change.select2");
        $('#od_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(time_from)).select2().val(time_to).trigger("change.select2");
        $('#od_booking_form select[name="customer_id"]').val("").trigger("change.select2");
        $('#od_booking_form select[name="service_type_id"]').select2().val(6).trigger("change.select2");
        $('#od_booking_form input[name="cleaning_materials"]').prop('checked', false);
        $('#od_booking_form input[name="booking_id"]').val(""); // important
        $('#od_booking_form input[name="service_rate_per_hour"]').val("");
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val("");
        $('#od_booking_form .vat_percentage').html(_service_vat_percentage.toFixed(2));
        $('#od_booking_form input[name="service_start_date"]').val($('#date').val());
        $('#od_booking_form input[name="service_end_date"]').val($('#date').val());
        reCalculateForm();
        /*********************************** */
        $('#od-booking-form-popup .title').html('New One Time Schedule');
        $('#od_booking_form select[name="customer_id"]').next().show();
        $('#od_booking_form input[name="customer_name"]').val("").hide();
        $("#od-booking-form-popup .modal-footer.new").show();
        $("#od-booking-form-popup .modal-footer.edit").hide();
        new_booking_form_validator.resetForm();
        fancybox_show('od-booking-form-popup', { width: 750 });
        $('#customer-picked-address').html('');
        $('#customer-address-panel-user').hide()
    }
});
if ($(".booking-position").length > 0) {
    var stickyTop = $('.booking-position').offset().top;
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= stickyTop) {
            $('.scroll-top-fix').addClass('box-fixed-top');
            $('.book-mid-det-lt-box').addClass('book-mid-det-top-padi');
        } else {
            $('.scroll-top-fix').removeClass('box-fixed-top');
            $('.book-mid-det-lt-box').removeClass('book-mid-det-top-padi');
        }
    });
}
function hourDifference(time_from, time_to) {
    var startTime = moment(time_from, 'HH:mm:ss');
    var endTime = moment(time_to, 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    return parseFloat(duration.asHours());
}
function RefreshSomeEventListener() {
    $(".booked_bubble").off();
    $(".booked_bubble").on("click", function () {
        //alert('b');
    });
}
function renderCurrentWeekDay() {
    maidWeeks(false);
    slots(false);
    $('#weeks .week[data-id="' + service_week_day + '"]').show();
    $('.tb-slider .slots[data-week="' + service_week_day + '"]').show();
}
function renderBookings() {
    window.history.pushState({}, "", "day_view?service_date=" + $('#date').val());
    loader(true);
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "schedule_data/day_schedules",
        data: { service_date: $('#date').val() },
        success: function (data) {
            /************************************************************** */
            // show current day only
            renderCurrentWeekDay();
            /************************************************************** */
            //toastr.clear();
            if (data.schedules.length === 0) {
                toast('info', "No schedules found on " + moment($('#date').val()).format('DD-MM-YYYY') + '.');
            }
            // reset cells
            $('.booked_bubble').remove();
            $('.cell').removeClass("booked");
            //
            var total_hour_week = data.total_hour_week;
            var grand_total = data.grand_total;
            var training_hours = data.training_hours;
            var free_hours = data.free_hours;
            var maid_count = data.maid_count;
            var emergency_leave_count = data.emergency_leave_count;
            var vacations_count = data.vacations_count;
            var holidays_count = data.holidays_count;
            var medical_leaves_count = data.medical_leaves_count;
            var total_maid_leave_count = data.total_maid_leave_count;
            var present_maid_count = data.present_maid_count;
            var grand_suspend_total = data.grand_suspend_total;
            var leave_maid_types = data.leave_maid_types;
            // maid leaves count
            var counting_emergency_hours = emergency_leave_count * 8;
            $('#emergency_leave_count').html(emergency_leave_count +' - '+ counting_emergency_hours + `hrs`);
            var counting_vacations_hours = vacations_count * 8;
            $('#vacations').html(vacations_count +' - '+ counting_vacations_hours + `hrs`);
            var counting_holidays_hours = holidays_count * 8;
            $('#holidays').html(holidays_count +' - '+ counting_holidays_hours + `hrs`);
            var counting_medical_leaves_hours = medical_leaves_count * 8;
            $('#medical_leaves').html(medical_leaves_count +' - '+ counting_medical_leaves_hours + `hrs`);
            $('#absents_count').html(total_maid_leave_count);
            $('#presents_count').html(present_maid_count);
            var emergency_leave_maids = data.emergency_leave_maids;
            
            updateTotalHourWeek(data.total_hour_week);
            $('#grand_percentage').html('');
            $('#grand_total').html('');
            $('#gained_hours').html('');
            $('#lost_hours').html('');
            $('#total_hours').html('');
            $('#free_hours').html('');
            $('#training_hours').html('');
            var leave_maid_ids = data.leave_maid_ids;
            // var leave_maid_types = data.leave_maid_types;
        
            // $.each(data.maidData, function(indexMaid, maid) {
            //     $.each(data.week_days, function(indexWeekDay, weekDay) {
            //         var maidId = maid.maid_id;
            //         var weekDayId = weekDay.week_day_id;
            //         var serviceWeekDay = weekDay.week_day_id; // Assuming service_week_day is available
                    
            //         var isOnLeave = false;
            //         var leaveType = '';
                    
            //         // Check if maid is on leave based on retrieved leave_maid_types data
            //         $.each(data.leave_maid_types, function(indexLeaveType, typevalue) {
            //             if (typevalue.maid_id == maidId && data.service_week_day == serviceWeekDay) {
            //                 leaveType = typevalue.maid_type;
            //                 isOnLeave = true;
            //                 return false; // Break out of the loop once leave type is found
            //             }
            //         });
                    
            //         if (isOnLeave) {
            //             // Maid is on leave, render leave type
            //             var leaveClassName = leaveType.replace('_', ''); // Normalize class name
            //             var leaveText = leaveType.replace('_', ' '); // Normalize display text

            //             var leaveHtml = '<div class="leavetypes slot dayoff ' + leaveClassName + '" data-maid="' + maidId + '" data-week="' + weekDayId + '"><div class="leave">' + leaveText + '</div></div>';
            //             $('.tb-slider').html(leaveHtml); // Replace '.your-container' with your actual container selector
            //         } else {
                        
            //         }
            //     });
            // });
           
            $.each(data.schedules, function (key, booking) {
                var cells = booking.working_minutes / 30;
                if (booking.parent_maid_name != null) {
                    var maid_name = `<p class="n_booking_maid_name">` + booking.parent_maid_name +`&nbsp;&nbsp;Temporary till..&nbsp;&nbsp;` + booking.service_start_date + ` - ` + booking.service_actual_end_date + `-`+ `Reason(`+ booking.booking_reassign_reason +`)</p>`;
                    var tooldata = booking.parent_maid_name +`&#13;&#10;`+ `Reason(` + booking.booking_reassign_reason + `)`;
                } else {
                    var tooldata ="";
                    var maid_name ="";
                }
                let maid_list = leave_maid_types.find(maid_list => {
                    // return maid_leave_data.some(leave => leave.maid_id === maidId && leave.leave_date === date);
                    return maid_list.maid_id  == booking.maid_id && data.service_week_day ==  booking.service_week_day 
                });

                var color = maid_list ? 'maid_leave_bubble' : booking.css_class;
                var bubble = `<div class="booked_bubble ` + color + `" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="`+ tooldata +`">
                <p class="n_booking_customer_name"><i class="fa fa-user" aria-hidden="true"></i>` + booking.customer_name + `</p>
                <p class="n_booking_time"><i class="fa fa-clock-o" aria-hidden="true"></i>` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours</p>
                <p class="n_booking_location"><i class="fa fa-map-marker" aria-hidden="true"></i>` + booking.booking_location + ` ~  `+ booking.booking_zone +`</p>
                `+ maid_name +`</div>`;
                // var bubble = `<div class="booked_bubble ` + booking.css_class + `" data-booking-id="` + booking.booking_id + `" style="width:` + cells * 71 + `px;" title="`+ tooldata +`"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;` + booking.customer_name + `
                // <br>
                // <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;` + moment(booking.time_from, 'hh:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'hh:mm A').format('hh:mm A') + ` ~ <b>` + booking.working_minutes / 60 + `</b> Hours
                // <p style="color:#fff;font-weight:bold;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;` + booking.booking_location + `</p>
                // <p style="color:#fff;font-weight:bold;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;` + booking.booking_zone + `</p></div>`;
                
                // $.each(total_hour_week, function (key, tot) {
                //     var working_minutes1 = tot.working_minutes / 60;
                //     var max_time = 8;
                //     var percentage = (working_minutes1 / (max_time / 100));
                //     percentage = percentage.toFixed(2)
                //     percentage =  percentage+`%`;
                //     $('[data-maid-id="'+ tot.maid_id +'"]').html(working_minutes1);
                //     $('[data-maid-id-percentage="' + tot.maid_id +'"]').html(percentage);
                //     $('[data-maid-week-id="'+ tot.maid_id +'"]').html(working_minutes1);
                //     $('[data-maid-id-week-id="' + tot.maid_id +'"]').html(percentage);

                // });

                //grand total percentage
                var total_maid_hours = maid_count * 8;
                var grantTotal = grand_total / 60;
                var grand_percentage = (grantTotal / (total_maid_hours / 100))
                grand_percentage = grand_percentage.toFixed(2)
                grand_percentage =  grand_percentage+`%`;
                
                $('#grand_percentage').html(grand_percentage);
                $('#grand_total').html(grantTotal);

                // Gained total hours 
                $('#gained_hours').html(grantTotal+`hrs`);

                // Grand Suspend total hours
                var suspend_total = grand_suspend_total / 60;
                $('#lost_hours').html(suspend_total+`hrs`);

                //Day schedule total Hrs
                var schedule_total = grantTotal + suspend_total;
                $('#total_hours').html(schedule_total+`hrs`);
                var leaveData = data.emergency_leave_maids;  // Assuming this is the leave data array.
                var leaveStartDate = null;
                var leaveEndDate = null;
                var currentMaidId = null;
                var currentLeaveType = null;
                
                // training_hours 
                var Training_hours = training_hours / 60;
                $('#training_hours').html(Training_hours+`hrs`);

                // Free Hours
                var Free_hours = free_hours / 60;
                $('#free_hours').html(Free_hours+`hrs`);
                
                // Sort the leave data by maid_id and then by leave_date
                leaveData.sort(function(a, b) {
                    var dateA = new Date(a.leave_date);
                    var dateB = new Date(b.leave_date);
                    if (a.maid_id === b.maid_id) {
                        return dateA - dateB;  // Sort by leave_date if maid_id is the same
                    } else {
                        return a.maid_id - b.maid_id;  // Sort by maid_id
                    }
                });
                
                // Loop through all the leave data
                $.each(leaveData, function(index, leave) {
                    var maid_id = leave.maid_id;
                    var leave_type = leave.typeleaves;
                
                    // Assign appropriate leave type label
                    var type_leaves;
                    if (leave_type == "leave") {
                        type_leaves = "Leave";
                    } else if (leave_type == "emergency_leave") {
                        type_leaves = "Emergency Leaves";
                    } else if (leave_type == "holidays") {
                        type_leaves = "Holidays";
                    } else if (leave_type == "vacations") {
                        type_leaves = "Vacations";
                    } else if (leave_type == "medical_leaves") {
                        type_leaves = "Medical Leaves";
                    }
                
                    // Check if we are still processing the same maid and the same leave type
                    if (currentMaidId !== maid_id || currentLeaveType !== type_leaves) {
                        // If there's a valid range for the previous maid's leave type, display it
                        // if (leaveStartDate !== null && leaveEndDate !== null && new Date(leaveEndDate) - new Date(leaveStartDate) > 0) {
                        //     $('[data-leave-maid="' + currentMaidId + '"]').html("<b>" + currentLeaveType + " from " + leaveStartDate + " to " + leaveEndDate + "</b><br>");
                        // }
                        if (leaveStartDate !== null && leaveEndDate !== null && new Date(leaveEndDate) - new Date(leaveStartDate) > 0) {
                            $('[data-leave-maid="' + currentMaidId + '"]').html("<b>" + currentLeaveType + " from " + formatDate(leaveStartDate) + " to " + formatDate(leaveEndDate) + "</b><br>");
                        }
                        // Now, reset to the new maid or new leave type
                        currentMaidId = maid_id;
                        currentLeaveType = type_leaves;
                        leaveStartDate = leave.leave_date;
                        leaveEndDate = leave.leave_date;
                    } else {
                        // If the leave type is the same, check if it's contiguous with the previous leave
                        var previousDate = new Date(leaveEndDate);
                        var currentDate = new Date(leave.leave_date);
                        var dayDifference = (currentDate - previousDate) / (1000 * 3600 * 24);  // Difference in days
                
                        if (dayDifference === 1) {
                            // If it's contiguous (1 day difference), extend the range
                            leaveEndDate = leave.leave_date;
                        } else {
                            // If not contiguous, display the current range (only if it's a multi-day range)
                            if (new Date(leaveEndDate) - new Date(leaveStartDate) > 0) {
                                $('[data-leave-maid="' + currentMaidId + '"]').html("<b>" + currentLeaveType + " from " + formatDate(leaveStartDate) + " to " + formatDate(leaveEndDate) + "</b><br>");
                            }
                            // Start a new range with the current leave date
                            leaveStartDate = leave.leave_date;
                            leaveEndDate = leave.leave_date;
                        }
                    }
                });
                
                // Output the last range (if any), but only if it's a multi-day range
               
                if (leaveStartDate !== null && leaveEndDate !== null && new Date(leaveEndDate) - new Date(leaveStartDate) > 0) {
                    $('[data-leave-maid="' + currentMaidId + '"]').html("<b>" + currentLeaveType + " from " + formatDate(leaveStartDate) + " to " + formatDate(leaveEndDate) + "</b><br>");
                }
                
                var element = $('[data-maid-week="' + booking.maid_id + "-" + booking.service_week_day + '"] [data-time-from="' + booking.time_from + '"]');
                $(element).html(bubble).addClass("booked").off().on("click", function () {
                    $('.mm-loader').show();
                    $.ajax({
                        type: 'GET',
                        dataType: "json",
                        url: _base_url + "schedule_data/get_schedule_data",
                        data: { booking_id: booking.booking_id, service_date: $('#date').val() },
                        success: function (data) {
                            var schedule_data = data.schedule_data;
                            var maid_id = schedule_data.maid_id;
                            var week_day = schedule_data.service_week_day;
                            var week_name = $('#weeks .maid.week[data-id="' + week_day + '"][data-maid="' + maid_id + '"]').attr('data-week-name');
                            cleaning_material_rate_per_hour = schedule_data._cleaning_material_rate_per_hour;
                            $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val(schedule_data._cleaning_material_rate_per_hour);
                            $('#new_booking_form input[name="service_week_day"]').val(week_day);
                            $('#new_booking_form input[name="maid_id"]').val(maid_id);
                            $('#new_booking_form input[name="customer_name"]').val(schedule_data.customer_name);
                            $('#new_booking_form input[name="maid_name"]').val(schedule_data.maid_name);
                            $('#new_booking_form select[name="service_type_id"]').select2().val(schedule_data.service_type_id).trigger("change.select2").prop('disabled', true);
                            $('#new_booking_form select[name="time_from"]').select2().val(schedule_data.time_from).trigger("change.select2").prop('disabled', false);
                            $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(schedule_data.time_from)).select2().val(schedule_data.time_to).trigger("change.select2").prop('disabled', false);
                            $('#new_booking_form input[name="working_hours"]').val(hourDifference(schedule_data.time_from, schedule_data.time_to));
                            $('#new_booking_form input[name="service_week_name"]').val(week_name);
                            $('#new_booking_form input[name="service_start_date"]').val(moment(schedule_data.service_start_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                            $('#new_booking_form input[name="service_end_date"]').val(moment(schedule_data.service_end_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
                            $('#new_booking_form input[name="cleaning_materials"]').prop('checked', schedule_data._cleaning_material == "Y");
                            if (schedule_data._cleaning_material == "Y") {
                                $('#new_booking_form #material').show();
                                $('#new_booking_form input[name="material_cost1"][value="' + schedule_data.material_cost + '"]').prop('checked', true);
                            } else {
                                $('#new_booking_form #material').hide();
                            }
                            $('#new_booking_form input[name="charge_type"][value="' + schedule_data.charge_type + '"]').prop('checked', true);
                            // fill hidden fields
                            $('#new_booking_form input[name="day_service_id"]').val(schedule_data.day_service_id || "");
                            $('#new_booking_form input[name="day_service_status"]').val(schedule_data.day_service_status || "");
                            $('#new_booking_form input[name="booking_id"]').val(booking.booking_id);
                            $('#new_booking_form input[name="booking_type"]').val(schedule_data.booking_type);
                            $('#new_booking_form input[name="service_date"]').val($('#date').val());
                            // fill fields from db
                            $('#new_booking_form input[name="service_rate_per_hour"]').val(schedule_data._service_rate_per_hour);
                            $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(schedule_data._service_discount_rate_per_hour);
                            $('#new_booking_form input[name="taxed_total"]').val(schedule_data._total_amount);
                            $('#new_booking_form input[name="service_vat_amount"]').val(schedule_data._vat_amount);
                            $('#new_booking_form input[name="material_fee"]').val(schedule_data._net_cleaning_material_amount);
                            service_vat_percentage = schedule_data._vat_percentage;
                            $('#new_booking_form .vat_percentage').html(service_vat_percentage);
                            $('#new_booking_form input[name="service_vat_percentage"]').val(service_vat_percentage);
                            reCalculateForm();
                            /*********************************** */
                            $('#new-booking-form-popup .title').html('Edit ' + (schedule_data.booking_type == "OD" ? "One Time Schedule" : "Schedule") + " ~ <i>" + (schedule_data.day_service_reference_id ? (schedule_data.day_service_reference_id + " (" + schedule_data.booking_reference_id + ")") : " (" + schedule_data.booking_reference_id + ")") + "</i>");
                            $('#new_booking_form select[name="customer_id"]').next().hide();
                            $('#new_booking_form input[name="customer_name"]').show();
                            $("#new-booking-form-popup .modal-footer.new").hide();
                            $("#new-booking-form-popup .modal-footer.edit").show();
                            /****** styling  */
                            $("#new-booking-form-popup .green-popup-head").removeClass(function (index, className) {
                                return (className.match(/(^|\s)bg-\S+/g) || []).join(' ');
                            });
                            $("#new-booking-form-popup .green-popup-head").addClass("bg-" + schedule_data.booking_type.toLowerCase());
                            /****** styling end  */
                            if (schedule_data.booking_type == "WE") { 
                                $("#new-booking-form-popup .modal-footer #update_one_day").show();
                            } else {
                                $("#new-booking-form-popup .modal-footer #update_one_day").hide();
                                
                            }
                            if (schedule_data.dispatch_status == 2) {
                                $('#new-booking-form-popup .title').html('Schedule ~  <i>' + (schedule_data.day_service_reference_id + " (" + schedule_data.booking_reference_id + ")</i>"));
                                // $("#new-booking-form-popup .modal-footer").hide();
                                $("#new-booking-form-popup .modal-footer .edit").show();
                            }
                            fancybox_show('new-booking-form-popup', { width: 900 });
                        },
                        error: function (data) {
                            toast('warning', "An error occured !");
                            $('.mm-loader').hide();
                        },
                    });
                });
            });
            RefreshSomeEventListener();
            styleThisWeekCells();
            loader(false);
        },
        error: function (data) {
            toast('warning', "An error occured !");
            $('.mm-loader').hide();
            loader(false);
        },
    });
}
function formatDate(date) {
    var d = new Date(date);
    var day = String(d.getDate()).padStart(2, '0');
    var month = String(d.getMonth() + 1).padStart(2, '0'); // Months are zero-indexed
    var year = d.getFullYear();
    return day + '/' + month + '/' + year;
}
function updateTotalHourWeek(total_hour_week) {
    // Clear previous data
    $('[data-maid-id]').html('');
    $('[data-maid-id-percentage]').html('');
    $('[data-maid-week-id]').html('');
    $('[data-maid-id-week-id]').html('');

    // Update HTML content with new data
    $.each(total_hour_week, function (key, tot) {
        var working_minutes1 = tot.working_minutes / 60;
        var max_time = 8;
        var percentage = (working_minutes1 / (max_time / 100)).toFixed(2) + "%";
        
        $('[data-maid-id="' + tot.maid_id + '"]').html(working_minutes1);
        $('[data-maid-id-percentage="' + tot.maid_id + '"]').html(percentage);
        $('[data-maid-week-id="' + tot.maid_id + '"]').html(working_minutes1);
        $('[data-maid-id-week-id="' + tot.maid_id + '"]').html(percentage);
    });
}
function styleThisWeekCells() {
    var currentTime = moment();
    var dow = currentTime.day();
    var time_rounded = moment(currentTime).format("H:00:00");
    $('.cell').removeClass("current-time");
    $('.cell').removeAttr('style');
    $('.cell').each(function (index, option) {
        /******* current time highlight  */
        if ($(this).attr("data-time-from") == time_rounded) {
            //$(this).addClass("current-time");
            $(this).css({ "border-right": "1px solid rgb(255 150 150)" });
        }
    });
}
$(document).ready(function () {
    renderBookings();

    validator = $('#delete_day_schedule').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/delete_schedule",
                data: $('#delete_day_schedule').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                        $("#delete_day_schedule")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#delete_day_schedule")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#delete-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_one_day_schedule_od_validator = $('#suspend_one_day_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_one_day_schedule_od",
                data: $('#suspend_one_day_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-one-day-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_one_day_schedule_we_validator = $('#suspend_one_day_schedule_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_one_day_schedule_we",
                data: $('#suspend_one_day_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-one-day-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_date_range_schedule_od_validator = $('#suspend_date_range_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_date_range_schedule_od",
                data: $('#suspend_date_range_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                        $("#suspend_date_range_schedule_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_date_range_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-date-range-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    suspend_date_range_schedule_we_validator = $('#suspend_date_range_schedule_we').validate({
        ignore: [],
        rules: {
            delete_date_from: {
                required: true,
            },
            delete_date_to: {
                required: true,
            },
            remark: {
                required: true,
            },
           
        },
        messages: {
            delete_date_from: "Please enter date.",
            delete_date_to: "Please enter date.",
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/suspend_date_range_schedule_we",
                data: $('#suspend_date_range_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                        $("#suspend_date_range_schedule_we")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#suspend_date_range_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#suspend-date-range-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_permeantly_od_validator = $('#cancel_permeantly_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_permeantly_od",
                data: $('#cancel_permeantly_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                        $("#cancel_permeantly_od")[0].reset() 
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_permeantly_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-permeantly-od-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_permeantly_we_validator = $('#cancel_permeantly_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_permeantly_we",
                data: $('#cancel_permeantly_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#cancel_permeantly_we")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_permeantly_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-permeantly-we-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_one_day_schedule_od_validator = $('#cancel_one_day_schedule_od').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_one_day_schedule_od",
                data: $('#cancel_one_day_schedule_od').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#cancel_one_day_schedule_od")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_one_day_schedule_od")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-one-day-od-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
    cancel_one_day_schedule_we_validator = $('#cancel_one_day_schedule_we').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_one_day_schedule_we",
                data: $('#cancel_one_day_schedule_we').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#cancel_one_day_schedule_we")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_one_day_schedule_we")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-one-day-we-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });

    cancel_validator = $('#cancel_day_schedule').validate({
        ignore: [],
        rules: {
            remark: {
                required: true,
            },
           
        },
        messages: {
            remark: "Add remark.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("id") == "remark") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.mm-loader').show();
            $('#form-error').empty();
            // $('#delete-schedule-btn').html('OK...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/cancel_schedule",
                data: $('#cancel_day_schedule').serialize(),
                success: function (data) {
                    
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#cancel_day_schedule")[0].reset() 
                        setTimeout(function () {
                            window.location.href = _base_url + "schedule/day_view";
                        }, 1000);
                    }
                    else {
                        toast('error', data.message);
                        $("#cancel_day_schedule")[0].reset() 
                        loader(false);
                    }
                }, 
                error: function (data) {
                    toast('error', data.statusText);
                    $('#cancel-schedule-btn').html('Ok').removeAttr("disabled");
                    $('.mm-loader').hide();
                },
            });
        }
    });
});

/********************************************************************************** */
// date picker
$('.datepicker').datepicker({ autoclose: true, todayHighlight: true, todayBtn: "linked" }).on('changeDate', function (event) {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var date_full = moment(date).format('DD/MM/YYYY');
    $('.hed-date-main-box').html(date_full);
    $('#date').val(date);
    service_week_day = moment(date).day();
    renderBookings();
    setTimeout(function () {
        window.location.href = _base_url + "schedule/day_view?service_date=" + date;
    }, 1000);
    // window.history.pushState({}, "", "day_view?service_date=" + date); // just change url without reload, useful for link sharing (with date)
});
$('.hed-date-left-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(-1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
$('.hed-date-right-arrow').click(function () {
    var date = $(".datepicker").data('datepicker').getFormattedDate('yyyy-mm-dd');
    $('#date').val(date);
    date = moment(date, 'YYYY-MM-DD').add(1, 'days');
    $(".datepicker").datepicker("update", date.toDate()).trigger('changeDate');
});
/***************************************************************************************** */
$('#new_booking_form select[name="time_from"]').on('change', function () {
    $('#new_booking_form input[name="working_hours"]').val(0);
    $('#new_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change");
});
$('#new_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#new_booking_form select[name="time_to"]').val();
    var time_from = $('#new_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#new_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
/***************************************************************************************** */
function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#new_booking_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
/***************************************************************************************** */
$('#suspend_one_day_schedule_od input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
});
$('#cancel_one_day_schedule_we input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    // startDate: "today",
    autoclose: true,
});
$('#suspend_one_day_schedule_we input[name="suspend_date_from"]').datepicker({
    format: 'dd/mm/yyyy',
    startDate: "today",
    autoclose: true,
});
$('#new_booking_form input[name="service_start_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_start_date"]').valid();
});
$('#new_booking_form input[name="service_end_date"]').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    clearBtn: true
}).on('changeDate', function (e) {
    $('#new_booking_form input[name="service_end_date"]').valid();
});
$('#new_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            if (response.id === "") {

            } else {
                return { results: response };
            }
        },
        cache: true
    },
}).on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});
$('#new_booking_form select[name="time_to"]').select2().on("change", function (e) {
    $(this).valid();
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_week_day"]').on('change', function () {
    var week_day_id = this.value;
    var maid_id = $('#schedule-menu select[name="filter_maid_id"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
});
/***************************************************************************************** */
$('#schedule-menu select[name="filter_maid_id"]').on('change', function () {
    var maid_id = this.value;
    var week_day_id = $('#schedule-menu select[name="filter_week_day"]').val();
    maidHeads(false);
    maidWeeks(false);
    slots(false);
    if (maid_id) {
        $('#weeks .maid_name_week[data-maid="' + maid_id + '"]').show();
        $('.tb-slider .maid_name[data-maid="' + maid_id + '"]').show();
    }
    else {
        maidHeads(true);
    }
    if (week_day_id) {
        if (maid_id) {
            $('#weeks .week[data-id="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"][data-maid="' + maid_id + '"]').show();
        }
        else {
            $('#weeks .week[data-id="' + week_day_id + '"]').show();
            $('.tb-slider .slots[data-week="' + week_day_id + '"]').show();
        }
    }
    else {
        if (maid_id) {
            $('#weeks .week[data-maid="' + maid_id + '"]').show();
            $('.tb-slider .slots[data-maid="' + maid_id + '"]').show();
        }
        else {
            maidWeeks(true);
            slots(true);
        }
    }
    renderCurrentWeekDay();
});
/***************************************************************************************** */
function maidHeads(show) {
    if (show == true) {
        $('#weeks .maid_name_week').show();
        $('.tb-slider .maid_name').show();
    }
    else {
        $('#weeks .maid_name_week').hide();
        $('.tb-slider .maid_name').hide();
    }
}
function maidWeeks(show) {
    if (show == true) {
        $('#weeks .week').show();
    }
    else {
        $('#weeks .week').hide();
    }
}
function slots(show) {
    if (show == true) {
        $('.tb-slider .slots').show();
    }
    else {
        $('.tb-slider .slots').hide();
    }
}
/***************************************************************************************** */
$().ready(function () {
    new_booking_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: true,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#new_booking_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/save_schedule",
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
    update_one_day_form_validator = $('#new_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#new_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_start_date: {
                required: true,
            },
            service_end_date: {
                required: true,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            service_start_date: "Select service start date.",
            service_end_date: "Select service end date.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
    });
});
/***************************************************************************************** */
function reCalculateForm() {
    var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#new_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#new_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var cleaning_material_rate_per_hour1 = $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val();

    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour1 * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#new_booking_form input[name="material_fee"]').val(material_fee);
    $('#new_booking_form input[name="service_amount"]').val(service_amount);
    $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#new_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#new_booking_form input[name="service_discount"]').val(service_discount);
    $('#new_booking_form input[name="service_rate_per_hour"]').valid();
    $('#new_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$("#new_booking_form input[name='cleaning_material_rate_per_hour'],#new_booking_form select[name='time_to']").change(function () {
    reCalculateForm();
});
$("#new_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateForm();
});
$("#new_booking_form input[name='service_discount_rate_per_hour'],#new_booking_form input[name='service_rate_per_hour']").on("input", function () {
    reCalculateForm();
});
$('button[data-action="show-bookings-for-dispatch"]').click(function (event) {
    $('.mm-loader').show();
    var service_date = $(this).attr("data-dispatch-date");
    $('input[id="check_all"]').prop('checked', false);
    $('#bookings-dispatch-list .inner').html('<div class="inner">Loading booikings<span class="dots_loader"></span></div>');
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: _base_url + "dispatch/get_bookings_for_dispatch",
        data: { 'service_date': service_date },
        success: function (bookings) {
            if (bookings.length == 0) {
                toast('warning', "No pending schedule available to dispatch.");
                return false;
            }
            var _maid_html = `<div class="table"><input type="hidden" name="dispatch_service_date" value="` + service_date + `">`;
            $.each(bookings, function (key, booking) {
                if (booking.day_service_id != null) {
                    var time_from = booking.ds_time_from;
                    var time_to = booking.ds_time_to;
                } else {
                    var time_from = booking.time_from;
                    var time_to = booking.time_to;
                }
                //if (booking.day_service_id == null) {
                _maid_html += `<div class="row n-pick-maid-c-main m-0 n-radi-check-main">
                    <div class="col-sm-10 n-pick-maid-left pl-0">
                    <span id=""><strong>Booking Ref. &nbsp;</strong>` + booking.reference_id + `</span><br />
                    <span id=""><strong>Schedule Ref. &nbsp;</strong>` + (booking.day_service_reference_id || '<i>NIL</i>') + `</span><br />
                    <span><i class="fa fa-user" aria-hidden="true"></i>&nbsp;` + booking.customer_name + `</span>
                    <p><strong><i class="fa fa-clock-o" aria-hidden="true"></i></strong>&nbsp;` + moment(time_from, ["HH:mm:ss"]).format("hh:mm A") + ` - ` + moment(time_to, ["HH:mm:ss"]).format("hh:mm A") + ` ( ` + booking.maid_name + ` )</p>
                    </div>
                    <div class="col-sm-2 n-pick-maid-right pr-0">
                    <input type="checkbox" value="` + booking.booking_id + `" name="dispatch_booking_ids[]" id="dispatch_booking_id_` + key + `" ` + (booking.dispatch_status == 1 ? "disabled" : "") + `/>
                    <label for="dispatch_booking_id_` + key + `">
                    <span class="border-radius-3"></span>
                    </label>
                    <p class="text-success">`+ (booking.dispatch_status == 1 ? "Dispatched !" : "") + `</p>
                    </div></div>`;
                //}
            });
            _maid_html += `</div>`;
            $('#bookings-dispatch-list .inner').html(_maid_html);
            fancybox_show('bookings-for-dispatch', { width: 600 });
        },
        error: function (data) {
            $('#alert-title').html('Error !');
            $('#alert-message').html('An error occured !');
            fancybox_show('alert-popup');
        },
    });
});
$(document).on('change', 'input[name="dispatch_booking_ids[]"]', function () {
    if ($('input[name="dispatch_booking_ids[]"]:checked').length == $('input[name="dispatch_booking_ids[]"]').not(":disabled").length) {
        $("#bookings-for-dispatch #check_all").prop("checked", true);
    }
    else {
        $("#bookings-for-dispatch #check_all").prop("checked", false);
    }
    if ($('input[name="dispatch_booking_ids[]"]:checked').length > 0) {
        $('button[data-action="dispatch-now"]').show();
    }
    else {
        $('button[data-action="dispatch-now"]').hide();
    }
});
$('#bookings-for-dispatch #check_all').change(function () {
    if (this.checked) {
        $('input[name="dispatch_booking_ids[]"]').not(":disabled").prop("checked", true);
        if ($('input[name="dispatch_booking_ids[]"]:checked').length == 0) {
            toast('warning', "No booking available to dispatch.");
            $('#bookings-for-dispatch #check_all').prop("checked", false);
        }
    }
    else {
        $('input[name="dispatch_booking_ids[]"]').prop('checked', false);
    }
    $('input[name="dispatch_booking_ids[]"]').trigger("change");
});
$(document).on('change', 'input[name="day_service_ids[]"]', function () {
    if ($('input[name="day_service_ids[]"]:checked').length == $('input[name="day_service_ids[]"]').not(":disabled").length) {
        $("#bookings-for-confirm-dispatch #check_all").prop("checked", true)
    }
    else {
        $("#bookings-for-confirm-dispatch #check_all").prop("checked", false)
    }
});
$('button[data-action="dispatch-now"]').click(function (event) {
    $('.mm-loader').show();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "dispatch/dispatch_bookings",
        data: $('#bookings-dispatch-form').serialize(),
        success: function (data) {
            if (data.status == true) {
                close_fancybox();
                toast('success', data.message);
            }
            else {
                toast('error', data.message);
            }
            renderBookings();
        },
        error: function (data) {
            close_fancybox();
            toast('error', 'An error occured !');
            renderBookings();
        },
    });
});
/*************************************************************************************************************************** */
$().ready(function () {
    new_od_booking_form_validator = $('#od_booking_form').validate({
        ignore: [],
        rules: {
            customer_id: {
                required: function () {
                    return $('#od_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_week_day: {
                required: true,
            },
            maid_id: {
                required: function () {
                    return $('#od_booking_form input[name="booking_id"]').val() == "";
                },
            },
            service_type_id: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
            service_rate_per_hour: {
                required: true,
            },
            service_discount_rate_per_hour: {
                required: true,
            },
            material_fee: {
                required: true,
            },
            cleaning_material_rate_per_hour: {
                required: true,
            }
        },
        messages: {
            customer_id: "Select customer from list.",
            maid_id: "Select maid.",
            service_week_day: "Select service week.",
            service_type_id: "Select service from list.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
            material_fee: "Enter material rate.",
            service_discount_rate_per_hour: "Enter discounted service rate.",
            cleaning_material_rate_per_hour: "Enter materials per hour rate."
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "customer_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "maid_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_type_id") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "material_fee") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_from") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "service_discount_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else if (element.attr("name") == "cleaning_material_rate_per_hour") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            loader(true);
            /****************************** */
            // format before send
            var values = $("#od_booking_form").serializeArray();
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "schedule/" + ($('#od_booking_form input[name="booking_id"]').val() == "" ? "save_booking" : "update_booking"),
                data: values,
                success: function (data) {
                    if (data.status == "success") {
                        close_fancybox();
                        toast('success', data.message);
                        renderBookings();
                        $("#customer-picked-address").html('');
                    }
                    else {
                        toast('error', data.message);
                        loader(false);
                    }
                },
                error: function (data) {
                    toast('error', data.statusText);
                },
            });
        }
    });
});
$('body').on('change', '#od_booking_form input[name="cleaning_materials"]', function () {

    if ($('#od_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        $('#od_booking_form #material').show();
    }else {
        $('#od_booking_form #material').hide();
    }
});
$('body').on('change', '#od_booking_form input[name="material_cost"]', function () {
    if ($(this).val() == "0") {
		$('#od_booking_form input[name="cleaning_material_rate_per_hour1"]').attr('readonly', true);
        $('#od_booking_form input[name="cleaning_material_rate_per_hour1"]').val(10);
    } else if ($(this).val() == "1") {

    $('#od_booking_form input[name="cleaning_material_rate_per_hour1"]').attr('readonly', false);
        $('#od_booking_form input[name="cleaning_material_rate_per_hour1"]').val('');
	} 
});
// $('body').on('change', '#od_booking_form #free-service', function () {
$('body').on('change', '#od_booking_form input[name="charge_type1"]', function () {
    if ($(this).val() == "0") {
        $('#od_booking_form input[name="service_rate_per_hour"]').attr('readonly', false);
		$('#od_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', false);
        $('#od_booking_form input[name="service_rate_per_hour"]').val('');
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val('');
    } else if ($(this).val() == "1") {
	// if ($('#od_booking_form #free-service').is(':checked') == true) {

		$('#od_booking_form input[name="service_rate_per_hour"]').attr('readonly', true);
		$('#od_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', true);
        $('#od_booking_form input[name="service_rate_per_hour"]').val(0);
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val(0);
	} else if($(this).val() == "2") {
        $('#od_booking_form input[name="service_rate_per_hour"]').attr('readonly', true);
		$('#od_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', true);
        $('#od_booking_form input[name="service_rate_per_hour"]').val(0);
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val(0);
	}
});
$('body').on('change', '#new_booking_form input[name="cleaning_materials"]', function () {

    if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        $('#new_booking_form #material').show();
    }else {
        $('#new_booking_form #material').hide();
    }
});
// var original_cleaning_material_rate_per_hour = $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val();

$('body').on('change', '#new_booking_form input[name="material_cost1"]', function () {
    if ($(this).val() == "0") {
		$('#new_booking_form input[name="cleaning_material_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val(10);
        
    } else if ($(this).val() == "1") {

    $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').attr('readonly', false);
        $('#new_booking_form input[name="cleaning_material_rate_per_hour"]').val('');
	} 
});
var original_service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
var original_service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
var startTime = moment($('#new_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
// if ($('#new_booking_form select[name="time_to"]').val() == null) {
//     return false;
// }
var endTime = moment($('#new_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
var duration = moment.duration(endTime.diff(startTime));
var hours = parseFloat(duration.asHours());
// $('#new_booking_form input[name="working_hours"]').val(hours);
var service_rate_per_hour = $('#new_booking_form input[name="service_rate_per_hour"]').val();
var service_discount_rate_per_hour = $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
var service_discount = (original_service_rate_per_hour - original_service_discount_rate_per_hour) * hours;
var material_fee = 0;
var service_rate = service_discount_rate_per_hour * hours;
if ($('#new_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
    material_fee = cleaning_material_rate_per_hour * hours;
}
var service_amount = service_rate + material_fee;
var original_service_amount = service_rate + material_fee;
var service_vat_amount = (service_vat_percentage / 100) * service_amount;
var taxed_total = service_amount + service_vat_amount;
// $('body').on('change', '#new_booking_form #free-service', function () {
 $('body').on('change', '#new_booking_form input[name="charge_type"]', function () {
    // $("#new_booking_form input[name='charge_type']").click(function () {
    var  service_rate_per_hour =  $('#new_booking_form input[name="service_rate_per_hour"]').val();
    var  service_discount_rate_per_hour =  $('#new_booking_form input[name="service_discount_rate_per_hour"]').val();
    var  service_amount1 =  $('#new_booking_form input[name="service_amount"]').val();
    var service_vat_amount1 =  $('#new_booking_form input[name="service_vat_amount"]').val();
    var taxed_total1 =  $('#new_booking_form input[name="taxed_total"]').val();
    if ($(this).val() == "0") {
        var  service_rate_per_hour =  $('#new_booking_form input[name="service_rate_per_hour"]').val();
		$('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', false);
        $('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', false).val(original_service_rate_per_hour);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(original_service_discount_rate_per_hour);
        $('#new_booking_form input[name="service_amount"]').val(original_service_amount);
        $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount);
        $('#new_booking_form input[name="taxed_total"]').val(taxed_total)
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', false);
    }
    else if ($(this).val() == "1") {

	    // if ($('#new_booking_form #free-service').is(':checked') == true) {
        original_service_rate_per_hour = service_rate_per_hour;
        original_service_discount_rate_per_hour = service_discount_rate_per_hour;
        original_service_discount_rate_per_hour = service_discount_rate_per_hour;
        original_service_amount = service_amount1;
        service_vat_amount = service_vat_amount1;
        taxed_total = taxed_total1;
		$('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', true);
		$('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="service_rate_per_hour"]').val(0);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(0);
        $('#new_booking_form input[name="service_amount"]').val(0);
        $('#new_booking_form input[name="service_vat_amount"]').val(0);
        $('#new_booking_form input[name="taxed_total"]').val(0);
	} else if ($(this).val() == "2") {
        $('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', true);
		$('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', true);
        $('#new_booking_form input[name="service_rate_per_hour"]').val(0);
        $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(0);
        $('#new_booking_form input[name="service_amount"]').val(0);
        $('#new_booking_form input[name="service_vat_amount"]').val(0);
        $('#new_booking_form input[name="taxed_total"]').val(0);
    }
    //  else {
    //     var  service_rate_per_hour =  $('#new_booking_form input[name="service_rate_per_hour"]').val();
	// 	$('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', false);
    //     $('#new_booking_form input[name="service_rate_per_hour"]').attr('readonly', false).val(original_service_rate_per_hour);
    //     $('#new_booking_form input[name="service_discount_rate_per_hour"]').val(original_service_discount_rate_per_hour);
    //     $('#new_booking_form input[name="service_amount"]').val(original_service_amount);
    //     $('#new_booking_form input[name="service_vat_amount"]').val(service_vat_amount);
    //     $('#new_booking_form input[name="taxed_total"]').val(taxed_total)
    //     $('#new_booking_form input[name="service_discount_rate_per_hour"]').attr('readonly', false);
    //     // $('#b-rate_per_hr').val(_saved_booking_rate_per_hour).trigger("input");
	// }
});
function reCalculateFormOD() {
    var startTime = moment($('#od_booking_form select[name="time_from"]').val(), 'HH:mm:ss');
    if ($('#od_booking_form select[name="time_to"]').val() == null) {
        return false;
    }
    var endTime = moment($('#od_booking_form select[name="time_to"]').val(), 'HH:mm:ss');
    var duration = moment.duration(endTime.diff(startTime));
    var hours = parseFloat(duration.asHours());
    $('#od_booking_form input[name="working_hours"]').val(hours);
    var service_rate_per_hour = $('#od_booking_form input[name="service_rate_per_hour"]').val();
    var service_discount_rate_per_hour = $('#od_booking_form input[name="service_discount_rate_per_hour"]').val();
    var service_discount = (service_rate_per_hour - service_discount_rate_per_hour) * hours;
    var cleaning_material_rate_per_hour1 = $('#od_booking_form input[name="cleaning_material_rate_per_hour1"]').val();
    var material_fee = 0;
    var service_rate = service_discount_rate_per_hour * hours;
    if ($('#od_booking_form input[name="cleaning_materials"]').prop('checked') == true) {
        material_fee = cleaning_material_rate_per_hour1 * hours;
    }
    var service_amount = service_rate + material_fee;
    var service_vat_amount = (service_vat_percentage / 100) * service_amount;
    var taxed_total = service_amount + service_vat_amount;
    $('#od_booking_form input[name="material_fee"]').val(material_fee);
    $('#od_booking_form input[name="service_amount"]').val(service_amount);
    $('#od_booking_form input[name="service_vat_amount"]').val(service_vat_amount.toFixed(2));
    $('#od_booking_form input[name="taxed_total"]').val(taxed_total.toFixed(2));
    $('#od_booking_form input[name="service_discount"]').val(service_discount);
    //$('#od_booking_form input[name="service_rate_per_hour"]').valid();
    //$('#od_booking_form input[name="service_discount_rate_per_hour"]').valid();
}
$('#od_booking_form select[name="customer_id"]').select2({
    ajax: {
        url: _base_url + "schedule/customers_search",
        type: "post",
        dataType: 'json',
        delay: 150,
        data: function (params) {
            return {
                query: params.term,
            };
        },
        processResults: function (response) {
            return { results: response };
        },
        cache: true
    },
}).on("change", function (e) {
    var cust_id = $(this).val();
    open_address_panel(cust_id)
    $(this).valid();
});
$('#od_booking_form select[name="time_from"]').on('change', function () {
    $('#od_booking_form input[name="working_hours"]').val(0);
    $('#od_booking_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change");
});
$('#od_booking_form select[name="time_to"]').on('change', function () {
    var time_to = $('#od_booking_form select[name="time_to"]').val();
    var time_from = $('#od_booking_form select[name="time_from"]').val();
    if (time_to) {
        $('#od_booking_form input[name="working_hours"]').val(hourDifference(time_from, time_to));
    }
});
$("#od_booking_form input[name='service_rate_per_hour']").on("change", function () {
    var service_discount_rate_per_hour = $('#od_booking_form input[name="service_discount_rate_per_hour"]').val();
    if (service_discount_rate_per_hour) {
    }
    else {
        $('#od_booking_form input[name="service_discount_rate_per_hour"]').val(this.value).trigger("change").valid();
    }
    reCalculateFormOD();
});
$("#od_booking_form input[name='service_discount_rate_per_hour'],#od_booking_form input[name='service_rate_per_hour']").on("input", function () {
    $('#od_booking_form input[name="service_rate_per_hour"]').valid();
    reCalculateFormOD();
});
// $("#od_booking_form input[name='cleaning_materials'],#od_booking_form select[name='time_to']").change(function () {
    $("#od_booking_form input[name='cleaning_material_rate_per_hour1'], #od_booking_form select[name='time_to']").change(function () {
    reCalculateFormOD();
});
$('#od_booking_form select[name="service_type_id"]').select2().on("change", function (e) {
    $(this).valid();
});

$('#schedulte_delete').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        $('#delete_day_schedule input[name="booking_id"]').val(booking_id);
        $('#delete_day_schedule input[name="day_service_id"]').val(day_service_id);
        $('#delete_day_schedule input[name="service_date"]').val(service_date);
        $('#delete_day_schedule input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('delete-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        // $('#booking_id_perment').val(booking_id);
        // $('#booking_id_day').val(booking_id);
        fancybox_show('delete-regular-schedule-popup', { width: 450});

    }
});
$(".n-delete-set-right").click(function () {
    $('.n-delete-set-right').removeClass('de-select');
    $('.n-delete-set-left').addClass('de-select');

    $('.n-delete-set-right-cont').show(500);
    $('.n-delete-set-left-cont').hide(500);
});

$(".n-delete-set-left").click(function () {
    $('.n-delete-set-left').removeClass('de-select');
    $('.n-delete-set-right').addClass('de-select');

    $('.n-delete-set-left-cont').show(500);
    $('.n-delete-set-right-cont').hide(500);
});

$('.n-delete-set-left-cont .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$('#delete_from_date').datepicker({
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    todayBtn: "linked",
    autoclose: true,
    //clearBtn: true,
    startDate: "today"
});

$('#pay_customer1').on('change', function() {
    if (this.checked) {
       $("#pay_amount").show();
    } else {
        $("#pay_amount").hide();
    }
});

$('#pay_customer').on('change', function() {
    if (this.checked) {
       $("#pay_amount1").show();
    } else {
        $("#pay_amount1").hide();
    }
});

$('#suspend_date_range_schedule_od .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    // startDate: "today",
    todayHighlight: true
});

$('#suspend_date_range_schedule_we .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    // startDate: "today",
    todayHighlight: true
});

$(".delete_yes_book_day").click(function () {
    var remarks = $.trim($('#delete_remark_day').val());
    var booking_id = $('#delete_one_schedule input[name="booking_id_day"]').val();
    var delete_date_from = $('#delete_one_schedule input[name="delete_date_from"]').val();
    var delete_date_to = $('#delete_one_schedule input[name="delete_date_to"]').val();
    // var booking_id = $('#booking_id_day').val();
    // var delete_date_from = $('#delete_date_from').val();
    // var delete_date_to = $('#delete_date_to').val();
   // alert(delete_date_from)
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');
    } else if (delete_date_from == "")
    {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block');   
    } else if (delete_date_to == "") {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#deleteremarks_book_day').css('display','block'); 
    }else {
        $('#deleteremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id, delete_date_from: delete_date_from, delete_date_to: delete_date_to},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".delete_yes_book").click(function () {
    var remarks = $.trim($('#delete_remark_perm').val());
    // var booking_id = $('#booking_id_perment').val();
    var booking_id = $('#delete_permeant_schedule input[name="booking_id_perment"]').val();
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#delete_remark_perm").focus();
         $('#deleteremarks_book').css('display','block');
    } else {
        $('#deleteremarks_book').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/delete_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$('#schedule_cancel').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        $('#cancel_day_schedule input[name="booking_id"]').val(booking_id);
        $('#cancel_day_schedule input[name="day_service_id"]').val(day_service_id);
        $('#cancel_day_schedule input[name="service_date"]').val(service_date);
        $('#cancel_day_schedule input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('cancel-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        $('#cancel_permanent_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#cancel_one_schedule input[name="booking_id_day"]').val(booking_id);  
        // $('#booking_id_perment').val(booking_id);
        // $('#booking_id_day').val(booking_id);
        fancybox_show('cancel-regular-schedule-popup', { width: 450});

    }
});

$(".n-cancel-set-right").click(function () {
    $('.n-cancel-set-right').removeClass('de-select');
    $('.n-cancel-set-left').addClass('de-select');

    $('.n-cancel-set-right-cont').show(500);
    $('.n-cancel-set-left-cont').hide(500);
});

$(".n-cancel-set-left").click(function () {
    $('.n-cancel-set-left').removeClass('de-select');
    $('.n-cancel-set-right').addClass('de-select');

    $('.n-cancel-set-left-cont').show(500);
    $('.n-cancel-set-right-cont').hide(500);
});

$('.n-cancel-set-left-cont .input-daterange').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: "today",
    todayHighlight: true
});

$(".cancel_yes_book_day").click(function () {
    var remarks = $.trim($('#cancel_remark_day').val());
    var booking_id = $('#cancel_one_schedule input[name="booking_id_day"]').val();
    var delete_date_from = $('#cancel_one_schedule input[name="delete_date_from"]').val();
    var delete_date_to = $('#cancel_one_schedule input[name="delete_date_to"]').val();
   // alert(delete_date_from)
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#cancel_remark_day").focus();
        $('#cancelremarks_book_day').css('display','block');
    } else if (delete_date_from == "")
    {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#cancelremarks_book_day').css('display','block');   
    } else if (delete_date_to == "") {
        toast('error', "Add date");
        // $("#delete_remark_day").focus();
        $('#cancelremarks_book_day').css('display','block'); 
    }else {
        $('#cancelremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/cancel_booking_one_day',
			data: {remarks: remarks,booking_id: booking_id, delete_date_from: delete_date_from, delete_date_to: delete_date_to},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$(".cancel_yes_book").click(function () {
    var remarks = $.trim($('#cancel_remark_perm').val());
    // var booking_id = $('#booking_id_perment').val();
    var booking_id = $('#cancel_permanent_schedule input[name="booking_id_perment"]').val();
    if(remarks == "")
    {
        toast('error', "Add remark");
        $("#cancel_remark_perm").focus();
         $('#cancelremarks_book_day').css('display','block');
    } else {
        $('#cancelremarks_book_day').css('display','none');
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/cancel_permeant_booking_schedule',
			data: {remarks: remarks,booking_id: booking_id},
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
});

$('#schedule_suspend_one_day').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        $('#suspend_one_day_schedule_od input[name="booking_id"]').val(booking_id);
        $('#suspend_one_day_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#suspend_one_day_schedule_od input[name="service_date"]').val(service_date);
        $('#suspend_one_day_schedule_od input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('suspend-one-day-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        // $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        // $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#suspend_one_day_schedule_we input[name="booking_id"]').val(booking_id);
        $('#suspend_one_day_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#suspend_one_day_schedule_we input[name="service_date"]').val(service_date);
        $('#suspend_one_day_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-one-day-we-popup', { width: 450});

    }
});


$('#schedule_suspend_date_range').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        
        $('#suspend_date_range_schedule_od input[name="booking_id"]').val(booking_id);
        $('#suspend_date_range_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#suspend_date_range_schedule_od input[name="service_date"]').val(service_date);
        $('#suspend_date_range_schedule_od input[name="delete_date_from"]').val(moment(service_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#suspend_date_range_schedule_od input[name="delete_date_to"]').val(moment(service_date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
        $('#suspend_date_range_schedule_od input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('suspend-date-range-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        // $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        // $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#suspend_date_range_schedule_we input[name="booking_id"]').val(booking_id);
        $('#suspend_date_range_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#suspend_date_range_schedule_we input[name="service_date"]').val(service_date);
        $('#suspend_date_range_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('suspend-date-range-we-popup', { width: 450});

    }
});

$('#cancel_permeantly').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        
        $('#cancel_permeantly_od input[name="booking_id"]').val(booking_id);
        $('#cancel_permeantly_od input[name="day_service_id"]').val(day_service_id);
        $('#cancel_permeantly_od input[name="service_date"]').val(service_date);
        $('#cancel_permeantly_od input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('cancel-permeantly-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        // $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        // $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#cancel_permeantly_we input[name="booking_id"]').val(booking_id);
        $('#cancel_permeantly_we input[name="day_service_id"]').val(day_service_id);
        $('#cancel_permeantly_we input[name="service_date"]').val(service_date);
        $('#cancel_permeantly_we input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-permeantly-we-popup', { width: 450});

    }
});

$('#cancel_one_day').click(function () {
    // alert("helooo");
    var booking_type = $('#new_booking_form input[name="booking_type"]').val();
    var day_service_id = $('#new_booking_form input[name="day_service_id"]').val();
    var booking_id = $('#new_booking_form input[name="booking_id"]').val();
    var service_date = $('#new_booking_form input[name="service_date"]').val();
    var service_status = $('#new_booking_form input[name="day_service_status"]').val();
    if (booking_type == 'OD') {
        
        $('#cancel_one_day_schedule_od input[name="booking_id"]').val(booking_id);
        $('#cancel_one_day_schedule_od input[name="day_service_id"]').val(day_service_id);
        $('#cancel_one_day_schedule_od input[name="service_date"]').val(service_date);
        $('#cancel_one_day_schedule_od input[name="day_service_status"]').val(service_status);
        // $('#day_service_id').val(day_service_id);
        // // $('#booking_id').val(booking_id);
        // $('#service_date').val(service_date);
        // $('#day_service_status').val(service_status);
        fancybox_show('cancel-one-day-od-popup', { width: 450 });

    } else if (booking_type == 'WE') {
        // $('#delete_one_schedule input[name="booking_id_day"]').val(booking_id);
        // $('#delete_permeant_schedule input[name="booking_id_perment"]').val(booking_id);
        $('#cancel_one_day_schedule_we input[name="booking_id"]').val(booking_id);
        $('#cancel_one_day_schedule_we input[name="day_service_id"]').val(day_service_id);
        $('#cancel_one_day_schedule_we input[name="service_date"]').val(service_date);
        $('#cancel_one_day_schedule_we input[name="day_service_status"]').val(service_status);
        fancybox_show('cancel-one-day-we-popup', { width: 450});

    }
});

function open_address_panel(customer_id)
{
    $('#customer-address-id-user').val('');
    $('#customer-address-panel-user').hide();
    $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');
	
    if($.isNumeric(customer_id) && customer_id > 0)
    {	
        $('#customer-address-panel-user').slideDown();
        $.ajax({
            url: _base_url + 'schedule/get_customer_address_details',
            type: 'POST',
            data: {customer_id: customer_id},
            dataType: 'json',
            success: function(response) {
                // console.log(response)
                // $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response)
                // {
                if(response == 'refresh')
                {
                    $.fancybox.open({
                            autoCenter : true,
                            fitToView : false,
                            scrolling : false,
                            openEffect : 'fade',
                            openSpeed : 100,
                            helpers : {
                                    overlay : {
                                            css : {
                                                    'background' : 'rgba(0, 0, 0, 0.3)'
                                            },
                                            closeClick: false
                                    }
                            },
                            padding : 0,
                            closeBtn : false,
                            content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                    });
                }
                else
                {
                    var adressdata = response.customer_address;	
                    // console.log(adressdata)			
                    var address_html = '<div class="table">';                                
                                    
                    $.each(adressdata, function(key, val) {
                        address_html += '<div class="row"><div class="cell1"><input type="hidden" id="address_location" value="'+ val.location_name +'"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                        
                    });				

                    address_html += '</div>';

                    $('#customer-address-panel-user .inner').html(address_html);
                }
            }
        });
    }
}

//End
$('body').on('click', '#customer-address-panel-user .close', function() {
	$('#customer-address-panel-user').slideUp( function() { $('#customer-picked-address').show(); } );
});


$('body').on('click', '.pick_customer_address', function() {
	var address_id = $(this).attr('id').replace('cadddress-', '');
	$('#customer-address-id-user').val(address_id);
	var addzone = $('#caddzone-' + address_id).html();
	var address = $('#cadd-' + address_id).html();
    // $('#customer-address').empty();
    // $('#customer-location').empty();
    // $('#customer-area-zone').empty();
    // $('#customer-sublocation').empty();
    $('#od_booking_form input[name="customer_address_id"]').val('');
    
	$('#customer-address-panel-user').slideUp(function() {
        // $('#customer-address').append(address);
        // $('#customer-area-zone').append(addzone);
        // $('#customer-location').append(response.customer_address[0]['location_name']);
        // $('#customer-sublocation').append(response.customer_address[0]['landmark_name']);
        $('#od_booking_form input[name="customer_address_id"]').val(address_id);
        $('#customer-picked-address').html('<div class="address"><strong>' + addzone + '</strong> - ' + address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
        $('#customer-picked-address').show();
        $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');

        var booking_id = $('#booking-id-user').val();
	});
});

$('body').on('click', '#chg-cust-address', function() {
	var _customer_id = $('#b-customer-id').val();
	$('#customer-picked-address').hide();
	open_address_panel(_customer_id);
});

$("#update_one_day").click(function () {
    
    update_one_day_form_validator.resetForm();
    if ($("#new_booking_form").valid()) {
        $.ajax({
			type: "POST",
			url: _base_url + 'schedule/update_one_day',
            data: $('#new_booking_form').serialize(),
			cache: false,
			success: function (data)
			{
				if (data.status == "success") {
                    close_fancybox();
                    toast('success', data.message);
                    renderBookings();
                }
                else {
                    toast('error', data.message);
                    loader(false);
                }
            },
            error: function (data) {
                toast('error', data.statusText);
            },
		});
    }
    else {
        $('label.error').each(function () {
            if ($(this).css('display') != 'none') {
                var tab_id = $(this).closest('.tab-pane').attr("data-panel");
                $('#mytabs li').removeClass('active');
                $('#' + tab_id + '-li').addClass('active');
                $('.tab-pane').removeClass('active').css('display', 'none');
                $('[data-panel="' + tab_id + '"]').addClass('active').css('display', 'block');
                toast('warning', $(this).text());
                return false;
            }
        });
    }
});
