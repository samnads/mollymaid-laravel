<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

// file versioning for force reload / ignore cached file
define('CSS_VERSION', time());
define('JS_VERSION',time());
define('IMG_VERSION', 1.5);
//
define('DEFAULT_AVATAR', 'img/default_avatar.jpg?v='.IMG_VERSION);
define('UPLOAD_AVATAR', 'img/upload_avatar.jpg?v='.IMG_VERSION);
//
define('DIR_UPLOADS_NAME', 'upload');
define('DIR_UPLOAD_TEMP', './'.DIR_UPLOADS_NAME.'/temp/');
//
define('PROMO_IMG_PATH', DIR_UPLOADS_NAME.'/promotion_images/');
define('DIR_UPLOAD_PROMO_IMG', './'.PROMO_IMG_PATH.'/');
//
define('MAID_AVATAR_PATH', DIR_UPLOADS_NAME.'/maid_avatars/');
define('DIR_UPLOAD_MAID_AVATAR', './' . MAID_AVATAR_PATH.'/');
//
define('CUSTOMER_AVATAR_PATH', DIR_UPLOADS_NAME.'/customer_avatars/');
define('DIR_UPLOAD_CUSTOMER_AVATAR', './' . CUSTOMER_AVATAR_PATH . '/');
//
define('DIR_UPLOAD_OFFER_IMG', './offer_img/uploads/');
//
define('OFFER_IMG_ANDROID_ABSOLUTE_URL', 'https://booking.emaid.info:3443/elite-demo/offer_img/uploads/');
//define('SITE_NAME', 'CompanyName'); // Name of website used in pages, mail and others that require site name.
//define('GOOGLE_MAP_API_KEY', 'AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk');
//define('SERVICE_VAT_PERCENTAGE', 5);
define('SETTINGS_IMG_PATH','uploads/images/settings/');
define('SITE_PAYMENT_URL', '');

/*For Quick Book Connection*/
define('QUICK_AUTH_REQ_URL', 'https://appcenter.intuit.com/connect/oauth2');
define('QUICK_TOKEN_END_URL', 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer');
define('QUICK_AUTH_SCOPE', 'com.intuit.quickbooks.accounting');
define('QUICK_OPENID_SCOPE', 'openid profile email');
define('QUICK_CLIENT_PATH', FCPATH.'application/libraries/Client.php');
define('QUICK_CERTIFICATE_PATH', FCPATH.'application/libraries/Certificate/cacert.pem');