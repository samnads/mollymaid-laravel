<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']    = "login";
//$route['404_override']          = 'dashboard';

$route['logout'] = 'login/logout';
$route['booking/(:num)-(:num)-(:num)']          = 'booking';
$route['booking/(:num)-(:num)-(:num)/(:num)']   = 'booking';
$route['booking/(:num)-(:num)-(:num)/(:num)/(:num)'] = 'booking';

$route['customer/add']              = 'customer';
$route['customer/add/(:any)'] = 'customer/index/$1';
$route['customers']                 = 'customer/customer_list_new';
$route['customers/(:any)']                 = 'customer/customer_list_new';
$route['customers-old']                 = 'customer/customer_list';
$route['customers/(:num)/(:num)']   = 'customer/customer_list/$1/$2';
$route['customers/(:num)/(:num)/(:num)'] = 'customer/customer_list/$1/$2/$3';
$route['customers/(:num)']          = 'customer/customer_list/$1';
//$route['customer/edit/(:num)']      = "customer/edit_customer/$1";
$route['customer/view/(:num)']      = "customer/customer_view/$1";
$route['customer/view/(:num)/(:num)']       = "customer/customer_view/$1/$2";
$route['customer/disable/(:num)/(:num)']    = "customer/disable_customer/$1/$2";

$route['maid/add']  = 'maid';
$route['maids']     = 'maid/maid_list';
$route['maids/(:num)'] = 'maid/maid_list/$1';
$route['maid/edit/(:num)'] = "maid/edit_maid/$1";
$route['maid/view/(:num)'] = "maid/maid_view/$1";
/************************************************************************ */
$route['zones']                     =   'settings/zones';
$route['areas']                     =   'settings/area';
$route['get_zone_area']             =   'settings/get_zone_area';
$route['flats']                     =   'settings/flats';
$route['tablets']                   =   'settings/tablets';
$route['tablets/(:num)']            =   'settings/tablets/$1';
$route['services']                  =   'settings/services';
$route['souqmaid-price']            =   'settings/souqmaid_price';
$route['teams']                     =   'settings/team';
$route['backpayment']               =   'settings/backpayment';
$route['backpayment/add']           =   'settings/add_backpayment';
$route['payment-settings']          =   'settings/payment_settings';
$route['sms-settings']              =   'settings/sms_settings';
$route['email-settings']            =   'settings/email_settings';
$route['tax-settings']              =   'settings/tax_settings';
$route['coupons']                   =   'settings/coupons';
$route['coupons/add']               =   'settings/add_coupons';
$route['coupons/edit/(:num)']       =   'settings/edit_coupon/$1';
/************************************************************************ */
$route['reports/zone']              =   'reports';
$route['reports/schedule']          =   'reports/schedule_reports';
$route['reports/schedule-report']   =   'reports/schedule_report_new';
$route['reports/vehicle']           =   'reports/vehicle_report';
$route['reports/payment']           =   'reports/payment_report';
$route['reports/oneday']            =   'reports/one_day_cancel';
$route['reports/call-report']       =   'reports/call_report';
$route['reports/booking/cancel']    =   'reports/booking_cancel';
$route['reports/work']              =   'reports/employee_work';
$route['reports/work/all']          =   'reports/employee_work_all';
$route['reports/activity']          =   'reports/activity_summary';
$route['reports/zone-wise-booking-report']  =   'reports/zone_activity_summary';
$route['reports/maid-leave-report'] =   'reports/maid_leave_report';
$route['reports/maid-leave-report-all'] =   'reports/maid_leave_report_all';
$route['reports/maid_leave_extend'] =   'reports/maid_leave_extend';
$route['reports/maid_leave_extend_data'] =   'reports/maid_leave_extend_data';
$route['reports/customer-booking-hours'] =  'reports/customerbookinghours';
//$route['invoices']                = 'reports/invoice';
/************************************************************************ */
$route['users/add']         = 'users/add_user';
$route['users/add/(:num)']  = 'users/add_user/$1';

$route['invoice/(:num)-(:num)-(:num)/(:num)/(:num)']   = 'invoice';
$route['invoices']           = 'invoice/view_invoices';
$route['monthly-invoices']           = 'invoice/monthly_invoices';

$route['customer_statement']           = 'customer/customer_statement';
$route['rating-review']           = 'reports/rate_review';
$route['bulk-sms']           = 'customer/bulk_sms';
$route['sms-list-upload']           = 'customer/sms_list_upload';
$route['email-list-upload']           = 'customer/email_list_upload';
//$route['receivable_payments']           = 'customer/customer_receivables';

/* End of file routes.php */
/* Location: ./application/config/routes.php */

$route['offers'] = 'offers';
$route['subscriptions/add']         = 'subscriptions/add_subscription';
$route['call-connect'] = 'call_logger/call_connect';
$route['call-disconnect'] = 'call_logger/call_disconnect';

$route['complaint-report'] = 'reports/complaint_report';

$route['dashboard/data/(:any)'] = 'dashboard_data/$1';

$route['customer/get-address-by-id/(:num)'] = 'customer/get_address_by_id';


$route['invoice/list/monthly-advanced']             =   'invoice/list_customer_monthly_invoices_advanced';
$route['invoice/view/monthly-advanced/(:num)']      =   'invoice/view_invoice_monthly_advanced/$1';
$route['invoice/view/monthly-advanced/(:num)/pdf']  =   'invoice/generate_invoice_monthly_advanced/$1';
$route['invoice/update/monthly-advanced']           =   'invoice/monthly_advanced_invoice_update';
$route['customer/payment/update']                   =   'customer/customer_payment_update';
$route['customer/outstanding']                   =   'customer/customer_list_outstanding';

$route['location']                     =   'settings/location';
$route['landmark']                     =   'settings/landmark';
/************************************************************************ */
// access module
$route['keys']                                  =   'key/index';
$route['cards']                                 =   'key/cards';
$route['access/assign/list']                    =   'customer_access/assign_list';
$route['access/get_available']                  =   'customer_access/get_available_accesses';
$route['access/assign']                         =   'customer_access/assign';
$route['access/customers_search']               =   'customer_access/customers_search';
$route['access/assigned_data']                  =   'customer_access/get_assigned_data';
$route['access/return']                         =   'customer_access/return';
$route['access/assign/list/(:num)']             =   'customer_access/assign_list/$1';
$route['access/report']                         =   'customer_access/report';
$route['access/report/(:num)']                  =   'customer_access/view_report/$1';

// vehicle module
$route['vehicles']                              =   'vehicle/list_vehicles';
$route['vehicle/new']                           =   'vehicle/new_vehicle';
$route['vehicle/save']                          =   'vehicle/save_vehicle';
$route['vehicle/get_data']                      =   'vehicle/get_vehicle_data';
$route['vehicle/assign_driver']                 =   'vehicle/assign_driver';
$route['vehicle/get_drivers_for_assign']        =   'vehicle/get_drivers_for_assign';

//security pass
$route['security_pass']                         =   'security_pass';
$route['security_pass/save_pass']               =   'security_pass/save_pass';
$route['security_pass/pass_list']               =   'security_pass/pass_list';
$route['security_pass/get_pass_details']        =   'security_pass/get_pass_details';
$route['security_pass/(:any)']                  = 'security_pass/index/$1';
$route['security_pass/update_pass']             = 'security_pass/update_pass';

// customer module
$route['customer/new']                          =   'customer_mollymaid/new_customer';
$route['customer/save']                         =   'customer_mollymaid/save_customer';
$route['customer/validate_customer_code']       =   'customer_mollymaid/validate_customer_code';
$route['customer/editvalidatemobilenumber']     =   'customer_mollymaid/editvalidatemobilenumber';
$route['customer/editvalidateemail']            =   'customer_mollymaid/editvalidateemail';
$route['customer/editvalidatewhatsnum']         =   'customer_mollymaid/editvalidatewhatsnum';
$route['customer/list']                         =   'customer_mollymaid/customer_list';
$route['customer/edit/(:num)']                  =   'customer_mollymaid/edit_customer';
$route['customer/update']                       =   'customer_mollymaid/update_customer';
$route['customer/getarea']                      =   'customer_mollymaid/getarea';
$route['customer/getlocation']                  =   'customer_mollymaid/getLocations';
$route['customer/getlandmark']                  =   'customer_mollymaid/getLandmark';
$route['customer/delete_customer']              =   'customer_mollymaid/delete_customer';

//salesman module
$route['salesman']                              =   'salesman';
//customers dealers module
$route['customer-dealers']                      =   'customer_dealers';
// dispatch module
$route['dispatch/list']                         =   'dispatch/dispatch_list';
$route['dispatch/list/(:any)']                  =   'dispatch/dispatch_list';
// driver module
$route['driver/list']                           =   'driver/index';
// employee module
$route['employee/list']                         =   'maid/maid_list';
$route['employee/validatemobilenumber']         =   'employee/validatemobilenumber';
$route['employee/editvalidatemobilenumber']         =   'employee/editvalidatemobilenumber';
$route['employee/validate_adress_type_mobilenumber'] = 'employee/validate_adress_type_mobilenumber';
$route['employee/edit_validate_adress_type_mobilenumber'] = 'employee/edit_validate_adress_type_mobilenumber';
$route['employee/daily_collection']              = 'employee/daily_collection';
$route['employee/get_leader_maid']              = 'employee/get_leader_maid';

// call manageent module
$route['call-management']                         =   'call_management/call_list';
$route['call-management/getlocation']             =   'call_management/getLocations';
$route['call-management/customers_search']        =   'call_management/customers_search';
$route['call-management/getlandmark']             =   'call_management/getlandmark';
$route['call-management/customersave']            =   'call_management/customersave';
$route['call-management/get-customer-details']    =   'call_management/get_customer_details';
$route['call-management/get-customer-address-details'] =   'call_management/get_customer_address_details';

$route['call-management/call_details']            =    'call_management/call_details';
$route['call-management/save_enquiry']            =   'call_management/save_enquiry';
$route['call-management/save_complaint']          =   'call_management/save_complaint';
$route['call-management/validatemobilenumber']    =   'call_management/validatemobilenumber';
$route['call-management/validateemail']           =   'call_management/validateemail';
$route['call-management/getemployeedetails']      =   'call_management/getemployeedetails';
$route['call-management/save_preference']         =   'call_management/save_preference';
$route['call-management/save_booking']            =   'call_management/save_booking';
$route['call-management/search_booking_maid']     =   'call_management/search_booking_maid';
$route['call-management/search_booking']          =   'call_management/search_booking';
$route['call-management/permeant_booking_delete_schedule'] =  'call_management/permeant_booking_delete_schedule';
$route['call-management/schedule_delete']         =  'call_management/schedule_delete';
$route['call-management/booking_delete_one_day']  =  'call_management/booking_delete_one_day';
$route['call-management/search_re_activation_booking'] =  'call_management/search_re_activation_booking';
$route['call-management/re_activation_booking']    =  'call_management/re_activation_booking';
$route['call-management/booking_re_activate']      =  'call_management/booking_re_activate';
$route['call-management/booking_schedule_re_activate'] =  'call_management/booking_schedule_re_activate';
$route['call-management/save_feedback']                =  'call_management/save_feedback';
$route['call-management/search_maid_availability_enquiry'] =  'call_management/search_maid_availability_enquiry';
$route['call-management/save_schedule_enquiry'] =  'call_management/save_schedule_enquiry';
$route['call-management/customer_enquiry_save'] =  'call_management/customer_enquiry_save';
$route['call-management/save_schedule'] =  'call_management/save_schedule';
$route['call-management/report'] =  'call_management/call_report';
$route['call-management/report/(:any)'] =  'call_management/call_report';
$route['call_management/update_status_complaint'] =  'call_management/update_status_complaint';
$route['call-management/Enquiry_Edit/(:num)']     =   'call_management/Enquiry_Edit';
$route['call-management/update_enquiry']          =   'call_management/update_enquiry';
$route['call-management/update_enquiry_od']       =   'call_management/update_enquiry_od';
$route['call-management/suspend_date_range_schedule_od']  = 'call_management/suspend_date_range_schedule_od';
$route['call-management/suspend_date_range_schedule_we']  = 'call_management/suspend_date_range_schedule_we';
$route['call-management/suspend_one_day_schedule_od']     = 'call_management/suspend_one_day_schedule_od';
$route['call-management/suspend_one_day_schedule_we']     = 'call_management/suspend_one_day_schedule_we';
$route['call-management/cancel_permeantly_od']            = 'call_management/cancel_permeantly_od';
$route['call-management/cancel_permeantly_we']            = 'call_management/cancel_permeantly_we';
$route['call_management/update_status_enquiry']           =  'call_management/update_status_enquiry';
$route['call-management/cancel_one_day_schedule_od']     = 'call_management/cancel_one_day_schedule_od';
$route['call-management/cancel_one_day_schedule_we']     = 'call_management/cancel_one_day_schedule_we';

/// suspend schedule
$route['schedule_suspended/maid_availability']     =   'schedule_suspended/maid_availability';
$route['schedule_suspended/maid_reassign']         =   'schedule_suspended/maid_reassign';
$route['schedule_suspended/search_available_maid'] =   'schedule_suspended/search_available_maid';
$route['schedule_suspended/re_assign_schedule']    =   'schedule_suspended/re_assign_schedule';

// cancel schedule

$route['schedule_cancel/maid_availability']     =   'schedule_cancel/maid_availability';
$route['schedule_cancel/maid_reassign']         =   'schedule_cancel/maid_reassign';
$route['schedule_cancel/search_available_maid'] =   'schedule_cancel/search_available_maid';
$route['schedule_cancel/re_assign_schedule']    =   'schedule_cancel/re_assign_schedule';
