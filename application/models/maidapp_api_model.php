<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Customers_Model Class
 * 
 * @author Geethu
 * @package HM
 * @version Version 1.0
 */
class Maidapp_api_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	public function get_maid_login($username, $password) 
	{
        $this->db->select('*')
                ->from('maids')
                ->where('username', $username)
                ->where('password', $password)
                ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
	
	function update_login_status($maid_id, $logged_status) 
	{
        $fields['maid_login_status'] = $logged_status;
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $fields);

        return $this->db->affected_rows();
    }
	
	public function get_maid_detail_by_id($maid_id) 
	{
        $this->db->select("maid_id,maid_name,maid_photo_file,maid_login_status", FALSE)
                ->from('maids')
                ->where('maid_id', $maid_id)
                ->limit(1);

        $get_maid_by_id_qry = $this->db->get();

        return $get_maid_by_id_qry->row();
    }
	
	function get_booking_deletes_by_date($service_date, $service_end_date = NULL) 
	{
        $this->db->select('booking_id')
                ->from('booking_deletes');
        if ($service_end_date != NULL) {
            $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
        } else {
            $this->db->where("service_date", $service_date);
        }


        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }
	
	function get_maid_job_details($service_date, $maid_id) 
	{
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as ftime, b.time_to as ttime, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, b.cleaning_material, b.crew_in, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.payment_mode, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
                ->where('b.booking_status', 1)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
                ->order_by('b.time_from');
        if ($maid_id != 0) {
            $this->db->where('m.maid_id', $maid_id);
        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }
	
	function get_day_service_by_booking_id($date, $booking_id) 
	{
        $this->db->select('ds.day_service_id, ds.service_added_by_id,cp.paid_amount, z.zone_id, b.maid_id, ds.booking_id, ds.total_fee, ds.service_status, ds.payment_status, c.customer_id, c.payment_type')
                ->from('day_services ds')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $date)
                ->limit(1);

        $get_day_service_by_booking_id_qry = $this->db->get();
//	echo $this->db->last_query();exit;	
        return $get_day_service_by_booking_id_qry->row();
    }
	
	function get_booking_exist($booking_id, $service_date) 
	{
		$this->db->select("just_mop_ref,mop", FALSE)
				->from('justmop_ref')
				->where('service_date', $service_date)
				->where('booking_id', $booking_id);
		$get_schedule_by_date_qry = $this->db->get();
		return $get_schedule_by_date_qry->row();
	}
	
	function update_location($maid_id,$latitude,$longitude)
    {
        $fields['maid_latitude'] = $latitude;
        $fields['maid_longitude'] = $longitude;
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $fields); 

        return $this->db->affected_rows();
    }
	
	function update_device_id($maid_id,$device_id)
    {
        $fields['device_id'] = $device_id;
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $fields); 

        return $this->db->affected_rows();
    }
	
	function get_booking_by_id($booking_id)
    {
        $this->db->select("b.booking_id, b.customer_id, b.total_amount, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_end_date, b.is_locked, b.booking_note, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.time_from as from_time, b.time_to as to_time, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.booked_by, b.cleaning_material, b.discount, c.customer_name, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, c.mobile_number_1, ca.customer_address, a.area_name,m.maid_name", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->where('b.booking_id', $booking_id)
                ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();

        return $get_bookings_by_id_qry->row();
    }
	
	function add_day_service($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('day_services'); 
        return $this->db->insert_id();
    }
	
	function add_customer_payment($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('customer_payments'); 
        return $this->db->insert_id();
    }
	
	function update_day_service($day_service_id, $fields = array())
    {
        $this->db->where('day_service_id', $day_service_id);
        $this->db->update('day_services', $fields); 

        return $this->db->affected_rows();
    }
	
	function get_maid_locations()
	{
		$this->db->select('maid_id,maid_name, maid_latitude, maid_longitude')
                ->from('maids')
				->where('maid_latitude is NOT NULL', NULL, FALSE)
				->where('maid_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
	}

}
