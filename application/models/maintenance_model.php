<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Maintenance_model Class 
  * 
  * @package	Emaid
  * @author		Azinova Developers 
  * @since		Version 1.0
  */
class Maintenance_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function total_booking_count()
	{
		$this->db->select("*")
				->from("bookings")
				->where('booking_category', 'M');
				//->where('booking_status', 1);
		$qry = $this->db->get();
		return $qry->num_rows();
	}
	
	function get_schedule_by_date_counts($service_date)
	{		
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.booking_status, c.customer_name, c.mobile_number_1, z.zone_id, z.zone_name, a.area_name, ca.customer_address, u.user_fullname", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->join('users u', 'b.booked_by = u.user_id', 'left')
				//->where('b.booking_status', 1)
				->where('b.booking_category', 'M')
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where('b.service_start_date', $this->db->escape($service_date))
				->order_by('b.time_from');
		
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		
		return $get_schedule_by_date_qry->num_rows();
	}
	
	function get_schedule_hrs_by_date($service_date)
	{
		$this->db->select("b.booking_id,  DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                ->join('users u', 'b.booked_by = u.user_id', 'left')
				->where('b.booking_category', 'M')
				//->where('b.booking_status', 1)
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where('b.service_start_date', $this->db->escape($service_date))
				->order_by('b.time_from');
                
		
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		
		return $get_schedule_by_date_qry->result();
	}
	
	public function get_total_customers_counts() {

        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status, ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
		$this->db->where('c.customer_from', 'M');
        $get_active_customer_qry = $this->db->get();
        return $get_active_customer_qry->num_rows();
    }
	
	function get_user_activity()
    {
        
        $this->db->select('user_activity.*,users.user_fullname', FALSE)
                ->from('user_activity')
                ->join('users','user_activity.added_user = users.user_id','left')
				->where('users.login_type','M')
                ->order_by('user_activity.addeddate', 'desc')
                ->limit(25);
        $get_user_activity_qry = $this->db->get();
        
        return $get_user_activity_qry->result();
    }
	
	function get_maintenance_booking()
	{
		$this->db->select("b.booking_id, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, c.customer_name, ca.customer_address, st.service_type_name, b.priority_type, b.service_start_date, b.time_type, b.booking_note, b.booking_status", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->join('service_types st','b.service_type_id = st.service_type_id','left')
                                ->join('users u', 'b.booked_by = u.user_id', 'left')
				->where('b.booking_category', 'M')
				//->where('b.booking_status', 1)
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				//->where('b.service_start_date', $service_date)
				->order_by('b.service_start_date','DESC');
                
		
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		
		return $get_schedule_by_date_qry->result();
	}
	
	public function get_available_cleaning_times_maint()
	{
		$this->db->select("*")
				->from('maint_booking_slots');
		
		$get_area_qry = $this->db->get();
		
		return $get_area_qry->result();  
	}
	
	public function add_booking_times_maint($fields = array())
	{
		$this->db->insert('maint_booking_slots', $fields); 
                
		return $this->db->insert_id();
	}
	
	function delete_booking_times_maint($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('maint_booking_slots'); 
	}
}