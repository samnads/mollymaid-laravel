<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Salesman_model extends CI_Model
{

    function add_salesman($data)
    {
        $this->db->set($data);
        $this->db->insert('salesman');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_salesman()
    {
        $qr = $this->db->select('*')->from('salesman')->where('deleted_at', null)->order_by('salesman_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_salesman_details($salesman_id)
    {
        $qr = $this->db->select('*')
            ->from('salesman')
            ->where('salesman_id',$salesman_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_salesman($data,$salesman_id)
    {
        $this->db->where('salesman_id',$salesman_id);
        $this->db->update('salesman', $data);
    }

    function delete_salesman($data,$salesman_id)
    {
        $this->db->where('salesman_id',$salesman_id);
        $this->db->update('salesman', $data);
    }

}