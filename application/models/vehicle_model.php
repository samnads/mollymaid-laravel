<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Vehicle_model extends CI_Model
{
    public function vehicles_list($where)
    {
        $this->db->select('v.vehicle_id,v.driver_id,m.maid_name as driver_name,v.brand_name,v.model_name,v.model_year,v.licence_number,v.engine_number,v.chassis_number,v.fitness_start_date,v.color,v.horsepower,v.fuel_type,v.insurance_number,v.insurance_start_date,v.insurance_expiry_date,v.insurance_type,v.insurance_note,v.insurance_file_name,v.fitness_expiry_date')
            ->from('vehicles as v')
            ->join('maids as m', 'v.driver_id = m.maid_id', 'left')
            ->where($where)
            ->order_by('v.vehicle_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function save_vehicle($data)
    {
        $data['insurance_start_date'] = $data['insurance_start_date'] ? DateTime::createFromFormat('d/m/Y', $data['insurance_start_date'])->format('Y-m-d') : null;
        $data['insurance_expiry_date'] = $data['insurance_expiry_date'] ? DateTime::createFromFormat('d/m/Y', $data['insurance_expiry_date'])->format('Y-m-d') : null;
        $data['fitness_start_date'] = $data['fitness_start_date'] ? DateTime::createFromFormat('d/m/Y', $data['fitness_start_date'])->format('Y-m-d') : null;
        $data['fitness_expiry_date'] = $data['fitness_expiry_date'] ? DateTime::createFromFormat('d/m/Y', $data['fitness_expiry_date'])->format('Y-m-d') : null;
        $this->db->insert('vehicles', $data);
        return ($this->db->insert_id() > 0);
    }
    public function update_vehicle($vehicle_id, $data)
    {
        $this->db->where('vehicle_id', $vehicle_id);
        $this->db->where('deleted_at', null);
        $this->db->update('vehicles', $data);
        return ($this->db->affected_rows() >= 0);
    }
    public function get_vehicle_data_by_id($id)
    {
        $this->db->select('v.vehicle_id,v.driver_id,m.maid_name as driver_name,v.brand_name,v.model_name,v.model_year,v.licence_number,v.engine_number,v.chassis_number,v.fitness_start_date,v.color,v.horsepower,v.fuel_type,v.insurance_number,v.insurance_start_date,v.insurance_expiry_date,v.insurance_type,v.insurance_note,v.insurance_file_name,v.fitness_expiry_date')
            ->from('vehicles as v')
            ->join('maids as m', 'v.driver_id = m.maid_id', 'left')
            ->where('v.vehicle_id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    public function driver_assign($id, $data)
    {
        $this->db->where('vehicle_id', $id);
        $this->db->where('deleted_at', null);
        $this->db->update('vehicles', $data);
    }
}
