<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/** 
 * Maids_model Class 
 * 
 * @package	HM
 * @author     Geethu
 * @since	Version 1.0
 */
class Maids_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    /** 
     * Get maid attandence by date
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	int, str
     * @return	array
     */
    function get_maid_attandence_by_date($maid_id, $date)
    {
        $this->db->select('attandence_id, maid_id, zone_id, tablet_id, date, maid_in_time, maid_out_time, attandence_status')
            ->from('maid_attandence')
            ->where('maid_id', $maid_id)
            ->where('date', $date)
            ->order_by('maid_in_time', 'desc')
            ->limit(1);

        $get_maid_attandence_by_date_qry = $this->db->get();
        //	echo $this->db->last_query();	
        return $get_maid_attandence_by_date_qry->row();
    }
    /** 
     * Get maids
     * 
     * @author	Geethu
     * @acces	public 
     * @param	bool
     * @return	array
     */
    //function get_maids($active_only = TRUE,$team_id = NULL)
    function get_maids()
    {
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        //$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status,t.team_name')
        $this->db->select('m.maid_id, m.odoo_package_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_package_flat_id, m.maid_status, m.odoo_package_maid_status')
            ->from('maids as m')
            ->join('flats as f', 'm.flat_id = f.flat_id', 'left');
        //->join('teams t','m.team_id = t.team_id','left')
        $this->db->where('maid_status', 1);
        $this->db->where('employee_type_id', 1);
        /******************************************************************** */
        // settings based ordering
        if ($settings->maid_listing_priority_type == "lifo") {
            $this->db->order_by('m.maid_id', 'desc');
        } else if ($settings->maid_listing_priority_type == "fifo") {
            $this->db->order_by('m.maid_id', 'asc');
        } else if ($settings->maid_listing_priority_type == "priority_number_asc") {
            $this->db->order_by('(-m.maid_priority)', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "priority_number_desc") {
            $this->db->order_by('m.maid_priority', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "maid_name_asc") {
            $this->db->order_by('m.maid_name', 'asc');
        } else if ($settings->maid_listing_priority_type == "maid_name_desc") {
            $this->db->order_by('m.maid_name', 'desc');
        } else {
            $this->db->order_by('m.maid_name', 'asc');
        }
        /******************************************************************** */
        /*if($team_id != NULL){
            $this->db->where('m.team_id', $team_id);
            }*/
        $query = $this->db->get();
        return $query->result();
    }
    function get_all_staffs()
    {
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        //$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status,t.team_name')
        $this->db->select('m.maid_id, m.odoo_package_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_package_flat_id, m.maid_status, m.odoo_package_maid_status')
            ->from('maids as m')
            ->join('flats as f', 'm.flat_id = f.flat_id', 'left');
        //->join('teams t','m.team_id = t.team_id','left')
        $this->db->where('maid_status', 1);
        // $this->db->where('employee_type_id', 1);
        /******************************************************************** */
        // settings based ordering
        if ($settings->maid_listing_priority_type == "lifo") {
            $this->db->order_by('m.maid_id', 'desc');
        } else if ($settings->maid_listing_priority_type == "fifo") {
            $this->db->order_by('m.maid_id', 'asc');
        } else if ($settings->maid_listing_priority_type == "priority_number_asc") {
            $this->db->order_by('(-m.maid_priority)', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "priority_number_desc") {
            $this->db->order_by('m.maid_priority', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "maid_name_asc") {
            $this->db->order_by('m.maid_name', 'asc');
        } else if ($settings->maid_listing_priority_type == "maid_name_desc") {
            $this->db->order_by('m.maid_name', 'desc');
        } else {
            $this->db->order_by('m.maid_name', 'asc');
        }
        /******************************************************************** */
        /*if($team_id != NULL){
            $this->db->where('m.team_id', $team_id);
            }*/
        $query = $this->db->get();
        return $query->result();
    }
    function get_maids_except_maids($maid_ids)
    {
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        $this->db->select('m.maid_id,m.maid_name,m.maid_nationality')
            ->from('maids as m')
            ->where('m.maid_status', 1)
            ->where_not_in('m.maid_id', $maid_ids ?: '[]');
        /******************************************************************** */
        // settings based ordering
        if ($settings->maid_listing_priority_type == "lifo") {
            $this->db->order_by('m.maid_id', 'desc');
        } else if ($settings->maid_listing_priority_type == "fifo") {
            $this->db->order_by('m.maid_id', 'asc');
        } else if ($settings->maid_listing_priority_type == "priority_number_asc") {
            $this->db->order_by('(-m.maid_priority)', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "priority_number_desc") {
            $this->db->order_by('m.maid_priority', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "maid_name_asc") {
            $this->db->order_by('m.maid_name', 'asc');
        } else if ($settings->maid_listing_priority_type == "maid_name_desc") {
            $this->db->order_by('m.maid_name', 'desc');
        } else {
            $this->db->order_by('m.maid_name', 'asc');
        }
        /******************************************************************** */
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_full_maids()
    {

        $this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
            ->from('maids m')
            ->join('flats f', 'm.flat_id = f.flat_id', 'left');

        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
    }

    function get_total_maids_counts()
    {
        $this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
            ->from('maids m')
            ->join('flats f', 'm.flat_id = f.flat_id', 'left')
            ->order_by('maid_name'); //maid_added_datetime
        $this->db->where('maid_status', 1);

        $get_maids_qry = $this->db->get();
        //echo $this->db->query();exit();
        return $get_maids_qry->num_rows();
    }

    function get_maid_services_by_maid_id($maid_id)
    {
        $this->db->select('s.odoo_service_id')
            ->from('service_types s')
            ->join('maid_service_type_joins m', 's.service_type_id = m.service_type_id')
            ->where('m.maid_id', $maid_id); //maid_added_datetime			

        $get_maids_qry = $this->db->get();
        //echo $this->db->query();exit();
        return $get_maids_qry->result();
    }
    /** 
     * Add maids
     * 
     * @author	Betsy
     * @acces	public 
     * @param	
     * @return	array
     */
    function add_maids($data)
    {
        $this->db->set($data);
        $this->db->insert('maids');
        $result = $this->db->insert_id();
        return $result;
    }
    /** 
     * Update Attachments
     * 
     * @author	Betsy
     * @acces	public 
     * @param	
     * @return	
     */
    function update_attachments($datas, $id)
    {
        $this->db->where('maid_id', $id);
        $this->db->update('maids', $datas);
    }
    function update_maid_services($datas, $id)
    {
        $this->db->where('maid_service_type_jon_id', $id);
        $this->db->update('maid_service_type_joins', $datas);
    }
    function update_services($dat, $id)
    {
        $this->db->set($dat);
        $this->db->insert('maid_service_type_joins');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_all_maids($active = 2)
    {
        $this->db->select('maids.*, flats.flat_name')
            ->from('maids')
            ->join('flats', 'maids.flat_id = flats.flat_id', 'left');
        //->order_by('maids.maid_added_datetime');
        if ($active == 2) {
            $this->db->where('maids.maid_status', 1);
        } else if ($active == 3) {
            $this->db->where('maids.maid_status', 0);
        } else if ($active == 4) {
            $this->db->where('maids.maid_status', 2);
        }
        $this->db->order_by('maids.maid_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function delete_maid($maid_id)
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->delete('maids');
    }
    function delete_maid_services($maid_id)
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->delete('maid_service_type_joins');
    }
    function get_maid_details($maid_id)
    {
        $this->db->select('maids.*, flats.flat_name,genders.gender,teams.team_name,employee_types.employee_type,countries.country')
            ->from('maids')
            ->join('flats', 'maids.flat_id = flats.flat_id', 'left')
            ->join('genders', 'maids.gender_id = genders.gender_id', 'left')
            ->join('teams', 'maids.team_id = teams.team_id', 'left')
            ->join('countries', 'maids.country_id=countries.id', 'left')
            ->join('employee_types', 'employee_types.employee_type_id = maids.employee_type_id', 'left')
            ->where('maids.maid_id', $maid_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_maid_addresses($maid_id)
    {
        //     $this->db->select('employee_addresses.street,employee_addresses.town,
        //     employee_addresses.city,employee_addresses.house_number,employee_addresses.relationship,employee_addresses.mobile_no_1 AS `mobile`,employee_addresses.whatsapp_no_1 AS `whatsapp`,
        //     address_types.address_type,countries.country,country_emirates.emirate')
        //     ->from('employee_addresses')
        //     ->join('address_types', 'employee_addresses.address_type_id=address_types.address_type_id', 'left')
        //     ->join('countries', 'employee_addresses.country_id=countries.id', 'left')
        //     ->join('country_emirates', 'employee_addresses.country_id = country_emirates.country_id', 'left')
        //     ->where('employee_addresses.maid_id', $maid_id)
        //     ->group_by('address_types.address_type_id')
        //     ->order_by('employee_addresses.address_type_id', 'ASC'); 

        // $query = $this->db->get();
        // return $query->result_array();
        $this->db->select('employee_addresses.address_type_id,
        MAX(employee_addresses.street) AS street,
        MAX(employee_addresses.town) AS town,
        MAX(employee_addresses.city) AS city,
        MAX(employee_addresses.house_number) AS house_number,
        MAX(employee_addresses.relationship) AS relationship,
        MAX(employee_addresses.mobile_no_1) AS mobile,
        MAX(employee_addresses.whatsapp_no_1) AS whatsapp,
        MAX(address_types.address_type) AS address_type,
        MAX(countries.country) AS country,
        MAX(country_emirates.emirate) AS emirate')
        ->from('employee_addresses')
        ->join('address_types', 'employee_addresses.address_type_id=address_types.address_type_id', 'left')
        ->join('countries', 'employee_addresses.country_id=countries.id', 'left')
        ->join('country_emirates', 'employee_addresses.country_id = country_emirates.country_id', 'left')
        ->where('employee_addresses.maid_id', $maid_id)
        ->group_by('employee_addresses.address_type_id')
        ->order_by('employee_addresses.address_type_id', 'ASC'); 

        $query = $this->db->get();
        return $query->result_array();

    }
    function get_maid_skills($maid_id)
    {
        $this->db->select('maids.*, employee_skills.employee_id,employee_skills.skill_id,employee_skills.rating_level_id,
         skills.skill, r.rating_level')
            ->from('maids')
            ->join('employee_skills', 'employee_skills.employee_id = maids.maid_id', 'left')
            ->join('skills', 'skills.skill_id = employee_skills.skill_id', 'left')
            ->join('rating_level as r', 'r.rating_level_id = employee_skills.rating_level_id', 'left')
            ->where('maids.maid_id', $maid_id);
        $query = $this->db->get();

        if ($query) {
            return $query->result_array();
        } else {
            return array();
        }
    }
    function get_maid_availability($maid_id)
    {

        $this->db->select('maid_availability.*,locations.*,w.week_name,areas.area_name,zones.zone_name')
            ->from('maid_availability')   
            ->join('zones','zones.zone_id=maid_availability.zone_id','left')
            ->join('areas','areas.zone_id=zones.zone_id','left')
            ->join('locations', 'locations.location_id = maid_availability.location_id', 'left')
            ->join('week_days as w', 'w.week_day_id = maid_availability.week_day', 'left')
            ->where('employee_id', $maid_id);
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        } else {
            return array();
        }
    }
    function get_maid_documents($maid_id)
    {
        $this->db->select('employee_documents.*,document_types.*')
            ->from('document_types')
            ->join('employee_documents', 'document_types.document_type_id = employee_documents.document_type_id', 'left')
            ->where('employee_id', $maid_id);
        $query = $this->db->get();
        if ($query) {
            return $query->result_array();
        } else {
            return array();
        }
    }
    function update_maids($data, $maid_id)
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $data);
    }
    function get_maid_services($maid_id)
    {
        $qr = $this->db->select('maid_service_type_joins.service_type_id')
            ->from('maid_service_type_joins')
            ->where('maid_service_type_joins.maid_id', $maid_id);
        $query = $this->db->get();
        return $query->result();
    }
    function disable_status($maid_id, $data)
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $data);
    }
    function activate_status($maid_id, $data)
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $data);
    }
    function maid_services($maid_id)
    {
        $qr = $this->db->select('maid_service_type_joins.maid_service_type_jon_id, maid_service_type_joins.service_type_id, service_types.service_type_name, service_types.odoo_service_id')
            ->from('maid_service_type_joins')
            ->join('service_types', 'maid_service_type_joins.service_type_id = service_types.service_type_id')
            ->where('maid_service_type_joins.maid_id', $maid_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /** 
     * Get maid by id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_maid_by_id($maid_id)
    {
        $this->db->select("maid_id, maid_name, maid_gender, maid_nationality, maid_present_address, maid_permanent_address, maid_mobile_1, maid_mobile_2, flat_id, maid_photo_file, maid_passport_number, maid_passport_expiry_date, maid_passport_file, maid_visa_number, maid_visa_expiry_date, maid_visa_file, maid_labour_card_number, maid_labour_card_expiry_date, maid_labour_card_file, maid_notes, odoo_maid_id, odoo_synch_status, odoo_new_maid_id, odoo_new_maid_status, maid_status, maid_added_datetime,maid_device_token as device_id,odoo_package_maid_status,odoo_package_maid_id", FALSE)
            ->from('maids')
            ->where('maid_id', $maid_id)
            ->limit(1);

        $get_maid_by_id_qry = $this->db->get();

        return $get_maid_by_id_qry->row();
    }
    /**
     * Get Maid Attendance by Date
     * 
     * @author Geethu
     * @access public
     * @param date $date search date
     * @return array 
     */

    function get_maid_attendance_by_date($date)
    {
        $this->db->select("m.maid_name, z.zone_name, a.attandence_status,a.maid_in_time, a.maid_out_time", FALSE)
            ->from('maid_attandence a')
            ->join('maids m', 'a.maid_id = m.maid_id')
            ->join('tablets t', 'a.tablet_id = t.tablet_id')
            ->join('zones z', 't.zone_id = z.zone_id')
            ->where('a.attandence_id IN (SELECT MAX(attandence_id) FROM maid_attandence WHERE DATE(date) = "' . $date . '" GROUP BY tablet_id, maid_id)')
            ->order_by('m.maid_name');

        $get_maid_attendance_by_date_qry = $this->db->get();

        return $get_maid_attendance_by_date_qry->result();
    }
    function get_maid_vehicle_report($date = NULL)
    {
        $date = $date === NULL ? date('Y-m-d') : $date;

        $get_maid_vehicle_report =  $this->db->query("SELECT ma.maid_id, m.maid_name, t.zone_id, z.zone_name, ma.attendance_status
                            FROM  maid_barcode_attendance ma
                            JOIN tablets t ON ma.tablet_id = t.tablet_id
                            JOIN zones z ON t.zone_id = z.zone_id
                            JOIN maids m ON ma.maid_id = m.maid_id
                            WHERE DATE( ma.addeddate ) =  '$date'
                            AND ma.id
                            IN (

                            SELECT MAX( id ) 
                            FROM maid_barcode_attendance
                            WHERE maid_id = ma.maid_id
                            AND tablet_id = ma.tablet_id
                            AND DATE( addeddate ) =  '$date'
                            )
                            GROUP BY ma.tablet_id, ma.maid_id");

        return $get_maid_vehicle_report->result();
    }

    /* Betsy Bernard */

    function add_maid_leave($fields = array())
    {



        $this->db->select("maid_leave.*", FALSE)
            ->from('maid_leave')
            ->where('maid_leave.maid_id', $fields['maid_id'])
            ->where('maid_leave.leave_date', $fields['leave_date'])
            ->where('maid_leave.leave_status', 0)
            ->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = array(
                'leave_status' => 1,
                'added_by' => $fields['added_by'],
                'leave_type' => $fields['leave_type'],
                'typeleaves' => $fields['typeleaves']
            );
            $this->db->where('leave_id', $query->row()->leave_id);
            $this->db->update('maid_leave', $data);
            $this->add_activity($fields['maid_id'], 5);
            return $query->row()->leave_id;
        } else {
            $fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');
            $fields['added_ip_address'] = isset($fields['added_ip_address']) ? $fields['added_ip_address'] : $_SERVER['REMOTE_ADDR'];

            $this->db->set($fields);
            $this->db->insert('maid_leave');
            $this->add_activity($fields['maid_id'], 5);
            return $this->db->insert_id();
        }
    }

    function check_maid_leave_by_date($maid_id, $start_date, $end_date)
    {
        $this->db->select("maid_leave.*", FALSE)
            ->from('maid_leave')
            ->where('maid_leave.maid_id', $maid_id)
            ->where("((maid_leave.leave_date BETWEEN " . $this->db->escape($start_date) . " and " . $this->db->escape($end_date) . " AND leave_status = 1))", NULL, FALSE);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    function get_maid_leave_suspend($maid_id, $start_date, $end_date)
    {
        $this->db->select('maid_leave.*')
            ->from('maid_leave')
            ->where('maid_leave.maid_id', $maid_id)
            ->where("((maid_leave.leave_date BETWEEN " . $this->db->escape($start_date) . " and " . $this->db->escape($end_date) . " AND leave_status = 1))", NULL, FALSE);

        $query = $this->db->get();

        return $query->result();
    }
    function get_maid_leaves($maid_id, $start_date)
    {
        $this->db->select('maid_leave.*')
            ->from('maid_leave')
            ->where('maid_leave.maid_id', $maid_id)
          ->where('leave_date >=', $start_date)
         ->where('leave_status', 1);
        $query = $this->db->get();

        return $query->result();
    }
    public function get_existing_leaves($maid_id, $start_date, $end_date)
    {
        $this->db->select('maid_leave.*');
        $this->db->from('maid_leave'); // Adjust table name as necessary
        $this->db->where('maid_id', $maid_id);
        $this->db->where('leave_date >=', $start_date);
        $this->db->where('leave_date <=', $end_date);
        $this->db->where('leave_status', 1);

        $query = $this->db->get();
        return $query->result();
    }
    public function update_maid_leave_suspend($maid_id, $leave_date, $fields = array())
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->where('leave_date', $leave_date);
        $this->db->update('maid_leave', $fields); // Adjust table name as necessary
    }
    function get_maid_leave($leave_date)
    {
        $this->db->select('maid_leave.*, maids.*, users.user_fullname')
            ->from('maid_leave')
            ->join('maids', 'maids.maid_id = maid_leave.maid_id')
            ->join('users', 'users.user_id = maid_leave.added_by')
            ->where('maid_leave.leave_date', $leave_date)
            ->where('maid_leave.leave_status', 1);

        $query = $this->db->get();

        return $query->result();
    }
    function get_maid_hours($date, $date_to)
    {
        $this->db->select('m.maid_name,SEC_TO_TIME(Sum(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from)))) AS hours', FALSE)
            ->from('maids m')
            ->join('day_services d', 'm.maid_id = d.maid_id')
            ->join('bookings b', 'b.booking_id = d.booking_id')
            //->where('m.maid_status', 1)
            ->where('d.service_date >=', "'$date'", FALSE)
            ->where('d.service_date <=', "'$date_to'", FALSE)
            ->where('d.service_status !=', "3")
            ->group_by('m.maid_name')
            ->order_by('m.maid_name', 'ASC');

        $query = $this->db->get();

        return $query->result();
    }
    function update_maid_leave($fields = array())
    {
        $fields['last_updated_ip_address'] = isset($fields['last_updated_ip_address']) ? $fields['last_updated_ip_address'] : $_SERVER['REMOTE_ADDR'];
        $fields['last_updated_date_time'] = isset($fields['last_updated_date_time']) ? $fields['last_updated_date_time'] : date('Y-m-d H:i:s');

        $this->db->where('leave_id', $fields['leave_id']);
        $this->db->update('maid_leave', $fields);
    }
    /*/ Betsy Bernard /*/


    /*
         * Maid Leave
         * @auther : Geethu
         */
    function get_maids_leave_by_date($service_date, $maid_id = NULL)
    {
        $this->db->select('maid_id,typeleaves,leave_date')
            ->from('maid_leave')
            ->where('leave_date', $service_date)
            ->where('leave_status', 1);

        if ($maid_id) {
            $this->db->where('maid_id', $maid_id);
        }

        $get_maids_leave_by_date_qry = $this->db->get();

        return $get_maids_leave_by_date_qry->result();
    }

    function get_maids_leave_by_date_we($filter_start_date, $filter_end_date, $maid_id = NULL)
    {
        $this->db->select('maid_id,typeleaves,leave_date')
            ->from('maid_leave')
            // ->where('leave_date', $service_date)
            ->where("DATE(leave_date) BETWEEN '$filter_start_date' AND '$filter_end_date'") 
            ->where('leave_status', 1);

        if ($maid_id) {
            $this->db->where('maid_id', $maid_id);
        }

        $get_maids_leave_by_date_qry = $this->db->get();

        return $get_maids_leave_by_date_qry->result();
    }


    function list_maids_leave_by_date($service_date)
    {
        $this->db->select('ml.maid_id,m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
            ->from('maid_leave ml')
            ->join('maids m', 'm.maid_id = ml.maid_id')
            ->join('flats f', 'm.flat_id = f.flat_id', 'left')
            ->where('ml.leave_date', $service_date)
            ->where('ml.leave_status', 1)
            ->order_by('m.maid_name', 'asc');
        $get_maids_leave_by_date_qry = $this->db->get();

        return $get_maids_leave_by_date_qry->result();
    }

    /*
         * Maid Booking module from customer page
         * @author : Jiby
         */
    function get_all_maid($filter = NULL)
    {

        if (isset($filter)) {
            $location_type  = $filter['location_type'];
            $value          = $filter['location_val'];
            $customer_id    = $filter['customer_id'];
            $area_id        = $filter['area_id'];
            $zone_id        = $filter['zone_id'];
            $province_id    = $filter['province_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];

            $service_week_day = date('w', strtotime($service_date));
            $time_from =  date('H:i:s', trim($from_time));
            $time_to = date('H:i:s', trim($to_time));
        }

        if ($location_type == 'area') {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where('a.area_id', $area_id);
        } else if ($location_type == 'zone') {
            $areaArray  = array();
            $subQuery   = "SELECT asq.area_id FROM `areas` as asq WHERE `zone_id` = $zone_id";
            $querySub   = $this->db->query($subQuery);
            foreach ($querySub->result_array() as $row) {
                $areaArray[] = $row['area_id'];
            }

            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            $this->db->join('zones as z', 'z.zone_id = a.zone_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where_in('a.area_id', $areaArray);
            //                 $this->db->where('z.zone_id', $value);
        } else if ($location_type == 'province') {
            $areaArray  = array();
            $subQuery   = "SELECT asq.area_id FROM `areas` as asq LEFT JOIN `zones` as zsq ON `zsq`.`zone_id` = `asq`.`zone_id` WHERE `zsq`.`province_id` = $province_id";
            $querySub   = $this->db->query($subQuery);
            foreach ($querySub->result_array() as $row) {
                $areaArray[] = $row['area_id'];
            }
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            //$this->db->join('province as p', 'p.province_id = z.province_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            $this->db->join('zones as z', 'z.zone_id = a.zone_id');
            $this->db->join('province as p', 'p.province_id = z.province_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where_in('a.area_id', $areaArray);
            //                 $this->db->where('p.province_id', $value);
        } else {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
            $this->db->from('maids m');
        }
        $this->db->where('maid_status', 1);
        $this->db->group_by('m.maid_id');
        $this->db->order_by('maid_name', 'ASC');
        //  ->limit(10);


        $get_maids_leave_by_date_qry = $this->db->get();
        //echo $this->db->last_query();
        return $get_maids_leave_by_date_qry->result();
    }


    function get_maid_leave_report($data)
    {
        $maid_id = $data['maid_id'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $leave_type = $data['leave_type'];
        $this->db->select("l.*,m.maid_name", FALSE)
            ->from('maid_leave l');
        $this->db->join('maids m', 'm.maid_id = l.maid_id')

            ->where("((l.leave_date BETWEEN " . $this->db->escape($start_date) . " and " . $this->db->escape($end_date) . " AND l.leave_status = 1))", NULL, FALSE);
        if ($maid_id) {
            $this->db->where('l.maid_id', $maid_id);
        }
        $this->db->order_by('l.leave_date', 'DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();
        $results = $query->result();
        // print_r($results);

        return $results;
    }

    function delete_leavereport($leave_id, $status)
    {
        $this->db->set('leave_status', $status);
        $this->db->where('leave_id', $leave_id);
        $this->db->update('maid_leave');
    }

    function get_maid_leave_reportresults($data)
    {
        $maid_id = $data['maid_id'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $leave_type = $data['leave_type'];
        $this->db->select('m.maid_name,SUM( IF( l.leave_type = "1", 1, 0 ) ) AS fullday,SUM( IF( l.leave_type = "2", 1, 0 ) ) AS halfday,leave_date', FALSE)
            ->from('maid_leave l');
        $this->db->join('maids m', 'm.maid_id = l.maid_id');
        $this->db->where("((l.leave_date BETWEEN " . $this->db->escape($start_date) . " and " . $this->db->escape($end_date) . " AND l.leave_status = 1))", NULL, FALSE);
        if ($maid_id) {
            $this->db->where('l.maid_id', $maid_id);
        }
        $this->db->group_by('l.maid_id');
        $query = $this->db->get();
        if ($query) {
            $result = $query->result();
            return $result;
        } else {
            return false;
        }
        // $result = $query->result();
        // return $result;
    }

    function add_maid_attandence($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('maid_attandence');
        return $this->db->insert_id();
    }

    function update_maid_attandence($attandence_id, $fields = array())
    {
        $this->db->where('attandence_id', $attandence_id);
        $this->db->update('maid_attandence', $fields);

        return $this->db->affected_rows();
    }
    function add_activity($maid_id, $action_type = NULL)
    {
        $maid = $this->get_maid_by_id($maid_id);
        $action = array("0" => "disabled", "1" => "enabled", "2" => "deleted", "3" => "added", "4" => "edited", "5" => "marked_leave");
        if ($action_type == 3) {
            $content_activity = "New staff " . $maid->maid_name . " is " . " " . $action[$action_type] . " " . "by Admin user";
        } else if ($action_type == 5) {
            $content_activity = "Staff " . $maid->maid_name . " has been marked leave by Admin user";
        } else {
            $content_activity = "Staff " . $maid->maid_name . " is " . " " . $action[$action_type] . " " . "by Admin user";
        }
        $data_activity = array(
            'added_user' => user_authenticate(),
            'action_type' => "Maid_" . $action[$action_type],
            'action_content' => $content_activity,
            'addeddate' => date('Y-m-d H:i:s'),
        );

        $this->db->set($data_activity);
        $this->db->insert('user_activity');
        $result = $this->db->insert_id();
        return $result;
    }

    function get_all_maid_new($filter = NULL)
    {
        if (isset($filter)) {
            $customer_id    = $filter['customer_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];

            $service_week_day = date('w', strtotime($service_date));
            $time_from =  date('H:i:s', trim($from_time));
            $time_to = date('H:i:s', trim($to_time));
        }
        $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
        $this->db->from('maids m');

        $this->db->where('maid_status', 1);
        $this->db->group_by('m.maid_id');
        $this->db->order_by('maid_name', 'ASC');
        $get_maids_leave_by_date_qry = $this->db->get();
        //echo $this->db->last_query();
        return $get_maids_leave_by_date_qry->result();
    }

    function get_maids_odoo()
    {
        $this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, m.maid_status, m.odoo_synch_status')
            ->from('maids m')
            ->order_by('m.maid_name', 'asc');
        $this->db->where('m.odoo_synch_status', 0);
        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
    }

    function list_inactivemaids()
    {
        $this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
            ->from('maids m')
            ->join('flats f', 'm.flat_id = f.flat_id', 'left')
            ->order_by('m.maid_name', 'asc');
        $this->db->where('maid_status', 0);
        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
    }

    function get_maids_for_odoo()
    {
        $this->db->select('m.maid_id, m.odoo_new_maid_id, m.maid_name, m.maid_full_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_new_flat_id, m.maid_status, m.odoo_new_maid_status')
            ->from('maids m')
            ->where('m.odoo_new_maid_status', 0)
            ->join('flats f', 'm.flat_id = f.flat_id', 'left')
            ->order_by('m.maid_name');
        $this->db->where('m.maid_status', 1);
        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
    }

    function get_maids_for_odoo_package()
    {
        $this->db->select('m.maid_id, m.odoo_package_maid_id, m.maid_name, m.maid_full_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_package_flat_id, m.maid_status, m.odoo_package_maid_status')
            ->from('maids m')
            ->where('m.odoo_package_maid_status', 0)
            ->join('flats f', 'm.flat_id = f.flat_id', 'left')
            ->order_by('m.maid_name');
        $this->db->where('m.maid_status', 1);
        $get_maids_qry = $this->db->get();
        return $get_maids_qry->result();
    }

    function get_maid_leave_report_new($data)
    {
        $maid_id = $data['maid_id'];
        $start_date = $data['start_date'];
        // echo $start_date; die;
        $end_date = $data['end_date'];
        $leave_type = $data['leave_type'];
        $this->db->select("l.*,m.maid_name,users.username,users.user_role", FALSE)
            ->from('maid_leave l')
            ->join('maids m', 'm.maid_id = l.maid_id')
            ->join('users', 'users.user_id = l.added_by')


            ->where("((l.leave_date BETWEEN " . $this->db->escape($start_date) . " and " . $this->db->escape($end_date) . " AND l.leave_status = 1))", NULL, FALSE);
        if ($maid_id) {
            $this->db->where('l.maid_id', $maid_id);
        }
        $this->db->order_by('l.leave_date', 'DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $results = $query->result();
        // print_r($results);

        return $results;
    }

    function get_maid_leave_reportresults_new($data)
    {
        $maid_id = $data['maid_id'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];
        $leave_type = $data['leave_type'];
        $this->db->select('m.maid_name,users.username,SUM( IF( l.leave_type = "1", 1, 0 ) ) AS fullday,SUM( IF( l.leave_type = "2", 1, 0 ) ) AS halfday,leave_date', FALSE)
            ->from('maid_leave l');
        $this->db->join('maids m', 'm.maid_id = l.maid_id');
        $this->db->join('users', 'users.user_id = l.added_by');
        $this->db->where("((l.leave_date BETWEEN " . $this->db->escape($start_date) . " and " . $this->db->escape($end_date) . " AND l.leave_status = 1))", NULL, FALSE);
        if ($maid_id) {
            $this->db->where('l.maid_id', $maid_id);
        }
        $this->db->group_by('l.maid_id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function get_custmer_maid_hours($date, $date_to)
    {
        //$date = '2021-11-05';
        //$date_to = '2023-02-14';
        $this->db->select('c.customer_name,c.customer_source,s.service_type_name,ROUND(SUM(TIME_TO_SEC(TIMEDIFF(d.end_time, d.start_time)))/3600,0) AS hours,SUM(b.total_amount) as netamount,COUNT(d.customer_id) as netcount,d.service_added_datetime,c.is_flag,ds.service_date', false)
            ->from('day_services d')
            ->join('bookings b', 'b.booking_id = d.booking_id', 'left')
            ->join('customers c', 'd.customer_id = c.customer_id', 'left')
            ->join('service_types s', 'b.service_type_id = s.service_type_id', 'left')
            ->join('(SELECT customer_id, MAX((day_service_id) ) AS max_id FROM day_services GROUP BY customer_id) AS ds_max', 'd.customer_id = ds_max.customer_id', 'left')
            ->join('day_services ds', 'ds_max.max_id = ds.day_service_id', 'left')
            ->where('d.service_date >=', "'$date'", false)
            ->where('d.service_date <=', "'$date_to'", false)
            ->where('d.service_status !=', "3")
            ->group_by('d.customer_id')
            ->group_by('b.service_type_id')
            ->order_by('hours', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function get_all_cleaners()
    {
        $this->db->select('maid_id, maid_name')
            ->from('maids')
            ->where('maid_status', 1);
        $get_maid_by_qry = $this->db->get();
        return $get_maid_by_qry->result();
    }
    function get_ongoing_bookings_on_time($service_date, $time_from, $time_to, $include_paused = false)
    {
        // in development mode
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $service_week_day    =    date('N', strtotime($service_date));
        //$this->db->select('m.maid_name')
        $this->db->select('b.booking_id,b.booking_type,b.time_from,b.time_to,m.maid_id,m.maid_name,m.maid_gender,m.maid_nationality,m.maid_priority,bd.booking_delete_id')
            ->from('bookings as b')
            ->join('maids as m', 'm.maid_id = b.maid_id')
            ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id', 'left')
            ->where('m.maid_status', 1)
            ->where('b.booking_status =', 1)
            /***************************** */
            ->where('b.service_week_day', $service_week_day);
        if ($include_paused == false) {
            $this->db->where_not_in('b.booking_id', $today_deleted_booking_ids); // uncomment to except paused or one day deleted
        }
        $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))")
            ->where("(
        (b.time_from = " . $this->db->escape($time_from) . ") OR
        (b.time_from = " . $this->db->escape($time_from) . " AND b.time_to = " . $this->db->escape($time_to) . ") OR
        (b.time_from > " . $this->db->escape($time_from) . " AND b.time_to <= " . $this->db->escape($time_to) . ") OR
        (b.time_from < " . $this->db->escape($time_from) . " AND b.time_to = " . $this->db->escape($time_to) . ") OR
        (b.time_from < " . $this->db->escape($time_to)   . " AND b.time_to > " . $this->db->escape($time_to) . ") OR
        (b.time_from < " . $this->db->escape($time_from) . " AND b.time_to > " . $this->db->escape($time_from) . ") OR
        (b.time_from < " . $this->db->escape($time_from) . " AND b.time_to > " . $this->db->escape($time_to) . "))");
        /******************************************************************** *
        // settings based ordering
        if ($settings->maid_listing_priority_type == "lifo") {
            $this->db->order_by('m.maid_id', 'desc');
        } else if ($settings->maid_listing_priority_type == "fifo") {
            $this->db->order_by('m.maid_id', 'asc');
        } else if ($settings->maid_listing_priority_type == "priority_number_asc") {
            $this->db->order_by('(-m.maid_priority)', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "priority_number_desc") {
            $this->db->order_by('m.maid_priority', 'desc', false);
            $this->db->order_by('m.maid_name', 'asc', false);
        } else if ($settings->maid_listing_priority_type == "maid_name_asc") {
            $this->db->order_by('m.maid_name', 'asc');
        } else if ($settings->maid_listing_priority_type == "maid_name_desc") {
            $this->db->order_by('m.maid_name', 'desc');
        } else {
            $this->db->order_by('m.maid_name', 'asc');
        }
        /******************************************************************** */
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->result();
    }
    public function get_booking_deletes_by_date($service_date)
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->where('bd.service_date', $service_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }

    function add_maids_address($localAddress, $permanentAddress, $emergencylocalAddress, $emergencyhomeAddress)
    {
        // $this->db->set($localAddress);
        // $this->db->insert('employee_addresses');
        // $result = $this->db->insert_id();
        // return $result;

        $this->db->insert('employee_addresses', $localAddress);
        $localAddressId = $this->db->insert_id();

        $this->db->insert('employee_addresses', $permanentAddress);
        $permanentAddressId = $this->db->insert_id();

        $this->db->insert('employee_addresses', $emergencylocalAddress);
        $emergencylocalAddressId = $this->db->insert_id();

        $this->db->insert('employee_addresses', $emergencyhomeAddress);
        $$emergencyhomeAddressId = $this->db->insert_id();

        return array(
            'local_address_id' => $localAddressId,
            'permanent_address_id' => $permanentAddressId,
            'emergency_local_Address_id' => $emergencylocalAddressId,
            'emergency_home_Address_id' => $$emergencyhomeAddress
        );
    }


    function get_maid_address_local($maid_id)
    {
        $this->db->select('employee_addresses.*')
            ->from('employee_addresses')
            ->where('employee_addresses.maid_id', $maid_id)
            ->where('employee_addresses.address_type_id', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_maid_address_perm($maid_id)
    {
        $this->db->select('employee_addresses.*')
            ->from('employee_addresses')
            ->where('employee_addresses.maid_id', $maid_id)
            ->where('employee_addresses.address_type_id', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_maid_address_emer_local($maid_id)
    {
        $this->db->select('employee_addresses.*')
            ->from('employee_addresses')
            ->where('employee_addresses.maid_id', $maid_id)
            ->where('employee_addresses.address_type_id', 3);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_maid_address_emer_home($maid_id)
    {
        $this->db->select('employee_addresses.*')
            ->from('employee_addresses')
            ->where('employee_addresses.maid_id', $maid_id)
            ->where('employee_addresses.address_type_id', 4);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_maids_address($maid_id, $localAddress, $permanentAddress, $emergencylocalAddress, $emergencyhomeAddress)
    {
        //    $this->db->where('maid_id', $maid_id);


        // $this->db->where('address_type_id', 1);
        // $this->db->update('employee_addresses', $localAddress);

        // $this->db->where('address_type_id', 2);
        // $this->db->update('employee_addresses', $permanentAddress);

        // $this->db->where('address_type_id', 3);
        // $this->db->update('employee_addresses', $emergencylocalAddress);

        // $this->db->where('address_type_id', 4);
        // $this->db->update('employee_addresses',$emergencyhomeAddress);
        $result = $this->check_maid_exists_type1($maid_id);
        if ($result > 0) {
            if (!empty($localAddress)) {

                $this->db->where('maid_id', $maid_id);
                $this->db->where('address_type_id', 1);
                $this->db->update('employee_addresses', $localAddress);
            }
        } else {
            $localAddress['maid_id'] = $maid_id;
            $this->db->insert('employee_addresses', $localAddress);
        }
        $resultt = $this->check_maid_exists_type2($maid_id);
        if ($resultt > 0) {
            if (!empty($permanentAddress)) {

                $this->db->where('maid_id', $maid_id);
                $this->db->where('address_type_id', 2);
                $this->db->update('employee_addresses', $permanentAddress);
            }
        } else {
            $permanentAddress['maid_id'] = $maid_id;
            $this->db->insert('employee_addresses', $permanentAddress);
            //print_r($this->db->last_query());die;

        }


        $resulttt = $this->check_maid_exists_type3($maid_id);
        if ($resulttt > 0) {


            if (!empty($emergencylocalAddress)) {


                $this->db->where('maid_id', $maid_id);
                $this->db->where('address_type_id', 3);
                $this->db->update('employee_addresses', $emergencylocalAddress);
            }
        } else {
            $emergencylocalAddress['maid_id'] = $maid_id;
            $this->db->insert('employee_addresses', $emergencylocalAddress);
        }
        $resultttt = $this->check_maid_exists_type4($maid_id);
        if ($resultttt > 0) {
            if (!empty($emergencyhomeAddress)) {

                $this->db->where('maid_id', $maid_id);
                $this->db->where('address_type_id', 4);
                $this->db->update('employee_addresses', $emergencyhomeAddress);
            }
        } else {
            $emergencyhomeAddress['maid_id'] = $maid_id;
            $this->db->insert('employee_addresses', $emergencyhomeAddress);
        }
    }

    function check_maid_exists($maid_id)
    {
        $this->db->from('employee_addresses');
        $this->db->where('maid_id', $maid_id);
        $this->db->where('address_type_id', 1);
        $this->db->limit(1);
        $query = $this->db->get();
        //print_r($this->db->last_query());die;
        return $query->num_rows() > 0;
    }

    function check_maid_exists_type1($maid_id)
    {
        $this->db->from('employee_addresses');
        $this->db->where('maid_id', $maid_id);
        //$this->db->where('address_type_id', 1);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }
    function check_maid_exists_type2($maid_id)
    {
        $this->db->from('employee_addresses');
        $this->db->where('maid_id', $maid_id);
        $this->db->where('address_type_id', 2);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }

    function check_maid_exists_type3($maid_id)
    {
        $this->db->from('employee_addresses');
        $this->db->where('maid_id', $maid_id);
        $this->db->where('address_type_id', 3);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }
    function check_maid_exists_type4($maid_id)
    {
        $this->db->from('employee_addresses');
        $this->db->where('maid_id', $maid_id);
        $this->db->where('address_type_id', 4);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }

    public function insertAvailability($updated_fields)
    {

        $this->db->insert_batch('maid_availability', $updated_fields);
        // print_r($this->db->last_query()); die;
        return $this->db->affected_rows();
        //return $this->db->insert_id();
    }

    function get_maid_preference($maid_id)
    {


        $this->db->select('*')
            ->from('maid_availability')
            ->where('employee_id', $maid_id);

        $get_permissions_qry = $this->db->get();

        return $get_permissions_qry->result();
    }

    function get_weekday_sun($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_weekday_mon($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_weekday_tues($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 2);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_weekday_wed($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 3);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_weekday_thursday($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 4);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_weekday_friday($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 5);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_weekday_sat($maid_id)
    {
        $this->db->select('location_id');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 6);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->location_id;
        } else {
            return null;
        }
    }

    function get_timefrom_sun($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }
    function get_timefrom_mon($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }

    function get_timefrom_tues($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 2);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }

    function get_timefrom_wed($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 3);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }

    function get_timefrom_thurs($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 4);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }


    function get_timefrom_friday($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 5);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }

    function get_timefrom_sat($maid_id)
    {
        $this->db->select('time_from');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 6);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_from;
        } else {
            return null;
        }
    }

    function get_timeto_sun($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }
    function get_timeto_mon($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }
    function get_timeto_tues($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 2);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }

    function get_timeto_wed($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 3);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }
    function get_timeto_thurs($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 4);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }
    function get_timeto_friday($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 5);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }
    function get_timeto_sat($maid_id)
    {
        $this->db->select('time_to');
        $this->db->from('maid_availability');
        $this->db->where('employee_id', $maid_id);
        $this->db->where('week_day', 6);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->time_to;
        } else {
            return null;
        }
    }

    function update_availabilityy($updated_fields, $maid_id)
    {
        foreach ($updated_fields as $data) {
            $week_day = $data['week_day'];
            $location_id = $data['location_id'];
            $time_from = $data['time_from'];
            $time_to = $data['time_to'];
            $created_at = $data['created_at'];
            $updated_at = $data['updated_at'];

            // Check if the row already exists for the maid_id and week_day
            $this->db->where('employee_id', $maid_id);
            $this->db->where('week_day', $week_day);
            $query = $this->db->get('maid_availability');

            if ($query->num_rows() > 0) {
                // Row exists, perform update
                $this->db->set('location_id', $location_id);
                $this->db->set('time_from', $time_from);
                $this->db->set('time_to', $time_to);
                $this->db->set('created_at', $created_at);
                $this->db->set('updated_at', $updated_at);
                $this->db->where('employee_id', $maid_id);
                $this->db->where('week_day', $week_day);
                $this->db->update('maid_availability');
            } else {
                // Row doesn't exist, perform insert
                $data['employee_id'] = $maid_id;
                $this->db->insert('maid_availability', $data);
            }
        }
    }

    public function update_availability($updated_fields, $maid_id)
    {
        // Get the list of existing week days for the maid_id
        $existing_week_days = array();
        $this->db->select('week_day');
        $this->db->where('employee_id', $maid_id);
        $query = $this->db->get('maid_availability');
        foreach ($query->result() as $row) {
            $existing_week_days[] = $row->week_day;
        }

        // Delete rows for unchecked week days
        $unchecked_week_days = array_diff($existing_week_days, array_column($updated_fields, 'week_day'));
        if (!empty($unchecked_week_days)) {
            $this->db->where('employee_id', $maid_id);
            $this->db->where_in('week_day', $unchecked_week_days);
            $this->db->delete('maid_availability');
        }

        // Insert or update rows for checked week days
        foreach ($updated_fields as $data) {
            $week_day = $data['week_day'];
            $location_id = $data['location_id'];
            $time_from = $data['time_from'];
            $time_to = $data['time_to'];
            $created_at = $data['created_at'];
            $updated_at = $data['updated_at'];

            if (in_array($week_day, $existing_week_days)) {
                // Row exists, perform update
                $this->db->set('location_id', $location_id);
                $this->db->set('time_from', $time_from);
                $this->db->set('time_to', $time_to);
                $this->db->set('created_at', $created_at);
                $this->db->set('updated_at', $updated_at);
                $this->db->where('employee_id', $maid_id);
                $this->db->where('week_day', $week_day);
                $this->db->update('maid_availability');
            } else {
                // Row doesn't exist, perform insert
                $data['employee_id'] = $maid_id;
                $this->db->insert('maid_availability', $data);
            }
        }
    }




    public function insert_skills($employeeId, $skills, $levels, $note)
    {
        $data = [];

        for ($i = 0; $i < count($skills); $i++) {
            $data[] = array(
                'employee_id' => $employeeId,
                'skill_id' => $skills[$i],
                'rating_level_id' => $levels[$i],
                'notes' => $note[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }


        $this->db->insert_batch('employee_skills', $data);
        return $this->db->affected_rows();
    }


    function get_employee_skills($maid_id)
    {
        $this->db->select('skill_id');
        $this->db->where('employee_id', $maid_id);
        $query = $this->db->get('employee_skills');
        $selectedSkills = $query->result_array();

        return $selectedSkills;
    }

    function get_employee_levels($maid_id)
    {
        $this->db->select('rating_level_id');
        $this->db->where('employee_id', $maid_id);
        $query = $this->db->get('employee_skills');
        $selectedLevels = $query->result_array();

        return $selectedLevels;
    }

    function get_employee_notes($maid_id)
    {
        $this->db->select('notes');
        $this->db->where('employee_id', $maid_id);
        $query = $this->db->get('employee_skills');
        $note = $query->row_array();

        return $note['notes'];;
    }


    function update_employee_skills($employeeId, $skills, $levels, $note)
    {
        for ($i = 0; $i < count($skills); $i++) {
            $skillId = $skills[$i];
            $level = $levels[$i];
            $noteValue = $note[$i];

            // Check if the skill already exists in the employee_skills table
            $this->db->where('employee_id', $employeeId);
            $this->db->where('skill_id', $skillId);
            $query = $this->db->get('employee_skills');
            $result = $query->row();

            if ($result) {
                // Skill already exists, update the level and note
                $data = array(
                    'rating_level_id' => $level,
                    'notes' => $noteValue,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->where('employee_id', $employeeId);
                $this->db->where('skill_id', $skillId);
                $this->db->update('employee_skills', $data);
            } else {
                // Skill doesn't exist, insert a new row
                $data = array(
                    'employee_id' => $employeeId,
                    'skill_id' => $skillId,
                    'rating_level_id' => $level,
                    'notes' => $noteValue,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->insert('employee_skills', $data);
            }
        }
    }


    function insert_new_skills($data)
    {
        $this->db->set($data);
        $this->db->insert('skills');
        $result = $this->db->insert_id();
        return $result;
    }

    function insert_driver_availability($details)
    {
        $this->db->set($details);
        $this->db->insert('driver_availability');
    }

    function get_driver_availability($maid_id)
    {

        $this->db->select('*');
        $this->db->where('driver_id', $maid_id);
        $query = $this->db->get('driver_availability');
        $driver_availability = $query->result_array();

        return $driver_availability;
    }

    function update_driver_availability($driverId, $details)
    {

        $this->db->where('driver_id', $driverId);
        $query = $this->db->get('driver_availability');

        if ($query->num_rows() > 0) {
            // If the record exists, perform an update
            $this->db->where('driver_id', $driverId);
            $this->db->update('driver_availability', $details);
        } else {
            // If the record doesn't exist, perform an insert
            $details['driver_id'] = $driverId;
            $details['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert('driver_availability', $details);
            // print_r($this->db->last_query()); die;
        }
    }


    function insert_leader($maidId, $data)
    {
        $this->db->where('maid_id', $maidId);
        $this->db->update('maids', $data);
    }

    function get_all_maids_new($active = 2)
    {
        $this->db->select('maids.*, CASE WHEN maids.maid_leader_id IS NOT NULL THEN leader.maid_name ELSE NULL END AS team_leader_name, flats.flat_name,employee_types.employee_type', FALSE);
        $this->db->from('maids');
        $this->db->join('maids AS leader', 'maids.maid_leader_id = leader.maid_id', 'LEFT');
        $this->db->join('flats', 'maids.flat_id = flats.flat_id', 'LEFT');
        $this->db->join('employee_types', 'employee_types.employee_type_id = maids.employee_type_id', 'LEFT');
        $this->db->where('maids.maid_status', 1);


        if ($active == 2) {
            $this->db->where('maids.maid_status', 1);
        } else if ($active == 3) {
            $this->db->where('maids.maid_status', 0);
        } else if ($active == 4) {
            $this->db->where('maids.maid_status', 2);
        }
        $this->db->order_by('maids.maid_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_maids_new_filter($status, $employeeType)
    {
        $this->db->select('maids.*, CASE WHEN maids.maid_leader_id IS NOT NULL THEN leader.maid_name ELSE NULL END AS team_leader_name, flats.flat_name,employee_types.employee_type', FALSE);
        $this->db->from('maids');
        $this->db->join('maids AS leader', 'maids.maid_leader_id = leader.maid_id', 'LEFT');
        $this->db->join('flats', 'maids.flat_id = flats.flat_id', 'LEFT');
        $this->db->join('employee_types', 'employee_types.employee_type_id = maids.employee_type_id', 'LEFT');
        // $this->db->where('maids.maid_status', 1);


        if ($status == 2) {
            $this->db->where('maids.maid_status', 1);
        } else if ($status == 3) {
            $this->db->where('maids.maid_status', 0);
        } else if ($status == 4) {
            $this->db->where('maids.maid_status', 2);
        }
        if ($employeeType == 1) {
            $this->db->where('maids.employee_type_id', 1);
        } else if ($employeeType == 2) {
            $this->db->where('maids.employee_type_id', 2);
        } else if ($employeeType == 2) {
            $this->db->where('maids.employee_type_id', 2);
        } else if ($employeeType == 3) {
            $this->db->where('maids.employee_type_id', 3);
        } else if ($employeeType == 4) {
            $this->db->where('maids.employee_type_id', 4);
        }
        $this->db->order_by('maids.maid_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_employee_types()
    {
        $query = $this->db->get('employee_types');
        return $query->result_array();
    }

    public function verifyMobilenumberExist($mobile_number_1) 
    {
        $this->db->select('maid_id');

        $this->db->where(array("maid_mobile_1" => $mobile_number_1));

        $query=$this->db->get('maids');

        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function verifyMobilenumberEditExist($mobile_number_1, $id) 
    {
        $this->db->select('maid_id, maid_name');

        $this->db->where(array("maid_mobile_1" => $mobile_number_1, "maid_id !=" =>$id));

        $query=$this->db->get('maids');

        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function get_all_maids_active($filter = NULL)
    {

        if (isset($filter)) {
            $location_type  = $filter['location_type'];
            $value          = $filter['location_val'];
            $customer_id    = $filter['customer_id'];
            $area_id        = $filter['area_id'];
            $zone_id        = $filter['zone_id'];
            $province_id    = $filter['province_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];

            $service_week_day = date('w', strtotime($service_date));
            $time_from =  date('H:i:s', trim($from_time));
            $time_to = date('H:i:s', trim($to_time));
        }

        if ($location_type == 'area') {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where('a.area_id', $area_id);
            $this->db->where('m.employee_type_id', 1);
            $this->db->order_by('m.maid_name', 'ASC');
        } else if ($location_type == 'zone') {
            $areaArray  = array();
            $subQuery   = "SELECT asq.area_id FROM `areas` as asq WHERE `zone_id` = $zone_id";
            $querySub   = $this->db->query($subQuery);
            foreach ($querySub->result_array() as $row) {
                $areaArray[] = $row['area_id'];
            }

            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            $this->db->join('zones as z', 'z.zone_id = a.zone_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where_in('a.area_id', $areaArray);
            //                 $this->db->where('z.zone_id', $value);
        } else if ($location_type == 'province') {
            $areaArray  = array();
            $subQuery   = "SELECT asq.area_id FROM `areas` as asq LEFT JOIN `zones` as zsq ON `zsq`.`zone_id` = `asq`.`zone_id` WHERE `zsq`.`province_id` = $province_id";
            $querySub   = $this->db->query($subQuery);
            foreach ($querySub->result_array() as $row) {
                $areaArray[] = $row['area_id'];
            }
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            //$this->db->join('province as p', 'p.province_id = z.province_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            $this->db->join('zones as z', 'z.zone_id = a.zone_id');
            $this->db->join('province as p', 'p.province_id = z.province_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where_in('a.area_id', $areaArray);
            $this->db->where('m.employee_type_id', 1);
            $this->db->order_by('m.maid_name', 'ASC');
            //                 $this->db->where('p.province_id', $value);
        } else {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
            $this->db->from('maids m');
        }
        $this->db->where('maid_status', 1);
        $this->db->group_by('m.maid_id');
        $this->db->where('m.employee_type_id', 1);
            // $this->db->order_by('m.maid_name', 'ASC');
        $this->db->order_by('maid_name', 'ASC');
        //  ->limit(10);


        $get_maids_leave_by_date_qry = $this->db->get();
        //echo $this->db->last_query();
        return $get_maids_leave_by_date_qry->result();
    }

    // function get_all_maids_active_enquiry($filter = NULL)
    // {
    //     if (isset($filter)) {
    //         // $location_type  = $filter['location_type'];
    //         // $value          = $filter['location_val'];
    //         $customer_id    = $filter['customer_id'];
    //         $area_id        = $filter['area_id'];
    //         $zone_id        = $filter['zone_id'];
    //         $province_id    = $filter['province_id'];
    //         $service_date   = $filter['service_date'];
    //         $from_time      = $filter['from_time'];
    //         $to_time        = $filter['to_time'];
    //         $maid_id = $filter['maid_id'];
    //         $service_week_day = date('w', strtotime($service_date));
    //         $time_from =  date('H:i:s', trim($from_time));
    //         $time_to = date('H:i:s', trim($to_time));
    //     }

    //     if ($maid_id !="" && $zone_id == "") {
       
    //         // $areaArray  = array();
    //         // $subQuery   = "SELECT asq.area_id FROM `areas` as asq WHERE `zone_id` = $zone_id";
    //         // $querySub   = $this->db->query($subQuery);
    //         // foreach ($querySub->result_array() as $row) {
    //         //     $areaArray[] = $row['area_id'];
    //         // }
    //         $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file, ma.zone_id');
    //         $this->db->from('maids m');
    //         $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
    //         $this->db->join('maid_availability as ma', 'm.maid_id = ma.employee_id', 'LEFT');
    //         // $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
    //         //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
    //         //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
    //         // $this->db->join('areas as a', 'ca.area_id = a.area_id');
    //         // $this->db->join('zones as z', 'a.zone_id = z.zone_id');
    //         //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
    //         // $this->db->where_in('a.area_id', $areaArray);
    //         $this->db->where('m.maid_id', $maid_id);
    //         // $this->db->where('ma.zone_id', $zone_id);
    //     } else if($maid_id !="" && $zone_id != "") {
    //         $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file, ma.zone_id');
    //         $this->db->from('maids m');
    //         $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
    //         $this->db->join('maid_availability as ma', 'm.maid_id = ma.employee_id', 'LEFT');
    //         $this->db->where('m.maid_id', $maid_id);
    //         $this->db->where('ma.zone_id', $zone_id);
    //     } 
    //     else if($maid_id =="" && $zone_id != "") {
    //         $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file, ma.zone_id');
    //         $this->db->from('maids m');
    //         $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
    //         $this->db->join('maid_availability as ma', 'm.maid_id = ma.employee_id', 'LEFT');
    //         // $this->db->where('m.maid_id', $maid_id);
    //         $this->db->where('ma.zone_id', $zone_id);

    //     }
    //     else {
    //         $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
    //         $this->db->from('maids m');
    //     }
    //     $this->db->where('maid_status', 1);
    //     $this->db->group_by('m.maid_id');
    //     $this->db->where('m.employee_type_id', 1);
    //         // $this->db->order_by('m.maid_name', 'ASC');
    //     $this->db->order_by('maid_name', 'ASC');
    //     //  ->limit(10);


    //     $get_maids_leave_by_date_qry = $this->db->get();
    //     // print_r($get_maids_leave_by_date_qry);die();
    //     //echo $this->db->last_query();
    //     return $get_maids_leave_by_date_qry->result();
    // }
    function get_all_maids_active_enquiry($filter = NULL)
    {
        if (isset($filter)) {
            // $location_type  = $filter['location_type'];
            // $value          = $filter['location_val'];
            $customer_id    = $filter['customer_id'];
            // $area_id        = $filter['area_id'];
            $zone_id        = $filter['zone_id'];
            // $province_id    = $filter['province_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];
            $maid_id = $filter['maid_id'];
            $service_week_day = date('w', strtotime($service_date));
            $time_from = (strtotime(trim($from_time)) !== false) ? date('H:i:s', strtotime(trim($from_time))) : null;
            $time_to = (strtotime(trim($to_time)) !== false) ? date('H:i:s', strtotime(trim($to_time))) : null;
        }

        if ($maid_id !="" && $zone_id == "") {
       
            // $areaArray  = array();
            // $subQuery   = "SELECT asq.area_id FROM `areas` as asq WHERE `zone_id` = $zone_id";
            // $querySub   = $this->db->query($subQuery);
            // foreach ($querySub->result_array() as $row) {
            //     $areaArray[] = $row['area_id'];
            // }
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file, ma.zone_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('maid_availability as ma', 'm.maid_id = ma.employee_id', 'LEFT');
            // $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            // $this->db->join('areas as a', 'ca.area_id = a.area_id');
            // $this->db->join('zones as z', 'a.zone_id = z.zone_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            // $this->db->where_in('a.area_id', $areaArray);
            $this->db->where('m.maid_id', $maid_id);
            // $this->db->where('ma.zone_id', $zone_id);
        } else if($maid_id !="" && $zone_id != "") {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file, ma.zone_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('maid_availability as ma', 'm.maid_id = ma.employee_id', 'LEFT');
            $this->db->where('m.maid_id', $maid_id);
            $this->db->where('ma.zone_id', $zone_id);
        } 
        else if($maid_id =="" && $zone_id != "") {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file, ma.zone_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('maid_availability as ma', 'm.maid_id = ma.employee_id', 'LEFT');
            // $this->db->where('m.maid_id', $maid_id);
            $this->db->where('ma.zone_id', $zone_id);

        }
        else {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
            $this->db->from('maids m');
        }
        $this->db->where('maid_status', 1);
        $this->db->group_by('m.maid_id');
        $this->db->where('m.employee_type_id', 1);
            // $this->db->order_by('m.maid_name', 'ASC');
        $this->db->order_by('maid_name', 'ASC');
        //  ->limit(10);


        $get_maids_leave_by_date_qry = $this->db->get();
        // print_r($get_maids_leave_by_date_qry);die();
        //echo $this->db->last_query();
        return $get_maids_leave_by_date_qry->result();
    }

    function get_pay_employee_maid($maid_id)
    {

        $this->db->select("mc.booking_id,mc.service_date,m.maid_id,m.maid_name,mc.total_hours,mc.total_amount,mc.added_date_time,c.customer_id,c.customer_name", FALSE)
        ->from('maid_hour_cancellation mc')
        ->join('customers c', 'mc.customer_id = c.customer_id')
        ->join('maids m', 'mc.maid_id = m.maid_id')
        ->where('mc.maid_id', $maid_id);
       
        $get_schedule_by_cancel_qry = $this->db->get()->result();
        // echo '<pre>';
        // print_r($get_schedule_by_cancel_qry);
        return $get_schedule_by_cancel_qry;

    }
}
