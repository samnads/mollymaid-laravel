<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Customers_Model Class
 * 
 * @author Geethu
 * @package HM
 * @version Version 1.0
 */
class Justmop_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Get Area by name
     * 
     * @author Vishnu
     * @access public
     * @param  bool
     * @return array
     */
    
    function get_area_id_name($area)
    {
        $this->db->select('area_id')
                ->from('areas')
                ->where('area_name',$area)
                ->where('area_status',1);
        $get_area_id_name_qry = $this->db->get();

        return $get_area_id_name_qry->row();
    }
    
    function get_customer_by_name($name)
    {
        $this->db->select('customer_id')
                ->from('customers')
                ->where('customer_name',$name);
        $get_customer_by_name = $this->db->get();

        return $get_customer_by_name->row();
    }
    
    function get_customer_by_name_and_address($name,$location)
    {
        $this->db->select('c.customer_id')
                ->from('customers as c')
                ->join('customer_addresses as ca','c.customer_id = ca.customer_id','left')
                ->where('ca.customer_address',$location)
                ->where('c.customer_name',$name)
                ->where('ca.default_address',1);
        $get_customer_by_name = $this->db->get();

        return $get_customer_by_name->row();
    }
    
    function get_customer_address_by_id($customer_id)
    {
        $this->db->select('customer_address_id')
                ->from('customer_addresses')
                ->where('customer_id',$customer_id);
        $get_customer_address_by_id_qry = $this->db->get();

        return $get_customer_address_by_id_qry->row();
    }
    
    function add_customer($data) 
    {
        $this->db->set($data);
        $this->db->insert('customers');
        $result = $this->db->insert_id();
        return $result;
    }
    
    function add_customer_address($datas) 
    {
        $this->db->set($datas);
        $this->db->insert('customer_addresses');
        $result = $this->db->insert_id();
        return $result;
    }
    
    function add_booking($fields = array())
    {   
        $this->db->set($fields);
        $this->db->insert('bookings'); 
        return $this->db->insert_id();
    }
    
    function get_all_booking_approval_list($filter = 'Pending',$service_date)
    {      
        //$service_date = date('Y-m-d');
//                $status = $filter == 'Assigned' ? 1 : 0;
        switch($filter):
        case 'Pending' :
            $status = '0';
            break;
        case 'Assigned' :
            $status = '1';
            break;
        case 'Deleted' :
            $status = '2';
            break;
        case 'Expired' :
            $status = '0';
            break;
        case 'All' :
            $status = '';
            break;      
        endswitch;
                
        $this->db->select('b.booking_id,b.from_justmop,b.booking_note,b.justmop_reference,jr.just_mop_ref, jr.mop, b.booking_type, b.delete_remarks, b.booked_from,b.booking_status,CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), "-" , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, c.customer_name, c.payment_type, ca.customer_address, a.area_name,z.zone_name,c.customer_id, DAYNAME(b.service_start_date) AS weekday, DATE_FORMAT(b.service_start_date, "%d/%m/%Y") AS service_date, m.maid_name, DATE_FORMAT(b.booked_datetime, "%d/%m/%Y %H:%i") AS booked_datetime, no_of_maids,cleaning_material,c.phone_number, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.email_address', FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left')
                ->join('maids m', 'b.maid_id = m.maid_id', 'left')
				->join('justmop_ref jr', 'b.booking_id = jr.booking_id', 'left')
                //->where_in('b.booked_from', array('W', 'M'))
				->where('b.booking_category', 'C')
                ->where('b.from_justmop', 1)
				->where('b.service_start_date', $service_date)

                ->order_by('b.booked_datetime', 'desc');
        if( (isset($status) != '')) 
        {
            $this->db->where('b.booking_status', $status);
        }  
        // if($filter == 'Pending')
        // {
            // $this->db->where("(b.service_start_date >= " . $this->db->escape($service_date) . ")", NULL, FALSE);                            

        // }
        // else if($filter == 'Expired')
        // {
            // $this->db->where("(b.service_start_date < " . $this->db->escape($service_date) . ")", NULL, FALSE);                             
        // } 
        $get_all_booking_approval_list_qry = $this->db->get();
                
        return $get_all_booking_approval_list_qry->result();
    }
    
    
    
    
    
    
    
    
    
    

    function get_customer_zone_by_address_id($customer_address_id) {
        $this->db->select('z.zone_id')
                ->from('customer_addresses ca')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('ca.customer_address_id', $customer_address_id)
                ->where('z.zone_status', 1)
                ->where('a.deleted_at', null);


        $get_customer_zone_by_address_id_qry = $this->db->get();

        return $get_customer_zone_by_address_id_qry->row();
    }

    function get_customer_by_id($customer_id) {
        $this->db->select("customer_id, odoo_customer_id, customer_name,customer_username, customer_password, customer_nick_name, mobile_number_1, mobile_number_2, mobile_number_3, phone_number, fax_number, email_address, website_url, customer_photo_file, customer_booktype, customer_type, contact_person, payment_type, payment_mode, price_hourly, price_extra, price_weekend, latitude, longitude, key_given, customer_notes, DATE_FORMAT(customer_added_datetime, '%d / %M / %Y %h:%i %p') AS added_datetime, customer_status,balance, signed, ", FALSE)
                ->from('customers')
                ->where('customer_id', $customer_id)
                ->limit(1);

        $get_customer_by_id_qry = $this->db->get();
//echo $this->db->last_query();exit;
        return $get_customer_by_id_qry->row();
    }

    /**
     * Get customer address
     * 
     * @author  Azinova     * 
     * @access  public
     * @param   int, bool
     * @return array
     */
    function get_customer_addresses($customer_id, $active_only = TRUE) {
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address, a.area_name, z.zone_id, z.zone_name,ca.latitude,ca.longitude')
                ->from('customer_addresses ca')
                ->where('ca.customer_id', $customer_id)
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('ca.customer_address_id');

        if ($active_only) {
            $this->db->where('z.zone_status', 1);
            $this->db->where('a.deleted_at', null);
        }

        $get_customer_addresses_qry = $this->db->get();

        return $get_customer_addresses_qry->result();
    }

    function add_customer_payment($fields = array()) {
        $fields['paid_datetime'] = isset($fields['paid_datetime']) ? $fields['paid_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('customer_payments');
        return $this->db->insert_id();
    }

    /**
     * Add customer
     * 
     * @author  Azinova Develpers      
     * @access  public
     * @param   
     * @return 
     */
    function add_customers($data) {
        $this->db->set($data);
        $this->db->insert('customers');
        $result = $this->db->insert_id();
        return $result;
    }
    
    function add_user_actvty($data) {
        $this->db->set($data);
        $this->db->insert('user_activity');
        $result = $this->db->insert_id();
        return $result;
    }

    

    /**
     * Get all customer 
     * 
     * @author  Betsy
     * @access  public
     * @param   
     * @return  array
     */
    function get_all_customers() {
        $this->db->select('customers.*')
                ->from('customers');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_customer_details($customer_id) {
        $this->db->select('*')
                ->from('customers')
                ->where('customers.customer_id', $customer_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_customer_address($cust_id) {

        $query = $this->db->query('select customer_addresses.customer_address_id, customer_addresses.customer_id, customer_addresses.area_id,customer_addresses.latitude,customer_addresses.longitude,customer_addresses.customer_address, areas.area_name, customers.customer_name, z.zone_name
from customer_addresses
join areas ON customer_addresses.area_id = areas.area_id
join zones z ON areas.zone_id = z.zone_id
join customers ON customer_addresses.customer_id = customers.customer_id
where customer_addresses.customer_id = ' . $cust_id);
        return $query->result_array();
    }

    function update_customers($data, $customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
           return $this->db->affected_rows();
    }
    
    function update_booktype($data,$customer_id){
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
    }

    function delete_customer_address($customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_addresses');
    }

    function update_customer_address($datas, $customer_address_id) {
        
        

        //$customer_address_id = $this->get_customer_address_by_area($datas['customer_id'], $datas['area_id']);
        if ($customer_address_id) {
            $this->db->where('customer_address_id', $customer_address_id);
            $this->db->update('customer_addresses', $datas);
            $result = $customer_address_id;
        } else {
            $this->db->set($datas);
            $this->db->insert('customer_addresses');
            $result = $this->db->insert_id();
        }
        return $result;
    }

    function get_customer_address_by_area($customer_id, $area_id) {
        $this->db->select('customer_address_id')
                ->from('customer_addresses')
                ->where('customer_id', $customer_id)
                ->where('area_id', $area_id)
                ->limit(1);

        $get_customer_address_by_area_qry = $this->db->get();

        $customer_address = $get_customer_address_by_area_qry->row();
        //print_r($customer_address);
        //exit;
        if ($customer_address->customer_address_id > 0)
            return $customer_address->customer_address_id;
        else
            return FALSE;
    }

    function remove_customer_address($address_id) {
        $this->db->where('customer_address_id', $address_id);
        $this->db->delete('customer_addresses');
    }

    function get_all_customer_details($customer_id) {
        $res['customers'] = array();
        $query = $this->db->query('select * from customers where customers.customer_id = ' . $customer_id);
        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $address = array();

                $cust_id = $row['customer_id'];

                $address_query = $this->db->query('select customer_addresses.customer_address_id, customer_addresses.customer_id, customer_addresses.area_id, customer_addresses.customer_address, areas.area_name, customers.customer_name
from customer_addresses
join areas ON customer_addresses.area_id = areas.area_id
join customers ON customer_addresses.customer_id = customers.customer_id
where customer_addresses.customer_id = ' . $cust_id);
                if ($address_query->num_rows() > 0) {

                    foreach ($address_query->result_array() as $rows) {
                        array_push($address, $rows);
                        $row['address'] = $address;
                    }
                    array_push($res['customers'], $row);
                }
            }
        }
        return $res['customers'];
    }

    function delete_customer($customer_id, $status) {
        $this->db->set('customer_status', $status);
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers');
    }

    public function record_count($active) {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status, ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $get_active_customer_qry = $this->db->get();

        return $get_active_customer_qry->num_rows();

        /* if ($active == 1) {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas','ca.area_id=a.area_id');
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          } else if ($active == 2) {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas a','ca.area_id=a.area_id')->where('c.customer_status', 1);
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          } else {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas a','ca.area_id=a.area_id')->where('c.customer_status', 0);
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          }
         */
    }

    public function fetch_customers($limit, $start, $active = 1) {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $this->db->group_by('c.customer_id');
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    public function get_last_job_date_by_customerid($customer_id)
    {
        $this->db->select('service_date')
                ->from('day_services')
                ->where('customer_id',$customer_id)
                ->order_by('service_date','desc')
                ->limit(1);
        $get_last_job_date_by_customerid_qry = $this->db->get();
        return $get_last_job_date_by_customerid_qry->row();        
    }
    
    public function fetch_customers_excel($active = 1) {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $this->db->group_by('c.customer_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    public function get_total_customers_counts() {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status, ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $get_active_customer_qry = $this->db->get();
        return $get_active_customer_qry->num_rows();
    }

    function get_customers_by_keyword($keyword) {
        $keyword = strtolower($keyword);

        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');

        $customer_field = $this->db->list_fields('customers');
        foreach ($customer_field as $field) {
            $this->db->or_like("LOWER(c." . $field . ")", $keyword);
        }
        $customer_address_field = $this->db->list_fields('customer_addresses');
        foreach ($customer_address_field as $field) {
            $this->db->or_like("LOWER(ca." . $field . ")", $keyword);
        }
        $area_field = $this->db->list_fields('areas');
        foreach ($area_field as $field) {
            $this->db->or_like("LOWER(a." . $field . ")", $keyword);
        }
        $zone_field = $this->db->list_fields('zones');
        foreach ($zone_field as $field) {
            $this->db->or_like("LOWER(z." . $field . ")", $keyword);
        }
        
         $this->db->group_by('c.customer_id');

        $get_customers_by_keyword_query = $this->db->get();

        //echo $this->db->last_query();exit;
        return $get_customers_by_keyword_query->result();
    }

    /**
     * Get customer address by id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_customer_address_by_id1($customer_address_id) {
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address, a.area_name, z.zone_id, z.zone_name')
                ->from('customer_addresses ca')
                ->where('ca.customer_address_id', $customer_address_id)
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('ca.customer_address_id');

        $get_customer_address_by_id_qry = $this->db->get();

        return $get_customer_address_by_id_qry->row();
    }

    /**
     * Get maid history by customer id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_maid_history($customer_id) {
        $this->db->select('m.maid_name, DATE_FORMAT(ds.service_date, "%d/%m/%Y") AS service_date, DATE_FORMAT(ds.start_time, "%h:%i %p") AS start_time, DATE_FORMAT(ds.end_time,"%h:%i %p") AS end_time, IF(ds.service_status = 1,"Service Started", IF(ds.service_status = 2, "Service Finished","Service not Done")) as service_status, DATE_FORMAT(b.time_from,"%h:%i %p") AS start_from_time, DATE_FORMAT(b.time_to,"%h:%i %p") AS end_to_time', FALSE)
                ->from('day_services ds')
                ->where('ds.customer_id', $customer_id)
                ->where('ds.service_status !=',3,FALSE)
                ->join('customers c', 'ds.customer_id = c.customer_id')
                ->join('maids m', 'ds.maid_id = m.maid_id')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
               
                ->order_by('ds.service_date', 'desc');

        $get_maid_history_qry = $this->db->get();

        return $get_maid_history_qry->result();
    }

    /**
     * Get payment history by customer id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_payment_history($customer_id) {
        $this->db->select('m.maid_name, DATE_FORMAT(ds.service_date, "%d/%m/%Y") AS service_date, ds.total_fee, cp.paid_amount, DATE_FORMAT(ds.start_time, "%h:%i %p") AS start_time, DATE_FORMAT(ds.end_time,"%h:%i %p") AS end_time, DATE_FORMAT(b.time_from,"%h:%i %p") AS start_from_time, DATE_FORMAT(b.time_to,"%h:%i %p") AS end_to_time', FALSE)
                ->from('day_services ds')
                ->where('ds.customer_id', $customer_id)
                ->where('ds.payment_status', '1')
                ->join('customers c', 'ds.customer_id = c.customer_id')
                ->join('maids m', 'ds.maid_id = m.maid_id')
                //added extra by vishnu
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id')
                //ends
                ->order_by('ds.service_date', 'desc');

        $get_maid_history_qry = $this->db->get();

        return $get_maid_history_qry->result();
    }

    function get_current_service($customer_id) {
        $service_date = date('Y-m-d');
        $service_week_day = date('w', strtotime($service_date));
        
        //$this->db->select('b.maid_id, m.maid_name, m.maid_nationality, m.maid_photo_file, CONCAT( GROUP_CONCAT(CONCAT( DATE_FORMAT(b.time_from, "%h:%i %p") ,"-", DATE_FORMAT(b.time_to,"%h:%i %p")) SEPARATOR ","), " [", (IF(b.booking_type = "WE", IF( b.service_week_day = 0, "Sunday", SUBSTRING_INDEX(SUBSTRING_INDEX("Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",",",b.service_week_day),",",-1)), DATE_FORMAT(b.service_start_date, "%d/%m/%Y"))), "]") AS shifts, b.booking_type', FALSE)
        $this->db->select('b.maid_id,b.booking_id, m.maid_name, m.maid_nationality, m.maid_photo_file, CONCAT( GROUP_CONCAT(CONCAT( DATE_FORMAT(b.time_from, "%h:%i %p") ,"-", DATE_FORMAT(b.time_to,"%h:%i %p")) SEPARATOR ","), " [", (IF(b.booking_type = "WE", IF( b.service_week_day = 0, "Sunday", SUBSTRING_INDEX(SUBSTRING_INDEX("Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",",",b.service_week_day),",",-1)), DATE_FORMAT(b.service_start_date, "%d/%m/%Y"))), "]") AS shifts, b.booking_type', FALSE)        
				->from('bookings b')
                ->where('b.customer_id', $customer_id)
				->where('b.booking_category', 'C')
                ->where('b.booking_status', '1')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR ((b.service_start_date >= " . $this->db->escape($service_date) . " OR b.service_start_date <= " . $this->db->escape($service_date) . ") AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->group_by('b.maid_id, b.booking_type, b.service_start_date')
                ->order_by('b.maid_id');

        $get_maid_history_qry = $this->db->get();
        
        
        return $get_maid_history_qry->result();
    }
    
    function getthisdata()
    {
        $this->db->select("c.customer_id")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $this->db->group_by('c.customer_id');
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->result();
    }
    
    function get_booking_deletes_by()
    {
        $this->db->select('booking_id')
                        ->from('booking_deletes');
        $get_booking_deletes_by_date_qry = $this->db->get();
        return $get_booking_deletes_by_date_qry->result();
    }
    
    function getbookcount($custid)
    {
//        $deletes = $this->get_booking_deletes_by();
//        $deleted_bookings = array();
//        foreach($deletes as $delete)
//        {
//                $deleted_bookings[] = $delete->booking_id;
//        }
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
				->where('b.booking_category', 'C')
                ->where('b.booking_status', 1)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where('c.customer_id',$custid)
                ->order_by('m.maid_name')
                ->order_by('b.time_from');

//        if(count($deleted_bookings) > 0)
//        {
//                $this->db->where_not_in('b.booking_id', $deleted_bookings);
//        }
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->num_rows();
    }
    
    public function getdata()
    {
        $customers = $this->getthisdata();
        $dates = date('Y-m-d');
        $datas = array();
        $test = array();
        foreach($customers as $row)
        {
            $custid = $row->customer_id;
            $bookingcount = $this->getbookcount($custid);
            $datas['cid'] = $custid;
            $datas['count'] = $bookingcount;
            array_push($test, $datas);
        }
        return $test;
    }

    public function search_by_date($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 1, $sortsources) {
        if($sort_customers == 3) {
            $this->db->select('b.customer_id,b.booking_id, COUNT(b.customer_id) AS count', false)
                  ->from('bookings b')
				  ->where('b.booking_category', 'C')
                  ->group_by('b.customer_id');
            $this->db->having('count = 1',false);
            $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes)', NULL, FALSE);
            $items = $this->db->get()->result();
            foreach ($items as $rowitem)
            {
                $cus_id = $rowitem->customer_id;
            }
            
            return $items;
            
        } else {
        $date = date('Y-m-d');
        $service_week_day = date('w', strtotime($date));
                
        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        if($sort_customers == 2) {
            $this->db->join('bookings b','c.customer_id = b.customer_id');
            $this->db->where('b.booking_status', 1);
			$this->db->where('b.booking_category', 'C');
            $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((b.service_start_date = " . $this->db->escape($date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        if($sortsources)
        {
          $this->db->where("c.customer_source",$sortsources);  
            
        }
        if ($from_date && $to_date) {
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$from_date' AND '$to_date'");
        } if(($from_date!="" && $to_date=="" ) || ($from_date=="" && $to_date!="" ) ) {
            $this->db->where("DATE(`customer_added_datetime`) = '$from_date' OR  DATE(`customer_added_datetime`) ='$to_date'");
        }
        if($from_date=="" && $to_date=="" && $payment_type=="")
        {
         $this->db->group_by('c.customer_id');
         //$this->db->limit(100);   
            
        }
        
        return $this->db->get()->result();
        //echo $this->db->last_query();exit();
        }
        
    }
    
    public function search_by_date1($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 1) {
        
        
        //$service_week_day = date('w', strtotime($date));
        $fromDate = date("Y-m-d", strtotime($from_date));
        $toDate = date("Y-m-d", strtotime($to_date));
                
        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        if($sort_customers == 2) {
            $this->db->join('bookings b','c.customer_id = b.customer_id');
			$this->db->where('b.booking_category', 'C');
            $this->db->where('b.booking_status', 1);
            
//            if ($from_date && $to_date) {
//                $fromDate = date("Y-m-d", strtotime($from_date));
//                $toDate = date("Y-m-d", strtotime($to_date));
//                $this->db->where("(((b.service_actual_end_date BETWEEN '$fromDate' AND '$toDate') AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
//                $this->db->where("((b.service_start_date BETWEEN '$fromDate' AND '$toDate'))",NULL, FALSE);               
            //}else
                if(($from_date!="" && $to_date=="" )) {
                if($from_date == "")
                {
                    $fromDate = "";
                } else {
                    $fromDate = date("Y-m-d", strtotime($from_date));
                    $service_week_day1 = date('w', strtotime($fromDate));
                }
                //$fromDate = date("Y-m-d", strtotime($from_date));
                if($to_date == "")
                {
                    $toDate = "";
                } else {
                    $toDate = date("Y-m-d", strtotime($to_date));
                }
                //$toDate = date("Y-m-d", strtotime($to_date));
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($fromDate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($fromDate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($fromDate) . " AND service_week_day = " . $service_week_day1 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($fromDate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($fromDate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            }else if(($from_date=="" && $to_date!="" )) {
                if($from_date == "")
                {
                    $fromDate = "";
                } else {
                    $fromDate = date("Y-m-d", strtotime($from_date));
                }
                //$fromDate = date("Y-m-d", strtotime($from_date));
                if($to_date == "")
                {
                    $toDate = "";
                } else {
                    $toDate = date("Y-m-d", strtotime($to_date));
                    $service_week_day2 = date('w', strtotime($toDate));
                }
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($toDate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($toDate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($toDate) . " AND service_week_day = " . $service_week_day2 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($toDate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($toDate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            } else {
                $date = date('Y-m-d');
                $newdate = date("Y-m-d", strtotime($date));
                $service_week_day3 = date('w', strtotime($date));
            //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
            $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($newdate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((b.service_start_date = " . $this->db->escape($newdate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($newdate) . " AND service_week_day = " . $service_week_day3 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($newdate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($newdate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            }
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        
        $this->db->group_by('c.customer_id');
        return $this->db->get()->result();
        //echo $this->db->last_query();exit();
        
        
    }
    
    
    public function search_by_date_sort($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 3) {
        $date = date('Y-m-d');
        $this->db->select('b.customer_id, COUNT(b.customer_id) AS count', false)
                ->from('bookings b')
				->where('b.booking_category', 'C')
                ->join('customers c','b.customer_id = c.customer_id');
        
        $this->db->group_by('b.customer_id');
        $this->db->having('count = 1',false);
        $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes where service_date = '.$date.')', NULL, FALSE);
        $items = $this->db->get()->result();
        return $items;
    }
    public function search_by_date_sort1($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 3) {
        //$date = date('Y-m-d');
        $fromDate = date("Y-m-d", strtotime($from_date));
        $toDate = date("Y-m-d", strtotime($to_date));
        $this->db->select('b.customer_id, COUNT(b.customer_id) AS count', false)
                ->from('bookings b')
				->where('b.booking_category', 'C')
                ->join('customers c','b.customer_id = c.customer_id');
        
        $this->db->group_by('b.customer_id');
        $this->db->having('count = 1',false);
        $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes where service_date BETWEEN '.$fromDate.' AND '.$toDate.')', NULL, FALSE);
        $items = $this->db->get()->result();
        return $items;
    }
    public function search_cust_by_booking($custid,$from_date, $to_date,$payment_type,$all_customers = 1)
    {
        //$originalDate = "2010-03-21";
        
        
        $date = date('Y-m-d');
        //$service_week_day = date('w', strtotime($date));
        $this->db->select("b.customer_id,c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('bookings b')
                ->join('customers c','b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
                ->where('b.booking_status', 1)
                ->where('b.customer_id',$custid);
        if ($from_date && $to_date) {
            $fromDate = date("Y-m-d", strtotime($from_date));
            $toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$fromDate' AND '$toDate'");
        } if(($from_date!="" && $to_date=="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            //$toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("DATE(`customer_added_datetime`) = '$fromDate'");
        } if(($from_date=="" && $to_date!="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            $this->db->where("DATE(`customer_added_datetime`) ='$toDate'");
        }
        //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
        $this->db->where('b.service_start_date',$date);
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        
        return $this->db->get()->result();
        
    }
    
    public function search_cust_by_booking1($custid,$from_date, $to_date,$payment_type,$all_customers = 1)
    {
        //$originalDate = "2010-03-21";
        
        
        $date = date('Y-m-d');
        //$service_week_day = date('w', strtotime($date));
        $this->db->select("b.customer_id,c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('bookings b')
                ->join('customers c','b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
                ->where('b.booking_status', 1)
                ->where('b.customer_id',$custid);
        if ($from_date && $to_date) {
            $fromDate = date("Y-m-d", strtotime($from_date));
            $toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("b.service_start_date BETWEEN '$fromDate' AND '$toDate'");
        }else if(($from_date!="" && $to_date=="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            //$toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("b.service_start_date = '$fromDate'");
        }else if(($from_date=="" && $to_date!="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            $this->db->where("b.service_start_date ='$toDate'");
        } else {
        //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
        $this->db->where('b.service_start_date',$date);
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        
        return $this->db->get()->result();
        
    }
    
    function get_back_payment($date)
    {
        $this->db->select('c.customer_id, c.customer_name, c.payment_type, z.zone_name, b.requested_amount, b.collected_amount, DATE_FORMAT(b.collected_date, "%d/%m/%Y") AS collected_date, DATE_FORMAT(b.collected_time, "%h:%i %p") AS collected_time', FALSE)
                ->from('back_payments b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                //edited by vishnu
                //->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                //->join('areas a', 'ca.area_id = a.area_id')
                //->join('zones z', 'a.zone_id = z.zone_id')
                ->join('zones z', 'b.zone_id = z.zone_id')
                //ends
                ->where('b.collected_date', $date);
        
        $get_back_payment = $this->db->get();
        
        return $get_back_payment->result();
    }
    function add_backpayment($fields)
    {
        $fields['payment_added'] = isset($fields['payment_added']) ?  $fields['payment_added'] : date('Y-m-d H:i:s');
        
        $this->db->set($fields);
        $this->db->insert('back_payments');
        return $this->db->insert_id();
    }
    function get_customer_balance($customer_id)
    {
        $this->db->select("SUM(total_fee) - (IFNULL((SELECT SUM(paid_amount) FROM customer_payments WHERE customer_id = $customer_id),0)) AS amount", FALSE)
                ->from('day_services')
                ->where('customer_id', $customer_id)
                ->where('payment_status', 0);
        
        $get_customer_balance_qry = $this->db->get();
        
        return $get_customer_balance_qry->row();
//        $this->db->select("SUM(total_fee) - (SELECT SUM(paid_amount) FROM customer_payments WHERE customer_id = $customer_id) AS amount", FALSE)
//                ->from('day_services')
//                ->where('customer_id', $customer_id)
//                ->where('payment_status', 0);
//        
//        $get_customer_balance_qry = $this->db->get();
//        
//        return $get_customer_balance_qry->row();
    }
    
    /*
     * @auther : Geethu     *
     */
    
    function get_bookings_by_customer_id($customer_id)
    {
        $this->db->select('b.booking_id')
                ->from('bookings b')
                ->join('customers c', 'c.customer_id = b.customer_id')
                ->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
                ->where('b.customer_id', $customer_id);
        
        $get_bookings_by_customer_id = $this->db->get();
        
        return $get_bookings_by_customer_id->num_rows();
    }
    
        function get_customer_zone_province_by_cust_id($customer_id) {
        $this->db->select('z.zone_id,ca.area_id,p.province_id,ca.customer_address_id')
                ->from('customer_addresses ca')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('province p', 'z.province_id = p.province_id')
                ->where('ca.customer_id', $customer_id)
                ->where('z.zone_status', 1)
                ->where('a.deleted_at', null)
                ->where('p.status', 1);


        $get_customer_zone_by_address_id_qry = $this->db->get();

        return $get_customer_zone_by_address_id_qry->row();
    }
        function get_customer_zone_province_by_area_id($area_id) {
        $this->db->select('a.area_id,z.zone_id,p.province_id')
                ->from('areas a')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('province p', 'z.province_id = p.province_id')
                ->where('a.area_id', $area_id)
                ->where('z.zone_status', 1)
                ->where('a.deleted_at', null)
                ->where('p.status', 1);


        $get_customer_zone_prov_By_area_qry = $this->db->get();

        return $get_customer_zone_prov_By_area_qry->row();
    }
    
    function role_exists($key)
    {
        $this->db->where('mobile_number_1',$key);
        $query = $this->db->get('customers');
        $count = $query->num_rows();
        return $count;
    } 
    
    function getcustomernamebyid($customer_id)
    {
        $this->db->select("customer_name", FALSE)
                ->from('customers')
                ->where('customer_id', $customer_id)
                ->limit(1);

        $get_customer_by_id_qry = $this->db->get();

        return $get_customer_by_id_qry->row();
    }
    
    
    
    
    /*
     * Author : Jiby
     * Purpose : Calculating customer current week Weekly booking hours
     * Date : 12-10-17
     */
        function get_bookings_data_by_customer_id($customer_id)
    {
            
        $lastSunday = new DateTime('last sunday');
        $nxtSaturday = new DateTime('next saturday');
        $this->db->select('b.booking_id,b.time_from,b.time_to')
                ->from('bookings b')
                ->join('customers c', 'c.customer_id = b.customer_id')
                ->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
                ->where('b.booking_type', 'WE')
                ->where('b.service_start_date >=', $lastSunday->format('Y-m-d'))
                ->where('b.service_start_date <=', $nxtSaturday->format('Y-m-d'))
                ->where('b.customer_id', $customer_id);
        
        $get_bookings_by_customer_id = $this->db->get();
        
        return $get_bookings_by_customer_id->result();
    }
    
    
     
       public function check_email($email) {
        $this->db->select('*')
                ->from('customers')
                ->where('email_address', $email)
                ->limit(1);
        $user = $this->db->get();
//        print_r($user->num_rows());
//        echo $this->db->last_query();exit;
        if ($user->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
       public function check_mobile($mobile) {
        $this->db->select('*')
                ->from('customers')
                ->where('phone_number', $mobile)
                ->or_where('mobile_number_1', $mobile)
                ->or_where('mobile_number_2', $mobile)
                ->or_where('mobile_number_3', $mobile)
                ->limit(1);
        $user = $this->db->get();
//        print_r($user->num_rows());
//        echo $this->db->last_query();exit;
        if ($user->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    
       public function customer_login($email,$password) {
        $this->db->select('*')
                ->from('customers')
                ->where('email_address', $email)
                ->where('customer_password', $password)
                ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
       public function get_customer_by_token($device_token,$device_type) {
        $this->db->select('*')
                ->from('customers')
                ->where('oauth_token', $device_token)
                ->where('oauth_provider', $device_type)
                ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
    
    function get_customers_by_field_value($field_name, $field_value)
	{
		$this->db->select('customer_id')
				 ->from('customers')
				 ->where($field_name, $field_value)
                                 ->limit(1);
		
		$get_customers_by_field_value_qry = $this->db->get();
		
		return $get_customers_by_field_value_qry->row();
	}
        
    function check_booking_exist($book_reference,$service_date,$starttime,$endtime)
    {
        $this->db->select('*')
                ->from('bookings')
                ->where('justmop_reference',$book_reference)
                ->where('service_start_date',$service_date)
				->where('booking_category', 'C')
                ->where('time_from',$starttime)
                ->where('time_to',$endtime)
				
                ->where('from_justmop',1)
				->where('booking_status !=',2);
        
        $check_booking_exist_qry = $this->db->get();        
        return $check_booking_exist_qry->result();       
    }
    
    function check_we_booking_assigned($customer_id,$customer_address_id,$service_week_day,$starttime,$endtime,$book_type)
    {
        $this->db->select('bookings.*')
                ->from('bookings')
                ->where('customer_id',$customer_id)
                ->where('customer_address_id',$customer_address_id)
                ->where('time_from',$starttime)
                ->where('time_to',$endtime)
                ->where('service_week_day',$service_week_day)
				->where('booking_category', 'C')
				->where('booking_type',$book_type)
                ->where('booking_status',1);
        
        $check_booking_exist_qry = $this->db->get();        
        return $check_booking_exist_qry->result();       
    }
	
	function add_ref($fields = array())
    {   
        $this->db->set($fields);
        $this->db->insert('justmop_ref'); 
        return $this->db->insert_id();
    }
	
	function check_ref_exist($booking_id,$service_date)
    {
        $this->db->select('*')
                ->from('justmop_ref')
                ->where('booking_id',$booking_id)
                ->where('service_date',$service_date);
        
        $check_booking_exist_qry = $this->db->get();        
        return $check_booking_exist_qry->result();       
    }
	
	function check_ref_exist_from_date($booking_id,$service_date)
        {
            $this->db->select('*')
                    ->from('justmop_ref')
                    ->where('booking_id',$booking_id)
                    ->where('service_date >= ',$service_date);

            $check_booking_exist_qry = $this->db->get();        
            return $check_booking_exist_qry->result();       
        }
        
        
}
