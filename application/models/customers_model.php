<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Customers_Model Class
 * 
 * @author Geethu
 * @package HM
 * @version Version 1.0
 */
class Customers_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Get Customers
     * 
     * @author Geethu
     * @access public
     * @param  bool
     * @return array
     */
    function get_customers($active_only = TRUE, $syn = FALSE)
    {

        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.fax_number, c.email_address, c.customer_status, c.odoo_customer_id, c.website_url, c.customer_type, c.payment_type, c.payment_mode, c.key_given, c.customer_password, c.customer_username, c.price_hourly, c.price_extra, c.price_weekend, c.latitude, c.longitude, c.customer_notes, ca.customer_address, a.odoo_area_id, a.area_name, z.zone_name, c.odoo_synch_status')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            ->order_by('c.customer_name', 'ASC');

        if ($active_only) {
            $this->db->where('customer_status', 1);
            $this->db->where('zone_status', 1);
            $this->db->where('area_status', 1);
        }

        if ($syn) {

            $this->db->where('odoo_synch_status', 0);
            //$this->db->where_not_in('c.customer_id', array(5132,310,8579,3596,1684,8632,3734,9071,8587,5626));
            //$this->db->limit(1000, 9000);
        }


        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    function get_customers_byid($id)
    {

        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.fax_number, c.email_address, c.customer_status, c.odoo_customer_id, c.website_url, c.customer_type, c.payment_type, c.payment_mode, c.key_given, c.customer_password, c.customer_username, c.price_hourly, c.price_extra, c.price_weekend, c.latitude, c.longitude, c.customer_notes, ca.customer_address, a.odoo_area_id, a.area_name, z.zone_name, c.odoo_synch_status')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            ->order_by('c.customer_name', 'ASC');

        $this->db->where('customer_status', 1);
        $this->db->where('zone_status', 1);
        $this->db->where('area_status', 1);
        $this->db->where('c.customer_id', $id);
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }
    function get_customer($id)
    {
        $this->db->select('c.*')
            ->from('customers as c')
            ->where('c.customer_id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function get_customers_for_search()
    {

        $this->db->select('c.customer_id, c.customer_name')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            ->order_by('c.customer_name', 'ASC');


        $this->db->where('customer_status', 1);
        $this->db->where('zone_status', 1);
        $this->db->where('area_status', 1);





        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    function report_srch_usr($searchTerm)
    {
        $this->db->select('c.customer_id, c.customer_name')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            ->order_by('c.customer_name', 'ASC');
        if (isset($searchTerm) && strlen($searchTerm) > 0) {
            $this->db->like('c.customer_name', $searchTerm);
        }
        $this->db->limit(50);
        $this->db->where('customer_status', 1);
        $this->db->where('zone_status', 1);
        $this->db->where('area_status', 1);





        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    function get_customer_zone_by_address_id($customer_address_id)
    {
        $this->db->select('z.zone_id')
            ->from('customer_addresses ca')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('ca.customer_address_id', $customer_address_id)
            ->where('z.zone_status', 1)
            ->where('a.deleted_at', null);


        $get_customer_zone_by_address_id_qry = $this->db->get();

        return $get_customer_zone_by_address_id_qry->row();
    }

    function get_customer_by_id($customer_id)
    {
        $this->db->select("customer_id, odoo_customer_id, odoo_synch_status, customer_name,customer_username, customer_password, customer_nick_name, mobile_number_1, mobile_number_2, mobile_number_3, phone_number, fax_number, trnnumber, vatnumber, email_address, website_url, customer_photo_file, customer_booktype, customer_type, contact_person, payment_type, payment_mode, price_hourly, price_extra, price_weekend, latitude, longitude, key_given, customer_notes, DATE_FORMAT(customer_added_datetime, '%d / %M / %Y %h:%i %p') AS added_datetime, customer_status,balance, signed, is_flag, flag_reason", FALSE)
            ->from('customers')
            ->where('customer_id', $customer_id)
            ->limit(1);

        $get_customer_by_id_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customer_by_id_qry->row();
    }

    /**
     * Get customer address
     * 
     * @author  Azinova     * 
     * @access  public
     * @param   int, bool
     * @return array
     */
    function get_customer_addresses($customer_id, $active_only = TRUE)
    {
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.building, ca.customer_address, a.area_name, z.zone_id, z.zone_name,ca.latitude,ca.longitude')
            ->from('customer_addresses as ca')
            ->where('ca.customer_id', $customer_id)
            ->where('ca.address_status', 0)
            ->join('areas as a', 'ca.area_id = a.area_id')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            ->order_by('ca.customer_address_id');

        if ($active_only) {
            $this->db->where('z.zone_status', 1);
            $this->db->where('a.deleted_at', null);
        }

        $get_customer_addresses_qry = $this->db->get();

        return $get_customer_addresses_qry->result();
    }

    function add_customer_payment($fields = array())
    {
        $fields['paid_datetime'] = isset($fields['paid_datetime']) ? $fields['paid_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('customer_payments');
        return $this->db->insert_id();
    }
    function update_customer_payment($where = array(),$data = array())
    {
        // update customer payment
		$this->db->where($where);
		foreach($data as $field => $value){
			$this->db->set($field,$value, FALSE);
		}
		$this->db->update('customer_payments');
		return $this->db->affected_rows();
    }

    /**
     * Add customer
     * 
     * @author  Azinova Develpers      
     * @access  public
     * @param   
     * @return 
     */
    function add_customers($data)
    {
        $this->db->set($data);
        $this->db->insert('customers');
        $result = $this->db->insert_id();
      // print_r( $this->db->last_query());die;
        return $result;
    }

    function add_user_actvty($data)
    {
        $this->db->set($data);
        $this->db->insert('user_activity');
        $result = $this->db->insert_id();
        return $result;
    }

    function add_customer_address($datas)
    {
        $this->db->set($datas);
        $this->db->insert('customer_addresses');
        $result = $this->db->insert_id();
    }

    /**
     * Get all customer 
     * 
     * @author  Betsy
     * @access  public
     * @param   
     * @return  array
     */
    function get_all_customers()
    {
        $this->db->select('customers.*')
            ->from('customers');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_customer_details($customer_id)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('customers.customer_id', $customer_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_customer_address($cust_id)
    {

        $query = $this->db->query('select customer_addresses.customer_address_id, customer_addresses.building, customer_addresses.unit_no, customer_addresses.street, customer_addresses.customer_id, customer_addresses.area_id,customer_addresses.latitude,customer_addresses.longitude,customer_addresses.customer_address, areas.area_name, customers.customer_name, z.zone_name
from customer_addresses
join areas ON customer_addresses.area_id = areas.area_id
join zones z ON areas.zone_id = z.zone_id
join customers ON customer_addresses.customer_id = customers.customer_id
where customer_addresses.address_status = 1 AND customer_addresses.customer_id = ' . $cust_id);
        return $query->result_array();
    }

    function update_customers($data, $customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
        return $this->db->affected_rows();
    }
    function update_customer_row($customer_id,$data)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
        return $this->db->affected_rows();
    }

    function update_booktype($data, $customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
    }

    function delete_customer_address($customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_addresses');
    }

    function update_customer_address($datas, $customer_address_id)
    {



        //$customer_address_id = $this->get_customer_address_by_area($datas['customer_id'], $datas['area_id']);
        if ($customer_address_id) {
            $this->db->where('customer_address_id', $customer_address_id);
            $this->db->update('customer_addresses', $datas);
            $result = $customer_address_id;
        } else {
            $this->db->set($datas);
            $this->db->insert('customer_addresses');
            $result = $this->db->insert_id();
        }
        return $result;
    }

    function get_customer_address_by_area($customer_id, $area_id)
    {
        $this->db->select('customer_address_id')
            ->from('customer_addresses')
            ->where('customer_id', $customer_id)
            ->where('area_id', $area_id)
            ->limit(1);

        $get_customer_address_by_area_qry = $this->db->get();

        $customer_address = $get_customer_address_by_area_qry->row();
        //print_r($customer_address);
        //exit;
        if ($customer_address->customer_address_id > 0)
            return $customer_address->customer_address_id;
        else
            return FALSE;
    }

    function remove_customer_address($address_id)
    {
        $data = array();
        $data['address_status'] = 1;
        $this->db->where('customer_address_id', $address_id);
        //$this->db->delete('customer_addresses');
        $this->db->update('customer_addresses', $data);
    }

    function get_all_customer_details($customer_id)
    {
        $res['customers'] = array();
        $query = $this->db->query('select * from customers where customers.customer_id = ' . $customer_id);
        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $address = array();

                $cust_id = $row['customer_id'];

                $address_query = $this->db->query('select customer_addresses.customer_address_id, customer_addresses.customer_id, customer_addresses.area_id, customer_addresses.customer_address, areas.area_name, customers.customer_name
from customer_addresses
join areas ON customer_addresses.area_id = areas.area_id
join customers ON customer_addresses.customer_id = customers.customer_id
where customer_addresses.customer_id = ' . $cust_id);
                if ($address_query->num_rows() > 0) {

                    foreach ($address_query->result_array() as $rows) {
                        array_push($address, $rows);
                        $row['address'] = $address;
                    }
                    array_push($res['customers'], $row);
                }
            }
        }
        return $res['customers'];
    }

    function delete_customer($customer_id, $status)
    {
        $this->db->set('customer_status', $status);
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers');
    }

    public function record_count($active)
    {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status, ca.customer_address, a.area_name, z.zone_name")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $get_active_customer_qry = $this->db->get();

        return $get_active_customer_qry->num_rows();

        /* if ($active == 1) {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas','ca.area_id=a.area_id');
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          } else if ($active == 2) {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas a','ca.area_id=a.area_id')->where('c.customer_status', 1);
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          } else {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas a','ca.area_id=a.area_id')->where('c.customer_status', 0);
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          }
         */
    }

    public function fetch_customers($limit, $start, $active = 1)
    {

        $this->db->select("c.customer_id, c.customer_name, c.is_flag, c.flag_reason, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $this->db->where('ca.address_status', 0);
        $this->db->group_by('c.customer_id');
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_last_job_date_by_customerid($customer_id)
    {
        $this->db->select('service_date')
            ->from('day_services')
            ->where('customer_id', $customer_id)
            ->order_by('service_date', 'desc')
            ->limit(1);
        $get_last_job_date_by_customerid_qry = $this->db->get();
        return $get_last_job_date_by_customerid_qry->row();
    }

    public function fetch_customers_excel($active = 1)
    {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.email_address, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $this->db->group_by('c.customer_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_total_customers_counts()
    {

        $this->db->select("count(c.customer_id) as total")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $get_active_customer_qry = $this->db->get();
        return $get_active_customer_qry->row_array()['total'];
    }

    function get_customers_by_keyword($keyword)
    {
        $keyword = strtolower($keyword);

        $this->db->select("c.customer_id, c.customer_name, c.is_flag, c.flag_reason, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');

        $customer_field = $this->db->list_fields('customers');
        foreach ($customer_field as $field) {
            $this->db->or_like("LOWER(c." . $field . ")", $keyword);
        }
        $customer_address_field = $this->db->list_fields('customer_addresses');
        foreach ($customer_address_field as $field) {
            $this->db->or_like("LOWER(ca." . $field . ")", $keyword);
        }
        $area_field = $this->db->list_fields('areas');
        foreach ($area_field as $field) {
            $this->db->or_like("LOWER(a." . $field . ")", $keyword);
        }
        $zone_field = $this->db->list_fields('zones');
        foreach ($zone_field as $field) {
            $this->db->or_like("LOWER(z." . $field . ")", $keyword);
        }

        $this->db->group_by('c.customer_id');

        $get_customers_by_keyword_query = $this->db->get();

        //echo $this->db->last_query();exit;
        return $get_customers_by_keyword_query->result();
    }

    /**
     * Get customer address by id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_customer_address_by_id($customer_address_id)
    {
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address,ca.building, a.area_name, z.zone_id, z.zone_name')
            ->from('customer_addresses ca')
            ->where('ca.customer_address_id', $customer_address_id)
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->order_by('ca.customer_address_id');

        $get_customer_address_by_id_qry = $this->db->get();

        return $get_customer_address_by_id_qry->row();
    }

    /**
     * Get maid history by customer id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_maid_history($customer_id)
    {
        $this->db->select('m.maid_name, DATE_FORMAT(ds.service_date, "%d/%m/%Y") AS service_date, DATE_FORMAT(ds.start_time, "%h:%i %p") AS start_time, DATE_FORMAT(ds.end_time,"%h:%i %p") AS end_time, IF(ds.service_status = 1,"Service Started", IF(ds.service_status = 2, "Service Finished","Service not Done")) as service_status, DATE_FORMAT(b.time_from,"%h:%i %p") AS start_from_time, DATE_FORMAT(b.time_to,"%h:%i %p") AS end_to_time', FALSE)
            ->from('day_services ds')
            ->where('ds.customer_id', $customer_id)
            ->where('ds.service_status !=', 3, FALSE)
            ->join('customers c', 'ds.customer_id = c.customer_id')
            ->join('maids m', 'ds.maid_id = m.maid_id')
            ->join('bookings b', 'ds.booking_id = b.booking_id')

            ->order_by('ds.service_date', 'desc');

        $get_maid_history_qry = $this->db->get();

        return $get_maid_history_qry->result();
    }

    /**
     * Get payment history by customer id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_payment_history($customer_id)
    {
        $this->db->select('m.maid_name, DATE_FORMAT(ds.service_date, "%d/%m/%Y") AS service_date, ds.total_fee, cp.paid_amount, DATE_FORMAT(ds.start_time, "%h:%i %p") AS start_time, DATE_FORMAT(ds.end_time,"%h:%i %p") AS end_time, DATE_FORMAT(b.time_from,"%h:%i %p") AS start_from_time, DATE_FORMAT(b.time_to,"%h:%i %p") AS end_to_time', FALSE)
            ->from('day_services ds')
            ->where('ds.customer_id', $customer_id)
            ->where('ds.payment_status', '1')
            ->join('customers c', 'ds.customer_id = c.customer_id')
            ->join('maids m', 'ds.maid_id = m.maid_id')
            //added extra by vishnu
            ->join('bookings b', 'ds.booking_id = b.booking_id')
            ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id')
            //ends
            ->order_by('ds.service_date', 'desc');

        $get_maid_history_qry = $this->db->get();

        return $get_maid_history_qry->result();
    }

    function get_current_service($customer_id)
    {
        $service_date = date('Y-m-d');
        $service_week_day = date('w', strtotime($service_date));

        //$this->db->select('b.maid_id, m.maid_name, m.maid_nationality, m.maid_photo_file, CONCAT( GROUP_CONCAT(CONCAT( DATE_FORMAT(b.time_from, "%h:%i %p") ,"-", DATE_FORMAT(b.time_to,"%h:%i %p")) SEPARATOR ","), " [", (IF(b.booking_type = "WE", IF( b.service_week_day = 0, "Sunday", SUBSTRING_INDEX(SUBSTRING_INDEX("Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",",",b.service_week_day),",",-1)), DATE_FORMAT(b.service_start_date, "%d/%m/%Y"))), "]") AS shifts, b.booking_type', FALSE)
        $this->db->select('b.maid_id,b.booking_id, m.maid_name, m.maid_nationality, m.maid_photo_file, CONCAT( GROUP_CONCAT(CONCAT( DATE_FORMAT(b.time_from, "%h:%i %p") ,"-", DATE_FORMAT(b.time_to,"%h:%i %p")) SEPARATOR ","), " [", (IF(b.booking_type = "WE", IF( b.service_week_day = 0, "Sunday", SUBSTRING_INDEX(SUBSTRING_INDEX("Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",",",b.service_week_day),",",-1)), DATE_FORMAT(b.service_start_date, "%d/%m/%Y"))), "]") AS shifts, b.booking_type', FALSE)
            ->from('bookings b')
            ->where('b.customer_id', $customer_id)
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', '1')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR ((b.service_start_date >= " . $this->db->escape($service_date) . " OR b.service_start_date <= " . $this->db->escape($service_date) . ") AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->group_by('b.maid_id, b.booking_type, b.service_start_date')
            ->order_by('b.maid_id');

        $get_maid_history_qry = $this->db->get();


        return $get_maid_history_qry->result();
    }

    function getthisdata()
    {
        $this->db->select("c.customer_id")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $this->db->group_by('c.customer_id');
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->result();
    }

    function get_booking_deletes_by()
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        $get_booking_deletes_by_date_qry = $this->db->get();
        return $get_booking_deletes_by_date_qry->result();
    }

    function getbookcount($custid)
    {
        //        $deletes = $this->get_booking_deletes_by();
        //        $deleted_bookings = array();
        //        foreach($deletes as $delete)
        //        {
        //                $deleted_bookings[] = $delete->booking_id;
        //        }
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('c.customer_id', $custid)
            ->order_by('m.maid_name')
            ->order_by('b.time_from');

        //        if(count($deleted_bookings) > 0)
        //        {
        //                $this->db->where_not_in('b.booking_id', $deleted_bookings);
        //        }
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->num_rows();
    }

    public function getdata()
    {
        $customers = $this->getthisdata();
        $dates = date('Y-m-d');
        $datas = array();
        $test = array();
        foreach ($customers as $row) {
            $custid = $row->customer_id;
            $bookingcount = $this->getbookcount($custid);
            $datas['cid'] = $custid;
            $datas['count'] = $bookingcount;
            array_push($test, $datas);
        }
        return $test;
    }

    public function search_by_date($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 1, $sortsources, $cust_type = NULL)
    {
        if ($sort_customers == 3) {
            $this->db->select('b.customer_id,b.booking_id, COUNT(b.customer_id) AS count', false)
                ->from('bookings b')
                ->where('b.booking_category', 'C')
                ->group_by('b.customer_id');
            $this->db->having('count = 1', false);
            $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes)', NULL, FALSE);
            $items = $this->db->get()->result();
            foreach ($items as $rowitem) {
                $cus_id = $rowitem->customer_id;
            }

            return $items;
        } else {
            $date = date('Y-m-d');
            $service_week_day = date('w', strtotime($date));

            $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,ca.customer_address,ca.building, ca.unit_no,ca.street, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
            if (is_numeric($cust_type)) {
                $this->db->where("c.customer_booktype", $cust_type);
            }
            if ($sort_customers == 2) {
                $this->db->join('bookings b', 'c.customer_id = b.customer_id');
                $this->db->where('b.booking_category', 'C');
                $this->db->where('b.booking_status', 1);
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            }
            if ($all_customers == 2) {
                $this->db->where('c.customer_status', 1);
            } else if ($all_customers == 3) {
                $this->db->where('c.customer_status', 0);
            }
            if ($payment_type) {
                $this->db->where("c.payment_type", $payment_type);
            }
            if ($sortsources) {
                $this->db->where("c.customer_source", $sortsources);
            }
            if ($from_date && $to_date) {
                $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$from_date' AND '$to_date'");
            }
            if (($from_date != "" && $to_date == "") || ($from_date == "" && $to_date != "")) {
                $this->db->where("DATE(`customer_added_datetime`) = '$from_date' OR  DATE(`customer_added_datetime`) ='$to_date'");
            }
            if ($from_date == "" && $to_date == "" && $payment_type == "") {
                $this->db->group_by('c.customer_id');
                //$this->db->limit(100);   

            }


            return $this->db->get()->result();
            //echo $this->db->last_query();exit();
        }
    }

    public function search_by_date1($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 1, $cust_type = NULL)
    {


        //$service_week_day = date('w', strtotime($date));
        $fromDate = date("Y-m-d", strtotime($from_date));
        $toDate = date("Y-m-d", strtotime($to_date));

        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, ca.building,ca.unit_no,ca.street, a.area_name, z.zone_name")
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id');
        if (is_numeric($cust_type)) {
            $this->db->where("c.customer_booktype", $cust_type);
        }
        if ($sort_customers == 2) {
            $this->db->join('bookings b', 'c.customer_id = b.customer_id');
            $this->db->where('b.booking_status', 1);
            $this->db->where('b.booking_category', 'C');

            //            if ($from_date && $to_date) {
            //                $fromDate = date("Y-m-d", strtotime($from_date));
            //                $toDate = date("Y-m-d", strtotime($to_date));
            //                $this->db->where("(((b.service_actual_end_date BETWEEN '$fromDate' AND '$toDate') AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            //                $this->db->where("((b.service_start_date BETWEEN '$fromDate' AND '$toDate'))",NULL, FALSE);               
            //}else
            if (($from_date != "" && $to_date == "")) {
                if ($from_date == "") {
                    $fromDate = "";
                } else {
                    $fromDate = date("Y-m-d", strtotime($from_date));
                    $service_week_day1 = date('w', strtotime($fromDate));
                }
                //$fromDate = date("Y-m-d", strtotime($from_date));
                if ($to_date == "") {
                    $toDate = "";
                } else {
                    $toDate = date("Y-m-d", strtotime($to_date));
                }
                //$toDate = date("Y-m-d", strtotime($to_date));
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($fromDate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($fromDate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($fromDate) . " AND service_week_day = " . $service_week_day1 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($fromDate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($fromDate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            } else if (($from_date == "" && $to_date != "")) {
                if ($from_date == "") {
                    $fromDate = "";
                } else {
                    $fromDate = date("Y-m-d", strtotime($from_date));
                }
                //$fromDate = date("Y-m-d", strtotime($from_date));
                if ($to_date == "") {
                    $toDate = "";
                } else {
                    $toDate = date("Y-m-d", strtotime($to_date));
                    $service_week_day2 = date('w', strtotime($toDate));
                }
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($toDate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($toDate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($toDate) . " AND service_week_day = " . $service_week_day2 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($toDate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($toDate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            } else {
                $date = date('Y-m-d');
                $newdate = date("Y-m-d", strtotime($date));
                $service_week_day3 = date('w', strtotime($date));
                //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($newdate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($newdate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($newdate) . " AND service_week_day = " . $service_week_day3 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($newdate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($newdate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            }
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if ($payment_type) {
            $this->db->where("c.payment_type", $payment_type);
        }

        $this->db->group_by('c.customer_id');
        return $this->db->get()->result();
        //echo $this->db->last_query();exit();


    }


    public function search_by_date_sort($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 3, $cust_type = NULL)
    {
        $date = date('Y-m-d');
        $this->db->select('b.customer_id, COUNT(b.customer_id) AS count', false)
            ->from('bookings b')
            ->where('b.booking_category', 'C')
            ->join('customers c', 'b.customer_id = c.customer_id');

        if (is_numeric($cust_type)) {
            $this->db->where("c.customer_booktype", $cust_type);
        }


        $this->db->group_by('b.customer_id');
        $this->db->having('count = 1', false);
        $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes where service_date = ' . $date . ')', NULL, FALSE);
        $items = $this->db->get()->result();
        return $items;
    }
    public function search_by_date_sort1($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 3, $cust_type = NULL)
    {
        //$date = date('Y-m-d');
        $fromDate = date("Y-m-d", strtotime($from_date));
        $toDate = date("Y-m-d", strtotime($to_date));
        $this->db->select('b.customer_id, COUNT(b.customer_id) AS count', false)
            ->from('bookings b')
            ->where('b.booking_category', 'C')
            ->join('customers c', 'b.customer_id = c.customer_id');
        if (is_numeric($cust_type)) {
            $this->db->where("c.customer_booktype", $cust_type);
        }
        $this->db->group_by('b.customer_id');
        $this->db->having('count = 1', false);
        $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes where service_date BETWEEN ' . $fromDate . ' AND ' . $toDate . ')', NULL, FALSE);
        $items = $this->db->get()->result();
        return $items;
    }
    public function search_cust_by_booking($custid, $from_date, $to_date, $payment_type, $all_customers = 1)
    {
        //$originalDate = "2010-03-21";


        $date = date('Y-m-d');
        //$service_week_day = date('w', strtotime($date));
        $this->db->select("b.customer_id,c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name")
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('b.customer_id', $custid);
        if ($from_date && $to_date) {
            $fromDate = date("Y-m-d", strtotime($from_date));
            $toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$fromDate' AND '$toDate'");
        }
        if (($from_date != "" && $to_date == "")) {
            if ($from_date == "") {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if ($to_date == "") {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            //$toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("DATE(`customer_added_datetime`) = '$fromDate'");
        }
        if (($from_date == "" && $to_date != "")) {
            if ($from_date == "") {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if ($to_date == "") {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            $this->db->where("DATE(`customer_added_datetime`) ='$toDate'");
        }
        //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
        $this->db->where('b.service_start_date', $date);
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if ($payment_type) {
            $this->db->where("c.payment_type", $payment_type);
        }

        return $this->db->get()->result();
    }

    public function search_cust_by_booking1($custid, $from_date, $to_date, $payment_type, $all_customers = 1)
    {
        //$originalDate = "2010-03-21";


        $date = date('Y-m-d');
        //$service_week_day = date('w', strtotime($date));
        $this->db->select("b.customer_id,c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, ca.building,ca.unit_no,ca.street, a.area_name, z.zone_name")
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('b.customer_id', $custid);
        if ($from_date && $to_date) {
            $fromDate = date("Y-m-d", strtotime($from_date));
            $toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("b.service_start_date BETWEEN '$fromDate' AND '$toDate'");
        } else if (($from_date != "" && $to_date == "")) {
            if ($from_date == "") {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if ($to_date == "") {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            //$toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("b.service_start_date = '$fromDate'");
        } else if (($from_date == "" && $to_date != "")) {
            if ($from_date == "") {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if ($to_date == "") {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            $this->db->where("b.service_start_date ='$toDate'");
        } else {
            //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
            $this->db->where('b.service_start_date', $date);
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if ($payment_type) {
            $this->db->where("c.payment_type", $payment_type);
        }

        return $this->db->get()->result();
    }

    function get_back_payment($date)
    {
        $this->db->select('c.customer_id, c.customer_name, c.payment_type, z.zone_name, b.requested_amount, b.collected_amount, DATE_FORMAT(b.collected_date, "%d/%m/%Y") AS collected_date, DATE_FORMAT(b.collected_time, "%h:%i %p") AS collected_time', FALSE)
            ->from('back_payments b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            //edited by vishnu
            //->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            //->join('areas a', 'ca.area_id = a.area_id')
            //->join('zones z', 'a.zone_id = z.zone_id')
            ->join('zones z', 'b.zone_id = z.zone_id')
            //ends
            ->where('b.collected_date', $date);

        $get_back_payment = $this->db->get();

        return $get_back_payment->result();
    }
    function add_backpayment($fields)
    {
        $fields['payment_added'] = isset($fields['payment_added']) ?  $fields['payment_added'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('back_payments');
        return $this->db->insert_id();
    }
    function get_customer_balance($customer_id)
    {
        $this->db->select("SUM(total_fee) - (IFNULL((SELECT SUM(paid_amount) FROM customer_payments WHERE customer_id = $customer_id),0)) AS amount", FALSE)
            ->from('day_services')
            ->where('customer_id', $customer_id)
            ->where('payment_status', 0);

        $get_customer_balance_qry = $this->db->get();

        return $get_customer_balance_qry->row();
        //        $this->db->select("SUM(total_fee) - (SELECT SUM(paid_amount) FROM customer_payments WHERE customer_id = $customer_id) AS amount", FALSE)
        //                ->from('day_services')
        //                ->where('customer_id', $customer_id)
        //                ->where('payment_status', 0);
        //        
        //        $get_customer_balance_qry = $this->db->get();
        //        
        //        return $get_customer_balance_qry->row();
    }
	
	function get_outstanding_balance($customer_id)
    {
        $this->db->select("balance AS amount,quickbook_id", FALSE)
            ->from('customers')
            ->where('customer_id', $customer_id);

        $get_customer_balance_qry = $this->db->get();

        return $get_customer_balance_qry->row();
    }

    /*
     * @auther : Geethu     *
     */

    function get_bookings_by_customer_id($customer_id)
    {
        $this->db->select('b.booking_id')
            ->from('bookings b')
            ->join('customers c', 'c.customer_id = b.customer_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('b.customer_id', $customer_id);

        $get_bookings_by_customer_id = $this->db->get();

        return $get_bookings_by_customer_id->num_rows();
    }

    function get_bookings_by_customer_id_new($customer_id)
    {
        $this->db->select('b.booking_id')
            ->from('bookings b')
            ->join('customers c', 'c.customer_id = b.customer_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('b.booking_status', 1)
            ->where('b.service_actual_end_date >=', date('Y-m-d'))
            ->where('b.customer_id', $customer_id);

        $get_bookings_by_customer_id = $this->db->get();

        return $get_bookings_by_customer_id->num_rows();
    }

    function get_customer_zone_province_by_cust_id($customer_id)
    {
        $this->db->select('z.zone_id,ca.area_id,p.province_id,ca.customer_address_id')
            ->from('customer_addresses ca')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left')
            ->join('province p', 'z.province_id = p.province_id', 'left')
            ->where('ca.customer_id', $customer_id);
        //->where('z.zone_status', 1)
        //->where('a.deleted_at', null)
        //->where('p.status', 1);


        $get_customer_zone_by_address_id_qry = $this->db->get();

        return $get_customer_zone_by_address_id_qry->row();
    }
    function get_customer_zone_province_by_area_id($area_id)
    {
        $this->db->select('a.area_id,z.zone_id,p.province_id')
            ->from('areas a')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('province p', 'z.province_id = p.province_id')
            ->where('a.area_id', $area_id)
            ->where('z.zone_status', 1)
            ->where('a.deleted_at', null)
            ->where('p.status', 1);


        $get_customer_zone_prov_By_area_qry = $this->db->get();

        return $get_customer_zone_prov_By_area_qry->row();
    }

    function role_exists($key)
    {
        $this->db->where('mobile_number_1', $key);
        $query = $this->db->get('customers');
        $count = $query->num_rows();
        return $count;
    }

    function getcustomernamebyid($customer_id)
    {
        $this->db->select("customer_name", FALSE)
            ->from('customers')
            ->where('customer_id', $customer_id)
            ->limit(1);

        $get_customer_by_id_qry = $this->db->get();

        return $get_customer_by_id_qry->row();
    }




    /*
     * Author : Jiby
     * Purpose : Calculating customer current week Weekly booking hours
     * Date : 12-10-17
     */
    function get_bookings_data_by_customer_id($customer_id)
    {

        $lastSunday = new DateTime('last sunday');
        $nxtSaturday = new DateTime('next saturday');
        $this->db->select('b.booking_id,b.time_from,b.time_to')
            ->from('bookings b')
            ->join('customers c', 'c.customer_id = b.customer_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('b.booking_type', 'WE')
            ->where('b.service_start_date >=', $lastSunday->format('Y-m-d'))
            ->where('b.service_start_date <=', $nxtSaturday->format('Y-m-d'))
            ->where('b.customer_id', $customer_id);

        $get_bookings_by_customer_id = $this->db->get();

        return $get_bookings_by_customer_id->result();
    }



    public function check_email($email)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('email_address', $email)
            ->limit(1);
        $user = $this->db->get();
        //        print_r($user->num_rows());
        //        echo $this->db->last_query();exit;
        if ($user->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function check_mobile($mobile)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('phone_number', $mobile)
            ->or_where('mobile_number_1', $mobile)
            ->or_where('mobile_number_2', $mobile)
            ->or_where('mobile_number_3', $mobile)
            ->limit(1);
        $user = $this->db->get();
        //        print_r($user->num_rows());
        //        echo $this->db->last_query();exit;
        if ($user->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }


    public function customer_login($email, $password)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('email_address', $email)
            ->where('customer_password', $password)
            ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
    public function get_customer_by_token($device_token, $device_type)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('oauth_token', $device_token)
            ->where('oauth_provider', $device_type)
            ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }

    function get_customers_by_field_value($field_name, $field_value)
    {
        $this->db->select('customer_id')
            ->from('customers')
            ->where($field_name, $field_value)
            ->limit(1);

        $get_customers_by_field_value_qry = $this->db->get();

        return $get_customers_by_field_value_qry->row();
    }

    function get_customers_odoo()
    {
        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.is_company, c.company_name, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.fax_number, c.email_address, c.customer_status, c.odoo_customer_id, c.website_url, c.customer_type,c.customer_booktype, c.payment_type, c.payment_mode, c.key_given, c.customer_password, c.customer_username, c.price_hourly, c.price_extra, c.price_weekend, c.latitude, c.longitude, c.customer_notes, ca.customer_address, ca.area_id,ca.building,ca.unit_no,ca.street, a.odoo_area_id, a.area_name, z.zone_name, c.odoo_synch_status')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            //->order_by('c.customer_name', 'ASC');
            ->order_by('c.customer_id', 'ASC');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.odoo_synch_status', 0);
        $this->db->where('ca.default_address', 1);
        $this->db->limit(250);
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    public function get_company_odoo_details($name)
    {
        $this->db->select('*')
            ->from('odoo_values')
            ->where('field_name', $name)
            ->limit(1);
        $user = $this->db->get();
        //echo $this->db->last_query();exit;
        return $user->row();
    }

    public function get_odoo_by_customerid($customerid)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('customer_id', $customerid)
            ->limit(1);
        $user = $this->db->get();
        //echo $this->db->last_query();exit;
        return $user->row();
    }

    function get_customers_detail_odoo($customer_id)
    {
        $this->db->select('c.customer_id, c.customer_name, c.customer_source, c.customer_nick_name, c.mobile_number_1, c.is_company, c.company_name, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.fax_number, c.email_address, c.customer_status, c.odoo_customer_id, c.website_url, c.customer_type,c.customer_booktype, c.payment_type, c.payment_mode, c.key_given, c.customer_password, c.customer_username, c.price_hourly, c.price_extra, c.price_weekend, c.latitude, c.longitude, c.customer_notes, ca.customer_address, ca.area_id, a.odoo_area_id, a.area_name, z.zone_name, c.odoo_synch_status')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id');
        //$this->db->where('c.customer_status', 1);
        $this->db->where('c.customer_id', $customer_id);
        $this->db->where('ca.default_address', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }

    public function get_customer_by_type($type)
    {
        $this->db->select('c.customer_id, c.customer_name, c.mobile_number_1,c.email_address')
            ->from('customers c');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.customer_booktype', $type);
        $this->db->where('c.is_company', 'N');
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->result();
    }


    function add_sms_group($data)
    {
        $this->db->set($data);
        $this->db->insert('sms_groups');
        $result = $this->db->insert_id();
        return $result;
    }


    function add_group_numbers($data)
    {
        $this->db->set($data);
        $this->db->insert('sms_group_numbers');
        $result = $this->db->insert_id();
        return $result;
    }


    function get_sms_groups()
    {
        $this->db->select('*')
            ->from('sms_groups s');
        $this->db->where('s.sms_group_status', 1);
        $sms_groups_qry = $this->db->get();
        return $sms_groups_qry->result();
    }

    function get_sms_group_numbers($group_id)
    {
        $this->db->select('*')
            ->from('sms_group_numbers n');
        $this->db->where('n.number_group_id', $group_id);
        $this->db->where('n.number_status', 1);
        $sms_numbers_qry = $this->db->get();
        return $sms_numbers_qry->result();
    }

    function add_email_group($data)
    {
        $this->db->set($data);
        $this->db->insert('email_groups');
        $result = $this->db->insert_id();
        return $result;
    }

    function add_group_emails($data)
    {
        $this->db->set($data);
        $this->db->insert('email_group_numbers');
        $result = $this->db->insert_id();
        return $result;
    }

    function get_email_groups()
    {
        $this->db->select('*')
            ->from('email_groups s');
        $this->db->where('s.email_group_status', 1);
        $email_groups_qry = $this->db->get();
        return $email_groups_qry->result();
    }

    function get_email_group_list($group_id)
    {
        $this->db->select('*')
            ->from('email_group_numbers n');
        $this->db->where('n.email_group_id', $group_id);
        $this->db->where('n.email_status', 1);
        $sms_numbers_qry = $this->db->get();
        return $sms_numbers_qry->result();
    }

    function report_srch_usr_news($searchterm)
    {
        $this->db->select('c.*')
            ->from('customers as c')
            ->join('customer_addresses as ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas as a', 'ca.area_id = a.area_id', 'left')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            ->group_by('c.customer_id')
            ->order_by('c.customer_id', 'DESC');
        if (isset($searchterm) && strlen($searchterm) > 0) {
            $this->db->where("(c.customer_name LIKE '%" . $searchterm . "%' OR c.customer_nick_name LIKE '%" . $searchterm . "%' OR c.mobile_number_1 LIKE '%" . $searchterm . "%' OR c.mobile_number_2 LIKE '%" . $searchterm . "%' OR c.mobile_number_3 LIKE '%" . $searchterm . "%' OR c.phone_number LIKE '%" . $searchterm . "%' OR c.customer_code LIKE '%" . $searchterm . "%')", NULL, FALSE);
            //$this->db->like('c.customer_name', $searchTerm);
        }
        $this->db->limit(50);
        $this->db->where('c.customer_status', 1);
        //$this->db->where('zone_status', 1);
        //$this->db->where('area_status', 1);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    function report_srch_usr_news_new($searchterm)
    {
        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.customer_status')
            ->from('customers c')
            ->order_by('c.customer_id', 'DESC');
        if (isset($searchterm) && strlen($searchterm) > 0) {
            $this->db->where("(customer_name LIKE '%" . $searchterm . "%' OR customer_nick_name LIKE '%" . $searchterm . "%' OR mobile_number_1 LIKE '%" . $searchterm . "%' OR mobile_number_2 LIKE '%" . $searchterm . "%' OR mobile_number_3 LIKE '%" . $searchterm . "%' OR phone_number LIKE '%" . $searchterm . "%')", NULL, FALSE);
        }
        $this->db->limit(50);
        $this->db->where('customer_status', 1);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->result();
    }

    function get_customer_new_odoolist()
    {
        $this->db->select('c.*,ca.*,a.*,z.*')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            //->order_by('c.customer_name', 'ASC');
            ->order_by('c.customer_id', 'ASC');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.odoo_customer_new_status', 0);
        $this->db->where('ca.default_address', 1);
        $this->db->limit(100);
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    public function get_customer_details_by_customerid($customerid)
    {
        $this->db->select('c.customer_id,c.payment_frequency_id,c.customer_dealer_id,c.refer_source_id,c.customer_type_id,c.customer_company_id,c.odoo_package_customer_status, c.odoo_package_customer_id, c.customer_type,c.payment_type,c.payment_mode,c.key_given,c.is_company,c.company_name,c.customer_booktype,c.customer_name,c.mobile_number_1,c.customer_nick_name, ca.area_id, ca.customer_address, ca.building, c.customer_source,c.website_url,c.phone_number,c.mobile_number_2,c.mobile_number_3,c.fax_number,c.email_address,c.customer_notes,c.price_hourly,c.price_extra,c.price_weekend,c.latitude,c.longitude,c.trnnumber,c.vatnumber')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->where('c.customer_id', $customerid)
            ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }

      public function get_customer_loc_address($cust_id)
    {
        $this->db->select('ca.customer_address_id, ca.makkani_number,ca.latitude,ca.longitude,ca.building,ca.unit_no, ca.street, ca.residence_type,ca.customer_id, ca.area_id,ca.latitude,ca.longitude,ca.customer_address, ca.location_id, ca.landmark_id, a.area_name, c.customer_name, z.zone_name, z.zone_id,loc.location_name, land.landmark_name')
        ->from('customer_addresses as ca')
        ->join('areas a', 'ca.area_id = a.area_id')
            // ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('locations loc', 'ca.location_id = loc.location_id')
            ->join('landmarks land', 'ca.landmark_id = land.landmark_id')
            ->join('customers c', 'ca.customer_id = c.customer_id')
            ->where('ca.customer_id', $cust_id)
            ->where('ca.address_status', 0);

        $query = $this->db->get();
        return $query->result();
    }

    function update_partnercompany($data, $company_id)
    {
        $this->db->where('company_id', $company_id);
        $this->db->update('partner_companies', $data);
        return $this->db->affected_rows();
    }

    public function get_company_details($company_name)
    {
        $this->db->select('*')
            ->from('partner_companies')
            ->where('partner_company_name', $company_name);
        $user = $this->db->get();
        return $user->row();
    }
    public function get_company_details_for_customer($company_name)
    {
        $this->db->select('*')
            ->from('customer_companies')
            ->where('customer_company_id ', $company_name);
        $user = $this->db->get();
        return $user->row();
    }
    public function get_dealer_details_for_customer($dealer_id)
    {
        $this->db->select('*')
            ->from('customer_dealers')
            ->where('customer_dealer_id ', $dealer_id);
        $user = $this->db->get();
        return $user->row();
    }
    public function count_all_customers()
    {
        $this->db->select('*')
            ->from('customers');
        $num_results = $this->db->count_all_results();
        return $num_results;
    }

    public function get_all_customersnew($useractive = NULL, $regdate = NULL, $regdateto = NULL, $paytype = NULL, $sourceval = NULL, $customertype = NULL, $keywordsearch = NULL)
    {
        $this->db->select('c.customer_id')
            ->from('customers c');
        //$this->db->group_by('c.customer_id');
        if ($useractive != '') {
            $this->db->where('c.customer_status', $useractive);
        }
        if ($paytype != '') {
            $this->db->where('c.payment_type', $paytype);
        }
        if ($sourceval != '') {
            $this->db->where('c.customer_source', $sourceval);
        }
        if ($customertype != '') {
            $this->db->where('c.customer_booktype', $customertype);
        }
        if ($customertype != '') {
            $this->db->where('c.customer_booktype', $customertype);
        }
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
        }
        //$this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
        $num_results = $this->db->count_all_results();
        return $num_results;
    }

    public function get_all_newcustomers($useractive = Null, $columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $regdate = NULL, $regdateto = NULL, $paytype = NULL, $sourceval = NULL, $customertype = NULL, $keywordsearch = NULL)
    {
        $response = array();
        ## Fetch records
        /*
        $this->db->select('c.customer_id, c.customer_name, c.price_hourly, c.is_flag, c.flag_reason, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
                ->from('customers c')
				->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
		$this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
		$this->db->where('ca.address_status',0);
		*/

        $this->db->select('c.customer_id, c.customer_name,c.customer_photo_file, c.price_hourly, c.is_flag, c.flag_reason, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime')
            ->from('customers c')
            ->order_by('c.customer_id', 'DESC');

        if ($useractive != '') {
            $this->db->where('c.customer_status', $useractive);
        }
        if ($paytype != '') {
            $this->db->where('c.payment_type', $paytype);
        }
        if ($sourceval != '') {
            $this->db->where('c.customer_source', $sourceval);
        }
        if ($customertype != '') {
            $this->db->where('c.customer_booktype', $customertype);
        }
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
            //$this->db->where("DATE(`customer_added_datetime`) = '$regdate' OR  DATE(`customer_added_datetime`) ='$regdateto'");
        }
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
		$this->db->order_by('c.customer_id', 'DESC');
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        $customerAddress = $this->getCustomerAddresses($records);

        //print_r($records);die();
        foreach ($records as $record) {
            if (!isset($customerAddress[$record->customer_id])) {
                $customerAddress[$record->customer_id] = (object) [
                    'customer_address' => '-',
                ];
            }

            if (isset($customerAddress[$record->customer_id]->building)) {
                $apartmnt_no = 'Apartment No:' . $customerAddress[$record->customer_id]->building . '<br/>';
            } else {
                $apartmnt_no = "";
            }
            //Payment Type
            if ($record->payment_type == "D") {
                $paytype = "(D)";
                $paytext = "Daily";
            } else if ($record->payment_type == "W") {
                $paytype = "(W)";
                $paytext = "Weekly";
            } else if ($record->payment_type == "M") {
                $paytype = "(M)";
                $paytext = "Monthly";
            } else {
                $paytype = "";
                $paytext = "";
            }
            $last_jobdate = $this->get_last_job_date_by_customerid($record->customer_id);
            if (empty($last_jobdate)) {
                $last_date = "";
            } else {
                $last_date = $last_jobdate->service_date;
            }

            if (!isset($customerAddress[$record->customer_id]->customer_address)) {
                $a_address = 'Building - ' . $customerAddress[$record->customer_id]->building . ', ' . $customerAddress[$record->customer_id]->unit_no . '' . $customerAddress[$record->customer_id]->street;
            } else {
                $a_address = $customerAddress[$record->customer_id]->customer_address;
            }

            if ($record->is_flag == "Y") {
                $isflag = " -- Flagged (" . $record->flag_reason . ")";
            } else if ($record->is_flag == "N") {
                $isflag = "";
            }

            $view = '<a class="n-btn-icon blue-btn" href="' . base_url() . 'customer/view/' . $record->customer_id . '" title="View Customer"><i class="btn-icon-only fa fa-eye "> </i></a>';
            $edit = '<a class="n-btn-icon purple-btn" href="' . base_url() . 'customer/edit/' . $record->customer_id . '" title="Edit Customer"><i class="btn-icon-only icon-pencil"> </i></a>';

            $btn_class = $record->customer_status == 1 ? 'n-btn-icon green-btn' : 'n-btn-icon red-btn';
            if ($record->customer_status == 1) {
                $icon = '<i class="btn-icon-only  fa fa-toggle-on "> </i>';
            } else {
                $icon = '<i class="btn-icon-only  fa fa-toggle-off"> </i>';
            }
            $disable = '<a href="javascript:void(0)" class="' . $btn_class . '" title="Disable Customer" onclick="confirm_disable_enable_modal(this, ' . $record->customer_id . ', ' . $record->customer_status . ');">' . $icon . '</a>';

            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'name' => '<a href="' . base_url('customer/view/'.$record->customer_id).'" style="text-decoration: none;color:#333;" data-toggle="tooltip" title="' . $paytext . '">' . $record->customer_name . ' ' . $paytype . '<br/><span style="color: red;">' . $isflag . '</span></a>',
                'mobile' => $record->mobile_number_1 ?: '-',
                'perhr' => 'AED ' . $record->price_hourly,
                'area' => $customerAddress[$record->customer_id]->zone_name . '-' . $customerAddress[$record->customer_id]->area_name,
                'address' => $apartmnt_no . wordwrap($a_address, 25, "<br>"),
                'source' => $record->customer_source ?: '-',
                'lastjob' => $last_date ?: '-',
                'addeddate' => ($record->customer_added_datetime) ? date("d/m/Y H:i:s", strtotime($record->customer_added_datetime)) : "",
                'action' => $view . $edit . $disable,
                'image' => '<img class="btn n-customers-pto" onclick="showAvatar(\''.$record->customer_name.'\',\''.base_url($record->customer_photo_file != '' ? CUSTOMER_AVATAR_PATH . $record->customer_photo_file : DEFAULT_AVATAR).'\')" style="width:50px;" src="'.base_url($record->customer_photo_file != '' ? CUSTOMER_AVATAR_PATH . $record->customer_photo_file : DEFAULT_AVATAR).'" />'
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }

    public function get_all_newcustomers_excel($regdate = NULL, $regdateto = NULL, $status = NULL, $paytype = NULL, $sourceval = NULL, $customertype = NULL, $keywordsearch = NULL)
    {
        $this->db->select('c.customer_id, c.email_address, c.customer_name, c.price_hourly, c.is_flag, c.flag_reason, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');
        $this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
        if ($status != '') {
            $this->db->where('c.customer_status', $status);
        }
        if ($paytype != '') {
            $this->db->where('c.payment_type', $paytype);
        }
        if ($sourceval != '') {
            $this->db->where('c.customer_source', $sourceval);
        }
        if ($customertype != '') {
            $this->db->where('c.customer_booktype', $customertype);
        }
        if ($keywordsearch != "") {
            $this->db->where('c.customer_name like"%' . $keywordsearch . '%" OR c.mobile_number_1 like"%' . $keywordsearch . '%" OR c.email_address like"%' . $keywordsearch . '%"');
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
            //$this->db->where("DATE(`customer_added_datetime`) = '$regdate' OR  DATE(`customer_added_datetime`) ='$regdateto'");
        }
        $query = $this->db->get();
        $records = $query->result();

        foreach ($records as $k => $v) {
            $last_jobdate = $this->get_last_job_date_by_customerid($v->customer_id);
            if (empty($last_jobdate)) {
                $records[$k]->last_date = "";
            } else {
                $records[$k]->last_date = date('d-m-Y', strtotime($last_jobdate->service_date));
            }
        }
        return $records;
    }

    function get_customer_new_packageodoolist()
    {
        $this->db->select('c.*,ca.*,a.*,z.*')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->group_by('ca.customer_id')
            //->order_by('c.customer_name', 'ASC');
            ->order_by('c.customer_id', 'ASC');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.odoo_package_customer_status', 0);
        $this->db->where('ca.default_address', 1);
        $this->db->limit(100);
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    protected function getCustomerAddresses($customers = array())
    {
        $customerIds = array();

        foreach ($customers as $customer) {
            $customerIds[] = $customer->customer_id;
        }

        $addresses = $this->db->select('ca.customer_id, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
            ->from('customer_addresses ca')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where_in('ca.customer_id', $customerIds ?: '[]')
            ->where('ca.address_status', 0)
            ->group_by('ca.customer_id')
            ->get()->result();

        $customerAddresses = [];

        foreach ($addresses as $address) {
            $customerAddresses[$address->customer_id] = $address;
        }

        return $customerAddresses;
    }

    function getallactivecustomers()
    {

        $this->db->select('customer_id, customer_name, mobile_number_1, email_address')
            ->from('customers');
        $this->db->where('customer_status', 1);
        $this->db->where('customer_booktype', 0);
        //$this->db->limit(10,1);


        $get_customers_qry = $this->db->get();
        return $get_customers_qry->result();
    }

    public function get_last_job_date_by_customerid_new($customer_id)
    {
        $this->db->select('service_date')
            ->from('day_services')
            ->where('customer_id', $customer_id)
            ->where('service_status !=', 3)
            ->order_by('service_date', 'desc')
            ->limit(1);
        $get_last_job_date_by_customerid_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_last_job_date_by_customerid_qry->row();
    }
    function get_customer_payments($date_from= NULL,$date_to= NULL,$customer_id,$company=null,$status=null)
    {
        $this->db->select('cp.*,c.customer_name')
                ->from('customer_payments cp')
				->join('customers c','c.customer_id = cp.customer_id');
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("cp.paid_datetime BETWEEN '$date_from' AND '$date_to'");
		}
		if($customer_id != NULL)
		{
			$this->db->where('cp.customer_id', $customer_id);
		}
        if($company != NULL)
		{
			$this->db->where(['c.is_company'=>'Y','c.company_name'=> $company]);
		}
        if($status != NULL)
		{
            if($status==22)$status=0;
			$this->db->where('cp.payment_method', $status);
		}
        $this->db->where('cp.deleted_at',null);
        $get_pay_qry = $this->db->order_by('cp.payment_id', "desc")->get();
        return $get_pay_qry->result();
    }
    function get_customer_payment_byid($pay_id)
	{
		$this->db->select('cp.*,DATE(cp.paid_datetime) as paid_date,c.customer_name,c.email_address,ca.customer_address,c.quickbook_id')
                ->from('customer_payments cp')
				->join('customers c','c.customer_id = cp.customer_id')
				->join('customer_addresses ca','c.customer_id = ca.customer_id');
		$this->db->where('cp.payment_id', $pay_id);
        $this->db->where('cp.deleted_at',null);
        $get_pay_qry = $this->db->get();
        return $get_pay_qry->row();
	}
    public function get_non_posted_invoices($customer_id)
	{
		$this->db->select('i.*')
                ->from('invoice i');
		$this->db->where('i.customer_id', $customer_id);
		$this->db->where('i.invoice_status', 1);
		$this->db->where('i.invoice_paid_status !=', 1);
		
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
    public function get_allocated_invoices($pay_id)
	{
		$this->db->select('im.inv_reference,im.payment_amount,i.*')
                ->from('InvoicePaymentMapping im')
				->join('invoice i','i.invoice_id = im.invoiceId');
		$this->db->where('im.paymentId', $pay_id);
		//$this->db->where('i.invoice_status', 1);
		//$this->db->where('i.invoice_paid_status !=', 1);
		
        $get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
    function get_customer_statement($customer_id)
    {
        $sql = '(select i.invoice_date as dateval,i.invoice_num as invoiceval,i.invoice_net_amount as amount,"A" as stattype,"NA" as payinvref,i.invoice_type, i.invoice_notes as invnotes from invoice i where i.invoice_status NOT IN (0,2) and i.customer_id = '.$customer_id.' order by i.invoice_date ASC) UNION ALL (select DATE(p.payment_added_datetime) as dateval, p.pay_reference as invoiceval,p.payment_amount as amount, "B" as stattype, inv_reference as payinvref, "ID" as invnotes,NULL as invoice_type from InvoicePaymentMapping p where p.inv_customer_id = '.$customer_id.' order by DATE(p.payment_added_datetime) ASC)';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_customer_statement_new($customer_id,$startdate,$enddate)
    {
        //$sql = '(select i.invoice_date as dateval,i.invoice_num as invoiceval,i.invoice_net_amount as amount,"A" as stattype,"NA" as payinvref, i.invoice_notes as invnotes from invoice i where i.invoice_status NOT IN (0,2) and invoice_date >="'.$startdate.'" and invoice_date <="'.$enddate.'" and i.customer_id = '.$customer_id.' order by i.invoice_date ASC) UNION ALL (select DATE(p.payment_added_datetime) as dateval, p.pay_reference as invoiceval,p.payment_amount as amount, "B" as stattype, inv_reference as payinvref, "ID" as invnotes from InvoicePaymentMapping p where p.inv_customer_id = '.$customer_id.' and DATE(p.payment_added_datetime) >= "'.$startdate.'" and DATE(p.payment_added_datetime) <= "'.$enddate.'" order by DATE(p.payment_added_datetime) ASC)';
        $sql = '(select i.invoice_date as dateval,i.invoice_num as invoiceval,i.invoice_net_amount as amount,"A" as stattype,"NA" as payinvref, i.invoice_notes as invnotes from invoice i where i.invoice_status NOT IN (0,2) and invoice_date >="'.$startdate.'" and invoice_date <="'.$enddate.'" and i.customer_id = '.$customer_id.' order by i.invoice_date ASC) UNION ALL (select DATE(p.paid_datetime) as dateval, p.payment_method as invoiceval,p.paid_amount as amount, "B" as stattype, "Y" as payinvref, "ID" as invnotes from customer_payments p where p.deleted_at IS NULL and p.customer_id = '.$customer_id.' and DATE(p.paid_datetime) >= "'.$startdate.'" and DATE(p.paid_datetime) <= "'.$enddate.'" order by DATE(p.paid_datetime) ASC)';
        $query = $this->db->query($sql);
        return $query->result();
    }
    function getinitialbal($customer_id)
    {
        $this->db->select('c.initial_balance, c.initial_bal_sign, c.initial_bal_date, c.customer_name, ca.customer_address')
                ->from('customers c')
                ->join('customer_addresses ca','c.customer_id = ca.customer_id','LEFT');
        $this->db->where('c.customer_id', $customer_id);
        $get_customers_qry = $this->db->get();
        return $get_customers_qry->row();
    }
    function get_online_payments($date_from= NULL,$date_to= NULL)
    {
        $this->db->select('op.*,c.customer_name')
                ->from('online_payments op')
				->join('customers c','c.customer_id = op.customer_id');
		if($date_from != NULL && $date_to != NULL)
		{
			$this->db->where("DATE(op.payment_datetime) BETWEEN '$date_from' AND '$date_to'");
		}
		$this->db->where('op.payment_status', 'Success');
		// if($customer_id != NULL)
		// {
			// $this->db->where('cp.customer_id', $customer_id);
		// }
        $get_pay_qry = $this->db->order_by('op.payment_id','desc')->get();
        return $get_pay_qry->result();
    }

    function get_all_users() {
        $this->db->select('customer_id, customer_name')
                ->from('customers')
                 ->where('customer_status', 1);
        $get_users_by_qry = $this->db->get();
        return $get_users_by_qry->result();
    }
    function customers_by_payment_types($types)
	{
        $this->db->select('c.customer_id,CONCAT(c.customer_name," (",c.payment_type,") ")as customer_name',false)
        ->from('customers as c')
        ->where('c.customer_status',1)
        ->where_in('c.payment_type',$types);
		$query = $this->db->get();
        return $query->result();
	}
	
	function update_customer_quickbookpayment($data = array(),$id)
    {
        $this->db->where('payment_id', $id);
        $this->db->update('customer_payments', $data);
        return $this->db->affected_rows();
    }
	
	public function count_all_customers_outstanding()
    {
        $this->db->select('*')
            ->from('customers');
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	public function get_all_customers_outstanding($useractive = NULL, $regdate = NULL, $regdateto = NULL, $custselect = NULL)
    {
        $this->db->select('c.customer_id')
            ->from('customers c');
        if ($useractive != '') {
            $this->db->where('c.customer_status', $useractive);
        }
        if ($custselect != '') {
            $this->db->where('c.customer_id', $custselect);
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
        }
        //$this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
        $num_results = $this->db->count_all_results();
		// echo $this->db->last_query();exit;
        return $num_results;
    }
	
	public function get_all_newcustomers_outstanding($useractive = Null, $columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $regdate = NULL, $regdateto = NULL, $custselect = NULL)
    {
        $response = array();

        $this->db->select('c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_added_datetime,c.balance')
            ->from('customers c')
            ->order_by('c.customer_id', 'DESC');

        if ($useractive != '') {
            $this->db->where('c.customer_status', $useractive);
        }
        if ($custselect != '') {
            $this->db->where('c.customer_id', $custselect);
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
            //$this->db->where("DATE(`customer_added_datetime`) = '$regdate' OR  DATE(`customer_added_datetime`) ='$regdateto'");
        }
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
		$this->db->order_by('c.customer_id', 'DESC');
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        $customerAddress = $this->getCustomerAddresses($records);

        //print_r($records);die();
        foreach ($records as $record) {
            if (!isset($customerAddress[$record->customer_id])) {
                $customerAddress[$record->customer_id] = (object) [
                    'customer_address' => '-',
                ];
            }

            if (isset($customerAddress[$record->customer_id]->building)) {
                $apartmnt_no = 'Apartment No:' . $customerAddress[$record->customer_id]->building . '<br/>';
            } else {
                $apartmnt_no = "";
            }
            
            if (!isset($customerAddress[$record->customer_id]->customer_address)) {
                $a_address = 'Building - ' . $customerAddress[$record->customer_id]->building . ', ' . $customerAddress[$record->customer_id]->unit_no . '' . $customerAddress[$record->customer_id]->street;
            } else {
                $a_address = $customerAddress[$record->customer_id]->customer_address;
            }

            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
                'name' => $record->customer_name,
                'mobile' => $record->mobile_number_1 ?: '-',
                'area' => $customerAddress[$record->customer_id]->zone_name . '-' . $customerAddress[$record->customer_id]->area_name,
                'address' => $apartmnt_no . wordwrap($a_address, 25, "<br>"),
                'balance' => $record->balance,
                'addeddate' => ($record->customer_added_datetime) ? date("d/m/Y H:i:s", strtotime($record->customer_added_datetime)) : "",
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_all_newcustomers_outstanding_excel($regdate = NULL, $regdateto = NULL, $status = NULL, $custid = NULL)
    {
        $this->db->select('c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status, c.customer_added_datetime, c.balance, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');
        $this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
        if ($status != '') {
            $this->db->where('c.customer_status', $status);
        }
        if ($custid != '') {
            $this->db->where('c.customer_id', $custid);
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
            //$this->db->where("DATE(`customer_added_datetime`) = '$regdate' OR  DATE(`customer_added_datetime`) ='$regdateto'");
        }
        $query = $this->db->get();
        $records = $query->result();

        return $records;
    }
}
