<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customer_access_model extends CI_Model
{

    public function customer_access_owned_list()
    {
        $this->db->select('cao.id,
        cat.name as access_type,
        c.customer_name,
        cao.received_at,
        cao.returned_at,
        ca.code,
        ur.user_fullname as received_user,
        urt.user_fullname as returned_user,
        rbe.maid_name as returned_by_employee')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->join('users as urt', 'cao.return_added_by = urt.user_id', 'left')
            ->join('maids as rbe', 'cao.returned_by_employee = rbe.maid_id', 'left')
            ->where('cao.deleted_at', null)
            ->order_by('cao.received_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_active_accesses()
    {
        $this->db->select('cao.id,cao.customer_access_id,cat.name as access_type,c.customer_name,cao.received_at,cao.returned_at,ca.code,ur.user_fullname as received_user,urt.user_fullname as returned_user')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->join('users as urt', 'cao.return_added_by = urt.user_id', 'left')
            ->where('cao.returned_at', null)
            ->order_by('cao.id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_accesses_except($active_access_ids, $where = [])
    {
        $this->db->select('ca.id,ca.customer_access_type,ca.code,cat.name as access_type_name')
            ->from('customer_access as ca')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->where('ca.deleted_at', null)
            ->where($where)
            ->where_not_in('ca.id', $active_access_ids ?: "[]")
            ->order_by('ca.id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_inactive_accesses()
    {
        
        $this->db->select('ca.id,ca.customer_access_type,ca.code,cat.name as access_type_name')
             ->from('customer_access as ca')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->where('ca.deleted_at', null)
            ->order_by('ca.id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_inative_accesses_except($active_access_ids, $where = [])
    {
        $customer_access_id = $this->get_customer_access_owned($active_access_ids);
        $accessId = array();
        foreach ($customer_access_id as $ids) {
            $accessId[] = $ids->customer_access_id;
        }
        $this->db->select('ca.id,ca.customer_access_type,ca.code,cat.name as access_type_name')
            ->from('customer_access as ca')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->where($where)
            ->where('ca.deleted_at', null)
            ->where_not_in('ca.id', $accessId)
            ->order_by('ca.id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_customer_access_owned($active_access_ids)
    {
        $this->db->select('customer_access_id')
            ->from('customer_access_owned')
            ->where_in('customer_access_id', $active_access_ids);
        $get_results_query = $this->db->get();
        return $get_results_query->result();
    }
    public function get_active_access_owned_by_id($id)
    {
        $this->db->select('cao.id,c.customer_name,cao.received_at,ca.code,ur.user_fullname as received_user,cat.name as access_type')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->where('cao.customer_access_id', $id)
            ->where('cao.returned_at', null);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_access_owned_by_id($id)
    {
        $this->db->select('cao.id,c.customer_name,cao.received_at,cao.returned_at,cao.received_notes,cao.returned_notes,ca.code,ur.user_fullname as received_user,cat.name as access_type')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->where('cao.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    public function access_assign($data)
    {
        $this->db->insert('customer_access_owned', $data);
        return $this->db->insert_id();
    }
    public function return_assign($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->where('returned_at', null);
        $this->db->where('return_added_by', null);
        $this->db->update('customer_access_owned', $data);
    }
    public function customers_search($searchterm)
    {
        $this->db->select('c.customer_id,c.customer_name')
            ->from('customers as c')
            ->order_by('c.customer_id', 'DESC');
        if (isset($searchterm) && strlen($searchterm) > 0) {
            $this->db->where("(customer_name LIKE '%" . $searchterm . "%' OR customer_nick_name LIKE '%" . $searchterm . "%' OR mobile_number_1 LIKE '%" . $searchterm . "%' OR mobile_number_2 LIKE '%" . $searchterm . "%' OR mobile_number_3 LIKE '%" . $searchterm . "%' OR phone_number LIKE '%" . $searchterm . "%')", null, false);
        }
        $this->db->limit(50);
        $this->db->where('c.customer_status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_access_details_by_id($id)
    {
        $this->db->select('cao.id,c.customer_name,cao.received_at,cao.returned_at,cao.received_notes,cao.returned_notes,ca.code,ur.user_fullname as received_user,cat.name as access_type,urt.user_fullname as returned_user')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->join('users as urt', 'cao.return_added_by = urt.user_id', 'left')
            ->where('cao.id', $id);
        $query = $this->db->get();
        $result_array = $query->result_array();
        foreach ($result_array as &$row) {
            $received_at = $row['received_at'];
            if ($received_at !== null) {
                $row['received_at'] = date('d-m-Y', strtotime($received_at));
            }
        }
        foreach ($result_array as &$row) {
            $returned_at = $row['returned_at'];
            if ($returned_at !== null) {
                $row['returned_at'] = date('d-m-Y', strtotime($returned_at));
            }
        }

        return $result_array;

    }
    public function get_access_assign_by_types($type)
    {
        $this->db->select('cao.id,cat.name as access_type,c.customer_name,cao.received_at,cao.returned_at,ca.code,ur.user_fullname as received_user,urt.user_fullname as returned_user')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->join('users as urt', 'cao.return_added_by = urt.user_id', 'left')
            ->where('cat.id', $type)
            ->where('cao.deleted_at', null)
            ->order_by('cao.received_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_access_owned_data($key_id)
    {
        $this->db->select('return_added_by, returned_at');
        $this->db->where('id', $key_id);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('customer_access_owned');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            // Check if the row exists without the limit
            $this->db->where('id', $key_id);
            $query = $this->db->get('customer_access_owned');

            if ($query->num_rows() > 0) {
                // The row exists, but no returned_by_user and returned_at data
                return (object) array('return_added_by' => null, 'returned_at' => null);
            } else {
                // The row does not exist
                return null;
            }
        }
    }
    public function delete_access($key_id)
    {
        $data = array('deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => user_authenticate());
        $this->db->where('id', $key_id);
        $this->db->update('customer_access_owned', $data);
    }
}
