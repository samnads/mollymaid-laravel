<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Security_pass_model extends CI_Model
{
    public function get_customers()
    {
        $this->db->select('
            c.customer_id,
            c.customer_code,
            c.customer_name,
            c.customer_nick_name,
            c.email_address,
            c.customer_type_id,
            c.gender_id,
            ct.customer_type,
            g.gender,
            c.mobile_number_1 
        ')
            ->from('customers as c')
            ->join('customer_types as ct', 'c.customer_type_id = ct.customer_type_id', 'left')
            ->join('genders as g', 'c.gender_id = g.gender_id', 'left')
            ->order_by('c.customer_id', 'ASC');

        $query = $this->db->get();
        return $query->result();
    }
    // ************************************************************************************


    public function get_all_maid()
    {
        $this->db->select('m.maid_id,m.maid_name,m.maid_full_name,m.maid_present_address,m.maid_mobile_1,genders.gender,teams.team_name,employee_types.employee_type,countries.country')
            ->from('maids m')
            ->join('genders', 'm.gender_id = genders.gender_id', 'left')
            ->join('teams', 'm.team_id = teams.team_id', 'left')
            ->join('countries', 'm.country_id=countries.id', 'left')
            ->join('employee_types', 'employee_types.employee_type_id = m.employee_type_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }
    // ***********************************************************************************************
    public function create_new_pass($data)
    {
        $inn = $this->db->insert('gate_pass', $data);
        if ($inn) {
            return $this->db->insert_id();
        } else {
            false;
        }
    }

    // **************************************************************************************************
    public function update_pass($data,$id)
    {
        $upp = $this->db->where('pass_id',$id)->update('gate_pass', $data);
        if ($upp) {
            return true;
        } else {
            false;
        }
    }

    // ****************************************************************************************************
    public function get_all_pass(){
        $this->db->select('p.*,m.maid_id,m.maid_name,m.maid_full_name,m.maid_present_address,
        m.maid_mobile_1,genders.gender,employee_types.employee_type,countries.country,
        c.customer_id,c.customer_code,c.customer_name,c.email_address,c.customer_type_id')
        ->from('gate_pass p')
        ->join('maids m', 'p.maid_id = m.maid_id', 'left')
        ->join('customers c', 'p.customer_id = c.customer_id', 'left')
        ->join('genders', 'm.gender_id = genders.gender_id', 'left')
        ->join('customer_types as ct', 'c.customer_type_id = ct.customer_type_id', 'left')
        ->join('countries', 'm.country_id=countries.id', 'left')
        ->join('employee_types', 'employee_types.employee_type_id = m.employee_type_id', 'left')
        ->where('p.deleted_at',null);
    $query = $this->db->get();
    return $query->result();
    }
// *********************************************************************************************************************
    public function getpass_details_by_id($id){
        $this->db->select('p.*,m.maid_id,m.maid_name,m.maid_full_name,m.maid_present_address,
        m.maid_mobile_1,genders.gender,employee_types.employee_type,countries.country,
        c.customer_id,c.customer_code,c.customer_name,c.email_address,c.customer_type_id')
        ->from('gate_pass p')
        ->join('maids m', 'p.maid_id = m.maid_id', 'left')
        ->join('customers c', 'p.customer_id = c.customer_id', 'left')
        ->join('genders', 'm.gender_id = genders.gender_id', 'left')
        ->join('customer_types as ct', 'c.customer_type_id = ct.customer_type_id', 'left')
        ->join('countries', 'm.country_id=countries.id', 'left')
        ->join('employee_types', 'employee_types.employee_type_id = m.employee_type_id', 'left')
        ->where('p.deleted_at',null)
        ->where('p.pass_id',$id);

        $query = $this->db->get();

        if ($query && $query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
// *****************************************************************************************************

}
