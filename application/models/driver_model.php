<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Driver_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_tablets_location_by_date($date)
    {
        $date = $date ?: date('d-m-Y');
        $this->db->select('tl.tablet_id,MAX(tl.added) as added,tl.latitude, tl.longitude,t.tablet_driver_name,z.zone_name')
            ->from('tablet_locations as tl')
            ->join('tablets as t', 'tl.tablet_id = t.tablet_id', 'left')
            ->join('zones as z', 't.zone_id = z.zone_id', 'left')
            ->where('DATE(tl.added)', $date)
            ->where('t.tablet_status', 1)
            ->where('z.zone_status', 1)
            ->group_by('tl.tablet_id')
            ->order_by('tl.added', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_current_tablet_details_by_service($booking_id, $service_date)
    {
        // for getting driver / tablet details for a specific service
        // if its a transferred tablet it's data will taken
        $this->db->select("b.booking_id,'" . $service_date . "' as service_date,
        b.tabletid as booking_tabletid,
        t_b.tablet_driver_name as booking_driver,
        t_btt.tablet_driver_name as transferred_driver,
        btt.transfering_to_tablet as transferred_tabletid", false)
            ->from('bookings as b')
            ->join('booking_transfers_tablet as btt', "b.booking_id = btt.booking_id AND btt.service_date = " . $this->db->escape($service_date), 'left')
            ->join('tablets as t_b', "b.tabletid = t_b.tablet_id", 'left')
            ->join('tablets as t_btt', "btt.transfering_to_tablet = t_btt.tablet_id", 'left')
            ->where('b.booking_id', $booking_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function get_drivers_for_dropdown()
    {
        $this->db->select('m.maid_id as driver_id,m.maid_name as driver_name')
            ->from('maids as m')
            ->where('m.employee_type_id', 2)
            ->where('m.maid_status', 1);
        $query = $this->db->get();
        return $query->result();
    }
    public function confirm_driver_dispatch($day_service_id,$data)
    {
        $data['is_driver_confirmed'] = 1;
        $this->db->where('day_service_id', $day_service_id);
        $this->db->where('dispatch_status', 2);
        $this->db->update('day_services', $data);
    }
    public function unconfirm_driver_dispatch($unconfirmed_schedule_id,$data)
    {
        $data['is_driver_unconfirmed'] = 1;
        $this->db->where('unconfirmed_schedule_id', $unconfirmed_schedule_id);
        // $this->db->where('deleted_at', 2);
        $this->db->update('driver_unconfirmed_schedules', $data);
    }
    public function save_driver_availability($data)
    {
        $this->db->insert('driver_availability', $data);
        $this->db->insert_id();
    }

    public function get_driver_details($driver_availability_id)
    {
        $this->db->select('driver_availability.*,areas.area_id,area_name,zones.zone_id,zones.zone_name')
            ->from('driver_availability')
            ->join('areas', 'areas.area_id = driver_availability.area_id')
            ->join('zones', 'zones.zone_id = areas.zone_id')
            ->where('driver_availability_id', $driver_availability_id);

        $query = $this->db->get();
        //   log_message('error', 'driver details query: ' . $this->db->last_query());
        return $query->result_array();
    }

    public function update_driver_availability($data, $edit_driver_avalability_id)
    {
        $this->db->where('driver_availability_id', $edit_driver_avalability_id);
        $this->db->update('driver_availability', $data);
    }
    public function delete_driver_availability($data, $driver_avalability_id)
    {
        $this->db->where('driver_availability_id', $driver_avalability_id);
        // $this->db->update('driver_availability', $data);
        $this->db->delete('driver_availability');
    }
    public function delete_multiple_driver_availability($data, $driver_avalability_ids)
    {
        $this->db->where_in('driver_availability_id', $driver_avalability_ids);
        $this->db->delete('driver_availability');
        // $this->db->update('driver_availability', $data);
    }
    public function driver_area_exist($edit_date_from, $area_id)
    {
        $this->db->select('*')
        ->from('driver_availability')
       ->where('date_from', $edit_date_from)
       ->where('area_id', $area_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function driver_area_exist_for_driver($edit_date_from, $area_id,$driver_id)
    {
        $this->db->select('*')
        ->from('driver_availability')
       ->where('date_from', $edit_date_from)
       ->where_in('area_id', $area_id)
       ->where('driver_id', $driver_id);
        $query = $this->db->get();
        // log_message('error', 'driver_area_exist query: ' . $this->db->last_query());
        return $query->result();
    }

    public function get_driver_availability_by_area($edit_date_from,$edit_date_to,$driver_id, $area_id)
    {
        $this->db->where('driver_id', $driver_id)
        ->where('date_from <=', $edit_date_to)
        ->where('date_to >=', $edit_date_from)
        ->where('area_id', $area_id)
        ->where('deleted_at', null);
        $query = $this->db->get('driver_availability');
        return $query->row();
    }
}
