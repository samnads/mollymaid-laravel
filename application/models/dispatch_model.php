<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dispatch_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_bookings_for_dispatch_by_date($service_date)
    {
        // in development mode
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $service_week_day = date('N', strtotime($service_date));
        $this->db->select('
        b.booking_id,
        b.reference_id,
        b.booking_type,
        b.time_from,
        b.time_to,
        m.maid_id,
        m.maid_name,
        m.maid_gender,
        m.maid_nationality,
        m.maid_priority,
        bd.booking_delete_id,
        c.customer_name,
        c.customer_id,
        ds.dispatch_status,
        ds.day_service_id,
        ds.day_service_reference_id,
        ds.dispatch_status,
        ds.time_from as ds_time_from,
        ds.time_to as ds_time_to
        ')
            ->from('bookings as b')
            ->join('maids as m', 'm.maid_id = b.maid_id')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id', 'left')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = ' . $this->db->escape($service_date), 'left')
            ->where('m.maid_status', 1)
            ->where('b.booking_status', 1)
            ->where('ds.dispatch_status', null)
        /***************************** */
            ->where('b.service_week_day', $service_week_day);
        $this->db->where_not_in('b.booking_id', $today_deleted_booking_ids); // uncomment to except paused or one day deleted
        $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_day_services_for_confirm_dispatch($service_date)
    {
        // in development mode
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $service_week_day = date('N', strtotime($service_date));
        $this->db->select('
        b.booking_id,
        b.reference_id as booking_reference_id,
        ds.day_service_id,
        ds.day_service_reference_id,
        b.booking_type,
        ds.time_from,
        ds.time_to,
        m.maid_id,
        m.maid_name,
        m.maid_gender,
        m.maid_nationality,
        m.maid_priority,
        c.customer_name,
        c.customer_id,
        ds.dispatch_status,
        ')
            ->from('bookings as b')
            ->join('maids as m', 'm.maid_id = b.maid_id')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            // ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id', 'left')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = ' . $this->db->escape($service_date), 'left')
            ->where('m.maid_status', 1)
            ->where('b.booking_status', 1)
            ->where('ds.dispatch_status', 1)
        /***************************** */
            ->where('b.service_week_day', $service_week_day);
        $this->db->where_not_in('b.booking_id', $today_deleted_booking_ids); // uncomment to except paused or one day deleted
        $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_day_services_for_undo_confirmed_dispatch($service_date)
    {
        // in development mode
        /*************************************** */
        $CI = get_instance();
        $CI->load->model('settings_model');
        $settings = $CI->settings_model->get_settings();
        /*************************************** */
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $service_week_day = date('N', strtotime($service_date));
        $this->db->select('
        b.booking_id,
        b.reference_id as booking_reference_id,
        ds.day_service_id,
        ds.day_service_reference_id,
        b.booking_type,
        b.time_from,
        b.time_to,
        m.maid_id,
        m.maid_name,
        m.maid_gender,
        m.maid_nationality,
        m.maid_priority,
        c.customer_name,
        c.customer_id,
        ds.dispatch_status,
        ')
            ->from('bookings as b')
            ->join('maids as m', 'm.maid_id = b.maid_id')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            // ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id', 'left')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = ' . $this->db->escape($service_date), 'left')
            ->where('m.maid_status', 1)
            ->where('b.booking_status', 1)
            ->where('ds.dispatch_status', 2)
        /***************************** */
            ->where('b.service_week_day', $service_week_day);
        $this->db->where_not_in('b.booking_id', $today_deleted_booking_ids); // uncomment to except paused or one day deleted
        $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))");
        $query = $this->db->get();
        return $query->result();
    }
    public function payment_modes()
    {
        $this->db->select('pm.payment_mode_id,pm.payment_mode')
            ->from('payment_modes as pm')
            ->where('pm.deleted_at',null)
            ->order_by('pm.payment_mode_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_data_for_dispatch_insert($booking_ids, $service_date)
    {
        $this->db->select('
        b.booking_id,
        b.customer_id,
        c.customer_name,
        b.maid_id,
        b.time_from,
        b.time_to,
        b.price_per_hr as service_rate_per_hour,
        b.price_per_hr as _service_rate_per_hour,
        b.discount_price_per_hr as service_discount_rate_per_hour,
        b.discount_price_per_hr as _service_discount_rate_per_hour,
        (b.price_per_hr * b._service_hours) as _service_amount,
        (b.price_per_hr - b.discount_price_per_hr) * b._service_hours as _service_discount,
        b.discount_price_per_hr * b._service_hours as _net_service_amount,
        ca.customer_address,
        c.payment_type as customer_payment_type,
        b.vat_charge as vatamount,
        b.total_amount as total_fee,
        b.service_charge as serviceamount,
        b.cleaning_material,
        IFNULL(b.cleaning_material_fee,0) as material_fee,
        "' . $service_date . '" as service_date,
        "0" as payment_status,
        "1" as dispatch_status,
        "B" as service_added_by,
        "' . user_authenticate() . '" as service_added_by_id,
        da.driver_id,
        b._service_hours,
        b._service_rate_per_hour,
        b._service_discount_rate_per_hour,
        b._service_amount,
        b._service_discount,
        b._net_service_amount,
        b._cleaning_material,
        b._cleaning_material_rate_per_hour,
        b._net_cleaning_material_amount,
        b._total_discount,
        b._taxable_amount,
        b._vat_percentage,
        b._vat_amount,
        b._total_amount,
        ', false)
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as l', 'ca.location_id = l.location_id', 'left')
            ->join('driver_availability as da', 'l.area_id = da.area_id AND (da.date_from <= "'.$service_date.'" AND da.date_to >= "'.$service_date.'")', 'left')
            ->where_in('b.booking_id', $booking_ids);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function dispatch_insert_batch($rows)
    {
        $insert = $this->db->insert_batch('day_services', $rows);
        if ($insert > 0) {
            return true;
        }
        return false;
    }
    public function dispatch_insert($row)
    {
        $insert = $this->db->insert('day_services', $row);
        return $this->db->insert_id();
    }
    public function confirm_dispatched_service_by_id($day_service_id, $data)
    {
        $data['dispatch_status'] = 2;
        $this->db->where('day_service_id', $day_service_id);
        $this->db->where('dispatch_status', 1);
        $this->db->update('day_services', $data);
    }
    public function undo_confirmed_dispatch_service_by_id($day_service_id, $data)
    {
        $data['dispatch_status'] = 1;
        $this->db->where('day_service_id', $day_service_id);
        $this->db->where('dispatch_status', 2);
        $this->db->update('day_services', $data);
    }
    public function get_booking_deletes_by_date($service_date)
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->where('bd.service_date', $service_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }
    public function get_dispatches_by_service_date($service_date)
    {
        $this->db->select('
        ds.day_service_id,
        ds.day_service_reference_id,
        b.booking_id,
        b.reference_id as booking_reference_id,
        ds.customer_id,
        c.customer_name,
        m.maid_name,
        ds.maid_id,
        ds.service_date,
        b.time_from as booking_time_from,
        b.time_to as booking_time_to,
        ds.time_from,
        ds.time_to,
        b.booking_type
        ')
            ->from('day_services as ds')
            ->join('bookings as b', 'b.booking_id = ds.booking_id', 'left')
            ->join('customers as c', 'c.customer_id = ds.customer_id', 'left')
            ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
            // ->join('booking_types as bt', 'b.booking_type = bt.booking_type', 'left')
            ->where('ds.service_date', $service_date)
            ->where('ds.dispatch_status', 1)
            ->order_by('b.time_from', 'ASC');
        $query = $this->db->get();
        if (!$query) {
            log_message('error', 'Database query failed: ' . $this->db->last_query());
            return []; // Handle error
        }
        return $query->result();
    }
    public function get_confirmed_dispatches_by_service_date($service_date)
    {
        $this->db->select('
        ds.day_service_id,
        ds.day_service_reference_id,
        b.booking_id,
        b.reference_id as booking_reference_id,
        ds.customer_id,
        c.customer_name,
        m.maid_name,
        ds.maid_id,
        ds.service_date,
        b.time_from as booking_time_from,
        b.time_to as booking_time_to,
        ds.time_from,
        ds.time_to,
        b.booking_type
        ')
            ->from('day_services as ds')
            ->join('bookings as b', 'b.booking_id = ds.booking_id', 'left')
            ->join('customers as c', 'c.customer_id = ds.customer_id', 'left')
            ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
            // ->join('booking_types as bt', 'b.booking_type = bt.booking_type', 'left')
            ->where('ds.service_date', $service_date)
            ->where('ds.dispatch_status', 2)
            ->order_by('b.time_from', 'ASC');
        $query = $this->db->get();
        if (!$query) {
            log_message('error', 'Database query failed: ' . $this->db->last_query());
            return []; // Handle error
        }
        return $query->result();
    }
}
