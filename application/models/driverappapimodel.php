<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Driverappapimodel extends CI_Model {

    function __construct() {
        parent::__construct();
    }
		
	function get_tablet_by_imei($imei) 
	{
        $this->db->select('tablet_id, zone_id, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->where('imei', $imei)
                ->limit(1);

        $get_tab_by_imei_qry = $this->db->get();

        return $get_tab_by_imei_qry->row();
    }
	
	function get_maids()
	{
		$this->db->select('m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id, m.maid_status, m.odoo_synch_status')
				->from('maids m')
				->join('flats f', 'm.flat_id = f.flat_id', 'left')
				->order_by('m.maid_name','asc');
		$this->db->where('maid_status', 1);
		$get_maids_qry = $this->db->get();
		//echo $this->db->last_query();exit();
		return $get_maids_qry->result();
	}
	
	function get_zones($active_only = TRUE, $no_spare = TRUE)
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				->from('zones')
				->order_by('zone_name');
		
		if($active_only)
		{
			$this->db->where('zone_status', 1);
		}
		
		if($no_spare)
		{
			$this->db->where('spare_zone', 'N');
		}
		
		$get_zones_qry = $this->db->get();
		
		return $get_zones_qry->result();
	}
	
	function get_schedule_by_date_for_tab($service_date) 
	{
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

//		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.vaccum_cleaning, b.cleaning_materials, b.sofa_cleaning, b.cleaning_chemicals, b.discount, b.booking_status, b.book_by_franchise, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, c.longitude, c.latitude, c.price_hourly, c.price_extra, c.price_weekend, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname", FALSE)
        $this->db->select("b.booking_id,b.total_amount, b.cleaning_material, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, ca.longitude, c.payment_mode, ca.latitude, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id')
                ->where('b.booking_status', 1)
                ->where('m.maid_status', 1)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
//	echo $this->db->last_query();	
        return $get_schedule_by_date_qry->result();
    }
	
	function get_schedule_by_date_for_tab_by_maid_id($service_date,$maid_id) 
	{
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

//		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.vaccum_cleaning, b.cleaning_materials, b.sofa_cleaning, b.cleaning_chemicals, b.discount, b.booking_status, b.book_by_franchise, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, c.longitude, c.latitude, c.price_hourly, c.price_extra, c.price_weekend, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname", FALSE)
        $this->db->select("b.booking_id,b.total_amount, b.cleaning_material, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, ca.longitude, c.payment_mode, ca.latitude, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,z.driver_name", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id')
                ->where('b.booking_status', 1)
                ->where('m.maid_status', 1)
                ->where('m.maid_id', $maid_id)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
//	echo $this->db->last_query();	
        return $get_schedule_by_date_qry->result();
    }
	
	function get_booking_transfers_by_date($service_date) 
	{
        $this->db->select('booking_transfer_id, booking_id, zone_id')
                ->from('booking_transfers')
                ->where('service_date', $service_date)
                ->group_by('booking_id')
                ->order_by('transferred_time', 'desc');

        $get_booking_transfers_by_date_qry = $this->db->get();

        return $get_booking_transfers_by_date_qry->result();
    }
	
	function get_booking_transfers_by_date_booking($service_date,$booking_id) 
	{
        $this->db->select('bt.booking_id, bt.zone_id,z.zone_name')
                ->from('booking_transfers bt')
				->join('zones z','bt.zone_id = z.zone_id')
                ->where('bt.service_date', $service_date)
                ->where('bt.booking_id', $booking_id);

        $get_booking_transfers_by_date_qry = $this->db->get();

        return $get_booking_transfers_by_date_qry->row();
    }
	
	function get_maid_attandence_by_date($maid_id, $date)
	{
		$this->db->select('attandence_id, maid_id, zone_id, tablet_id, date, maid_in_time, maid_out_time, attandence_status')
				->from('maid_attandence')
				->where('maid_id', $maid_id)
				->where('date', $date)
				->order_by('maid_in_time', 'desc')
				->limit(1);
		
		$get_maid_attandence_by_date_qry = $this->db->get();
//	echo $this->db->last_query();	
		return $get_maid_attandence_by_date_qry->row();
	}
	
	function get_day_service_by_booking_id($date, $booking_id)
	{
		$this->db->select('ds.day_service_id, ds.service_added_by_id, z.zone_id, b.maid_id, ds.booking_id, ds.total_fee, ds.service_status, c.customer_id, c.payment_type')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
                                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')				
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('ds.booking_id', $booking_id)
				->where('ds.service_date', $date)
				->limit(1);
		
		$get_day_service_by_booking_id_qry = $this->db->get();
//	echo $this->db->last_query();exit;	
		return $get_day_service_by_booking_id_qry->row();
	}
	
	function get_day_service_by_booking_id_new($date, $booking_id)
	{
		$this->db->select('ds.day_service_id, ds.service_added_by_id, z.zone_name, z.driver_name')
				->from('day_services ds')
				->join('tablets t', 'ds.service_added_by_id = t.tablet_id','left')
				->join('zones z', 't.zone_id = z.zone_id','left')
				->where('ds.booking_id', $booking_id)
				->where('ds.service_date', $date)
				->limit(1);
		
		$get_day_service_by_booking_id_qry = $this->db->get();
		//echo $this->db->last_query();exit;	
		return $get_day_service_by_booking_id_qry->row();
	}
	
	function get_customer_address_by_id($customer_address_id) {
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address, a.area_name, z.zone_id, z.zone_name')
                ->from('customer_addresses ca')
                ->where('ca.customer_address_id', $customer_address_id)
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('ca.customer_address_id');

        $get_customer_address_by_id_qry = $this->db->get();

        return $get_customer_address_by_id_qry->row();
    }
	
	function get_customer_by_id($customer_id) {
        $this->db->select("customer_id, odoo_customer_id, customer_name,customer_username, customer_password, customer_nick_name, mobile_number_1, mobile_number_2, mobile_number_3, phone_number, fax_number, email_address, website_url, customer_photo_file, customer_booktype, customer_type, contact_person, payment_type, payment_mode, price_hourly, price_extra, price_weekend, latitude, longitude, key_given, customer_notes, DATE_FORMAT(customer_added_datetime, '%d / %M / %Y %h:%i %p') AS added_datetime, customer_status,balance, signed, ", FALSE)
                ->from('customers')
                ->where('customer_id', $customer_id)
                ->limit(1);

        $get_customer_by_id_qry = $this->db->get();
//echo $this->db->last_query();exit;
        return $get_customer_by_id_qry->row();
    }
	
	function get_area_by_id($area_id)
	{
		$this->db->select('a.area_id, a.zone_id, a.area_name, a.area_charge, a.area_status, z.zone_name')
				->from('areas a')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('a.area_id', $area_id);

		$get_area_by_id_qry = $this->db->get();
		
		return $get_area_by_id_qry->row();
	}
	
	function get_booking_exist($booking_id, $service_date) {
        $this->db->select("just_mop_ref,mop", FALSE)
                ->from('justmop_ref')
                ->where('service_date', $service_date)
                ->where('booking_id', $booking_id);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->row();
    }
	
	function add_maid_attandence($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('maid_attandence'); 
		return $this->db->insert_id();
	}
	
	function update_maid_attandence($attandence_id, $fields = array())
	{
		$this->db->where('attandence_id', $attandence_id);
		$this->db->update('maid_attandence', $fields); 

		return $this->db->affected_rows();
	}
	
	function get_default_fee()
    {
        $this->db->select('*')
                ->from('settings')
                ->where('settings_id',1);
        $get_payments_qry = $this->db->get();
        
        return $get_payments_qry->row();
                
    }
	
	function update_customers($data, $customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
           return $this->db->affected_rows();
    }
	
	function add_day_service($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('day_services'); 
		return $this->db->insert_id();
	}
	
	function add_customer_payment($fields = array()) {
        $fields['paid_datetime'] = isset($fields['paid_datetime']) ? $fields['paid_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('customer_payments');
        return $this->db->insert_id();
    }
	
	function get_booking_by_id($booking_id) {
        $this->db->select("b.time_from, b.time_to, z.zone_id, b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.booking_note, b.discount, b.total_amount, b.pending_amount, c.customer_name,c.mobile_number_1, c.email_address, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, ca.customer_address, a.area_name,ca.area_id, b.is_locked, b.booked_by, m.maid_name, b.service_end_date, b.cleaning_material,b.price_per_hr,b.booked_from,u.is_admin", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id', 'left')
                ->join('zones z', 'a.zone_id = z.zone_id', 'left')
                ->join('maids m', 'b.maid_id = m.maid_id', 'left')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
                ->where('b.booking_id', $booking_id)
                ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();

        return $get_bookings_by_id_qry->row();
    }
	
	function get_device_tokens() {
        $this->db->select('device_token')
                ->from('ipad_login');

        $get_device_tokens_qry = $this->db->get();

        return $get_device_tokens_qry->result();
    }
	
	function get_zone_by_id($zone_id)
	{
		$this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
				 ->from('zones')
				 ->where('zone_id', $zone_id)
				 ->limit(1);
		
		$get_zone_by_id_qry = $this->db->get();
		
		return $get_zone_by_id_qry->row();
	}
	
	function get_maid_bookings_on_zone_by_date($maid_id, $zone_id, $service_date) 
	{
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('b.booking_status', 1)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where('b.maid_id', $maid_id)
                ->where('a.zone_id', $zone_id)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_maid_bookings_on_zone_by_date_qry = $this->db->get();
//echo $this->db->last_query();exit;		
        return $get_maid_bookings_on_zone_by_date_qry->result();
    }
	
	function add_booking_transfer($fields = array()) {
        $this->db->set($fields);
        $this->db->insert('booking_transfers');

        return $this->db->insert_id();
    }
	
	function get_tablet_by_zone($zone_id) {
        $this->db->select('tablet_id, zone_id, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->where('zone_id', $zone_id)
                ->where('flat_id', 0)
                ->where('tablet_status', 1)
                ->limit(1);

        $get_tablet_by_zone_qry = $this->db->get();

        return $get_tablet_by_zone_qry->row();
    }
	
	function delete_booking_transfer($booking_transfer_id) {
        $this->db->where('booking_transfer_id', $booking_transfer_id);
        $this->db->delete('booking_transfers');
    }
	
	function add_maid_change_request($fields = array()) {

        $fields['reqested_datetime'] = isset($fields['reqested_datetime']) ? $fields['reqested_datetime'] : date('Y-m-d H:i:s');
        $fields['status'] = isset($fields['status']) ? $fields['status'] : 0;

        $this->db->set($fields);
        $this->db->insert('maid_change_request');
        return $this->db->insert_id();
    }
	
	function add_tablet_locations($fields = array()) {
        $this->db->set($fields);
        if ($this->db->insert('tablet_locations'))
            return $this->db->insert_id();
        else
            return FALSE;
    }
	
	function get_payments_new($tablet_id)
	{
		$this->db->select('1 AS payment_status, c.customer_id, c.customer_name, c.mobile_number_1, c.payment_type, c.key_given, ca.customer_address, a.area_id, a.area_name, z.zone_id, z.zone_name, cp.paid_amount', FALSE)
			->from('customers c')
			->join('day_services ds', 'c.customer_id = ds.customer_id')
			->join('bookings b', 'ds.booking_id = b.booking_id')
			->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
			->join('customer_payments cp', 'cp.day_service_id = ds.day_service_id')
			->join('areas a', 'ca.area_id = a.area_id')
			->join('zones z', 'a.zone_id = z.zone_id')
			->where('cp.paid_at', 'T')
			->where('cp.paid_at_id', $tablet_id)
			->where('DATE(cp.paid_datetime)',date('Y-m-d'))
			->order_by('cp.paid_datetime');
		
		$get_payments_qry = $this->db->get();
//	echo $this->db->last_query();	
		return $get_payments_qry->result();
	}
	
	function maid_attendance_by_tablet($tablet_id) {
        $date = date('Y-m-d');
        //$maid_attendance_by_tablet_qry = $this->db->query("SELECT m.maid_id,m.maid_name, IF( ma.attandence_status =1, 'IN', 'OUT' ) AS attandence_status FROM maid_attandence ma JOIN maids m ON ma.maid_id = m.maid_id WHERE ma.date = '$date' AND ma.attandence_id IN ( SELECT max( attandence_id ) FROM maid_attandence WHERE date = '$date' AND tablet_id =$tablet_id GROUP BY maid_id) AND ma.tablet_id =$tablet_id GROUP BY ma.maid_id");
        $maid_attendance_by_tablet_qry = $this->db->query("SELECT m.maid_id, m.maid_name,tt.imei AS tablet_id, IF( ma.attandence_status =1, 'IN', 'OUT' ) AS attandence_status, zo.zone_name
                                                                FROM maid_attandence ma
                                                                RIGHT JOIN maids m ON ma.maid_id = m.maid_id
                                                                JOIN tablets tt ON ma.tablet_id = tt.tablet_id
                                                                JOIN zones zo ON tt.zone_id = zo.zone_id
                                                                WHERE ma.date = '$date'
                                                                AND ma.attandence_id
                                                                IN (

                                                                SELECT max( m.attandence_id )
                                                                FROM maid_attandence m
                                                                JOIN tablets t ON m.tablet_id = t.tablet_id
                                                                JOIN zones z ON t.zone_id = z.zone_id
                                                                WHERE date = '$date'
                                                                GROUP BY m.maid_id
                                                                )
                                                                GROUP BY ma.maid_id");
        //$maid_attendance_by_tablet_qry = $this->db->get();

        return $maid_attendance_by_tablet_qry->result();
    }
	
	function get_schedule_by_maid($maid_id, $zone_id) {
        $service_date = date('Y-m-d');
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.vaccum_cleaning, b.cleaning_material, b.cleaning_chemicals, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('m.maid_id', $maid_id)
                //->where('z.zone_id', $zone_id)
                ->where('b.booking_status', 1)
                ->where('m.maid_status', 1)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->order_by('b.time_from')
                ->limit(1);

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
//	echo $this->db->last_query();	
        return $get_schedule_by_date_qry->row();
    }
	
	function update_day_service($day_service_id, $fields = array())
	{
		$this->db->where('day_service_id', $day_service_id);
		$this->db->update('day_services', $fields); 

		return $this->db->affected_rows();
	}
	
	function update_tablet($tablet_id, $fields = array()) {
        $this->db->where('tablet_id', $tablet_id);
        $this->db->update('tablets', $fields);

        return $this->db->affected_rows();
    }
	
	
	
	
	
	
	
	function get_booking_deletes_by_date($service_date, $service_end_date = NULL) 
	{
        $this->db->select('booking_id')
                ->from('booking_deletes');
        if ($service_end_date != NULL) {
            $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
        } else {
            $this->db->where("service_date", $service_date);
        }


        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }
    
    
        function get_driver_notifications($tablet_id) 
	{
        $this->db->select('message')
                ->from('push_notications');
        $this->db->where("tab_id", $tablet_id);
        $this->db->where('DATE(addeddate)', date('Y-m-d'));


        $driver_notifications_qry = $this->db->get();

        return $driver_notifications_qry->result();
    }
}