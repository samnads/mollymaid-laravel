<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Employee_model extends CI_Model
{

    public function get_employees_by_type($employee_type_id,$where)
    {
        $this->db->select('m.maid_id,m.maid_name,m.maid_full_name,m.maid_gender')
            ->from('maids as m')
            ->where('m.employee_type_id', $employee_type_id)
            ->where($where)
            ->order_by('m.maid_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function verifyMobilenumberExist($mobile_number_1) 
    {
        
        $this->db->select('id');

        $this->db->where(array("mobile_no_1" => $mobile_number_1));

        $query=$this->db->get('employee_addresses');
        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function verifyMobilenumberEditExist($mobile_number_1, $id) 
    {
        
        $this->db->select('id');

        $this->db->where(array("mobile_no_1" => $mobile_number_1, "maid_id !=" =>$id));
        $query=$this->db->get('employee_addresses');
        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
