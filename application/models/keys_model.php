<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Keys_model extends CI_Model
{

    public function add_keys($data)
    {
        $this->db->set($data);
        $this->db->insert('customer_access');
        $result = $this->db->insert_id();
        // print_r($this->db->last_query());die;
        return $result;
    }

    public function get_keys()
    {
         // $qr = $this->db->select('*')->from('customer_access')->where('deleted_at', null)->where('customer_access_type', 1)->order_by('id', 'desc');
        $qr = $this->db->select('ca.id, ca.customer_access_type, ca.code, cao.received_at, cao.returned_at')
            ->select('(CASE WHEN cao.customer_id IS NOT NULL THEN 1 ELSE 0 END) AS is_assigned', false)
            ->from('customer_access as ca')
            ->join('customer_access_owned as cao', 'cao.customer_access_id = ca.id', 'left')
            ->where('ca.deleted_at', null)
            ->where('ca.customer_access_type', 1)
            // ->group_by('cao.customer_access_id') 
            ->order_by('ca.id', 'desc');

        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_key_details($key_id)
    {
        $qr = $this->db->select('*')
            ->from('customer_access')
            ->where('id', $key_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function update_keys($data, $key_id)
    {
        $this->db->where('id', $key_id);
        $this->db->update('customer_access', $data);
    }

    public function delete_key($key_id)
    {
        $data = array('deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => user_authenticate());
        $this->db->where('id', $key_id);
        $this->db->update('customer_access', $data);
    }
    public function get_cards()
    {
        // $qr = $this->db->select('*')->from('customer_access')->where('deleted_at', null)->where('customer_access_type', 2)->order_by('id', 'desc');
        $qr = $this->db->select('ca.id, ca.customer_access_type, ca.code, cao.received_at, cao.returned_at')
        ->select('(CASE WHEN cao.customer_id IS NOT NULL THEN 1 ELSE 0 END) AS is_assigned', false)
        ->from('customer_access as ca')
        ->join('customer_access_owned as cao', 'cao.customer_access_id = ca.id', 'left')
        ->where('ca.deleted_at', null)
        ->where('ca.customer_access_type', 2)
        // ->group_by('cao.customer_access_id') 
        ->order_by('ca.id', 'desc');

    $query = $this->db->get();
    return $query->result_array();
    }

    public function add_cards($data)
    {
        $this->db->set($data);
        $this->db->insert('customer_access');
        $result = $this->db->insert_id();
        // print_r($this->db->last_query());die;
        return $result;
    }

    public function get_access_owned_data($key_id)
    {
        $this->db->select('return_added_by, returned_at');
        $this->db->where('customer_access_id', $key_id);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('customer_access_owned');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            // Check if the row exists without the limit
            $this->db->where('customer_access_id', $key_id);
            $query = $this->db->get('customer_access_owned');

            if ($query->num_rows() > 0) {
                // The row exists, but no returned_by_user and returned_at data
                return (object) array('return_added_by' => null, 'returned_at' => null);
            } else {
                // The row does not exist
                return null;
            }
        }
    }

    public function check_key_exists($key)
    {
        $this->db->where('code', $key);
        $this->db->where('deleted_at', null);  
        $query = $this->db->get('customer_access');
        return $query->num_rows() > 0;
    }

    public function check_key_exists_edit($key, $keyid)
    {
        $this->db->where('code', $key);
        $this->db->where('id !=', $keyid);
        $query = $this->db->get('customer_access');
        return $query->num_rows() > 0;
    }
}
