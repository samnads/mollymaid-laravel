<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Schedule_model extends CI_Model
{
    public function get_week_days()
    {
        $this->db->select('wd.week_day_id,wd.week_name')
            ->from('week_days as wd')
            ->order_by('wd.order_id');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_cancel_resons()
    {
        $this->db->select('cr.cancel_reason_id,cr.cancel_reason')
            ->from('cancel_resons as cr')
            ->order_by('cr.cancel_reason_id');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_suspend_resons()
    {
         $reason_ids = [1,2,3];
        $this->db->select('cr.cancel_reason_id,cr.cancel_reason')
            ->from('cancel_resons as cr')
            ->where_in('cr.cancel_reason_id', $reason_ids)
            ->order_by('cr.cancel_reason_id');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_maids()
    {
        $this->db->select('m.maid_id,m.maid_name,m.maid_full_name,m.maid_gender')
            ->from('maids as m')
            ->where('m.employee_type_id', 1)
            ->where('m.maid_status', 1)
            ->order_by('m.maid_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_maids_leave($service_date)
    {
        $this->db->select('ml.maid_id,ml.leave_date,ml.typeleaves')
            ->from('maid_leave as ml')
            ->join('maids as m', 'ml.maid_id = m.maid_id', 'left')
            ->where('ml.leave_date', $service_date)
            ->where('ml.leave_status', 1)
            ->where('m.employee_type_id', 1)
            ->where('ml.typeleaves', 'leave');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_maids_emergency_leave($service_date)
    {
        $this->db->select('ml.maid_id,ml.leave_date,ml.typeleaves')
            ->from('maid_leave as ml')
            ->join('maids as m', 'ml.maid_id = m.maid_id', 'left')
            ->where('ml.leave_date', $service_date)
            ->where('ml.leave_status', 1)
            ->where('m.employee_type_id', 1)
            ->where('ml.typeleaves', 'emergency_leave');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_all_leave_by_maids($start_date)
{
    $this->db->select('ml.maid_id, ml.leave_date, ml.typeleaves')
        ->from('maid_leave as ml')
        ->where('ml.leave_status', 1)  // Only active leaves
        // ->where('ml.typeleaves', 'emergency_leave')  // Only emergency leaves
        ->where('ml.leave_date >=', $start_date);  // Check if leave date is from start_date onwards

    $query = $this->db->get();
    return $query->result();
}

    public function get_maids_vacations($service_date)
    {
        $this->db->select('ml.maid_id,ml.leave_date,ml.typeleaves')
        
            ->from('maid_leave as ml')
            ->where('ml.leave_status', 1)
            ->where('ml.leave_date', $service_date)
            ->where('ml.typeleaves', 'vacations');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_maids_holidays($service_date)
    {
        $this->db->select('ml.maid_id,ml.leave_date,ml.typeleaves')
            ->from('maid_leave as ml')
            ->join('maids as m', 'ml.maid_id = m.maid_id', 'left')
            ->where('ml.leave_date', $service_date)
            ->where('ml.leave_status', 1)
            ->where('m.employee_type_id', 1)
            ->where('ml.typeleaves', 'holidays');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_maids_medical_leaves($service_date)
    {
        $this->db->select('ml.maid_id,ml.leave_date,ml.typeleaves')
            ->from('maid_leave as ml')
            ->join('maids as m', 'ml.maid_id = m.maid_id', 'left')
            ->where('ml.leave_date', $service_date)
            ->where('ml.leave_status', 1)
            ->where('m.employee_type_id', 1)
            ->where('ml.typeleaves', 'medical_leaves');
        $query = $this->db->get();
        return $query->result();
    }
    public function customers_search($searchterm)
    {
        $this->db->select('c.customer_id,c.customer_name')
            ->from('customers as c')
            ->order_by('c.customer_id', 'DESC');
        if (isset($searchterm) && strlen($searchterm) > 0) {
            $this->db->where("(customer_name LIKE '%" . $searchterm . "%' OR customer_nick_name LIKE '%" . $searchterm . "%' OR mobile_number_1 LIKE '%" . $searchterm . "%' OR mobile_number_2 LIKE '%" . $searchterm . "%' OR mobile_number_3 LIKE '%" . $searchterm . "%' OR phone_number LIKE '%" . $searchterm . "%')", null, false);
        }
        $this->db->limit(50);
        $this->db->where('c.customer_status', 1);
        $query = $this->db->get();
        return $query->result();
    }
    // *****************************************************************
    public function is_maidflagged($maid_id, $customer_id)
    {
        $this->db->where('maid_id', $maid_id);
        $this->db->where('customer_id', $customer_id);
        $this->db->where('flaged_status', 0);

        $result = $this->db->get('flaged_maids_customers')->row();
        return !empty($result);
    }
    public function flagged_maids($customer_id)
    {
        $this->db->select('ml.maid_id')
            ->from('flaged_maids_customers as ml')
           ->where('customer_id', $customer_id)
           ->where('flaged_status', 0);
        $query = $this->db->get();
        return $query->result();
    }
}
