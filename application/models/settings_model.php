<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends CI_Model
{

    function get_zones()
    {
        // $qr = $this->db->select('*')->from('zones')->where('zone_status', 1)->order_by('zone_id', 'desc');
        $qr = $this->db->select('*')->from('zones')->order_by('zone_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_zones($data)
    {
        $this->db->set($data);
        $this->db->insert('zones');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_zone_details($zone_id)
    {
        $qr = $this->db->select('*')
            ->from('zones')
            ->where('zone_id', $zone_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_zone_details_view($zone_id)
    {
        $qr = $this->db->select('z.zone_id,z.zone_status,z.zone_name,z.description')
            ->from('zones z')
            ->where('zone_id', $zone_id);
        $query = $this->db->get();
        return $query->row_array();
    }
    function get_team_details($team_id)
    {
        $qr = $this->db->select('*')
            ->from('teams')
            ->where('team_id', $team_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_zones($data, $zone_id)
    {
        $this->db->where('zone_id', $zone_id);
        $this->db->update('zones', $data);
    }
    function delete_zone_new($data, $zone_id)
    {
        $qr = $this->db->select('*')
            ->from('areas')
            ->where('areas.zone_id', $zone_id)
            ->where('areas.area_status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            $this->db->where('zone_id', $zone_id);
            $this->db->update('zones', $data);
            return 0;
        }
    }
    function update_teams($data, $team_id)
    {
        $this->db->where('team_id', $team_id);
        $this->db->update('teams', $data);
    }
    function delete_zone($zone_id)
    {
        $qr = $this->db->select('*')
            ->from('areas')
            ->where('areas.zone_id', $zone_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            $this->db->where('zone_id', $zone_id);
            $this->db->delete('zones');
            return 0;
        }
    }
    function add_teams($fields = array())
    {
        $fields['team_status'] = isset($fields['team_status']) ? $fields['team_status'] : 1;

        $this->db->set($fields);
        $this->db->insert('teams');
        return $this->db->insert_id();
    }
    function get_teams($active_only = TRUE)
    {
        $this->db->select('team_id, team_name, team_status')
            ->from('teams')
            ->order_by('team_name');

        if ($active_only) {
            $this->db->where('team_status', 1);
        }

        $get_teams_qry = $this->db->get();

        return $get_teams_qry->result();
    }
    function get_teams_for_count($team_id = NULL)
    {
        if ($team_id == NULL) {
            $this->db->select('team_id, team_name, team_status')
                ->from('teams')
                ->order_by('team_name');
            $this->db->where('team_status', 1);
            $get_teams_qry = $this->db->get();
            $results = $get_teams_qry->result();
            $res_array = array();
            foreach ($results as $result) {
                $this->db->select('maid_id')
                    ->from('maids')
                    ->where('team_id', $result->team_id);
                $get_maid_qry = $this->db->get();
                $res_count = $get_maid_qry->num_rows();

                if ($res_count != 0) {
                    $data = array();
                    $data['team_id'] = $result->team_id;
                    array_push($res_array, $data);
                }
            }
            return $res_array;
        } else {
            $this->db->select('team_id, team_name, team_status')
                ->from('teams')
                ->order_by('team_name');
            $this->db->where('team_status', 1);

            if ($team_id != NULL) {
                $this->db->where('team_id', $team_id);
            }

            $get_teams_qry = $this->db->get();

            return $get_teams_qry->result();
        }
    }
    function get_areas()
    {
        $qr = $this->db->select('*')
            ->from('areas')
            ->where('area_status', 1)
            ->join('zones', 'zones.zone_id = areas.zone_id')->order_by('area_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_area($data)
    {
        $this->db->set($data);
        $this->db->insert('areas');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_area_details($area_id)
    {
        $qr = $this->db->select('*')
            ->from('areas')
            ->where('area_id', $area_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_area($data, $area_id)
    {
        $this->db->where('area_id', $area_id);
        $this->db->update('areas', $data);
    }
    function delete_area($area_id)
    {
        $this->db->where('area_id', $area_id);
        $this->db->delete('areas');
    }
    function get_flats()
    {
        $qr = $this->db->select('*')->from('flats')->order_by('flat_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_flats($data)
    {
        $this->db->set($data);
        $this->db->insert('flats');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_flat_details($flat_id)
    {
        $qr = $this->db->select('*')
            ->from('flats')
            ->where('flat_id', $flat_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_flats($data, $flat_id)
    {
        $this->db->where('flat_id', $flat_id);
        $this->db->update('flats', $data);
    }
    function update_ser_webshow_stat($serviceid, $webshow_status)
    {
        $this->db->set('web_status', $webshow_status);
        $this->db->where('service_type_id', $serviceid);
        $this->db->update('service_types');

        return $this->db->affected_rows();
    }
    function delete_flat($flat_id)
    {
        $this->db->where('flat_id', $flat_id);
        $this->db->delete('flats');
    }
    function get_services()
    {
        $qr = $this->db->select('*')->from('service_types')->where('service_category', 'C');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_services_maid()
    {
        $qr = $this->db->select('*')->from('service_types')->where('service_category', 'C')->where('maid_status', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_services($data)
    {
        $this->db->set($data);
        $this->db->insert('service_types');
        $this->db->insert_id();
    }
    function get_service_details($service_id)
    {
        $this->db->select('*')->from('service_types')->where('service_type_id', $service_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_services($data, $service_id)
    {
        $this->db->where('service_type_id', $service_id);
        $this->db->update('service_types', $data);
    }
    function update_souqmaid_price($data, $service_id)
    {
        $this->db->where('id', $service_id);
        $this->db->update('offer_price', $data);
    }
    function delete_services($service_id)
    {
        $this->db->where('service_type_id', $service_id);
        $this->db->delete('service_types');
    }
    function get_tablets()
    {
        $this->db->select('*')->from('tablets')->join('zones', 'zones.zone_id = tablets.zone_id')->order_by('tablet_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_tablet($data)
    {
        $this->db->set($data);
        $this->db->insert('tablets');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_tablet_details($tablet_id)
    {
        $qr = $this->db->select('*')
            ->from('tablets')
            ->where('tablet_id', $tablet_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function update_tablet($data, $tablet_id)
    {
        $this->db->where('tablet_id', $tablet_id);
        $this->db->update('tablets', $data);
    }
    function disable_status($tablet_id, $data)
    {
        $this->db->where('tablet_id', $tablet_id);
        $this->db->update('tablets', $data);
    }
    function activate_status($tablet_id, $data)
    {
        $this->db->where('tablet_id', $tablet_id);
        $this->db->update('tablets', $data);
    }
    function get_prices()
    {
        $qr = $this->db->select('*')->from('payment_settings');
        $query = $this->db->get();
        //        return $query->result_array();
        return $query->result();
    }

    function get_hourly_price_details($service_id)
    {
        $this->db->select('*')->from('payment_settings')->where('ps_id', $service_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_hourly_charges($data, $service_id)
    {
        $this->db->where('ps_id', $service_id);
        $this->db->update('payment_settings', $data);
    }

    function add_hrly_price($data)
    {
        $this->db->set($data);
        $this->db->insert('payment_settings');
        $this->db->insert_id();
    }


    function delete_hrly_price($service_id)
    {
        $this->db->where('ps_id', $service_id);
        $this->db->delete('payment_settings');
    }

    function get_sms_info()
    {
        $qr = $this->db->select('*')->from('sms_settings');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_sms_details($sms_id)
    {
        $this->db->select('*')->from('sms_settings')->where('id', $sms_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_sms_settings($data, $sms_id)
    {
        $this->db->where('id', $sms_id);
        $this->db->update('sms_settings', $data);
    }

    function update_email_settings($data, $e_id)
    {
        $this->db->where('id', $e_id);
        $this->db->update('email_settings', $data);
    }

    function update_tax_settings($data, $t_id)
    {
        $this->db->where('tax_id', $t_id);
        $this->db->update('tax_settings', $data);
    }

    function get_email_info()
    {
        $qr = $this->db->select('*')->from('email_settings');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_tax_info()
    {
        $qr = $this->db->select('*')->from('tax_settings');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_email_details($e_id)
    {
        $this->db->select('*')->from('email_settings')->where('id', $e_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_tax_details($t_id)
    {
        $this->db->select('*')->from('tax_settings')->where('tax_id', $t_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_timezone_info()
    {
        $qr = $this->db->select('*')->from('time_zone_settings');
        $query = $this->db->get();
        return $query->result();
    }

    function get_customer_per_hrprices($customerid)
    {
        $this->db->select('*')->from('customers');
        $this->db->where('customer_id', $customerid);
        $query = $this->db->get();
        return $query->row();
    }

    /*odoo*/
    function get_all_zones_odoo()
    {
        $this->db->select('*')
            ->from('zones')
            ->where('zone_status', 1)
            ->where('odoo_sync_status', 0);

        $query = $this->db->get();
        return $query->result();
    }

    function get_areas_odoo()
    {
        $qr = $this->db->select('a.*,z.odoo_zone_id')
            ->from('areas a')
            ->join('zones z', 'z.zone_id = a.zone_id')
            ->where('a.deleted_at', null)
            ->where('a.odoo_sync_status', 0);
        $query = $this->db->get();
        return $query->result();
    }

    function get_zone_odoo_id($zoneid)
    {
        $this->db->select('*')
            ->from('zones')
            ->where('zone_id', $zoneid);

        $query = $this->db->get();
        return $query->row();
    }

    function get_coupons()
    {
        $this->db->select('*')
            ->from('coupon_code')
            ->where('type', 'C')
            ->order_by('added_date', 'DESC');

        $query = $this->db->get();
        return $query->result();
    }

    function get_coupon_by_id($e_coupon_id)
    {
        $this->db->select('*')
            ->from('coupon_code')
            ->where('coupon_id', $e_coupon_id);

        $query = $this->db->get();
        return $query->row();
    }

    function add_coupon($data)
    {
        $this->db->set($data);
        $this->db->insert('coupon_code');
        $result = $this->db->insert_id();
        return $result;
    }

    function update_coupon($coupon_id, $data)
    {
        $this->db->where('coupon_id', $coupon_id);
        $this->db->update('coupon_code', $data);
        return $this->db->affected_rows();
    }

    function get_souqmaid_pricelist()
    {
        $qr = $this->db->select('*')->from('offer_price')->order_by('offer_date', 'DESC')->limit(20);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_offer_list($date)
    {
        $this->db->select('*')
            ->from('offer_price')
            ->where('offer_date', $date);

        $query = $this->db->get();
        return $query->row();
    }

    function add_souqmaid_price($data)
    {
        $this->db->set($data);
        $this->db->insert('offer_price');
        $result = $this->db->insert_id();
        return $result;
    }

    function get_souqmaid_pricelist_byid($price_id)
    {
        $this->db->select('*')->from('offer_price')->where('id', $price_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_area_id_name($area)
    {
        $this->db->select('area_id')
            ->from('areas')
            ->where('area_name', $area)
            ->where('area_status', 1);
        $get_area_id_name_qry = $this->db->get();

        return $get_area_id_name_qry->row();
    }

    function get_zones_api()
    {
        $qr = $this->db->select('*')
            ->where('odoo_new_zone_status', 0)
            ->from('zones');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_areas_api()
    {
        $qr = $this->db->select('areas.*,zones.odoo_new_zone_id as odoo_zone_id,zones.odoo_new_zone_status as statuss')
            ->from('areas')
            ->where('areas.odoo_new_area_status', 0)
            ->join('zones', 'zones.zone_id = areas.zone_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_flats_api()
    {
        $qr = $this->db->select('*')
            ->from('flats')
            ->where('odoo_new_syncflat_status', 0)
            ->where('flat_status', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_tablet_list()
    {
        $qr = $this->db->select('tablet_id,tablet_driver_name')
            ->from('tablets')
            ->where('tablet_status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function get_tablets_by_status($status)
    {
        $this->db->select('*')->from('tablets')->join('zones', 'zones.zone_id = tablets.zone_id')->where('tablets.tablet_status', $status)->order_by('tablet_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_zones_packages_api()
    {
        $qr = $this->db->select('*')
            ->where('odoo_package_zone_status', 0)
            ->from('zones')
            ->where('zone_status', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_packages_areas_api()
    {
        $qr = $this->db->select('areas.*,zones.odoo_package_zone_id as odoo_zone_id,zones.odoo_package_zone_status as statuss')
            ->from('areas')
            ->where('areas.odoo_package_area_status', 0)
            ->join('zones', 'zones.zone_id = areas.zone_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_package_flats_api()
    {
        $qr = $this->db->select('*')
            ->from('flats')
            ->where('odoo_package_flat_status', 0)
            ->where('flat_status', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_status()
    {
        $data = array('web_status' => 0);
        $this->db->update('areas', $data);
    }

    function update_status_enable()
    {
        $data = array('web_status' => 1);
        $this->db->update('areas', $data);
    }


    function delete_area_click($area_id, $status)
    {
        $this->db->set('web_status', $status);
        $this->db->where('area_id', $area_id);
        $this->db->update('areas');
    }


    function get_area_by_id($area_id)
    {
        $this->db->select('*', FALSE)
            ->from('areas')
            ->where('area_id', $area_id)
            ->limit(1);

        $get_area_by_id_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_area_by_id_qry->row();
    }

    function disable_webstatus($area_id, $data)
    {
        $this->db->where('area_id', $area_id);
        $this->db->update('areas', $data);
    }
    function activate_webstatus($area_id, $data)
    {
        $this->db->where('area_id', $area_id);
        $this->db->update('areas', $data);
    }
    function get_settings()
    {
        $query = $this->db->select('s.*')
            ->from('settings as s')
            ->where('s.deleted_at', NULL)
            ->get();
        return $query->row();
    }
    function update_settings($data)
    {
        $this->db->update('settings', $data);
        $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Settings Updated Successfully !</div>');
    }

    function add_location($data)
    {
        $this->db->set($data);
        $this->db->insert('locations');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_locations()
    {
        $this->db->select('
        l.location_id,
        l.location_name,
        a.area_id,
        a.area_name,
        z.zone_name,
        l.longitude,
        l.latitude,
        l.deleted_at,
        l.description,
        l.min_booking_hour,
        a.description as area_description
        ')
            ->from('locations as l')
            // ->where('l.deleted_at', null)
            ->join('areas as a', 'a.area_id = l.area_id')
            ->join('zones as z', 'z.zone_id = a.zone_id')
            ->order_by('l.location_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_location_details($location_id)
    {
        $qr = $this->db->select('*')
            ->from('locations')
            ->where('location_id', $location_id);
        $query = $this->db->get();
        return $query->row();
    }
    function get_location_details_by_id($location_id)
    {
        $qr = $this->db->select('*')
            ->from('locations')
            ->where('location_id', $location_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_location($data, $location_id)
    {
        $this->db->where('location_id', $location_id);
        $this->db->update('locations', $data);
    }
    function update_location_status_enable()
    {
        $data = array('web_status' => 1);
        $this->db->update('locations', $data);
    }

    function update_status_location()
    {
        $data = array('web_status' => 0);
        $this->db->update('locations', $data);
    }
    function delete_location($location_id)
    {
        // $this->db->where('location_id', $location_id);
        // $this->db->delete('locations');
        $data = array('deleted_at' => date('Y-m-d H:i:s'));
        $this->db->where('location_id', $location_id);
        $this->db->update('locations', $data);
    }
    function disable_zone_status($zone_id, $data)
    {
        $this->db->where('zone_id', $zone_id);
        $this->db->update('zones', $data);
    }
    function enable_zone_status($zone_id, $data)
    {
        $this->db->where('zone_id', $zone_id);
        $this->db->update('zones', $data);
    }
    function loc_disable_webstatus($location_id, $data)
    {
        $this->db->where('location_id', $location_id);
        $this->db->update('locations', $data);
    }
    function loc_activate_webstatus($location_id, $data)
    {
        $this->db->where('location_id', $location_id);
        $this->db->update('locations', $data);
    }
    function land_disable_webstatus($landmark_id, $data)
    {
        $this->db->where('landmark_id', $landmark_id);
        $this->db->update('landmarks', $data);
    }
    function land_activate_webstatus($landmark_id, $data)
    {
        $this->db->where('landmark_id', $landmark_id);
        $this->db->update('landmarks', $data);
    }

    function add_landmark($data)
    {
        $this->db->set($data);
        $this->db->insert('landmarks');
        $result = $this->db->insert_id();
        return $result;
    }
    function get_landmarks()
    {
        $qr = $this->db->select('landmarks.*,areas.area_id,areas.area_name,locations.location_id,locations.location_name,zones.zone_id,zones.zone_name')
            ->from('landmarks')
            // ->where('landmarks.deleted_at', null)
            ->join('locations', 'locations.location_id = landmarks.location_id')
            ->join('areas', 'areas.area_id = locations.area_id')
            ->join('zones', 'zones.zone_id = areas.zone_id')
            ->order_by('landmarks.landmark_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLocationsByArea($areaId)
    {
        $this->db->where('area_id', $areaId);
        $query = $this->db->get('locations');
        return $query->result_array();
    }

    function get_landmark_details($landmark_id)
    {
        $qr = $this->db->select('landmarks.*,areas.area_id,area_name,locations.location_id,locations.location_name,zones.zone_id,zones.zone_name')
            ->from('landmarks')
            ->join('locations', 'locations.location_id = landmarks.location_id')
            ->join('areas', 'areas.area_id = locations.area_id')
            ->join('zones', 'zones.zone_id = areas.zone_id')
            ->where('landmark_id', $landmark_id);

        $query = $this->db->get();
        return $query->result_array();
    }

    function get_landmark_details_view($landmark_id)
    {
        $this->db->select('landmarks.*, areas.area_id, areas.area_name, locations.location_id, locations.location_name, zones.zone_id, zones.zone_name');
        $this->db->from('landmarks');
        $this->db->join('locations', 'locations.location_id = landmarks.location_id');
        $this->db->join('areas', 'areas.area_id = locations.area_id');
        $this->db->join('zones', 'zones.zone_id = areas.zone_id');
        $this->db->where('landmarks.landmark_id', $landmark_id);
    
        $query = $this->db->get();
    
        return $query->row_array(); // Assuming you're fetching a single row
    }

    function update_landmark($data, $landmark_id)
    {
        $this->db->where('landmark_id', $landmark_id);
        $this->db->update('landmarks', $data);
    }

    function delete_landmark($landmark_id)
    {
        $data = array('deleted_at' => date('Y-m-d H:i:s'));
        $this->db->where('landmark_id', $landmark_id);
        $this->db->update('landmarks', $data);
    }

    function get_employee_types()
    {
        $this->db->select('*')
            ->from('employee_types')
            ->where('deleted_at', null)
            ->order_by('employee_type');
        $get_teams_qry = $this->db->get();
        return $get_teams_qry->result();
    }

    function get_employee_types_new()
    {
        $qr = $this->db->select('*')->from('employee_types')->where('deleted_at', null)->order_by('employee_type', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_countries()
    {
        $qr = $this->db->select('*')->from('countries');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_locations_list()
    {
        $qr = $this->db->select('*')->from('locations')->where('deleted_at', null)->order_by('location_name', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_skills_list()
    {
        $qr = $this->db->select('*')->from('skills')->where('deleted_at', null)->order_by('skill', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_levels_list()
    {
        $qr = $this->db->select('*')->from('rating_level')->order_by('rating_level', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_leaders_list()
    {
        $qr = $this->db->select('maid_id,maid_name')->from('maids')->where('maid_id', 'maid_leader_id', false)->order_by('maid_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function check_zone_exists($zoneName)
    {
        $this->db->where('zone_name', $zoneName);
        $query = $this->db->get('zones');

        return $query->num_rows() > 0; // Return true if the zone name exists, false otherwise


    }

    public function check_zone_exists_edit($zoneName, $zoneId)
    {
        $this->db->where('zone_name', $zoneName);
        $this->db->where('zone_id !=', $zoneId);
        $query = $this->db->get('zones');
        return $query->num_rows() > 0;
    }

    function check_area_exists($area_name)
    {
        $this->db->where('area_name', $area_name);
        $query = $this->db->get('areas');
        return $query->num_rows() > 0;
    }

    function check_area_exists_edit($area_name, $area_id)
    {
        $this->db->where('area_name', $area_name);
        $this->db->where('area_id !=', $area_id);
        $query = $this->db->get('areas');
        return $query->num_rows() > 0;
    }

    function check_location_exists($location_name)
    {
        $this->db->where('location_name', $location_name);
        $query = $this->db->get('locations');
        return $query->num_rows() > 0;
    }

    function check_location_exists_edit($locationname, $locationid)
    {
        $this->db->where('location_name', $locationname);
        $this->db->where('location_id !=', $locationid);
        $query = $this->db->get('locations');
        return $query->num_rows() > 0;
    }

    function getZoneArea($areaId, $zoneId)
    {
        $date = date('Y-m-d');
        $this->db->select('
        da.driver_availability_id,
        da.driver_id,
        z.zone_id,
        z.zone_name,
        da.area_id,
        a.area_name,
        da.date_from,
        da.date_to')
            ->from('driver_availability as da')
            ->join('areas as a', 'da.area_id = a.area_id', 'left')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            // ->join('maids as m', 'da.driver_id = m.maid_id', 'left')
            ->where('a.area_id', $areaId)
            ->where(array('da.date_from <=' => $date, 'da.date_to >=' => $date))
            ->where('da.deleted_at', null);
            $query = $this->db->get();
            return $query->result();
    }

    // ***************************************************************************

    function update_existing_area_zone($data, $zone_id, $area_id)
    {
        log_message('error','model post zone_id'.$zone_id);
        try {
            $existingArea = $this->db->select('area_id')
                ->from('areas')
                ->where('zone_id', $zone_id)
                ->where('area_name', $data['area_name'])
                ->where('deleted_at', null)
                ->get()
                ->row();

            if ($existingArea) {
                throw new Exception("An area with the same name already exists in the selected zone.");
            }

            $this->db->where('area_id', $area_id);
            $this->db->update('areas', $data);
        } catch (Exception $e) {

            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }
    }
}
