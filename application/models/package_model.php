<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Package_model Class
 *
 * @package HM
 * @author  Samnad. S
 * @since   Version 1.0
 */
class Package_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
    }
    public function get_all_packages($status)
    {
        //error_reporting(E_ALL);
        $this->db->select('sp.*,st.service_type_name as service_type_name,CASE WHEN sp.booking_type = "WE" THEN "Weekly" WHEN sp.booking_type = "MO" THEN "Monthly" WHEN sp.booking_type = "YE" THEN "Yearly" END AS booking_type,
        CASE WHEN sp.status = 1 THEN "Active" WHEN sp.status = 0 THEN "Inactive" WHEN sp.status = 2 THEN "Deleted" END AS status', false)
            ->from('package as sp')
            ->join('service_types as st', 'sp.service_type_id = st.service_type_id', 'left')
            ->order_by('sp.package_id','desc');
        if ($status != null) {
            $this->db->where('sp.status', $status);
        }
        $query = $this->db->get();
        //header('Content-Type: application/json'); echo json_encode($query->result_array(), JSON_PRETTY_PRINT); die();
        return $query->result_array();
    }
    public function get_package_by_id($id)
    {
        $this->db->where('package_id', $id);
        $q = $this->db->get('package');
        if ($q) {
            return $q->row_array();
        } else {
            return $q->row_array();
        }
    }
    public function get_all_service_types()
    {
        $this->db->select('st.*')
            ->from('service_types as st')
            ->where('st.service_type_status', 1)
            ->order_by('st.service_type_id');
        $query = $this->db->get();
        //header('Content-Type: application/json'); echo json_encode($query->result_array(), JSON_PRETTY_PRINT); die();
        return $query->result();
    }
    public function save_package($data)
    {
        $data['promotion_image'] = null;
        /*************************************** */// upload image
        if ($data['promotion_image_base64'] != null) {
            $base64 = $data['promotion_image_base64'];
            list($type, $base64) = explode(';', $base64);
            list(, $base64) = explode(',', $base64);
            $base64 = base64_decode($base64);
            $file_name = date('d-m-Y_') . md5(time()) . ".jpg";
            unset($data['promotion_image_base64']);
            if (file_put_contents(DIR_UPLOAD_PROMO_IMG . $file_name, $base64)) {
                $data['promotion_image'] = $file_name;
            }
        }
        if ($data['promotion_image_web_base64'] != null) {
            $base64 = $data['promotion_image_web_base64'];
            list($type, $base64) = explode(';', $base64);
            list(, $base64) = explode(',', $base64);
            $base64 = base64_decode($base64);
            $file_name = date('d-m-Y_') . md5(time()) . "-web.jpg";
            unset($data['promotion_image_web_base64']);
            if (file_put_contents(DIR_UPLOAD_PROMO_IMG . $file_name, $base64)) {
                $data['promotion_image_web'] = $file_name;
            }
        }
        /*************************************** */
        unset($data['promotion_image_base64']);
        unset($data['promotion_image_web_base64']);
        //var_dump($data);die();
        $data['addedDateTime'] = date('Y-m-d H:m:s');
        $data['addedBy'] = $this->session->userdata['user_logged_in']['user_id'];
        $data['notes'] = $data['notes'] ?: null;
        $data['description'] = $data['description'] ?: null;
        $data['strikethrough_amount'] = $data['strikethrough_amount'] ?: null;
        $this->db->insert('package', $data);
        $this->add_activity(array('action_type' => 'package_added', 'action_content' => 'Package (' . $data['package_name'] . ') added by ' . $this->users_model->get_user_by_id(user_authenticate())->user_fullname));
        return $this->db->insert_id();
    }
    public function update_package($data)
    {
        /*************************************** */// upload image
        if ($data['promotion_image_base64'] != null) {
            $base64 = $data['promotion_image_base64'];
            list($type, $base64) = explode(';', $base64);
            list(, $base64) = explode(',', $base64);
            $base64 = base64_decode($base64);
            $file_name = date('d-m-Y_') . md5(time()) . "-mob.jpg";
            unset($data['promotion_image_base64']);
            if (file_put_contents(DIR_UPLOAD_PROMO_IMG . $file_name, $base64)) {
                $data['promotion_image'] = $file_name;
            }
        }
        if ($data['promotion_image_web_base64'] != null) {
            $base64 = $data['promotion_image_web_base64'];
            list($type, $base64) = explode(';', $base64);
            list(, $base64) = explode(',', $base64);
            $base64 = base64_decode($base64);
            $file_name = date('d-m-Y_') . md5(time()) . "-web.jpg";
            unset($data['promotion_image_web_base64']);
            if (file_put_contents(DIR_UPLOAD_PROMO_IMG . $file_name, $base64)) {
                $data['promotion_image_web'] = $file_name;
            }
        }
        /*************************************** */
        unset($data['promotion_image_base64']);
        unset($data['promotion_image_web_base64']);
        $data['cleaning_material'] = $data['cleaning_material'] ? 'Y' : 'N';
        $data['strikethrough_amount'] = $data['strikethrough_amount'] ?: null;
        $this->db->where('package_id', $data['id']);
        unset($data['id']);
        $this->db->update('package', $data);
        if ($this->db->affected_rows()) {
            $this->add_activity(array('action_type' => 'package_updated', 'action_content' => 'Package ('.$data['package_name'].') updated by '.$this->users_model->get_user_by_id(user_authenticate())->user_fullname));
            return true;
        } else {
            return false;
        }

    }
    public function disable_package($id)
    {
        $this->db->where('package_id', $id);
        $this->db->update('package', array('status' => 0));
        if ($this->db->affected_rows()) {
            $this->add_activity(array('action_type' => 'package_disabled', 'action_content' => 'Package disabled by ' . $this->users_model->get_user_by_id(user_authenticate())->user_fullname));
            return true;
        } else {
            return false;
        }

    }
    public function enable_package($id)
    {
        $this->db->where('package_id', $id);
        $this->db->update('package', array('status' => 1));
        if ($this->db->affected_rows()) {
            $this->add_activity(array('action_type' => 'package_enabled', 'action_content' => 'Package enabled by ' . $this->users_model->get_user_by_id(user_authenticate())->user_fullname));
            return true;
        } else {
            return false;
        }

    }
    public function add_activity($data)
    {
        $data['added_user'] = user_authenticate();
        $data['addeddate'] = date('Y-m-d H:i:s');
        $this->db->insert('user_activity',$data);
        return $this->db->insert_id();
    }
}
