<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablets_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Add tablet
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	bool
     * @return	array
     */
    function add_tablet($fields = array()) {
        $fields['tablet_status'] = isset($fields['tablet_status']) ? $fields['tablet_status'] : 1;

        $this->db->set($fields);
        $this->db->insert('tablets');
        return $this->db->insert_id();
    }

    /**
     * Get tablets
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	bool, bool
     * @return	array
     */
    function get_tablets($active_only = TRUE, $no_spare = TRUE) {
        $this->db->select('zone_id, zone_name, driver_name, spare_zone, zone_status')
                ->from('zones')
                ->order_by('zone_name');

        if ($active_only) {
            $this->db->where('zone_status', 1);
        }

        if ($no_spare) {
            $this->db->where('spare_zone', 'N');
        }

        $get_zones_qry = $this->db->get();

        return $get_zones_qry->result();
    }

    /**
     * Get tablet by zone
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	int
     * @return	obj
     */
    function get_tablet_by_zone($zone_id) {
        $this->db->select('tablet_id, zone_id, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->where('zone_id', $zone_id)
                ->where('flat_id', 0)
                ->where('tablet_status', 1)
                ->limit(1);

        $get_tablet_by_zone_qry = $this->db->get();

        return $get_tablet_by_zone_qry->row();
    }

    /**
     * Get tablet by imei
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	str
     * @return	obj
     */
    function get_tablet_by_imei($imei) {
        $this->db->select('tablets.tablet_id, tablets.tablet_driver_name, tablets.zone_id, tablets.imei, tablets.google_reg_id, tablets.access_code, tablets.tablet_status, zones.driver_name')
                ->from('tablets')
		->join('zones', 'tablets.zone_id = zones.zone_id', 'left') 
                ->where('imei', $imei)
                ->limit(1);

        $get_tab_by_imei_qry = $this->db->get();
        return $get_tab_by_imei_qry->row();
    }

    /**
     * Update tablet
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	int, array
     * @return	int
     */
    function update_tablet($tablet_id, $fields = array()) {
        $this->db->where('tablet_id', $tablet_id);
        $this->db->update('tablets', $fields);

        return $this->db->affected_rows();
    }

    function authenticate($user_name, $password) {
        $this->db->select('manager_id')
                ->from('ipad_manager')
                ->where('user_name', $user_name)
                ->where('password', $password)
                ->where('status', 1)
                ->limit(1);

        $get_manager_qry = $this->db->get();

        return $get_manager_qry->row();
    }

    function authenticate_mobile_user($user_name, $password) {
        $this->db->select('customer_id')
                ->from('customers')
                ->where('email_address', $user_name)
                ->where('customer_password', $password)
                ->limit(1);

        $get_login_qry = $this->db->get();

        return $get_login_qry->row();
    }

    function add_login($login_fields = array()) {
        $this->db->select('id')
                ->from('ipad_login')
                ->where('manager_id', $login_fields['manager_id'])
                ->like('device_token', $login_fields['device_token'])
                ->limit(1);

        $get_login_qry = $this->db->get();

        //$login = $get_login_qry->row();
        if ($get_login_qry->num_rows() > 0) {
            $login = $get_login_qry->row();
            $this->db->where('id', $login->id);
            $this->db->update('ipad_login', $login_fields);
        } else {
            $this->db->set($login_fields);
            $this->db->insert('ipad_login');
        }
        return $this->db->affected_rows();
    }

    function get_device_tokens() {
        $this->db->select('device_token')
                ->from('ipad_login');

        $get_device_tokens_qry = $this->db->get();

        return $get_device_tokens_qry->result();
    }

    /**
     * Add tablet locations
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int, array
     * @return	int
     */
    function add_tablet_locations($fields = array()) {
        $this->db->set($fields);
        if ($this->db->insert('tablet_locations'))
            return $this->db->insert_id();
        else
            return FALSE;
    }

    /**
     * Get tablets
     * 
     * @author	Habeeb Rahman
     * @acces	public 
     * @param	bool, bool
     * @return	array
     */
    function get_all_tablets($active_only = TRUE, $no_spare = TRUE) {
        $this->db->select('tablet_id, tablets.zone_id, zone_name, zone_nick, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->join('zones', 'tablets.zone_id = zones.zone_id')
                ->where('tablet_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
    }

    function get_tablet_locations() {
        $tablets = $this->get_all_tablets();

        $query = '';
        if (!empty($tablets)) {
            foreach ($tablets as $tab) {
                $query .= '( SELECT id, tablet_id, "' . $tab->zone_name . '" AS zone_name, "' . $tab->zone_nick . '" AS zone_nick, `latitude` , `longitude` , `speed` , `added`  FROM `tablet_locations` WHERE tablet_id = ' . $tab->tablet_id . ' ORDER BY id DESC LIMIT 1 ) UNION ';
				//$query .= '( SELECT id, tablet_id, "' . $tab->zone_name . '" AS zone_name, `latitude` , `longitude` , `speed` , `added`  FROM `tablet_locations` WHERE tablet_id = ' . $tab->tablet_id . ' ORDER BY id DESC LIMIT 1 ) UNION ';
            }

            $query = rtrim($query, ' UNION');
        }

        /* $this->db->select('*')
          ->from('tablet_locations'); */

        $get_tablet_locations_qry = $this->db->query($query);

        return $get_tablet_locations_qry->result();
    }

    function check_ipad_user($user_name, $password) {
        $this->db->select('user_id')
                ->from('ipad_users')
                ->where('user_name', $user_name)
                ->where('password', $password)
                ->where('status', 1)
                ->limit(1);

        $get_ipad_user_qry = $this->db->get();

        return $get_ipad_user_qry->row();
    }

    function change_password($old_password, $new_password) {
        $fields['password'] = $new_password;
        $this->db->where('password', $old_password);
        $this->db->update('ipad_users', $fields);

        return $this->db->affected_rows();
    }

    function get_driver_activity_by_date($date) {
        $this->db->select("z.zone_name, tr.action_type, tr.action_content, TIME(tr.tracked_datetime) AS tracked_time", FALSE)
                ->from('tablet_tracking tr')
                ->join('tablets t', 'tr.tablet_tracked_by_id = t.tablet_id')
                ->join('zones z', 't.zone_id = z.zone_id')
                ->where('tr.tablet_tracked_by', 'T')
                ->where('DATE(tr.tracked_datetime)', $date);

        $get_driver_activity_by_date_query = $this->db->get();

        return $get_driver_activity_by_date_query->result();
    }

    function maid_attendance_by_tablet($tablet_id) {
        $date = date('Y-m-d');
        //$maid_attendance_by_tablet_qry = $this->db->query("SELECT m.maid_id,m.maid_name, IF( ma.attandence_status =1, 'IN', 'OUT' ) AS attandence_status FROM maid_attandence ma JOIN maids m ON ma.maid_id = m.maid_id WHERE ma.date = '$date' AND ma.attandence_id IN ( SELECT max( attandence_id ) FROM maid_attandence WHERE date = '$date' AND tablet_id =$tablet_id GROUP BY maid_id) AND ma.tablet_id =$tablet_id GROUP BY ma.maid_id");
        $maid_attendance_by_tablet_qry = $this->db->query("SELECT m.maid_id, m.maid_name,tt.imei AS tablet_id, IF( ma.attandence_status =1, 'IN', 'OUT' ) AS attandence_status, zo.zone_name
                                                                FROM maid_attandence ma
                                                                RIGHT JOIN maids m ON ma.maid_id = m.maid_id
                                                                JOIN tablets tt ON ma.tablet_id = tt.tablet_id
                                                                JOIN zones zo ON tt.zone_id = zo.zone_id
                                                                WHERE ma.date = '$date'
                                                                AND ma.attandence_id
                                                                IN (

                                                                SELECT max( m.attandence_id )
                                                                FROM maid_attandence m
                                                                JOIN tablets t ON m.tablet_id = t.tablet_id
                                                                JOIN zones z ON t.zone_id = z.zone_id
                                                                WHERE date = '$date'
                                                                GROUP BY m.maid_id
                                                                )
                                                                GROUP BY ma.maid_id");
        //$maid_attendance_by_tablet_qry = $this->db->get();

        return $maid_attendance_by_tablet_qry->result();
    }

    public function getFlatLIst() {
        $flatList = $this->db->query("Select flat_id as id, flat_name From flats Where flat_status = 1 ORDER BY flat_id DESC");

        return $flatList->result();
    }

    public function regTabletForFlat($regNo, $imeiNo, $flatId) {
        $result = 0;
        $tabExists = $this->db->query("SELECT tablet_id AS id FROM tablets Where access_code = '$regNo'  And tablet_status = 0"); //And flat_id = 1 
//        echo $this->db->last_query(); exit;
        if ($tabExists->num_rows() > 0) {


            $fields['flat_id'] = $flatId;
            $fields['imei'] = $imeiNo;
            $fields['tablet_status'] = '1';
            $this->db->where('access_code', $regNo);
            $this->db->where('tablet_status', $regNo);
            $this->db->update('tablets', $fields);


            $tabInfo = $tabExists->result();

            $result = $tabInfo[0]->id;
        } else {
            $result = -1;
        }
        return $result;
    }

    public function loginTablet($access_code, $tabId) {
        $result = 0;

        $logTab = $this->db->query("SELECT tablet_id FROM tablets Where access_code = '$access_code' And tablet_id = '$tabId' And tablet_status = 1");

        if ($logTab->num_rows() > 0)
            $result = 1;
        return $result;
    }

    public function getMaidForFlatTab($flatId) {
        $maidList = $this->db->query("Select maid_id As id, maid_name, maid_nationality, maid_photo_file From maids Where flat_id = {$flatId}");
        return $maidsResult = $maidList->result();
    }

    function get_tablet_by_id($tablet_id) {
        $get_tablet_by_id_qry = $this->db->query("SELECT tablet_id, zone_id, imei, google_reg_id, access_code, tablet_status FROM tablets WHERE tablet_id = $tablet_id LIMIT 1");
        return $get_tablet_by_id_qry->row();
//        return mysql_fetch_object($get_tablet_by_id_qry);
    }

    function get_maids($active = TRUE) {
//        $db = new Database();

        $status = $active === TRUE ? 1 : 0;

        $active_qry = 'WHERE m.maid_status = ' . $status;

        $get_maids_qry = $this->db->query("SELECT m.maid_id, m.odoo_maid_id, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, f.flat_name, f.odoo_flat_id FROM maids m JOIN flats f ON m.flat_id = f.flat_id WHERE m.maid_status = 1 ORDER BY maid_name");

        $maid_list = array();
        $maids = $get_maids_qry->result();
        foreach ($maids as $row) {

            array_push($maid_list, $row);
        }

        return $maid_list;
    }

    public function get_booking_deletes_by_date($service_date) {
        $retrun = array();

        $get_booking_deletes_by_date_qry = $this->db->query("SELECT booking_id FROM booking_deletes WHERE service_date = '$service_date'");

        $deleteBookings = $get_booking_deletes_by_date_qry->result();
        foreach ($deleteBookings as $row) {
            array_push($retrun, $row->booking_id);
        }


        return $retrun;
    }

    public function get_schedule_by_flat($tablet_id, $zone_id, $service_date) {

        
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = $this->get_booking_deletes_by_date($service_date);
        
        $schedule_by_date_qry = "SELECT b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, LOWER(DATE_FORMAT(b.time_from, '%h%p')) AS time_from, LOWER(DATE_FORMAT(b.time_to, '%h%p')) AS time_to, b.booking_type, b.booking_note, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, m.maid_present_address, m.maid_permanent_address"
                . " FROM bookings b JOIN customers c ON b.customer_id = c.customer_id"
                . " JOIN maids m ON b.maid_id = m.maid_id"
                . " JOIN customer_addresses ca ON b.customer_address_id = ca.customer_address_id"
                . " JOIN areas a ON ca.area_id = a.area_id"
                . " JOIN zones z ON a.zone_id = z.zone_id"
                . " WHERE b.booking_status = 1 AND  b.booking_category = 'C' AND a.area_status = 1 AND z.zone_status = 1 AND m.maid_status = 1"
                . " AND ((b.service_actual_end_date >= '" . mysqli_real_escape_string($service_date) . "' AND service_end = 1) OR (service_end = 0))"
                . " AND ((b.service_start_date = '" . mysqli_real_escape_string($service_date) . "' AND b.booking_type = 'OD') OR (b.service_start_date <= '" . mysqli_real_escape_string($service_date) . "' AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= '" . mysqli_real_escape_string($service_date) . "' AND MOD(DATEDIFF(DATE('" . mysqli_real_escape_string($service_date) . "'), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW')) ";
        
        if (count($deleted_bookings) > 0) {
            $deleted_bookings = join(",", $deleted_bookings);
            $schedule_by_date_qry .= " AND b.booking_id NOT IN ($deleted_bookings)";
        }
        $schedule_by_date_qry .= " ORDER BY b.time_from";
        
        

//        echo $schedule_by_date_qry;exit;


        $booking_transfers = $this->get_booking_transfers_by_date($service_date);
        $transferred_booking_zones = array();
        foreach ($booking_transfers as $b_transfer) {
            $transferred_booking_zones[$b_transfer->booking_id] = $b_transfer->zone_id;
        }

        if ($tablet_id && $tablet_id > 0)
            $tablet = $this->get_tablet_by_id($tablet_id);
        $return = array();
        $i = 0;
        $get_schedule_by_date_qry = $this->db->query($schedule_by_date_qry);
        $getSchedules = $get_schedule_by_date_qry->result();
        if ($get_schedule_by_date_qry->num_rows() > 0) {
            foreach ($getSchedules as $row) {
//            while ($row = mysql_fetch_object($get_schedule_by_date_qry)) {

                $day_service_qry = $this->db->query("SELECT service_status, start_time, end_time FROM day_services WHERE service_date = '" . mysqli_real_escape_string($service_date) . "' AND booking_id = " . $row->booking_id . " LIMIT 1");
                $row->service_status = NULL;
                $row->start_time = NULL;
                $row->end_time = NULL;
                $ds_results = $day_service_qry->result();
                foreach ($ds_results as $ds_row) {
//                while ($ds_row = mysql_fetch_object($day_service_qry)) {
                    $row->service_status = $ds_row->service_status;
                    $row->start_time = $ds_row->start_time;
                    $row->end_time = $ds_row->end_time;
                }
//print_r($row);
//exit;
                if ($row->service_status == 2 || $row->service_status == 3) {
                    continue;
                }
                //if (( !isset($transferred_booking_zones[$row->booking_id])) || (isset($transferred_booking_zones[$row->booking_id]) && $transferred_booking_zones[$row->booking_id] == $tablet->zone_id)) {
                //if( ($row->zone_id == $tablet->zone_id && ($transferred_booking_zones[$row->booking_id] != $tablet->zone_id)) || (isset($transferred_booking_zones[$row->booking_id]) && $transferred_booking_zones[$row->booking_id] == $tablet->zone_id))//$row->zone_id == $tablet->zone_id && //(!isset($transferred_booking_zones[$row->booking_id]))
                $return[$i]['activeBookingId'] = $row->booking_id;
                $return[$i]['customerId'] = $row->customer_id;
                //$return[$i]['customer_address_id'] = $row->customer_address_id;
                $return[$i]['maidId'] = $row->maid_id;
                //$return[$i]['service_type_id'] = $row->service_type_id;
                //$return[$i]['service_start_date'] = $row->service_start_date;
                //$return[$i]['service_week_day'] = $row->service_week_day;
                $return[$i]['picktime'] = $row->time_to;
                $return[$i]['droptime'] = $row->time_from;
                $return[$i]['schedule'] = $row->time_from . '-' . $row->time_to;
                $return[$i]['booking_type'] = $row->booking_type;
                //$return[$i]['service_end'] = $row->service_end;
                //$return[$i]['service_end_date'] = $row->service_end_date;
                //$return[$i]['service_actual_end_date'] = $row->service_actual_end_date;
                $return[$i]['notes'] = $row->booking_note ? $row->booking_note : NULL;
                //$return[$i]['is_locked'] = $row->is_locked;
                //$return[$i]['pending_amount'] = $row->pending_amount;                    
                //$return[$i]['discount'] = $row->discount;
                //$return[$i]['booking_status'] = $row->booking_status;
                $return[$i]['customer_name'] = $row->customer_name;
                //$return[$i]['customer_nick_name'] = $row->customer_nick_name;
                $return[$i]['mobile1'] = $row->mobile_number_1;
                $return[$i]['key_status'] = $row->key_given == 'N' ? "0" : "1";
                //$return[$i]['payment_type'] = $row->payment_type;
                //$return[$i]['longitude'] = $row->longitude;
                // $return[$i]['latitude'] = $row->latitude;
                //$return[$i]['zone_id'] = $row->zone_id;
                $return[$i]['customer_zone'] = $row->zone_name;
                $return[$i]['customer_area'] = $row->area_name;
                $return[$i]['customer_adress'] = $row->customer_address;
                $return[$i]['maid_name'] = $row->maid_name;
                //$return[$i]['maid_nationality'] = $row->maid_nationality;
                //$return[$i]['maid_mobile_1'] = $row->maid_mobile_1;
                $return[$i]['maidPhoto'] = base_url() . 'maidimg/' . $row->maid_photo_file;
                $return[$i]['maidPhoto'] = "";
                $return[$i]['maidAddress'] = $row->maid_present_address;
                //$return[$i]['maid_permanent_address'] = $row->maid_permanent_address;
                $return[$i]['droppedtime'] = $row->start_time;
                $return[$i]['topicktime'] = $row->end_time;
                if ($row->start_time == null || $row->end_time == null) {
                    $return[$i]['cust_time_out'] = 0;
                    if ($row->start_time != null)
                        $return[$i]['cust_time_in'] = 1;
                    else
                        $return[$i]['cust_time_in'] = 0;
                }
                else {
                    $return[$i]['cust_time_in'] = 1;
                    $return[$i]['cust_time_out'] = 0;
                }

                ++$i;
                //}
            }
        }

        return $return;
    }

    function get_booking_transfers_by_date($service_date) {
        $get_booking_transfers_by_date_qry = $this->db->query("SELECT booking_transfer_id, booking_id, zone_id FROM booking_transfers WHERE service_date = '{$service_date}' GROUP BY booking_id ORDER BY transferred_time DESC");

        $booking_transfers = array();
        $bookings = $get_booking_transfers_by_date_qry->result();
        foreach ($bookings as $row) {
//        while ($row = mysql_fetch_object($get_booking_transfers_by_date_qry)) {
            array_push($booking_transfers, $row);
        }

        return $booking_transfers;
    }

    public function get_zone_name_by_bookingid($bookingid, $service_date) {
        $regId = $this->db->query("Select z.zone_name From zones as z join booking_transfers as b on z.zone_id = b.zone_id Where b.booking_id = '$bookingid' and b.service_date = '$service_date'");
        return $regId->result();
    }

    function get_maid_attandence_by_date($maid_id, $date) {
        $get_maid_attandence_by_date_qry = $this->db->query("SELECT attandence_id, maid_id, zone_id, tablet_id, date, maid_in_time, maid_out_time, attandence_status FROM maid_attandence WHERE maid_id = {$maid_id} AND date = '{$date}' ORDER BY maid_in_time DESC LIMIT 1");
        return $get_maid_attandence_by_date_qry->row();
    }

    function get_day_service_by_booking_id($date, $booking_id) {
//        echo "SELECT ds.day_service_id, ds.booking_id, ds.total_fee, ds.material_fee, ds.service_status, c.customer_id, c.payment_type FROM day_services ds JOIN bookings b ON ds.booking_id = b.booking_id JOIN customers c ON b.customer_id = c.customer_id WHERE ds.booking_id = $booking_id AND ds.service_date = '$date' LIMIT 1";exit;
        $get_day_service_by_booking_id_qry = $this->db->query("SELECT ds.day_service_id, ds.booking_id, ds.total_fee, ds.material_fee, ds.service_status, c.customer_id, c.payment_type FROM day_services ds JOIN bookings b ON ds.booking_id = b.booking_id JOIN customers c ON b.customer_id = c.customer_id WHERE ds.booking_id = $booking_id AND ds.service_date = '$date' LIMIT 1");
        return $get_day_service_by_booking_id_qry->row();
    }
	
	function get_tablet_by_zone_tabid($tabletidval) {
        $this->db->select('tablet_id, tablet_driver_name, zone_id, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->where('tablet_id', $tabletidval)
                //->where('flat_id', 0)
                ->where('tablet_status', 1)
                ->limit(1);

        $get_tablet_by_zone_qry = $this->db->get();

        return $get_tablet_by_zone_qry->row();
    }
	
	function get_all_drivers($hiddentabletid) {
        $this->db->select('tablet_id, tablet_driver_name')
                ->from('tablets')
                ->where('tablet_id !=', $hiddentabletid)
                ->where('tablet_status', 1);
        $get_tab_by_qry = $this->db->get();
        return $get_tab_by_qry->result();
    }
	
	function get_all_newdrivers() {
        $this->db->select('tablet_id, tablet_driver_name')
                ->from('tablets')
                //->where('tablet_id !=', $hiddentabletid)
                ->where('tablet_status', 1);
        $get_tab_by_qry = $this->db->get();
        return $get_tab_by_qry->result();
    }
	
	function get_all_tabletdrivers() {
        $this->db->select('t.tablet_id, t.tablet_driver_name, z.zone_name')
                ->from('tablets t')
                ->join('zones z', 't.zone_id = z.zone_id', 'left')
                ->where('tablet_status', 1);
        $get_tab_by_qry = $this->db->get();
        return $get_tab_by_qry->result();
    }
    function report_srch_driver_news($searchterm)
    {
        $this->db->select('t.tablet_id, t.tablet_driver_name, z.zone_name')
            ->from('tablets t')
            ->join('zones z', 't.zone_id = z.zone_id', 'left')
            ->order_by('t.tablet_id', 'DESC');
        if (isset($searchterm) && strlen($searchterm) > 0) {
            $this->db->where("(tablet_driver_name LIKE '%" . $searchterm . "%' OR zone_name LIKE '%" . $searchterm . "%')", NULL, FALSE);
        }
        $this->db->limit(50);
        $this->db->where('tablet_status', 1);
       $get_driver_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_driver_qry->result();
    }

    

}

