<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Daily_collection_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }



    function maid_services($maid_id)
    {
        $qr = $this->db->select('maid_service_type_joins.maid_service_type_jon_id, maid_service_type_joins.service_type_id, service_types.service_type_name, service_types.odoo_service_id')
            ->from('maid_service_type_joins')
            ->join('service_types', 'maid_service_type_joins.service_type_id = service_types.service_type_id')
            ->where('maid_service_type_joins.maid_id', $maid_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    // ********************************************************************************************************************


    function get_all_maids_new()
    {
        $this->db->select('maids.maid_id, maids.maid_leader_id, maids.maid_name, 
             maids.employee_type_id, 
            COALESCE(leader.maid_name, "No Leader") AS leader_name', FALSE);
        $this->db->from('maids');
        $this->db->join('maids AS leader', 'maids.maid_leader_id = leader.maid_id', 'LEFT');
        $this->db->join('employee_types', 'employee_types.employee_type_id = maids.employee_type_id', 'LEFT');
        $this->db->where('maids.maid_status', 1);
        $this->db->order_by('leader_name', 'asc');

        $query = $this->db->get();
        // log_message('error', $this->db->last_query());
        return $query->result_array();
    }
    // **********************************************************************************************************

    function get_collection_data($date, $id, $status)
    {
        $this->db->select('ds.day_service_id,
        ds.day_service_reference_id,ds.booking_id,ds.customer_id,ds.maid_id,ds.customer_name,ds.maid_id,
        ds.customer_address,ds.service_date,ds.payment_status,ds._service_hours,ds._total_amount,
        b.customer_address_id,
        m.maid_leader_id,m.maid_name,
        ca.location_id,ca.landmark_id,ca.area_id,
        l.location_name,
        a.area_name,a.zone_id,
        la.landmark_name,
        cp.paid_amount',);
        $this->db->from('day_services as ds')
            ->join('bookings b', 'b.booking_id = ds.booking_id', 'left')
            ->join('customer_addresses ca', 'ca.customer_address_id = b.customer_address_id', 'left')
            ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
            ->join('maids m', 'ds.maid_id = m.maid_id', 'left')
            ->join('locations l', 'ca.location_id = l.location_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('landmarks la', 'ca.landmark_id= la.landmark_id', 'left');
        $this->db->where('ds.service_date', $date);
        $this->db->where('ds.maid_id', $id);


        if ($status == 0) {
            $this->db->where('ds.payment_status', 0);
        } else if ($status == 1) {
            $this->db->where('ds.payment_status', 1);
        }
        $this->db->order_by('ds.service_date', 'desc');
        $query = $this->db->get();
        // log_message('error', $this->db->last_query());
        return $query->result_array();
    }

    // ********************************************************************************************************
    public function get_all_leader_maid($leader_id)
    {
        $this->db->select('maids.maid_id, maids.maid_leader_id, maids.maid_name');
        $this->db->from('maids');
        $this->db->where('maids.maid_status', 1);

        if ($leader_id !== 'no_leader') {
            $this->db->where('maids.maid_leader_id', $leader_id);
        } else {
            $this->db->where('maids.maid_leader_id IS NULL');
            $this->db->or_where('maids.maid_leader_id', ''); 
        }

        $query = $this->db->get();
        // log_message('error', $this->db->last_query());
        return $query->result_array();
    }

    // *****************************************************************************************
}
