<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/** 
 * Bookings_model Class 
 * 
 * @package	Emaid
 * @author		Azinova Developers 
 * @since		Version 1.0
 */
class Bookings_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /** 
     * Add booking
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	array
     * @return	int
     */
    function add_activity($booking_id, $action_type)
    {
        $booking = $this->get_booking_by_id($booking_id);

        if (!empty($booking)) {
            $data = array();
            $data['added_user'] = user_authenticate();
            $data['booking_type'] = $booking->booking_type == 'OD' ? 'One Day' : 'Every Week';
            $data['shift'] = $booking->time_from . '-' . $booking->time_to;
            $data['action_type'] = $action_type;
            $data['addeddate'] = date('Y-m-d H:i:s');
            //$data['action_content'] = $booking->maid_name . ' to ' . $booking->customer_name . ', for ' . $booking->service_start_date . ' as start date';
            if ($booking->is_admin == "Y") {
                if ($action_type == "Booking_add") {
                    $data['action_content'] = "A new booking for customer: " . $booking->customer_name . " is created.";
                } else {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is updated.";
                }
            } else {
                if ($action_type == "Booking_add") {
                    $data['action_content'] = "A new booking for customer: " . $booking->customer_name . " is created.";
                } else {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is updated.";
                }
            }
            $this->db->set($data);
            $this->db->insert('user_activity');

            return $this->db->insert_id();
        }
    }

    function insert_driverchnage($data, $transferids)
    {
        $this->db->set($data);
        $this->db->insert('booking_transfers_tablet');
        $insert_id = $this->db->insert_id();
        if ($insert_id > 0) {
            if ($transferids != 0) {
                $this->db->where('booking_transfer_tablet_id', $transferids);
                $this->db->delete('booking_transfers_tablet');
            }
            $activity_add = array();
            $activity_add['added_user'] = $data['transferred_by'];
            $activity_add['booking_type'] = '';
            $activity_add['transferred_bookid'] = $data['booking_id'];
            $activity_add['action_type'] = "driver_change";
            $activity_add['action_content'] = "User changed driver for booking " . $data['booking_id'] . " dated " . $data['service_date'];
            $activity_add['addeddate'] = $data['transferred_date_time'];
            $activity_add['date_time_added'] = $data['transferred_date_time'];
            $this->db->set($activity_add);
            $this->db->insert('user_activity');
            return $insert_id = $this->db->insert_id();

        }
    }
    function phone_exists($phone)
    {
        $this->db->where('mobile_number_1', $phone);
        $this->db->or_where('mobile_number_2', $phone);
        $this->db->or_where('mobile_number_3', $phone);
        $this->db->or_where('phone_number', $phone);
        $query = $this->db->get('customers');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    function phone_exists_new($phone)
    {
        $this->db->where('mobile_number_1', $phone);
        $this->db->or_where('mobile_number_2', $phone);
        $this->db->or_where('mobile_number_3', $phone);
        $this->db->or_where('phone_number', $phone);
        $query = $this->db->get('customers');
        //echo $this->db->last_query();exit;
        return $query->num_rows();
        //if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
    }
    function add_delete_activity($booking_id, $action_type, $deleted_by)
    {
        $booking = $this->get_booking_by_id($booking_id);

        if (!empty($booking)) {
            $data = array();
            //$data['added_user'] = $booking->booked_by;
            $data['added_user'] = $deleted_by;
            $data['booking_type'] = $booking->booking_type == 'OD' ? 'One Day' : 'Every Week';
            $data['shift'] = $booking->time_from . '-' . $booking->time_to;
            $data['action_type'] = $action_type;
            $data['addeddate'] = date('Y-m-d H:i:s');
            //$data['action_content'] = $booking->maid_name . ' to ' . $booking->customer_name . ', for ' . $booking->service_start_date . ' as start date';
            if ($booking->is_admin == "Y") {
                if ($action_type == "Add") {
                    $data['action_content'] = "A new booking for customer: " . $booking->customer_name . " is created.";
                } else if ($action_type == "Update") {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is updated.";
                } 
                else if ($action_type == "One_day_Cancel") {
                    $data['action_content'] = "Booking #".$booking_id." for customer: " . $booking->customer_name . " is paused.";
                } 
                else if ($action_type == "One_day_Restart") {
                    $data['action_content'] = "Booking #".$booking_id." for customer: " . $booking->customer_name . " is resumed.";
                }
                else {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is deleted.";
                }
            } else {
                if ($action_type == "Add") {
                    $data['action_content'] = "A new booking for customer: " . $booking->customer_name . " is created.";
                } else if ($action_type == "Update") {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is updated.";
                }
                else if ($action_type == "One_day_Cancel") {
                    $data['action_content'] = "Booking #".$booking_id." for customer: " . $booking->customer_name . " is paused.";
                }
                else if ($action_type == "One_day_Restart") {
                    $data['action_content'] = "Booking #".$booking_id." for customer: " . $booking->customer_name . " is resumed.";
                }else {
                    $data['action_content'] = "A booking for customer: " . $booking->customer_name . " is deleted.";
                }
            }
            $this->db->set($data);
            $this->db->insert('user_activity');

            return $this->db->insert_id();
        }
    }

    function add_booking_remarks($fields = array())
    {
        $fields['deleted_date_time'] = isset($fields['deleted_date_time']) ? $fields['deleted_date_time'] : date('Y-m-d H:i:s');

        //$fields['service_date'] = date('Y-m-d');
        $this->db->set($fields);
        $this->db->insert('booking_delete_remarks');
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }
    function check_booking($fields)
    {
        $custid = $fields['customer_id'];
        $customer_address_id = $fields['customer_address_id'];
        $maid_id = $fields['maid_id'];
        $service_type_id = $fields['service_type_id'];
        $service_start_date = $fields['service_start_date'];
        $repeat_day = $fields['service_week_day'];
        $time_from = $fields['time_from'];
        $time_to = $fields['time_to'];
        $booking_type = $fields['booking_type'];
        $repeat_end = $fields['service_end'];
        $service_end_date = $fields['service_end_date'];
        $booking_note = $fields['booking_note'];
        $pending_amount = $fields['pending_amount'];
        $discount = $fields['discount'];
        $total_amts = $fields['total_amount'];
        $booking_category = 'C';
        $booked_by = user_authenticate();
        $booking_status = 1;
        $cleaning_material = $fields['cleaning_material'];
        $justmop_reference = $fields['justmop_reference'];

        $this->db->select("b.*")
            ->from('bookings b')
            ->where('b.customer_id', $custid)
            ->where('b.customer_address_id', $customer_address_id)
            ->where('b.maid_id', $maid_id)
            ->where('b.service_type_id', $service_type_id)
            ->where('b.service_start_date', $service_start_date)
            ->where('b.service_week_day', $repeat_day)
            ->where('b.time_from', $time_from)
            ->where('b.time_to', $time_to)
            ->where('b.booking_type', $booking_type)
            ->where('b.service_end', $repeat_end)
            ->where('b.service_end_date', $service_end_date)
            ->where('b.booking_status', 1);

        $get_duplicate_qry = $this->db->get();


        return $get_duplicate_qry->num_rows();
    }
    function add_booking($fields = array())
    {
        $fields['service_actual_end_date'] = isset($fields['service_actual_end_date']) ? $fields['service_actual_end_date'] : $fields['service_end_date'];
        $fields['booked_datetime'] = isset($fields['booked_datetime']) ? $fields['booked_datetime'] : date('Y-m-d H:i:s');
        $fields['booking_status'] = isset($fields['booking_status']) ? $fields['booking_status'] : 0;

        $this->db->set($fields);
        $this->db->insert('bookings');

        $booking_id = $this->db->insert_id();
        $this->add_activity($booking_id, 'Booking_add');

        return $booking_id;
    }

    function getbookingsbymaid_id_new($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deleted_bookings = array();
        $deletes = $this->get_booking_deletes_by_date($service_date);
        if (!empty($deletes)) {
            foreach ($deletes as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }
        }


        //$this->db->select('round(SUM(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600)) as total_hrs', FALSE)
        //new hidden//$this->db->select('round(SUM(HOUR(TIMEDIFF(bookings.time_to, bookings.time_from)))) as total_hrs', FALSE)
        $this->db->select('SUM(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600) as total_hrs', FALSE)
            //$this->db->select('bookings.time_to, bookings.time_from', FALSE)
            ->from('bookings')
            ->join('customers', 'bookings.customer_id = customers.customer_id')
            //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
            ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
            ->join('maids', 'maids.maid_id = bookings.maid_id')
            ->join('areas', 'customer_addresses.area_id = areas.area_id')
            ->join('zones', 'areas.zone_id = zones.zone_id')
            ->where('bookings.booking_category', 'C')
            ->where('bookings.booking_status', 1)
            ->where('maids.maid_id', $maid_id)
            ->where('maids.maid_status', 1);


        if (!empty($deleted_bookings)) {
            $this->db->where_not_in('bookings.booking_id', $deleted_bookings);
        }


        if ($service_date != "") {
            $this->db->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE);
        }

        // if($zone_id != "") {
        // $this->db->where('zones.zone_id',$zone_id);
        // }
        // $this->db->order_by('bookings.time_from');
        // $this->db->order_by('zones.driver_name');
        $query = $this->db->get();
        // echo $this->db->last_query();
        // exit;
        return $query->result();
    }


    function add_push_notifications($fields = array())
    {

        $fields['addeddate'] = isset($fields['addeddate']) ? $fields['addeddate'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('push_notications');
        return $this->db->insert_id();
    }
    /** 
     * Add booking
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	array
     * @return	int
     */
    function add_booking_from_mobile($fields = array())
    {

        $this->db->set($fields);
        $this->db->insert('mobile_bookings');
        return $this->db->insert_id();
    }

    /** 
     * Get booking by id
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_booking_by_id($booking_id)
    {
        $this->db->select("b.time_from, b.time_to, z.zone_id, b.booking_type, b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.justmop_reference, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.booking_note, b._service_rate_per_hour, b._service_discount_rate_per_hour, b._service_hours, b._taxable_amount, b._total_discount, b._cleaning_material, b._vat_percentage, b._cleaning_material_rate_per_hour, b.price_per_hr, b.discount_price_per_hr, b.discount, b.net_service_cost, b.service_charge, b.vat_charge, b.total_amount, b.pending_amount, c.customer_name,c.mobile_number_1, c.email_address, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name,ca.area_id, b.is_locked, b.booked_by, m.maid_name, b.service_end_date, b.cleaning_material,b.booked_from,b.pay_by,u.is_admin,c.balance,c.signed,b.month_durations,b.tabletid,DATE_FORMAT(b.time_from, '%h:%i %p') AS newtime_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS newtime_to", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids m', 'b.maid_id = m.maid_id', 'left')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_category', 'C')
            ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();

        return $get_bookings_by_id_qry->row();
    }
    function get_booking_by_id_new($booking_id)
    {
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_end_date, b.is_locked, b.booking_note, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.time_from as from_time, b.time_to as to_time, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.booked_by, b.discount, c.customer_name, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, c.mobile_number_1, c.email_address, ca.customer_address, a.area_name,m.maid_name,DATE_FORMAT(b.time_from, '%h:%i %p') AS newtime_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS newtime_to", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_category', 'C')
            ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();

        return $get_bookings_by_id_qry->row();
    }
    /** 
     * Get booking by id for delete
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_booking_by_id_fordelete($booking_id)
    {
        $this->db->select("b.time_from, b.time_to, z.zone_id, b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_actual_end_date, b.booking_status, b.booking_note, b.discount, b.pending_amount, c.customer_name,c.mobile_number_1, c.email_address, c.payment_type, c.price_hourly, c.price_extra, c.price_weekend, ca.customer_address, a.area_name, b.is_locked, b.booked_by, m.maid_name, b.service_end_date,DATE_FORMAT(b.time_from, '%h:%i %p') AS newtime_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS newtime_to", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids m', 'b.maid_id = m.maid_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_category', 'C')
            ->limit(1);

        $get_bookings_by_id_qry = $this->db->get();

        return $get_bookings_by_id_qry->row();
    }

    /** 
     * Get bookings by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	str
     * @return	array
     */

    function get_servicemapping_by_bukid($booking_id)
    {
        $this->db->select("bsm.service_id,bsm.category_id,bsm.sub_category_id,bsm.service_sub_furnish_id,st.service_type_name,sc.service_category_name,sbc.sub_category_name,sf.service_furnish_name,bsm.name,bsm.service_cost ", FALSE)
            ->from('bookingservicemapping bsm')
            ->join('service_types st', 'st.service_type_id = bsm.service_id')
            ->join('servicecategory sc', 'sc.id = bsm.category_id')
            ->join('servicesubcategory sbc', 'sbc.id = bsm.sub_category_id', 'left')
            ->join('servicesubfurnishcategory sf', 'sf.id = bsm.service_sub_furnish_id', 'left')
            ->where('bsm.status', '1')
            ->where('bsm.booking_id', $booking_id);

        $get_servicemapping_by_bukid_qry = $this->db->get();

        return $get_servicemapping_by_bukid_qry->result();
    }

    public function get_servicemapping_by_booking_ids($booking_ids = [])
    {
        $this->db->select("bsm.booking_id, bsm.service_id,bsm.category_id,bsm.sub_category_id,bsm.service_sub_furnish_id,st.service_type_name,sc.service_category_name,sbc.sub_category_name,sf.service_furnish_name,bsm.name,bsm.service_cost ", FALSE)
            ->from('bookingservicemapping bsm')
            ->join('service_types st', 'st.service_type_id = bsm.service_id')
            ->join('servicecategory sc', 'sc.id = bsm.category_id')
            ->join('servicesubcategory sbc', 'sbc.id = bsm.sub_category_id', 'left')
            ->join('servicesubfurnishcategory sf', 'sf.id = bsm.service_sub_furnish_id', 'left')
            ->where('bsm.status', '1')
            ->where_in('bsm.booking_id', $booking_ids);

        $get_servicemapping_by_bukid_qry = $this->db->get();

        return $get_servicemapping_by_bukid_qry->result();
    }

    function get_bookings_by_date($service_date)
    {
        $this->db->select("b.booking_id, b.maid_id, b.service_start_date, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_status, c.customer_nick_name", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->where('b.booking_category', 'C')
            ->where('b.service_start_date', $service_date);

        $get_bookings_by_date_qry = $this->db->get();

        return $get_bookings_by_date_qry->result();
    }

    /** 
     * Get schedule by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_schedule_by_date($service_date, $team_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.tabletid, b.pay_by, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.pending_amount, b.discount, b.service_charge, b.vat_charge, b.total_amount, b.booking_status, b.price_per_hr, b.discount_price_per_hr, b.crew_in, b.net_service_cost, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.payment_mode, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address,ca.building, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, ca.building, ca.unit_no, ca.street, st.service_type_name, ds.service_status", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->join('service_types st', 'b.service_type_id = st.service_type_id', 'left')
            ->join('day_services ds', "b.booking_id = ds.booking_id AND ds.service_date='$service_date'", 'left')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        // echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_schedule_by_date_avail($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_schedule_by_date_avail_new($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where('m.maid_id', $maid_id)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_schedule_by_date_avail_new_booking($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where('m.maid_id', $maid_id)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        // if (count($deleted_bookings) > 0) {
        //     $this->db->where_not_in('b.booking_id', $deleted_bookings);
        // }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }


    function get_schedule_by_date_job($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.email_address, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, CONCAT(DATE_FORMAT(b.time_from, '%h:%i %p'), '-' , DATE_FORMAT(b.time_to, '%h:%i %p')) AS shift,b.total_amount", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_delayed_schedule_by_date_job($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $dayservices = $this->get_booking_dayservices_by_date($service_date);
        $dayservices_bookings = array();
        foreach ($dayservices as $dayservice) {
            $dayservices_bookings[] = $dayservice->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material,b.time_from as timefrom, b.time_to as timeto, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.email_address, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, CONCAT(DATE_FORMAT(b.time_from, '%h:%i %p'), '-' , DATE_FORMAT(b.time_to, '%h:%i %p')) AS shift,b.total_amount", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
        if (count($dayservices_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $dayservices_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_schedule_recurring_date_job($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.email_address, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, CONCAT(DATE_FORMAT(b.time_from, '%h:%i %p'), '-' , DATE_FORMAT(b.time_to, '%h:%i %p')) AS shift,b.total_amount", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.booking_type', 'WE')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    /** 
     * Get schedule by date count
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_schedule_by_date_counts($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        //$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
        $this->db->select("count(b.booking_id) as total")
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('m.maid_name')
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->row_array()['total'];
    }

    function total_booking_count()
    {
        $this->db->select("count(booking_id) as total")
            ->from("bookings")
            ->where('booking_category', 'C')
            ->where('booking_status', 1);
        $qry = $this->db->get();
        return $qry->row_array()['total'];
    }

    function get_maid_booking_by_week_day_new($service_date, $maid_id, $repeat_day)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.maid_id', $maid_id)
            ->where('b.service_week_day', $service_week_day);
        //->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
        //->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function total_invoices_count()
    {
        $this->db->select("count(day_service_id) as total")
            ->from("day_services")
            ->where('service_status', 2);
        $qry = $this->db->get();
        return $qry->row_array()['total'];
    }

    function get_maid_schedule_by_date($maid_id, $service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, b.service_charge, b.vat_charge, b.total_amount, b.cleaning_material, c.customer_name, c.price_hourly, c.customer_source, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.maid_id', $maid_id)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();

        return $get_schedule_by_date_qry->result();
    }

    /** 
     * Get maid bookings on zone by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int, int, str
     * @return	array
     */
    function get_maid_bookings_on_zone_by_date($maid_id, $zone_id, $service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('a.zone_id', $zone_id)
            ->where('b.maid_id', $maid_id)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_maid_bookings_on_zone_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;		
        return $get_maid_bookings_on_zone_by_date_qry->result();
    }
    function get_same_zone_maids($zone_id, $maid_ids, $service_week_day)
    {
        $this->db->select("b.maid_id", FALSE)
            ->from('bookings b')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where_in('b.maid_id', $maid_ids)
            ->where('b.booking_category', 'C')
            ->where('z.zone_id', $zone_id)
            ->where('b.service_week_day', $service_week_day)
            ->where('b.booking_status', 1)
            ->group_by('b.maid_id');

        $get_maid_booking_by_week_day_qry = $this->db->get();

        return $get_maid_booking_by_week_day_qry->result();
    }
    /** 
     * Get maid booking by week day
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int, int, str
     * @return	array
     */
    function get_maid_booking_by_week_day($maid_id, $service_week_day)
    {
        $this->db->select("booking_id, maid_id, service_start_date, service_week_day, DATE_FORMAT(time_from, '%H:%i') AS time_from, DATE_FORMAT(time_to, '%H:%i') AS time_to, service_end, service_end_date, service_actual_end_date, booking_type, booking_status", FALSE)
            ->from('bookings')
            ->where('maid_id', $maid_id)
            ->where('service_week_day', $service_week_day)
            ->where('booking_category', 'C')
            ->where('booking_status', 1);

        $get_maid_booking_by_week_day_qry = $this->db->get();

        return $get_maid_booking_by_week_day_qry->result();
    }

    function get_maid_booking_by_week_day_forfree($maid_id, $service_week_day, $service_date)
    {
        $this->db->select("booking_id, maid_id, service_start_date, service_week_day, DATE_FORMAT(time_from, '%H:%i') AS time_from, DATE_FORMAT(time_to, '%H:%i') AS time_to, service_end, service_end_date, service_actual_end_date, booking_type, booking_status", FALSE)
            ->from('bookings')
            ->where('maid_id', $maid_id)
            ->where('service_week_day', $service_week_day)
            ->where('booking_category', 'C')
            ->where('booking_status', 1)
            ->where("((service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);

        $get_maid_booking_by_week_day_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_maid_booking_by_week_day_qry->result();
    }

    /** 
     * Get maid booking by week day for update by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int, int, str
     * @return	array
     */
    function get_maid_booking_by_week_dayupdate($maid_id, $service_week_day, $update_date)
    {
        $deletes = $this->get_booking_deletes_by_date($update_date);
        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }


        $this->db->select("booking_id, maid_id, service_start_date, service_week_day, DATE_FORMAT(time_from, '%H:%i') AS time_from, DATE_FORMAT(time_to, '%H:%i') AS time_to, service_end, service_end_date, service_actual_end_date, booking_type, booking_status", FALSE)
            ->from('bookings')
            ->where('maid_id', $maid_id)
            ->where('service_week_day', $service_week_day)
            ->where('booking_category', 'C')
            ->where('service_actual_end_date >', $update_date)
                // ->where('service_actual_end_date > service_end_date',null,false)
            ->where('booking_status', 1);
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('booking_id', $deleted_bookings);
        }

        $get_maid_booking_by_week_day_qry = $this->db->get();

        return $get_maid_booking_by_week_day_qry->result();
    }

    /** 
     * Update booking
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int, array
     * @return	int
     */
    function update_booking($booking_id, $fields = array(), $action = 'Update')
    {
        $this->db->where('booking_id', $booking_id);
        $this->db->update('bookings', $fields);
        $affected = $this->db->affected_rows();
        if ($action == 'Delete') {

            $booking = $this->get_booking_by_id($booking_id);
            $usersid = user_authenticate();
            if ($booking->booking_type == 'WE') {
                $this->add_delete_activity($booking_id, 'Weekly_Cancel', $usersid);
            } else {
                $this->add_delete_activity($booking_id, 'One_day_Cancel', $usersid);
            }
        } else {
            $action = "Booking_update";
            $this->add_activity($booking_id, $action);
        }
        return $affected;
    }

    /**
     * Restart booking
     */
    function restart_booking($booking_id, $fields = array(), $service_date)
    {
        $booking = $this->get_booking_by_id($booking_id);
        if ($booking->booking_type == 'OD') {
            // $this->db->where('booking_id', $booking_id);
            // $this->db->update('bookings', $fields);
            // $affected = $this->db->affected_rows();
            $this->db->where('booking_id', $booking_id);
            $this->db->where('service_date', $service_date);
            $this->db->delete('booking_deletes');
        }
        //$booking = $this->get_booking_by_id($booking_id);
        $usersid = user_authenticate();
        if ($booking->booking_type == 'WE' || $booking->booking_type == 'BW') {
            //$this->delete_booking_from_delete($booking_id,$service_date);
            $this->db->where('booking_id', $booking_id);
            $this->db->where('service_date', $service_date);
            $this->db->delete('booking_cancel');

            $this->db->where('booking_id', $booking_id);
            $this->db->where('service_date', $service_date);
            $this->db->delete('booking_deletes');

            //$this->db->delete('booking_cancel', array('booking_id' => $booking_id,'service_date' => $service_date));
            //$this->db->delete('booking_deletes', array('booking_id' => $booking_id,'service_date' => $service_date));
            return $this->add_delete_activity($booking_id, 'One_day_Restart', $usersid);
        } else {
            return $this->add_delete_activity($booking_id, 'One_day_Restart', $usersid);
        }

        //return $affected;
    }

    /** 
     * Add booking delete
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	array
     * @return	int
     */
    function add_booking_delete($fields = array())
    {
        $fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('booking_deletes');
        $insert_id = $this->db->insert_id();

        $this->add_booking_cancel($fields);
        //$this->add_activity($fields['booking_id'], 'One day cancel');
        $this->add_delete_activity($fields['booking_id'], 'One_day_Cancel', $fields['deleted_by']);
        return $fields;
    }
    function add_booking_cancel($fields = array())
    {
        $fields['added_datetime'] = isset($fields['added_datetime']) ? $fields['added_datetime'] : date('Y-m-d H:i:s');

        $fields['service_date'] = date('Y-m-d');
        $this->db->set($fields);
        $this->db->insert('booking_cancel');
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    /** 
     * Get booking deletes by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        if ($service_end_date != NULL) {
            $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
        } else {
            $this->db->where("service_date", $service_date);
        }


        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }

    function check_booking_deletes_by_id_and_date($booking_id, $service_date)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');

        $this->db->where("booking_id", $booking_id);
        $this->db->where("service_date", $service_date);

        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();



    }

    function get_booking_dayservices_by_date($service_date)
    {
        $this->db->select('booking_id, service_status')
            ->from('day_services');
        $this->db->where("service_date", $service_date);

        $get_booking_dayservices_by_date_qry = $this->db->get();

        return $get_booking_dayservices_by_date_qry->result();
    }

    /** 
     * Get booking deletes by date
     * 
     * @author	Geethu
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_oneday_cancel_bookings_by_date($service_date)
    {

        $this->db->select("bd.booking_delete_id, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, c.customer_name, c.customer_nick_name, m.maid_name", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('booking_deletes bd', 'bd.booking_id = b.booking_id')
            ->where('bd.service_date', $service_date);

        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }
    /** 
     * Add booking delete
     * 
     * @author	Geethu
     * @acces	public 
     * @param	array
     * @return	int
     */
    function undo_booking_delete($id)
    {
        $this->db->where('booking_delete_id', $id);
        $this->db->delete('booking_deletes');

        return $this->db->affected_rows();
    }

    /** 
     * Add booking transfer
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	array
     * @return	int
     */
    function add_booking_transfer($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('booking_transfers');

        return $this->db->insert_id();
    }

    /** 
     * Delete booking transfer
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	int
     * @return	none
     */
    function delete_booking_transfer($booking_transfer_id)
    {
        $this->db->where('booking_transfer_id', $booking_transfer_id);
        $this->db->delete('booking_transfers');
    }

    /** 
     * Get booking transfers by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_booking_transfers_by_date($service_date)
    {
        $this->db->select('booking_transfer_id, booking_id, zone_id')
            ->from('booking_transfers')
            ->where('service_date', $service_date)
            ->group_by('booking_id')
            ->order_by('transferred_time', 'desc');

        $get_booking_transfers_by_date_qry = $this->db->get();

        return $get_booking_transfers_by_date_qry->result();
    }

    function get_booking_transfers_by_date_new($service_date)
    {
        $this->db->select('booking_transfer_tablet_id, booking_id, transfering_to_tablet')
            ->from('booking_transfers_tablet')
            ->where('service_date', $service_date)
            ->group_by('booking_id')
            ->order_by('booking_transfer_tablet_id', 'desc');

        $get_booking_transfers_by_date_qry = $this->db->get();

        return $get_booking_transfers_by_date_qry->result();
    }

    /** 
     * Get booking deletes by date
     * 
     * @author	Geethu
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_new_bookings()
    {
        $this->db->select("GROUP_CONCAT( b.booking_id SEPARATOR  ',' ) AS booking_id, b.service_start_date, GROUP_CONCAT( b.service_week_day SEPARATOR  ',' ) AS service_week_day, GROUP_CONCAT( CONCAT( DATE_FORMAT( b.time_from,  '%H:%i %p' ) ,  '-', DATE_FORMAT( b.time_to,  '%H:%i %p' ) ) SEPARATOR  ',' ) AS shift, b.booking_type, s.service_type_name, c.customer_name, z.zone_name, a.area_name, ca.customer_address", FALSE)
            ->from('mobile_bookings b')
            ->join('service_types s', 'b.service_type_id = s.service_type_id')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('b.booking_status', 0)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->group_by('c.customer_id, b.service_start_date, b.booking_type')
            ->order_by('b.booked_datetime', 'desc');

        $get_new_bookings_qry = $this->db->get();

        //	echo $this->db->last_query();	

        return $get_new_bookings_qry->result();
    }

    /** 
     * Get booking deletes by date
     * 
     * @author	Geethu
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_online_bookings()
    {
        $this->db->select(" b.booking_id, b.service_start_date, b.service_week_day, CONCAT( DATE_FORMAT( b.time_from,  '%H:%i %p' ) ,  '-', DATE_FORMAT( b.time_to,  '%H:%i %p' ) ) AS shift, b.booking_type, s.service_type_name, c.customer_id, c.customer_name, c.mobile_number_1, z.zone_name, a.area_name, ca.customer_address", FALSE)
            ->from('mobile_bookings b')
            ->join('service_types s', 'b.service_type_id = s.service_type_id')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('b.booking_status', 0)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->order_by('b.booked_datetime', 'desc');

        $get_new_bookings_qry = $this->db->get();

        //echo $this->db->last_query(); exit();

        return $get_new_bookings_qry->result();
    }

    /** 
     * Update booking
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int, array
     * @return	int
     */
    function delete_booking($booking_id, $fields = array())
    {
        $this->db->set('booking_status', 2);
        $this->db->where_in('booking_id', $booking_id);

        $this->db->update('mobile_bookings');

        return $this->db->affected_rows();
    }
    /** 
     * Add maid change request
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	array
     * @return	int
     */
    function add_maid_change_request($fields = array())
    {

        $fields['reqested_datetime'] = isset($fields['reqested_datetime']) ? $fields['reqested_datetime'] : date('Y-m-d H:i:s');
        $fields['status'] = isset($fields['status']) ? $fields['status'] : 0;

        $this->db->set($fields);
        $this->db->insert('maid_change_request');
        return $this->db->insert_id();
    }
    /** 
     * Get schedule by zone and date
     * 
     * @author	Geethu
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_schedule_by_zone($service_date, $zone_id)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
        if ($zone_id > 0) {
            $this->db->where('z.zone_id', $zone_id);
        }

        $get_schedule_by_date_qry = $this->db->get();

        return $get_schedule_by_date_qry->result();
    }
    /**
     * Get total booking hours
     * 
     * @author   Geethu
     * @access   public
     * @param    date, type : 1- plan, 2-activity
     * @return   array
     */

    function get_total_booking_hours($date_from = NULL, $type = 1, $date_to = NULL)
    {
        $service_week_day = date('w', strtotime($date_from));

        $deletes = $this->get_booking_deletes_by_date($date_from);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        if ($type === 1) {
            $this->db->select("SUM(TIME_TO_SEC(TIMEDIFF(time_to,time_from))) AS duration", FALSE)
                ->from('bookings')
                ->where('booking_category', 'C')
                ->where('booking_status', 1)
                ->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE)
                ->limit(1);
        } else if ($type === 2) {
            $this->db->select("SUM(TIME_TO_SEC(TIMEDIFF(end_time, start_time))) AS duration", FALSE)
                ->from('day_services')
                ->where('service_status', 2);
            if ($date_from && $date_to) {
                $this->db->where('service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"');
            } else if ($date_from || $date_to) {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('service_date', $date);
            } else {
                $this->db->where('service_date', $date_from);
            }

            $this->db->limit(1);
        }
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('booking_id', $deleted_bookings);
        }

        $get_total_booking_hours_qry = $this->db->get();


        $booking = $get_total_booking_hours_qry->row();

        return isset($booking->duration) ? floor($booking->duration / 3600) : 0;
    }
    function get_total_bookings($date_from = NULL, $type = 1, $date_to = NULL)
    {
        $service_week_day = date('w', strtotime($date_from));

        $deletes = $this->get_booking_deletes_by_date($date_from);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        if ($type === 1) {
            $this->db->select("booking_id", FALSE)
                ->from('bookings')
                ->where('booking_category', 'C')
                ->where('booking_status', 1)
                ->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);

        } else if ($type === 2) {
            $this->db->select("day_service_id", FALSE)
                ->from('day_services')
                ->where('service_status', 2);
            if ($date_from && $date_to) {
                $this->db->where('service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"');
            } else if ($date_from || $date_to) {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('service_date', $date);
            } else {
                $this->db->where('service_date', $date_from);
            }


        }
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('booking_id', $deleted_bookings);
        }

        $get_total_bookings_qry = $this->db->get();


        return $get_total_bookings_qry->num_rows();
    }
    function get_one_day_bookings($date_from = NULL, $type = 1, $date_to = NULL)
    {
        $service_week_day = date('w', strtotime($date_from));

        $deletes = $this->get_booking_deletes_by_date($date_from);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        if ($type === 1) {
            $this->db->select("booking_id", FALSE)
                ->from('bookings')
                ->where('booking_category', 'C')
                ->where('booking_status', 1)
                ->where('booking_type', 'OD')
                ->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);

            if (count($deleted_bookings) > 0) {
                $this->db->where_not_in('booking_id', $deleted_bookings);
            }
        } else if ($type === 2) {
            $this->db->select("d.day_service_id", FALSE)
                ->from('day_services d')
                ->join('bookings b', 'd.booking_id=b.booking_id')
                ->where('b.booking_type', 'OD')
                ->where('b.booking_status', 1)
                ->where('d.service_status', 2);
            if ($date_from && $date_to) {
                $this->db->where('d.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"');
            } else if ($date_from || $date_to) {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('d.service_date', $date);
            } else {
                $this->db->where('d.service_date', $date_from);
            }
            if (count($deleted_bookings) > 0) {
                $this->db->where_not_in('d.booking_id', $deleted_bookings);
            }


        }


        $get_total_bookings_qry = $this->db->get();


        return $get_total_bookings_qry->num_rows();
    }
    function get_week_day_bookings($date_from = NULL, $type = 1, $date_to = NULL)
    {
        $service_week_day = date('w', strtotime($date_from));

        $deletes = $this->get_booking_deletes_by_date($date_from);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        if ($type === 1) {
            $this->db->select("booking_id", FALSE)
                ->from('bookings')
                ->where('booking_category', 'C')
                ->where('booking_status', 1)
                ->where('booking_type', 'WE')
                ->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);

            if (count($deleted_bookings) > 0) {
                $this->db->where_not_in('booking_id', $deleted_bookings);
            }
        } else if ($type === 2) {
            $this->db->select("d.booking_id", FALSE)
                ->from('day_services d')
                ->join('bookings b', 'd.booking_id=b.booking_id')
                ->where('b.booking_category', 'C')
                ->where('b.booking_type', 'WE')
                ->where('b.booking_status', 1)
                ->where('d.service_status', 2);
            if ($date_from && $date_to) {
                $this->db->where('d.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"');
            } else if ($date_from || $date_to) {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('d.service_date', $date);
            } else {
                $this->db->where('d.service_date', $date_from);
            }
            if (count($deleted_bookings) > 0) {
                $this->db->where_not_in('d.booking_id', $deleted_bookings);
            }

        }


        $get_total_bookings_qry = $this->db->get();


        return $get_total_bookings_qry->num_rows();
    }

    function get_total_cancellation($date_from = NULL, $date_to = NULL)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');

        if ($date_from && $date_to) {
            $this->db->where('service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"');
        } else {
            $date = $date_from ? $date_from : $date_to;

            $this->db->where('service_date', $date);
        }
        $get_booking_deletes_by_date_qry = $this->db->get();


        return $get_booking_deletes_by_date_qry->num_rows();
    }
    function get_total_invoice_amount($date_from = NULL, $date_to = NULL)
    {
        $service_week_day = date('w', strtotime($date_from));

        $deletes = $this->get_booking_deletes_by_date($date_from);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("i.day_service_id", FALSE)
            ->from('day_services d')
            ->join('invoice i', 'd.day_service_id=i.day_service_id')
            ->where('d.service_status', 2);
        if ($date_from && $date_to) {
            $this->db->where('d.service_date BETWEEN "' . $date_from . '" and "' . $date_to . '"');
        } else if ($date_from || $date_to) {
            $date = $date_from ? $date_from : $date_to;
            $this->db->where('d.service_date', $date);
        } else {
            $this->db->where('d.service_date', $date_from);
        }
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('d.booking_id', $deleted_bookings);
        }

        $get_total_invoice_amount_qry = $this->db->get();


        return $get_total_invoice_amount_qry->num_rows();
    }
    function get_day_collection($date_from = NULL, $date_to = NULL)
    {
        $this->db->select('SUM(paid_amount) AS day_collection', FALSE)
            ->from('customer_payments');

        if ($date_from && $date_to) {
            $this->db->where('DATE(paid_datetime) BETWEEN "' . $date_from . '" and "' . $date_to . '"');
        } else {
            $date = $date_from ? $date_from : $date_to;

            $this->db->where('DATE(paid_datetime)', $date);
        }

        $this->db->limit(1);

        $get_day_collection_qry = $this->db->get();


        $payments = $get_day_collection_qry->row();

        return isset($payments->day_collection) ? $payments->day_collection : 0;
    }
    function get_new_customer($date_from = NULL, $date_to = NULL)
    {
        $this->db->select('customer_id')
            ->from('customers');

        if ($date_from && $date_to) {
            $this->db->where('DATE(customer_added_datetime) BETWEEN "' . $date_from . '" and "' . $date_to . '"');
        } else {
            $date = $date_from ? $date_from : $date_to;

            $this->db->where('DATE(customer_added_datetime)', $date);
        }

        $get_new_customer_qry = $this->db->get();


        return $get_new_customer_qry->num_rows();

    }
    function get_total_pending_amount($date_from = NULL, $date_to = NULL)
    {
        $service_week_day = date('w', strtotime($date_from));

        $deletes = $this->get_booking_deletes_by_date($date_from);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("SUM(d.total_fee) AS pending_amount", FALSE)
            ->from('day_services d')
            ->join('invoice i', 'd.day_service_id=i.day_service_id')
            ->where('d.service_status', 2)
            ->where('i.invoice_status', 0);
        if ($date_from && $date_to) {
            $this->db->where('DATE(i.added) BETWEEN "' . $date_from . '" and "' . $date_to . '"');
        } else if ($date_from || $date_to) {
            $date = $date_from ? $date_from : $date_to;
            $this->db->where('DATE(i.added)', $date);
        } else {
            $this->db->where('DATE(i.added)', $date_from);
        }
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('d.booking_id', $deleted_bookings);
        }

        $this->db->limit(1);

        $get_total_pending_amount_qry = $this->db->get();


        $payments = $get_total_pending_amount_qry->row();

        return isset($payments->pending_amount) ? $payments->pending_amount : 0;
    }
    /** 
     * Get maids by booking id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_not_free_maids_by_booking_id($booking_id, $service_week_day, $time_from, $time_to, $service_date)
    {
        $deletes = $this->get_booking_deletes_by_date(date('Y-m-d', strtotime($service_date)));

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $time_from = strtotime($time_from);
        $time_to = strtotime($time_to);

        $this->db->select("b.maid_id, b.time_from, b.time_to, b.service_start_date, b.service_end_date", FALSE)
            ->from('bookings b')
            ->where('b.service_week_day', $service_week_day)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)



            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_not_free_maids_by_booking_id_qry = $this->db->get();
        //echo $this->db->last_query();exit; 
        return $get_not_free_maids_by_booking_id_qry->result();
    }

    function get_not_free_maids_by_booking_id_new($booking_id, $service_week_day, $time_from, $time_to, $service_date)
    {
        $deletes = $this->get_booking_deletes_by_date(date('Y-m-d', strtotime($service_date)));

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $time_from = strtotime($time_from);
        $time_to = strtotime($time_to);

        $this->db->select("b.maid_id, b.time_from, b.time_to, b.service_start_date, b.service_end_date", FALSE)
            ->from('bookings b')
            ->where('b.service_week_day', $service_week_day)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE' AND ((" . $time_from . " > b.time_from AND " . $time_from . " < b.time_to) OR (" . $time_to . " > b.time_from AND " . $time_to . " < b.time_to) OR (" . $time_from . " <= b.time_from AND " . $time_to . " >= b.time_to))) OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW' AND ((" . $time_from . " > b.time_from AND " . $time_from . " < b.time_to) OR (" . $time_to . " > b.time_from AND " . $time_to . " < b.time_to) OR (" . $time_from . " <= b.time_from AND " . $time_to . " >= b.time_to))))", NULL, FALSE)
            //->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD' OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE' ))", NULL, FALSE);


            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_not_free_maids_by_booking_id_qry = $this->db->get();
        //echo $this->db->last_query();exit; 
        return $get_not_free_maids_by_booking_id_qry->result();
    }

    //for pause booking starts (2)
    function get_schedule_by_customer($customer_id, $service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id,DAYNAME(b.service_start_date) AS shift_day, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.maid_id', $maid_id)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();

        return $get_schedule_by_date_qry->result();
    }
    // for pause booking ends

    //get booking by customerid and date
    function get_schedule_by_date_and_customer($customer_id, $service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select("b.booking_id, b.customer_id, DAYNAME(b.service_start_date) AS shift_day, b.customer_address_id, b.maid_id, b.service_type_id, DATE_FORMAT(b.service_start_date, '%d/%m/%Y') as service_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,'$service_date' as scheduledates", FALSE)
            //$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)                 
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->where('c.customer_id', $customer_id)
                //->order_by('m.maid_name')
                //->order_by('b.time_from');
            ->order_by('b.service_start_date', 'ASC');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }
    function get_schedule_by_date_and_customer_new($customer_id, $service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        //$deletes = $this->get_booking_deletes_by_date($service_date);

        //$deleted_bookings = array();
        //foreach($deletes as $delete)
        //{
        //        $deleted_bookings[] = $delete->booking_id;
        //}
        $this->db->select("b.booking_id, b.customer_id, DAYNAME(b.service_start_date) AS shift_day, b.customer_address_id, b.maid_id, b.service_type_id, DATE_FORMAT(b.service_start_date, '%d/%m/%Y') as service_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,'$service_date' as scheduledates", FALSE)
            //$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)                 
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where("(b.booking_type='WE' OR b.booking_type='BW' OR b.booking_type='OD')", NULL, FALSE)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->where('c.customer_id', $customer_id)
                //->order_by('m.maid_name')
                //->order_by('b.time_from');
            ->order_by('b.service_start_date', 'ASC');

        //if(count($deleted_bookings) > 0)
        //{
        //	$this->db->where_not_in('b.booking_id', $deleted_bookings);
        //}

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function checkbooking_in_delete_weekly($bookingid, $date)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        $this->db->where("service_date", $date);
        $this->db->where("booking_id", $bookingid);

        $get_booking_deletes_by_date_qry = $this->db->get();

        //return $get_booking_deletes_by_date_qry->result();
        return $get_booking_deletes_by_date_qry->num_rows();
    }
    function checkbooking_in_delete_biweekly($bookingid, $date)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        $this->db->where("service_date", $date);
        $this->db->where("booking_id", $bookingid);

        $get_booking_deletes_by_date_qry = $this->db->get();

        //return $get_booking_deletes_by_date_qry->result();
        return $get_booking_deletes_by_date_qry->num_rows();
    }
    function checkbooking_in_delete_oneday($bookingid, $date)
    {
        $this->db->select('booking_id')
        ->from('booking_deletes');
        $this->db->where("service_date", $date);
        $this->db->where("booking_id", $bookingid);
        // $this->db->select('booking_id')
        //     ->from('bookings');
        // $this->db->where('booking_category', 'C');
        // $this->db->where("service_start_date", $date);
        // $this->db->where("booking_id", $bookingid);
        // $this->db->where("booking_status", 2);

        $get_booking_deletes_by_date_qry = $this->db->get();

        //return $get_booking_deletes_by_date_qry->result();
        return $get_booking_deletes_by_date_qry->num_rows();
    }
    function get_all_booking_approval_list($filter = 'Pending')
{

    $service_date = date('Y-m-d');
    //                $status = $filter == 'Assigned' ? 1 : 0;

    switch ($filter):
case 'Pending':
    $status = '0';
    break;
case 'Assigned':
    $status = '1';
    break;
case 'Deleted':
    $status = '2';
    break;
case 'Expired':
    $status = '0';
    break;
case 'All':
    $status = '';
    break;

    endswitch;

    $this->db->select('b.booking_id, b.pay_by,b.crew_in, b.booking_note, b.booking_type, b.delete_remarks, b.booked_from,b.booking_status,CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), "-" , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, c.customer_name, c.payment_type, ca.customer_address, a.area_name,z.zone_name,c.customer_id, DAYNAME(b.service_start_date) AS weekday, DATE_FORMAT(b.service_start_date, "%d/%m/%Y") AS service_date, m.maid_name, DATE_FORMAT(b.booked_datetime, "%d/%m/%Y %H:%i") AS booked_datetime, no_of_maids,cleaning_material,c.phone_number, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.email_address,ca.building,ca.unit_no,ca.street,st.service_type_name,CONCAT(if(b.interior_window_clean,"Inetrior Window Cleaning",""),if(b.fridge_cleaning,if(b.interior_window_clean,", Fridge Cleaning","Fridge Cleaning"),""),if(b.ironing_services,if(b.interior_window_clean OR b.fridge_cleaning,", Ironing Services","Ironing Services"),""),if(oven_cleaning,if(b.interior_window_clean OR b.fridge_cleaning OR b.ironing_services,", Oven Cleaning","Oven Cleaning"),"")) as exserv,b.total_amount', false)
        ->from('bookings b')
        ->join('customers c', 'b.customer_id = c.customer_id')
        ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
        ->join('areas a', 'ca.area_id = a.area_id')
        ->join('zones z', 'a.zone_id = z.zone_id')
        ->join('maids m', 'b.maid_id = m.maid_id', 'left')
        ->join('service_types st', 'b.service_type_id = st.service_type_id', 'left')
        ->where('b.booking_category', 'C')
        ->where_in('b.booked_from', array('W', 'M'))

        ->order_by('b.booked_datetime', 'desc');
    if ((isset($status) != '')) {
            $this->db->where('b.booking_status', $status);
    }

    if ($filter == 'Pending') {
        $this->db->where("(b.service_start_date >= " . $this->db->escape($service_date) . ")", null, false);

    } else if ($filter == 'Expired') {
        $this->db->where("(b.service_start_date < " . $this->db->escape($service_date) . ")", null, false);
    }

    $get_all_booking_approval_list_qry = $this->db->get();

    return $get_all_booking_approval_list_qry->result();
}

function get_all_booking_approval_list_by_date($filter = 'Pending',$filter_date)
{

    $service_date = $filter_date ?: date('Y-m-d');
    //                $status = $filter == 'Assigned' ? 1 : 0;

    switch ($filter):
case 'Pending':
    $status = '0';
    break;
case 'Assigned':
    $status = '1';
    break;
case 'Deleted':
    $status = '2';
    break;
case 'Expired':
    $status = '0';
    break;
case 'All':
    $status = '';
    break;

    endswitch;

    $this->db->select('b.booking_id, b.pay_by,b.crew_in, b.booking_note, b.booking_type, b.delete_remarks, b.booked_from,b.booking_status,CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), "-" , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, c.customer_name, c.payment_type, ca.customer_address, a.area_name,z.zone_name,c.customer_id, DAYNAME(b.service_start_date) AS weekday, DATE_FORMAT(b.service_start_date, "%d/%m/%Y") AS service_date, m.maid_name, DATE_FORMAT(b.booked_datetime, "%d/%m/%Y %H:%i") AS booked_datetime, no_of_maids,cleaning_material,c.phone_number, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.email_address,ca.building,ca.unit_no,ca.street,st.service_type_name,CONCAT(if(b.interior_window_clean,"Inetrior Window Cleaning",""),if(b.fridge_cleaning,if(b.interior_window_clean,", Fridge Cleaning","Fridge Cleaning"),""),if(b.ironing_services,if(b.interior_window_clean OR b.fridge_cleaning,", Ironing Services","Ironing Services"),""),if(oven_cleaning,if(b.interior_window_clean OR b.fridge_cleaning OR b.ironing_services,", Oven Cleaning","Oven Cleaning"),"")) as exserv', false)
        ->from('bookings b')
        ->join('customers c', 'b.customer_id = c.customer_id')
        ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
        ->join('areas a', 'ca.area_id = a.area_id')
        ->join('zones z', 'a.zone_id = z.zone_id')
        ->join('maids m', 'b.maid_id = m.maid_id', 'left')
        ->join('service_types st', 'b.service_type_id = st.service_type_id', 'left')
        ->where('b.booking_category', 'C')
        ->where_in('b.booked_from', array('W', 'M'))
        ->order_by('b.booked_datetime', 'desc');
    if ((isset($status) != '')) {
            $this->db->where('b.booking_status', $status);
    }
    if ($filter == 'Pending') {
        $this->db->where("(b.service_start_date >= " . $this->db->escape($service_date) . ")", null, false);

    } else if ($filter == 'Expired') {
        $this->db->where("(b.service_start_date < " . $this->db->escape($service_date) . ")", null, false);
    }
    $get_all_booking_approval_list_qry = $this->db->get();
    return $get_all_booking_approval_list_qry->result();
}

    function get_all_booking_approval_list_job($filter = 'Pending', $date)
    {

        $service_date = $date;
        //                $status = $filter == 'Assigned' ? 1 : 0;

        switch ($filter):
            case 'Pending':
                $status = '0';
                break;
            case 'Assigned':
                $status = '1';
                break;
            case 'Deleted':
                $status = '2';
                break;
            case 'Expired':
                $status = '0';
                break;
            case 'All':
                $status = '';
                break;

        endswitch;

        $this->db->select('b.booking_id, b.booking_type, b.delete_remarks, b.booked_from,b.booking_status,CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), "-" , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, c.customer_name, c.payment_type, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name,z.zone_name,c.customer_id, DAYNAME(b.service_start_date) AS weekday, DATE_FORMAT(b.service_start_date, "%d/%m/%Y") AS service_date, m.maid_name, DATE_FORMAT(b.booked_datetime, "%d/%m/%Y %H:%i") AS booked_datetime, no_of_maids,cleaning_material,c.phone_number, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.email_address,b.total_amount', FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('maids m', 'b.maid_id = m.maid_id', 'left')
            ->where('b.booking_category', 'C')
            ->where_in('b.booked_from', array('W', 'M'))

            ->order_by('b.booked_datetime', 'desc');
        if ((isset($status) != '')) {
            $this->db->where('b.booking_status', $status);
        }



        if ($filter == 'Pending') {
            $this->db->where("(b.service_start_date >= " . $this->db->escape($service_date) . ")", NULL, FALSE);

        } else if ($filter == 'Expired') {
            $this->db->where("(b.service_start_date < " . $this->db->escape($service_date) . ")", NULL, FALSE);
        }


        $get_all_booking_approval_list_qry = $this->db->get();

        return $get_all_booking_approval_list_qry->result();
    }
    function reject_booking($booking_id, $fields = array())
    {
        $this->db->set('booking_status', 2);
        $this->db->where_in('booking_id', $booking_id);

        $this->db->update('bookings');
        $affected = $this->db->affected_rows();

        $this->add_activity($booking_id, 'Reject Booking from website');

        return $affected;
    }

    function get_search_maid_booking_old($service_date)
    {


        $service_week_day = date('w', strtotime($service_date));
        $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file')
            ->from('maids m')

            ->order_by('maid_name'); //maid_added_datetime
        $this->db->where('maid_status', 1);

        $get_schedule_by_date_qry = $this->db->get();
        //$booking['maid']=$get_schedule_by_date_qry->result();
//echo $this->db->last_query();exit;
        $testBok = $get_schedule_by_date_qry->result();
        $bookVal = array();
        $i = 1;
        foreach ($testBok as $maids) {

            $bookVal[$i]['maid'] = $maids;
            //	echo '<pre>';
// echo "test".$maids->maid_id;
// exit();
            $this->db->select('b.time_from, b.time_to,b.service_start_date,b.service_actual_end_date,m.maid_id, m.maid_name,m.maid_photo_file')
                ->from('bookings b')
                ->from('maids m')
                ->where('b.booking_status', 1)
                ->where('b.booking_category', 'C')
                ->where('b.maid_id', $maids->maid_id)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . "  ))", NULL, FALSE);
            $get_schedule_maids_query = $this->db->get();
            //echo $this->db->last_query();exit;
            $times = $get_schedule_maids_query->result();
            foreach ($times as $time) {
                // $bookVal[$i]['time_from']=$time;
//                           print_r($time);
            }
            $i++;


            // print_r($get_schedule_maids_query->result())	;	
        }
        return $times;
        //	return $bookVal;

    }



    //sandeep
    function get_search_maid_booking($service_date)
    {


        $service_week_day = date('w', strtotime($service_date));

        $this->db->select("m.*,b.service_start_date,b.service_week_day,b.time_from,b.time_to,GROUP_CONCAT(b.booking_type ORDER BY b.time_from SEPARATOR ',') as booking_type,b.service_end,GROUP_CONCAT(CONCAT(DATE_FORMAT( b.time_from,  '%h:%i %p' ),'-',DATE_FORMAT( b.time_to,  '%h:%i %p' )) ORDER BY b.time_from SEPARATOR ',') as shifts", FALSE)
            ->from('maids m')
            ->join('bookings b', 'b.maid_id = m.maid_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)

            ->where('m.maid_status', 1)
            //->where("((b.booking_status=1) OR (b.maid_id IS NULL))")
            ->where("((b.service_start_date= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_week_day = " . $service_week_day . " AND b.booking_type='WE' and b.service_end=0))", NULL, FALSE)
            ->group_by('m.maid_id')
            ->order_by('m.maid_name', 'asc');
        $get_schedule_maids_query = $this->db->get();
        //echo $this->db->last_query();exit;
        $times = $get_schedule_maids_query->result();



        return $times;
        //	return $bookVal;
    }
    /* function get_shift_maid($service_date,$maid_id)
    {
    $service_week_day = date('w', strtotime($service_date));
    
    $this->db->select("b.service_start_date,b.service_week_day,b.time_from,b.time_to,GROUP_CONCAT(b.booking_type ORDER BY b.time_from SEPARATOR ',') as booking_type,b.service_end,GROUP_CONCAT(CONCAT(DATE_FORMAT( b.time_from,  '%h:%i %p' ),'-',DATE_FORMAT( b.time_to,  '%h:%i %p' )) ORDER BY b.time_from SEPARATOR ',') as shifts",FALSE)
    ->from('bookings b')
    
    ->where('b.booking_status', 1)
    ->where('b.maid_id', $maid_id)
    
    ->where("((b.service_start_date= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_week_day = " . $service_week_day . " AND b.booking_type='WE' and b.service_end=0))", NULL, FALSE)
    ->group_by('b.maid_id') ; 
    
    $get_schedule_maids_query = $this->db->get();
    //echo $this->db->last_query();exit;
    $times = $get_schedule_maids_query->result();
    
    return $times;
    //	return $bookVal;
    } */

    function get_free_maid($service_date, $from_time, $to_time, $maid_id)
    {

        $service_week_day = date('w', strtotime($service_date));
        $time_from = date('H:i:s', trim($from_time));
        $time_to = date('H:i:s', trim($to_time));

        $this->db->select("b.maid_id, b.service_start_date, b.booking_type,b.service_week_day,b.time_from,b.time_to,ca.area_id", FALSE)
            ->from('bookings b');
        $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');

        $this->db->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.maid_id', $maid_id)
            ->where("((b.service_start_date= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('" . $time_to . "' > b.time_from and '" . $time_from . "' < b.time_to)) or (b.service_week_day = " . $service_week_day . " AND (b.booking_type='WE' OR b.booking_type='BW') and b.service_end=0 AND ('" . $time_to . "' > b.time_from and '" . $time_from . "' < b.time_to )) or (b.service_week_day = " . $service_week_day . " AND (b.booking_type='WE' OR b.booking_type='BW') and b.service_end=1 AND ('" . $time_to . "' > b.time_from and '" . $time_from . "' < b.time_to ) AND service_actual_end_date>=" . $this->db->escape($service_date) . "))", NULL, FALSE);
        $get_schedule_maids_query = $this->db->get();
        // echo $this->db->last_query();exit;
        //$times = $get_schedule_maids_query->result();
        $num = $get_schedule_maids_query->num_rows();

        return $num;

        //	return $bookVal;
    }
    function get_free_maid_bw($service_date, $from_time, $to_time, $maid_id)
    {

        $service_week_day = date('w', strtotime($service_date));
        $time_from = date('H:i:s', trim($from_time));
        $time_to = date('H:i:s', trim($to_time));

        $this->db->select("b.maid_id, b.service_start_date, b.booking_type,b.service_week_day,b.time_from,b.time_to,ca.area_id", FALSE)
            ->from('bookings b');
        $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');

        $this->db->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.maid_id', $maid_id)
                //->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND (('" . $time_from . "' > b.time_from AND '" . $time_from . "' < b.time_to) OR ('" . $time_to . "' > b.time_from AND '" . $time_to . "' < b.time_to) OR ('" . $time_from . "' <= b.time_from AND '" . $time_to . "' >= b.time_to))) OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE' AND (('" . $time_from . "' > b.time_from AND '" . $time_from . "' < b.time_to) OR ('" . $time_to . "' > b.time_from AND '" . $time_to . "' < b.time_to) OR ('" . $time_from . "' <= b.time_from AND '" . $time_to . "' >= b.time_to))))", NULL, FALSE);
        $get_schedule_maids_query = $this->db->get();
        // echo $this->db->last_query();exit;
        //$times = $get_schedule_maids_query->result();
        $num = $get_schedule_maids_query->num_rows();

        return $num;

        //	return $bookVal;
    }

    function get_free($service_date, $from_time, $to_time, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));
        $time_from = date('H:i:s', trim($from_time));
        $time_to = date('H:i:s', trim($to_time));
        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select("b.maid_id, b.service_start_date, b.booking_type,b.service_week_day,b.time_from,b.time_to,ca.area_id", FALSE)
            ->from('bookings b');
        $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT'); //if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
        $this->db->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.maid_id', $maid_id)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND (('" . $time_from . "' > b.time_from AND '" . $time_from . "' < b.time_to) OR ('" . $time_to . "' > b.time_from AND '" . $time_to . "' < b.time_to) OR ('" . $time_from . "' <= b.time_from AND '" . $time_to . "' >= b.time_to))) OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE' AND (('" . $time_from . "' > b.time_from AND '" . $time_from . "' < b.time_to) OR ('" . $time_to . "' > b.time_from AND '" . $time_to . "' < b.time_to) OR ('" . $time_from . "' <= b.time_from AND '" . $time_to . "' >= b.time_to))) OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
        $get_schedule_maids_query = $this->db->get();
        $num = $get_schedule_maids_query->num_rows();

        return $num;
        //echo $this->db->last_query();exit;

    }

    function get_shifts_maid($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }


        $this->db->select("b.service_start_date,b.service_week_day,b.time_from,b.time_to,
             GROUP_CONCAT(ca.area_id ORDER BY b.time_from SEPARATOR ',') as area_id,
             GROUP_CONCAT(a.area_name ORDER BY b.time_from SEPARATOR ',') as area_name,
             GROUP_CONCAT(z.zone_id ORDER BY b.time_from SEPARATOR ',') as zone_id,
             GROUP_CONCAT(p.province_id ORDER BY b.time_from SEPARATOR ',') as province_id,
             GROUP_CONCAT(b.booking_type ORDER BY b.time_from SEPARATOR ',') as booking_type,
             b.service_end,
             GROUP_CONCAT(CONCAT(DATE_FORMAT( b.time_from,  '%h:%i %p' ),'-',DATE_FORMAT( b.time_to,  '%h:%i %p' )) ORDER BY b.time_from SEPARATOR ',') as shifts", FALSE)
            ->from('bookings b');
        $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');

        $this->db->join('areas as a', 'a.area_id = ca.area_id ', 'LEFT');
        $this->db->join('zones as z', 'z.zone_id = a.zone_id', 'LEFT');
        $this->db->join('province as p', 'p.province_id = z.province_id', 'LEFT');

        $this->db->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.maid_id', $maid_id)

            ->where("((b.service_start_date= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_week_day = " . $service_week_day . " AND b.booking_type='WE' and b.service_end=0))", NULL, FALSE)
            ->group_by('b.maid_id');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
        // $get_schedule_maids_query = $this->db->get();
        // // echo $this->db->last_query();//exit;
        // $times = $get_schedule_maids_query->result();

        // return $times;
        $get_schedule_maids_query = $this->db->get();

        if (!$get_schedule_maids_query) {
            // Handle the error
            $error_message = mysqli_error($this->db->conn_id);
            log_message('error', 'Database query error: ' . $error_message);
            return false; // Or handle the error in another appropriate way
        } else {
            return $get_schedule_maids_query->result();
        }

    }

    function get_shifts_maid_new($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.service_start_date,b.service_week_day,b.time_from,b.time_to,b.maid_id,m.maid_name,
             GROUP_CONCAT(ca.area_id ORDER BY b.time_from SEPARATOR ',') as area_id,
             GROUP_CONCAT(a.area_name ORDER BY b.time_from SEPARATOR ',') as area_name,
             GROUP_CONCAT(z.zone_id ORDER BY b.time_from SEPARATOR ',') as zone_id,
             GROUP_CONCAT(p.province_id ORDER BY b.time_from SEPARATOR ',') as province_id,
             GROUP_CONCAT(b.booking_type ORDER BY b.time_from SEPARATOR ',') as booking_type,
             b.service_end,
             GROUP_CONCAT(CONCAT(DATE_FORMAT( b.time_from,  '%h:%i %p' ),'-',DATE_FORMAT( b.time_to,  '%h:%i %p' )) ORDER BY b.time_from SEPARATOR ',') as shifts", FALSE)
            ->from('bookings b');
        $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');

        $this->db->join('areas as a', 'a.area_id = ca.area_id ', 'LEFT');
        $this->db->join('zones as z', 'z.zone_id = a.zone_id', 'LEFT');
        $this->db->join('maids m', 'b.maid_id = m.maid_id');
        $this->db->join('province as p', 'p.province_id = z.province_id', 'LEFT');
        $this->db->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.maid_id', $maid_id)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->group_by('b.maid_id');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
        // $get_schedule_maids_query = $this->db->get();
        // // echo $this->db->last_query();//exit;
        // $times = $get_schedule_maids_query->result();

        // return $times;
        $get_schedule_maids_query = $this->db->get();

        if (!$get_schedule_maids_query) {
            // Handle the error
            $error_message = mysqli_error($this->db->conn_id);
            log_message('error', 'Database query error: ' . $error_message);
            return false; // Or handle the error in another appropriate way
        } else {
            return $get_schedule_maids_query->result();
        }

    }

    function check_day_service_booking($booking_id)
    {
        $service_date = date('Y-m-d');

        $this->db->select("day_service_id,booking_id", FALSE)
            ->from('day_services');
        $this->db->where('booking_id', $booking_id);
        $this->db->where('service_date', $service_date);
        $get_results_query = $this->db->get();
        $rowcount = $get_results_query->num_rows();
        if ($rowcount != 0) {

            return $get_results_query->result();
        } else {
            return false;
        }
    }

    function update_day_service_booking($day_service_id, $fields)
    {
        $this->db->where('day_service_id', $day_service_id);
        $this->db->update('day_services', $fields);
        // echo $this->db->last_query(); 
    }





    /*
     * Author : Jiby
     * Purpose : Added for driver app on 27/09/17
     */
    function get_schedule_by_date_for_tab($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        //		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.vaccum_cleaning, b.cleaning_materials, b.sofa_cleaning, b.cleaning_chemicals, b.discount, b.booking_status, b.book_by_franchise, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, c.longitude, c.latitude, c.price_hourly, c.price_extra, c.price_weekend, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname", FALSE)
        $this->db->select("b.booking_id,b.total_amount, b.cleaning_material, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.tabletid, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1,c.mobile_number_2, c.key_given, c.payment_type, c.longitude, c.payment_mode, c.latitude, c.price_hourly, c.price_extra, c.price_weekend, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, ca.building, ca.unit_no, ca.street, ca.latitude, ca.longitude, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,m.driver_name as driver", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id')
            ->where('m.maid_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //	echo $this->db->last_query();	
        return $get_schedule_by_date_qry->result();
    }
    function get_schedule_by_maid($maid_id, $zone_id)
    {
        $service_date = date('Y-m-d');
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.vaccum_cleaning, b.cleaning_material, b.cleaning_chemicals, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('m.maid_id', $maid_id)
            //->where('z.zone_id', $zone_id)
            ->where('m.maid_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from')
            ->limit(1);

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //	echo $this->db->last_query();	
        return $get_schedule_by_date_qry->row();
    }

    function get_schedule_by_date_new_count($service_date, $maid_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));
        $deletes = $this->get_booking_deletes_by_date($service_date);
        $deleted_bookings = array_column($deletes, 'booking_id');
        $this->db->select("count(b.booking_id) as total")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id')
            ->join('maids as m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas as a', 'ca.area_id = a.area_id')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            //->join('users as u','b.booked_by = u.user_id')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('m.maid_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');

        if ($maid_id != NULL) {
            $this->db->where('m.maid_id', $maid_id);
        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        
        return $get_schedule_by_date_qry->row_array()['total'];
    }

    function get_schedule_by_availabilty_time($service_date, $maid_id = NULL, $filter_by = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        if ($filter_by == 2) {
            $time_from = "08:00";
            $time_to = "13:00";

        } else if ($filter_by == 3) {
            $time_from = "14:00";
            $time_to = "18:00";
        }
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("Floor(SUM(TIME_TO_SEC(timediff(b.time_to,b.time_from)))/60) AS totalhours", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.ara_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            //->join('users u','b.booked_by = u.user_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('m.maid_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');
        if ($filter_by) {
            $this->db->where("((DATE_FORMAT(b.time_from, '%H:%i') > '" . $time_from . "') AND (DATE_FORMAT(b.time_from, '%H:%i')<'" . $time_to . "'))");
            //$this->db->where('DATE_FORMAT(b.time_from, "%H:%i") BETWEEN "'. $time_from. '" and "'. $time_to.'"');
        }
        if ($maid_id != NULL) {
            $this->db->where('m.maid_id', $maid_id);
        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();

        $query = $get_schedule_by_date_qry->result();
        return $query;
    }

    function get_customer_total_bookings($service_date, $customer_id, $time_from, $time_to)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.price_hourly, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, c.customer_booktype,u.user_fullname,b.booked_by", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('m.maid_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('c.customer_id', $customer_id)
            ->where('b.time_from', $time_from)
            ->where('b.time_to', $time_to)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();

        $query = $get_schedule_by_date_qry->result();
        return $query;
    }



    /*
     * Author : Jiby
     * Purpose : Get total booking hours of the days
     */

    function get_schedule_hrs_by_date($service_date, $team_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id,  DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('m.team_id')
            ->order_by('b.time_from');
        if ($team_id != NULL) {
            $this->db->where('m.team_id', $team_id);
        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_complaints_count($from_date = null)
    {

        if (empty($from_date)) {
            $today = date('Y-m-d');
        }


        $this->db->select("count(cmp_id) as total")
            ->from('complaints');
        if (!empty($today)) {

            $this->db->like('created_date ', $today);
        } else {
            $this->db->where('created_date >=', @$from_date);
        }
        $get_results_query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_results_query->row_array()['total'];
    }

    function get_transit_jobs_count()
    {

        $this->db->select("day_service_id")
            ->from('day_services');
        $this->db->where('service_status =', 1);
        $this->db->like('service_added_datetime ', date('Y-m-d'));

        $get_results_query = $this->db->get();
        return $get_results_query->num_rows();
    }
    function get_invoice_count($type = null, $from_date = null)
    {

        $this->db->select("invoice_id")
            ->from('invoice');
        $this->db->where('invoice_status =', $type);
        if (empty($from_date)) {
            //    if empty date consider for today
            $this->db->like('invoice_date ', date('Y-m-d'));
        } else {
            // if date is provided consider as weekly or monthly
            $this->db->where('invoice_date >= ', $from_date);
        }

        $get_results_query = $this->db->get();
        //        echo $this->db->last_query();
        return $get_results_query->num_rows();
    }

    function job_start_or_finish($booking_id, $schedule_date)
    {
        $service_date = $schedule_date;
        $this->db->select("*")
            ->from('day_services')
            ->where('booking_id', $booking_id)
            ->where('service_date', $service_date);


        $get_results_query = $this->db->get();
        return $result = $get_results_query->result();

    }

    function get_today_job_details($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("DATE_FORMAT(b.time_from, '%H:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%H:%i %p') AS time_to, b.booking_type, c.customer_name, ca.longitude, ca.latitude, m.maid_name, m.maid_photo_file", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('m.team_id')
            ->order_by('b.time_from');
        //        if($team_id != NULL)
//        {
//                $this->db->where('m.team_id', $team_id);
//        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result_array();
    }
    /*
     * Author : Vishnu
     * Purpose : Get Complaint by booking id
     */
    function getjobcomplaintbyid($booking_id, $date)
    {
        $this->db->select("*")
            ->from('complaints')
            ->where('booking_id', $booking_id)
            ->where('service_date', $date);


        $get_results_query = $this->db->get();
        return $result = $get_results_query->row();
    }

    function add_complaints($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('complaints');

        return $complaint_id = $this->db->insert_id();
    }

    function get_complaint_by_cid($complaint_id)
    {
        $this->db->select("c.*,u.user_fullname")
            ->from('complaints c')
            ->join('users u', 'c.added_by = u.user_id', 'left')
            ->where('c.cmp_id', $complaint_id);
        $get_results_query = $this->db->get();
        return $result = $get_results_query->row();
    }

    function update_complaints($fields = array(), $complaint_id)
    {
        $this->db->where('cmp_id', $complaint_id);
        $this->db->update('complaints', $fields);
        $affected = $this->db->affected_rows();
        return $affected;
    }

    /** 
     * Get schedule by date
     * 
     * @author	Azinova Developers
     * @acces	public 
     * @param	str
     * @return	array
     */
    function get_total_booking_by_customer($service_date, $customer_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->order_by('m.team_id')
            ->order_by('b.time_from');
        if ($customer_id != NULL) {
            $this->db->where('c.customer_id', $customer_id);
        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_maid_performance($maid_id, $service_date, $zone_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);
        $deleted_bookings = array_column($deletes, 'booking_id');
        $s_date = date("d/m/Y", strtotime($service_date));
        $this->db->select("GROUP_CONCAT(c.customer_name) as customer_names, GROUP_CONCAT(c.customer_source) as sources, GROUP_CONCAT(DATE_FORMAT(b.time_from, '%h:%i')) as time_from, GROUP_CONCAT(DATE_FORMAT(b.time_to, '%h:%i')) as time_to, GROUP_CONCAT(z.driver_name) as drivers, GROUP_CONCAT(b.total_amount) as totamt,GROUP_CONCAT(c.mobile_number_1) as mobile_number,GROUP_CONCAT(b.booking_type) as booking_type,GROUP_CONCAT(ca.customer_address) as customer_address, GROUP_CONCAT(b.cleaning_material) as cleaning_materials, m.maid_name,GROUP_CONCAT(m.maid_name) as maid_names,'$s_date' as service_start_date,m.maid_status,m.maid_last_modified_datetime,m.maid_disabled_datetime", FALSE)


            //$this->db->select("b.booking_id, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('m.maid_id', $maid_id)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->group_by('m.maid_name')
            ->order_by('m.maid_name');
        if ($zone_id != NULL) {
            $this->db->where('z.zone_id', $zone_id);
        }

        if ($service_date >= date('Y-m-d')) {
            $this->db->where('m.maid_status', 1);
        }

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    public function get_zone_name_by_id($zone_id)
    {
        $this->db->select('zone_name')
            ->from('zones')
            ->where('zone_id', $zone_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_available_cleaning_times()
    {
        $this->db->select("*")
            ->from('booking_slots')
            ->where('status', 1);

        $get_area_qry = $this->db->get();

        return $get_area_qry->result();

    }

    function add_booking_times($fields = array())
    {
        //		$this->db->insert('time_availabilities', $fields); 
        $this->db->insert('booking_slots', $fields);

        return $this->db->insert_id();
    }

    function delete_booking_times($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('booking_slots');
    }

    function get_booking_exist($booking_id, $service_date)
    {
        $this->db->select("just_mop_ref,mop", FALSE)
            ->from('justmop_ref')
            ->where('booking_id', $booking_id)
            ->where('service_date', $service_date);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->row();
    }

    function get_today_schedule_by_date($service_date, $current_time)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.justmop_reference, b.customer_id, b.tabletid, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, b.time_from as starttime, b.time_to as endtime, DATE_FORMAT(b.time_from, '%h:%i:%s %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i:%s %p') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.payment_mode, c.longitude, c.latitude, c.customer_source,c.company_name, z.zone_id, z.zone_name, z.driver_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, ca.building, ca.unit_no, ca.street , ca.latitude as lat, ca.longitude as longt", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where('b.time_from <', $current_time)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_previous_schedule_by_date($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.tabletid, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, b.time_from as starttime, b.time_to as endtime, DATE_FORMAT(b.time_from, '%h:%i:%s %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i:%s %p') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.payment_mode, c.longitude, c.latitude, c.customer_source,c.company_name, z.zone_id, z.zone_name,z.driver_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, ca.building, ca.unit_no, ca.street, ca.latitude as lat, ca.longitude as longt", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            //->where('b.time_from <', $current_time)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    public function get_day_schedule_by_date($today_date, $current_time, $booking_id)
    {
        $this->db->select('*')
            ->from('day_services ds')
            ->join('bookings b', 'ds.booking_id = b.booking_id')

            ->where('ds.booking_id', $booking_id)
            ->where('ds.service_date', $today_date);
        //->where('ds.start_time <',$current_time);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->row();
    }
    public function get_delayed_booking_by_date($today_date)
    {
        $this->db->select('*')
            ->from('day_services ds')
            ->join('bookings b', 'ds.booking_id = b.booking_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
                //->where('ds.booking_id',$booking_id)
            ->where('ds.service_date', $today_date);
        //->where('ds.start_time <',$current_time);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->result();
    }

    public function check_booking_transferred($booking_id, $date)
    {
        $this->db->select('*')
            ->from('booking_transfers bt')
            ->where('bt.booking_id', $booking_id)
            ->where('bt.service_date', $date)
            ->order_by('bt.booking_transfer_id', 'DESC')
            ->limit(1);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->row();

    }

    public function get_tablet_id($t_zone_id)
    {
        $this->db->select('t.tablet_id')
            ->from('tablets t')
            ->where('t.zone_id', $t_zone_id)
            ->limit(1);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->row();
    }

    public function get_tablet_loc($tablet_id)
    {
        $this->db->select('tl.latitude,tl.longitude')
            ->from('tablet_locations tl')
            ->where('tl.tablet_id', $tablet_id)
            ->order_by('tl.id', 'DESC')
            ->limit(1);
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->row();
    }


    public function transfered_booking($booking_id, $service_date)
    {
        $this->db->select('booking_transfer_id, booking_id, zone_id')
            ->from('booking_transfers')
            ->where('service_date', $service_date)
            ->where('booking_id', $booking_id)
            ->order_by('booking_transfer_id', 'DESC');

        $get_booking_transfers_by_date_qry = $this->db->get();

        return $get_booking_transfers_by_date_qry->row();



    }
    public function add_justmop_ref($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('justmop_ref');
        return $this->db->insert_id();
    }

    public function get_payment_mode($booking_id, $service_date)
    {

        $this->db->select('booking_id,mop,just_mop_ref,service_date')
            ->from('justmop_ref')
            ->where('booking_id', $booking_id)
            ->where('service_date', $service_date);
        $get_booking_mode_qry = $this->db->get();
        return $get_booking_mode_qry->row();

    }

    public function update_payment_justmop($booking_id, $service_date, $fields = array())
    {

        $this->db->where('booking_id', $booking_id);
        $this->db->where('service_date', $service_date);
        $this->db->update('justmop_ref', $fields);
        $affected = $this->db->affected_rows();
        return $affected;

    }

    public function get_avilable_before_noon($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));
        $deletes = $this->get_booking_deletes_by_date($service_date);
        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_start_date, b.service_week_day,IF(time_from<'08:00:00','08:00:00',time_from) as time_from, IF(time_to>'13:00:00','13:00:00',time_to) as time_to", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            //->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            //->join('areas a', 'ca.area_id = a.area_id')
            //->join('zones z', 'a.zone_id = z.zone_id')
            //->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.time_from <', '13:00:00')
            //->where('a.deleted_at', null)
            //->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->where('b.maid_id', $maid_id)
            ->order_by('b.time_from', 'ASC');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    public function get_avilable_after_noon($service_date, $maid_id)
    {
        $service_week_day = date('w', strtotime($service_date));
        $deletes = $this->get_booking_deletes_by_date($service_date);
        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_start_date, b.service_week_day, IF(time_from<'13:00:00','13:00:00',time_from) as time_from, IF(time_to>'19:00:00','19:00:00',time_to) as time_to", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            //->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            //->join('areas a', 'ca.area_id = a.area_id')
            //->join('zones z', 'a.zone_id = z.zone_id')
            //->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('b.time_to >', '13:00:00')
            //->where('a.deleted_at', null)
            //->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->where('b.maid_id', $maid_id)
            ->order_by('b.time_from', 'ASC');
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    public function check_day_service_booking_new($booking_id, $service_date)
    {
        $this->db->select("day_service_id,booking_id,odoo_package_activity_status", FALSE)
            ->from('day_services');
        $this->db->where('booking_id', $booking_id);
        $this->db->where('service_date', $service_date);
        $get_results_query = $this->db->get();
        return $get_results_query->row();
    }

    public function check_booking_transferred_new($booking_id, $service_date)
    {
        $this->db->select("bft.booking_transfer_tablet_id,bft.transfering_to_tablet,t.tablet_driver_name", FALSE)
            ->from('booking_transfers_tablet bft')
            ->join('tablets t', 'bft.transfering_to_tablet = t.tablet_id');
        $this->db->where('bft.booking_id', $booking_id);
        $this->db->where('bft.service_date', $service_date);
        $this->db->order_by('bft.booking_transfer_tablet_id', 'DESC');
        $this->db->limit(1);
        $get_results_query = $this->db->get();
        return $get_results_query->row();
    }

    public function check_booking_transferred_new_by_booking_ids($booking_ids = [], $service_date)
    {
        $this->db->select("bft.booking_id, bft.booking_transfer_tablet_id,bft.transfering_to_tablet,t.tablet_driver_name", FALSE)
            ->from('booking_transfers_tablet bft')
            ->join('tablets t', 'bft.transfering_to_tablet = t.tablet_id');
        $this->db->where_in('bft.booking_id', $booking_ids);
        $this->db->where('bft.service_date', $service_date);
        $this->db->order_by('bft.booking_transfer_tablet_id', 'DESC');
        $get_results_query = $this->db->get();
        return $get_results_query->result();
    }

    function getjobcomplaintbyid_new($booking_id, $date)
    {
        $this->db->select("*")
            ->from('complaints_new')
            ->where('booking_id', $booking_id)
            ->where('service_date', $date);


        $get_results_query = $this->db->get();
        return $result = $get_results_query->row();
    }

    function add_complaints_new($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('complaints_new');

        return $complaint_id = $this->db->insert_id();
    }
    function get_complaint_by_cid_new($complaint_id)
    {
        $this->db->select("c.*,u.user_fullname")
            ->from('complaints_new c')
            ->join('users u', 'c.added_by = u.user_id', 'left')
            ->where('c.cmp_id', $complaint_id);
        $get_results_query = $this->db->get();
        return $result = $get_results_query->row();
    }
    function update_complaints_new($fields = array(), $complaint_id)
    {
        $this->db->where('cmp_id', $complaint_id);
        $this->db->update('complaints_new', $fields);
        $affected = $this->db->affected_rows();
        return $affected;
    }

    function get_schedule_by_date_new($service_date, $team_id = NULL)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("
        b.booking_id,
        b.tabletid,
        b.pay_by,
        b.justmop_reference,
        b.customer_id,
        b.customer_address_id,
        b.maid_id,
        b.service_type_id,
        b.service_start_date,
        b.service_week_day,
        b.is_locked,
        b.cleaning_material,
        b.cleaning_material_fee,
        DATE_FORMAT(b.time_from, '%H:%i') AS time_from,
        DATE_FORMAT(b.time_to, '%H:%i') AS time_to,
        ROUND(TIMESTAMPDIFF(MINUTE,b.time_from,b.time_to)/60,2) AS working_hours,
        b.booking_type, b.service_end,
        b.service_end_date, b.service_actual_end_date,
        b.booking_note, b.pending_amount,
        b.discount,
        b.service_charge,
        b.vat_charge,
        b.net_service_cost,
        b.total_amount,
        b.booking_status,
        b.price_per_hr,
        b.discount_price_per_hr,
        b.crew_in,
        c.customer_name,
        c.price_hourly,
        c.balance,
        c.signed,
        c.customer_nick_name,
        c.mobile_number_1,
        c.key_given,
        c.payment_type,
        c.payment_mode,
        c.longitude,
        c.latitude,
        c.customer_source,
        z.zone_id,
        z.zone_name,
        a.area_name,
        ca.customer_address,
        ca.building,
        m.maid_name,
        m.maid_nationality,
        m.maid_mobile_1,
        m.maid_photo_file,
        u.user_fullname,
        ca.building,
        ca.unit_no,
        ca.street,
        st.service_type_name", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->join('service_types st', 'b.service_type_id = st.service_type_id', 'left')
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');
        //                if($team_id != NULL)
//		{
//			$this->db->where('m.team_id', $team_id);
//		}

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();

        $bookings = $get_schedule_by_date_qry->result();

        $dayservices = $this->get_booking_dayservices_by_date($service_date);

        $serviceStatuses = [];

        foreach ($dayservices as $dayservice) {
            $serviceStatuses[$dayservice->booking_id] = $dayservice->service_status;
        }

        foreach ($bookings as $key => $booking) {
            $bookings[$key]->service_status = $serviceStatuses[$booking->booking_id];
        }

        return $bookings;
    }
    function get_bookings_by_customer_and_between_dates($customer_id,$start_date,$end_date)
    {
        $this->db->select("b.booking_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('bookings as b')
            ->where("b.booking_status", 1)
            ->where("b.customer_id", $customer_id)
            ->where("(b.service_start_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' OR b.service_actual_end_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' OR (b.service_end = 0 AND b.service_start_date < '" . $start_date . "'))", null, false);
        $query = $this->db->get();
        return $query->result();
    }

    function get_booking_maid($week_day, $maid_id)
    {
        $service_date = date('Y-m-d');

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, b.maid_id, b.service_start_date,
        
            b.service_week_day, 'm.maid_name', 
            b.booking_type, b.booking_status, c.customer_name, 
            b.time_from,
            ds.time_from as ds_time_from,
            ds.time_to as ds_time_to,
            b.time_to,
            TIMESTAMPDIFF(MINUTE,ds.time_from ,ds.time_to) as ds_working_minutes,
            TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minute,
            SUM(TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to)) as working_minutes", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = "' . $service_date . '"','left')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.area_status', 1)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where('b.booking_type', 'WE')
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where('b.service_week_day', $week_day)
            ->where('b.maid_id', $maid_id);
        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get()->result();
        $booking_data = array();
        $booking_data2 = array();
        foreach ($get_schedule_by_date_qry as $key => $schedule) {
            $get_schedule_by_date_qry[$key]->working_minutes = $schedule->working_minutes / 60;
            $tot_time = $schedule->working_minutes;
            $max_time = 8;
            $percentage = ($tot_time / ($max_time / 100));
            $percentage =  round($percentage,2);
            $get_schedule_by_date_qry[$key]->percentage = $percentage;

            if ($schedule->day_service_id != "")
            {
                $get_schedule_by_date_qry[$key]->time_from = $schedule->ds_time_from;
                $get_schedule_by_date_qry[$key]->time_to = $schedule->ds_time_to;
                $get_schedule_by_date_qry[$key]->working_minutes = $schedule->ds_working_minutes;
                
            }
            $working_minutes = $schedule->working_minutes;
            $booking_data[$key]['maid_id']  = $schedule->maid_id;
            $booking_data[$key]['working_minutes']  = $working_minutes;
            $booking_data2[$key]['working_minutes']  = $working_minutes;
        }
        
        return $get_schedule_by_date_qry;
    }

    public function get_customer_address($cust_id)
    {
        $this->db->select('ca.customer_address_id, ca.building,ca.unit_no, ca.street, ca.customer_id, ca.area_id,ca.latitude,ca.longitude,ca.customer_address, ca.location_id, ca.landmark_id, a.area_name, c.customer_name, z.zone_name, loc.location_name, land.landmark_name')
        ->from('customer_addresses as ca')
        ->join('areas a', 'ca.area_id = a.area_id')
            // ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('locations loc', 'ca.location_id = loc.location_id')
            ->join('landmarks land', 'ca.landmark_id = land.landmark_id')
            ->join('customers c', 'ca.customer_id = c.customer_id')
            ->where('ca.customer_id', $cust_id)
            ->where('ca.address_status', 1);

        $query = $this->db->get();
        return $query->result();
    }

    function get_cancel_schedule($customer_id)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
          $this->db->where("delete_status", 'cancel_permeantly');


        $get_booking_deletes_qry = $this->db->get()->result();
        $deleted_bookings = array();
        foreach ($get_booking_deletes_qry as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.customer_id, DAYNAME(b.service_start_date) AS shift_day, b.customer_address_id, b.maid_id, b.service_type_id, DATE_FORMAT(b.service_start_date, '%d/%m/%Y') as service_date, b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, DATE_FORMAT(b.service_end_date, '%d/%m/%Y') as service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file, bd.remarks, bd.cancel_reason_id, cr.cancel_reason", FALSE)
        ->from('bookings b')
        ->join('customers c', 'b.customer_id = c.customer_id')
        ->join('maids m', 'b.maid_id = m.maid_id')
        ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
        ->join('areas a', 'ca.area_id = a.area_id')
        ->join('zones z', 'a.zone_id = z.zone_id')
        ->join('users u', 'b.booked_by = u.user_id', 'left')
        ->join('booking_deletes bd', 'b.booking_id = bd.booking_id', 'left')
        ->join('cancel_resons cr', 'bd.cancel_reason_id = cr.cancel_reason_id', 'left')
        ->where('b.booking_category', 'C')
        ->where('b.booking_status', 2)
        ->where("(b.booking_type='WE' OR b.booking_type='BW' OR b.booking_type='OD')", NULL, FALSE)
        ->where('bd.delete_status', 'cancel_permeantly')
        // ->where('z.zone_status', 1)
        // ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
        // ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
        ->where('c.customer_id', $customer_id)
        // ->where_in('b.booking_id', $deleted_bookings)
        ->order_by('b.service_start_date', 'ASC');
        if (count($deleted_bookings) > 0) {
            $this->db->where_in('b.booking_id', $deleted_bookings);
        }
        $get_schedule_by_cancel_qry = $this->db->get()->result();
        // echo '<pre>';
        // print_r($get_schedule_by_cancel_qry);
        return $get_schedule_by_cancel_qry;

    }

    
    function get_pay_employee_customer($customer_id)
    {

        $this->db->select("mc.booking_id, mc.customer_id,mc.service_date,m.maid_id,m.maid_name,mc.total_hours,mc.total_amount,mc.added_date_time", FALSE)
        ->from('maid_hour_cancellation mc')
        ->join('customers c', 'mc.customer_id = c.customer_id')
        ->join('maids m', 'mc.maid_id = m.maid_id')
       
       
        ->where('mc.customer_id', $customer_id);
       
        $get_schedule_by_cancel_qry = $this->db->get()->result();
        // echo '<pre>';
        // print_r($get_schedule_by_cancel_qry);
        return $get_schedule_by_cancel_qry;

    }
}