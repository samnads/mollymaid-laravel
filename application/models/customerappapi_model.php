<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Customers_Model Class
 * 
 * @author Geethu
 * @package HM
 * @version Version 1.0
 */
class Customerappapi_model extends CI_Model {

    function __construct() {
        parent::__construct();
		
    }
    
    public function check_email($email) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('customers')
                ->where('email_address', $email)
                ->limit(1);
        $user = $this->db->get();
        if ($user->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public function check_mobile($mobile) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('customers')
                ->where('phone_number', $mobile)
                ->or_where('mobile_number_1', $mobile)
                ->or_where('mobile_number_2', $mobile)
                ->or_where('mobile_number_3', $mobile)
                ->limit(1);
        $user = $this->db->get();
        if ($user->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public function add_customers($data) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->set($data);
        $this->db->insert('customers');
        $result = $this->db->insert_id();
        return $result;
    }
    
    public function get_customer_by_id($customer_id) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("customer_id, odoo_customer_id, customer_name,customer_username, customer_password, customer_nick_name, mobile_number_1, mobile_number_2, mobile_number_3, phone_number, fax_number, email_address, website_url, customer_photo_file, customer_booktype, customer_type, contact_person, payment_type, payment_mode, price_hourly, price_extra, price_weekend, latitude, longitude, key_given, customer_notes,mobile_verification_code,mobile_status, DATE_FORMAT(customer_added_datetime, '%d / %M / %Y %h:%i %p') AS added_datetime, customer_status,balance, signed, ",FALSE)
                ->from('customers')
                ->where('customer_id', $customer_id)
                ->limit(1);

        $get_customer_by_id_qry = $this->db->get();
        //echo $this->db->last_query();exit;
		$details=$get_customer_by_id_qry->row();
		
        return $details;
    }
    
    public function customer_login($email,$password) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('customers')
                ->where('email_address', $email) 
                ->where('customer_password', $password)
                ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
    
    public function get_all_customer_details($customer_id) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.customer_photo_file, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.fax_number, c.email_address, c.customer_status, c.odoo_customer_id, c.website_url, c.customer_type, c.payment_type, c.payment_mode, c.key_given, c.customer_password, c.customer_username, c.price_hourly, c.price_extra, c.price_weekend, c.latitude, c.longitude, c.mobile_status, c.customer_notes, ca.customer_address,ca.area_id,ca.other_area,ca.building,ca.unit_no,ca.street, a.odoo_area_id, a.area_name')
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->where('c.customer_id',$customer_id)
                ->limit(1);
                //->join('zones z', 'a.zone_id = z.zone_id')
                //->group_by('ca.customer_id')
                //->order_by('c.customer_name', 'ASC');
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->row();
    }
    
    public function update_customers($data, $customer_id)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
        return $this->db->affected_rows();
    }
    
    public function get_customer_by_token($device_token) 
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('customers')
                ->where('fb_id', $device_token)
                ->limit(1);
        $user = $this->db->get();
        return $user->row();
    }
    
    public function get_areas()
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('a.area_id, a.zone_id, a.area_name, a.area_charge, a.area_status, z.zone_name')
                ->from('areas a')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('a.zone_id, a.area_name');
        $this->db->where('area_status', 1);
        $this->db->where('zone_status', 1);
		$this->db->where('a.web_status',1);
        $get_areas_qry = $this->db->get();

        return $get_areas_qry->result();
    }
    
    /**
     * Get customer address
     * 
     * @author  Azinova     * 
     * @access  public
     * @param   int, bool
     * @return array
     */
    public function get_customer_addresses($customer_id, $active_only = TRUE)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address, a.area_name, z.zone_id, z.zone_name')
                ->from('customer_addresses ca')
                ->where('ca.customer_id', $customer_id)
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('ca.customer_address_id');

        if ($active_only) {
            $this->db->where('z.zone_status', 1);
            $this->db->where('a.deleted_at', null);
        }

        $get_customer_addresses_qry = $this->db->get();

        return $get_customer_addresses_qry->result();
    }
    
    public function add_customer_address_new($datas)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->set($datas);
        $this->db->insert('customer_addresses');
        $result = $this->db->insert_id();
        return $result;
    }
    
    public function update_customer_address_new($datas,$user_id)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->where('customer_id', $user_id);
        $this->db->update('customer_addresses', $datas);
        $result = $user_id;
        return $result;
    }
    
    public function get_extra_service_details()
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('extra_services')
                ->where('status',0);
        $get_qry = $this->db->get();
        return $get_qry->result();
    }
    
    public function getcleaningservices()
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('service_types')
                ->where('service_category','C')
                ->where('service_type_status',1);
        $get_qry = $this->db->get();
        return $get_qry->result();
    }
    
    public function getmaintenanceservices()
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('*')
                ->from('service_types')
                ->where('service_category','M')
				->or_where('service_category','A')
                ->where('service_type_status',1)
				->order_by('order_id','asc');
        $get_qry = $this->db->get();
        return $get_qry->result();
    }
    
    public function get_customers_by_field_value($field_name, $field_value)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('customer_id')
                ->from('customers')
                ->where($field_name, $field_value)
                ->limit(1);

        $get_customers_by_field_value_qry = $this->db->get();

        return $get_customers_by_field_value_qry->row();
    }
    
    public function get_available_times_new($date)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("from_time")
                ->from('booking_slots')
                ->where('date',$date)
                ->where('status',1);

        $get_time_qry = $this->db->get();

        return $get_time_qry->result_array();  
    }
    
    public function get_fee_details()
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("*")
                ->from('price_settings');

        $get_fee_details_qry = $this->db->get();

        return $get_fee_details_qry->result(); 
    }
    
    public function get_extraservice_rate($id)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("*")
                ->from('extra_services')
                ->where('id',$id)
                ->where('status',0);

        $get_extra_qry = $this->db->get();

        return $get_extra_qry->row();  
    }
    
//    public function get_vat_rate($id)
//    {
//        $this->db->select("*")
//                ->from('settings')
//                ->where('id',$id);
//
//        $get_extra_qry = $this->db->get();
//
//        return $get_extra_qry->row();  
//    }
    
    public function add_booking($fields = array())
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
//            $fields['service_actual_end_date'] = isset($fields['service_actual_end_date']) ? $fields['service_actual_end_date'] : $fields['service_end_date'];
//            $fields['booked_datetime'] = isset($fields['booked_datetime']) ? $fields['booked_datetime'] : date('Y-m-d H:i:s');
//            $fields['booking_status'] = isset($fields['booking_status']) ? $fields['booking_status'] : 0;

            $this->db->set($fields);
            $this->db->insert('bookings'); 

            $booking_id = $this->db->insert_id();
            //$this->add_activity($booking_id, 'Booking_add');

            return $booking_id;
    }
    
	
	
    public function get_address_details($customer_id) {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('customer_address_id')
                ->from('customer_addresses')
                ->where('customer_id', $customer_id)
                ->where('default_address', 1)
                ->limit(1);

        $get_customer_address_by_area_qry = $this->db->get();

        return $customer_address = $get_customer_address_by_area_qry->row();
        
    }
    
    function get_bookingdetails_by_refid($reference_id)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('b.*,c.*,a.area_name,s.service_type_name,ca.building,ca.unit_no,ca.street,ca.other_area,c.mobile_number_1,DATE_FORMAT(b.time_from, "%h:%i %p") AS start_time,DATE_FORMAT(b.time_to, "%h:%i %p") AS end_time', FALSE)
                ->from('bookings b')    
                ->join('customers c','b.customer_id = c.customer_id')                                 
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id','left')
                ->join('areas a','ca.area_id = a.area_id','left')
                ->join('service_types s','b.service_type_id = s.service_type_id','left')
                ->where('b.reference_id', $reference_id)
		->order_by('b.booking_id', 'desc')
                ->limit(1);		//->group_by('ca.customer_id');

        $get_booking_by_id_qry = $this->db->get();

        return $get_booking_by_id_qry->result();
    }
	
	function get_bookingdetails_by_bookid($book_id)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('ds.*', FALSE)
                ->from('day_services ds')
                ->where('ds.booking_id', $book_id);
				//->group_by('ca.customer_id');

        $get_booking_by_id_qry = $this->db->get();

        return $get_booking_by_id_qry->row();
    }
    
    function get_current_booking_mobile($user_id)
    {
		////$DB2 = $this->load->database('anotherdb', TRUE);
        $service_date = date('Y-m-d');
        //$service_date="2021-02-24";
        $service_week_day = date('w', strtotime($service_date));
        
        $this->db->select('b.booking_id,b.is_cancelled,b.payment_type,b.booked_from,st.service_type_name,b.booking_category,b.reference_id,b.time_type,b.booking_status,b.no_of_maids,b.total_amount,b.total_net_amount,b.cleaning_material,b.priority_type,DATE_FORMAT(b.time_from, "%h:%i %p") as time_val, b.booking_type,b.discount,IF(b.booking_type = "OD", DATE_FORMAT(b.service_start_date, "%d-%b-%Y") , CONCAT("Every ", DAYNAME(b.service_start_date)))  AS service_date, b.service_start_date, b.time_from, b.time_to, CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), " - " , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, DATE_FORMAT(b.service_start_date, "%d %M %Y") AS service_start_date,no_of_maids', FALSE)
                 ->from('bookings b')
                 ->join('customers c', 'b.customer_id = c.customer_id')
                 ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
                 ->where('b.booking_status !=', 3)
                 ->where('b.customer_id', $user_id)
                 ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                 ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW') )", NULL, FALSE)
                 //->group_by('b.reference_id')
				 ->order_by('b.service_start_date', 'ASC');
		
        
        $get_booking_qry = $this->db->get();
        
        return $get_booking_qry->result();
    }
	
    function get_shift_booking_mobile($reference_id)
    {
        $service_date = date('Y-m-d');
        //$service_date="2018-02-24";
        $service_week_day = date('w', strtotime($service_date));
        
        $this->db->select('b.booking_id,b.reference_id,st.service_type_name,b.booking_category,b.time_type,b.booking_status,b.no_of_maids,b.total_amount, b.total_net_amount, b.cleaning_material,b.priority_type,DATE_FORMAT(b.time_from, "%h:%i %p") as time_val, b.booking_type,b.discount,IF(b.booking_type = "OD", DATE_FORMAT(b.service_start_date, "%d-%b-%Y") , CONCAT("Every ", DAYNAME(b.service_start_date)))  AS service_date, b.service_start_date, b.time_from, b.time_to, CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), " - " , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, DATE_FORMAT(b.service_start_date, "%d %M %Y") AS service_start_date,no_of_maids', FALSE)
                 ->from('bookings b')
                 ->join('customers c', 'b.customer_id = c.customer_id')
                 ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
                 ->where('b.booking_status !=', 3)
                 ->where('b.reference_id', $reference_id)
                 ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                 ->where("((b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'WE'))", NULL, FALSE)
                 //->group_by('b.reference_id')
				 ->order_by('b.service_start_date', 'ASC');
		
        
        $get_booking_qry = $this->db->get();
        
        return $get_booking_qry->result();
    }
    
    function get_shift_booking_mobile_by_bookingid($booking_id)
    {
        $service_date = date('Y-m-d');
        //$service_date="2018-02-24";
        $service_week_day = date('w', strtotime($service_date));
        
        $this->db->select('b.booking_id,b.reference_id,st.service_type_name,b.booking_category,b.time_type,b.booking_status,b.no_of_maids,b.total_amount, b.total_net_amount,b.cleaning_material,b.priority_type,DATE_FORMAT(b.time_from, "%h:%i %p") as time_val, b.booking_type,b.discount,IF(b.booking_type = "OD", DATE_FORMAT(b.service_start_date, "%d-%b-%Y") , CONCAT("Every ", DAYNAME(b.service_start_date)))  AS service_date, b.service_start_date, b.time_from, b.time_to, CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), " - " , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, DATE_FORMAT(b.service_start_date, "%d %M %Y") AS service_start_date,no_of_maids', FALSE)
                 ->from('bookings b')
                 ->join('customers c', 'b.customer_id = c.customer_id')
                 ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
                 ->where('b.booking_status !=', 3)
                 ->where('b.booking_id', $booking_id)
                 ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                 ->where("((b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'WE'))", NULL, FALSE)
                 //->group_by('b.reference_id')
                ->order_by('b.service_start_date', 'ASC');
		
        
        $get_booking_qry = $this->db->get();
        
        return $get_booking_qry->result();
    }
    
    function get_bookingdetails_by_id($booking_id)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('b.*,c.*,a.area_name,s.service_type_name,ca.building,ca.unit_no,ca.street,c.mobile_number_1,DATE_FORMAT(b.time_from, "%H:%i %p") AS start_time,DATE_FORMAT(b.time_to, "%H:%i %p") AS end_time', FALSE)
                ->from('bookings b')    
                ->join('customers c','b.customer_id = c.customer_id')                                 
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a','ca.area_id = a.area_id','left')
                ->join('service_types s','b.service_type_id = s.service_type_id','left')
                ->where('b.booking_id', $booking_id)
                ->limit(1);

        $get_booking_by_id_qry = $this->db->get();

        return $get_booking_by_id_qry->row();
    }
    
    function check_online_pay_by_bookid($book_id)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
      $this->db->select("*")
                ->from('online_payments o')
//			->where('o.payment_status','Success')
                ->where('o.booking_id',$book_id);
        $get_booking_qry = $this->db->get();

        return $get_booking_qry->row();  

    }
	
	function check_online_pay_by_refid($ref_id)
    {
		
      $this->db->select("*")
                ->from('online_payments o')
                ->where('o.reference_id',$ref_id);
        $get_booking_qry = $this->db->get();

        return $get_booking_qry->row();  

    }
    
    function get_previous_booking_mobile($customer_id, $params = array())
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        
        $service_date = date('Y-m-d');
        
        //$this->db->limit($per_page, $offset);
        $this->db->select('m.maid_name,b.reference_id,b.booking_id,b.payment_type,b.booked_from,b.is_cancelled,b.total_amount,st.service_type_name,b.time_type,b.booking_category,b.priority_type, b.cleaning_material,b.no_of_maids,b.booking_status, b.booking_type, CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), " - " , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, DATE_FORMAT(ds.service_date, "%d-%b-%Y") AS service_date, ds.total_fee, ds.service_status, ds.day_service_id,ds.payment_status', FALSE)
                 ->from('day_services ds')
                 ->join('bookings b', 'ds.booking_id = b.booking_id')
                 ->join('customers c', 'ds.customer_id = c.customer_id')
                ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
                 ->join('maids m', 'ds.maid_id = m.maid_id')
                 ->where('ds.service_status', 2)
                 ->where('ds.customer_id', $customer_id)
                 ->where("ds.service_date < '" . $service_date . "'")
				 ->group_by('b.reference_id')
                 ->order_by('b.service_start_date', 'DESC');
//          if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
//            $this->db->limit($params['limit'],$params['start']);
//        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
//            $this->db->limit($params['limit']);
//        }
        
        $get_previous_bookings_qry = $this->db->get();
        
        return $get_previous_bookings_qry->result();
    }
	
    function get_shift_prev_booking_mobile($ref_id)
    {        
        $service_date = date('Y-m-d');        
        
        $this->db->select('m.maid_name,b.total_amount,b.booking_id,st.service_type_name,b.time_type,b.booking_category,b.priority_type, b.cleaning_material,b.no_of_maids,b.booking_status, b.booking_type, CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), " - " , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, DATE_FORMAT(ds.service_date, "%d-%b-%Y") AS service_date, ds.total_fee, ds.service_status, ds.day_service_id,ds.payment_status', FALSE)
                 ->from('day_services ds')
                 ->join('bookings b', 'ds.booking_id = b.booking_id')
                 ->join('customers c', 'ds.customer_id = c.customer_id')
                 ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
                 ->join('maids m', 'ds.maid_id = m.maid_id')
                 ->where('ds.service_status', 2)
                 ->where('b.reference_id', $ref_id)
                 ->where("ds.service_date < '" . $service_date . "'")
                 ->order_by('b.service_start_date', 'DESC');
        
        $get_previous_bookings_qry = $this->db->get();
        
        return $get_previous_bookings_qry->result();
    }
    
    function get_shift_prev_booking_mobile_by_bookingid($booking_id)
    {        
        $service_date = date('Y-m-d');        
        
        $this->db->select('m.maid_name,b.total_amount,b.booking_id,st.service_type_name,b.time_type,b.booking_category,b.priority_type, b.cleaning_material,b.no_of_maids,b.booking_status, b.booking_type, CONCAT(DATE_FORMAT(b.time_from, "%h:%i %p"), " - " , DATE_FORMAT(b.time_to, "%h:%i %p")) AS shift, DATE_FORMAT(ds.service_date, "%d-%b-%Y") AS service_date, ds.total_fee, ds.service_status, ds.day_service_id,ds.payment_status', FALSE)
                 ->from('day_services ds')
                 ->join('bookings b', 'ds.booking_id = b.booking_id')
                 ->join('customers c', 'ds.customer_id = c.customer_id')
                 ->join('service_types st', 'b.service_type_id = st.service_type_id','left')
                 ->join('maids m', 'ds.maid_id = m.maid_id')
                 ->where('ds.service_status', 2)
                 ->where('b.booking_id', $booking_id)
                 ->where("ds.service_date < '" . $service_date . "'")
                 ->order_by('b.service_start_date', 'DESC');
        
        $get_previous_bookings_qry = $this->db->get();
        
        return $get_previous_bookings_qry->result();
    }
    
    function get_coupon_id($coupon)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $today = date('Y-m-d');
        $this->db->select("*")
                ->from('coupon_code')
                ->where('coupon_name',$coupon)
                ->where('type','C')
                ->where('status',1)
                ->where('expiry_date >=',$today);

        $get_coupon_qry = $this->db->get();

        return $get_coupon_qry->row(); 
    }
	
	function get_coupon_by_id($coupon_id)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        //$today = date('Y-m-d');
        $this->db->select("*")
                ->from('coupon_code')
                ->where('coupon_id',$coupon_id)
                ->where('type','M')
                ->where('status',1);

        $get_coupon_qry = $this->db->get();

        return $get_coupon_qry->row(); 
    }
    
    function check_coupon_used($customer_id,$coupon_id)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("*")
                ->from('customer_coupons')
                ->where('customer_id',$customer_id)
                ->where('coupon_id',$coupon_id);

        $get_coupon_qry = $this->db->get();

        return $get_coupon_qry->result(); 
    }
    
    function customer_new_booking($customer_id)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("b.booking_id")
                ->from('bookings b')
                ->where('b.customer_id',$customer_id);
        $get_booking_qry = $this->db->get();

        return $get_booking_qry->num_rows(); 
    }
    
    function insert_coupon_for_customer($fields = array())
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->set($fields);
        $this->db->insert('customer_coupons'); 
        return $this->db->insert_id();
    }
    
    function get_coupon_fee_refid($refid)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("discount")
                ->from('customer_coupons')
                ->where('reference_id',$refid);

        $get_fee_details_qry = $this->db->get();

        return $get_fee_details_qry->row(); 
    }
    
    function get_coupon_id_maint($coupon)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $today = date('Y-m-d');
        $this->db->select("*")
                ->from('coupon_code')
                ->where('coupon_name',$coupon)
                ->where('type','M')
                ->where('status',1)
                ->where('expiry_date >=',$today);

        $get_coupon_qry = $this->db->get();

        return $get_coupon_qry->row(); 
    }
    
    function get_coupon_percentage($coupon_id)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("percentage")
                ->from('coupon_code')
                ->where('coupon_id',$coupon_id);

        $get_coupon_qry = $this->db->get();

        return $get_coupon_qry->row();
    }
	
	function getversiondetails($type)
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("new_version,type")
                ->from('version_update')
                ->where('device_type',$type);

        $getversiondetails_qry = $this->db->get();

        return $getversiondetails_qry->row(); 
    }
	
	function get_service_area_detail()
    {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select("*")
                ->from('areas');
		$this->db->where('area_status', 1);
		$this->db->where('web_status',1);

        $get_service_area_detail_qry = $this->db->get();

        return $get_service_area_detail_qry->result(); 
    }
	
	public function add_payment($data) 
    {
        $this->db->set($data);
        $this->db->insert('online_payments');
        $result = $this->db->insert_id();
        return $result;
    }
	
	public function check_payment($data) 
		{
			$this->db->select("*")
					->from('online_payments')
					->where('reference_id',$data['reference_id'])
					->limit(1);

			$check_payment_qry = $this->db->get();

			return $check_payment_qry->row();
		}
	
	public function update_payment($datas)
    {
        $this->db->where('reference_id', $datas['reference_id']);
        $this->db->update('online_payments', $datas);
        return $datas['reference_id'];
    }
	
	public function update_payment_type($refid)
    {
        $this->db->set('pay_by', 'cash');
        $this->db->where('reference_id', $refid);
        $this->db->update('bookings');
		return $refid;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    function get_customers($active_only = TRUE, $syn = FALSE) {

        $this->db->select('c.customer_id, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.mobile_number_2, c.mobile_number_3, c.phone_number, c.fax_number, c.email_address, c.customer_status, c.odoo_customer_id, c.website_url, c.customer_type, c.payment_type, c.payment_mode, c.key_given, c.customer_password, c.customer_username, c.price_hourly, c.price_extra, c.price_weekend, c.latitude, c.longitude, c.customer_notes, ca.customer_address, a.odoo_area_id, a.area_name, z.zone_name, c.odoo_synch_status')
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->group_by('ca.customer_id')
                ->order_by('c.customer_name', 'ASC');

        if ($active_only) {
            $this->db->where('customer_status', 1);
            $this->db->where('zone_status', 1);
            $this->db->where('area_status', 1);
        }
        
        if($syn)
        {
            
            $this->db->where('odoo_synch_status', 0);
            //$this->db->where_not_in('c.customer_id', array(5132,310,8579,3596,1684,8632,3734,9071,8587,5626));
            //$this->db->limit(1000, 9000);
        }
        
        
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->result();
    }

    function get_customer_zone_by_address_id($customer_address_id) {
        $this->db->select('z.zone_id')
                ->from('customer_addresses ca')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('ca.customer_address_id', $customer_address_id)
                ->where('z.zone_status', 1)
                ->where('a.deleted_at', null);


        $get_customer_zone_by_address_id_qry = $this->db->get();

        return $get_customer_zone_by_address_id_qry->row();
    }

    

    

    function add_customer_payment($fields = array()) {
        $fields['paid_datetime'] = isset($fields['paid_datetime']) ? $fields['paid_datetime'] : date('Y-m-d H:i:s');

        $this->db->set($fields);
        $this->db->insert('customer_payments');
        return $this->db->insert_id();
    }

    /**
     * Add customer
     * 
     * @author  Azinova Develpers      
     * @access  public
     * @param   
     * @return 
     */
    
    
    function add_user_actvty($data) {
        $this->db->set($data);
        $this->db->insert('user_activity');
        $result = $this->db->insert_id();
        return $result;
    }

    function add_customer_address($datas) {
        $this->db->set($datas);
        $this->db->insert('customer_addresses');
        $result = $this->db->insert_id();
    }

    /**
     * Get all customer 
     * 
     * @author  Betsy
     * @access  public
     * @param   
     * @return  array
     */
    function get_all_customers() {
        $this->db->select('customers.*')
                ->from('customers');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_customer_details($customer_id) {
        $this->db->select('*')
                ->from('customers')
                ->where('customers.customer_id', $customer_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_customer_address($cust_id) {

        $query = $this->db->query('select customer_addresses.customer_address_id, customer_addresses.customer_id, customer_addresses.area_id,customer_addresses.latitude,customer_addresses.longitude,customer_addresses.customer_address, areas.area_name, customers.customer_name, z.zone_name
from customer_addresses
join areas ON customer_addresses.area_id = areas.area_id
join zones z ON areas.zone_id = z.zone_id
join customers ON customer_addresses.customer_id = customers.customer_id
where customer_addresses.customer_id = ' . $cust_id);
        return $query->result_array();
    }

    
    
    function update_booktype($data,$customer_id){
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers', $data);
    }

    function delete_customer_address($customer_id) {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_addresses');
    }

    function update_customer_address($datas, $customer_address_id) {
        
        

        //$customer_address_id = $this->get_customer_address_by_area($datas['customer_id'], $datas['area_id']);
        if ($customer_address_id) {
            $this->db->where('customer_address_id', $customer_address_id);
            $this->db->update('customer_addresses', $datas);
            $result = $customer_address_id;
        } else {
            $this->db->set($datas);
            $this->db->insert('customer_addresses');
            $result = $this->db->insert_id();
        }
        return $result;
    }

    function get_customer_address_by_area($customer_id, $area_id) {
        $this->db->select('customer_address_id')
                ->from('customer_addresses')
                ->where('customer_id', $customer_id)
                ->where('area_id', $area_id)
                ->limit(1);

        $get_customer_address_by_area_qry = $this->db->get();

        $customer_address = $get_customer_address_by_area_qry->row();
        //print_r($customer_address);
        //exit;
        if ($customer_address->customer_address_id > 0)
            return $customer_address->customer_address_id;
        else
            return FALSE;
    }

    function remove_customer_address($address_id) {
        $this->db->where('customer_address_id', $address_id);
        $this->db->delete('customer_addresses');
    }

    

    function delete_customer($customer_id, $status) {
        $this->db->set('customer_status', $status);
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers');
    }

    public function record_count($active) {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status, ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $get_active_customer_qry = $this->db->get();

        return $get_active_customer_qry->num_rows();

        /* if ($active == 1) {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas','ca.area_id=a.area_id');
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          } else if ($active == 2) {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas a','ca.area_id=a.area_id')->where('c.customer_status', 1);
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          } else {
          $this->db->select('c.customer_id')->from("customers c")->join('customer_addresses ca', 'c.customer_id=ca.customer_id')->join('areas a','ca.area_id=a.area_id')->where('c.customer_status', 0);
          $get_active_customer_qry = $this->db->get();

          return $get_active_customer_qry->num_rows();
          }
         */
    }

    public function fetch_customers($limit, $start, $active = 1) {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $this->db->group_by('c.customer_id');
        $this->db->limit($limit, $start);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    public function get_last_job_date_by_customerid($customer_id)
    {
        $this->db->select('service_date')
                ->from('day_services')
                ->where('customer_id',$customer_id)
                ->order_by('service_date','desc')
                ->limit(1);
        $get_last_job_date_by_customerid_qry = $this->db->get();
        return $get_last_job_date_by_customerid_qry->row();        
    }
    
    public function fetch_customers_excel($active = 1) {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');
        if ($active == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($active == 3) {
            $this->db->where('c.customer_status', 0);
        }
        $this->db->group_by('c.customer_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    public function get_total_customers_counts() {

        $this->db->select("c.customer_id, c.customer_name, c.payment_type, c.mobile_number_1, c.customer_status, ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $get_active_customer_qry = $this->db->get();
        return $get_active_customer_qry->num_rows();
    }

    function get_customers_by_keyword($keyword) {
        $keyword = strtolower($keyword);

        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left');

        $customer_field = $this->db->list_fields('customers');
        foreach ($customer_field as $field) {
            $this->db->or_like("LOWER(c." . $field . ")", $keyword);
        }
        $customer_address_field = $this->db->list_fields('customer_addresses');
        foreach ($customer_address_field as $field) {
            $this->db->or_like("LOWER(ca." . $field . ")", $keyword);
        }
        $area_field = $this->db->list_fields('areas');
        foreach ($area_field as $field) {
            $this->db->or_like("LOWER(a." . $field . ")", $keyword);
        }
        $zone_field = $this->db->list_fields('zones');
        foreach ($zone_field as $field) {
            $this->db->or_like("LOWER(z." . $field . ")", $keyword);
        }
        
         $this->db->group_by('c.customer_id');

        $get_customers_by_keyword_query = $this->db->get();

        //echo $this->db->last_query();exit;
        return $get_customers_by_keyword_query->result();
    }

    /**
     * Get customer address by id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_customer_address_by_id($customer_address_id) {
        $this->db->select('ca.customer_address_id, ca.customer_id, ca.area_id, ca.customer_address, a.area_name, z.zone_id, z.zone_name')
                ->from('customer_addresses ca')
                ->where('ca.customer_address_id', $customer_address_id)
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->order_by('ca.customer_address_id');

        $get_customer_address_by_id_qry = $this->db->get();

        return $get_customer_address_by_id_qry->row();
    }

    /**
     * Get maid history by customer id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_maid_history($customer_id) {
        $this->db->select('m.maid_name, DATE_FORMAT(ds.service_date, "%d/%m/%Y") AS service_date, DATE_FORMAT(ds.start_time, "%h:%i %p") AS start_time, DATE_FORMAT(ds.end_time,"%h:%i %p") AS end_time, IF(ds.service_status = 1,"Service Started", IF(ds.service_status = 2, "Service Finished","Service not Done")) as service_status, DATE_FORMAT(b.time_from,"%h:%i %p") AS start_from_time, DATE_FORMAT(b.time_to,"%h:%i %p") AS end_to_time', FALSE)
                ->from('day_services ds')
                ->where('ds.customer_id', $customer_id)
                ->where('ds.service_status !=',3,FALSE)
                ->join('customers c', 'ds.customer_id = c.customer_id')
                ->join('maids m', 'ds.maid_id = m.maid_id')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
               
                ->order_by('ds.service_date', 'desc');

        $get_maid_history_qry = $this->db->get();

        return $get_maid_history_qry->result();
    }

    /**
     * Get payment history by customer id
     * 
     * @author	Geethu
     * @acces	public 
     * @param	int
     * @return	array
     */
    function get_payment_history($customer_id) {
        $this->db->select('m.maid_name, DATE_FORMAT(ds.service_date, "%d/%m/%Y") AS service_date, ds.total_fee, cp.paid_amount, DATE_FORMAT(ds.start_time, "%h:%i %p") AS start_time, DATE_FORMAT(ds.end_time,"%h:%i %p") AS end_time, DATE_FORMAT(b.time_from,"%h:%i %p") AS start_from_time, DATE_FORMAT(b.time_to,"%h:%i %p") AS end_to_time', FALSE)
                ->from('day_services ds')
                ->where('ds.customer_id', $customer_id)
                ->where('ds.payment_status', '1')
                ->join('customers c', 'ds.customer_id = c.customer_id')
                ->join('maids m', 'ds.maid_id = m.maid_id')
                //added extra by vishnu
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id')
                //ends
                ->order_by('ds.service_date', 'desc');

        $get_maid_history_qry = $this->db->get();

        return $get_maid_history_qry->result();
    }

    function get_current_service($customer_id) {
        $service_date = date('Y-m-d');
        $service_week_day = date('w', strtotime($service_date));
        
        //$this->db->select('b.maid_id, m.maid_name, m.maid_nationality, m.maid_photo_file, CONCAT( GROUP_CONCAT(CONCAT( DATE_FORMAT(b.time_from, "%h:%i %p") ,"-", DATE_FORMAT(b.time_to,"%h:%i %p")) SEPARATOR ","), " [", (IF(b.booking_type = "WE", IF( b.service_week_day = 0, "Sunday", SUBSTRING_INDEX(SUBSTRING_INDEX("Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",",",b.service_week_day),",",-1)), DATE_FORMAT(b.service_start_date, "%d/%m/%Y"))), "]") AS shifts, b.booking_type', FALSE)
        $this->db->select('b.maid_id,b.booking_id, m.maid_name, m.maid_nationality, m.maid_photo_file, CONCAT( GROUP_CONCAT(CONCAT( DATE_FORMAT(b.time_from, "%h:%i %p") ,"-", DATE_FORMAT(b.time_to,"%h:%i %p")) SEPARATOR ","), " [", (IF(b.booking_type = "WE", IF( b.service_week_day = 0, "Sunday", SUBSTRING_INDEX(SUBSTRING_INDEX("Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",",",b.service_week_day),",",-1)), DATE_FORMAT(b.service_start_date, "%d/%m/%Y"))), "]") AS shifts, b.booking_type', FALSE)        
				->from('bookings b')
                ->where('b.customer_id', $customer_id)
                ->where('b.booking_status', '1')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->where("((b.service_start_date >= " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR ((b.service_start_date >= " . $this->db->escape($service_date) . " OR b.service_start_date <= " . $this->db->escape($service_date) . ") AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                ->group_by('b.maid_id, b.booking_type, b.service_start_date')
                ->order_by('b.maid_id');

        $get_maid_history_qry = $this->db->get();
        
        
        return $get_maid_history_qry->result();
    }
    
    function getthisdata()
    {
        $this->db->select("c.customer_id")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        $this->db->where('c.customer_status', 1);
        $this->db->group_by('c.customer_id');
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->result();
    }
    
    function get_booking_deletes_by()
    {
        $this->db->select('booking_id')
                        ->from('booking_deletes');
        $get_booking_deletes_by_date_qry = $this->db->get();
        return $get_booking_deletes_by_date_qry->result();
    }
    
    function getbookcount($custid)
    {
//        $deletes = $this->get_booking_deletes_by();
//        $deleted_bookings = array();
//        foreach($deletes as $delete)
//        {
//                $deleted_bookings[] = $delete->booking_id;
//        }
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
                ->where('b.booking_status', 1)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where('c.customer_id',$custid)
                ->order_by('m.maid_name')
                ->order_by('b.time_from');

//        if(count($deleted_bookings) > 0)
//        {
//                $this->db->where_not_in('b.booking_id', $deleted_bookings);
//        }
        $get_schedule_by_date_qry = $this->db->get();
        return $get_schedule_by_date_qry->num_rows();
    }
    
    public function getdata()
    {
        $customers = $this->getthisdata();
        $dates = date('Y-m-d');
        $datas = array();
        $test = array();
        foreach($customers as $row)
        {
            $custid = $row->customer_id;
            $bookingcount = $this->getbookcount($custid);
            $datas['cid'] = $custid;
            $datas['count'] = $bookingcount;
            array_push($test, $datas);
        }
        return $test;
    }

    public function search_by_date($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 1, $sortsources) {
        if($sort_customers == 3) {
            $this->db->select('b.customer_id,b.booking_id, COUNT(b.customer_id) AS count', false)
                  ->from('bookings b')
                  ->group_by('b.customer_id');
            $this->db->having('count = 1',false);
            $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes)', NULL, FALSE);
            $items = $this->db->get()->result();
            foreach ($items as $rowitem)
            {
                $cus_id = $rowitem->customer_id;
            }
            
            return $items;
            
        } else {
        $date = date('Y-m-d');
        $service_week_day = date('w', strtotime($date));
                
        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        if($sort_customers == 2) {
            $this->db->join('bookings b','c.customer_id = b.customer_id');
            $this->db->where('b.booking_status', 1);
            $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((b.service_start_date = " . $this->db->escape($date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        if($sortsources)
        {
          $this->db->where("c.customer_source",$sortsources);  
            
        }
        if ($from_date && $to_date) {
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$from_date' AND '$to_date'");
        } if(($from_date!="" && $to_date=="" ) || ($from_date=="" && $to_date!="" ) ) {
            $this->db->where("DATE(`customer_added_datetime`) = '$from_date' OR  DATE(`customer_added_datetime`) ='$to_date'");
        }
        if($from_date=="" && $to_date=="" && $payment_type=="")
        {
         $this->db->group_by('c.customer_id');
         //$this->db->limit(100);   
            
        }
        
        return $this->db->get()->result();
        //echo $this->db->last_query();exit();
        }
        
    }
    
    public function search_by_date1($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 1) {
        
        
        //$service_week_day = date('w', strtotime($date));
        $fromDate = date("Y-m-d", strtotime($from_date));
        $toDate = date("Y-m-d", strtotime($to_date));
                
        $this->db->select("c.customer_id, c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id');
        if($sort_customers == 2) {
            $this->db->join('bookings b','c.customer_id = b.customer_id');
            $this->db->where('b.booking_status', 1);
            
//            if ($from_date && $to_date) {
//                $fromDate = date("Y-m-d", strtotime($from_date));
//                $toDate = date("Y-m-d", strtotime($to_date));
//                $this->db->where("(((b.service_actual_end_date BETWEEN '$fromDate' AND '$toDate') AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
//                $this->db->where("((b.service_start_date BETWEEN '$fromDate' AND '$toDate'))",NULL, FALSE);               
            //}else
                if(($from_date!="" && $to_date=="" )) {
                if($from_date == "")
                {
                    $fromDate = "";
                } else {
                    $fromDate = date("Y-m-d", strtotime($from_date));
                    $service_week_day1 = date('w', strtotime($fromDate));
                }
                //$fromDate = date("Y-m-d", strtotime($from_date));
                if($to_date == "")
                {
                    $toDate = "";
                } else {
                    $toDate = date("Y-m-d", strtotime($to_date));
                }
                //$toDate = date("Y-m-d", strtotime($to_date));
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($fromDate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($fromDate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($fromDate) . " AND service_week_day = " . $service_week_day1 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($fromDate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($fromDate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            }else if(($from_date=="" && $to_date!="" )) {
                if($from_date == "")
                {
                    $fromDate = "";
                } else {
                    $fromDate = date("Y-m-d", strtotime($from_date));
                }
                //$fromDate = date("Y-m-d", strtotime($from_date));
                if($to_date == "")
                {
                    $toDate = "";
                } else {
                    $toDate = date("Y-m-d", strtotime($to_date));
                    $service_week_day2 = date('w', strtotime($toDate));
                }
                $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($toDate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
                $this->db->where("((b.service_start_date = " . $this->db->escape($toDate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($toDate) . " AND service_week_day = " . $service_week_day2 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($toDate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($toDate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            } else {
                $date = date('Y-m-d');
                $newdate = date("Y-m-d", strtotime($date));
                $service_week_day3 = date('w', strtotime($date));
            //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
            $this->db->where("((b.service_actual_end_date >= " . $this->db->escape($newdate) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE);
            $this->db->where("((b.service_start_date = " . $this->db->escape($newdate) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($newdate) . " AND service_week_day = " . $service_week_day3 . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($newdate) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($newdate) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE);
            }
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        
        $this->db->group_by('c.customer_id');
        return $this->db->get()->result();
        //echo $this->db->last_query();exit();
        
        
    }
    
    
    public function search_by_date_sort($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 3) {
        $date = date('Y-m-d');
        $this->db->select('b.customer_id, COUNT(b.customer_id) AS count', false)
                ->from('bookings b')
                ->join('customers c','b.customer_id = c.customer_id');
        
        $this->db->group_by('b.customer_id');
        $this->db->having('count = 1',false);
        $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes where service_date = '.$date.')', NULL, FALSE);
        $items = $this->db->get()->result();
        return $items;
    }
    public function search_by_date_sort1($from_date, $to_date, $payment_type, $all_customers = 1, $sort_customers = 3) {
        //$date = date('Y-m-d');
        $fromDate = date("Y-m-d", strtotime($from_date));
        $toDate = date("Y-m-d", strtotime($to_date));
        $this->db->select('b.customer_id, COUNT(b.customer_id) AS count', false)
                ->from('bookings b')
                ->join('customers c','b.customer_id = c.customer_id');
        
        $this->db->group_by('b.customer_id');
        $this->db->having('count = 1',false);
        $this->db->where('b.booking_id NOT IN (SELECT booking_id FROM booking_deletes where service_date BETWEEN '.$fromDate.' AND '.$toDate.')', NULL, FALSE);
        $items = $this->db->get()->result();
        return $items;
    }
    public function search_cust_by_booking($custid,$from_date, $to_date,$payment_type,$all_customers = 1)
    {
        //$originalDate = "2010-03-21";
        
        
        $date = date('Y-m-d');
        //$service_week_day = date('w', strtotime($date));
        $this->db->select("b.customer_id,c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('bookings b')
                ->join('customers c','b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('b.booking_status', 1)
                ->where('b.customer_id',$custid);
        if ($from_date && $to_date) {
            $fromDate = date("Y-m-d", strtotime($from_date));
            $toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$fromDate' AND '$toDate'");
        } if(($from_date!="" && $to_date=="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            //$toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("DATE(`customer_added_datetime`) = '$fromDate'");
        } if(($from_date=="" && $to_date!="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            $this->db->where("DATE(`customer_added_datetime`) ='$toDate'");
        }
        //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
        $this->db->where('b.service_start_date',$date);
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        
        return $this->db->get()->result();
        
    }
    
    public function search_cust_by_booking1($custid,$from_date, $to_date,$payment_type,$all_customers = 1)
    {
        //$originalDate = "2010-03-21";
        
        
        $date = date('Y-m-d');
        //$service_week_day = date('w', strtotime($date));
        $this->db->select("b.customer_id,c.customer_name, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_added_datetime,ca.customer_address, a.area_name, z.zone_name")
                ->from('bookings b')
                ->join('customers c','b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->where('b.booking_status', 1)
                ->where('b.customer_id',$custid);
        if ($from_date && $to_date) {
            $fromDate = date("Y-m-d", strtotime($from_date));
            $toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("b.service_start_date BETWEEN '$fromDate' AND '$toDate'");
        }else if(($from_date!="" && $to_date=="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            //$toDate = date("Y-m-d", strtotime($to_date));
            $this->db->where("b.service_start_date = '$fromDate'");
        }else if(($from_date=="" && $to_date!="" )) {
            if($from_date == "")
            {
                $fromDate = "";
            } else {
                $fromDate = date("Y-m-d", strtotime($from_date));
            }
            //$fromDate = date("Y-m-d", strtotime($from_date));
            if($to_date == "")
            {
                $toDate = "";
            } else {
                $toDate = date("Y-m-d", strtotime($to_date));
            }
            $this->db->where("b.service_start_date ='$toDate'");
        } else {
        //$this->db->where("DATE(`customer_added_datetime`) = ".$newDate);
        $this->db->where('b.service_start_date',$date);
        }
        if ($all_customers == 2) {
            $this->db->where('c.customer_status', 1);
        } else if ($all_customers == 3) {
            $this->db->where('c.customer_status', 0);
        }
        if($payment_type)
        {
          $this->db->where("c.payment_type",$payment_type);  
            
        }
        
        return $this->db->get()->result();
        
    }
    
    function get_back_payment($date)
    {
        $this->db->select('c.customer_id, c.customer_name, c.payment_type, z.zone_name, b.requested_amount, b.collected_amount, DATE_FORMAT(b.collected_date, "%d/%m/%Y") AS collected_date, DATE_FORMAT(b.collected_time, "%h:%i %p") AS collected_time', FALSE)
                ->from('back_payments b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                //edited by vishnu
                //->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                //->join('areas a', 'ca.area_id = a.area_id')
                //->join('zones z', 'a.zone_id = z.zone_id')
                ->join('zones z', 'b.zone_id = z.zone_id')
                //ends
                ->where('b.collected_date', $date);
        
        $get_back_payment = $this->db->get();
        
        return $get_back_payment->result();
    }
    function add_backpayment($fields)
    {
        $fields['payment_added'] = isset($fields['payment_added']) ?  $fields['payment_added'] : date('Y-m-d H:i:s');
        
        $this->db->set($fields);
        $this->db->insert('back_payments');
        return $this->db->insert_id();
    }
    function get_customer_balance($customer_id)
    {
        $this->db->select("SUM(total_fee) - (IFNULL((SELECT SUM(paid_amount) FROM customer_payments WHERE customer_id = $customer_id),0)) AS amount", FALSE)
                ->from('day_services')
                ->where('customer_id', $customer_id)
                ->where('payment_status', 0);
        
        $get_customer_balance_qry = $this->db->get();
        
        return $get_customer_balance_qry->row();
//        $this->db->select("SUM(total_fee) - (SELECT SUM(paid_amount) FROM customer_payments WHERE customer_id = $customer_id) AS amount", FALSE)
//                ->from('day_services')
//                ->where('customer_id', $customer_id)
//                ->where('payment_status', 0);
//        
//        $get_customer_balance_qry = $this->db->get();
//        
//        return $get_customer_balance_qry->row();
    }
    
    /*
     * @auther : Geethu     *
     */
    
    function get_bookings_by_customer_id($customer_id)
    {
        $this->db->select('b.booking_id')
                ->from('bookings b')
                ->join('customers c', 'c.customer_id = b.customer_id')
                ->where('b.booking_status', 1)
                ->where('b.customer_id', $customer_id);
        
        $get_bookings_by_customer_id = $this->db->get();
        
        return $get_bookings_by_customer_id->num_rows();
    }
    
        function get_customer_zone_province_by_cust_id($customer_id) {
        $this->db->select('z.zone_id,ca.area_id,p.province_id,ca.customer_address_id')
                ->from('customer_addresses ca')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('province p', 'z.province_id = p.province_id')
                ->where('ca.customer_id', $customer_id)
                ->where('z.zone_status', 1)
                ->where('a.deleted_at', null)
                ->where('p.status', 1);


        $get_customer_zone_by_address_id_qry = $this->db->get();

        return $get_customer_zone_by_address_id_qry->row();
    }
        function get_customer_zone_province_by_area_id($area_id) {
        $this->db->select('a.area_id,z.zone_id,p.province_id')
                ->from('areas a')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('province p', 'z.province_id = p.province_id')
                ->where('a.area_id', $area_id)
                ->where('z.zone_status', 1)
                ->where('a.deleted_at', null)
                ->where('p.status', 1);


        $get_customer_zone_prov_By_area_qry = $this->db->get();

        return $get_customer_zone_prov_By_area_qry->row();
    }
    
    function role_exists($key)
    {
        $this->db->where('mobile_number_1',$key);
        $query = $this->db->get('customers');
        $count = $query->num_rows();
        return $count;
    } 
    
    function getcustomernamebyid($customer_id)
    {
        $this->db->select("customer_name", FALSE)
                ->from('customers')
                ->where('customer_id', $customer_id)
                ->limit(1);

        $get_customer_by_id_qry = $this->db->get();

        return $get_customer_by_id_qry->row();
    }
    
    
    
    
    /*
     * Author : Jiby
     * Purpose : Calculating customer current week Weekly booking hours
     * Date : 12-10-17
     */
        function get_bookings_data_by_customer_id($customer_id)
    {
            
        $lastSunday = new DateTime('last sunday');
        $nxtSaturday = new DateTime('next saturday');
        $this->db->select('b.booking_id,b.time_from,b.time_to')
                ->from('bookings b')
                ->join('customers c', 'c.customer_id = b.customer_id')
                ->where('b.booking_status', 1)
                ->where('b.booking_type', 'WE')
                ->where('b.service_start_date >=', $lastSunday->format('Y-m-d'))
                ->where('b.service_start_date <=', $nxtSaturday->format('Y-m-d'))
                ->where('b.customer_id', $customer_id);
        
        $get_bookings_by_customer_id = $this->db->get();
        
        return $get_bookings_by_customer_id->result();
    }
	
	
	
	/*Admin App */
	
	function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
	{
		//$DB2 = $this->load->database('anotherdb', TRUE);
		$this->db->select('booking_id')
				->from('booking_deletes');
                if($service_end_date != NULL)
                {
                    $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
                }
                else
                {
                    $this->db->where("service_date", $service_date);
                }
				
		
		$get_booking_deletes_by_date_qry = $this->db->get();
		
		return $get_booking_deletes_by_date_qry->result();
	}
	
	function get_total_booking_hours($date_from = NULL, $type = 1, $date_to = NULL) 
	{
		//$DB2 = $this->load->database('anotherdb', TRUE);
		$service_week_day = date('w', strtotime($date_from));
	
		$deletes = $this->get_booking_deletes_by_date($date_from);

		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
				$deleted_bookings[] = $delete->booking_id;
		}
		if($type === 1)
		{
		$this->db->select("SUM(TIME_TO_SEC(TIMEDIFF(time_to,time_from))) AS duration", FALSE)
						->from('bookings')   
						->where('booking_category', 'C')
						->where('booking_status', 1)                            
						->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
						->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE)
						->limit(1);
		}
		else if($type === 2)
		{
			$this->db->select("SUM(TIME_TO_SEC(TIMEDIFF(end_time, start_time))) AS duration", FALSE)
					->from('day_services')
					->where('service_status', 2);
			if($date_from && $date_to)
			{
				$this->db->where('service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
			}
			else if($date_from || $date_to)
			{
				$date = $date_from ? $date_from : $date_to;
				$this->db->where('service_date', $date);
			}
			else
			{
				$this->db->where('service_date', $date_from);
			}
			
			$this->db->limit(1);
		}
		if(count($deleted_bookings) > 0)
		{
				$this->db->where_not_in('booking_id', $deleted_bookings);
		}

		$get_total_booking_hours_qry = $this->db->get();
		
		
		$booking = $get_total_booking_hours_qry->row();
		
		return isset($booking->duration) ? floor($booking->duration/3600): 0;
	}
	
	function get_total_bookings($date_from = NULL, $type = 1, $date_to = NULL) 
	{
		//$DB2 = $this->load->database('anotherdb', TRUE);
		$service_week_day = date('w', strtotime($date_from));
	
		$deletes = $this->get_booking_deletes_by_date($date_from);

		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
				$deleted_bookings[] = $delete->booking_id;
		}
		if($type === 1)
		{
		$this->db->select("booking_id", FALSE)
						->from('bookings')      
						->where('booking_category', 'C')
						->where('booking_status', 1)                            
						->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
						->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);
						
		}
		else if($type === 2)
		{
			$this->db->select("day_service_id", FALSE)
					->from('day_services')
					->where('service_status', 2);
			if($date_from && $date_to)
			{
				$this->db->where('service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
			}
			else if($date_from || $date_to)
			{
				$date = $date_from ? $date_from : $date_to;
				$this->db->where('service_date', $date);
			}
			else
			{
				$this->db->where('service_date', $date_from);
			}
			
			
		}
		if(count($deleted_bookings) > 0)
		{
				$this->db->where_not_in('booking_id', $deleted_bookings);
		}

		$get_total_bookings_qry = $this->db->get();
		
		
		return $get_total_bookings_qry->num_rows();
	}
	
	function get_one_day_bookings($date_from = NULL, $type = 1, $date_to = NULL) 
        {
			//$DB2 = $this->load->database('anotherdb', TRUE);
            $service_week_day = date('w', strtotime($date_from));
		
            $deletes = $this->get_booking_deletes_by_date($date_from);

            $deleted_bookings = array();
            foreach($deletes as $delete)
            {
                    $deleted_bookings[] = $delete->booking_id;
            }
            if($type === 1)
            {
                $this->db->select("booking_id", FALSE)
                                ->from('bookings')  
								->where('booking_category', 'C')
                                ->where('booking_status', 1) 
                                ->where('booking_type', 'OD')
                                ->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                                ->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);
                                
                if(count($deleted_bookings) > 0)
                {
                        $this->db->where_not_in('booking_id', $deleted_bookings);
                }
            }
            else if($type === 2)
            {
                $this->db->select("d.day_service_id", FALSE)
                        ->from('day_services d')
                        ->join('bookings b', 'd.booking_id=b.booking_id')
                        ->where('b.booking_type', 'OD')
                        ->where('b.booking_status', 1) 
                        ->where('d.service_status', 2);
                if($date_from && $date_to)
                {
                    $this->db->where('d.service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
                }
                else if($date_from || $date_to)
                {
                    $date = $date_from ? $date_from : $date_to;
                    $this->db->where('d.service_date', $date);
                }
                else
                {
                    $this->db->where('d.service_date', $date_from);
                }
                if(count($deleted_bookings) > 0)
                {
                        $this->db->where_not_in('d.booking_id', $deleted_bookings);
                }
                
                
            }
            

            $get_total_bookings_qry = $this->db->get();
            
            
            return $get_total_bookings_qry->num_rows();
        }
		
	function get_week_day_bookings($date_from = NULL, $type = 1, $date_to = NULL) 
        {
			//$DB2 = $this->load->database('anotherdb', TRUE);
            $service_week_day = date('w', strtotime($date_from));
		
            $deletes = $this->get_booking_deletes_by_date($date_from);

            $deleted_bookings = array();
            foreach($deletes as $delete)
            {
                    $deleted_bookings[] = $delete->booking_id;
            }
            if($type === 1)
            {
                $this->db->select("booking_id", FALSE)
                                ->from('bookings')
								->where('booking_category', 'C')
                                ->where('booking_status', 1) 
                                ->where('booking_type', 'WE')
                                ->where("((service_actual_end_date >= " . $this->db->escape($date_from) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                                ->where("((service_start_date = " . $this->db->escape($date_from) . " AND booking_type = 'OD') OR (service_start_date <= " . $this->db->escape($date_from) . " AND service_week_day = " . $service_week_day . " AND booking_type = 'WE') OR (service_start_date <= " . $this->db->escape($date_from) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($date_from) . "), DATE(service_start_date)), 14) = 0 AND booking_type = 'BW'))", NULL, FALSE);
                                
                if(count($deleted_bookings) > 0)
                {
                        $this->db->where_not_in('booking_id', $deleted_bookings);
                }
            }
            else if($type === 2)
            {
                $this->db->select("d.booking_id", FALSE)
                        ->from('day_services d')
                        ->join('bookings b', 'd.booking_id=b.booking_id')
						->where('b.booking_category', 'C')
                        ->where('b.booking_type', 'WE')
                        ->where('b.booking_status', 1) 
                        ->where('d.service_status', 2);
                if($date_from && $date_to)
                {
                    $this->db->where('d.service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
                }
                else if($date_from || $date_to)
                {
                    $date = $date_from ? $date_from : $date_to;
                    $this->db->where('d.service_date', $date);
                }
                else
                {
                    $this->db->where('d.service_date', $date_from);
                }
                if(count($deleted_bookings) > 0)
                {
                        $this->db->where_not_in('d.booking_id', $deleted_bookings);
                }                
                
            }
            

            $get_total_bookings_qry = $this->db->get();
            
            
            return $get_total_bookings_qry->num_rows();
        }
		
	function get_total_cancellation($date_from = NULL, $date_to = NULL)
	{
		//$DB2 = $this->load->database('anotherdb', TRUE);
		$this->db->select('booking_id')
			->from('booking_deletes');
			
		if($date_from && $date_to)
		{
			$this->db->where('service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else
		{
			$date = $date_from ? $date_from : $date_to;
			
			$this->db->where('service_date', $date);
		}
		$get_booking_deletes_by_date_qry = $this->db->get();             
		
		
		return $get_booking_deletes_by_date_qry->num_rows();
	}
	
	function get_total_invoice_amount($date_from = NULL, $date_to = NULL) 
        {
			//$DB2 = $this->load->database('anotherdb', TRUE);
            $service_week_day = date('w', strtotime($date_from));
		
            $deletes = $this->get_booking_deletes_by_date($date_from);

            $deleted_bookings = array();
            foreach($deletes as $delete)
            {
                    $deleted_bookings[] = $delete->booking_id;
            }
            
            $this->db->select("i.day_service_id", FALSE)
                    ->from('day_services d')
                    ->join('invoice i', 'd.day_service_id=i.day_service_id')                     
                    ->where('d.service_status', 2);
            if($date_from && $date_to)
            {
                $this->db->where('d.service_date BETWEEN "'. $date_from. '" and "'. $date_to.'"');
            }
            else if($date_from || $date_to)
            {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('d.service_date', $date);
            }
            else
            {
                $this->db->where('d.service_date', $date_from);
            }
            if(count($deleted_bookings) > 0)
            {
                    $this->db->where_not_in('d.booking_id', $deleted_bookings);
            }

            $get_total_invoice_amount_qry = $this->db->get();
            
            
            return $get_total_invoice_amount_qry->num_rows();
        }
		
	function get_day_collection($date_from = NULL, $date_to = NULL)
        {
			//$DB2 = $this->load->database('anotherdb', TRUE);
            $this->db->select('SUM(paid_amount) AS day_collection', FALSE)
				->from('customer_payments');
				
            if($date_from && $date_to)
            {
                $this->db->where('DATE(paid_datetime) BETWEEN "'. $date_from. '" and "'. $date_to.'"');
            }
            else
            {
                $date = $date_from ? $date_from : $date_to;
                
                $this->db->where('DATE(paid_datetime)', $date);
            }
            
            $this->db->limit(1);
            
            $get_day_collection_qry = $this->db->get();             
            
            
            $payments = $get_day_collection_qry->row();
            
            return isset($payments->day_collection) ? $payments->day_collection : 0;
        }
		
	function get_total_pending_amount($date_from = NULL, $date_to = NULL) 
        {
			//$DB2 = $this->load->database('anotherdb', TRUE);
            $service_week_day = date('w', strtotime($date_from));
		
            $deletes = $this->get_booking_deletes_by_date($date_from);

            $deleted_bookings = array();
            foreach($deletes as $delete)
            {
                    $deleted_bookings[] = $delete->booking_id;
            }
            
            $this->db->select("SUM(d.total_fee) AS pending_amount", FALSE)
                    ->from('day_services d')
                    ->join('invoice i', 'd.day_service_id=i.day_service_id')                     
                    ->where('d.service_status', 2)
                    ->where('i.invoice_status', 0);
            if($date_from && $date_to)
            {
                $this->db->where('DATE(i.added) BETWEEN "'. $date_from. '" and "'. $date_to.'"');
            }
            else if($date_from || $date_to)
            {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('DATE(i.added)', $date);
            }
            else
            {
                $this->db->where('DATE(i.added)', $date_from);
            }
            if(count($deleted_bookings) > 0)
            {
                    $this->db->where_not_in('d.booking_id', $deleted_bookings);
            }
            
            $this->db->limit(1);
            
            $get_total_pending_amount_qry = $this->db->get();
            
            
            $payments = $get_total_pending_amount_qry->row();
            
            return isset($payments->pending_amount) ? $payments->pending_amount : 0;
        }
		
	function get_new_customer($date_from = NULL, $date_to = NULL)
	{
		//$DB2 = $this->load->database('anotherdb', TRUE);
		$this->db->select('customer_id')
			->from('customers');
			
		if($date_from && $date_to)
		{
			$this->db->where('DATE(customer_added_datetime) BETWEEN "'. $date_from. '" and "'. $date_to.'"');
		}
		else
		{
			$date = $date_from ? $date_from : $date_to;
			
			$this->db->where('DATE(customer_added_datetime)', $date);
		}            
		
		$get_new_customer_qry = $this->db->get();             
		
		
		return $get_new_customer_qry->num_rows();            
		
	}
	
	function change_password($old_password, $new_password) {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $fields['password'] = $new_password;
        $this->db->where('password', $old_password);
        $this->db->update('ipad_users', $fields);

        return $this->db->affected_rows();
    }
	
	function get_all_tablets($active_only = TRUE, $no_spare = TRUE) {
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $this->db->select('tablet_id, tablets.zone_id, zone_name, imei, google_reg_id, access_code, tablet_status')
                ->from('tablets')
                ->join('zones', 'tablets.zone_id = zones.zone_id')
                ->where('tablet_status', 1);

        $get_tab_by_qry = $this->db->get();

        return $get_tab_by_qry->result();
    }
	
	function get_tablet_locations() 
	{
		//$DB2 = $this->load->database('anotherdb', TRUE);
        $tablets = $this->get_all_tablets();

        $query = '';
        if (!empty($tablets)) {
            foreach ($tablets as $tab) {
                $query .= '( SELECT id, tablet_id, "' . $tab->zone_name . '" AS zone_name, `latitude` , `longitude` , `speed` , `added`  FROM `tablet_locations` WHERE tablet_id = ' . $tab->tablet_id . ' ORDER BY id DESC LIMIT 1 ) UNION ';
            }

            $query = rtrim($query, ' UNION');
        }

        /* $this->db->select('*')
          ->from('tablet_locations'); */

        $get_tablet_locations_qry = $this->db->query($query);

        return $get_tablet_locations_qry->result();
    }
	
	function getCustomerMobSearch($mobile) {
		$query = $this->db->query("SELECT c.customer_id AS custId, customer_name AS custName, email_address AS email, phone_number as phone, balance, signed, mobile_number_1 AS mobile1, mobile_number_2 AS mobile2, mobile_number_3 as mobile3,  a.area_name as area, ca.customer_address as address 
			FROM customers as c 
			LEFT JOIN customer_addresses as ca ON ca.customer_id = c.customer_id 
			LEFT JOIN areas as a ON ca.area_id = a.area_id 
			WHERE c.customer_status =1 AND ( phone_number = '$mobile' OR mobile_number_1 = '$mobile' OR mobile_number_2 = '$mobile' OR mobile_number_3 = '$mobile') LIMIT 1");
        $custList = $query->result();
		$respArr['custList'] = array();
		foreach ($custList as $detail)
		{
			$detail->url = 'http://software.spectrumservices.ae:8090/spectrum/customer/view/'. $detail->custId;
			$detail->pending_amount = $detail->balance. $detail->signed;
			array_push($respArr['custList'],$detail);
		}
		return $respArr['custList'];
		
    }
	
	/*Maid App */
	
	function get_maid_login($username,$password)
    {	
        $this->db->select("maid_id,maid_name,maid_photo_file,maid_login_status", FALSE)
                ->from('maids')
                ->where('username', $username)
                ->where('password', $password)
                ->limit(1);

        $get_maid_qry = $this->db->get();
        return $get_maid_qry->row();
    }
	
	function update_login_status($maid_id, $logged_status)
    {
        $fields['maid_login_status'] = $logged_status;
        $this->db->where('maid_id', $maid_id);
        $this->db->update('maids', $fields); 

        return $this->db->affected_rows();
    }
	
	function get_maid_detail_by_id($maid_id)
    {
        $this->db->select("maid_id,maid_name,maid_photo_file,maid_login_status", FALSE)
                ->from('maids')
                ->where('maid_id', $maid_id)
                ->limit(1);

        $get_maid_by_id_qry = $this->db->get();

        return $get_maid_by_id_qry->row();
    }
	
	function get_maid_job_details($service_date,$maid_id)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as ftime, b.time_to as ttime, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.payment_mode, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                ->join('users u', 'b.booked_by = u.user_id', 'left')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				//->order_by('m.team_id')
				->order_by('b.time_from');
if($maid_id != 0)
        {
            $this->db->where('m.maid_id',$maid_id);
        }
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		//echo $this->db->last_query();exit;
		
		return $get_schedule_by_date_qry->result();
	}
	
	function get_day_service_by_booking_id($date, $booking_id)
	{
		$this->db->select('ds.day_service_id, ds.service_added_by_id,cp.paid_amount, z.zone_id, b.maid_id, ds.booking_id, ds.total_fee, ds.service_status, ds.payment_status, c.customer_id, c.payment_type')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')	
				->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id','left')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
				->where('ds.booking_id', $booking_id)
				->where('ds.service_date', $date)
				->limit(1);
		
		$get_day_service_by_booking_id_qry = $this->db->get();
//	echo $this->db->last_query();exit;	
		return $get_day_service_by_booking_id_qry->row();
	}
	
	function get_booking_exist($booking_id,$service_date)
	{
		$this->db->select("just_mop_ref,mop", FALSE)
				->from('justmop_ref')
				->where('service_date',$service_date)
				->where('booking_id',$booking_id);
		$get_schedule_by_date_qry = $this->db->get();
		return $get_schedule_by_date_qry->row();
	}
	
	function update_booking_status($cancel_array, $reference_id)
	{
		$this->db->where('reference_id', $reference_id);
		$this->db->update('bookings', $cancel_array); 

		return $this->db->affected_rows();
	}

    function add_justmop_ref($fields = array())
    {
        $this->db->set($fields);
        $this->db->insert('justmop_ref'); 
        return $this->db->insert_id();
    }
	
	function add_call_detail($data) {
        $this->db->set($data);
        $this->db->insert('call_logger');
        $result = $this->db->insert_id();
        return $result;
    }
	
	function get_servicefee_details($servicetypeid)
    {
        $this->db->select("service_type_id, service_type_name, service_rate, material_incl")
                ->from('service_types')
                ->where('web_status',1)
                ->where('service_type_id',$servicetypeid);
        $get_fee_details_qry = $this->db->get();
        return $get_fee_details_qry->row(); 
    }
    
    
     
       
       
    
    
       
       
    
}
