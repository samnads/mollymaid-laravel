<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Customer_mollymaid_model extends CI_Model
{
    public function get_customer_sources()
    {
        $this->db->select('rs.refer_source_id,rs.refer_source')
            ->from('refer_sources as rs')
            ->where('rs.deleted_at', null)
            ->where('rs.for_customer', 1)
            ->order_by('rs.refer_source_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function residence_types()
    {
        $this->db->select('rt.residence_type_id,rt.residence_type')
            ->from('residence_types as rt')
            ->where('rt.deleted_at', null)
            ->order_by('rt.order_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_areas()
    {
        $this->db->select('a.area_id,a.area_name')
            ->from('areas as a')
            ->where('a.deleted_at', null)
            ->order_by('a.area_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_zones()
    {
        $this->db->select('z.zone_id,z.zone_name')
            ->from('zones as z')
            ->where('z.zone_status', 1)
            ->order_by('z.zone_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_genders()
    {
        $this->db->select('z.gender_id,z.gender')
            ->from('genders as z')
            ->where('z.deleted_at', null)
            ->order_by('z.gender_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_locations()
    {
        $this->db->select('l.location_id,l.location_name,l.area_id')
            ->from('locations as l')
            ->where('l.deleted_at', null)
            ->order_by('l.location_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_pref_maids()
    {
        $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_status', 1)
            ->where('m.employee_type_id', 1)
            ->order_by('m.maid_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_flaged_maids()
    {
        $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_status', 1)
            ->where('m.employee_type_id', 1)
            ->order_by('m.maid_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_payment_frequencies()
    {
        $this->db->select('pf.payment_frequency_id,pf.payment_frequency')
            ->from('payment_frequencies as pf')
            ->where('pf.deleted_at', null)
            ->order_by('pf.payment_frequency_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function payment_modes()
    {
        $this->db->select('pm.payment_mode_id,pm.payment_mode')
            ->from('payment_modes as pm')
            ->where('pm.deleted_at', null)
            ->order_by('pm.payment_mode_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_landmarks()
    {
        $this->db->select('lm.landmark_id,lm.landmark_name')
            ->from('landmarks as lm')
            ->where('lm.deleted_at', null)
            ->order_by('lm.landmark_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_country_emirates($country_id)
    {
        $this->db->select('ce.country_emirate_id,ce.country_id,ce.emirate')
            ->from('country_emirates as ce')
            ->where('ce.deleted_at', null)
            ->where('ce.country_id', $country_id)
            ->order_by('ce.country_emirate_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_customer_companies()
    {
        $this->db->select('cc.customer_company_id,cc.company_name')
            ->from('customer_companies as cc')
            ->where('cc.deleted_at', null)
            ->order_by('cc.sort_priority', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_customer_dealers()
    {
        $this->db->select('cd.customer_dealer_id,cd.dealer_name')
            ->from('customer_dealers as cd')
            ->where('cd.deleted_at', null)
            ->order_by('cd.sort_priority', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function save_customer($data)
    {
        $this->db->insert('customers', $data);
        return $this->db->insert_id();
    }
    public function save_preferred_maid($data)
    {
        $this->db->insert('customer_pref_maids', $data);
        return $this->db->insert_id();
    }

        public function save_flagged_maid($data)
    {
        $this->db->insert('flaged_maids_customers', $data);
        return $this->db->insert_id();
    }
    public function update_customer($id, $data)
    {
        $this->db->where('customer_id', $id);
        $this->db->update('customers', $data);
    }
    public function update_zones($id, $data)
    {
        $this->db->where('zone_id', $id);
        $this->db->update('zones', $data);
    }
    public function save_customer_address($data)
    {
        $this->db->insert('customer_addresses', $data);
        return $this->db->insert_id();
    }
    public function customer_list()
    {
        $this->db->select(
            '
        c.customer_id,
        c.customer_code,
        c.customer_name,
        c.customer_nick_name,
        c.email_address,
        c.customer_type_id,
        c.refer_source_id,
        c.payment_mode_id,
        c.payment_frequency_id,
        c.gender_id,
        ct.customer_type,
        pf.payment_frequency,
        g.gender,
        rs.refer_source,
        c.mobile_number_1,
        c.customer_status,
        def_ca_l.location_name as default_booking_address_location,
        allowed_new_booking_status.status_code as is_allowed_new_booking_status_code,
        allowed_new_booking_status.status_text as is_allowed_new_booking_status_text,
        a.area_id,a.area_name,z.zone_id,z.zone_name'
        )
            ->from('customers as c')
            ->join('customer_types as ct', 'c.customer_type_id = ct.customer_type_id', 'left')
            ->join('payment_modes as pm', 'c.payment_mode_id = pm.payment_mode_id', 'left')
            ->join('payment_frequencies as pf', 'c.payment_frequency_id = pf.payment_frequency_id', 'left')
            ->join('genders as g', 'c.gender_id = g.gender_id', 'left')
            ->join('refer_sources as rs', 'c.refer_source_id = rs.refer_source_id', 'left')
            ->join('customer_addresses as def_ca', 'c.default_booking_address = def_ca.customer_address_id', 'left')
            ->join('locations as def_ca_l', 'def_ca.location_id = def_ca_l.location_id', 'left')
            ->join('statuses as allowed_new_booking_status', 'c.is_allowed_new_booking = allowed_new_booking_status.status_code', 'left')
            ->join('areas as a', 'def_ca.area_id = a.area_id', 'left')
            ->join('zones as z', 'z.zone_id = a.zone_id', 'left')
            ->where('z.zone_status', 1)
            ->where('a.deleted_at', null)
            ->where('c.deleted_at', null)
            ->order_by('c.customer_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }


    public function get_filtered_customers($ctype, $ptype, $ztype, $atype, $ltype,$status)
    {
        $this->db->select(
            '
        c.customer_id,
        c.customer_code,
        c.customer_name,
        c.customer_nick_name,
        c.email_address,
        c.customer_type_id,
        c.refer_source_id,
        c.payment_mode_id,
        c.payment_frequency_id,
        c.gender_id,
        ct.customer_type,
        pf.payment_frequency,
        g.gender,
        rs.refer_source,
        c.mobile_number_1,
        def_ca_l.location_name as default_booking_address_location,
        def_ca_z.zone_name,
        def_ca_a.area_name'
        
        );
        $this->db->from('customers as c');
        $this->db->join('customer_types as ct', 'c.customer_type_id = ct.customer_type_id', 'left');
        $this->db->join('payment_modes as pm', 'c.payment_mode_id = pm.payment_mode_id', 'left');
        $this->db->join('payment_frequencies as pf', 'c.payment_frequency_id = pf.payment_frequency_id', 'left');
        $this->db->join('genders as g', 'c.gender_id = g.gender_id', 'left');
        $this->db->join('refer_sources as rs', 'c.refer_source_id = rs.refer_source_id', 'left');
        $this->db->join('customer_addresses as def_ca', 'c.default_booking_address = def_ca.customer_address_id', 'left');
        $this->db->join('locations as def_ca_l', 'def_ca.location_id = def_ca_l.location_id', 'left');
        $this->db->join('areas as def_ca_a', 'def_ca.area_id = def_ca_a.area_id', 'left');
        $this->db->join('zones as def_ca_z', 'def_ca_a.zone_id = def_ca_z.zone_id', 'left');


        if ($ctype == 1) {
            $this->db->where('c.customer_type_id', 1);
        } else if ($ctype == 2) {
            $this->db->where('c.customer_type_id', 2);
        } else if ($ctype == 3) {
            $this->db->where('c.customer_type_id', 3);
        }
        if ($status == 1) {
            $this->db->where('c.deleted_at', null);
        } elseif ($status == 2) {
            $this->db->where('c.deleted_at IS NOT NULL');
        }
        if ($ptype == 1) {
            $this->db->where('c.payment_mode_id', 1);
        } else if ($ptype == 2) {
            $this->db->where('c.payment_mode_id', 2);
        }
        if ($ztype !== '') {
            $this->db->where('def_ca_z.zone_id', $ztype);
        }
        if ($atype !== '') {
            $this->db->where('def_ca_a.area_id', $atype);
        }
        if ($ltype !== '') {
            $this->db->where('def_ca_l.location_id', $ltype);
        }

        $this->db->order_by('c.customer_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getZonesByArea($zoneId)
    {
        $this->db->where('zone_id', $zoneId);
        $query = $this->db->get('areas');
        return $query->result_array();
    }

    public function getLocationsByArea($areaId)
    {
        $this->db->where('area_id', $areaId);
        $query = $this->db->get('locations');
        return $query->result_array();
    }

    public function getLandmarkByLocation($locationId)
    {
        $this->db->where('location_id', $locationId);
        $query = $this->db->get('landmarks');
        return $query->result_array();
    }

    public function check_mob_number($mobile_number_1, $id, $form)
    {
        if ($form == 'new') {
            $this->db->where('mobile_number_1', $mobile_number_1);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            $this->db->where('mobile_number_1', $mobile_number_1);
            $this->db->where('customer_id !=', $id);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function check_whats_number($whatsapp_number, $id, $form)
    {
        if ($form == 'new') {
            $this->db->where('whatsapp_no_1', $whatsapp_number);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            $this->db->where('whatsapp_no_1', $whatsapp_number);
            $this->db->where('customer_id !=', $id);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function check_email($email, $id, $form)
    {
        if ($form == 'new') {
            $this->db->where('email_address', $email);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            $this->db->where('email_address', $email);
            $this->db->where('customer_id !=', $id);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function check_customer_code($customer_code, $form, $id)
    {
        log_message('error', 'ID' . $id);
        if ($form == 'new') {

            $this->db->where('customer_code', $customer_code);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            $this->db->where('customer_code', $customer_code);
            $this->db->where('customer_id !=', $id);
            $query = $this->db->get('customers');

            if ($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    // **************************************************
    public function delete_customer($customer_id) {

        $this->db->set('deleted_at', date('Y-m-d H:i:s'));
        $this->db->where('customer_id', $customer_id);
        $this->db->update('customers');

        return $this->db->affected_rows() > 0;
    }
}
