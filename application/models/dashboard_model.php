<?php if (!defined('BASEPATH')) {
    /*****************************************************/
    //             CODE By  :   Samnad . S               */
    //                                                   */
    //    Last Edit by      :                            */
    //                                                   */
    //                                                   */
    //                                                   */
    /*****************************************************/
    exit('No direct script access allowed');
}
class Dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$CI=&get_instance();
        //$CI->load->database('elite_maids');
    }
    public function booking_counts()
    {
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d')) ?: '[]';
        $this->db->select("count(b.booking_id) as total,
        ROUND(IFNULL(SUM(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600,0),2) AS total_hours,
        IFNULL(SUM(CASE WHEN c.is_company = 'Y' THEN 1 ELSE 0 END),0) as partner_total,
        ROUND(IFNULL(SUM(CASE WHEN c.is_company = 'Y' THEN (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from)) ELSE 0 END)/3600,0),2) as partner_total_hours", false)
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->where('b.booking_status', 1)
            ->where_not_in('b.booking_id', $today_deleted_booking_ids);
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->row_array();
    }
    public function customer_counts()
    {
        $this->db->select("count(c.customer_id) as total", false)
            ->from('customers as c');
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->row_array();
    }
    // public function cancel_bookings()
    // {
    //     $this->db->select("count(bc.booking_cancel_id) as total,
    //     IFNULL(SUM(CASE WHEN DATE(bc.service_date) = CURRENT_DATE() THEN 1 ELSE 0 END),0) as today_total,
    //     IFNULL(ROUND(SUM(CASE WHEN DATE(bc.service_date) = CURRENT_DATE() THEN TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from) ELSE 0 END)/3600,2),0) as today_hours", false)
    //         ->from('booking_cancel as bc')
    //         ->join('bookings as b', 'bc.booking_id = b.booking_id', 'left')
    //         ->where('b.booking_status', 1);
    //     $query = $this->db->get();
    //     //die($this->db->last_query());
    //     return $query->row_array();
    // }
    public function cancel_bookings()
    {
        $this->db->select("count(bc.booking_delete_id) as total,
        IFNULL(SUM(CASE WHEN DATE(bc.service_date) = CURRENT_DATE() THEN 1 ELSE 0 END),0) as today_total,
        IFNULL(ROUND(SUM(CASE WHEN DATE(bc.service_date) = CURRENT_DATE() THEN TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from) ELSE 0 END)/3600,2),0) as today_hours", false)
            ->from('booking_deletes as bc')
            ->join('bookings as b', 'bc.booking_id = b.booking_id', 'left')
            ->where('b.booking_status', 1);
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->row_array();
    }
    public function complaint_counts()
    {
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight", $previous_week);
        $end_week = strtotime("next saturday", $start_week);
        $start_week = date("Y-m-d", $start_week);
        $end_week = date("Y-m-d", $end_week);
        //die($last_week);
        $this->db->select("count(c.cmp_id) as total,
        IFNULL(SUM(CASE WHEN DATE(c.added_time) >= CURRENT_DATE() THEN 1 ELSE 0 END),0) as today_total,
        IFNULL(SUM(CASE WHEN DATE(c.added_time) >= '" . $start_week . "' AND DATE(c.added_time) <= '" . $end_week . "' THEN 1 ELSE 0 END),0) as last_week_total,
        IFNULL(SUM(CASE WHEN MONTH(c.added_time) = MONTH(CURRENT_DATE()) AND YEAR(c.added_time) = YEAR(CURRENT_DATE()) THEN 1 ELSE 0 END),0) as this_month_total", false)
            ->from('complaints_new as c');
        $query = $this->db->get();
        $result = $query->row_array();
        $result['last_week_date_range']['from'] = $start_week;
        $result['last_week_date_range']['to'] = $end_week;
        return $result;
    }
    public function payment_collected_and_pending()
    {
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight", $previous_week);
        $end_week = strtotime("next saturday", $start_week);
        $start_week = date("Y-m-d", $start_week);
        $end_week = date("Y-m-d", $end_week);
        $this->db->select("
        SUM(CASE WHEN CURRENT_DATE() BETWEEN b.service_start_date AND b.service_actual_end_date THEN b.total_amount ELSE 0 END) as today_total_amount,
        SUM(CASE WHEN DATE(cp.paid_datetime) = CURRENT_DATE() AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as today_collected_amount,
        SUM(CASE WHEN (DATE(b.service_actual_end_date) >= '" . $end_week . "') OR (DATE(b.service_actual_end_date) BETWEEN '" . $start_week . "' AND '" . $end_week . "') THEN b.total_amount ELSE 0 END) as weekly_total_amount,
        SUM(CASE WHEN (DATE(cp.paid_datetime) BETWEEN '" . $start_week . "' AND '" . $end_week . "') AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as weekly_collected_amount,
        SUM(CASE WHEN (DATE(b.service_actual_end_date) >= '" . date('Y-m-t') . "') OR (DATE(b.service_actual_end_date) BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "') THEN b.total_amount ELSE 0 END) as monthly_total_amount,
        SUM(CASE WHEN (DATE(cp.paid_datetime) BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "') AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as monthly_collected_amount,", false)
            ->from('day_services as ds')
            ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
            ->join('customer_payments as cp', 'ds.day_service_id = cp.day_service_id', 'left');
        $query = $this->db->get();
        $result = $query->row_array();
        $result['week_date_range']['from'] = $start_week;
        $result['week_date_range']['to'] = $end_week;
        $result['month_date_range']['from'] = date('Y-m-01');
        $result['month_date_range']['to'] = date('Y-m-t');
        return $result;
    }
    public function recent_activities($lastid = null, $fromid = null)
    {
        $this->db->select("ua.activity_id as id,
        ua.action_content as content,
        u.user_fullname as user,
         ua.action_type as action_type,
        ua.addeddate as time", false);
        $this->db->from('user_activity as ua')
            ->join('users as u', 'ua.added_user = u.user_id', 'left');
        if ($lastid != null) {
            $this->db->where('ua.activity_id >=', $fromid);
            $query = $this->db->order_by('activity_id', "desc")->get();
        } else {
            $query = $this->db->order_by('activity_id', "desc")->limit(20)->get();
        }
        $results = $query->result_array();
        foreach ($results as $key => $value) {
            $results[$key]['time'] = datetime_to_time_elapsed_string($results[$key]['time']);
            $results[$key]['action_type_icon'] = activity_icon($results[$key]['action_type']) ?: '<i class="icon-question"></i>';
        }
        return $results;
    }
    public function chart_counts()
    {
        /*************************************************************************** */
        // this day of week
        $service_date = date('Y-m-d');
        $week_day = date('w');
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d')) ?: '[]';
        $this->db->select("
        SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND b.booking_type = 'OD' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as today_od_count,
        SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND b.booking_type = 'WE' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as today_we_count,
        SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND b.booking_type = 'BW' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as today_bw_count", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result1 = $query->row_array();
        /*************************************************************************** */
        // same day prev week
        $service_date = date('Y-m-d', strtotime("-1 week"));
        $week_day = date('w', strtotime("-1 week"));
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d'), strtotime("-1 week")) ?: '[]';
        $this->db->select("
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'OD' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prev_od_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'WE' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prev_we_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'BW' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prev_bw_count", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result2 = $query->row_array();
        /*************************************************************************** */
        // same day prior week
        $service_date = date('Y-m-d', strtotime("-2 week"));
        $week_day = date('w', strtotime("-2 week"));
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d'), strtotime("-2 week")) ?: '[]';
        $this->db->select("
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'OD' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prior_od_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'WE' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prior_we_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'BW' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prior_bw_count", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result3 = $query->row_array();
        /*************************************************************************** */
        $result = (array) array_merge((array) $result1, (array) $result2, (array) $result3);
        return $result;
    }
    public function today_counts()
    {
        /*************************************************************************** */
        $service_date = date('Y-m-d');
        $week_day = date('w');
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $this->db->select("
        IFNULL(SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END),0) as total_bookings,
        ROUND(IFNULL(SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND cancel_date IS NULL AND b.booking_status = '1' THEN (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from)) ELSE 0 END)/3600,0),2)  as total_booking_hours", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result = $query->row_array();
        /*************************************************************************** */
        return $result;
    }
    public function monthly_counts_sql()
    {

        /*************************************************************************** */
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $this->db->select("
         IFNULL(SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN
         CASE
            WHEN (b.booking_type = 'OD')
                THEN 1
            WHEN (b.booking_type = 'WE')
                THEN
                CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_actual_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_actual_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0)
            WHEN (b.booking_type = 'BW')
                THEN ceil((CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0))/2)
         END


         ELSE 0 END),0) as total_bookings,

         IFNULL(ROUND(SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN
         CASE
            WHEN (b.booking_type = 'OD')
                THEN ((TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600)
            WHEN (b.booking_type = 'WE')
                THEN (CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0))*((TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600)
            WHEN (b.booking_type = 'BW')
                THEN (CEIL((CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0))/2))*((TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600)
         END


         ELSE 0 END),2),0) as total_booking_hours,



        SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN 1 ELSE 0 END) as total_bookings_old,
        ROUND(SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from)) ELSE 0 END)/3600)  as total_booking_hours_old", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids);
        $query = $this->db->get();
        $result = $query->row_array();
        /*************************************************************************** */
        return $result;
    }
    public function dayCount($from, $to, $day)
    {
        // count number of same week day between date range
        $from = new DateTime($from);
        $to = new DateTime($to);
        $wF = $from->format('w');
        $wT = $to->format('w');
        if ($wF < $wT) {
            $isExtraDay = $day >= $wF && $day <= $wT;
        } else if ($wF == $wT) {
            $isExtraDay = $wF == $day;
        } else {
            $isExtraDay = $day >= $wF || $day <= $wT;
        }
        return floor($from->diff($to)->days / 7) + $isExtraDay;
    }

    public function monthly_counts_php()
    {
        /*************************************************************************** */
        // get data from main table
        // this includes one day deleted data tooo
        // so we will exclude it in next section
        $this->db->select("b.booking_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('bookings as b')
            ->where("b.booking_status", 1)
            ->where("(b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR (b.service_end = 0 AND b.service_start_date < '" . date('Y-m-01') . "'))", null, false);
        $query = $this->db->get();
        $result = $query->result_array();
        /*************************************************************************** */
        // get one day deletes between this month
        // only filter not permanent deleted
        // join for hour calculation and exclude deleted
        $this->db->select("bd.booking_id,(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('booking_deletes as bd')
            ->join('bookings as b', 'b.booking_id = bd.booking_id', 'left')
            ->where("bd.service_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "'", null, false)
            ->where("b.booking_status", 1);
        $query = $this->db->get();
        $dayDeletes = $query->result_array();
        $dayDeleteHours = 0;
        $dayDeleteCount = 0;
        foreach ($dayDeletes as $key => $deleted) {
            $dayDeleteHours += $deleted['hours'];
            $dayDeleteCount += 1;
        }
        /*************************************************************************** */
        $counts = array();
        $counts['total_bookings'] = 0;
        $counts['total_booking_hours'] = 0;
        foreach ($result as $key => $booking) {
            // set end date for calculation
            if ($booking['service_end'] == 0) {
                $end_date = date('Y-m-t');
            } else {
                if ($booking['service_actual_end_date'] >= date('Y-m-t')) {
                    $end_date = date('Y-m-t');
                } else {
                    $end_date = $booking['service_actual_end_date'];
                }
            }
            // set start date for calculation
            if ($booking['service_start_date'] <= date('Y-m-01')) {
                $start_date = date('Y-m-01');
            } else {
                $start_date = $booking['service_start_date'];
            }
            if ($booking['booking_type'] == 'OD') {
                $booking_count = 1;
            } else if ($booking['booking_type'] == 'WE') {
                $booking_count = $this->dayCount($start_date, $end_date, $booking['service_week_day']);
            } else if ($booking['booking_type'] == 'BW') {
                $booking_count = ceil($this->dayCount($start_date, $end_date, $booking['service_week_day']) / 2); // .5 to next integer (eg. 2.5 to 3)
            }
            $counts['total_bookings'] += $booking_count;
            $counts['total_booking_hours'] += ($booking_count * $booking['hours']);
        }
        $counts['total_bookings'] = $counts['total_bookings'] - $dayDeleteCount;
        $counts['total_booking_hours'] = $counts['total_booking_hours'] - $dayDeleteHours;
        $counts['total_booking_hours'] = number_format((float) $counts['total_booking_hours'], 2, '.', '');
        return $counts;
    }
    public function get_booking_deletes_by_date($service_date)
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->where('bd.service_date', $service_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }
    public function test()
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->where('bd.service_date', $service_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }
}
