<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Day_services_model Class 
  * 
  * @package	Homemaid
  * @author     Geethu
  * @since      Version 1.0
  */
class Day_services_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function add_activity($booking, $user_id, $action_type, $amount = NULL,$zone=NULL)
        {
            
            if(!empty($booking))
            {
                $data = array();
                $data['added_user'] = $user_id;
                $data['booking_type'] = $booking->booking_type == 'OD' ? 'One Day' : 'Every Week';
                $data['shift'] = $booking->time_from . '-' . $booking->time_to;
                $data['action_type'] = $action_type;
                $data['addeddate'] = date('Y-m-d H:i:s');
                if($action_type == 'Customer_IN')
                {
                    $data['action_content'] = $booking->maid_name . ' is dropped to customer ' . $booking->customer_name . ' at ' . date('H:i:s');
                }
                if($action_type == 'Customer_OUT')
                {
                    $data['action_content'] = $booking->maid_name . ' is picked from customer ' . $booking->customer_name . ' at ' . date('H:i:s');
                }
                if($action_type == 'Payment')
                {
                    $data['action_content'] = 'Payment of amount ' . $amount . ' is received from customer ' . $booking->customer_name . ' at ' . date('H:i:s');
                }
                if($action_type == 'Maid_OUT')
                {
                    $data['action_content'] = $booking->maid_name . ' vehicle OUT attendance marked at  ' . date('H:i:s');
                }
                if($action_type == 'Service_Cancel')
                {
                    $data['action_content'] = $booking->customer_name  . ' service cancelled the job with maid ' . $booking->maid_name . '  at ' . date('H:i:s');
                }
                if($action_type == 'Transfer')
                {
                   
                    $data['action_content'] = $booking->customer_name  . ' service transfered to ' .$zone.' at '  . date('H:i:s');
                }
                
                $this->db->set($data);
		$this->db->insert('user_activity'); 
                
                return $this->db->insert_id();
            }
        }
	/** 
	 * Add day service
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	array
	 * @return	int
	 */
	function add_day_service($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('day_services'); 
		return $this->db->insert_id();
	}
	function delete_payment($payment_id)
        {
            $this->db->where('payment_id', $payment_id);
            $this->db->delete('customer_payments');
            
        }
	/** 
	 * Get day service by booking by id
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str, int
	 * @return	array
	 */
	function get_day_service_by_booking_id($date, $booking_id)
	{
		$this->db->select('ds.day_service_id, ds.day_service_reference_id,ds.service_added_by_id, ds.booking_id, ds.total_fee, ds.service_status, z.zone_id, b.maid_id, c.customer_id, c.payment_type, c.is_company, c.rating_mail,ds.invoice_status')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
                                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')				
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
				->where('ds.booking_id', $booking_id)
				->where('ds.service_date', $date)
				->limit(1);
		
		$get_day_service_by_booking_id_qry = $this->db->get();
//	echo $this->db->last_query();exit;	
		return $get_day_service_by_booking_id_qry->row();
	}
	
	
	function get_day_services_by_status($date, $status)
	{
		$this->db->select('ds.day_service_id, ds.booking_id, ds.service_status, b.customer_id, b.maid_id, b.time_from, b.time_to, b.booking_note, c.customer_name, c.mobile_number_1, c.payment_type, c.key_given, ca.customer_address, m.maid_name, m.maid_gender, m.maid_mobile_1, m.maid_nationality, m.maid_photo_file, a.area_id, a.area_name, z.zone_id, z.zone_name')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
				->where('ds.service_status', $status)
				->where('ds.service_date', $date)
				->order_by('b.time_from');
		
		$get_day_services_by_status_qry = $this->db->get();
//	echo $this->db->last_query();
                
		return $get_day_services_by_status_qry->result();
	}
        
        function get_schedule_progress_date_job($date, $status)
	{
		$this->db->select('ds.day_service_id, ds.booking_id, ds.service_status, ds.start_time, ds.end_time, b.customer_id, b.booking_type, b.maid_id, b.time_from, b.time_to, b.booking_note, c.customer_name, c.mobile_number_1,c.email_address, c.payment_type, c.key_given, ca.customer_address, ca.building, ca.unit_no, ca.street, m.maid_name, m.maid_gender, m.maid_mobile_1, m.maid_nationality, m.maid_photo_file, a.area_id, a.area_name, z.zone_id, z.zone_name,b.total_amount')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
				->where('ds.service_status', $status)
				->where('ds.service_date', $date)
				->order_by('b.time_from');
		
		$get_day_services_by_status_qry = $this->db->get();
//	echo $this->db->last_query();
                
		return $get_day_services_by_status_qry->result();
	}
	
	/** 
	 * Get day services by payment status
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	str, int
	 * @return	array
	 */
	function get_day_services_by_payment_status($date, $payment_status)
	{
		$this->db->select('ds.day_service_id, ds.booking_id, ds.service_status, b.customer_id, b.maid_id, b.time_from, b.time_to, b.booking_note, c.customer_name, c.mobile_number_1, c.payment_type, c.key_given, ca.customer_address, m.maid_name, m.maid_gender, m.maid_mobile_1, m.maid_nationality, m.maid_photo_file, a.area_id, a.area_name, z.zone_id, z.zone_name')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
				->where('ds.service_status', 2)
				->where('ds.payment_status', $payment_status)
				->where('ds.service_date', $date)
				->order_by('b.time_from');
		
		$get_day_services_by_payment_status_qry = $this->db->get();
//	echo $this->db->last_query();	
		return $get_day_services_by_payment_status_qry->result();
	}
	
	/** 
	 * Update day service
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_day_service($day_service_id, $fields = array())
	{
		$this->db->where('day_service_id', $day_service_id);
		$this->db->update('day_services', $fields); 

		return $this->db->affected_rows();
	}
        
        /** 
	 * Get payments
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	str, int
	 * @return	array
	 */
	function get_payments($tablet_id)
	{
		$this->db->select('1 AS payment_status, c.customer_id, c.customer_name, c.mobile_number_1, c.payment_type, c.key_given, ca.customer_address, a.area_id, a.area_name, z.zone_id, z.zone_name, cp.paid_amount', FALSE)
				->from('customers c')
				//->join('bookings b', 'ds.booking_id = b.booking_id')
				//->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                                ->join('customer_payments cp', 'cp.customer_id = c.customer_id')
				//->join('maids m', 'b.maid_id = m.maid_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				//->where('ds.service_status', 2)
                                //->where('ds.payment_status',1)
				->where('cp.paid_at', 'T')
                                ->where('cp.paid_at_id', $tablet_id)
                                ->where('DATE(cp.paid_datetime)',date('Y-m-d'))
				->order_by('cp.paid_datetime');
		
		$get_payments_qry = $this->db->get();
		
		return $get_payments_qry->result();
	}
        
        /** 
	 * Get payments
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	str, int
	 * @return	array
	 */
	function get_dayservice($date, $zone_id)
	{
                
		$this->db->select('ds.day_service_id, ds.booking_id, ds.service_status, ds.payment_status, b.customer_id, b.maid_id, b.time_from, b.time_to, b.booking_note,b.service_end_date,b.booking_type, b.service_week_day,b.service_end, c.customer_name, c.customer_nick_name, c.mobile_number_1, c.payment_type, c.key_given, ca.customer_address, m.maid_name, m.maid_gender, m.maid_mobile_1, m.maid_nationality, m.maid_photo_file, a.area_id, a.area_name, z.zone_id, z.zone_name, cp.paid_amount')
				->from('day_services ds')
				->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                                ->join('customer_payments cp', 'cp.customer_id = c.customer_id','left')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
                                //->where('ds.service_date', $date)
				//->where('ds.service_status', 2)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
				->order_by('b.time_from');
		if($date)
                {
                   $this->db->where('ds.service_date', $date); 
                }
                if($zone_id > 0)
                {
                    $this->db->where('z.zone_id', $zone_id); 
                }
		$get_payments_qry = $this->db->get();
		
		return $get_payments_qry->result();
	}
        /** 
	 * Get payments
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	str, int
	 * @return	array
	 */
	function get_payment_by_date($date, $zone_id)
	{
                
		$this->db->select('c.customer_name, c.customer_nick_name, z.zone_name, SUM(cp.paid_amount) AS paid_amount', FALSE)
				->from('customers c')
				//->join('bookings b', 'ds.booking_id = b.booking_id')
				//->join('customers c', 'b.customer_id = c.customer_id')
				->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                                ->join('customer_payments cp', 'cp.customer_id = c.customer_id','left')
				//->join('maids m', 'b.maid_id = m.maid_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                //->where('ds.service_date', $date)
				//->where('ds.service_status', 2)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
                                ->group_by('cp.customer_id')
				->order_by('cp.paid_datetime');
		if($date)
                {
                   $this->db->where('DATE(cp.paid_datetime)', $date); 
                }
                if($zone_id > 0)
                {
                    $this->db->where('z.zone_id', $zone_id); 
                }
		$get_payments_qry = $this->db->get();
		
		return $get_payments_qry->result();
	}
        /** 
	 * Get activity view
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	str, int
	 * @return	array
	 */
	function get_activity_view($date)
	{
                
		$this->db->select('day_services.day_service_id,day_services.customer_name,day_services.customer_address, day_services.customer_payment_type, day_services.total_fee, day_services.service_date, day_services.start_time, day_services.end_time, day_services.service_status, day_services.payment_status,zones.zone_name as transfered_zone, bookings.time_from,bookings.time_to, (SELECT zones.zone_name FROM zones JOIN areas ON zones.zone_id=areas.zone_id JOIN customer_addresses ON areas.area_id=customer_addresses.area_id WHERE customer_addresses.customer_id=bookings.customer_id) as actual_zone, customer_payments.paid_amount, maids.maid_name', FALSE)
				->from('day_services')
				->join('bookings', 'day_services.booking_id = bookings.booking_id')
				->join('booking_transfers', 'day_services.booking_id = booking_transfers.booking_id','left')
				->join('maids', 'day_services.maid_id = maids.maid_id','left')				
				->join('zones', 'booking_transfers.zone_id = zones.zone_id','left')
                                ->join('customer_payments', 'customer_payments.customer_id = day_services.customer_id','left')
								->where('b.booking_category', 'C')
                                ->where('day_services.service_date', $date)
                                ->group_by('bookings.booking_id')
				//->where('ds.service_status', 2)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
				->order_by('bookings.time_from');
		
		$get_activity_view_qry = $this->db->get();
		
                
                
		return $get_activity_view_qry->result();
	}
        /** 
	 * Get activity summary
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	date, date
	 * @return	array
	 */
	function get_activity_summary($date)
	{
                
		$this->db->select("(SUM(IF(bookings.booking_type = 'WE', TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from)), 0))/3600) AS weekend_hrs, (SUM(IF(bookings.booking_type = 'OD', TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from)), 0))/3600) AS oneday_hrs, SUM(IF (payment_status = 1, day_services.total_fee, 0)) AS paid_amount, (SELECT SUM(ds.total_fee) FROM invoice i JOIN day_services ds ON ds.day_service_id = i.day_service_id WHERE ds.service_date = '$date') AS invoice_amount", FALSE)
                        ->from('day_services')
                        ->join('bookings', 'day_services.booking_id = bookings.booking_id')
                        //->join('invoice', 'day_services.day_service_id = invoice.day_service_id', 'left')
						->where('b.booking_category', 'C')
                        ->where('day_services.service_status', 2)
                        ->where('day_services.service_date', $date)
                        ->limit(1);
		
		$get_activity_summary_qry = $this->db->get(); 
		
                //print_r($get_activity_summary_qry->row());
                
		return $get_activity_summary_qry->row();
	} 
        /** 
	 * Get day service
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	array
	 * @return	int
	 */
	function get_day_service($date)
	{
		$this->db->select('ds.day_service_id, ds.customer_id, ds.total_fee, ds.service_status, ds.payment_status')
				->from('day_services ds')				
				->where('ds.service_date', $date)
                                ->where('ds.service_status',2)
                                ->order_by('customer_id');
		
		$get_day_service_by_booking_id_qry = $this->db->get();
		
		return $get_day_service_by_booking_id_qry->result();
	}
        /** 
	 * Update batch day service
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_batch_day_service($fields = array())
	{
		$this->db->update_batch('day_services', $fields, 'day_service_id'); 
	}
        function update_day_service_by_invoice_ids($invoice_ids, $fields = array())
	{
                $this->db->select('GROUP_CONCAT(i.day_service_id SEPARATOR ",") AS day_service_id', FALSE)
                        ->from('day_services ds')
                        ->join('invoice i','i.day_service_id = ds.day_service_id')
                        ->where_in('i.invoice_id', $invoice_ids);
                
                $get_day_service_by_invoice_qry = $this->db->get();
		
		$day_service = $get_day_service_by_invoice_qry->row();
                
                $day_service_ids = explode(",", $day_service->day_service_id);
                
		$this->db->where_in('day_service_id', $day_service_ids);
		$this->db->update('day_services', $fields); 

		return $this->db->affected_rows();
	}
        function get_performance($maid_id, $date_from, $date_to)
        {
            $this->db->select('day_services.day_service_id,day_services.customer_name,day_services.customer_address, day_services.customer_payment_type, day_services.total_fee, DATE_FORMAT(day_services.service_date, "%d/%m/%Y") as service_date, day_services.start_time, day_services.end_time, day_services.service_status, day_services.payment_status,zones.zone_name as transfered_zone, bookings.time_from,bookings.time_to, (SELECT zones.zone_name FROM zones JOIN areas ON zones.zone_id=areas.zone_id JOIN customer_addresses ON areas.area_id=customer_addresses.area_id WHERE customer_addresses.customer_id=bookings.customer_id) as actual_zone, customer_payments.paid_amount, maids.maid_name', FALSE)
				->from('day_services')
				->join('bookings', 'day_services.booking_id = bookings.booking_id')
				->join('booking_transfers', 'day_services.booking_id = booking_transfers.booking_id','left')
				->join('maids', 'day_services.maid_id = maids.maid_id','left')				
				->join('zones', 'booking_transfers.zone_id = zones.zone_id','left')
                                ->join('customer_payments', 'customer_payments.customer_id = day_services.customer_id','left')    
								->where('bookings.booking_category', 'C')
                                //->where('day_services.service_date', $date)
                                ->group_by('bookings.booking_id')
				->where('day_services.service_status', 2)
                                ->where('day_services.maid_id', $maid_id)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
				->order_by('day_services.service_date');
            
            if($date_from && $date_to)
                $this->db->where('day_services.service_date BETWEEN "'. $date_from . '" AND "' . $date_to . '"');
            else if($date_from || $date_to)
            {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('day_services.service_date', $date);
            }
            
            $get_performance_qry = $this->db->get();

            return $get_performance_qry->result();
        }
        function get_report_of_the_day($date)
        {
            $this->db->select('a.area_charge, bookings.vaccum_cleaning, bookings.cleaning_materials, bookings.cleaning_chemicals, bookings.discount, day_services.day_service_id,day_services.customer_name,day_services.customer_address, day_services.customer_payment_type, day_services.total_fee, DATE_FORMAT(day_services.service_date, "%d/%m/%Y") as service_date, day_services.start_time, day_services.end_time, day_services.service_status, day_services.payment_status,zones.zone_name as transfered_zone, bookings.time_from,bookings.time_to, (SELECT zones.zone_name FROM zones JOIN areas ON zones.zone_id=areas.zone_id JOIN customer_addresses ON areas.area_id=customer_addresses.area_id WHERE customer_addresses.customer_id=bookings.customer_id) as actual_zone, customer_payments.paid_amount, maids.maid_name, service_types.service_rate, service_types.service_type_id', FALSE)
				->from('day_services')
				->join('bookings', 'day_services.booking_id = bookings.booking_id')
				->join('booking_transfers', 'day_services.booking_id = booking_transfers.booking_id','left')
				->join('maids', 'day_services.maid_id = maids.maid_id','left')				
				->join('zones', 'booking_transfers.zone_id = zones.zone_id','left')
                                ->join('customer_addresses ca', 'bookings.customer_id = ca.customer_id')                                
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                ->join('customer_payments', 'customer_payments.customer_id = day_services.customer_id','left')  
                                ->join('service_types', 'bookings.service_type_id = service_types.service_type_id')
								->where('bookings.booking_category', 'C')
                                ->where('day_services.service_date', $date)
                                ->group_by('bookings.booking_id')
				->where('day_services.service_status', 2)
                                //->where('day_services.maid_id', $maid_id)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
				->order_by('day_services.service_date');
            
            $get_report_of_the_day_qry = $this->db->get();

            return $get_report_of_the_day_qry->result();
        }
        function get_performance_by_zone($zone_id, $date_from, $date_to = NULL)
        {
            $this->db->select('SUM(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600) AS total_hrs', FALSE)
				->from('day_services')
				->join('bookings', 'day_services.booking_id = bookings.booking_id')
				//->join('booking_transfers', 'day_services.booking_id = booking_transfers.booking_id','left')
				//->join('maids', 'day_services.maid_id = maids.maid_id','left')				
				->join('customer_addresses ca', 'bookings.customer_id = ca.customer_id')                                
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                //->join('customer_payments', 'customer_payments.customer_id = day_services.customer_id','left')                                
                                //->where('day_services.service_date', $date)
                                //->group_by('bookings.booking_id')
								->where('bookings.booking_category', 'C')
				->where('day_services.service_status', 2)
                                ->where('z.zone_id', $zone_id)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
				->order_by('day_services.service_date');
            
            
            if($date_from && $date_to)
                $this->db->where('day_services.service_date BETWEEN "'. $date_from . '" AND "' . $date_to . '"');
            else if($date_from || $date_to)
            {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('day_services.service_date', $date);
            }
            $get_performance_qry = $this->db->get();
            //echo $this->db->last_query();exit;
            return $get_performance_qry->row();
        }
        function get_performance_by_company($date)
        {
            $this->db->select('SUM(TIME_TO_SEC(TIMEDIFF(bookings.time_to, bookings.time_from))/3600) AS total_hrs', FALSE)
				->from('day_services')
				->join('bookings', 'day_services.booking_id = bookings.booking_id')
				//->join('booking_transfers', 'day_services.booking_id = booking_transfers.booking_id','left')
				//->join('maids', 'day_services.maid_id = maids.maid_id','left')				
				//->join('customer_addresses ca', 'bookings.customer_id = ca.customer_id')                                
				//->join('areas a', 'ca.area_id = a.area_id')
				//->join('zones z', 'a.zone_id = z.zone_id')
                                //->join('customer_payments', 'customer_payments.customer_id = day_services.customer_id','left')                                
                                ->where('day_services.service_date', $date)
                                //->group_by('bookings.booking_id')
								->where('bookings.booking_category', 'C')
				->where('day_services.service_status', 2)
                                //->where('zones.zone_id', $zone_id)
				//->where('ds.service_added_by', 'T')
                                //->where('ds.service_added_by_id', $tablet_id)
				->order_by('day_services.service_date');
            
                                
            
            $get_performance_qry = $this->db->get();
            //echo $this->db->last_query();exit;
            return $get_performance_qry->row();
        }
        function get_company_income($date)
        {
            $this->db->select('SUM(paid_amount) AS total_amount', FALSE)
				->from('customer_payments')                               
                                ->where('DATE(paid_datetime)', $date);  
            
            $get_performance_qry = $this->db->get();
            //echo $this->db->last_query();exit;
            return $get_performance_qry->row();
        }
        function get_cancellation_by_zone($zone_id, $date_from, $date_to){
            $this->db->select('count(*) AS no_of_cancellations', FALSE)
				->from('booking_deletes bd')  
                                ->join('bookings b', 'bd.booking_id = b.booking_id')
                                ->join('customer_addresses ca', 'b.customer_id = ca.customer_id')                                
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                ->where('z.zone_id', $zone_id);
                                //->where('bd.service_date', $date_from);  
            if($date_from && $date_to)
                $this->db->where('bd.service_date BETWEEN "'. $date_from . '" AND "' . $date_to . '"');
            else if($date_from || $date_to)
            {
                $date = $date_from ? $date_from : $date_to;
                $this->db->where('bd.service_date', $date);
            }
            $get_performance_qry = $this->db->get();
            //echo $this->db->last_query();exit;
            return $get_performance_qry->row();
        }
        
        // New
        function get_booking_deletes_by_date($service_date)
	{
		$this->db->select('booking_id')
				->from('booking_deletes')
				->where('service_date', $service_date);
		
		$get_booking_deletes_by_date_qry = $this->db->get();
		
		return $get_booking_deletes_by_date_qry->result();
	}
        function get_activity_by_date($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, b.cleaning_material, c.customer_name, c.customer_nick_name, c.payment_type, c.price_hourly, c.balance, c.signed, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,c.odoo_customer_id,m.odoo_maid_id", FALSE)
				->from('day_services ds')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id','left')
				->join('areas a', 'ca.area_id = a.area_id','left')
				->join('zones z', 'a.zone_id = z.zone_id','left')
                                //->join('users u', 'b.booked_by = u.user_id', 'left')
				//->where('b.booking_status', 1)
                                 //->where('m.maid_status', 1)
                                  //->where('b.booking_status', 1)
                                  //->or_where('b.booking_status',2)
				//->where('a.deleted_at', null)
				//->where('z.zone_status', 1)
								->where('b.booking_category', 'C')
                                ->where('ds.service_date', $service_date)
                                ->where('ds.service_status', 2)
								//->where('ca.customer_address_id !="" OR ca.customer_address_id =""')
				//->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				//->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				 ->group_by('ds.booking_id')
                                 ->order_by('z.zone_id, b.time_from');
		
		if(count($deleted_bookings) > 0)
		{
			//$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
            /*    echo "<pre>";
               print_r($services);
			    echo "<pre>";*/
                
                $this->db->select("ds.booking_id, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no, ds.payment_status, ds.service_status", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        //->where('ds.booking_id', $service->booking_id)
                        ->where('ds.service_date', $service_date)
                        //->where('m.maid_status', 1)
                        ->where('ds.service_status', 2)
                        
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                if(!empty($transfer_zone))
                {
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
                         
                        if(!empty($transfer_zone) && $transfer !==FALSE)
                        {
                            
                            $service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            $service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->total_fee = $transfer->total_fee;
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;
                            $service->cleaning_material = $service->cleaning_material;

                        }
                        else
                       {
                           
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL;                        
                            $service->cleaning_material = $service->cleaning_material;
                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                }
                //echo '<pre>';print_r($schedule);exit();
//                $sort_zones = array();
//                foreach ($schedule as $row)
//                {
//                    foreach ($row as $key => $val)
//                    {
//                        $sort_zones[$key] = $val['zone_id'];
//                    }
//                }
                
                //array_multisort($sort_zones, SORT_ASC, $schedule);
               /*
                foreach ($services as $service)
                {
                    
                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $this->db->select("z.zone_id, z.zone_name, ds.total_fee, ds.payment_status, ds.service_status")
                                ->from('day_services ds')
                                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->where('ds.booking_id', $service->booking_id)
                                ->where('ds.service_date', $service_date)
                                ->limit(1);
                        $transfer_zone_qry = $this->db->get();
                        $transfer_zone = $transfer_zone_qry->row();                       

                        $service->zone_id = $transfer_zone->zone_id ? $transfer_zone->zone_id : $service->zone_id;
                        $service->zone_name = $transfer_zone->zone_name ? $transfer_zone->zone_name : $service->zone_name;
                        $service->total_fee = $transfer_zone->total_fee !== NULL ? $transfer_zone->total_fee : NULL;
                        $service->payment_status = $transfer_zone->payment_status !== NULL ? $transfer_zone->payment_status : NULL;
                        $service->service_status = $transfer_zone->service_status !== NULL ? $transfer_zone->service_status : NULL;                        
                        
                        array_push($schedule, $service);
                        
                }*/
               
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}
        function get_activity_by_date_plan($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, bt.zone_id as t_zone, b.total_amount, b.service_start_date, b.cleaning_material, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status,b.pay_by,c.customer_name, c.customer_nick_name, c.payment_type, c.payment_mode, c.price_hourly, c.balance, c.signed, m.maid_name, m.maid_status, m.maid_disabled_datetime, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,c.odoo_customer_id,m.odoo_maid_id,zt.zone_name as transferred_zone", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->join('booking_transfers bt', 'b.booking_id = bt.booking_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->join('zones zt', 'bt.zone_id = zt.zone_id', 'left')
				->where('b.booking_category', 'C')
				->where('b.booking_status', 1)
                                ->where('m.maid_status', 1)
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('z.zone_id, b.time_from')
				->group_by('b.booking_id');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
                
                $this->db->select("ds.booking_id, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no, ds.payment_status, ds.service_status,zt.zone_name as transferred_zone", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        //->where('ds.booking_id', $service->booking_id)
                        ->where('ds.service_date', $service_date)
                        ->join('zones zt', 'bt.zone_id = zt.zone_id', 'left')
                        //->where('m.maid_status', 1)
                        //->where('ds.service_status', 2)
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                //if(!empty($transfer_zone))
                //{
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
						
						if($service->t_zone != "")
						{
							$service->zone_id = $service->t_zone;
						} else {
							$service->zone_id = $service->zone_id;
						}

                        if(!empty($transfer_zone) && $transfer !== FALSE)
                        {

                            //$service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            //$service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            //$service->total_fee = $transfer->total_fee;
                            $service->total_fee = $service->total_amount;
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;                        
                            $service->cleaning_material = $service->cleaning_material;  
                            $service->transferred_zone= $service->transferred_zone;  
                        }
                        else
                        {

                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL; 
                            $service->cleaning_material = $service->cleaning_material;
                            $service->transferred_zone= $service->transferred_zone; 

                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                //}
                //echo '<pre>';print_r($schedule);exit();
                //vishnu
//                $sort_zones = array();
//                foreach ($schedule as $row)
//                {
//                    foreach ($row as $key => $val)
//                    {
//                        $sort_zones[$key] = $val['zone_id'];
//                    }
//                }
                //ends vishnu
                //array_multisort($sort_zones, SORT_ASC, $schedule);
                /*
                foreach ($services as $service)
                {
                    
                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $this->db->select("z.zone_id, z.zone_name, ds.total_fee, ds.payment_status, ds.service_status")
                                ->from('day_services ds')
                                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->where('ds.booking_id', $service->booking_id)
                                ->where('ds.service_date', $service_date)
                                ->limit(1);
                        $transfer_zone_qry = $this->db->get();
                        $transfer_zone = $transfer_zone_qry->row();                       

                        $service->zone_id = $transfer_zone->zone_id ? $transfer_zone->zone_id : $service->zone_id;
                        $service->zone_name = $transfer_zone->zone_name ? $transfer_zone->zone_name : $service->zone_name;
                        $service->total_fee = $transfer_zone->total_fee !== NULL ? $transfer_zone->total_fee : NULL;
                        $service->payment_status = $transfer_zone->payment_status !== NULL ? $transfer_zone->payment_status : NULL;
                        $service->service_status = $transfer_zone->service_status !== NULL ? $transfer_zone->service_status : NULL;                        
                        
                        array_push($schedule, $service);
                        
                }
                */
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}

	function get_activity_by_date_plan_new($service_date, $filter_zone_type = NULL, $filter_maid_id = NULL, $searchterm = NULL)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id,ds.day_service_id,ds.day_service_reference_id,b.reference_id,b.booked_datetime, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, bt.zone_id as t_zone, b.total_amount, b.service_start_date, b.cleaning_material, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status,b.pay_by,c.customer_name, c.customer_nick_name, c.payment_type, c.payment_mode, c.price_hourly, c.balance, c.signed, m.maid_name, m.maid_status, m.maid_disabled_datetime, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,c.odoo_customer_id,m.odoo_maid_id,zt.zone_name as transferred_zone,cp.payment_id,cp.verified_status,cp.payment_method,ds.invoice_status,ds._total_amount, ds.dispatch_status", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->join('booking_transfers bt', 'b.booking_id = bt.booking_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->join('zones zt', 'bt.zone_id = zt.zone_id', 'left')
                                ->join('day_services ds', 'b.booking_id = ds.booking_id AND ds.service_date = '. $this->db->escape($service_date), 'left')
                                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id','left')
				->where('b.booking_category', 'C')
				->where('b.booking_status', 1)
                //                ->where('m.maid_status', 1)
				->where('a.deleted_at', null);
                                if ($filter_zone_type != NULL) {
                                  $this->db->where('z.zone_id', $filter_zone_type);
                                }
                                if ($filter_maid_id != NULL) {
                                        $this->db->where('m.maid_id', $filter_maid_id);
                                }
                                if ($searchterm  != NULL) {
                                        $this->db->where("(c.customer_name LIKE '%" . $searchterm . "%' OR ds.day_service_reference_id LIKE '%" . $searchterm . "%' OR m.maid_name LIKE '%" . $searchterm . "%' OR z.zone_name LIKE '%" . $searchterm . "%')");
                                }
                                $this->db->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('z.zone_id, b.time_from')
				->group_by('b.booking_id');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
                
                $this->db->select("ds.booking_id, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no, ds.payment_status, ds.service_status,zt.zone_name as transferred_zone", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        //->where('ds.booking_id', $service->booking_id)
                        ->where('ds.service_date', $service_date)
                        ->join('zones zt', 'bt.zone_id = zt.zone_id', 'left')
                        //->where('m.maid_status', 1)
                        //->where('ds.service_status', 2)
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                //if(!empty($transfer_zone))
                //{
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
						
						if($service->t_zone != "")
						{
							$service->zone_id = $service->t_zone;
						} else {
							$service->zone_id = $service->zone_id;
						}

                        if(!empty($transfer_zone) && $transfer !== FALSE)
                        {

                            //$service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            //$service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            //$service->total_fee = $transfer->total_fee;
                            if ($service->day_service_id != "") {
                                $service->total_fee = $service->_total_amount;
                            } else {
                                $service->total_fee = $service->total_amount;
                            }
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;                        
                            $service->cleaning_material = $service->cleaning_material;  
                            $service->transferred_zone= $service->transferred_zone;  
                        }
                        else
                        {

                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL; 
                            $service->cleaning_material = $service->cleaning_material;
                            $service->transferred_zone= $service->transferred_zone; 

                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                //}
                //echo '<pre>';print_r($schedule);exit();
                //vishnu
//                $sort_zones = array();
//                foreach ($schedule as $row)
//                {
//                    foreach ($row as $key => $val)
//                    {
//                        $sort_zones[$key] = $val['zone_id'];
//                    }
//                }
                //ends vishnu
                //array_multisort($sort_zones, SORT_ASC, $schedule);
                /*
                foreach ($services as $service)
                {
                    
                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $this->db->select("z.zone_id, z.zone_name, ds.total_fee, ds.payment_status, ds.service_status")
                                ->from('day_services ds')
                                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->where('ds.booking_id', $service->booking_id)
                                ->where('ds.service_date', $service_date)
                                ->limit(1);
                        $transfer_zone_qry = $this->db->get();
                        $transfer_zone = $transfer_zone_qry->row();                       

                        $service->zone_id = $transfer_zone->zone_id ? $transfer_zone->zone_id : $service->zone_id;
                        $service->zone_name = $transfer_zone->zone_name ? $transfer_zone->zone_name : $service->zone_name;
                        $service->total_fee = $transfer_zone->total_fee !== NULL ? $transfer_zone->total_fee : NULL;
                        $service->payment_status = $transfer_zone->payment_status !== NULL ? $transfer_zone->payment_status : NULL;
                        $service->service_status = $transfer_zone->service_status !== NULL ? $transfer_zone->service_status : NULL;                        
                        
                        array_push($schedule, $service);
                        
                }
                */
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}


        function get_activity_by_date_plan_job($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.cleaning_material, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, c.customer_name, c.customer_nick_name, c.payment_type, c.price_hourly, c.balance, c.signed, c.mobile_number_1, c.email_address, ca.customer_address, ca.building,ca.unit_no,ca.street, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,c.odoo_customer_id,m.odoo_maid_id,b.total_amount", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                //->join('users u', 'b.booked_by = u.user_id', 'left')
				->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
                                ->where('m.maid_status', 1)
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('z.zone_id, b.time_from');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
                
                $this->db->select("ds.booking_id, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS receipt_no, ds.payment_status, ds.service_status", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        //->where('ds.booking_id', $service->booking_id)
                        ->where('ds.service_date', $service_date)
                        //->where('m.maid_status', 1)
                        //->where('ds.service_status', 2)
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                //if(!empty($transfer_zone))
                //{
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);

                        if(!empty($transfer_zone) && $transfer !== FALSE)
                        {

                            //$service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            //$service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = $transfer->total_fee;
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;                        
                            $service->cleaning_material = $service->cleaning_material;   
                        }
                        else
                        {

                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL; 
                            $service->cleaning_material = $service->cleaning_material;

                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}
        function get_activity_by_date_plan_job_view($service_date,$booking_id)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.booking_note, b.cleaning_material, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, c.customer_name, c.customer_nick_name, c.payment_type, c.price_hourly, c.balance, c.signed, c.mobile_number_1, c.email_address, ca.customer_address, ca.building,ca.unit_no,ca.street, ca.latitude, ca.longitude, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,c.odoo_customer_id,m.odoo_maid_id, u.user_fullname", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
                                ->join('users u', 'b.booked_by = u.user_id', 'left')
				//->where('b.booking_status', 1)
				->where('b.booking_category', 'C')
                                ->where('b.booking_id', $booking_id)
                                ->where('m.maid_status', 1)
				->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('z.zone_id, b.time_from');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
                
                $this->db->select("ds.day_service_id, ds.booking_id, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, ds.start_time as starting_time, ds.end_time as ending_time, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no, ds.payment_status, ds.service_status", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        ->where('ds.booking_id', $booking_id)
                        ->where('ds.service_date', $service_date)
                        //->where('m.maid_status', 1)
                        //->where('ds.service_status', 2)
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                //if(!empty($transfer_zone))
                //{
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);

                        if(!empty($transfer_zone) && $transfer !== FALSE)
                        {

                            //$service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            //$service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = $transfer->total_fee;
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;                        
                            $service->cleaning_material = $service->cleaning_material; 
                            $service->starting_time = $transfer->starting_time;
                            $service->ending_time = $transfer->ending_time;
                            $service->day_service_id = $transfer->day_service_id;
                        }
                        else
                        {

                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL; 
                            $service->starting_time = NULL;
                            $service->ending_time = NULL;
                            $service->day_service_id = NULL;
                            $service->cleaning_material = $service->cleaning_material;

                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}
        function get_zone_activity_by_date($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
			
                $this->db->select("bookings.maid_id, bookings.booking_id, bookings.booking_type, customers.customer_id, customers.customer_name, maids.maid_name, DAYNAME(bookings.service_start_date) as day, areas.area_name, zones.zone_id, zones.zone_name, DATE_FORMAT(bookings.time_from, '%h:%i') AS time_from, DATE_FORMAT(bookings.time_to, '%h:%i') AS time_to, bookings.time_from AS start_time, bookings.time_to AS end_time", FALSE)
                ->from('bookings')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                //->join('customer_addresses', 'customers.customer_id = customer_addresses.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id')
                ->join('zones', 'areas.zone_id = zones.zone_id')
                ->where("((bookings.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0 AND bookings.service_start_date <= " . $this->db->escape($service_date) . "))", NULL, FALSE)
                ->where("((bookings.service_start_date = " . $this->db->escape($service_date) . " AND bookings.booking_type = 'OD') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND bookings.booking_type = 'WE') OR (bookings.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND bookings.booking_type = 'BW'))", NULL, FALSE)                
                ->where('bookings.booking_category', 'C')
				->where('bookings.booking_status', 1)
                ->where('maids.maid_status', 1)
                ->order_by('zones.zone_id, bookings.time_from');


                if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('bookings.booking_id', $deleted_bookings);
		}               
                
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();                          
                
                
                
                $schedule = array();
                foreach ($services as $service)
                {
                    array_push($schedule, $service);
                }
                
                
                $sort_zones = array();
                foreach ($schedule as $row)
                {
                    foreach ($row as $key => $val)
                    {
                        $sort_zones[$key] = $val['zone_id'];
                    }
                }
                
                //array_multisort($sort_zones, SORT_ASC, $schedule);
                /*
                foreach ($services as $service)
                {
                    
                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $this->db->select("z.zone_id, z.zone_name, ds.total_fee, ds.payment_status, ds.service_status")
                                ->from('day_services ds')
                                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->where('ds.booking_id', $service->booking_id)
                                ->where('ds.service_date', $service_date)
                                ->limit(1);
                        $transfer_zone_qry = $this->db->get();
                        $transfer_zone = $transfer_zone_qry->row();                       

                        $service->zone_id = $transfer_zone->zone_id ? $transfer_zone->zone_id : $service->zone_id;
                        $service->zone_name = $transfer_zone->zone_name ? $transfer_zone->zone_name : $service->zone_name;
                        $service->total_fee = $transfer_zone->total_fee !== NULL ? $transfer_zone->total_fee : NULL;
                        $service->payment_status = $transfer_zone->payment_status !== NULL ? $transfer_zone->payment_status : NULL;
                        $service->service_status = $transfer_zone->service_status !== NULL ? $transfer_zone->service_status : NULL;                        
                        
                        array_push($schedule, $service);
                        
                }
                */
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}
        function is_exist_booking($transfered_zones, $booking_id)
        {
            $return = FALSE;
            foreach ($transfered_zones as $transfer)
            {
                
                if($booking_id == $transfer->booking_id)
                {
                    $return = $transfer;
                    break;
                }
                else
                    $return = FALSE;
            }
            return $return;
        }
        function get_activity_by_date2($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
		
                //$this->db->select("b.booking_id, NULL AS actual_zone, NULL AS zone_name, NULL AS zone_id, b.customer_id, ds.total_fee, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, c.customer_name, c.customer_nick_name, c.payment_type, m.maid_name, ds.payment_status, ds.service_status", FALSE)
		$this->db->select("b.booking_id, IF(bt.zone_id IS NOT NULL, bt.zone_id, (SELECT zones.zone_id FROM zones JOIN areas ON zones.zone_id=areas.zone_id JOIN customer_addresses ON areas.area_id=customer_addresses.area_id WHERE customer_addresses.customer_id=b.customer_id LIMIT 1)) as zone_id, IF(bt.zone_id IS NOT NULL, (SELECT zone_name FROM zones WHERE zones.zone_id = bt.zone_id), (SELECT zone_name FROM zones JOIN areas ON zones.zone_id=areas.zone_id JOIN customer_addresses ON areas.area_id=customer_addresses.area_id WHERE customer_addresses.customer_id=b.customer_id LIMIT 1)) as zone_name, (SELECT CONCAT(zones.zone_id, '-', zones.zone_name) FROM zones JOIN areas ON zones.zone_id=areas.zone_id JOIN customer_addresses ON areas.area_id=customer_addresses.area_id WHERE customer_addresses.customer_id=b.customer_id LIMIT 1) as actual_zone, b.customer_id, ds.total_fee, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, c.customer_name, c.customer_nick_name, c.payment_type, m.maid_name, ds.payment_status, ds.service_status", FALSE)
				->from('bookings b')
                                ->join('day_services ds', 'b.booking_id = ds.booking_id AND ds.service_date = "' . $service_date . '"', 'left')
                                ->join('booking_transfers bt', 'b.booking_id = bt.booking_id AND bt.service_date = "' . $service_date . '"', 'left')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				//->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				//->join('areas a', 'ca.area_id = a.area_id')
				//->join('zones z', 'z.zone_id = bt.zone_id', 'left')
				->where('b.booking_category', 'C')
				->where('b.booking_status', 1)
				//->where('a.deleted_at', null)
				//->where('z.zone_status', 1)
                                
				->where("((b.service_actual_end_date >= '" . ($service_date) . "' AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = '" . ($service_date) . "' AND b.booking_type = 'OD') OR (b.service_start_date <= '" . ($service_date) . "' AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= '" . ($service_date) . "' AND MOD(DATEDIFF(DATE('" . ($service_date) . "'), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                                //->where('(IF(ds.service_date IS NULL ,1,IF(ds.service_date = "' . $service_date . '",1,0)))' , NULL, FALSE)
				->order_by('zone_id, b.time_from');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
                //echo $this->db->last_query();exit();
		/*$services = $get_schedule_by_date_qry->result();
                
                
                $schedule = array();
                
                foreach ($services as $service)
                {
                    
                        $this->db->select("z.zone_id, z.zone_name")
                                ->from('zones z')
                                ->join('areas a', 'z.zone_id = a.zone_id')
                                ->join('customer_addresses ca', 'a.area_id = ca.area_id')
                                ->where('ca.customer_address_id', $service->customer_address_id)
                                ->where('ca.customer_id', $service->customer_id)
                                ->limit(1);
                        $get_customer_zone_query = $this->db->get();
                        $customer_zone = $get_customer_zone_query->row();
                        if(isset($customer_zone->zone_id) && isset($customer_zone->zone_name))
                        {
                            $service->actual_zone = $customer_zone->zone_id . '-' . $customer_zone->zone_name;
                            
                        }
                        if($service->zone_id == NULL || $service->zone_id == '')
                        {
                            if(isset($customer_zone->zone_id) && isset($customer_zone->zone_name))
                            {

                                    $service->zone_id = $customer_zone->zone_id;
                                    $service->zone_name = $customer_zone->zone_name;

                            }
                        }
                        else
                        {
                            $this->db->select('z.zone_id, z.zone_id')
                                    ->from('zones z')
                                    ->where('z.zone_id', $service->zone_id)
                                    ->limit(1);
                            
                            $transfer_zone_qry = $this->db->get();
                            $transfer_zone = $transfer_zone_qry->row();
                            if(isset($transfer_zone->zone_id) && isset($transfer_zone->zone_name))
                            {

                                    $service->zone_id = $transfer_zone->zone_id;
                                    $service->zone_name = $transfer_zone->zone_name;

                            }
                        }
                        
                        array_push($schedule, $service);
                        
                }
                //echo '<pre>';print_r($schedule);exit();
                //usort($schedule,"my_sort");
                */
                
                return $get_schedule_by_date_qry->result();
	}
        function get_zones()
        {
            $this->db->select('zone_id, zone_name')
                    ->from('zones')
                    ->where('zone_status', 1);
            
            $get_zones_query = $this->db->get();
            
            return $get_zones_query->result();
        }
        function my_sort($a,$b)
        {    
            //print_r($a);exit;
                if ($a->zone_id == $b) return 0;
                   return ($a->zone_id < $b)?-1:1;                   
        }
        
        function get_payment_by_day_service_id($date, $day_service_id)
	{
		$this->db->select('cp.payment_id, cp.day_service_id, ds.day_service_reference_id, cp.customer_id, cp.paid_amount, cp.payment_method, cp.paid_at, cp.paid_at_id, cp.paid_datetime,cp.receipt_no,cp.verified_status')
				->from('customer_payments cp')
                                ->join('day_services ds', 'cp.day_service_id = ds.day_service_id')
				->where('cp.day_service_id', $day_service_id)
				->where('ds.service_date', $date)
				->limit(1);
		
		$get_payment_by_day_service_id_qry = $this->db->get();
		
		return $get_payment_by_day_service_id_qry->row();
	}
        function update_payment($payment_id, $fields = array())
	{
		$this->db->where('payment_id', $payment_id);
		$this->db->update('customer_payments', $fields); 

		return $this->db->affected_rows();
	}

        function update_maid_payment($payment_id, $fields = array())
	{
		$this->db->where('mp_id', $payment_id);
		$this->db->update('maid_payments', $fields); 

		return $this->db->affected_rows();
	}
        
        function get_payments_new($tablet_id)
	{
		$this->db->select('1 AS payment_status, c.customer_id, c.customer_name, c.mobile_number_1, c.payment_type, c.key_given, ca.customer_address, a.area_id, a.area_name, z.zone_id, z.zone_name, cp.paid_amount', FALSE)
			->from('customers c')
			->join('day_services ds', 'c.customer_id = ds.customer_id')
			->join('bookings b', 'ds.booking_id = b.booking_id')
			->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
			->join('customer_payments cp', 'cp.day_service_id = ds.day_service_id')
			->join('areas a', 'ca.area_id = a.area_id')
			->join('zones z', 'a.zone_id = z.zone_id')
			->where('b.booking_category', 'C')
			->where('cp.paid_at', 'T')
			->where('cp.paid_at_id', $tablet_id)
			->where('DATE(cp.paid_datetime)',date('Y-m-d'))
			->order_by('cp.paid_datetime');
		
		$get_payments_qry = $this->db->get();
//	echo $this->db->last_query();	
		return $get_payments_qry->result();
	}
        
    function get_activity_by_date_plan_job_invoice($service_date,$booking_id)
    {
        $this->db->select("ds.booking_id, ds.service_date, ds.day_service_id, ds.customer_id, c.customer_name, ca.customer_address, ca.latitude, ca.longitude, m.maid_name, c.balance, c.signed, c.mobile_number_1, c.email_address, u.user_fullname, b.customer_address_id, b.maid_id, b.booking_note, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, ds.total_fee, ds.material_fee, ds.start_time as starting_time, ds.end_time as ending_time, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no, ds.payment_status, ds.service_status", FALSE)
                ->from('day_services ds')
                ->join('bookings b','ds.booking_id = b.booking_id')
                ->join('customers c', 'ds.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('maids m', 'ds.maid_id = m.maid_id')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
				->where('b.booking_category', 'C')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
                //->where('m.maid_status', 1)
                //->where('ds.service_status', 2)
                //->order_by('bt.zone_id');
                //->limit(1);
        $transfer_zone_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $transfer_zone_qry->row();
    }
    
    public function get_the_previous_services($from_date,$customer_id)
    {
        $this->db->select('SUM(ds.total_fee) as totalfee,SUM(cp.outstanding_amt) as outfee,SUM(cp.paid_amount) as paidamount')
                ->from('day_services ds')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customer_payments cp','ds.day_service_id = cp.day_service_id','left')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')				
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
                ->where('ds.customer_id', $customer_id)
                ->where('ds.service_date <', $from_date);
		
        $get_day_service_by_booking_id_qry = $this->db->get();
//	echo $this->db->last_query();exit;	
        return $get_day_service_by_booking_id_qry->row();
    }
    
    public function get_service_list($date,$customer_id)
    {
        $this->db->select('c.customer_name,m.maid_name,ds.total_fee,cp.outstanding_amt,cp.paid_amount,DATE_FORMAT(b.time_from, "%h:%i %p") AS time_from, DATE_FORMAT(b.time_to, "%h:%i %p") AS time_to,ds.service_date,cp.paid_datetime', FALSE)
                ->from('day_services ds')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customer_payments cp','ds.day_service_id = cp.day_service_id','left')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m','ds.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')				
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
				->where('b.booking_category', 'C')
                ->where('ds.customer_id', $customer_id)
                ->where('ds.service_date', $date);
		
        $get_day_service_by_booking_id_qry = $this->db->get();
//	echo $this->db->last_query();exit;	
        return $get_day_service_by_booking_id_qry->result();
    }
    
    function get_activity_by_date_new_odoo($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));
		
        $deletes = $this->get_booking_deletes_by_date($service_date);
		
        $deleted_bookings = array();
        foreach($deletes as $delete)
        {
            $deleted_bookings[] = $delete->booking_id;
        }
				
        $this->db->select("b.booking_id, b.customer_id, b.customer_address_id,c.odoo_customer_id, b.maid_id, a.area_name, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h %p') AS time_from, DATE_FORMAT(b.time_to, '%h %p') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, b.cleaning_material, b.booking_note, c.customer_name, ca.customer_address, c.customer_nick_name, c.payment_type, c.price_hourly, c.customer_booktype, c.balance, c.signed, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS customer_address, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,m.odoo_maid_id,z.driver_name,z.zone_name", FALSE)
                ->from('day_services ds')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left')
                ->where('ds.service_date', $service_date)
                ->where('ds.service_status', 2)
                //->group_by('ds.booking_id')//
                ->order_by('z.zone_id, b.time_from');
		
        if(count($deleted_bookings) > 0)
        {
                //$this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
		
        $get_schedule_by_date_qry = $this->db->get();
		
		
        $services = $get_schedule_by_date_qry->result();
           
        $this->db->select("ds.booking_id,ds.day_service_id,ds.customer_address,ds.odoo_sync_status, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS receipt_no, ds.payment_status, ds.service_status,DATE(cp.paid_datetime) as paid_date", FALSE)
                ->from('day_services ds')
                //->join('maids m', 'ds.maid_id = m.maid_id')
                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                //->where('ds.booking_id', $service->booking_id)
                ->where('ds.service_date', $service_date)
                //->where('m.maid_status', 1)
                ->where('ds.service_status', 2)

                ->order_by('bt.zone_id');
                        //->limit(1);
        $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
        $transfer_zone = $transfer_zone_qry->result();  
              
        $schedule = array();
                //$i = 0;
        if(!empty($transfer_zone))
        {
            foreach ($services as $service)
            {
                $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
                         
                if(!empty($transfer_zone) && $transfer !==FALSE)
                {

                    $service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                    $service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                    $service->total_fee = $transfer->total_fee;
                    $service->material_fee = $transfer->material_fee;
                    $service->collected_amount = $transfer->collected_amount;
                    $service->receipt_no = $transfer->receipt_no;
                    $service->payment_status = $transfer->payment_status;
                    $service->service_status = $transfer->service_status;
                    $service->cleaning_material = $service->cleaning_material;
                    $service->paid_date = $transfer->paid_date;
                    $service->day_service_id = $transfer->day_service_id;
                    $service->customer_address = $transfer->customer_address;
                    $service->odoo_sync_status = $transfer->odoo_sync_status;
                }
                else
                {       
                    $service->zone_id = $service->zone_id;
                    $service->zone_name = $service->zone_name;
                    $service->total_fee = NULL;
                    $service->material_fee = NULL;
                    $service->collected_amount = NULL;
                    $service->receipt_no = NULL;
                    $service->payment_status = NULL;
                    $service->customer_address = NULL;
                    $service->service_status = NULL;                        
                    $service->cleaning_material = $service->cleaning_material;
                    $service->paid_date = NULL;
                    $service->day_service_id = NULL;
                    $service->odoo_sync_status = NULL;
                }

                array_push($schedule, $service);
                        //++$i;
            }
        }
        return $schedule;
    }
	
	function get_activity_by_date_newww_odoo($service_date)
    {
        $service_week_day = date('w', strtotime($service_date));
		
        $deletes = $this->get_booking_deletes_by_date($service_date);
		
        $deleted_bookings = array();
        foreach($deletes as $delete)
        {
            $deleted_bookings[] = $delete->booking_id;
        }
				
        $this->db->select("b.booking_id, ds.day_service_id, c.odoo_synch_status as syncstat, b.customer_id, b.total_amount, b.customer_address_id,c.odoo_customer_id, c.is_company, c.company_name, b.maid_id, a.area_name, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h %p') AS time_from, DATE_FORMAT(b.time_to, '%h %p') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, b.cleaning_material, b.booking_note, c.customer_name, ca.customer_address, c.customer_nick_name, c.payment_type, c.price_hourly, c.customer_booktype, c.balance, c.signed, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS customer_address, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,m.odoo_maid_id,z.driver_name,z.zone_name,c.payment_mode", FALSE)
                ->from('day_services ds')
                ->join('bookings b', 'ds.booking_id = b.booking_id')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id','left')
                ->join('areas a', 'ca.area_id = a.area_id','left')
                ->join('zones z', 'a.zone_id = z.zone_id','left')
				//newly added
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				//ends
				
                ->where('ds.service_date', $service_date)
                ->where('ds.service_status', 2)
				->where('b.booking_status', 1)
                //->group_by('ds.booking_id')//
                //->order_by('z.zone_id, b.time_from');
                ->order_by('ds.day_service_id');
		
        if(count($deleted_bookings) > 0)
        {
                $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }
		
        $get_schedule_by_date_qry = $this->db->get();
		
		
        $services = $get_schedule_by_date_qry->result();
           
        $this->db->select("ds.booking_id,ds.day_service_id,ds.customer_address,ds.odoo_sync_status, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS receipt_no, ds.payment_status, ds.service_status,DATE(cp.paid_datetime) as paid_date", FALSE)
                ->from('day_services ds')
                //->join('maids m', 'ds.maid_id = m.maid_id')
                ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                //->where('ds.booking_id', $service->booking_id)
                ->where('ds.service_date', $service_date)
                //->where('m.maid_status', 1)
                ->where('ds.service_status', 2)

                ->order_by('bt.zone_id');
                        //->limit(1);
        $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
        $transfer_zone = $transfer_zone_qry->result();  
              
        $schedule = array();
                //$i = 0;
        if(!empty($transfer_zone))
        {
            foreach ($services as $service)
            {
                $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
                         
                if(!empty($transfer_zone) && $transfer !==FALSE)
                {

                    $service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                    $service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                    //$service->total_fee = $transfer->total_fee;
                    $service->total_fee = $service->total_amount;
                    $service->material_fee = $transfer->material_fee;
                    $service->collected_amount = $transfer->collected_amount;
                    $service->receipt_no = $transfer->receipt_no;
                    $service->payment_status = $transfer->payment_status;
                    $service->service_status = $transfer->service_status;
                    $service->cleaning_material = $service->cleaning_material;
                    $service->paid_date = $transfer->paid_date;
                    $service->day_service_id = $transfer->day_service_id;
                    $service->customer_address = $transfer->customer_address;
                    $service->odoo_sync_status = $transfer->odoo_sync_status;
                }
                else
                {       
                    $service->zone_id = $service->zone_id;
                    $service->zone_name = $service->zone_name;
                    $service->total_fee = NULL;
                    $service->material_fee = NULL;
                    $service->collected_amount = NULL;
                    $service->receipt_no = NULL;
                    $service->payment_status = NULL;
                    $service->customer_address = NULL;
                    $service->service_status = NULL;                        
                    $service->cleaning_material = $service->cleaning_material;
                    $service->paid_date = NULL;
                    $service->day_service_id = NULL;
                    $service->odoo_sync_status = NULL;
                }

                array_push($schedule, $service);
                        //++$i;
            }
        }
        return $schedule;
    }
	
	function get_customers_odoo_new_id($customer_id)
    {
        $this->db->select('c.*,ca.customer_address,ca.area_id')
                ->from('customers c')
                ->join('customer_addresses ca', 'c.customer_id = ca.customer_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->group_by('ca.customer_id')
                ->order_by('c.customer_name', 'ASC');
        $this->db->where('c.customer_status', 1);
        $this->db->where('c.customer_id', $customer_id);
        $this->db->where('z.zone_status', 1);
        $this->db->where('a.deleted_at', null);
        
        //$this->db->where('odoo_synch_status', 0);
        
        $get_customers_qry = $this->db->get();
        //echo $this->db->last_query();exit;
        return $get_customers_qry->row();
    }
	
	function get_activity_by_date_new_odoo_common($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, ds.day_service_reference_id, b.booked_from, b.discount, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h %p') AS time_from, DATE_FORMAT(b.time_to, '%h %p') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status, b.cleaning_material, b.booking_note, c.customer_name, c.odoo_package_customer_id, c.odoo_package_customer_status, c.customer_nick_name, c.payment_type, c.price_hourly, c.company_name, b.price_per_hr, c.balance, c.signed, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,ca.customer_address,m.odoo_package_maid_id,a.odoo_package_area_id,z.odoo_package_zone_id", FALSE)
				->from('day_services ds')
                                ->join('bookings b', 'ds.booking_id = b.booking_id')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id','left')
				->join('areas a', 'ca.area_id = a.area_id','left')
				->join('zones z', 'a.zone_id = z.zone_id','left')
				->where('ds.service_date', $service_date)
				//->where('ds.payment_status', 1)
				//->where('c.payment_type', 'D')
				->where('ds.service_status', 2)
				->where('ds.odoo_package_activity_status', 0)
				->group_by('ds.booking_id')
				->order_by('z.zone_id, b.time_from');
				//->limit(1);
		
		if(count($deleted_bookings) > 0)
		{
			//$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
            /*    echo "<pre>";
               print_r($services);
			    echo "<pre>";*/
                
                $this->db->select("ds.booking_id, ds.customer_address as delvry_addr,ds.day_service_id,ds.odoo_package_activity_status, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no, cp.payment_method, ds.payment_status, ds.service_status,DATE(cp.paid_datetime) as paid_date", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        //->where('ds.booking_id', $service->booking_id)
                        ->where('ds.service_date', $service_date)
                        //->where('m.maid_status', 1)
                        ->where('ds.service_status', 2)
						->where('ds.odoo_package_activity_status', 0)
                        
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                if(!empty($transfer_zone))
                {
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
                         
                        if(!empty($transfer_zone) && $transfer !==FALSE)
                        {
                            
                            $service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            $service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->total_fee = $transfer->total_fee;
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;
                            $service->cleaning_material = $service->cleaning_material;
                            $service->paid_date = $transfer->paid_date;
                            $service->day_service_id = $transfer->day_service_id;
                            $service->odoo_new_sync_status = $transfer->odoo_package_activity_status;
                            $service->delvry_addr = $transfer->delvry_addr;
                            $service->payment_method = $transfer->payment_method;
                        }
                        else
                       {
                           
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL;                        
                            $service->cleaning_material = $service->cleaning_material;
                            $service->paid_date = NULL;
                            $service->day_service_id = NULL;
                            $service->odoo_new_sync_status = NULL;
                            $service->delvry_addr = NULL;
                            $service->payment_method = 'cash';
                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                }
               
                //echo '<pre>';print_r($schedule);exit();
                return $schedule;
	}
        function getservicedetails($service_id)
	{
		$this->db->select("ds.customer_id, ds.booking_id, ds.day_service_id, ds.customer_name, ds.customer_address, ds.service_date, ds.serviceamount, ds.vatamount, ds.total_fee, ds.booking_service_id, ds.material_fee, ds.maid_id, m.maid_name, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as start_time, b.time_to as end_time, c.price_hourly", FALSE)
			   ->from('day_services ds')
			   ->join('bookings b','ds.booking_id = b.booking_id')
			   ->join('customers c', 'b.customer_id = c.customer_id')
			   ->join('maids m', 'ds.maid_id = m.maid_id')
			   ->where('ds.service_status',2)
			   ->where('ds.invoice_status',0)
			   ->where('ds.day_service_id',$service_id);
		$get_service_qry = $this->db->get();
        return $get_service_qry->row();
	}
        function get_related_bookings_for_invoice($customer_id,$service_date,$ds_id)
	{
		$this->db->select("ds.customer_id, ds.booking_id, ds.day_service_id, ds.customer_name, ds.customer_address, ds.service_date, ds.serviceamount, ds.vatamount, ds.total_fee, ds.booking_service_id, ds.material_fee, ds.maid_id, m.maid_name, DATE_FORMAT(b.time_from, '%h:%i %p') AS time_from, DATE_FORMAT(b.time_to, '%h:%i %p') AS time_to, b.time_from as start_time, b.time_to as end_time, c.price_hourly", FALSE)
			   ->from('day_services ds')
			   ->join('bookings b','ds.booking_id = b.booking_id')
			   ->join('customers c', 'b.customer_id = c.customer_id')
			   ->join('maids m', 'ds.maid_id = m.maid_id')
			   ->where('ds.service_status',2)
			   ->where('ds.invoice_status',0)
			   ->where('ds.customer_id',$customer_id)
			   ->where('ds.service_date',$service_date)
			   ->where('ds.day_service_id !=',$ds_id);
		$get_service_qry = $this->db->get();
        return $get_service_qry->result();
	}
        function add_main_invoice($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('invoice'); 
		return $this->db->insert_id();
	}
        function add_line_invoice($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('invoice_line_items'); 
		return $this->db->insert_id();
	}
        function update_day_services_where($where,$data)
        {
                $this->db->where($where);
                $this->db->update('day_services', $data);
        }
        function update_service_invoice($invoice_id, $fields = array())
	{
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $fields); 

		return $this->db->affected_rows();
	}
        function payment_details($date){
                return $this->db->select('ds.customer_name,ds.customer_address,cp.paid_amount,m.maid_name')
                ->from('day_services as ds')
                ->join('customer_payments as cp','cp.day_service_id=ds.day_service_id','left')
                ->join('maids as m','m.maid_id=ds.maid_id','left')
                ->where(['ds.service_date'=>$date,'ds.payment_status'=>1])
                ->get()
                ->result_array();
        }
        function get_activity_by_date_plan_past($service_date)
	{
		$service_week_day = date('w', strtotime($service_date));
		
		$deletes = $this->get_booking_deletes_by_date($service_date);
		
		$deleted_bookings = array();
		foreach($deletes as $delete)
		{
			$deleted_bookings[] = $delete->booking_id;
		}
				
		$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, bt.zone_id as t_zone, b.total_amount, b.service_start_date, b.cleaning_material, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%h:%i') AS time_from, DATE_FORMAT(b.time_to, '%h:%i') AS time_to, b.time_from AS start_time, b.time_to AS end_time, b.booking_type, b.booking_status,b.pay_by,c.customer_name, c.customer_nick_name, c.payment_type, c.payment_mode, c.price_hourly, c.balance, c.signed, m.maid_name, z.zone_id, z.zone_name, NULL AS total_fee, NULL AS material_fee, NULL AS actual_zone, NULL AS payment_status, NULL AS service_status,c.odoo_customer_id,m.odoo_maid_id,zt.zone_name as transferred_zone", FALSE)
				->from('bookings b')
				->join('customers c', 'b.customer_id = c.customer_id')
				->join('maids m', 'b.maid_id = m.maid_id')
				->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
				->join('areas a', 'ca.area_id = a.area_id')
				->join('zones z', 'a.zone_id = z.zone_id')
				->join('booking_transfers bt', 'b.booking_id = bt.booking_id AND bt.service_date = "' . $service_date . '"', 'left')
                                ->join('zones zt', 'bt.zone_id = zt.zone_id', 'left')
				->where('b.booking_category', 'C')
				->where('b.booking_status', 1)
                                ->where('m.maid_status', 1)
				//->where('a.deleted_at', null)
				->where('z.zone_status', 1)
				->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
				->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
				->order_by('z.zone_id, b.time_from')
				->group_by('b.booking_id');
		
		if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('b.booking_id', $deleted_bookings);
		}
		
		$get_schedule_by_date_qry = $this->db->get();
		
		//echo $this->db->last_query();exit;
                $services = $get_schedule_by_date_qry->result();
                
                $this->db->select("ds.booking_id, ds.day_service_id, z.zone_id, z.zone_name, ds.total_fee, ds.material_fee, IF(cp.paid_amount > 0, cp.paid_amount, 0 ) AS collected_amount, IF(cp.receipt_no > 0, cp.receipt_no, '' ) AS ps_no,cp.verified_status,IF(cp.payment_id > 0, cp.payment_id, '' ) AS payment_id, ds.payment_status, ds.service_status, ds.invoice_status, zt.zone_name as transferred_zone,mp.mp_status,mp.mp_id", FALSE)
                        ->from('day_services ds')
                        //->join('maids m', 'ds.maid_id = m.maid_id')
                        ->join('customer_payments cp', 'ds.day_service_id = cp.day_service_id', 'left')
                        ->join('booking_transfers bt', 'ds.booking_id = bt.booking_id', 'left')
                        ->join('zones z', 'bt.zone_id = z.zone_id AND bt.service_date = "' . $service_date . '"', 'left')
                        ->join('maid_payments mp', 'mp.mp_day_ser_id = ds.day_service_id', 'left')
                        //->where('ds.booking_id', $service->booking_id)
                        ->where('ds.service_date', $service_date)
                        ->join('zones zt', 'bt.zone_id = zt.zone_id', 'left')
                        //->where('m.maid_status', 1)
                        //->where('ds.service_status', 2)
                        ->order_by('bt.zone_id');
                        //->limit(1);
                $transfer_zone_qry = $this->db->get();
                //echo $this->db->last_query();exit;
                $transfer_zone = $transfer_zone_qry->result();  
                
                
                
                $schedule = array();
                //$i = 0;
                //if(!empty($transfer_zone))
                //{
                    foreach ($services as $service)
                    {

                        $service->actual_zone = $service->zone_id . '-' . $service->zone_name;
                        $transfer = $this->is_exist_booking($transfer_zone, $service->booking_id);
						
						if($service->t_zone != "")
						{
							$service->zone_id = $service->t_zone;
						} else {
							$service->zone_id = $service->zone_id;
						}

                        if(!empty($transfer_zone) && $transfer !== FALSE)
                        {

                            //$service->zone_id = $transfer->zone_id ? $transfer->zone_id : $service->zone_id;
                            //$service->zone_name = $transfer->zone_name ? $transfer->zone_name : $service->zone_name;
                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            //$service->total_fee = $transfer->total_fee;
                            $service->total_fee = $service->total_amount;
                            $service->material_fee = $transfer->material_fee;
                            $service->collected_amount = $transfer->collected_amount;
                            $service->ps_no = $transfer->ps_no;
                            $service->verified_status = $transfer->verified_status;
                            $service->mp_status = $transfer->mp_status;
                            $service->mp_id = $transfer->mp_id;
                            $service->payment_id = $transfer->payment_id;
                            $service->payment_status = $transfer->payment_status;
                            $service->service_status = $transfer->service_status;                        
                            $service->invoice_status = $transfer->invoice_status;                        
                            $service->cleaning_material = $service->cleaning_material;  
                            $service->transferred_zone= $service->transferred_zone;  
                            $service->service_id= $transfer->day_service_id;  
                        }
                        else
                        {

                            $service->zone_id = $service->zone_id;
                            $service->zone_name = $service->zone_name;
                            $service->total_fee = NULL;
                            $service->material_fee = NULL;
                            $service->collected_amount = NULL;
                            $service->ps_no = NULL;
                            $service->verified_status = NULL;
                            $service->mp_status = NULL;
                            $service->mp_id = NULL;
                            $service->payment_id = NULL;
                            $service->payment_status = NULL;
                            $service->service_status = NULL; 
                            $service->invoice_status = NULL; 
                            $service->cleaning_material = $service->cleaning_material;
                            $service->transferred_zone= $service->transferred_zone; 
                            $service->service_id= NULL;

                        }

                        array_push($schedule, $service);
                        //++$i;
                    }
                
                return $schedule;
	}
        function get_day_services_where($where)
	{
                $this->db->select("ds.*", FALSE)
                ->from('day_services as ds')
                ->where($where);
                $query = $this->db->get();
                return $query->result();
	}
        function get_booking_deletes_by_date_where($where)
	{
                // $this->db->select("ds.*", FALSE)
                $this->db->select('bd.booking_id')
		->from('booking_deletes as bd')
                ->where($where);
                $query = $this->db->get();
                return $query->result();
	}
        
        public function maid_job_card_details($service_date, $booking_id, $maid_id, $day_service_id, $customer_id) {
                // print_r("helloo");die();
                $service_week_day = date('w', strtotime($service_date));
                $deleted_bookings = array();
                $deletes = $this->get_booking_deletes_by_date($service_date);

                if (!empty($deletes)) {

                        foreach ($deletes as $delete) {

                        $deleted_bookings[] = $delete->booking_id;
                        }
                }

                $this->db->select('
                bookings.maid_id, 
                bookings.booking_id, 
                bookings.booking_type, 
                day_services.day_service_id, 
                day_services.service_date, 
                day_services._total_amount, 
                day_services._vat_amount, 
                customers.customer_id, 
                customers.customer_name, 
                customers.customer_code, 
                maids.maid_name, 
                areas.area_name, 
                zones.zone_name,
                bookings.time_from AS time_from, 
                bookings.time_to AS time_to, 
                customer_addresses.customer_address, 
                customer_payments.paid_amount, 
                customer_payments.balance_amount, 
                customer_payments.receipt_no, 
                TIMESTAMPDIFF(MINUTE, day_services.time_from, day_services.time_to) AS working_minutes', false)
                ->from('day_services')
                ->join('bookings', 'day_services.booking_id = bookings.booking_id')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id', 'left')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id', 'left')
                ->join('zones', 'areas.zone_id = zones.zone_id', 'left')
                ->join('customer_payments', 'day_services.day_service_id = customer_payments.day_service_id', 'left')
                ->where('day_services.service_date', $service_date)
                ->where('day_services.day_service_id', $day_service_id)
                ->where('maids.maid_id', $maid_id)
                ->where('customers.customer_id', $customer_id)
                ->where('day_services.dispatch_status', 2);

                // ->group_by('day_services.booking_id')
                // ->order_by('maids.maid_name');
                if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('bookings.booking_id', $deleted_bookings);
		}
                $query = $this->db->get();
                // echo $this->db->last_query();die();
                return $query->result();
        }
        public function maid_payment_details($service_date, $booking_id, $maid_id, $day_service_id, $customer_id) {
                // print_r("helloo");die();
                $service_week_day = date('w', strtotime($service_date));
                $deleted_bookings = array();
                $deletes = $this->get_booking_deletes_by_date($service_date);

                if (!empty($deletes)) {

                        foreach ($deletes as $delete) {

                        $deleted_bookings[] = $delete->booking_id;
                        }
                }

                $this->db->select('
                bookings.maid_id, 
                bookings.booking_id, 
                bookings.booking_type, 
                day_services.day_service_id, 
                day_services.service_date, 
                day_services._total_amount, 
                day_services._vat_amount, 
                customers.customer_id, 
                customers.customer_name, 
                customers.customer_code, 
                maids.maid_name, 
                areas.area_name, 
                zones.zone_name,
                bookings.time_from AS time_from, 
                bookings.time_to AS time_to, 
                customer_addresses.customer_address, 
                customer_payments.paid_amount, 
                customer_payments.balance_amount, 
                customer_payments.receipt_no, 
                TIMESTAMPDIFF(MINUTE, day_services.time_from, day_services.time_to) AS working_minutes', false)
                ->from('day_services')
                ->join('bookings', 'day_services.booking_id = bookings.booking_id')
                ->join('customers', 'bookings.customer_id = customers.customer_id')
                ->join('customer_addresses', 'bookings.customer_address_id = customer_addresses.customer_address_id', 'left')
                ->join('maids', 'maids.maid_id = bookings.maid_id')
                ->join('areas', 'customer_addresses.area_id = areas.area_id', 'left')
                ->join('zones', 'areas.zone_id = zones.zone_id', 'left')
                ->join('customer_payments', 'day_services.day_service_id = customer_payments.day_service_id', 'left')
                // ->where('day_services.service_date', $service_date)
                ->where('day_services.service_date <= ' . $this->db->escape($service_date))
                // ->where('day_services.day_service_id', $day_service_id)
                ->where('day_services.maid_id', $maid_id)
                ->where('customers.customer_id', $customer_id)
                ->where('day_services.dispatch_status', 2)
                ->limit(5);
                // ->group_by('day_services.booking_id')
                // ->order_by('maids.maid_name');
                if(count($deleted_bookings) > 0)
		{
			$this->db->where_not_in('bookings.booking_id', $deleted_bookings);
		}
                $query = $this->db->get();
                // echo $this->db->last_query();die();
                return $query->result();
        }
        public function customer_keys($customer_id) {
                $this->db->select('
                cao.id,
                cao.customer_access_id,
                ca.code as customer_access_code,
                cat.name as customer_access_type
                ')
                    ->from('customer_access_owned as cao')
                    ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
                    ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
                    ->where('cao.customer_id', $customer_id)
                    ->where('cao.returned_at', null)
                    ->where('cao.return_added_by', null);
                $keys = $this->db->get();
                return $keys->result_array();
        }
}