<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Call_management_model extends CI_Model
{
    public function get_booking_types()
    {
        $this->db->select('b.booking_type,b.booking_type_name')
            ->from('booking_types as b')
            ->order_by('b.booking_type_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_zones()
    {
        $this->db->select('z.zone_id,z.zone_name')
            ->from('zones as z')
            ->where('z.zone_status',1)
            ->order_by('z.zone_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pref_maids()
    {
        $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_status',1)
            ->order_by('m.maid_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_areas()
    {
        $this->db->select('a.area_id,a.area_name')
            ->from('areas as a')
            ->where('a.deleted_at',null)
            ->order_by('a.area_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_call_list()
    {
        $this->db->select('ch.*,
        et.enquiry_type_id,et.enquiry_type,
        c.customer_name,c.customer_id,
        cn.status,
        ur.user_fullname,
        ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->join('complaints_new as cn', 'ch.call_history_id  = cn.call_history_id', 'left')
            ->where('ch.deleted_at', null)
            ->order_by('ch.call_history_id', 'DESC')
            ->limit('5');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_call_list_for_sel_customer($customer_id)
    {
        $this->db->select('ch.*,
        et.enquiry_type_id,et.enquiry_type,
        c.customer_name,c.customer_id,
        cn.status,
        ur.user_fullname,
        ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->join('complaints_new as cn', 'ch.call_history_id  = cn.call_history_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.customer_id', $customer_id)
            ->order_by('ch.call_history_id', 'DESC')
            ->limit('5');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getLocationsByArea($areaId) {
        $this->db->where('area_id', $areaId);
        $query = $this->db->get('locations');
        return $query->result_array();
    }

    public function customers_search($searchterm)
    {
        $this->db->select('c.customer_id,c.customer_name,c.customer_code,c.mobile_number_1,c.mobile_number_2')
            ->from('customers as c')
            ->order_by('c.customer_id', 'DESC');
        if (isset($searchterm) && strlen($searchterm) > 0) {
            $this->db->where("(customer_name LIKE '%" . $searchterm . "%' OR customer_nick_name LIKE '%" . $searchterm . "%' OR mobile_number_1 LIKE '%" . $searchterm . "%' OR mobile_number_2 LIKE '%" . $searchterm . "%' OR mobile_number_3 LIKE '%" . $searchterm . "%' OR phone_number LIKE '%" . $searchterm . "%' OR customer_code LIKE '%" . $searchterm . "%')", null, false);
        }
        $this->db->limit(50);
        $this->db->where('c.customer_status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function getLandmarkByLocation($locationId) {
        $this->db->where('location_id', $locationId);
        $query = $this->db->get('landmarks');
        return $query->result_array();
    }
    public function residence_types()
    {
        $this->db->select('rt.residence_type_id,rt.residence_type')
            ->from('residence_types as rt')
            ->where('rt.deleted_at',null)
            ->order_by('rt.order_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_customer_details($customer_id)
    {
        $this->db->select('*')
            ->from('customers')
            ->where('customers.customer_id', $customer_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_customer_address($cust_id)
    {
        // print_r("helloo");die();
        // $query = $this->db->query('select customer_addresses.customer_address_id, customer_addresses.building, customer_addresses.unit_no, customer_addresses.street, customer_addresses.customer_id, customer_addresses.area_id,customer_addresses.latitude,customer_addresses.longitude,customer_addresses.customer_address, customer_addresses.location_id, customer_addresses.landmark_id, areas.area_name, customers.customer_name, z.zone_name, loc.location_name, land.landmark_name
        // from customer_addresses
        // join areas ON customer_addresses.area_id = areas.area_id
        // join zones z ON areas.zone_id = z.zone_id
        // join locations loc ON areas.area_id = loc.area_id
        // join landmarks land ON loc.location_id = land.location_id
        // join customers ON customer_addresses.customer_id = customers.customer_id
        // where customer_addresses.address_status = 0 AND customer_addresses.customer_id = ' . $cust_id);
        // return $query->result_array();
        $this->db->select('ca.customer_address_id, ca.building,ca.unit_no, ca.street, ca.customer_id, ca.area_id,ca.latitude,ca.longitude,ca.customer_address, ca.location_id, ca.landmark_id, a.area_name, c.customer_name, z.zone_name, loc.location_name, land.landmark_name')
        ->from('customer_addresses as ca')
        ->join('areas a', 'ca.area_id = a.area_id')
            // ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('locations loc', 'ca.location_id = loc.location_id')
            ->join('landmarks land', 'ca.landmark_id = land.landmark_id')
            ->join('customers c', 'ca.customer_id = c.customer_id')
            ->where('ca.customer_id', $cust_id)
            ->where('ca.address_status', 1);

        $query = $this->db->get();
        return $query->result();
    }

    public function enquiry_types()
    {
        $this->db->select('et.enquiry_type_id,et.enquiry_type')
            ->from('enquiry_types as et')
            ->where('et.deleted_at',null)
            ->order_by('et.enquiry_type_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function save_service($data)
    {
        $this->db->insert('service_enquiry', $data);
        return $this->db->insert_id();
    }

    public function save_call_history($data)
    {
        $this->db->insert('call_history', $data);
        return $this->db->insert_id();
    }

    public function save_complaint($data)
    {
        $this->db->insert('complaints_new', $data);
        return $this->db->insert_id();
    }
    public function update_complaint($id, $data)
    {
        $this->db->where('cmp_id', $id);
        $this->db->update('complaints_new', $data);
    }
    public function save_preference($data)
    {
        $this->db->insert('preferences', $data);
        return $this->db->insert_id();
    }
    public function save_feedback($data)
    {
        $this->db->insert('feedback', $data);
        return $this->db->insert_id();
    }
    
    public function verifyMobilenumberExist($mobile_number_1) 
    {
        $this->db->select('customer_id');

        $this->db->where(array("mobile_number_1" => $mobile_number_1));

        $query=$this->db->get('customers');

        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function verifyEmailExist($email_address) 
    {
        $this->db->select('customer_id');

        $this->db->where(array("email_address" => $email_address));

        $query=$this->db->get('customers');

        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function complaint_types()
    {
        $this->db->select('ct.type_id,ct.type')
            ->from('complaint_types as ct')
            ->where('ct.deleted_at',null)
            ->order_by('ct.type_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function complaint_statuses()
    {
        $this->db->select('cs.complaint_status_id,cs.complaint_status')
            ->from('complaint_status as cs')
            ->where('cs.deleted_at',null)
            ->order_by('cs.complaint_status_id', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getEmployeedetails($employee_type_id) {
        if ($employee_type_id == 'C') {
            $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_status',1)
            ->order_by('m.maid_id', 'ASC');
            $query = $this->db->get();
            return $query->result_array();
        } else if ($employee_type_id == 'D') {
            $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_status', 1)
            ->where('m.employee_type_id', 2)
            ->order_by('m.maid_id', 'ASC'); 
            $query = $this->db->get();
            return $query->result_array();
        } else if ($employee_type_id == 'U'){
            $this->db->select('user_id, user_fullname')
            ->from('users')
            ->order_by('user_id', 'ASC');

             $query = $this->db->get();
            
            return $query->result();
        }
   
    }

    function get_schedule_by_date_avail($service_date)
    {
        // print_r($service_date);die();
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }

        $this->db->select("b.booking_id, b.justmop_reference, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, 'm.maid_name',b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes", FALSE)
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('a.area_status', 1)
            ->where('z.zone_status', 1)
            ->where('m.maid_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
                //->order_by('m.team_id')
            ->order_by('b.time_from');

        if (count($deleted_bookings) > 0) {
            $this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        return $get_schedule_by_date_qry->result();
    }

    function get_all_maid($filter = NULL)
    {

        if (isset($filter)) {
            $location_type  = $filter['location_type'];
            $value          = $filter['location_val'];
            $customer_id    = $filter['customer_id'];
            $area_id        = $filter['area_id'];
            $zone_id        = $filter['zone_id'];
            $province_id    = $filter['province_id'];
            $service_date   = $filter['service_date'];
            $from_time      = $filter['from_time'];
            $to_time        = $filter['to_time'];

            $service_week_day = date('w', strtotime($service_date));
            $time_from =  date('H:i:s', trim($from_time));
            $time_to = date('H:i:s', trim($to_time));
        }

        if ($location_type == 'area') {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where('a.area_id', $area_id);
        } else if ($location_type == 'zone') {
            $areaArray  = array();
            $subQuery   = "SELECT asq.area_id FROM `areas` as asq WHERE `zone_id` = $zone_id";
            $querySub   = $this->db->query($subQuery);
            foreach ($querySub->result_array() as $row) {
                $areaArray[] = $row['area_id'];
            }

            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            $this->db->join('zones as z', 'z.zone_id = a.zone_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where_in('a.area_id', $areaArray);
            //                 $this->db->where('z.zone_id', $value);
        } else if ($location_type == 'province') {
            $areaArray  = array();
            $subQuery   = "SELECT asq.area_id FROM `areas` as asq LEFT JOIN `zones` as zsq ON `zsq`.`zone_id` = `asq`.`zone_id` WHERE `zsq`.`province_id` = $province_id";
            $querySub   = $this->db->query($subQuery);
            foreach ($querySub->result_array() as $row) {
                $areaArray[] = $row['area_id'];
            }
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file,ca.area_id');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');
            $this->db->join('customer_addresses as ca', 'ca.customer_address_id = b.customer_address_id', 'LEFT');
            //$this->db->join('areas as a', 'ca.area_id = a.area_id','LEFT'); // habeeb replaced left join
            //$this->db->join('zones as z', 'z.zone_id = a.zone_id','LEFT'); // habeeb replaced left join
            //$this->db->join('province as p', 'p.province_id = z.province_id','LEFT'); // habeeb replaced left join
            $this->db->join('areas as a', 'ca.area_id = a.area_id');
            $this->db->join('zones as z', 'z.zone_id = a.zone_id');
            $this->db->join('province as p', 'p.province_id = z.province_id');
            //                $this->db->where("((b.service_start_date= ". $this->db->escape($service_date) . " AND b.booking_type = 'OD' AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to)) or (b.service_week_day = " . $service_week_day ." AND b.booking_type='WE' and b.service_end=0 AND ('".$time_to."' > b.time_from and '".$time_from."' < b.time_to )))", NULL, FALSE);
            $this->db->where_in('a.area_id', $areaArray);
            //                 $this->db->where('p.province_id', $value);
        } else {
            $this->db->select('m.maid_id, m.maid_name,m.maid_photo_file');
            $this->db->from('maids m');
            $this->db->join('bookings as b', 'm.maid_id = b.maid_id', 'LEFT');

        }
        $this->db->where('maid_status', 1);
        $this->db->group_by('m.maid_id');
        $this->db->order_by('maid_name', 'ASC');
        //  ->limit(10);


        $get_maids_leave_by_date_qry = $this->db->get();
        //echo $this->db->last_query();
        return $get_maids_leave_by_date_qry->result();
    }
    function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        if ($service_end_date != NULL) {
            $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
        } else {
            $this->db->where("service_date", $service_date);
        }


        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }

    function get_schedule_by_date_and_customer_new($customer_id, $service_date)
    {
        $service_week_day = date('w', strtotime($service_date));

        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach($deletes as $delete)
        {
               $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select("b.booking_id, b.customer_id, DAYNAME(b.service_start_date) AS shift_day, b.customer_address_id, b.maid_id, b.service_type_id, 
        DATE_FORMAT(b.service_start_date, '%d/%m/%Y') as service_date, 
        b.service_week_day, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, 
        DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, 
        b.service_end, b.service_end_date, b.service_actual_end_date, 
        b.booking_note, b.is_locked, b.pending_amount, b.discount, 
        b.booking_status, c.customer_name, c.customer_nick_name, 
        ds.day_service_id,
        ds.service_date as ds_service_date,
        ds.time_from as ds_time_from,
        ds.time_to as ds_time_to,
        ds.dispatch_status,
        ds.service_status as day_service_status,
        c.mobile_number_1, c.key_given, c.payment_type, c.longitude,
         c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address,
          m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,
          TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes,
          TIMESTAMPDIFF(MINUTE,ds.time_from ,ds.time_to) as ds_working_minutes,
          '$service_date' as scheduledates", FALSE)
            //$this->db->select("b.booking_id, b.customer_id, b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname", FALSE)                 
            ->from('bookings b')
            ->join('customers c', 'b.customer_id = c.customer_id')
            ->join('maids m', 'b.maid_id = m.maid_id')
             ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = "' . $service_date . '"','left')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->join('users u', 'b.booked_by = u.user_id', 'left')
            ->where('b.booking_category', 'C')
            ->where('b.booking_status', 1)
            // ->where("(b.booking_type='WE' OR b.booking_type='BW')", NULL, FALSE)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND service_week_day = " . $service_week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", NULL, FALSE)
            ->where('c.customer_id', $customer_id)
                //->order_by('m.maid_name')
                //->order_by('b.time_from');
            ->order_by('b.service_start_date', 'ASC');

        if(count($deleted_bookings) > 0)
        {
        	$this->db->where_not_in('b.booking_id', $deleted_bookings);
        }

        $get_schedule_by_date_qry = $this->db->get();
        //echo $this->db->last_query();exit;

        foreach ($get_schedule_by_date_qry as $key => $schedule) {
           
            if ($schedule->day_service_id != "")
            {
                $get_schedule_by_date_qry[$key]->time_from = $schedule->ds_time_from;
                $get_schedule_by_date_qry[$key]->time_to = $schedule->ds_time_to;
                $get_schedule_by_date_qry[$key]->working_minutes = $schedule->ds_working_minutes;
                
            }
        }
        return $get_schedule_by_date_qry->result();

    }
    // ******************************************************
    public function get_call_history_customer($customer_id)
    {
        $this->db->select('ch.*,
        et.enquiry_type_id,et.enquiry_type,
        c.customer_name,c.customer_id,c.customer_code,
        cn.status,cn.service_date,cn.complaint_type,cn.action_taken,cn.complaint_against_name,cn.complaint_against_id,cn.complaint_no,
        ur.user_fullname,
        ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->join('complaints_new as cn', 'ch.call_history_id  = cn.call_history_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.customer_id', $customer_id)
            ->order_by('ch.call_history_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    // ***********************************************
    public function get_call_history_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
        et.enquiry_type_id,et.enquiry_type,
        c.customer_name,c.customer_id,c.customer_code,
        cn.status,cn.service_date,cn.complaint_type,cn.action_taken,cn.complaint_against_name,cn.complaint_against_id,cn.complaint_no,
        ur.user_fullname,
        ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->join('complaints_new as cn', 'ch.call_history_id  = cn.call_history_id', 'left')
            ->where('ch.deleted_at', null)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // ************************************************************
    public function get_call_booking_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
        et.enquiry_type_id,et.enquiry_type,
        c.customer_name,c.customer_id,c.customer_code,
        b.booking_id,b.customer_id,b.customer_address_id,b.maid_id,b.service_week_day,b.service_start_date,b.service_type_id,
        b.service_end,b.service_actual_end_date,b.booking_type,b.booked_datetime,b.time_from,b.time_to,
        b.pending_amount, b.discount, b.total_amount, b.booking_status,
        z.zone_id, z.zone_name, a.area_name,
        ca.customer_address,ca.area_id,
        s.service_type_name,
        m.maid_id,m.maid_name
           ')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('bookings as b', 'b.call_history_id  = ch.call_history_id', 'left')
            ->join('maids as m', 'm.maid_id  = b.maid_id', 'left')
            ->join('service_types as s', 's.service_type_id  = b.service_type_id', 'left')
            ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
            ->join('areas a', 'ca.area_id = a.area_id')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where('ch.deleted_at', null)
            ->where('b.booking_from', 'CB')
            ->where('b.deleted_at', null)
            ->where('a.area_status', 1)
            ->where('z.zone_status', 1)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->where('ch.enquiry_type_id', 1)
            ->order_by('ch.call_history_id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    // ******************************************************************

    public function get_call_reactivation_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
        br.booking_re_activate_id,br.re_activate_date_from,br.re_activate_date_to,br.service_date,
        et.enquiry_type_id,et.enquiry_type,
        c.customer_name,c.customer_id,c.customer_code,
        b.customer_address_id,b.maid_id,b.service_week_day,b.service_type_id,
        b.pending_amount, b.discount, b.total_amount, b.booking_status,
        s.service_type_name,
        ur.user_fullname,ur.user_id')

            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('booking_re_activate as br', 'br.call_history_id  = ch.call_history_id', 'left')
            ->join('bookings as b', 'b.booking_id  = br.booking_id', 'left')
            ->join('service_types as s', 's.service_type_id  = b.service_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->group_by('br.booking_id')
            ->where('ch.deleted_at', null)
            ->where('ch.enquiry_type_id', 5)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
        $query = $this->db->get();
        if (!$query) {
            // Handle the error
            $error = $this->db->_error_message();
            log_message('error', 'Database query error: ' . $error);
            return false; // Or handle the error in another appropriate way
        } else {
            return $query->result();
        }
    }

    // ************************************************************
    public function get_call_suspend_all($from_date,$to_date)
    {
        $this->db->select('ch.*,bd.booking_id,bd.service_date, bd.day_service_id, 
            bd.delete_from_date, bd.delete_to_date, bd.remarks, 
            b.booking_type, b.service_week_day,b.time_from,b.time_to,b.service_start_date,
            TIMEDIFF(b.time_to, b.time_from) as total_hrs,
            et.enquiry_type_id, et.enquiry_type,
            c.customer_name, c.customer_id,c.customer_code,
            b.customer_address_id, b.maid_id, b.service_week_day, b.service_type_id,
            b.pending_amount, b.discount, b.total_amount, b.booking_status,
            s.service_type_name,
            m.maid_id, m.maid_name,
            ur.user_fullname, ur.user_id',FALSE)
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('booking_deletes as bd', 'bd.call_history_id  = ch.call_history_id', 'left')
            ->join('bookings as b', 'b.booking_id  = bd.booking_id', 'left')
            ->join('maids as m', 'm.maid_id  = b.maid_id', 'left')
            ->join('service_types as s', 's.service_type_id  = b.service_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.enquiry_type_id', 4)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }
    
    // ************************************************************
    public function get_call_complaints_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
            et.enquiry_type_id, et.enquiry_type,
            c.customer_name, c.customer_id,c.customer_code,
            cn.service_date,cn.complaint,cn.status,cn.action_taken,cn.added_time,cn.complaint_against,
            cn.complaint_against_name,cn.complaint_no,cn.complaint_time,ct.type,
            ur.user_fullname,ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('complaints_new as cn', 'cn.call_history_id  = ch.call_history_id', 'left')
            ->join('complaint_types as ct', 'ct.type_id  = cn.complaint_type', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.enquiry_type_id', 3)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }
    
    // ************************************************************
    public function get_call_enquiry_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
            et.enquiry_type_id, et.enquiry_type,c.customer_id,c.customer_code,c.customer_name,
            se.service_enquiry_id,se.date_from,se.date_to,se.time_from,se.time_to,se.area_id,se.location_id,
            se.customer_service_area,se.new_service_area,se.enquiry_customer_name,
            se.enquiry_customer_phoneno,se.enquiry_customer_email,se.enquiry_status,se.enquiry_week_day,
            s.service_type_name,a.area_name,z.zone_name,l.location_name,
            m.maid_id,m.maid_name,
            rs.refer_source_id,rs.refer_source,
            ur.user_fullname,ur.user_id')

            ->from('call_history as ch')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('service_enquiry as se', 'se.call_history_id  = ch.call_history_id', 'left')
            ->join('maids as m', 'se.maid_id = m.maid_id', 'left')
            ->join('customers as c', 'se.customer_id = c.customer_id', 'left')
            ->join('locations as l', 'l.location_id = se.location_id', 'left')
            ->join('areas as a', 'a.area_id = se.area_id', 'left')
            ->join('zones as z', 'z.zone_id = a.zone_id', 'left')
            ->join('service_types as s', 's.service_type_id  = se.service_type_id', 'left')
            ->join('refer_sources as rs', 'rs.refer_source_id  = se.refer_source_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('se.deleted_at', null)
            ->where('ch.enquiry_type_id', 2)
            ->where("(a.area_status = 1 OR a.area_status IS NULL)")
            ->where("(z.zone_status = 1 OR z.zone_status IS NULL)")
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }
    
    // ************************************************************
    public function get_call_preference_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
            et.enquiry_type_id, et.enquiry_type,c.customer_id,c.customer_code,c.customer_name,p.booking_type,
            p.date_from,p.date_to,p.time_from,p.time_to,p.area_id,p.location_id,
            s.service_type_name,a.area_name,z.zone_name,l.location_name,
            m.maid_id,m.maid_name,
            ur.user_fullname,ur.user_id')

            ->from('call_history as ch')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('preferences as p', 'p.call_history_id  = ch.call_history_id', 'left')
            ->join('customers as c', 'c.customer_id = p.customer_id', 'left')
            ->join('maids as m', 'p.maid_id = m.maid_id', 'left')
            ->join('locations as l', 'l.location_id = p.location_id', 'left')
            ->join('areas as a', 'a.area_id = p.area_id', 'left')
            ->join('zones as z', 'z.zone_id = a.zone_id', 'left')
            ->join('service_types as s', 's.service_type_id  = p.service_type_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.enquiry_type_id', 6)
            ->where('a.area_status', 1)
            ->where('z.zone_status', 1)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }
    
    // ************************************************************
    public function get_feedback_all($from_date,$to_date)
    {
        $this->db->select('ch.*,
            et.enquiry_type_id, et.enquiry_type,c.customer_id,c.customer_name,c.customer_code,
            f.date,f.message,
            m.maid_id,m.maid_name,
            ur.user_fullname,ur.user_id')

            ->from('call_history as ch')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('feedback as f', 'f.call_history_id  = ch.call_history_id', 'left')
            ->join('customers as c', 'c.customer_id = ch.customer_id', 'left')
            ->join('maids as m', 'f.maid_id = m.maid_id', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.enquiry_type_id', 9)
            ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }
    
    // ************************************************************
    public function get_call_complaints_customer($customer_id)
    {
        $this->db->select('ch.*,
            et.enquiry_type_id, et.enquiry_type,
            c.customer_name, c.customer_id,c.customer_code,
            cn.service_date,cn.complaint,cn.status,cn.action_taken,cn.added_time,cn.complaint_against,
            cn.complaint_against_name,cn.complaint_no,cn.complaint_time,ct.type,
            ur.user_fullname,ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('complaints_new as cn', 'cn.call_history_id  = ch.call_history_id', 'left')
            ->join('complaint_types as ct', 'ct.type_id  = cn.complaint_type', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('ch.customer_id', $customer_id)
            ->where('ch.enquiry_type_id', 3)
            // ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }


    // ************************************************************
    public function get_call_complaints_maid($maid_id)
    {
        $this->db->select('ch.*,
            et.enquiry_type_id, et.enquiry_type,
            c.customer_name, c.customer_id,c.customer_code,
            cn.service_date,cn.complaint,cn.status,cn.action_taken,cn.added_time,cn.complaint_against,
            cn.complaint_against_name,cn.complaint_no,cn.complaint_time,ct.type,
            ur.user_fullname,ur.user_id')
            ->from('call_history as ch')
            ->join('customers as c', 'ch.customer_id = c.customer_id', 'left')
            ->join('enquiry_types as et', 'ch.enquiry_type_id = et.enquiry_type_id', 'left')
            ->join('complaints_new as cn', 'cn.call_history_id  = ch.call_history_id', 'left')
            ->join('complaint_types as ct', 'ct.type_id  = cn.complaint_type', 'left')
            ->join('users as ur', 'ch.attended_by  = ur.user_id', 'left')
            ->where('ch.deleted_at', null)
            ->where('cn.complaint_against !=','U')
            ->where('cn.complaint_against_id', $maid_id)
            ->where('ch.enquiry_type_id', 3)
            // ->where("DATE(ch.added_date_time) BETWEEN '$from_date' AND '$to_date'") 
            ->order_by('ch.call_history_id', 'DESC');
    
        $query = $this->db->get();
        return $query->result();
    }

   public function get_enquiry_details($call_history_id)
   {
    $this->db->select('se.*')

        
            ->from('service_enquiry as se')
            ->join('call_history as ch', 'ch.call_history_id = se.call_history_id', 'left')
            // ->join('customers as c', 'se.customer_id = c.customer_id', 'left')
            // ->join('locations as l', 'l.location_id = se.location_id', 'left')
            // ->join('areas as a', 'a.area_id = se.area_id', 'left')
            // ->join('zones as z', 'z.zone_id = a.zone_id', 'left')
            // ->join('service_types as s', 's.service_type_id  = se.service_type_id', 'left')
            ->where('se.deleted_at', null)
            ->where('se.call_history_id', $call_history_id);
            
    
        $query = $this->db->get();
        return $query->result();
   }

}