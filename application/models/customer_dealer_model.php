<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_dealer_model extends CI_Model
{
    function add_dealers($data)
    {
        $this->db->set($data);
        $this->db->insert('customer_dealers');
        $result = $this->db->insert_id();
        return $result;
    }

    function get_customer_dealers()
    {
        $qr = $this->db->select('*')->from('customer_dealers')->where('deleted_at', null)->order_by('customer_dealer_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_dealer_details($customer_dealer_id)
    {
        $qr = $this->db->select('*')
            ->from('customer_dealers')
            ->where('customer_dealer_id',$customer_dealer_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_dealers($data,$dealer_id)
    {
        $this->db->where('customer_dealer_id',$dealer_id);
        $this->db->update('customer_dealers', $data);
       // print_r($this->db->last_query());die;
      
    }
    function delete_dealer($data,$customer_dealer_id)
    {
        $this->db->where('customer_dealer_id',$customer_dealer_id);
        $this->db->update('customer_dealers', $data);
    }





}