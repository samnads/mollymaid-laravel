<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
            
            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>User Activity Report</h3>
            </li>
            <li>
                <span style="margin-left:15px; color: #fff;">From :</span>
                <input type="text" style="width: 100px;" id="act_from_date" />
                </li>
            <li>
                <span style="margin-left:15px; color: #fff;">To :</span>
                <input type="text" style="width: 100px; margin-right: 10px;" id="act_to_date" />
                <input type="text" style="width:150px;" id="keyword-search" Placeholder="Search customer">
            </li>
            <li class="mr-0 float-right">   
                
                
                        
                <div class="topiconnew border-0 green-btn">
                     <a onclick="customerexcelbtn" id="customerexcelbtn" title="Download to Excel"> <i class="fa fa-download"></i></a>
                </div>
            </li>        
                            
            </div>
            <div class="widget-content" style="margin-bottom:30px">
                <table id="activityreportlist" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Sl. No.</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Type</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Booking Type</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Shift</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Action</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Done By</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;">Time</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /widget -->
</div>
<!-- /span6 -->
</div>
<style>
    .dataTables_wrapper .dataTables_processing {
       background-color:#294C5A;
    }
</style>