<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl No.</th>
            <th> Maid Name</th>
            <th> Present Address</th>
            <th> Mobile Number</th>
            <th> Nationality</th>
            <th> Gender</th>
            <th> Joining Date</th>
            <th> Flat</th>
            <th> Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($maids as $maids_val)
        {
            if ($maids_val['maid_gender'] == 'F') {
                $gender = "Female";
            } else if ($maids_val['maid_gender'] == 'M') {
                $gender = "Male";
            }
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $maids_val['maid_name']; ?></td>
            <td><?php echo $maids_val['maid_present_address']; ?></td>
            <td><?php echo $maids_val['maid_mobile_1']; ?></td>
            <td><?php echo $maids_val['maid_nationality']; ?></td>
            <td><?php echo $gender; ?></td>
            <td><?php echo $maids_val['maid_joining']; ?></td>
            <td><?php echo $maids_val['flat_name']; ?></td>
            <td>
                <?php
                if($maids_val['maid_status'] == 1)
                {
                    echo 'Active';
                } else {
                    echo 'Inactive';
                }
                ?>
            </td>
        </tr>
        <?php
        $i++;
        }
        ?>
    </tbody>
</table>