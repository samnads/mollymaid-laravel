<style>
  .add-leave {
    background-color: #b2d157;
    background-image: linear-gradient(to bottom, #b2d157, #64a434);
    border: 0 none;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    font-weight: bold;
    margin-right: 20px;
    padding: 6px 15px;
    text-decoration: none;
    text-transform: uppercase;
  }
</style>
<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }

  .topiconnew {
    cursor: pointer;
  }
</style>
<div id="leave-extend-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="maid_leave_extend" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Leave</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Enter Leave information</h3>
        <input type="hidden" id="maid_id" name="maid_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="booking_type" name="booking_type">
        <input type="hidden" id="booking_type" name="booking_type">
        <input type="hidden" id="delete_from_date1" name="delete_from_date1">
        <input type="hidden" id="delete_to_date1" name="delete_to_date1">

        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="start_date1" id="start_date1" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date To</p>
              <div class="n-field-box">
                <input name="end_date1" id="end_date1" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Leave Type</p>
          <div class="col-sm-12 p-0 ">
            <select name="leave_type1" id="leave_type1" style="margin-bottom: 4px;">
              <option value="1">Full Day</option>
              <option value="2">Half Day</option>
            </select>

          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Leaves</p>
          <div class="col-sm-12 p-0">
            <select name="leave_type_new1" id="leave_type_new1" style="margin-bottom: 4px;">
              <option value="leave">Leave</option>
              <option value="emergency_leave">Emergency Leaves</option>
              <option value="vacations">Vacations</option>
              <option value="holidays">Holidays</option>
              <option value="medical_leaves">Medical Leaves</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="button" id="leave-extend-btn" class="n-btn mb-0">Leave Extend</button>
      </div>
    </form>
  </div>
</div>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure want to disable this leave ?</h3>
      <input type="hidden" id="leave_id" value="">
      <input type="hidden" id="leave_status" value="">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-sm-12">
    <div class="widget widget-table action-table" style="margin-bottom:30px">
      <div class="widget-header">
        <form id="maid_leave" class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/maid-leave-report' ?>">

          <ul>
            <li><i class="icon-th-list"></i>
              <h3>Staff Leave Reports</h3>

            </li>
            <li>
              <select name="maid_id" id="search-maid-id" class="sel2">
                <option value="0">All Staffs</option>
                <?php
                $maid_id = "";
                foreach ($maids as $maid) {
                  $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
                  echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
                }
                ?>
              </select>

            </li>
            <li>
              <?php
              if (isset($startdate)) {
                $s_date = $startdate;
              } else {
                $s_date = date('d/m/Y');
              }
              if (isset($enddate)) {
                $e_date = $enddate;
              } else {
                $e_date = date('d/m/Y');
              }
              ?>
              <input type="text" name="start_date" id="start-date" class="span3" style="width: 90px;" value="<?php echo $s_date ?>" placeholder="Start date">
            </li>
            <li class="mr-2">
              <input type="text" name="end_date" id="end-date" class="span3" style="width: 90px;" value="<?php echo $e_date ?>" placeholder="End date">

            </li>
            <li>
              <input type="submit" class="n-btn" value="Go" id="searchgo" name="search">
            </li>
            <li>
              <input type="button" class="add-leave n-btn" value="Add Leave" id="add_leave" name="leave" style="display: none;">

            </li>


            <li class="mr-0 float-right">

              <div class="topiconnew border-0 green-btn">
                <a id="printButnforleave" title="Print"> <i class="fa fa-print"></i></a>
              </div>


              <div class="topiconnew border-0 green-btn">
                <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
              </div>

            </li>
          </ul>
        </form>
      </div>
      <div class="widget-content" style="margin-bottom:30px">
        <div id="LoadingImage" style="text-align:center;display:none;"><img src="<?php echo base_url() ?>img/loader.gif"></div>
        <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 30px;"> Sl. No.</th>
              <?php if (!@$maid_id) { ?>
                <th style="line-height: 18px; width: 30px;"> Staff</th>
              <?php } ?>
              <th style="line-height: 18px; width: 50px;"> Date</th>
              <th style="line-height: 18px; width: 50px;"> Type</th>
              <th style="line-height: 18px; width: 50px;"> Leave Type</th>
              <th style="line-height: 18px; width: 50px;"> User</th>
              <th style="line-height: 18px; width: 50px;"> Actions </th>
            </tr>
          </thead>
          <tbody>
            <?php

            if (!empty($results)) {
              $i = 0;
              foreach ($results as $lists) {
                if ($lists->typeleaves == "leave") {
                  $leave = "Leave";
                } else if ($lists->typeleaves == "emergency_leave") {
                  $leave = "Emergency Leaves";
                } else if ($lists->typeleaves == "vacations") {
                  $leave = "Vacations";
                } else if ($lists->typeleaves == "holidays") {
                  $leave = "Holidays";
                } else if ($lists->typeleaves == "medical_leaves") {
                  $leave = "Medical Leaves";
                  // if ($lists->typeleaves == "leave") {
                  //     $leave = "Leave";
                  // } else if ($lists->typeleaves == "absent") {
                  //     $leave = "Absent";
                  // } else if ($lists->typeleaves == "dayoff") {
                  //     $leave = "Dayoff";
                  // } else if ($lists->typeleaves == "offset") {
                  //     $leave = "Offset";
                  // } else if ($lists->typeleaves == "sick leave") {
                  //     $leave = "Sick Leave";
                  // } else if ($lists->typeleaves == "sm_replacement") {
                  //     $leave = "SM Replacement";
                } else {
                  $leave = "";
                }
                $leavedate = @$lists->leave_date;
                //$originalDate = "2010-03-21";
                $newDate = date("d/m/Y", strtotime($leavedate));

                echo '<tr>'
                  . '<td style="line-height: 18px;">' . ++$i . '</td>';
                if (!@$maid_id) {
                  echo '<td style="line-height: 18px;">' . $lists->maid_name . '</td>';
                }
                echo '<td style="line-height: 18px;">' . $newDate . '</td>'
                  . '<td style="line-height: 18px;">' . @$type[$lists->leave_type] . '</td>'
                  . '<td style="line-height: 18px;">' . $leave . '</td>';
            ?>
                <?php echo '<td style="line-height: 18px;">' . $lists->username . '</td>'; ?>
                <td style="line-height: 18px;">
                  <a href="javascript:void(0);" onclick="confirm_disable_modal(this,<?php echo $lists->leave_id ?>, <?php echo $lists->leave_status ?>);" style="text-decoration:none;"><?php echo $lists->leave_status == 1 ? '<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"> </i>' : '<i class="btn-icon-only icon-ok" style="font-size:25px;"> </i>' ?></a>
                  <?php if (user_authenticate_role() == 'hr') { ?>
                    <a href="javascript:void(0);" onclick="confirm_leave_extend_modal(<?php echo $lists->leave_id ?>, <?php echo $lists->leave_status ?>, '<?php echo $leavedate ?>', <?php echo $lists->maid_id ?>, '<?php echo $lists->typeleaves ?>');" style="text-decoration:none;"><?php echo $lists->leave_status == 1 ? '<i class="btn-icon-only fa fa-plus" style="color:green; font-size:25px;"> </i>' : '<i class="btn-icon-only icon-ok" style="font-size:25px;"> </i>' ?></a>
                  <?php } ?>
                </td>
            <?php echo '</tr>';
              }
            } else {
              // echo '<tr><td>No Results!</td></tr>';
            }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->

    </div>
    <!-- /widget -->
  </div>
  <!-- /span12 -->
</div>

<!-- For Print -->
<div id="divForPrintLeave" style="display: none">
  <div class="widget-content" style="margin-bottom:30px">
    <table border="1" width="70%" cellspacing="0" cellpadding="0" align="center">
      <thead>
        <tr>
          <th style="line-height: 18px; width: 75px; padding: 4px;"> Sl.NO</th>
          <th style="line-height: 18px; width: 350px; padding: 4px;"> Maid Name</th>
          <th style="line-height: 18px; width: 350px; padding: 4px;"> Date</th>
          <th style="line-height: 18px; width: 175px; padding: 4px;"> Total Full Day</th>
          <th style="line-height: 18px; width: 175px; padding: 4px;">Total Half Day</th>
          <th style="line-height: 18px; width: 175px; padding: 4px;">Total Day</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($leaveresults) {
          $i = 0;
          $total = 0;
          foreach ($leaveresults as $leaveresultsrow) {
            $totalday = ($leaveresultsrow->fullday + ($leaveresultsrow->halfday / 2));
            $total += $totalday;
        ?>
            <tr>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo ++$i; ?></td>
              <td style="line-height: 18px; padding: 4px;"><?php echo $leaveresultsrow->maid_name; ?></td>
              <td style="line-height: 18px; padding: 4px;"><?php echo date("d/m/Y", strtotime($leaveresultsrow->leave_date)); ?></td>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $leaveresultsrow->fullday; ?></td>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $leaveresultsrow->halfday; ?></td>
              <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo $totalday; ?></td>
            </tr>
          <?php
          }
          ?>
          <tr>
            <td style="line-height: 18px; text-align: center; padding: 4px;"></td>
            <td style="line-height: 18px; padding: 4px;"></td>
            <td style="line-height: 18px; padding: 4px;"></td>
            <td style="line-height: 18px; text-align: center; padding: 4px;"></td>
            <td style="line-height: 18px; text-align: center; padding: 4px;">Total :</td>
            <td style="line-height: 18px; text-align: center; padding: 4px;"><?php echo number_format($total, 1); ?></td>
          </tr>
        <?php }
        ?>
      </tbody>
    </table>
  </div>
  <!-- /widget-content -->
</div>
<script type="text/javascript">
  function exportF(elem) {
    var table = document.getElementById("divForPrintLeave");
    var html = table.outerHTML;
    var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url
    elem.setAttribute("href", url);
    elem.setAttribute("download", "MaidLeaveReport.xls"); // Choose the file name
    return false;
  }
  var del_fun_this = null;

  function confirm_disable() {
    $.ajax({
      type: "POST",
      url: _base_url + "reports/remove_leave",
      data: {
        leave_id: $('#leave_id').val(),
        leave_status: $('#leave_status').val()
      },
      // dataType: "text",
      // cache: false,
      success: function(result) {
        //window.location.assign(_base_url + 'customers');
        if (result == 1) {
          //$($this).attr('class', 'btn btn-success btn-small');
          $(del_fun_this).html('<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"></i>');
        } else {
          //$($this).attr('class', 'btn btn-danger btn-small');
          $('#searchgo').trigger('click');
          //$($this).html('<i class="btn-icon-only icon-remove" style="color:red; font-size:25px;"> </i>');
        }
        $(del_fun_this).attr('onclick', 'confirm_disable_modal(this, ' + $('#leave_id').val() + ', ' + result + ')');
      }
    });
  }

  function confirm_disable_modal($this, id, status) {
    del_fun_this = $this;
    $('#leave_id').val(id);
    $('#leave_status').val(status);
    $.fancybox.open({
      autoCenter: true,
      fitToView: false,
      scrolling: false,
      openEffect: 'none',
      openSpeed: 1,
      autoSize: false,
      width: 450,
      height: 'auto',
      helpers: {
        overlay: {
          css: {
            'background': 'rgba(0, 0, 0, 0.3)'
          },
          closeClick: false
        }
      },
      padding: 0,
      closeBtn: false,
      content: $('#disable-popup'),
    });
  }

  function confirm_leave_extend_modal(id, status, leavedate, maid_id, leave_type) {
    // $('#leave_id').val(id);
    // $('#leave_status').val(status);
    // $('#leavedate').val(leavedate);
    $.ajax({
      type: "POST",
      dataType: "json",
      url: _base_url + 'reports/maid_leave_extend',
      data: {
        leavedate: leavedate,
        status: status,
        id: id,
        maid_id: maid_id,
        leave_type: leave_type
      },
      // cache: false,
      success: function(data) {
        fancybox_show('leave-extend-popup', {
          width: 450
        });
        // var formattedDeleteFromDate = formatDate(data.delete_from_date);
        // var formattedDeleteToDate = formatDate(data.delete_to_date);
        // var selectedLeaveType = $('#leave_type').val();
        var maid_leave_data = data.maid_leave_data;
        maid_leave_data.forEach(function(leave) {
          var selectedValue = '';
          var selectedText = '';

          if (leave.leave_type === '1') {
            selectedValue = '1';
            selectedText = 'Full Day';
          } else if (leave.leave_type === '2') {
            selectedValue = '2';
            selectedText = 'Half Day';
          }
          var selectedValue1 = '';
          var selectedText1 = '';
          if (leave.typeleaves === 'vaccations') {
            selectedValue1 = 'vaccations';
            selectedText1 = 'Vacations';
          } else if (leave.typeleaves === 'leave') {
            selectedValue1 = 'leave';
            selectedText1 = 'Leave';
          } else if (leave.typeleaves === 'holidays') {
            selectedValue1 = 'holidays';
            selectedText1 = 'Holidays';
          } else if (leave.typeleaves === 'medical_leaves') {
            selectedValue1 = 'medical_leaves';
            selectedText1 = 'Medical Leaves';
          }
          // Set the selected value
          var selectElement = document.getElementById('leave_type1');
          selectElement.value = leave.leave_type;
          // var selectedText = selectElement.options[selectElement.selectedIndex].text;

          var selectElement1 = document.getElementById('leave_type_new1');
          selectElement1.value = leave.typeleaves;

          // // var selectedText1 = selectElement1.options[selectElement1.selectedIndex1].text;
        });
        var leaveData = data.maid_leave_data;
        if (leaveData.length > 0) {
          var firstLeaveDate = leaveData[0].leave_date;
          var lastLeaveDate = leaveData[leaveData.length - 1].leave_date;
          var lastLeaveDateObj = new Date(lastLeaveDate);
          var extendedLastLeaveDateObj = addDays(lastLeaveDateObj, 0);
          var extendedLastLeaveDate = formatDate1(extendedLastLeaveDateObj);
          $('#maid_leave_extend input[name="start_date1"]').val(formatDate(firstLeaveDate));
          $('#maid_leave_extend input[name="end_date1"]').val(formatDate(extendedLastLeaveDate));
        }

        $('#maid_leave_extend input[name="maid_id"]').val(maid_id);
        // $('#maid_leave_extend input[name="booking_id"]').val(booking_id);
        // $('#maid_leave_extend input[name="booking_type"]').val(booking_type);
        // $('#maid_leave_extend input[name="delete_from_date1"]').val(delete_from_date);
        // $('#maid_leave_extend input[name="delete_to_date1"]').val(delete_to_date);
      },
      error: function(data) {
        toast('error', data.statusText);
      },
    });
    // fancybox_show('leave-extend-popup', { width: 450 });
  }

  function formatDate1(date) {
    var d = new Date(date);
    var year = d.getFullYear();
    var month = ("0" + (d.getMonth() + 1)).slice(-2); // Months are zero-based, so add 1
    var day = ("0" + d.getDate()).slice(-2);
    return year + "-" + month + "-" + day;
  }

  function formatDate(inputDate) {
    if (!inputDate) return ''; // Return empty string if inputDate is falsy

    var parts = inputDate.split('-'); // Split 'yyyy-mm-dd'
    if (parts.length !== 3) return ''; // Ensure there are 3 parts

    return parts[2] + '/' + parts[1] + '/' + parts[0]; // Return 'dd/mm/yyyy'
  }

  function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
</script>