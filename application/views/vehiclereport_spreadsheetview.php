<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th> Sl.No</th>
            <th> Source</th>
            <th> Cleaner</th>
            <th> Client Name</th>
            <th> Customer Type</th>
            <th> Ref Code</th>
            <th> Location</th>
            <th> MAT</th>
            <th> M.O.P</th>
            <th> HRS</th>
            <th> Total</th>
            <th> VAT</th>
            <th> Grand Total</th>
            <th> Service<br>Type</th>
            <th> Timings</th>
            <!--<th> Zone</th>-->
            <th> Date</th>
            <th> Remarks</th>
        </tr>
    </thead>
    <tbody>
    <?php             
    if ($vehicle_report != NULL) 
    {
        $i=1;
        foreach ($vehicle_report as $veh)
        {
            //Payment Type
            if($veh['customer_paytype'] == "D")
            {
                $paytype = "Daily";
            } else if($veh['customer_paytype'] == "W")
            {
                $paytype = "Weekly";
            } else if($veh['customer_paytype'] == "M")
            {
                $paytype = "Monthly";
            } else
            {
                $paytype = "";
            }
            $t_shrt = $veh['time_to']; 
            $cur_shrt = $veh['time_from'];
            $hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
                                
            if($veh['cleaning_material'] == 'Y')
            {
                $mat = "Yes";
            } else {
                $mat = "No";
            }
            //price calculation
            $normal_hours = 0;
            $extra_hours = 0;
            $weekend_hours = 0;

            $normal_from = strtotime('08:00:00');
            $normal_to = strtotime('20:00:00');

            $shift_from = strtotime($veh['time_from']);
            $shift_to = strtotime($veh['time_to']);

            $total_hours = ($shift_to - $shift_from) / 3600;
            if($veh['cleaning_material'] == 'Y')
            {
                $materialfee = ($total_hours * 10);
            } else {
                $materialfee = 0;
            }
//            if ($search['search_date'] == "") {
//                $schedule_date = date('d/m/Y');
//            } else {
//                $schedule_date = $search['search_date'];
//            }
            //$s_date = explode("/", $schedule_date);
            $service_date = $searchdate;

            $today_week_day = date('w', strtotime($service_date));

            if($today_week_day != 5 )                        // except friday  case // changed on 23-01-2017
            { 
                if($shift_from < $normal_from)
                {
                    if($shift_to <= $normal_from)
                    {
                        $extra_hours = ($shift_to - $shift_from) / 3600;
                    }

                    if($shift_to > $normal_from && $shift_to <= $normal_to)
                    {
                        $extra_hours = ($normal_from - $shift_from) / 3600;
                        $normal_hours = ($shift_to - $normal_from) / 3600;
                    }

                    if($shift_to > $normal_to)
                    {
                        $extra_hours = ($normal_from - $shift_from) / 3600;
                        $extra_hours += ($shift_to - $normal_to) / 3600;
                        $normal_hours = ($normal_to - $normal_from) / 3600;
                    }
                }

                if($shift_from >= $normal_from && $shift_from < $normal_to)
                {
                    if($shift_to <= $normal_to)
                    {
                        $normal_hours = ($shift_to - $shift_from) / 3600;
                    }

                    if($shift_to > $normal_to)
                    {
                        $normal_hours = ($normal_to - $shift_from) / 3600;
                        $extra_hours = ($shift_to - $normal_to) / 3600;
                    }
                }

                if($shift_from > $normal_to)
                {
                    $extra_hours = ($shift_to - $shift_from) / 3600;
                }
            }
            else
            {
                $weekend_hours = ($shift_to - $shift_from) / 3600;
            }

            $service_description = array();

            $service_description['normal'] = new stdClass();
            $service_description['normal']->hours = $normal_hours;
            if($veh['booked_from'] == "W")
            {
                $service_description['normal']->fees = $normal_hours * $veh['price_per_hr'];
            } else {
                $service_description['normal']->fees = $normal_hours * $veh['price_hourly'];
            }
            $service_description['normal']->fees;
            $service_description['extra'] = new stdClass();
            $service_description['extra']->hours = $extra_hours;
            $service_description['extra']->fees = $extra_hours * $veh['price_extra'];

            $service_description['weekend'] = new stdClass();
            $service_description['weekend']->hours = $weekend_hours;
            $service_description['weekend']->fees = $weekend_hours * $veh['price_weekend'];
                                
            $location_charge = 0;

            $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees+ $location_charge - $veh['discount'];

            $vat_fee = ($total_fee * (5/100));

            $netpay = ($total_fee + $vat_fee);
            //ends
			$subtotal = ($veh['total_amount']/1.05);
			$vat = ($subtotal * 0.05);
            ?>
        <tr>
            <td><?php echo $i; ?> </td>
			<td><?php echo $veh['customer_source'] ?> </td>
			<td><?php echo $veh['maid'] ?> </td>
			<td>
                <?php

                if ($veh['booking_type'] == "OD") {
                ?>
                    <p style="color: #ff7223;">
                         <?php echo $veh['customer'] ?>
                    </p>

                <?php
                } else if ($veh['booking_type'] == "WE") {
                ?>
                <p style="color: #9e6ab8;">
                   <?php echo $veh['customer'] ?>
                </p>

                <?php
                } else {
                ?>
				<p style="color: #9e6ab8;">
                   <?php echo $veh['customer'] ?>
                </p>
				<?php } ?>
            </td>
            <td><?php echo $paytype; ?></td>
			<td>
			<?php
//			if($veh['customer_source'] == "Justmop")
//			{
//				if($veh['booking_type'] == "OD")
//				{
//					$ref = $veh['justmop_reference'];
//				} else {
//					$ref = $veh['justmop_reference'];
//				}
//			} else {
//				$ref = "";
//			}
                        
			$ref = $veh['justmop_reference'];
			echo $ref;
			?>
			</td>
			<td><?php echo $veh['customer_address'] ?></td>
			<td><?php echo $mat; ?></td>
			<td><?php echo $veh['payment_mode']; ?></td>
			<td><?php echo $hours ?> </td>
			<td><?php echo round($subtotal,2); ?></td>
			<td><?php echo round($vat,2); ?></td>
                        <td><?php echo $veh['total_amount']; ?></td>
                        <td><?php echo $veh['service_type_name']; ?></td>
			<td>
                            <?php echo date("g:i A", strtotime($veh['time_from']));?> &nbsp; - &nbsp;
                            <?php echo date("g:i A", strtotime($veh['time_to']));?> 
                            <?php //echo $veh['time_from'] ?> <?php //echo $veh['time_to'] ?>
                        </td>
			<!--<td><?php// echo $veh['zone_name'] ?> </td>-->
			<td><?php echo $veh['veh_date'] ?> </td>
			<td><?php echo $veh['booking_note']; ?></td>
        </tr>
        <?php
        $i++;
        }
        }
        ?>
    </tbody>
</table>