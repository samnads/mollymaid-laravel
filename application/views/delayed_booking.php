<style>
    .book-nav-top li{
        margin: 0 10px 0 0 !important;
    }
    .select2-arrow{visibility : hidden;}
    .select2-container .select2-choice{
	-moz-appearance: none;
        background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #ccc;
        border-radius: 3px;
        cursor: pointer;
        font-size: 12px;
        height: 30px;
        line-height: 24px;
        padding: 3px 0 3px 10px;
        text-indent: 0.01px;
    }
    .select2-results li{margin-left: 0px !important;}
/*    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }*/
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">
        <div class="col-md-12 col-sm-12 no-left-right-padding">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header"> 
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>booking/delayed_booking">
                    <div class="book-nav-top">
                        <ul>
                            <li>
                                <i class="icon-th-list"></i>
                                <h3>Delayed Booking</h3>
                            </li>
                            
                            <li>
                                <input type="text" id="cstatemnet-date-from" name="cstatemnet-date-from" style="width: 160px;" data-date="<?php echo $currentdate; ?>" readonly value="<?php echo $currentdate; ?>" data-date-format="dd/mm/yyyy"/> 
                            </li>
                            
                            <li>
                                <input type="submit" id="customer-statement-search" value="Search" class="n-btn" />
                            </li>


                            <div class="clear"></div>
                     
                     
                        </ul>
                    </div>
                    
                    
                </form>
            </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
            <div id="statement_content" class="">	
                <table id="statement-content-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <!--<th class="no-left-border"></th>-->
                            <th>Sl</th>
                            <th>Customer</th>
                            <th>Maid</th>
                            <th>Booking Time</th>
                            <th>Drop Time</th>
                            <th>Zone(Driver)</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody id="invoice-tabtbody1">
                        <?php
                        if(!empty($current_booking))
                        {
                        $sln = 1;
                        foreach($current_booking as $val)
                        {
                        ?>
                        <tr>
                            <td><?php echo $sln++; ?></td>
                            <td><?php echo $val['customer_name']; ?></td>
                            <td><?php echo $val['maid_name']; ?></td>
                            <td><?php echo $val['from_time']; ?></td>
                            <td><?php echo $val['drop_time']; ?></td>
                            <td><?php echo $val['zone_name']; ?></td>
                            <td><?php echo $val['total_amount']; ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        
                        <?php
                        }
                        ?>
                    </tbody>
                 </table> 
            </div>
        </div><!--welcome-text-main end-->
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->