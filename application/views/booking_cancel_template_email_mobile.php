<html>
<head>
</head>

<body>

<div class="main-wrapper" style="width: 800px; margin:0 auto;">

     <div style="width: 800px;"><img src="<?php echo base_url(); ?>images/elitemaidemaidbanner.jpg" width="800" height="232" alt="" /></div>
     
     <div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; width: 798px;">
	 <div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
     
     <div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear <?php echo $name;  ?>,</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Your booking with below reference id has been cancelled successfully...</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;">
          <span style="font-size:15px; background: #00bff3; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Your Reference ID - <?php echo $bookingdetails[0]->reference_id;?></span>
          </p>
     </div>
     
     
     <div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px; ">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:0px 0px 0px 0px; font-weight: bold; margin:0px;">Address Summary</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:5px 0px 0px 0px; margin:0px;">
			 Building : <?php echo $bookingdetails[0]->building; ?><br />
             Unit : <?php echo $bookingdetails[0]->unit_no; ?><br />
             Street : <?php echo $bookingdetails[0]->street; ?><br />
             Area - <?php if($bookingdetails[0]->area_name == 'Other') { echo $bookingdetails[0]->other_area; } else { echo $bookingdetails[0]->area_name; } ?></p>
     </div>
     
     
     
     <div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
          <table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
            <tr style="background: #FFF;">
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">No of Maids</p></td>
              <td width="4">:</td>
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $bookingdetails[0]->no_of_maids ?></p></td>
          
            </tr>
			
			<?php
			foreach ($bookingdetails as $details)
			{ 
			?>
			<tr style="  ">
              <td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Service Date & Time</p></td>
              <td style="">:</td>
              <td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $details->service_start_date; ?> (One Day)<br/><?php echo date('h:i a', strtotime($details->time_from)) . ' - ' . date('h:i a', strtotime($details->time_to)) ?></p></td>
            </tr>
			<?php
			}
			?>
            
            <tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Hours</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php echo $bookingdetails[0]->no_of_hrs ?></p></td>
            </tr>
            
            <tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Payment Method</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;"><?php if($bookingdetails[0]->pay_by == 'card') { echo 'Card'; } else { echo 'Cash'; } ?></p></td>
            </tr>
          </table>

     </div>
     
     
     
     
     <div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
          <strong>Elite Maids Cleaning Services</strong><br />
						  Office 201B, Prime Business Center,<br />
						  Jumeirah Village Circle, Dubai,<br />
						  United Arab Emirates.<br />
						  For Bookings : +971 800 258 / +971 58 286 4783<br />
						  Email : info@elitemaids.ae</p>
     </div>
     
     

     
     </div>
     
<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Elite Maids All Rights Reserved.</p>
     </div>

</div>
	 
	 </div>


</body>
</html>
