<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }

    table.da-table tr td {
        padding: 0px 6px;
    }

    #pass_image {
        max-width: 100px;
        max-height: 100px;
    }

    #view_pass_modal .modal-body {
        max-height: 500px;
        overflow-y: auto;
    }
</style>
<div id="disable-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Disable ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to disable this pass ?</h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
        </div>
    </div>
</div>
<div id="enable-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Enable ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to enable this pass ?</h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
        </div>
    </div>
</div>
<div id="avatar-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name" class="avatar-popup-name"></span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div id="" class="col-12 p-0">
            <div class="modal-body">
                <div class="img-container">
                    <img class="thumbnail" id="avatar-popup-image" src="#">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- view and edit gate_pass -->
<div id="view_pass_modal" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="location-view-edit-title">View GatesPass</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div id="view_pass_popup" class="col-12 p-0">
            <div class="modal-body">
                <div class="row m-0 n-field-main">
                    <p>Customer Code</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="customercode" id="customercode" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Customer name</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="customername" id="customername" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Employee name</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="employee_name" id="employee_name" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Employee Type</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="employee_type" id="employee_type" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Pass Type</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="pass_type" id="pass_type" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Service Date</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="service_date" id="service_date" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Service Start Date</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="service_start_date" id="service_start_date" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Service End Date</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="service_end_date" id="service_end_date" autocomplete="off">
                    </div>
                </div>
                <div class="row m-0 n-field-main">
                    <p>Pass Status</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="pass_status" id="pass_status" autocomplete="off">
                    </div>
                </div>

                <div class="row m-0 n-field-main">
                    <p>Gate pass File</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <label>
                            <a href="" id="pass_image_link" target="_blank">
                                <img class="rounded" id="pass_image" src="">
                            </a>
                        </label>
                        <div id="security_pass_image_error"></div>
                    </div>
                </div>

            </div>
        </div>


    </div>
</div>

<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
                <!-- <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data"> -->
                <ul>
                    <li>
                        <i class="icon-th-list"></i>
                        <h3>Gate Pass</h3>
                    </li>

                    <li class="float-right">

                        <div class="topiconnew border-0 green-btn">
                            <a href="<?php echo base_url('security_pass'); ?>" title="New Gate Pass"> <i class="fa fa-plus"></i></a>
                        </div>

                    </li>

                </ul>
                <!-- </form> -->
            </div>
            <!-- /widget-header -->

            <div class="widget-content">
                <table id="maids-list-table" class="table table-hover da-table" width="100%">
                    <thead>
                        <tr>
                            <th style="width:45px;">Sl. No.</th>
                            <th>Customer Code</th>
                            <th>Customer Name</th>
                            <th>Employee Name</th>
                            <th>Employee Type</th>
                            <th>Pass Type</th>
                            <th>Service Date</th>
                            <th>Service Start Date</th>
                            <th>Service End Date</th>
                            <th>Created At</th>
                            <th>Pass Image</th>
                            <th class="td-actions" style="width:120px !important;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($gate_pass) > 0) {
                            $i = 1;
                            foreach ($gate_pass as $pass) {
                        ?>
                                <tr>
                                    <td style="line-height: 18px;">
                                        <center><?php echo $i; ?></center>
                                    </td>
                                    <?php
                                    $image = $pass->file_name;
                                    ?>
                                    <td style="line-height: 18px;"><?php echo $pass->customer_code; ?></td>
                                    <td style="line-height: 18px;"><?php echo $pass->customer_name; ?></td>
                                    <td style="line-height: 18px; "><?php echo $pass->maid_name; ?></td>
                                    <td style="line-height: 18px;"><?php echo $pass->employee_type; ?></td>
                                    <td style="line-height: 18px;"><?php echo $pass->pass_type; ?></td>
                                    <td style="line-height: 18px;">
                                        <?php echo !empty($pass->service_date) ? date('d-m-y', strtotime($pass->service_date)) : '-'; ?>
                                    </td>
                                    <td style="line-height: 18px;">
                                        <?php echo !empty($pass->service_start_date) ? date('d-m-y', strtotime($pass->service_start_date)) : '-'; ?>
                                    </td>
                                    <td style="line-height: 18px;">
                                        <?php echo !empty($pass->service_end_date) ? date('d-m-y', strtotime($pass->service_end_date)) : '-'; ?>
                                    </td>
                                    <td style="line-height: 18px;"><?php echo date('d-m-y', strtotime($pass->created_at)); ?></td>
                                    <td style="line-height: 18px; cursor: pointer;text-align: center;" onclick="openImageWindow('<?php echo base_url('uploads/gate_pass/' . $image); ?>')">
                                        <img src="<?php echo base_url('uploads/gate_pass/' . $image); ?>" style="height: 40px; width: 40px; margin: 3px auto 3px; border-radius: 50%;" />
                                    </td>

                                    <td style="line-height: 18px;width:120px !important" class="td-actions">
                                        <center>

                                            <a class="n-btn-icon blue-btn" data-action="view-pass" data-id="<?= $pass->pass_id; ?>" title="View"><i class="btn-icon-only fa fa-eye"></i></a>
                                            <a class="n-btn-icon purple-btn" href="<?php echo base_url('security_pass/' . $pass->pass_id); ?>" title="Edit Pass">
                                                <div class="center-icon">
                                                    <i class="btn-icon-only icon-pencil"></i>
                                                </div>
                                            </a>
                                            <!-- <?php if ($pass->pass_status == 1) { ?>
                                                <a href="javascript:void(0)" class="n-btn-icon green-btn" title="Disable" onclick="confirm_disable_modal(<?php echo $pass->pass_id; ?>,<?php echo $pass->pass_status; ?>);"><i class="btn-icon-only fa fa-toggle-on "> </i></a>
                                            <?php } ?>
                                            <?php if ($pass->pass_status == 0) { ?>
                                                <a href="javascript:void(0)" class="n-btn-icon red-btn" title="Enable" onclick="confirm_enable_modal(<?php echo $pass->pass_id; ?>,<?php echo $pass->pass_status; ?>);"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                                            <?php } ?> -->
                                        </center>
                                    </td>
                                </tr>
                            <?php
                                $i++;
                            }
                        } else {
                            ?>
                            <tr class="bg-warning">
                                <td colspan="10" style="text-align:center;color:chocolate">No Data Found</td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div>
    <!-- /span6 -->
</div>


<script>
    var filter_status = <?= $this->uri->segment(3) ?: 2 ?>;

    function maidFilter(id) {
        window.location.href = _base_url + "maids/" + id;
    }
    (function(a) {
        a(document).ready(function(b) {
            if (a('#maids-list-table').length > 0) {
                a("table#maids-list-table").dataTable({
                    'sPaginationType': "full_numbers",
                    "bSort": true,
                    "iDisplayLength": 100,
                    "scrollY": true,
                    "orderMulti": false,
                    "scrollX": true,
                    'columnDefs': [{
                        'targets': [1, 4, 5],
                        'orderable': false
                    }, ]
                });
            }
        });
    })(jQuery);

    function showAvatar(name, src) {
        $('.avatar-popup-name').html(name);
        $('#avatar-popup-image').attr('src', src);
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 400,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: true
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#avatar-popup'),
        });
    }
    var pass_id;
    var pass_status;

    function confirm_disable() {
        $.ajax({
            type: "POST",
            url: _base_url + "maid/change_status",
            data: {
                pass_id: pass_id,
                status: pass_status
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                window.location.assign(_base_url + 'maids/' + filter_status);
            }
        });
    }

    function confirm_enable() {
        $.ajax({
            type: "POST",
            url: _base_url + "maid/change_status",
            data: {
                pass_id: pass_id,
                status: pass_status
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                window.location.assign(_base_url + 'maids/' + filter_status);
            }
        });
    }

    function confirm_delete() {
        $.ajax({
            type: "POST",
            url: _base_url + "maid/change_status",
            data: {
                pass_id: pass_id,
                status: pass_status
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                window.location.assign(_base_url + 'maids');
            }
        });
    }

    function confirm_disable_modal(maid, status) {
        pass_id = maid;
        pass_status = status;
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#disable-popup'),
        });
    }

    function confirm_enable_modal(maid, status) {
        pass_id = maid;
        pass_status = status;
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#enable-popup'),
        });
    }
    $(function() {
        let current = window.location.href;
        $('#primary_nav_wrap li a').each(function() {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href') === '<?php echo base_url('maids'); ?>') {
                $this.addClass('active');
            }
        })
    })
    // $("#maid-status > option").each(function() {
    //   if(filter_status == this.value){
    //      $(this).attr("selected","selected");
    //   }
    // });
    // $('#maid-status').change(function () {
    //         window.location = _base_url + 'maids/' + $('#maid-status').val();
    //     });

    function openImageWindow(imageUrl) {
        window.open(imageUrl, '_blank');
    }
</script>