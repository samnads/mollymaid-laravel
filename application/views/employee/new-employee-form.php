<div id="crop-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span id="b-maid-name">Image Cropper</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
      </div>
      <div id="" class="col-12 p-0">
         <div class="modal-body">
            <div class="img-container">
               <img id="image" src="#">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
            <button type="button" class="n-btn mb-0" id="crop">Crop</button>
         </div>
      </div>
   </div>
</div>
<div class="row m-0">
   <div class="col-sm-12">
      <div class="widget">
         <div class="widget-header">
            <div class="book-nav-top">
               <ul>
                  <li>
                     <i class="icon-user"></i>
                     <h3>New Employee</h3>
                  </li>
                  <li class="mr-0 float-right">
                     <div class="topiconnew border-0 green-btn">
                        <a href="<?php echo base_url('employee/list'); ?>" title="Employees List"> <i class="fa fa-list"></i></a>
                     </div>
                  </li>
                  <div class="clear"></div>
               </ul>
            </div>
         </div>
         <div class="widget-content">
            <div class="tabbable">
               <ul class="nav nav-tabs pb-4">
                  <li>
                     <a data-target="#tab1" data-toggle="tab" data-tab="tab1">Basic Details</a>
                  </li>
                  <li>
                     <a data-target="#tab2" data-toggle="tab" data-tab="tab2">Address</a>
                  </li>
                  <li>
                     <a data-target="#tab3" data-toggle="tab" data-tab="tab3">Skills</a>
                  </li>
                  <li>
                     <a data-target="#tab4" data-toggle="tab" data-tab="tab4">Daily Preference</a>
                  </li>
                  <!-- <li>
                     <a data-target="#tab5" data-toggle="tab" data-tab="tab5">Documents</a>
                  </li> -->
               </ul>
               <form id="new_employee_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                  <div class="tab-content">
                     <div class="tab-pane" id="tab1">
                        <div class="col-sm-4">
                           <div class="row m-0 n-field-main">
                              <div class="col-sm-12 p-0 n-field-box">
                                 <div class="row m-0">
                                    <p>Employee Type<rf>
                                    </p>
                                    <div class="col-sm-3 n-radi-check-main p-0">
                                       <input type="radio" value="1" id="employee_type_1" name="employee_type_id" class="" checked>
                                       <label for="employee_type_1">
                                          <span class="border-radius-3"></span>
                                          <p>Maid</p>
                                       </label>
                                    </div>
                                    <div class="col-sm-3 n-radi-check-main p-0">
                                       <input type="radio" value="2" id="employee_type_2" name="employee_type_id" class="">
                                       <label for="employee_type_2">
                                          <span class="border-radius-3"></span>
                                          <p>Driver</p>
                                       </label>
                                    </div>
                                    <div class="col-sm-3 n-radi-check-main p-0">
                                       <input type="radio" value="3" id="employee_type_3" name="employee_type_id" class="">
                                       <label for="employee_type_3">
                                          <span class="border-radius-3"></span>
                                          <p>Accountant</p>
                                       </label>
                                    </div>
                                    <div class="col-sm-3 n-radi-check-main p-0">
                                       <input type="radio" value="4" id="employee_type_4" name="employee_type_id" class="">
                                       <label for="employee_type_4">
                                          <span class="border-radius-3"></span>
                                          <p>Mechanic</p>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main" id="is-maid-leader">
                              <div class="col-sm-3 n-radi-check-main p-0">
                                 <input type="checkbox" value="Y" id="is_maid_leader" name="is_maid_leader" class="">
                                 <label for="is_maid_leader">
                                    <span class="border-radius-3"></span>
                                    <p>Is Team Leader</p>
                                 </label>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main" id="maid-leader">
                              <p>Select Leader<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <select name="maid_leader_id" class="sel2" style="width:100%">
                                    <option value="">-- Select leader --</option>
                                    <?php foreach ($maid_leaders as $maid_leader) : ?>
                                       <option value="<?= $maid_leader->maid_id ?>"><?= $maid_leader->maid_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Employee Name<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" name="employee_name" autocomplete="off">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Profile Image</p>
                              <label style="width: 100px;">
                                 <img class="img-thumbnail" id="employee_avatar_show" src="<?= base_url('uploads/images/default/maid-avatar-upload.png'); ?>" style="width: 100%;">
                                 <input type="file" class="input-image" accept="image/*" data-selector="employee_avatar" data-crop-width="250" data-crop-height="250">
                              </label>
                              <input type="hidden" name="employee_avatar_base64" id="employee_avatar_base64">
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Nationality<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <select name="nationality" class="sel2" style="width:100%">
                                    <option value="">-- Select nationality --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>"><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Gender</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <select name="gender_id" class="sel2" style="width:100%">
                                    <option value="">-- Select gender --</option>
                                    <?php foreach ($genders as $gender) : ?>
                                       <option value="<?= $gender->gender_id ?>"><?= $gender->gender ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Present Address<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <textarea class="" rows="1" name="present_address"></textarea>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Permanent Address</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <textarea class="" rows="1" name="permanent_address"></textarea>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Date of Joining<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="date_of_joining" readonly>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number 1<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="mobile_number_1" id="mobile_number_1">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number 2</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="mobile_number_2">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp No.</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="whatsapp_number_1">
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="row m-0 n-field-main" id="service-types">
                              <p>Services<rf>
                              </p>
                              <?php foreach ($services as $key => $service) : ?>
                                 <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
                                    <div class="col-sm-12 pr-0 pl-0 n-radi-check-main">
                                       <input id="service_<?= $key ?>" type="checkbox" value="<?php echo $service['service_type_id'] ?>" name="services_ids[]">
                                       <label for="service_<?= $key ?>">
                                          <span class="border-radius-3"></span>
                                          <p><?php echo $service['service_type_name'] ?></p>
                                       </label>
                                    </div>
                                 </div>
                              <?php endforeach; ?>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Notes</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <textarea rows="5" name="notes"></textarea>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Username</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" class="" name="username" readonly onfocus="this.removeAttribute('readonly');">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Password</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="password" autocomplete="off" class="" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main payment_mode_customer" style="">
                              <p>Priority Number</p>
                              <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                 <input type="text" name="priority_number">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="tab2">
                        <div class="col-sm-3">
                           <h3>Local Address</h3>
                           <input type="hidden" name="address_type_ids[0]" value="1">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[0]" id="address_type_mobile_no_1[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[0]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[0]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>"><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <h3>Permanent Address</h3>
                           <input type="hidden" name="address_type_ids[1]" value="2">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[1]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>"><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <h3>Emergency Contact - Local</h3>
                           <input type="hidden" name="address_type_ids[2]" value="3">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[2]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>"><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <h3>Emergency Contact - Home Country</h3>
                           <input type="hidden" name="address_type_ids[3]" value="4">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[3]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[3]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>"><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="tab3">
                        <div class="col-sm-4">
                           <div class="row">
                              <div class="col-xs-6 col-sm-4">
                                 <select id="skill_id" class="sel2" style="width:100%">
                                    <option value="">-- Select Skill --</option>
                                    <?php foreach ($skills as $skill) : ?>
                                       <option value="<?= $skill->skill_id ?>"><?= $skill->skill ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                              <div class="col-xs-6 col-sm-4">
                                 <select id="skill_rating_id" class="sel2" style="width:100%">
                                    <option value="">-- Select Level --</option>
                                    <?php foreach ($rating_levels as $rating_level) : ?>
                                       <option value="<?= $rating_level->rating_level_id ?>"><?= $rating_level->rating_level ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                              <div class="col-xs-6 col-sm-4">
                                 <button type="button" class="n-btn purple-btn" data-action="select-skill"><i class="fa fa-plus" aria-hidden="true"></i></button>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="row">
                              <div class="col-sm-12 p-0" id="skills-holder">
                                 <!-- -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="tab4">

                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="1" name="availability_week_days[1]">
                                    <input type="checkbox" id="week_day_1" value="Y" name="availability_checked[1]">
                                    <label for="week_day_1">
                                       <span class="border-radius-3"></span>
                                       <p>Monday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[1]" class="sel2" style="width:100%">
                                    <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[1]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[1]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="2" name="availability_week_days[2]">
                                    <input type="checkbox" id="week_day_2" value="Y" name="availability_checked[2]">
                                    <label for="week_day_2">
                                       <span class="border-radius-3"></span>
                                       <p>Tuesday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[2]" class="sel2" style="width:100%">
                                 <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[2]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[2]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="3" name="availability_week_days[3]">
                                    <input type="checkbox" id="week_day_3" value="Y" name="availability_checked[3]">
                                    <label for="week_day_3">
                                       <span class="border-radius-3"></span>
                                       <p>Wednessday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[3]" class="sel2" style="width:100%">
                                 <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[3]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[3]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="4" name="availability_week_days[4]">
                                    <input type="checkbox" id="week_day_4" value="Y" name="availability_checked[4]">
                                    <label for="week_day_4">
                                       <span class="border-radius-3"></span>
                                       <p>Thursday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[4]" class="sel2" style="width:100%">
                                 <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[4]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[4]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>

                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="5" name="availability_week_days[5]">
                                    <input type="checkbox" id="week_day_5" value="Y" name="availability_checked[5]">
                                    <label for="week_day_5">
                                       <span class="border-radius-3"></span>
                                       <p>Friday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[5]" class="sel2" style="width:100%">
                                 <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[5]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[5]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="6" name="availability_week_days[6]">
                                    <input type="checkbox" id="week_day_6" value="Y" name="availability_checked[6]">
                                    <label for="week_day_6">
                                       <span class="border-radius-3"></span>
                                       <p>Saturday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[6]" class="sel2" style="width:100%">
                                 <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[6]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[6]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>

                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="0" name="availability_week_days[0]">
                                    <input type="checkbox" id="week_day_0" value="Y" name="availability_checked[0]">
                                    <label for="week_day_0">
                                       <span class="border-radius-3"></span>
                                       <p>Sunday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[0]" class="sel2" style="width:100%">
                                 <option value="">-- Select Zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[0]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>"><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[0]" class="sel2" style="width:100%">
                                 <option value="">-- Select end time --</option>
                              </select>
                           </div>
                        </div>

                     </div>

                     <div class="tab-pane" id="tab5">
                        <div class="col-sm-4">
                           <h3>Passport</h3>
                           <input type="hidden" name="document_type_ids[0]" value="1">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Passport Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" name="document_number[0]">
                                 <input type="hidden" value="Passport" name="document_name[0]">
                                 <span class="error-message"></span>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Passport Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input type="date" name="document_expiry_date[0]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <p>Passport Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" type="number" name="grace_period[0]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Passport Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[0]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <div id="image_error[0]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[0]" value="1" id="document_image[0]">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <h3>Visa</h3>
                           <input type="hidden" name="document_type_ids[1]" value="2">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Visa Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" name="document_number[1]">
                                 <input type="hidden" value="Visa" name="document_name[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Visa Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input type="date" name="document_expiry_date[1]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Visa Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" type="number" name="grace_period[1]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Visa Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[1]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <div id="image_error[1]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[1]" value="2" id="document_image[1]">
                                 </div>
                              </div>
                           </div>

                        </div>

                        <div class="col-sm-4">
                           <h3>Labour Card</h3>
                           <input type="hidden" name="document_type_ids[2]" value="3">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Labour Card Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" name="document_number[2]">
                                 <input type="hidden" value="Labour Card" name="document_name[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Labour Card Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input type="date" name="document_expiry_date[2]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Labour Card Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" type="number" name="grace_period[2]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Labour Card Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[2]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <div id="image_error[2]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[2]" value="2" id="document_image[2]">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <h3>Emirate ID</h3>
                           <input type="hidden" name="document_type_ids[4]" value="5">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Emirate Card Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" name="document_number[4]">
                                 <input type="hidden" value="Emirates ID" name="document_name[4]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Emirate Card Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input type="date" name="document_expiry_date[4]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Emirate Card Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" type="number" name="grace_period[4]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Emirate Card Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[3]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <div id="image_error[3]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[4]" value="4" id="document_image[3]">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-sm-12">
                        <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                        <button type="button" class="n-btn" id="employee-save-btn">Save</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script>
   var cropper;

   function showCropper(cropWidth, cropHeight) {
      cropper = new Cropper(image, {
         aspectRatio: cropWidth / cropHeight,
         viewMode: 0,
      });
      fancybox_show('crop-popup', {
         width: 600
      });
   }
   window.addEventListener('DOMContentLoaded', function() {
      var selector = null;
      var selectorWidth = null;
      var selectorHeight = null;
      var image = document.getElementById('image');
      [...document.querySelectorAll('.input-image')].forEach(function(item) {
         item.addEventListener('change', function(e) {
            selector = e.target.getAttribute('data-selector');
            selectorWidth = e.target.getAttribute('data-crop-width');
            selectorHeight = e.target.getAttribute('data-crop-height');
            var files = e.target.files;
            var done = function(url) {
               item.value = '';
               image.src = url;
               showCropper(selectorWidth, selectorHeight);
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
               file = files[0];
               if (URL) {
                  done(URL.createObjectURL(file));
               } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function(e) {
                     done(reader.result);
                  };
                  reader.readAsDataURL(file);
               }
            }
         });
      });
      document.getElementById('crop').addEventListener('click', function() {
         var canvas;
         if (cropper) {
            canvas = cropper.getCroppedCanvas({
               width: selectorWidth,
               height: selectorHeight,
            });
            var src = canvas.toDataURL();
            document.getElementById(selector + "_base64").value = src; // reflect changed value
            document.getElementById(selector + "_show").src = src; // show changed image
         }
         closeCropper();
      });
   });

   function closeCropper() {
      cropper.destroy();
      cropper = null;
      $.fancybox.close();
   }
   // ******************************
</script>