<script>
   var employee_skills = <?php echo json_encode($employee_skills); ?>;
</script>
<div id="crop-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span id="b-maid-name">Image Cropper</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
      </div>
      <div id="" class="col-12 p-0">
         <div class="modal-body">
            <div class="img-container">
               <img id="image" src="#">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
            <button type="button" class="n-btn mb-0" id="crop">Crop</button>
         </div>
      </div>
   </div>
</div>
<div class="row m-0">
   <div class="col-sm-12">
      <div class="widget">
         <div class="widget-header">
            <div class="book-nav-top">
               <ul>
                  <li>
                     <i class="icon-user"></i>
                     <h3>Edit Employee - <?= $employee->maid_name; ?></h3>
                  </li>
                  <li class="mr-0 float-right">
                     <div class="topiconnew border-0 green-btn">
                        <a href="<?php echo base_url('employee/list'); ?>" title="Employee List"> <i class="fa fa-list"></i></a>
                     </div>
                  </li>
                  <div class="clear"></div>
               </ul>
            </div>
         </div>
         <div class="widget-content">
            <div class="tabbable">
               <ul class="nav nav-tabs pb-4">
                  <li>
                     <a data-target="#tab1" data-toggle="tab" data-tab="tab1">Basic Details</a>
                  </li>
                  <li>
                     <a data-target="#tab2" data-toggle="tab" data-tab="tab2">Address</a>
                  </li>
                  <li>
                     <a data-target="#tab3" data-toggle="tab" data-tab="tab3">Skills</a>
                  </li>
                  <li <?= $employee->employee_type_id == 1 ? "" : 'style="display:none;"' ?>>
                     <a data-target="#tab4" data-toggle="tab" data-tab="tab4">Daily Preference</a>
                  </li>
                  <!-- <li>
                     <a data-target="#tab5" data-toggle="tab" data-tab="tab5">Documents</a>
                  </li> -->
                  <li>
                     <a data-target="#tab6" data-toggle="tab" data-tab="tab6">Complaint</a>
                  </li>
               </ul>
               <form id="edit_employee_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                  <input type="hidden" value="<?= $employee->maid_id ?>" name="employee_id">
                  <div class="tab-content">
                     <div class="tab-pane" id="tab1">
                        <div class="col-sm-4">
                           <div class="row m-0 n-field-main">
                              <div class="col-sm-12 p-0 n-field-box">
                                 <div class="row m-0">
                                    <p>Employee Type<rf>
                                    </p>
                                    <div class="col-sm-3 n-radi-check-main p-0" <?= $employee->employee_type_id == 1 ? "" : 'style="display:none;"' ?>>
                                       <input type="radio" value="1" id="employee_type_1" name="employee_type_id" <?= $employee->employee_type_id == 1 ? "checked" : "disabled" ?>>
                                       <label for="employee_type_1">
                                          <span class="border-radius-3"></span>
                                          <p>Maid</p>
                                       </label>
                                    </div>
                                    <div class="col-sm-3 n-radi-check-main p-0" <?= $employee->employee_type_id == 2 ? "" : 'style="display:none;"' ?>>
                                       <input type="radio" value="2" id="employee_type_2" name="employee_type_id" <?= $employee->employee_type_id == 2 ? "checked" : "disabled" ?>>
                                       <label for="employee_type_2">
                                          <span class="border-radius-3"></span>
                                          <p>Driver</p>
                                       </label>
                                    </div>
                                    <div class="col-sm-3 n-radi-check-main p-0" <?= $employee->employee_type_id == 3 ? "" : 'style="display:none;"' ?>>
                                       <input type="radio" value="3" id="employee_type_3" name="employee_type_id" <?= $employee->employee_type_id == 3 ? "checked" : "disabled" ?>>
                                       <label for="employee_type_3">
                                          <span class="border-radius-3"></span>
                                          <p>Accountant</p>
                                       </label>
                                    </div>
                                    <div class="col-sm-3 n-radi-check-main p-0" <?= $employee->employee_type_id == 4 ? "" : 'style="display:none;"' ?>>
                                       <input type="radio" value="4" id="employee_type_4" name="employee_type_id" <?= $employee->employee_type_id == 4 ? "checked" : 'disabled' ?>>
                                       <label for="employee_type_4">
                                          <span class="border-radius-3"></span>
                                          <p>Mechanic</p>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Employee Name<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" name="employee_name" autocomplete="off" value="<?= $employee->maid_name; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Profile Image</p>
                              <label style="width: 100px;">
                                 <img class="img-thumbnail" id="employee_avatar_show" src="<?= $employee->maid_photo_file != "" ? base_url('uploads/images/avatars/employee/' . $employee->maid_photo_file) : base_url('uploads/images/default/maid-avatar-upload.png'); ?>" style="width: 100%;">
                                 <input type="file" class="input-image" accept="image/*" data-selector="employee_avatar" data-crop-width="250" data-crop-height="250">
                              </label>
                              <input type="hidden" name="employee_avatar_base64" id="employee_avatar_base64">
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Nationality<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <select name="nationality" class="sel2" style="width:100%">
                                    <option value="">-- Select nationality --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>" <?= $employee->country_id == $country->id ? "selected" : "" ?>><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Gender</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <select name="gender_id" class="sel2" style="width:100%">
                                    <option value="">-- Select gender --</option>
                                    <?php foreach ($genders as $gender) : ?>
                                       <option value="<?= $gender->gender_id ?>" <?= $employee->gender_id == $gender->gender_id ? "selected" : "" ?>><?= $gender->gender ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Present Address<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <textarea class="" rows="1" name="present_address"><?= $employee->maid_present_address; ?></textarea>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Permanent Address</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <textarea class="" rows="1" name="permanent_address"><?= $employee->maid_permanent_address; ?></textarea>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Date of Joining<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="date_of_joining" value="<?= DateTime::createFromFormat('Y-m-d', $employee->maid_joining)->format('d/m/Y') ?>" readonly>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number 1<rf>
                              </p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="mobile_number_1" value="<?= $employee->maid_mobile_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number 2</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="mobile_number_2" value="<?= $employee->maid_mobile_2; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp No.</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" name="whatsapp_number_1" value="<?= $employee->maid_whatsapp_no_1; ?>">
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="row m-0 n-field-main" id="service-types">
                              <p>Services<rf>
                              </p>
                              <?php foreach ($services as $key => $service) : ?>
                                 <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
                                    <div class="col-sm-12 pr-0 pl-0 n-radi-check-main">
                                       <input id="service_<?= $key ?>" type="checkbox" value="<?php echo $service['service_type_id'] ?>" name="services_ids[]" <?= in_array($service['service_type_id'], $service_types) ? "checked" : "" ?>>
                                       <label for="service_<?= $key ?>">
                                          <span class="border-radius-3"></span>
                                          <p><?php echo $service['service_type_name'] ?></p>
                                       </label>
                                    </div>
                                 </div>
                              <?php endforeach; ?>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Notes</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <textarea rows="5" name="notes"><?= $employee->maid_notes; ?></textarea>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Username</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="text" autocomplete="off" value="<?= $employee->username; ?>" name="username" readonly onfocus="this.removeAttribute('readonly');">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Password</p>
                              <div class="col-sm-12 p-0 n-field-box">
                                 <input type="password" autocomplete="off" value="<?= $employee->password; ?>" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main payment_mode_customer" style="">
                              <p>Priority Number</p>
                              <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                 <input type="text" name="priority_number" value="<?= $employee->maid_priority; ?>">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="tab2">
                        <div class="col-sm-3">
                           <h3>Local Address</h3>
                           <input type="hidden" name="address_type_ids[0]" value="1">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[0]" value="<?= @$addresses[1]->name; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[0]" value="<?= @$addresses[1]->house_number; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[0]" value="<?= @$addresses[1]->mobile_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[0]" value="<?= @$addresses[1]->whatsapp_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[0]" value="<?= @$addresses[1]->relationship; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[0]" value="<?= @$addresses[1]->street; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[0]" value="<?= @$addresses[1]->town; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[0]" value="<?= @$addresses[1]->city; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[0]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>" <?= @$addresses[1]->country_id == $country->id ? "selected" : "" ?>><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <h3>Permanent Address</h3>
                           <input type="hidden" name="address_type_ids[1]" value="2">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[1]" value="<?= @$addresses[2]->name; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[1]" value="<?= @$addresses[2]->house_number; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[1]" value="<?= @$addresses[2]->mobile_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[1]" value="<?= @$addresses[2]->whatsapp_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[1]" value="<?= @$addresses[2]->relationship; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[1]" value="<?= @$addresses[2]->street; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[1]" value="<?= @$addresses[2]->town; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[1]" value="<?= @$addresses[2]->city; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[1]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>" <?= @$addresses[2]->country_id == $country->id ? "selected" : "" ?>><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <h3>Emergency Contact - Local</h3>
                           <input type="hidden" name="address_type_ids[2]" value="3">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[2]" value="<?= @$addresses[3]->name; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[2]" value="<?= @$addresses[3]->house_number; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[2]" value="<?= @$addresses[3]->mobile_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[2]" value="<?= @$addresses[3]->whatsapp_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[2]" value="<?= @$addresses[3]->relationship; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[2]" value="<?= @$addresses[3]->street; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[2]" value="<?= @$addresses[3]->town; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[2]" value="<?= @$addresses[3]->city; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[2]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>" <?= @$addresses[3]->country_id == $country->id ? "selected" : "" ?>><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <h3>Emergency Contact - Home Country</h3>
                           <input type="hidden" name="address_type_ids[3]" value="4">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Name<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_contact_name[3]" value="<?= @$addresses[4]->name; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>House Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_house_no[3]" value="<?= @$addresses[4]->house_number; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Mobile Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_mobile_no_1[3]" value="<?= @$addresses[4]->mobile_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>WhatsApp Number</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_whatsapp_no_1[3]" value="<?= @$addresses[4]->whatsapp_no_1; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Relationship with Employee</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_relationship[3]" value="<?= @$addresses[4]->relationship; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Street</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_street[3]" value="<?= @$addresses[4]->street; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Town</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_town[3]" value="<?= @$addresses[4]->town; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>City</p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input type="text" name="address_type_city[3]" value="<?= @$addresses[4]->city; ?>">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Country<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="address_type_country_id[3]" class="sel2" style="width:100%">
                                    <option value="">-- Select country --</option>
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?= $country->id ?>" <?= @$addresses[4]->country_id == $country->id ? "selected" : "" ?>><?= $country->country ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="tab3">
                        <div class="col-sm-4">
                           <div class="row">
                              <div class="col-xs-6 col-sm-4">
                                 <select id="skill_id" class="sel2" style="width:100%">
                                    <option value="">-- Select Skill --</option>
                                    <?php foreach ($skills as $skill) : ?>
                                       <option value="<?= $skill->skill_id ?>"><?= $skill->skill ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                              <div class="col-xs-6 col-sm-4">
                                 <select id="skill_rating_id" class="sel2" style="width:100%">
                                    <option value="">-- Select Level --</option>
                                    <?php foreach ($rating_levels as $rating_level) : ?>
                                       <option value="<?= $rating_level->rating_level_id ?>"><?= $rating_level->rating_level ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                              <div class="col-xs-6 col-sm-4">
                                 <button type="button" class="n-btn purple-btn" data-action="select-skill"><i class="fa fa-plus" aria-hidden="true"></i></button>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="row">
                              <div class="col-sm-12 p-0" id="skills-holder">
                                 <!-- -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="tab4">
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="1" name="availability_week_days[1]">
                                    <input type="checkbox" id="week_day_1" value="Y" name="availability_checked[1]" <?= @$maid_availabilities[1] ? "checked" : "" ?>>
                                    <label for="week_day_1">
                                       <span class="border-radius-3"></span>
                                       <p>Monday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[1]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[1]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[1]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[1]) && @$maid_availabilities[1]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[1]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[1]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>


                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="2" name="availability_week_days[2]">
                                    <input type="checkbox" id="week_day_2" value="Y" name="availability_checked[2]" <?= @$maid_availabilities[2] ? "checked" : "" ?>>
                                    <label for="week_day_2">
                                       <span class="border-radius-3"></span>
                                       <p>Tuesday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[2]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[2]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[2]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[2]) && @$maid_availabilities[2]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[2]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[2]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="3" name="availability_week_days[3]">
                                    <input type="checkbox" id="week_day_3" value="Y" name="availability_checked[3]" <?= @$maid_availabilities[3] ? "checked" : "" ?>>
                                    <label for="week_day_3">
                                       <span class="border-radius-3"></span>
                                       <p>Wednessday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[3]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[3]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[3]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[3]) && @$maid_availabilities[3]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[3]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[3]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="4" name="availability_week_days[4]">
                                    <input type="checkbox" id="week_day_4" value="Y" name="availability_checked[4]" <?= @$maid_availabilities[4] ? "checked" : "" ?>>
                                    <label for="week_day_4">
                                       <span class="border-radius-3"></span>
                                       <p>Thursday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[4]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[4]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[4]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[4]) && @$maid_availabilities[4]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[4]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[4]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="5" name="availability_week_days[5]">
                                    <input type="checkbox" id="week_day_5" value="Y" name="availability_checked[5]" <?= @$maid_availabilities[5] ? "checked" : "" ?>>
                                    <label for="week_day_5">
                                       <span class="border-radius-3"></span>
                                       <p>Friday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[5]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[5]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[5]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[5]) && @$maid_availabilities[5]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[5]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[5]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="6" name="availability_week_days[6]">
                                    <input type="checkbox" id="week_day_6" value="Y" name="availability_checked[6]" <?= @$maid_availabilities[6] ? "checked" : "" ?>>
                                    <label for="week_day_6">
                                       <span class="border-radius-3"></span>
                                       <p>Saturday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[6]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[6]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[6]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[6]) && @$maid_availabilities[6]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[6]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[6]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>

                        <div>
                           <div class="col-sm-3 mb-2">
                              <div class="n-field-box">
                                 <div class="n-radi-check-main">
                                    <input type="hidden" value="0" name="availability_week_days[0]">
                                    <input type="checkbox" id="week_day_0" value="Y" name="availability_checked[0]" <?= @$maid_availabilities[0] ? "checked" : "" ?>>
                                    <label for="week_day_0">
                                       <span class="border-radius-3"></span>
                                       <p>Sunday</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <div class="col-sm-8 p-0 n-field-box">
                                 <select name="availability_zone[0]" class="sel2" style="width:100%">
                                    <option value="">-- Select zone --</option>
                                    <?php foreach ($zones as $zone) : ?>
                                       <option value="<?= $zone->zone_id ?>" <?= @$maid_availabilities[0]->zone_id == $zone->zone_id ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_from[0]" class="sel2" style="width:100%">
                                 <option value="">-- Select start time --</option>
                                 <?php foreach ($time_slots as $time => $label) : ?>
                                    <option value="<?= $time; ?>" <?= isset($maid_availabilities[0]) && @$maid_availabilities[0]->time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                           <div class="col-sm-3 mb-2">
                              <select name="availability_time_to[0]" class="sel2" style="width:100%">
                                 <option value="<?= @$maid_availabilities[0]->time_to; ?>" selected>-- Select end time --</option>
                              </select>
                           </div>
                        </div>
                     </div>

                     <div class="tab-pane" id="tab5">
                        <div class="col-sm-4">
                           <h3>Passport</h3>
                           <input type="hidden" name="document_type_ids[0]" value="1">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Passport Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" value="<?= $employee_documents[0]->document_number ?>" name="document_number[0]">
                                 <input type="hidden" value="Passport" name="document_name[0]">
                                 <span class="error-message"></span>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Passport Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <?php
                                    $passportexpiry = date('Y-m-d', strtotime($employee_documents[0]->expiry_date));
                                    ?>
                                    <input type="date" value="<?= $passportexpiry ?>" name="document_expiry_date[0]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <p>Passport Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" value="<?= $employee_documents[0]->grace_period ?>" type="number" name="grace_period[0]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Passport Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[0]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <?php if (isset($employee_documents[0]->file_name)) { ?>
                                       <center style="line-height: 20px; width: 200px">
                                          <a href="<?= base_url('./uploads/employee/documents/' . $employee_documents[0]->file_name); ?>" target="_blank">
                                             <img class="thumbnail" src="<?= base_url('./uploads/employee/documents/' . $employee_documents[0]->file_name); ?>" />
                                          </a>
                                       </center>
                                    <?php } ?>
                                    <div id="image_error[0]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[0]" value="1" id="document_image[0]">

                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <h3>Visa</h3>
                           <input type="hidden" name="document_type_ids[1]" value="2">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Visa Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" value="<?= $employee_documents[1]->document_number ?>" name="document_number[1]">
                                 <input type="hidden" value="Visa" name="document_name[1]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Visa Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                 <?php
                                    $visaexpiry = date('Y-m-d', strtotime($employee_documents[1]->expiry_date));
                                    ?>
                                    <input type="date" value="<?= $visaexpiry ?>" name="document_expiry_date[1]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Visa Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" type="number" value="<?= $employee_documents[1]->grace_period ?>" name="grace_period[1]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Visa Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[1]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <?php if (isset($employee_documents[1]->file_name)) { ?>
                                       <center style="line-height: 20px; width: 200px">
                                          <a href="<?= base_url('./uploads/employee/documents/' . $employee_documents[1]->file_name); ?>" target="_blank">
                                             <img class="thumbnail" src="<?= base_url('./uploads/employee/documents/' . $employee_documents[1]->file_name); ?>" />
                                          </a>
                                       </center>
                                    <?php } ?>
                                    <div id="image_error[1]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[1]" value="2" id="document_image[1]">
                                 </div>
                              </div>
                           </div>

                        </div>

                        <div class="col-sm-4">
                           <h3>Labour Card</h3>
                           <input type="hidden" name="document_type_ids[2]" value="3">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Labour Card Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" value="<?= $employee_documents[2]->document_number ?>" name="document_number[2]">
                                 <input type="hidden" value="Labour Card" name="document_name[2]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Labour Card Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                 <?php
                                    $labourexpiry = date('Y-m-d', strtotime($employee_documents[2]->expiry_date));
                                    ?>
                                    <input type="date" value="<?= $labourexpiry?>" name="document_expiry_date[2]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Labour Card Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" value="<?= $employee_documents[2]->grace_period ?>" type="number" name="grace_period[2]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Labour Card Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[2]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <?php if (isset($employee_documents[2]->file_name)) { ?>
                                       <center style="line-height: 20px; width: 200px">
                                          <a href="<?= base_url('./uploads/employee/documents/' . $employee_documents[2]->file_name); ?>" target="_blank">
                                             <img class="thumbnail" src="<?= base_url('./uploads/employee/documents/' . $employee_documents[2]->file_name); ?>" />
                                          </a>
                                       </center>
                                    <?php } ?>
                                    <div id="image_error[2]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[2]" value="2" id="document_image[2]">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <h3>Emirate ID</h3>
                           <input type="hidden" name="document_type_ids[4]" value="5">
                           <hr>
                           <div class="row m-0 n-field-main">
                              <p>Emirate Card Number<rf>
                              </p>
                              <div class="col-sm-8 p-0 n-field-box">
                                 <input class="no-arrows" type="number" value="<?= $employee_documents[3]->document_number ?>" name="document_number[4]">
                                 <input type="hidden" value="Emirates ID" name="document_name[4]">
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Emirate Card Expiry Date</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                 <?php
                                    $emiratexpiry = date('Y-m-d', strtotime($employee_documents[3]->expiry_date));
                                    ?>
                                    <input type="date" value="<?= $emiratexpiry?>" name="document_expiry_date[4]">
                                 </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <p>Emirate Card Grace Period</p>
                              <rf>
                                 <div class="col-sm-8 p-0 n-field-box">
                                    <input class="no-arrows" value="<?= $employee_documents[3]->grace_period ?>" type="number" name="grace_period[4]">
                                 </div>
                           </div>

                           <div class="row m-0 n-field-main">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <p>Emirate Card Image<rf>
                                    </p>
                                 </div>
                                 <div class="custom-file mt-2">
                                    <label class="custom-file-label " for="document_image[3]">Choose file</label>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <?php if (isset($employee_documents[3]->file_name)) { ?>
                                       <center style="line-height: 20px; width: 200px">
                                          <a href="<?= base_url('./uploads/employee/documents/' . $employee_documents[3]->file_name); ?>" target="_blank">
                                             <img class="thumbnail" src="<?= base_url('./uploads/employee/documents/' . $employee_documents[3]->file_name); ?>" />
                                          </a>
                                       </center>
                                    <?php } ?>
                                    <div id="image_error[3]"></div>
                                    <input type="file" class="custom-file-input" name="document_image[4]" value="4" id="document_image[3]">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="tab6" class="tab-pane">
                        <table id="cal_comp" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Sl.No.
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Call ID
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Customer ID
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Complaint ID
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Customer Name
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Complaint Added Date
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Complaint Service Date
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Complaint
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Action Taken
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Status
                          </center>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($call_complaint_report)) {
                        $i = 0;
                        foreach ($call_complaint_report as $ccr) {

                          if ($ccr->complaint_against == 'D') {
                            $comp_against = 'Driver';
                          } else if ($ccr->complaint_against == 'U') {
                            $comp_against = 'User';
                          } else if ($ccr->complaint_against == 'C') {
                            $comp_against = 'Staff';
                          }

                        
                     $status_style = ''; 


                     switch ($ccr->status) {
                        case 'In Progress':
                           $status_style = 'background-color: #b4b304; border-radius: 20px; color: white; font-weight: bold; padding: 5px 8px;';
                           break;
                        case 'Completed':
                           $status_style = 'background-color: #00ff00; border-radius: 20px; color: white; font-weight: bold; padding: 5px 8px;';
                           break;
                        case 'Assigned':
                           $status_style = 'background-color: #0000ff; border-radius: 20px; color: white; font-weight: bold; padding: 5px 8px;';
                           break;
                        case 'On Hold':
                           $status_style = 'background-color: #ffa500; border-radius: 20px; color: white; font-weight: bold; padding: 5px 8px;';
                           break;
                        case 'Rejected':
                           $status_style = 'background-color: #ff0000; border-radius: 20px; color: white; font-weight: bold; padding: 5px 8px;';
                           break;
                        default:
                           $status_style = ''; 
                        }
                      ?>
                          <tr>
                            <td style="line-height: 18px; width: 20px">
                              <center><?= ++$i ?></center>
                            </td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->call_history_id ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->customer_code ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint_no ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->customer_name ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= date('d-m-Y', strtotime($ccr->added_date_time)) ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= date('d-m-Y', strtotime($ccr->service_date)) ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->action_taken ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;">
                              <div style="display: flex; justify-content: space-between; align-items: center;">
                              <span style="<?= $status_style ?>"><?= $ccr->status ?></span>
                                <button class="bg-primary edit-status-btn" data-call-history-id="<?= $ccr->call_history_id ?>">Edit</button>
                              </div>
                            </td>
                          </tr>
                        <?php }
                      } else { ?>
                        <tr>
                          <td style="line-height: 18px;" colspan="13">
                            <center>No Records!</center>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>

                  </table>
                        </div>

                     <div class="col-sm-12">
                        <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                        <button type="button" class="n-btn" id="employee-save-btn">Update</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->

<div id="edit-status-popup" style="display: none;height:150px;width:350px">
  <div class="modal-body">
    <form method="post">
      <input type="hidden" id='call_history_id_pop_up' name="call_history_id" value="">
      <label for="status">Select Status:</label>
      <select name="status" id="status">
        <option value="">---select Status---</option>
        <option value="In Progress">In Progress</option>
        <option value="Completed">Completed</option>
        <option value="Assigned">Assigned</option>
        <option value="On Hold">On Hold</option>
        <option value="Rejected">Rejected</option>
      </select>
  </div>
  <div class="modal-footer">
    <button class="btn btn-success update-btn-status" type="submit">Update Status</button>
    <button type="button" class="btn btn-danger cancel-btn">Cancel</button>
  </div>
  </form>
</div>


<script>
   $(function() {
      let current = window.location.href;
      $('#primary_nav_wrap li a').each(function() {
         var $this = $(this);
         // if the current path is like this link, make it active
         if ($this.attr('href') === _base_url + 'employee') {
            $this.addClass('active');
         }
      })
   })
   var cropper;

   function showCropper(cropWidth, cropHeight) {
      cropper = new Cropper(image, {
         aspectRatio: cropWidth / cropHeight,
         viewMode: 0,
      });
      fancybox_show('crop-popup', {
         width: 600
      });
   }
   window.addEventListener('DOMContentLoaded', function() {
      var selector = null;
      var selectorWidth = null;
      var selectorHeight = null;
      var image = document.getElementById('image');
      [...document.querySelectorAll('.input-image')].forEach(function(item) {
         item.addEventListener('change', function(e) {
            selector = e.target.getAttribute('data-selector');
            selectorWidth = e.target.getAttribute('data-crop-width');
            selectorHeight = e.target.getAttribute('data-crop-height');
            var files = e.target.files;
            var done = function(url) {
               item.value = '';
               image.src = url;
               showCropper(selectorWidth, selectorHeight);
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
               file = files[0];
               if (URL) {
                  done(URL.createObjectURL(file));
               } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function(e) {
                     done(reader.result);
                  };
                  reader.readAsDataURL(file);
               }
            }
         });
      });
      document.getElementById('crop').addEventListener('click', function() {
         var canvas;
         if (cropper) {
            canvas = cropper.getCroppedCanvas({
               width: selectorWidth,
               height: selectorHeight,
            });
            var src = canvas.toDataURL();
            document.getElementById(selector + "_base64").value = src; // reflect changed value
            document.getElementById(selector + "_show").src = src; // show changed image
         }
         closeCropper();
      });
   });

   function closeCropper() {
      cropper.destroy();
      cropper = null;
      $.fancybox.close();
   }

   $(document).ready(function(){
      var callcomplaint_report = <?php echo json_encode($call_complaint_report); ?>;

    if (callcomplaint_report.length > 0) {
         $('#cal_comp').DataTable();
      }
$('.edit-status-btn').click(function() {
   event.preventDefault();
  var call_history_id = $(this).data('call-history-id');
  $('#call_history_id_pop_up').val(call_history_id);

  $.fancybox.open({
    autoCenter: true,
    fitToView: true,
    scrolling: false,
    openEffect: 'fade',
    openSpeed: 100,
    helpers: {
      overlay: {
        css: {
          'background': 'rgba(0, 0, 0, 0.3)'
        },
        closeClick: false
      }
    },
    width: '800px',
    height: '800px',
    padding: 0,
    content: $('#edit-status-popup'),
  });
});

$(document).on('click', '.cancel-btn', function() {
  $.fancybox.close();
});

$('#edit-status-popup form').on('submit', function(e) {
  $('.update-btn-status').show();
  e.preventDefault();
  var status=$('#status').val();
  if(status==''){
    alert('please select a status');
    return false;
  }
  if (status !='') {
    $.ajax({
      type: 'POST',
      url: _base_url + 'call_management/update_status_complaint',
      data: $('#edit-status-popup form').serialize(),
      dataType: 'json',
      success: function(response) {
        console.log(response);
        if (response.status == 'success') {
          $.fancybox.close();
          toast('success', response.message);
          setTimeout(function() {
            location.reload();
          }, 2500);

        } else {
          toast('error', response.message);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
        toast('error', errorThrown);
      }
    });
  }
});

});
</script>