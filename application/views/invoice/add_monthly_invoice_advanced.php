<div class="row">
    <div class="span12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Create Advanced Monthly Invoice</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url();?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url('invoice/list/monthly-advanced'); ?>"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Invoice List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                    </ul>
                    <br>
                    <form class="form-horizontal" id="invoice-ma-form" method="post" enctype="multipart/form-data">      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Invoice added Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                <fieldset>            
                                    <div class="span10">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Customer&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="customer" id="customers_vh_rep_new" class="sel2 span7" required>
                                                                <option value="">Select customer</option>
                                                                <?php
                                                                foreach($customerlist as $c_val)
                                                                {
                                                                    
                                                                ?>
                                                                <option value="<?php echo $c_val->customer_id; ?>"><?php echo $c_val->customer_name; ?></option>
                                                                <?php
                                                                }
                                                                ?>                                                                  
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Invoice Month &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="invoice_month" name="invoice_month" value="<?php echo date('Y-m-01')?>" readonly required>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->
                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Issue Date &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="invoice_issue_date" name="invoice_issue_date" value="<?php echo date('Y-m-d')?>" required>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">                                         
                                                        <label class="control-label" for="firstname">Due Date &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span7" id="invoice_due_date" name="invoice_due_date" value="<?php echo date('Y-m-d',strtotime('+14 days'))?>" required>
                                                        </div> <!-- /controls -->               
                                                    </div> <!-- /control-group -->

                                                    <div class="form-actions" >
                                                        <input type="submit" class="btn mm-btn" value="Submit" name="add_ma_invoice" onclick="return validate_cust_monthly_inv_add();">
                                                    </div> 
                                                </fieldset>
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->
                                </fieldset>   
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
