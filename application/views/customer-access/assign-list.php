<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this package ?</h3>
      <input type="hidden" id="disable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this package ?</h3>
      <input type="hidden" id="enable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Access</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="access-assign-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Customer</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="customer_id" id="customer_id" data-placeholder="Select customer" style="width:100%">
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Type</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="access_type" id="access_type" class="sel2" data-placeholder="Select type" style="width:100%">
                <option value="">-- Select type --</option>
                <option value="1">Key</option>
                <option value="2">Card</option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="access" id="access" class="sel2" data-placeholder="Select access" style="width:100%">
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Receive Notes</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea type="text" name="received_notes" id="received_notes"></textarea>
            </div>
          </div>
          <p class="text-danger" id="form-error">&nbsp;</p>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" id="access-assign-btn">Assign</button>
    </div>
    </form>
  </div>
</div>
<div id="view-access-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">View Access Assign</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="access-view-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Customer</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select data-placeholder="Select customer" style="width:100%">
                <option value="" class="customer_name"></option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Type</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select data-placeholder="Select type" style="width:100%">
                <option value="" class="access_type"></option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select data-placeholder="Select access" style="width:100%">
                <option value="" class="access_code"></option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Received Key Notes</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea type="text" class="received_notes" readonly></textarea>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Returned Key Notes</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea type="text" class="returned_notes" readonly></textarea>
            </div>
          </div>
          <p class="text-danger" id="form-error">&nbsp;</p>
        </div>
    </div>
    </form>
  </div>
</div>
<div id="return-access-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Return Access <span class="access_type"></span> <span class="access_code"></span></span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="access-return-form">
        <input name="return_access_id" id="return_access_id" type="hidden" />
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Customer</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select data-placeholder="Select customer" style="width:100%" readonly>
                <option id="customer_name"></option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Type</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select style="width:100%" readonly>
                <option class="access_type"></option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select style="width:100%" readonly>
                <option class="access_code"></option>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Returned By Employee</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="returned_by_employee_id" style="width:100%" data-placeholder="Select employee">
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Return Notes</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea type="text" name="returned_notes" id="returned_notes"></textarea>
            </div>
          </div>
          <p class="text-danger" id="return-form-error">&nbsp;</p>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" id="access-return-btn">Return</button>
    </div>
    </form>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Access Assign</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete?</h3>
      <input type="hidden" id="delete_keyid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="delete-success-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Deleted</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Deleted Successfully!</h3>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <ul>
          <li><i class="icon-th-list"></i>
            <h3>Key and Card Assignment</h3>
          </li>

          <!--<li><select style="width:160px;" id="all-maids" onchange="statusFilter(this.value)">
          <option value="" <?php echo $active == null ? 'selected' : ''; ?>>All</option>
          <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Active</option>
          <option value="0" <?php echo $status === '0' ? 'selected' : ''; ?>>Returned</option>
        </select></li>-->
          <li>
            <select style="width: 160px; " name="access_type_filter" id="access_type_filter" class="span3">
              <option value="" <?php echo $accesstype == 0 ? 'selected="selected"' : ''; ?>>All</option>
              <option value="1" <?php echo $accesstype == 1 ? 'selected="selected"' : ''; ?>>Key</option>
              <option value="2" <?php echo $accesstype == 2 ? 'selected="selected"' : ''; ?>>Card</option>
            </select>

          </li>

          <li><select style="width:160px;" id="all-maids" onchange="activefilter(this.value)">
              <option value="" <?php echo empty($status) ? 'selected' : ''; ?>>Select</option>
              <option value="0" <?php echo $status == '0' ? 'selected' : ''; ?>>Active</option>
              <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Inactive</option>
            </select></li>

          <li class="mr-0 float-right">

            <div class="topiconnew border-0 green-btn">
              <a href="#" title="Assign Access" onclick="assignAccessPopUp()"> <i class="fa fa-plus"></i></a>
            </div>

          </li>
        </ul>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="package-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th style="width:45px">
                <center>Sl. No.</center>
              </th>
              <th>
                Access Code
              </th>
              <th>
                Type
              </th>
              <th>
                Customer
              </th>
              <th>
                Received On
              </th>
              <th>
                Received By
              </th>
              <th>
                Returned On
              </th>
              <th>
                Return Added By
              </th>
              <th>
                Returned By Employee
              </th>
              <th>
                Assign Status
              </th>
              <th>
                Status
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($rows) > 0) {
              foreach ($rows as $key => $row) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo $row->code ?></td>
                  <td><?php echo $row->access_type ?></td>
                  <td><?php echo $row->customer_name ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $row->received_at)->format('d M Y') ?></td>
                  <td><?php echo $row->received_user ?></td>
                  <td><?php echo $row->returned_at ? DateTime::createFromFormat('Y-m-d H:i:s', $row->returned_at)->format('d M Y') : "-" ?></td>
                  <td><?php echo $row->returned_user ?: "-"; ?></td>
                  <td><?php echo $row->returned_by_employee ?: "-"; ?></td>
                  <td style="line-height: 18px; width: 15px">
                    <?php
                    if ($row->returned_at) {
                      $css_class = 'success';
                    } else {
                      $css_class = 'warning';
                    }
                    ?>
                    <span class="btn-block badge badge-<?php echo $css_class; ?>"><?php echo $row->returned_at ? "Not Assigned" : "Assigned"; ?></span>
                  </td>
                  <td>
                    <?php
                    if ($row->returned_at) {
                      $css_class = 'success';
                    } else {
                      $css_class = 'warning';
                    }
                    ?>
                    <span class="btn-block badge badge-<?php echo $css_class; ?>"><?php echo $row->returned_at ? "Returned" : "Active"; ?></span>
                  </td>
                  <td style="line-height: 18px; width: 158px" class="td-actions">
                    <center>
                      <button class="btn" data-action="return-access" href="#" data-id="<?= $row->id ?>" title="Return Key" <?= $row->returned_at ? "disabled" : ""; ?>><i class="fa fa-undo" aria-hidden="true"></i></button>
                      <a class="n-btn-icon blue-btn" data-id="<?= $row->id ?>" data-action="view-access" title="View"><i class="btn-icon-only fa fa-eye"> </i></a>
                      <?php if ($package['maid_status'] == 2) { ?>
                        <p class="btn btn-danger" style="cursor:default;" title="Deleted"><i class="fa fa-ban"></i></p>
                      <?php } ?>
                      <?php
                      //if(user_authenticate() == 1)
                      if ($this->session->userdata('user_logged_in')['user_admin'] == 'Y') {
                        if ($package['status'] == "Active") {
                      ?>
                          <a href="javascript:void(0)" class="n-btn-icon green-btn" title="Disbale" onclick="confirm_disable_enable_modal(<?php echo $package['package_id'] ?>,'<?= $package['status'] ?>');"><i class="btn-icon-only fa fa-toggle-on"> </i></a>
                        <?php
                        } else if ($package['status'] == "Inactive") {
                        ?>
                          <a href="javascript:void(0)" class="n-btn-icon red-btn" title="Enable" onclick="confirm_disable_enable_modal(<?php echo $package['package_id'] ?>,'<?= $package['status'] ?>');"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                        <?php
                        }
                        //                                         if ($package['maid_status']!=2) {
                        ?>

                        <!--                                        <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="change_status(<?php echo $package['maid_id'] ?>,'2');" title="Delete"><i class="btn-icon-only icon-trash"></i> </a>    -->

                      <?php
                        //                                         }
                      }
                      ?>
                      <!-- <a class="n-btn-icon btn-danger" href="#" data-id="<?= $row->id ?>" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
                      <a href="javascript:;" class="n-btn-icon btn-danger" onclick="confirm_delete_modal(<?php echo $row->id  ?>);"><i class="fa fa-trash" aria-hidden="true"> </i></a>
                    </center>
                  </td>
                </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
  function statusFilter(status) {
    if (status) window.open("package?status=" + status, "_self")
    else window.open("package", "_self")
  }

  function confirm_disable_enable_modal(id, status) {
    if (status == 'Active') {
      $('#disable_id').val(id);
      $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        autoSize: false,
        width: 450,
        height: 'auto',
        helpers: {
          overlay: {
            css: {
              'background': 'rgba(0, 0, 0, 0.3)'
            },
            closeClick: false
          }
        },
        padding: 0,
        closeBtn: false,
        content: $('#disable-popup'),
      });
    } else {
      $('#enable_id').val(id);
      $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        autoSize: false,
        width: 450,
        height: 'auto',
        helpers: {
          overlay: {
            css: {
              'background': 'rgba(0, 0, 0, 0.3)'
            },
            closeClick: false
          }
        },
        padding: 0,
        closeBtn: false,
        content: $('#enable-popup'),
      });
    }
  }

  function confirm_disable() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    status = params.status ? params.status : ''; // "some_value"
    $('body').append($('<form/>').attr({
      'action': _base_url + "package/disable?status=" + status,
      'method': 'post',
      'id': 'form_asdf'
    }).append($('<input/>').attr({
      'type': 'hidden',
      'name': 'id',
      'value': $('#disable_id').val()
    }))).find('#form_asdf').submit();
  }

  function confirm_enable() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    status = params.status ? params.status : ''; // "some_value"
    $('body').append($('<form/>').attr({
      'action': _base_url + "package/enable?status=" + status,
      'method': 'post',
      'id': 'form_asdf'
    }).append($('<input/>').attr({
      'type': 'hidden',
      'name': 'id',
      'value': $('#enable_id').val()
    }))).find('#form_asdf').submit();
  }

  function closeFancy() {
    $.fancybox.close();
  }
  (function(a) {
    $(document).ready(function() {
      var dataTable;

      if ($('#package-list-table').length > 0) {
        dataTable = $('table#package-list-table').DataTable({
          'sPaginationType': 'full_numbers',
          'bSort': true,
          'iDisplayLength': 100,
          'scrollY': true,
          'orderMulti': false,
          'scrollX': true,
          'columnDefs': [{
            'targets': [-1],
            'orderable': false
          }]
        });
      }

      activefilter($('#all-maids').val());

      function activefilter(status) {
        // Clear any existing search
        dataTable.search('').draw();

        // Filter the DataTable based on the selected status
        if (status === '0') {
          dataTable.column(10).search('Active').draw();
        } else if (status === '1') {
          dataTable.column(10).search('Returned').draw();
        }
      }
      $('#all-maids').on('change', function() {
        var status = $(this).val();
        activefilter(status);
      });
    });
  })(jQuery);
</script>