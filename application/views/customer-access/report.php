<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this package ?</h3>
      <input type="hidden" id="disable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this package ?</h3>
      <input type="hidden" id="enable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>

<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Key and Card Report</h3></li>
        <li>
            <span style="margin-left:15px; color: #fff;">Access Type :</span>
            <select style="width: 160px; " name="access_type_filter" id="access_type_filter" class="span3">
                <option value="" <?php echo $accesstype == 0 ? 'selected="selected"' : ''; ?>>All</option>
                <option value="1" <?php echo $accesstype == 1 ? 'selected="selected"' : ''; ?> >Key</option>
                <option value="2" <?php echo $accesstype == 2 ? 'selected="selected"' : ''; ?>>Card</option>
            </select>
            <!-- <input type="text" style="width:150px;" id="keyword-search" Placeholder="Search customer"> -->
            
            </li>
        
        <li class="mr-0 float-right">
        
        <!-- <div class="topiconnew border-0 green-btn">
        	<a href="#" title="Assign Access" onclick="assignAccessPopUp()"> <i class="fa fa-plus"></i></a>
        </div> -->
        
        </li>
        </ul>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="package-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th style="width:45px">
                <center>Sl. No.</center>
              </th>
              <th>
                Access Code
              </th>
              <th>
                Type
              </th>
              <th>
                Assigned Customer
              </th>
              <th>
                Received On
              </th>
              <th>
                Received By
              </th>
              <th>
               Returned On
              </th>
              <th>
                Return Added By
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($rows) > 0) {
              foreach ($rows as $key => $row) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo $row->code ?></td>
                  <td><?php echo $row->access_type ?></td>
                  <td><?php echo $row->customer_name ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s',$row->received_at)->format('d M Y') ?></td>
                  <td><?php echo $row->received_user ?></td>
                  <td><?php echo $row->returned_at ? DateTime::createFromFormat('Y-m-d H:i:s',$row->returned_at)->format('d M Y') : "-" ?></td>
                  <td><?php echo $row->returned_user ?: "-"; ?></td>
                </tr>
              <?php
              }
            }
              ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>

function confirm_disable_enable_modal(id, status) {
	if (status == 'Active') {
		$('#disable_id').val(id);
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'none',
			openSpeed: 1,
			autoSize: false,
			width: 450,
			height: 'auto',
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: $('#disable-popup'),
		});
	} else {
		$('#enable_id').val(id);
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'none',
			openSpeed: 1,
			autoSize: false,
			width: 450,
			height: 'auto',
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: $('#enable-popup'),
		});
	}
}

function confirm_disable() {
	const params = new Proxy(new URLSearchParams(window.location.search), {
		get: (searchParams, prop) => searchParams.get(prop),
	});
	status = params.status ? params.status : ''; // "some_value"
	$('body').append($('<form/>').attr({
		'action': _base_url + "package/disable?status=" + status,
		'method': 'post',
		'id': 'form_asdf'
	}).append($('<input/>').attr({
		'type': 'hidden',
		'name': 'id',
		'value': $('#disable_id').val()
	}))).find('#form_asdf').submit();
}

function confirm_enable() {
	const params = new Proxy(new URLSearchParams(window.location.search), {
		get: (searchParams, prop) => searchParams.get(prop),
	});
	status = params.status ? params.status : ''; // "some_value"
	$('body').append($('<form/>').attr({
		'action': _base_url + "package/enable?status=" + status,
		'method': 'post',
		'id': 'form_asdf'
	}).append($('<input/>').attr({
		'type': 'hidden',
		'name': 'id',
		'value': $('#enable_id').val()
	}))).find('#form_asdf').submit();
}

function closeFancy() {
	$.fancybox.close();
}
(function(a) {
	a(document).ready(function(b) {
		if (a('#package-list-table').length > 0) {
			a("table#package-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"iDisplayLength": 100,
				"scrollY": true,
				"orderMulti": false,
				"scrollX": true,
				'columnDefs': [{
					'targets': [-1],
					'orderable': false
				}, ]
			});
		}
	});
})(jQuery);
</script>