<div class="content-wrapper">
	
	<div class="customer-list-main-wrapper">
		<div class="customer-list-content-main-strock">
			<?php if($this->session->flashdata('failure_msg')) { echo '<div class="flash_failure">' . $this->session->flashdata('failure_msg') . '</div>'; } else if($this->session->flashdata('success_msg')) { echo '<div class="flash_success">' . $this->session->flashdata('success_msg') . '</div>'; } ?>
			<div class="customer-list-content-main">

				<div class="customer-list-main-heading">
					<h2>Modules</h2>
					<div class="customer-list-main-heading-link"> Modules <a href="#">Admin Home</a> </div>
					<div class="clear"></div>
				</div>

				<div class="customer-search-main-box">

					<h2 class="heading-half-box01">Add Module </h2> 
					<h2 class="heading-half-box01">Modules</h2> 

					<div class="clear"></div>
				</div>

				<div class="customer-listing-main-content-box">

					<div class="customer-listing-main-cont-list-box-half-main-half">						
						<div class="customer-listing-main-cont-list-box-half">
							
							<?php echo form_open(base_url() . 'settings/modules' . ( !empty($e_modules) ? '/'. $e_modules->module_id : '' ), array('id' => 'add-module-form'));  ?>
								<div class="zone-add-left-box">
									<div class="zone-fld-box">	</div>
									<?php echo form_label('<strong>Module Name</strong> <span>:</span>', 'module_name',  array( 'class' => 'zone-name-box')); ?>
                                                                        <?php echo form_label('<strong>Parent</strong> <span>:</span>', 'parent_id',  array( 'class' => 'zone-name-box')); ?>
									<div class="zone-name-box">	</div>
								</div>
								<div class="zone-add-right-box">
									<div class="error"><?php echo isset($error) && strlen(trim($error)) > 0 ? $error : '&nbsp;'; ?></div>
									<div class="zone-fld-box">
										<?php echo form_input(array('name' => 'module_name', 'id' => 'module-name', 'maxlength' => 100, 'tabindex' => 1, 'class' => 'hed-search-small-fld-big', 'value' => set_value('module_name', !empty($e_module) ? $e_module->module_name : ''))); ?>
									</div>
                                                                        <div class="zone-fld-box">
										<div class="dropdown">
											<?php echo form_dropdown('parent_id', $sel_modules, set_value('parent_id', !empty($e_module) ? $e_module->parent_id : ''), 'tabindex=2 id=parent_id'); ?>
										</div>
									</div>
									<div class="zone-fld-box">
										<?php echo form_submit(array('name' => 'add_module', 'id' => 'add-module', 'tabindex' => 2, 'value' => 'Add Module', 'class' => 'customer-list-add-zone-but') ); ?>
									</div>
								</div>
							<?php echo form_close(); ?>
							
							<div class="clear"></div>

							<div class="blank-space"></div>
						</div>
					</div>

					<div class="customer-listing-main-cont-list-box-half-main-half-big">
						<div id="customer-listing">
							<div class="row">
								<div class="cell w10p"><strong>Sl No</strong></div>
								<div class="cell w35p"><strong>Module Name</strong></div>
                                                                <div class="cell w35p"><strong>Parent Module Name</strong></div>
								<div class="cell w20p last"><strong>Actions</strong></div>
							</div>
						
							<?php
							$sln = 1;
							foreach($modules as $module)
							{
								?>
								<div class="row module_row" id="module-<?php echo $module->module_id; ?>">
									<div class="cell"><?php echo $sln++; ?></div>
									<div class="cell"><?php echo ucwords(html_escape($module->sub_module_name)); ?></div>
                                                                        <div class="cell"><?php echo ucwords(html_escape($module->module_name)); ?></div>
									<div class="cell last actions">
										<a href="<?php echo base_url() . 'settings/modules/' . $module->module_id ?>" class="edit" ></a>
										<a onclick="deleteSettings(<?php echo $module->module_id; ?>, 6)" class="delete" ></a>
									</div>
								</div>
								<?php
							}
							?>
						</div>							
					</div>

					<div class="clear"></div>
					<div class="blank-space"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="blank-space"></div>
	<div class="blank-space"></div>

</div>