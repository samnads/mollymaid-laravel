<style type="text/css">
    .pac-container {
        z-index: 9999999999 !important;
    }
    .select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    font-size: 12px;
    height: 30px;
    line-height: 24px;
   padding: 3px 0 3px 10px;

    text-indent: 0.01px;
	}
        .popup-main-box .booking_form .row .cell1{
            width: 160px !important;
        }
     .container.booking-fl-box {width: 100% !important;}
	 #maids-panel{z-index: 99999999 !important;}
    #service-panel{z-index: 99999999 !important;}
    .select2-dropdown{z-index: 99999999 !important;}
    #customer-address-panel{z-index: 99999999 !important;}
    .fancybox-inner{height: auto !important;width: 100%;margin: 0px auto;}

</style>
<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDpDi6GKhcr4mTO180oPmDh3cFQ5ciBP94&amp;libraries=places"></script>-->
<!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAzKGuULLF6eqMALYjRS7FncopnXnJI4ws&amp;libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk&amp;libraries=places"></script>
<!-- this is replaced from PM -->


<div class="content-wrapper" style="width: 100%; margin-left: 0px;">

    <div id="schedule-top">		
        <div class="row">

            <div class="container" style="width:100%;">
            <div class="book-nav-top">

                <ul>
                    <li>
                        <!--<div class="mm-color-box"><span class="oneday"></span> One Day &nbsp; </div>
                        <div class="mm-color-box"><span class="weekly"></span> Weekly</div>
                        <div class="mm-color-box"><span class="biweekly"></span> Biweekly</div>-->
                        
                        
                        <div class="mm-color-box purple">One Day</div>
                        <div class="mm-color-box green">Weekly</div>
                        <div class="mm-color-box yellow">Biweekly</div>
                        
                        
                        <!--<div class="mm-color-box"><span class="help"></span>&nbsp;Helpling&nbsp;</div>
                        <div class="mm-color-box"><span class="just"></span>&nbsp;Justmop(R)&nbsp;</div>
						<div class="mm-color-box"><span class="justod"></span>&nbsp;Justmop(OD)&nbsp;</div>
                        <div class="mm-color-box"><span class="helper"></span>&nbsp;Urban Clap&nbsp;</div>
                        <div class="mm-color-box"><span class="helpbitss"></span>&nbsp;Helpbit&nbsp;</div>
                        <div class="mm-color-box"><span class="usta"></span>&nbsp;Mr Usta&nbsp;</div><br/>
                        <div class="mm-color-box"><span class="servicemarket"></span>&nbsp;ServiceMarket&nbsp;</div>
                        <div class="mm-color-box"><span class="specss"></span>&nbsp;Spectrum&nbsp;</div>
						<div class="mm-color-box"><span class="jane"></span>&nbsp;Matic&nbsp;</div>
                        <div class="mm-color-box"><span class="specdom"></span>&nbsp;Spec Dom&nbsp;</div><br/>
                        <div class="mm-color-box"><span class="gladess"></span>&nbsp;Gladess&nbsp;</div>
                        <div class="mm-color-box"><span class="mildred"></span>&nbsp;Mildred&nbsp;</div>
                        <div class="mm-color-box"><span class="glory"></span>&nbsp;Glory&nbsp;</div>
                        <div class="mm-color-box"><span  class="justod"></span>&nbsp;SMART JM&nbsp;</div>-->
                        
                    </li>
                    <li><div class="hed-main-date">
                            <div class="row">
                                <div class="prev_day"><a class="hed-date-left-arrow" href="<?php echo base_url() . 'booking/' . $prev_day; ?>"></a></div>
                                <div class="hed-date-main-box"><?php echo html_escape($schedule_day); ?></div>
                                <div class="next_day"><a class="hed-date-right-arrow" href="<?php echo base_url() . 'booking/' . $next_day; ?>"></a></div>
                                <div class="hed-date-calender-icon"><span class="datepicker" data-date="<?php echo html_escape($schedule_day_c); ?>" data-date-format="dd-mm-yyyy"></span></div>
                            </div>
                        </div></li>
                    <li><div class="mm-drop"><!--fleft class removed-->
                            <input type="hidden" value="<?php echo $services_date; ?>" id="servicesdate"/>
                            <select id="freemaids" name="freemaids" class="" style="">
                                <option value="0"<?php if ($freemaid_ids == '0') { echo ' selected="selected"'; } ?>>Select All</option>
                                <option value="1"<?php if ($freemaid_ids == '1') { echo ' selected="selected"'; } ?>>Free Maids</option>
                                <option value="2"<?php if ($morning_avail == '2') { echo ' selected="selected"'; } ?>>Morning Availability</option>
                                <option value="3"<?php if ($eve_avail == '3') { echo ' selected="selected"'; } ?>>Evening Availability</option>
                                <option value="4"<?php if ($tot_avail == '4') { echo ' selected="selected"'; } ?>>On Duty Maids</option>
                            </select>
                        </div></li>
                    
                        <li class="no-right-margin"><div class="hed-main-button">
                            <ul>
                                <li class="no-right-margin"><span id="print-schedule-report">Print Schedule</span></li>
                            </ul>
                        </div></li>
                        
                        <div class="clear"></div>
                </ul>
            </div>


</div>




            <!--			<div class="cell1">
                                            
                                            
                                            <div><span class="biweekly"></span> Biweekly</div>
                                            
                                    </div>
                                    <div class="cell2">
                                            
                                    </div>
                                    <div class="cell4">
                                        
                                    </div>
                                    <div class="cell5">
                                        
                                    </div>
                                    <div class="cell3">
                                            
                                    </div>-->
        </div>
    </div>

    <div id="schedule-wrapper" style="min-height: <?php echo ((count($maids) * 80) + 70); ?>px;">
        <div id="schedule">
        
        <div class="scroll-top-fix" style="position: relative;">
                <div class="booking-position"></div>
            <div class="head">Maid Name</div>
            <div class="prev"><input type="submit" value="" id="tb-slide-left" /></div>
            <div class="time_line">			
                <div class="time_slider">
                    <div style="width:4820px;">
                        <?php
                        //$i = 1;
                        foreach ($times as $time_index => $time) {
                            //echo '<div>' . ( $i % 2 == 0 ? '<span style="color:#fff ;">' . $time->display . '</span>' : $time->display) . '</div>';
                            //++$i;
							echo '<div>' . $time->display . '</div>';
                        }
                        ?>
                    </div>
                </div>

            </div>
            <div class="next"><input type="submit" value="" id="tb-slide-right" /></div>
            <div class="clear"></div>
            </div>
            
            
            <div class="book-mid-det-lt-box">
            
            
            <div class="maids">
                <?php
				$i = 1;
                foreach ($maids as $maid) {
					// $veh1 = $this->bookings_model->getbookingsbymaid_id_new($service_datesss,$maid->maid_id);
					
					// if($veh1[0]->total_hrs == "")
					// {
						// $tothrs = 0;
					// } else {
						// $tothrs = $veh1[0]->total_hrs;
						// $tothrs = number_format((float)$tothrs, 1, '.', '');
					// }
                    ?>
                    
                    <!--<div class="maid" style="padding:30px 0px 30px 5px; text-align: left; font-weight: bold; font-size: 13px;"><?php// echo $i; ?>. <?php// echo html_escape($maid->maid_name); ?> (<?php// echo $tothrs; ?>)</div>--> 
                    <div class="maid" style="padding:30px 0px 30px 5px; text-align: left; font-weight: bold; font-size: 13px;"><?php echo $i; ?>. <?php echo html_escape($maid->maid_name); ?></div> 
                    <?php
                 $i++;   
                }
                ?>
            </div>
            <div class="time_grid" style="background: #f9f6f1; height: <?php echo (count($maids) * 71);  ?>px;">
                <div class="grids"><div id="schedule-grid-rows"><?php echo $schedule_grid; ?></div></div>
            </div>
            <div class="clear"></div>
            
            </div>
            
        </div>
    </div>
</div>




<input type="hidden" id="all-maids" value='<?php echo $all_maids; ?>' />
<input type="hidden" id="time-slots" value='<?php echo json_encode($times); ?>' />
<input type="hidden" id="repeate-end-start" value="<?php echo html_escape($repeate_end_start_c); ?>" />

<div id="booking-popup" style="display:none;">
    
	<div class="popup-main-box">
               
		<div class="col-md-12 col-sm-12 green-popup-head"><span id="b-maid-name"></span> <span id="b-time-slot"></span> <span class="pop_close"><img src="<?php echo base_url() ?>images/pup_close.png" alt="" /></span> </div>
                <div id="popup-booking" class="col-md-12 col-sm-12">
                <!--<div class="white-content-box">-->
			<div id="b-error"></div>
			<div class="booking_form" id="customer-info">
				<div class="row">
                                        <div class="cell1"><i class="fa fa-user"></i> Customer</div>
					<div class="cell2">:</div>
					<div class="cell3" id="b-customer-id-cell">
						<select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="">
							<option></option>
                                                        
							<?php
                                                        
                                                            
                                                        
                                                        // foreach($customers as $customer)
							// {  
                                                                // if($customer->payment_type == "D")
                                    // {
                                                                    // $paytype = "(Daily)";
                                                                // } else if($customer->payment_type == "W")
                                                                // {
                                                                    // $paytype = "(Weekly)";
                                                                // } else if($customer->payment_type == "M")
                                                                // {
                                                                    // $paytype = "(Monthly)";
                                                                // } else
                                                                // {
                                                                    // $paytype = "";
                                    // } 
                                                                // $p_number = array();
                                                                // if($customer->phone_number != NULL)
                                                                    // array_push ($p_number, $customer->phone_number);
                                                                // if($customer->mobile_number_1 != NULL)
                                                                    // array_push ($p_number, $customer->mobile_number_1);
                                                                // if($customer->mobile_number_2 != NULL)
                                                                    // array_push ($p_number, $customer->mobile_number_2);
                                                                // if($customer->mobile_number_3 != NULL)
                                                                    // array_push ($p_number, $customer->mobile_number_3);
                                                                
                                                                // $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';
                                                                
								// echo '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
							// }
							
							?>
						</select>
<!--                                            <span style="margin-top: 6px;"><a href="javascript:void(0);" id="add-customer-popup">Add</a></span>-->
						<div id="customer-picked-address"></div>
					</div>				
				</div>
			</div>
			<div id="customer-address-panel">
				<div class="head">Pick One Address <span class="close">Close</span></div>
				
				<div class="inner">
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
                        <!-- Edited by Geethu -->
                       <div id="maids-panel"> <!--style=" min-height:  338px; box-shadow: 0px 0px 0px;" -->
				<div class="head">Pick Maids <span class="close">Close</span></div>
				<div class="controls">
                                    <label class="radio inline">
                                        
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="same_zone" value="0" checked="checked"> All
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="same_zone" value="1"> Same Zone
                                    </label>
                                    
                                    
                                </div>
				<div class="inner"> <!-- height:260px; overflow: scroll; overflow-x:hidden; padding-top:  0px;  -->
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
			<div id="drivers-panel">
				<div class="head">Select Driver <span class="close">Close</span></div>
				<div class="controls">
                                    
                                    
                                    
				</div>
				<div class="inner">
					Loading<span class="dots_loader"></span>
				</div>			
			</div>
                        
                        <!--<div style="clear:both">&nbsp;</div>-->
                        <!-- End Edited by Geethu -->
			<div class="booking_form">
				<div class="row">
					<div class="cell1"><span class="icon_stype"></span> Service Type</div>
					<div class="cell2">:</div>
					<div class="cell3 sertype_sectn">
						<select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="sel2">
							<option></option>
							<?php
							foreach($service_types as $service_type)
							{
                                                                $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
								echo '<option data-cost="'.$service_type->service_rate.'" data-pricetype="'.$service_type->price_condition.'" data-type="'.$service_type->service_type.'" value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
							}
							?>
						</select>
					</div>				
				</div>
				
				<div id="service-panel"> <!--style=" min-height:  338px; box-shadow: 0px 0px 0px;" -->
                    <div class="head">Pick Service Type <span class="close">Close</span></div>
                    <div class="inner"> <!-- height:260px; overflow: scroll; overflow-x:hidden; padding-top:  0px;  -->
                            Loading<span class="dots_loader"></span>
                    </div>			
                </div>
				
				<div class="row" id="sqrbtnshow" style="display: none;">
                    <div class="cell1"><span class="icon_stype"></span> Enter Square ft</div>
                    <div class="cell2">:</div>
                    <div class="cell3">
                        <input type="text" name="squarefeetval" id="squarefeetval" value="">
                    </div>				
                </div>
                                <div class="row">

                                <div class="cell1"><!--<span class="icon_clean"></span>--><i class="fa fa-bitbucket"></i> Cleaning Materials</div>
                                    <div class="cell2">:</div>
                                    <div class="cell3">
                                        <label>  
                                            <!--<input type="checkbox" name="vaccum_cleaning" id="b-vaccum-cleaning" value="1">Vaccum Cleaning <br/>-->
                                            <input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="Y"> Yes
                                            <!--<input type="checkbox" name="cleaning_chemicals" id="b-cleaning-chemicals" value="1">Cleaning Chemicals-->
                                        </label>
                                    </div>
                                </div>
				<div class="row">
					<div class="cell1"><span class="icon_time"></span> Time</div>
					<div class="cell2">:</div>
					<div class="cell3">
						From: <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel2 small mr15">
								<option></option>
								<?php
								foreach($times as $time_index=>$time)
								{
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
						To: <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel2 small">
								<option></option>
								<?php
								foreach($times as $time_index=>$time)
								{
									if($time_index == 't-0')
									{                                                                                
										continue;
									}
									
									echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
								}
								?>
							</select>
                                                        
					</div>				
				</div>
				<div class="row">
					<div class="cell1"><span class="icon_btype"></span> Repeats</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="sel2">
							<option></option>
							<option value="OD">Never Repeat - One Day Only</option>
							<option value="WE">Every Week</option>
                                                        <option value="BW">Every 2 Week</option>
							<!--
							<option value="BW">Every Other Week</option>
							-->
						</select>	
					</div>				
				</div>
				<div class="row" id="repeat-days">
					<div class="cell1"><span class="icon_btype"></span> Repeat On</div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label><input type="checkbox" class="w_day" name="w_day[]" id="repeat-on-0" value="0" <?php if($day_number == 0) { echo 'checked="checked"'; } ?> /> Sun</label>
						<label><input type="checkbox"  class="w_day" name="w_day[]" id="repeat-on-1" value="1" <?php if($day_number == 1) { echo 'checked="checked"'; } ?> /> Mon</label>
						<label><input type="checkbox" class="w_day"  name="w_day[]" id="repeat-on-2" value="2" <?php if($day_number == 2) { echo 'checked="checked"'; } ?> /> Tue</label>
						<label><input type="checkbox"  class="w_day" name="w_day[]" id="repeat-on-3" value="3" <?php if($day_number == 3) { echo 'checked="checked"'; } ?> /> Wed</label>
						<label><input type="checkbox"  class="w_day" name="w_day[]" id="repeat-on-4" value="4" <?php if($day_number == 4) { echo 'checked="checked"'; } ?> /> Thu</label>
						<label><input type="checkbox" class="w_day"  name="w_day[]" id="repeat-on-5" value="5" <?php if($day_number == 5) { echo 'checked="checked"'; } ?> /> Fri</label>
						<label><input type="checkbox" class="w_day"  name="w_day[]" id="repeat-on-6" value="6" <?php if($day_number == 6) { echo 'checked="checked"'; } ?> /> Sat</label>
					</div>				
				</div>
				<div class="row" id="repeat-ends">
					<div class="cell1"><span class="icon_btype"></span> Ends</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label class="mr15"><input type="radio" name="repeat_end" id="repeat-end-never" value="never" checked="checked" /> Never</label>
						<label><input type="radio" name="repeat_end" id="repeat-end-ondate" value="ondate" /> On</label> 
						<input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" />
					</div>				
				</div>
                                <div class="row">
					<div class="cell1"><span class="icon_lock"></span> Lock Booking</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label><input name="lock_booking" id="lock-booking" type="checkbox" /> Lock Booking</label>
					</div>				
				</div>

               <div class="row payment_mode_customer" style="display:none;">
                    <div class="cell1"><span class="icon_lock"></span> Payment mode</div>
                    <div class="cell2">:</div>
                    <div class="cell3">
                      <input type="text" name="payment_mode" id="b-customer-paymode-cell">
                    </div>              
                </div>
				
				<div class="row">
					<div class="cell1"><span class="icon_time"></span> Discount Rate </div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label>
							<input name="discount_rate_perhr" id="discount_rate_perhr" value="" type="text" class="popup-disc-fld" style="width: 80px;"/>
						/hr
						</label>
						<label style="font-size:13px;">&nbsp;&nbsp;Rate&nbsp;:&nbsp; 
							AED <input name="rate_per_hr" id="b-rate_per_hr" readonly="" value="" type="text" class="popup-disc-fld" style="width: 80px;"/>
						/hr
						</label>
					</div>
                                        
				</div>
                            <!--hided for total amount <div class="row">
					<div class="cell1"><span class="icon_time"></span> Total Hours </div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label>
                                                <input type="hidden" id="total_prebook_hrs" value="0">
                                                <input type="hidden" id="total_week_days" value="1">
                                                <input name="hrs_per_week" id="b-hrs_per_week" value="" readonly="" type="text" class="popup-disc-fld" style="width: 80px;"/>
                                            /week
                                            </label>
                                            <label style="font-size:13px;">&nbsp;&nbsp;<i class="fa fa-dollar"></i> Rate&nbsp;:&nbsp; 
                                                <input name="rate_per_hr" id="b-rate_per_hr" readonly="" value="" type="text" class="popup-disc-fld" style="width: 80px;"/>
                                            /hr
                                            </label>
					</div>
                                        
				</div>-->
                            <div class="row">
					<div class="cell1"><span class="icon_time"></span> Amount </div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label> 
							<input name="serviceamountcost" id="serviceamountcost" readonly="" value="" type="text" class="popup-disc-fld" style="width: 80px;"/>
						</label>
						<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i> Discount &nbsp;:&nbsp; <input name="discount" id="b-discount" readonly="" type="text" class="popup-disc-fld" style="width: 100px;" /></label>
					</div>
                                        
				</div>
				<div class="row">
					<div class="cell1"><span class="icon_time"></span> Total Amount </div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label>
							<input type="hidden" id="total_prebook_hrs" value="0">
							<input type="hidden" id="total_week_days" value="1">
							<input type="hidden" id="flatrateval" value="">
							<input type="hidden" id="categoryidval" value="">
							<input type="hidden" id="subcategoryidval" value="">
							<input type="hidden" id="furnishid" value="">
							<input type="hidden" id="scrubpolishid" value="">
							<input type="hidden" id="valname" value="">
							<input type="hidden" id="servicecostval" value="35">
							<input type="hidden" id="servicetypeval" value="H">
							<input type="hidden" id="service_pricetype" value="CP">
							<input type="hidden" id="sqftcount" value="">
							<input type="hidden" id="b-rate_per_hr_cust" value="35">
							<input type="hidden" id="customer_flag_val" value="">
							<input type="hidden" id="customer_flag_reason" value="">
							<!--<input name="hrs_per_week" id="b-hrs_per_week" value="" readonly="" type="text" class="popup-disc-fld" style="width: 80px;"/>-->
							<input name="tot_amout" id="tot_amout" value="" readonly="" type="text" class="popup-disc-fld" style="width: 80px;"/>
						</label>
					</div>
                                        
				</div>
<!--                                <div class="row">
					<div class="cell1"><span class="icon_disc"></span> Rate </div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label>
                                                <input name="rate_per_hr" id="b-rate_per_hr" readonly="" value="" type="text" class="popup-disc-fld" style="width: 100px;"/>
                                            /hr
                                            </label>
					</div>				
				</div>-->
				<!--<div class="row">
					<div class="cell1"><i class="fa fa-money"></i> Pending Amount</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<label><input name="pending_amount" id="b-pending-amount" readonly="" type="text" class="popup-disc-fld" style="width: 100px;"/></label>
						<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i> Discount &nbsp;:&nbsp; <input name="discount" id="b-discount" readonly="" type="text" class="popup-disc-fld" style="width: 100px;" /></label>
					</div>				
				</div>-->
<!--                            <div class="row">
					<div class="cell1"><span class="icon_disc"></span><i class="fa fa-tag"></i> Discount</div>
					<div class="cell2">:</div>
					<div class="cell3">
                                            <label><input name="discount" id="b-discount" type="text" class="popup-disc-fld" style="width: 100px;" /></label>
					</div>				
                            </div>-->
                            <div class="row">
                                <div class="cell1"><span class="icon_notification"></span> Notification</div>
                                <div class="cell2">:</div>
                                <div class="cell1">
                                    <div style="display :inline-flex;">
                                    &nbsp;&nbsp;<label><input name="notification_email_booking" id="notification-email-booking" type="checkbox" /> Email</label>
                                    &nbsp;&nbsp;<label><input name="notification_sms_booking" id="notification-sms-booking" type="checkbox" /> SMS</label>
                                    </div>
<!--                                    <div class="sms_field">
                                        <label><input name="notification_sms_booking" id="notification-sms-booking" type="checkbox" /> SMS</label>
                                        <div class="sms_content">
                                            <label><input name="notification_smss_booking" class="notification-sms-booking" value="N" type="radio" /> Send Now</label>
                                            <label><input name="notification_smss_booking" class="notification-sms-booking" value="L" type="radio" /> Send Later</label>
                                        </div>
                                    </div>
                                    <div class="sms_calender">
                                        <label>Date <input type="text" style="margin-top: 6px; width: 160px;" id="sms_schedule_date" name="sms_schedule_date" value=""></label> 
                                    </div>-->
                                </div>
                            </div>
							
							<div class="row normaldriversec">
					<div class="cell1"><span class="icon_stype"></span> Select Driver</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="driver_select_id" id="driver_select_id" data-placeholder="Select Driver" class="sel2">
							<option></option>
							<?php
							foreach($tablets as $tabletval)
							{
								//$selected = $tabletval->tablet_id == 1 ? 'selected="selected"' : '';
								echo '<option value="' . $tabletval->tablet_id . '">' . $tabletval->tablet_driver_name . '</option>';
							}
							?>
						</select>
					</div>				
				</div>
				<div class="row" id="updatedriversec" style="display: none;">
					<div class="cell1"><span class="icon_stype"></span> Select Driver</div>
					<div class="cell2">:</div>
					<div class="cell3">
						<select name="driver_select_id_update" id="driver_select_id_update" data-placeholder="Select Driver" class="sel2" style="width: 150px !important;">
							<option></option>
							<?php
							foreach($tablets as $tabletval)
							{
								//$selected = $tabletval->tablet_id == 1 ? 'selected="selected"' : '';
								echo '<option value="' . $tabletval->tablet_id . '">' . $tabletval->tablet_driver_name . '</option>';
							}
							?>
						</select>
						<input type="button" class="copy-but e-new-but" id="updatedrivertobooking" value="Update Driver"  style="width: 135px !important; float: right; margin-right: 0px;" />
					</div>				
				</div>
				<div class="row" id="transferred_to" style="display: none;">
					<div class="cell1"><span class="icon_stype"></span> Transferred To</div>
					<div class="cell2">:</div>
					<div class="cell3" id="transfeered_to_show">
						
					</div>				
				</div>
			</div>	
			<div class="booking_form">
				<div class="row crewsec" id="crewsec" style="display: none;">
					<div class="cell1"><span class="icon_notification"></span> Crew In</div>
					<div class="cell2">:</div>
					<div class="cell1" id="crewsecval">
					</div>
				</div>
			</div>
			<div style="clear:both; padding: 5px;"></div>
			<div class="pop-main-cont-fld-box"><textarea name="booking_note" id="booking-note" class="popup-note-fld" placeholder="Note to Driver"></textarea></div>
			
			<div class="pop-main-button">
				<input type="hidden" id="booking-id" />
				<input type="hidden" id="hiddentabletid" name="hiddentabletid" />
				<input type="hidden" id="customer-address-id" />
				<input type="hidden" id="maid-id" />
				<input type="button" class="save-but" id="save-booking" value="Save" />
                                <input type="button" class="copy-but e-new-but" id="copy-booking" value="Copy" />
                                <input type="button" class="transfer-driver-but e-new-but" id="transfer-driver" value="Transfer Driver" />
                                
				<input type="button" class="delete-but" id="delete-booking" value="Delete" />
                                
				<div class="clear"></div>
			</div>
		<!--</div>-->
                </div>
                <div id="customer-add-popup" class="col-md-5 col-sm-5" style="display:none;">
                    <div class="booking_form">
                        <form name="customer-popup" id="customer-popup" method="POST">
                            <div class="row">
                                <div class="cell1">
                                    <i class="fa fa-user"></i>&nbsp; Customer
                                </div>
                                <div class="cell2">:</div>
                                <div class="cell3">
                                    <input type="text" name="customer_name" id="customer_name" style="width:220px;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell1">
                                <i class="fa fa-envelope"></i>&nbsp; Email
                                </div>
                                <div class="cell2">:</div>
                                <div class="cell3">
                                    <input type="text" name="customer_email" id="customer_email" style="width:220px;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell1">
                                    <i class="fa fa-flag"></i>&nbsp; Area
                                </div>
                                <div class="cell2">:</div>
                                <div class="cell3">
                                    <select name="area" id="area" class="sel2" style="width:220px;">
                                        <option value="" selected>Select Area</option>
                                         <?php
                                        if (count($areas) > 0) {
                                            foreach ($areas as $areasVal) {
                                                ?>
                                                <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell1">
                                    <i class="fa fa-tag"></i>&nbsp; Apartment No
                                </div>
                                <div class="cell2">:</div>
                                <div class="cell3">
                                    <input type="text" name="apartmentnos" id="apartmentnos" style="width:220px;" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell1">
                                    <i class="fa fa-tag"></i>&nbsp; Address
                                </div>
                                <div class="cell2">:</div>
                                <div class="cell3">
                                    <input id="address" type="text" name="address" onkeyup="loadLocationField('address');" style="width:220px;" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell1">
                                    <i class="fa fa-mobile"></i>&nbsp; Mobile Number
                                </div>
                                <div class="cell2">:</div>
                                <div class="cell3">
                                    <input type="text"  id="mobile_number1" name="mobile_number1"  style="width:220px;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="cell1">
                                    
                                </div>
                                <div class="cell2"></div>
                                <div class="cell3">
                                    <input type="button"  id="popup-add-customer" value="Add" class="save-but"> 
                                </div>
                            </div>
                        </form>
                    </div>
                    
               
               
                </div>
                <div id="customer-detail-popup" class="col-md-5 col-sm-5" style="float:right;display:none;">
                    <div class="booking_form">
                    <div class="row">
                        <div class="cell1"><i class="fa fa-user"></i>&nbsp; Name</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-ids-cell">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-mobile"></i>&nbsp; Mobile</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-mobile-cell">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-envelope"></i>&nbsp; Email</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-email-cell">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-flag"></i>&nbsp; Area</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-area-cell">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-flag"></i>&nbsp; Zone</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-zone-cell">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-tag"></i>&nbsp; Address</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-address-cell">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-tag"></i>&nbsp; Apartment No</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-apartment-no">
						
                        </div>				
                    </div>
                    <div class="row">
                        <div class="cell1"><i class="fa fa-bookmark"></i>&nbsp; Customer Book Type</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-booktype-cell">
						
                        </div>				
                    </div>
					<div class="row">
                        <div class="cell1"><i class="fa fa-bookmark"></i>&nbsp; Customer Notes</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-notes-cell">
						
                        </div>				
                    </div>
                 <!--    <div class="row">
                        <div class="cell1"><i class="fa fa-money"></i>&nbsp; Payment Mode</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="b-customer-paymode-cell">
						
                        </div>				
                    </div> -->
					<!--<div class="row">
                        <div class="cell1"><i class="fa fa-money"></i>&nbsp; Reference No</div>
                        <div class="cell2">:</div>
                        <div class="cell3" >
				<input	type="text" id="b-customer-reference-cell"/>	
                        </div>				
                    </div>-->
                    <div style="height:21px;"></div> 
                    <div class="row start_status" style="display:none;">
                        <div class="cell1 status" style="width:50px !important;"><span class="icon_btype"></span>&nbsp;Status</div>
                        <div class="cell2" style="width:50px !important;">: &nbsp;&nbsp;<i class="fa fa-gear fa-spin" style="font-size:18px;color:#4897E6;" title="In progress"></i></div>
                        <div class="cell3" id="progress_time" style="color:#428bca"></div>				
                    </div>
        
      
                     <div class="row service_started_at" style="display:none;">
                        <div class="cell1 status" style="width:50px !important;"><span class="icon_time"></span>&nbsp;Started at</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="started_time"></div>				
                     </div>
             
                   
                    <div class="row finish_status" style="display:none;">
                        <div class="cell1 status" style="width:50px !important;"><span class="icon_btype"></span>&nbsp;Status</div>
                        <div class="cell2">:</div>
                        <div class="cell3">&nbsp;<b>Finished</b>&nbsp;<i class="fa fa-check" aria-hidden="true" style="color:#7eb216;font-size:18px"></i></div>				
                    </div>
                    <div class="row finish_status" style="display:none;">
                        <div class="cell1 status" style="width:50px !important;"><span class="icon_time"></span>&nbsp;Started at</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="time_started"></div>				
                     </div>
                        
                    <div class="row finish_status" style="display:none;">
                        <div class="cell1 status" style="width:50px !important;"><span class="icon_time"></span>&nbsp;Finished at</div>
                        <div class="cell2">:</div>
                        <div class="cell3" id="time_finished"></div>				
                    </div>
                        
                    <div class="row finish_status" style="display:none;">
                       <div class="cell3" id="worked_hrs"></div>
                       	
                        			
                    </div>     
                    </div>
                </div>
	</div>
</div>

<div id="schedule-report" style="display: none !important;"></div>
