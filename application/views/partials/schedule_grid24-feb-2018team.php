<?php
$var = '';
foreach($maids as $maid)
{
	$top = 15;
	?>
        <?php
        if($var == $maid->team_name)
        {
        ?>
	<div class="row tb-slider" id="<?php echo 'maid-' . $maid->maid_id; ?>">
		<?php
		$booked = isset($maid_bookings[$maid->maid_id]) ? $maid_bookings[$maid->maid_id] : array();
		$f_slot = FALSE;
		$b_slot = FALSE;
		foreach($times as $time_index=>$time)
		{
			if($b_slot && isset($tb_end_time) && $tb_end_time == $time->stamp)
			{
				echo '</div>';
				$b_slot = FALSE;
				$f_slot = FALSE;
			}

			if(!$b_slot && isset($booked[$time->stamp]['end_time']))
			{
				if($f_slot)
				{
					echo '</div>';
				}

				$f_slot = FALSE;
				$b_slot = TRUE;
				$tb_end_time = $booked[$time->stamp]['end_time'];
				echo '<div class="slot">';
			}

			if(!$b_slot && !$f_slot)
			{
				$f_slot = TRUE;
                                
                                if(in_array($maid->maid_id, $leave_maid_ids)) // Maid Leave
                                {
                                    echo '<div class="slot" style="background:#ddd;">';
                                }
                                else
                                {
                                    echo '<div class="' . ($booking_allowed == 1 ? 'selectable' : '') . ' slot">';
                                }
				
			}
			?>

			<div class="cell" id="<?php echo $time_index; ?>" <?php if($current_hour_index === $time_index) { echo 'style="border-right:1px solid #F00;"'; } ?>><?php // echo $time->display . '-' . $current_hour_index; ?>
				<?php
				if($b_slot && isset($booked[$time->stamp]['end_time']) && isset($tb_end_time))
				{
					//$diff = (($tb_end_time - $time->stamp) / 60) / 60;
					$diff = (($tb_end_time - $time->stamp) / 60) / 30;
					//$width = ($diff * 100) - 22 + 20;
					$width = ($diff * 100) - 22 + 20;
					$top = $top == 3 ? 20 : 3;
                                        //Payment Type
                                        if($booked[$time->stamp]['payment_type'] == "D")
                                        {
                                            $paytype = "(D)";
                                        } else if($booked[$time->stamp]['payment_type'] == "W")
                                        {
                                            $paytype = "(W)";
                                        } else if($booked[$time->stamp]['payment_type'] == "M")
                                        {
                                            $paytype = "(M)";
                                        } else
                                        {
                                            $paytype = "";
                                        }
					//$top = $top == 3 ? 34 : 3;
					echo '<div class="schedule_bubble ' . strtolower($booked[$time->stamp]['type']) . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '<br>B.A : '.$booked[$time->stamp]['balance'].$booked[$time->stamp]['signed'].' '. $paytype .'</span></div></div>';
				}
				?>
			</div>
		<?php
		}

		if($f_slot || $b_slot)
		{
			echo '</div>';
		}
		?>						
		<div class="clear"></div>
	</div>
        <?php
        } else {
        ?>
<div class="row tb-slider" style="height:30px; background:#fff;">
            
        </div>
        <div class="row tb-slider" id="<?php echo 'maid-' . $maid->maid_id; ?>">
		<?php
		$booked = isset($maid_bookings[$maid->maid_id]) ? $maid_bookings[$maid->maid_id] : array();
		$f_slot = FALSE;
		$b_slot = FALSE;
		foreach($times as $time_index=>$time)
		{
			if($b_slot && isset($tb_end_time) && $tb_end_time == $time->stamp)
			{
				echo '</div>';
				$b_slot = FALSE;
				$f_slot = FALSE;
			}

			if(!$b_slot && isset($booked[$time->stamp]['end_time']))
			{
				if($f_slot)
				{
					echo '</div>';
				}

				$f_slot = FALSE;
				$b_slot = TRUE;
				$tb_end_time = $booked[$time->stamp]['end_time'];
				echo '<div class="slot">';
			}

			if(!$b_slot && !$f_slot)
			{
				$f_slot = TRUE;
                                
                                if(in_array($maid->maid_id, $leave_maid_ids)) // Maid Leave
                                {
                                    echo '<div class="slot" style="background:#ddd;">';
                                }
                                else
                                {
                                    echo '<div class="' . ($booking_allowed == 1 ? 'selectable' : '') . ' slot">';
                                }
				
			}
			?>

			<div class="cell" id="<?php echo $time_index; ?>" <?php if($current_hour_index === $time_index) { echo 'style="border-right:1px solid #F00;"'; } ?>><?php // echo $time->display . '-' . $current_hour_index; ?>
				<?php
				if($b_slot && isset($booked[$time->stamp]['end_time']) && isset($tb_end_time))
				{
					//$diff = (($tb_end_time - $time->stamp) / 60) / 60;
					$diff = (($tb_end_time - $time->stamp) / 60) / 30;
					//$width = ($diff * 100) - 22 + 20;
					$width = ($diff * 100) - 22;
					$top = $top == 3 ? 34 : 3;
                                        //Payment Type
                                        if($booked[$time->stamp]['payment_type'] == "D")
                                        {
                                            $paytype = "(D)";
                                        } else if($booked[$time->stamp]['payment_type'] == "W")
                                        {
                                            $paytype = "(W)";
                                        } else if($booked[$time->stamp]['payment_type'] == "M")
                                        {
                                            $paytype = "(M)";
                                        } else
                                        {
                                            $paytype = "";
                                        }
					//$top = $top == 3 ? 34 : 3;
                                        $progress_service_icon='';
                                        $scheduled_date = date("Y-m-d"); 
                                        $job_service=$this->bookings_model->job_start_or_finish($booked[$time->stamp]['booking_id'],$scheduled_date);
                                        if(!empty($job_service)){
                                        if($job_service[0]->service_status==1){
                                        $progress_service_icon='<i class="fa fa-gear fa-spin" style="font-size:18px;title="In progress"></i>';
                                        }
                                        if($job_service[0]->service_status==2){
                                        $progress_service_icon='<i class="fa fa-check" aria-hidden="true" style="font-size:18px;"></i>';
                                        }
                                        
                                        }
                                        
					echo '<div class="schedule_bubble ' . strtolower($booked[$time->stamp]['type']) . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '<br>B.A : '.$booked[$time->stamp]['balance'].$booked[$time->stamp]['signed'].' '. $paytype .'</span>'.$progress_service_icon.'</div></div>';
				}
				?>
			</div>
		<?php
		}

		if($f_slot || $b_slot)
		{
			echo '</div>';
		}
		?>						
		<div class="clear"></div>
	</div>
	<?php
        }
        $var = $maid->team_name;
}
?>
<input type="hidden" id="all-bookings" value='<?php echo htmlspecialchars(json_encode($all_bookings), ENT_QUOTES, 'UTF-8'); ?>' />