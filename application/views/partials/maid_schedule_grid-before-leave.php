<?php
 
foreach($maid_schedule as $maid)
{
	$top = 15;
	?>
	<div class="row tb-slider" id="<?php echo 'maid-' . $maid->date; ?>">
		<?php
		$booked = isset($maid_bookings[$maid->date]) ? $maid_bookings[$maid->date] : array();
		$f_slot = FALSE;
		$b_slot = FALSE;
		foreach($times as $time_index=>$time)
		{
			if($b_slot && isset($tb_end_time) && $tb_end_time == $time->stamp)
			{
				echo '</div>';
				$b_slot = FALSE;
				$f_slot = FALSE;
			}

			if(!$b_slot && isset($booked[$time->stamp]['end_time']))
			{
				if($f_slot)
				{
					echo '</div>';
				}

				$f_slot = FALSE;
				$b_slot = TRUE;
				$tb_end_time = $booked[$time->stamp]['end_time'];
				echo '<div class="slot">';
			}

			if(!$b_slot && !$f_slot)
			{
				$f_slot = TRUE;
				echo '<div class="' . ($booking_allowed == 1 ? 'selectable' : '') . ' slot">';
			}
			?>

			<div class="cell" id="<?php echo $time_index; ?>" <?php if($current_hour_index === $time_index) { echo 'style="border-right:1px solid #F00;"'; } ?>><?php // echo $time->display . '-' . $current_hour_index; ?>
				<?php
				if($b_slot && isset($booked[$time->stamp]['end_time']) && isset($tb_end_time))
				{
					$diff = (($tb_end_time - $time->stamp) / 60) / 60;
					$width = ($diff * 100) - 22 + 20;
					$top = $top == 3 ? 34 : 3;
					echo '<div class="schedule_bubble ' . strtolower($booked[$time->stamp]['type']) . '" data-bind="' . $maid->date . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['zone']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '</span></div></div>';
				}
				?>
			</div>
		<?php
		}

		if($f_slot || $b_slot)
		{
			echo '</div>';
		}
		?>						
		<div class="clear"></div>
	</div>
	<?php
}
?>
<input type="hidden" id="all-bookings" value='<?php echo json_encode($all_bookings); ?>' />