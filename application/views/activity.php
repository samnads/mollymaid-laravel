<?php
foreach ($dayservices as $dayservice) {
    $zone_services[$dayservice->zone_id][] = $dayservice;
    $zone_summary[$dayservice->zone_id]['total_hours'] += (strtotime($dayservice->end_time) - strtotime($dayservice->start_time)) / 3600;
}
if (isset($_GET['test'])) {
    header('Content-Type: application/json');
    $zones_names = array_unique(array_column($dayservices, 'zone_name'));
    echo print_r($zone_services, JSON_PRETTY_PRINT);
    //echo $zone_summary[4]['total_hours'];
    die();
}
?>
<style type="text/css">
.bg-od{
  background-color: <?= $settings->color_bg_booking_od; ?> !important;
  color: <?= $settings->color_bg_text_booking_od; ?> !important;
}
.bg-we {
  background-color: <?= $settings->color_bg_booking_we; ?> !important;
  color: <?= $settings->color_bg_text_booking_we; ?> !important;
}
.bg-bw{
  background-color: <?= $settings->color_bg_booking_bw; ?> !important;
  color: <?= $settings->color_bg_text_booking_bw; ?> !important;
}
</style>
<style>
.finished {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/complete.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}
.not-started {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/not-start.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}
.on-going {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/on-going.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}
.cancelled {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/cancel.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}

.n-details-list-view {}
.n-details-grid-view {display:none;}
.n-details-list-view, .n-details-grid-view {width: 100% !important;}
</style>

<div class="row m-0">
  <div class="col-md-12">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/owl.carousel.css"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.js"></script>
    <script>
$(document).ready(function() {

	var owl = $("#owl-demo");
	owl.owlCarousel({
	items : 3, //10 items above 1000px browser width
	itemsDesktop : [1200,2], //5 items between 1000px and 901px
	itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,2], // itemsMobile disabled - inherit from itemsTablet option
    });

  $(".next").click(function(){
    owl.trigger('owl.next');
  });


  $(".prev").click(function(){
    owl.trigger('owl.prev');
  });


  $('.grid-view-btn').click(function(){
	  $(".n-details-list-view").hide(500);
	  $(".n-details-grid-view").show(500);
    $('.grid-view-btn').hide();
    $('.list-view-btn').show();
  });


  $('.list-view-btn').click(function(){
	  $(".n-details-grid-view").hide(500);
	  $(".n-details-list-view").show(500);
    $('.list-view-btn').hide();
    $('.grid-view-btn').show();
  });


});
</script>
    <div class="row m-0">
      <div class="col-md-12 p-0">
           <div class="widget-header">
           <form class="form-horizontal" method="post">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Activity</h3></li>

              <li class="mr-2">
                  <input type="text" style="width: 160px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">
              </li>

              <li><select style="width:160px;" name="filter_zone_type" id="filter_zone_type">
                <option value="" selected>-- Zone --</option>
                <?php foreach ($zones as $zones) : ?>
                  <option value="<?php echo $zones->zone_id; ?>" <?php if ($filter_zone_type == $zones->zone_id) echo 'selected'; ?>><?php echo $zones->zone_name; ?></option>
                <?php endforeach; ?>
              </select>
            </li>
            <li>
              <select style="width:160px;" name="filter_maid_id" style="width:100%">
                <option value="">-- All Maids --</option>
                <?php foreach ($maids as $key => $maid) : ?>
                  <option value="<?= $maid->maid_id; ?>" <?php if ($filter_maid_id == $maid->maid_id) echo 'selected'; ?>><?= $maid->maid_name; ?></option>
                <?php endforeach; ?>
              </select>
            </li>
            <li>
                <input type="hidden" id="formatted-date" value="<?php echo $formatted_date ?>"/>
            </li>

            <li>
            <label style="color: #f6f6f6;">Search:<input type="search" name="searchterm" class="" value="<?php echo $searchterm ?>" placeholder="" aria-controls="activity"></label>
            </li>

              <li>
                  <input type="submit" class="n-btn" value="Go" name="vehicle_report">
              </li>



              <li>
                  <a id="synch-to-odoo-new" href="#" class="n-btn">Synchronize</a>
              </li>



              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn" title="Print"> <a href="<?php echo base_url() . 'reports/activity_summary_view/' . $formatted_date; ?>" target="_blank"><i class="fa fa-print"></i></a> </div>

                  <div class="topiconnew border-0 green-btn" title="Download to Excel"> <a href="<?php echo base_url(); ?>activity/plan_export/<?php echo $formatted_date; ?>" target="_blank"> <i class="fa fa-file-excel-o"></i></a> </div>

                <div class="topiconnew border-0 green-btn list-view-btn" style="display:none" title="List View"> <i class="fa fa-bars"></i> </div>

                <div class="topiconnew border-0 green-btn grid-view-btn" title="Grid View"> <i class="fa fa-th"></i> </div>
              </li>

              <div class="clear"></div>
            </ul>
     </div>
     </form>
</div>

      </div>





<div class="col-md-12 p-0  n-details-grid-view">
  <div class="row m-0">
    <div id="owl-demo" class="owl-carousel">
      <?php foreach ($zone_services as $zone_id => $services): ?>
        <div class="item">
        <h2><?=$services[0]->zone_name?></h2>
        <?php foreach ($services as $key => $service): ?>
        <div class="col-md-12 plan-thumb-main">
          <div class="row plan-thumb-cont-set m-0">
            <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
              <?php
              ?>
              <p><?=$service->reference_id ?: 'EM-'.(new DateTime($service->booked_datetime))->format('Y').'-'.str_pad($service->booking_id, 4, '0', STR_PAD_LEFT);?><span class="bg-<?=strtolower($service->booking_type)?>"><?=$service->booking_type == 'OD' ? $settings->od_booking_text : ($service->booking_type == 'WE' ? $settings->we_booking_text : $settings->bw_booking_text)?></span>
              </p>
            </div>
            <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0 button-area-<?=$service->booking_id?>">
              <?php
                $activity_status = 0;
                $activity_status = $service->service_status == 1 ? 1 : ($service->service_status == 2 ? 2 : ($service->service_status == 3 ? 3 : 0));
                $activity_status = $service->payment_status == 1 ? 5 : ($service->payment_status == 0 && $service->service_status == 2 ? 6 : ($service->service_status == 1 && $service->payment_status != null ? 1 : ($service->service_status == 3 ? 3 : 0)));
                $class = $service->service_status == 1 ? 'on-going' : ($service->service_status == 2 ? 'finished' : ($service->service_status == 3 ? 'cancelled' : 'not-started'));
                $title = $service->service_status == 1 ? 'Ongoing' : ($service->service_status == 2 ? 'Completed' : ($service->service_status == 3 ? 'Cancelled' : 'Pending'));
                if ($service->ps_no != "") {
                    $pinkslip_no = $service->ps_no;
                } else {
                    $pinkslip_no = "0";
                }
                if ($service->collected_amount != "") {
                    echo '<a href="javascript:void(0)" data-bind="' . $service->collected_amount . '" onclick="get_activity_new(' . $activity_status . ', ' . $service->booking_id . ', this, ' . number_format($service->collected_amount,2) . ', ' . $service->service_status . ',' . $service->payment_status . ',' . $pinkslip_no . ');"><span class="' . $class . '">' . $service_status . '</span><label>' . $title . '</label></a>';
                } else {
                    echo '<a href="javascript:void(0)" data-bind="' . $service->collected_amount . '" onclick="get_activity(' . $activity_status . ', ' . $service->booking_id . ', this);"><span class="' . $class . '"></span><label>' . $title . '</label></a>';
                }
              ?>
            </div>
          </div>
          <div class="row plan-thumb-cont-set m-0">
            <div class="col-md-6 plan-thumb-text">
              <p>
                <span>Maid Name</span><?=$service->maid_name?>
              </p>
            </div>
            <div class="col-md-6 plan-thumb-text border-right-0">
              <p>
                <span>Customer Name</span>
                <?= $service->customer_name ?> <?= $service->payment_type ? ' <label class="text-muted">('.$service->payment_type.')</label>' : ''?>
              </p>
            </div>
          </div>
          <div class="row plan-thumb-cont-set m-0">
            <div class="col-md-6 plan-thumb-text n-time-section">
              <p>
                <span>Working Hours</span> <?=$service->time_from?> <label><?=date("A", strtotime($service->start_time))?></label> &nbsp; <?=$service->time_to?> <label><?=date("A", strtotime($service->end_time))?></label>
                <strong class="text-red"> (<?=(strtotime($service->end_time) - strtotime($service->start_time)) / 3600?> hrs)</strong>
              </p>
            </div>
            <div class="col-md-6 plan-thumb-text border-right-0">
              <div class="p-0">
                <div class="col-md-6 pl-0">
                  <p><span>Payment Mode</span><?=$service->payment_mode ?: '-'?></p>
                </div>
                <div class="col-md-6 mt-2">
                  <input type="hidden" id="day-service-id-of-booking-<?=$service->booking_id?>" value="<?=$service->day_service_id?>"/>
                  <div class="btn-bloc btn-group-xs pull-right" role="group" aria-label="...">
                    <?php
                    echo '<button data-bookingid="'.$service->booking_id.'" data-customerpaymentid="'.$service->payment_id.'"  data-maidpaymentid="" data-amount="" onclick="verifyPayment(this)" type="button" class="btn btn-block btn-xs verify-payment-btn-'.$service->booking_id.'" style="display:'.($service->collected_amount > 0 && $service->verified_status != 1 ? '' : 'none;') .'">Verify Payment <i class="fa fa-question" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-block btn-xs verified-payment-btn-'.$service->booking_id.'" style="display:'.($service->verified_status == 1 ? '' : 'none;') .'" disabled>Payment Verified <i class="fa fa-check" aria-hidden="true"></i></button>
              <button data-bookingid="'.$service->booking_id.'" data-dayserviceid="'.$service->day_service_id.'" onclick="generateInvoice(this)" type="button" class="btn btn-block btn-xs '.($service->payment_type == 'D' ? '' : 'hidden').' create-invoice-btn-'.$service->booking_id.'" style="display:'.($service->service_status == 2 && $service->invoice_status == 0 ? '' : 'none;') .'">Create Invoice <i class="fa fa-question" aria-hidden="true"></i></button>
              <button type="button" class="btn btn-block btn-xs invoiced-btn-'.$service->booking_id.'" style="display:'.($service->invoice_status == 1 ? '' : 'none;') .'" disabled>Invoiced <i class="fa fa-check" aria-hidden="true"></i></button>';
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row plan-thumb-cont-set border-bottom-0 m-0">
            <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
              <p>
                <span>Billed Amount</span>
                <label>AED</label> <label class="billed-amount-<?=$service->booking_id?>"><?=number_format($service->total_amount, 2) ?: '0.00'?></label>
              </p>
            </div>
            <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
              <p>
                <span>Collected Amount</span>
                <label>AED</label> <label class="collected-amount-<?=$service->booking_id?>"><?=number_format($service->collected_amount,2) ?: '0.00'?></label>
              </p>
            </div>
            <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
              <p>
                <span>Balance</span>
                <label>AED</label> <label class="balance-amount-<?=$service->booking_id?>"><?=number_format($service->total_amount - ($service->collected_amount ?: 0), 2)?></label>
              </p>
            </div>
          </div>
        </div>
        <?php endforeach;?>
        <!--<div class="col-md-12 zone-bottom-space">&nbsp;</div>-->
      </div>
      <?php endforeach;?>
    </div>
  </div>
</div>


      <div class="widget widget-table action-table n-details-list-view">
      <div class="widget-content">
        <table class="table da-table table-hover" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 10px; text-align: center;"> No.</th>
              <th style="line-height: 18px;"> Maid</th>
              <th style="line-height: 18px;"> Customer</th>
              <th style="line-height: 18px;"> Activity Refernce number</th>
              <th style="line-height: 18px;"> Working Hours</th>
              <th style="line-height: 18px;"> M.O.P</th>
              <th style="line-height: 18px;"> Billed Amount</th>
              <th style="line-height: 18px;text-align:center;"> Action</th>
              <th style="line-height: 18px;"> Collected Amount</th>
              <th style="line-height: 18px;"> Balance Amount</th>
              <th style="line-height: 18px;"> Receipt No</th>
              <th style="line-height: 18px;"> Status</th>
              <!--<th style="line-height: 18px; width: 123px;"> Actions</th>-->

            </tr>
          </thead>
          <tbody>
            <?php
if (!empty($dayservices)) {
   $i = 0;
    $j = 1;
    $zone_id = 0;
    $maid_id = 0;
    $zone_id2 = 0;
    $total_fee = 0;
    $material_fee = 0;
    $collected_total_fee = 0;
    $total_wrk_hrs = 0;
    $collected_amount = 0;
    $ztotal_wrk_hrs = 0;
    $size =count((array)$dayservices);
    foreach ($dayservices as $service) {
        $check_buk_exist = $this->bookings_model->get_booking_exist($service->booking_id, $formatted_date);
        if (!empty($check_buk_exist)) {
            $mop = $check_buk_exist->mop;
            //$ref = $check_buk_exist->just_mop_ref;
        } else {
              if ($service->pay_by != "") {
                $mop = $service->pay_by;
              } else if($service->payment_method != "") {
                if ($service->payment_method == "0") {
                  $mop = "Cash";
                } else if ($service->payment_method == "1") {
                  $mop = "Card";
                } else if ($service->payment_method == "2") {
                  $mop = "Cheque";
                }  else if ($service->payment_method == "3") {
                  $mop = "Online";
                } 
              } else {
                $mop = $service->payment_mode;
            }
            //$ref = "";
        }
        //Payment Type
        if ($service->payment_type == "D") {
            $paytype = "(D)";
        } else if ($service->payment_type == "W") {
            $paytype = "(W)";
        } else if ($service->payment_type == "M") {
            $paytype = "(M)";
        } else {
            $paytype = "";
        }

        if ($service->maid_status == 0) {
            if (strtotime($formatted_date) > strtotime($service->maid_disabled_datetime)) {
                continue;
            }
        }

//                                $service_status = $service->service_status == 1 ? 'ON GOING' : ($service->service_status == 2 ? 'FINISHED' : ($service->service_status == 3 ? 'CANCELLED' : 'NOT STARTED'));
        $service_status = $service->service_status == 1 ? '' : ($service->service_status == 2 ? '' : ($service->service_status == 3 ? '' : ''));
        $class = $service->service_status == 1 ? 'on-going' : ($service->service_status == 2 ? 'finished' : ($service->service_status == 3 ? 'cancelled' : 'not-started'));
        $title = $service->service_status == 1 ? 'On-Going' : ($service->service_status == 2 ? 'Finished' : ($service->service_status == 3 ? 'Cancelled' : 'Not-Started'));

//$service_status .=  $service->payment_status == 1 ? (' ' . $service->total_fee) : ($service->payment_status == 0  && $service->service_status == 2 ? ' NP': '');

        $activity_status = 0;
        //$activity_status = $service->service_status ? $service->service_status : 0;
        $activity_status = $service->service_status == 1 ? 1 : ($service->service_status == 2 ? 2 : ($service->service_status == 3 ? 3 : 0));
        $activity_status = $service->payment_status == 1 ? 5 : ($service->payment_status == 0 && $service->service_status == 2 ? 6 : ($service->service_status == 1 && $service->payment_status != null ? 1 : ($service->service_status == 3 ? 3 : 0)));
        $service->total_fee = $service->service_status == 2 ? $service->total_fee : '';
        $service->material_fee = $service->service_status == 2 ? $service->material_fee : '';
        // if($service->t_zone != "")
        // {
        // $service->zone_id = $service->t_zone;
        // } else {
        // $service->zone_id = $service->zone_id;
        // }
        $transfer_text = (strlen($service->transferred_zone) > 0) ? "<br><b> - Transferred Zone - " . $service->transferred_zone . "</b>" : "";
        if ($maid_id != $service->maid_id) {
            if ($ztotal_wrk_hrs != 0) {

                echo '<tr>'
                . '<td colspan="4" style="line-height: 18px;"></td>'
                . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
                . '<td colspan="6" style="line-height: 18px;"></td>'
                //. '<td style="line-height: 18px;"></td>'
                 . '</tr>';
                $ztotal_wrk_hrs = 0;
            }
$maid_id = $service->maid_id;

            echo '<tr>'
            . '<td style="line-height: 18px;"></td>'
            . '<td style="line-height: 18px;"><b style="color:#CB3636;">' . $service->maid_name . '</b></td>'
            . '<td colspan="7" style="line-height: 18px;"></td>'
            //. '<td style="line-height: 18px;"></td>'
             . '</tr>';

        }
        if ($service->total_fee == "") {
            $tot_hrs = ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);

            if ($service->cleaning_material == 'Y') {
                $materialfee = ($tot_hrs * 5);
                $materialview = '(' . $materialfee . ')';
            } else {
                $materialview = "";
            }
            //$servicetotalfee = (($service->price_hourly)*((strtotime($service->end_time) - strtotime($service->start_time))/ 3600));total_amount
            if ($service->day_service_id != ""){
              $servicetotalfee = $service->_total_amount;
            } else {
              $servicetotalfee = $service->total_amount;
            }
        } else {
            $servicetotalfee = $service->total_fee;

            if ($service->cleaning_material == 'Y') {
                $materialfee = $service->material_fee;
                $materialview = '(' . $materialfee . ')';
            } else {
                $materialview = "";
            }

        }
        if ($service->day_service_id != ""){
          $servicebalance = $service->_total_amount;
        } else {
          $servicebalance = $service->total_amount;
        }
        //$total_fee += $service->total_fee;
        $total_fee += $servicetotalfee;
        $material_fee += $service->material_fee;
        $collected_total_fee += $service->collected_amount;
        $collected_amount += ($service->payment_status == 1 ? $service->total_fee : 0);
        $total_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);
        $ztotal_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600);
        //$total_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
        //$ztotal_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
        echo '<tr>'
        . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . ++$i . '</td>'
        . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . $service->maid_name . '</td>'
        . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . '">' . $service->customer_name . '(' . $service->odoo_customer_id . ') <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>) ' . $paytype . $transfer_text . '</td>'
         . '<td style="line-height: 18px;" class="book_type_' . $service->booking_type . ' day-service-reference-id-'. $service->booking_id.'">' . $service->day_service_reference_id . '</td>'
        . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time)) / 3600) . ']' . '</td>'
        . '<td style="line-height: 18px; text-align : center;" class="book_type_' . $service->booking_type . ' mop-'. $service->booking_id.'">' . $mop . '</td>'
        //. '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->total_fee . '</td>'
         . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . '">
         ' . $servicetotalfee . '
         </td>
         <td>
          <div class="btn-block btn-group-xs pull-right" role="group" aria-label="...">
              <button data-bookingid="'.$service->booking_id.'" data-customerpayment-id="'.$service->payment_id.'"  data-dayserviceid="'.$service->day_service_id.'" data-maid-id="'.$service->maid_id.'" data-maidpayment-id="" data-customer-id="'.$service->customer_id.'" onclick="jobCard(this)" type="button" class="btn btn-block btn-xs job-card-btn-'.$service->booking_id.'" style="display:'.($service->dispatch_status == 2 && $service->service_status != 3? '' : 'none;') .'">Job Card <i class="fa fa-question" aria-hidden="true"></i></button>
              <button data-bookingid="'.$service->booking_id.'" data-customerpaymentid="'.$service->payment_id.'"  data-maidpaymentid="" data-amount="" onclick="verifyPayment(this)" type="button" class="btn btn-block btn-xs verify-payment-btn-'.$service->booking_id.'" style="display:'.($service->collected_amount > 0 && $service->verified_status != 1 ? '' : 'none;') .'">Verify Payment <i class="fa fa-question" aria-hidden="true"></i></button>
              <button type="button" class="btn btn-block btn-xs verified-payment-btn-'.$service->booking_id.'" style="display:'.($service->verified_status == 1 ? '' : 'none;') .'" disabled>Payment Verified <i class="fa fa-check" aria-hidden="true"></i></button>
              <button data-bookingid="'.$service->booking_id.'" data-dayserviceid="'.$service->day_service_id.'" onclick="generateInvoice(this)" type="button" class="btn btn-block btn-xs '.($service->payment_type == 'D' ? '' : 'hidden').' create-invoice-btn-'.$service->booking_id.'" style="display:'.($service->service_status == 2 && $service->invoice_status == 0 ? '' : 'none;') .'">Create Invoice <i class="fa fa-question" aria-hidden="true"></i></button>
              <button type="button" class="btn btn-block btn-xs '.($service->payment_type == 'D' ? '' : 'hidden').' invoiced-btn-'.$service->booking_id.'" style="display:'.($service->invoice_status == 1 ? '' : 'none;') .'" disabled>Invoiced <i class="fa fa-check" aria-hidden="true"></i></button>
          </div>
         </td>'
        . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . ' collected-amount-'. $service->booking_id.'">
        ' . (number_format($service->collected_amount ?: 0,2)) . 
        '
        </td>'
        . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . ' balance-amount-'. $service->booking_id.'">' . number_format(($servicebalance - $service->collected_amount),2) . '</td>'
        . '<td style="line-height: 18px; text-align : right;" class="book_type_' . $service->booking_type . ' ps-no-'. $service->booking_id.'">' . ($service->ps_no ?: '-') . '</td>';
        if ($service->ps_no != "") {
            $pinkslip_no = $service->ps_no;
        } else {
            $pinkslip_no = "0";
        }
        if ($service->service_status == "") {
            $service->service_status = "0";
        }
        if ($service->collected_amount != "") {
            echo '<td style="line-height: 18px;text-align: center;" class="button-area-' . $service->booking_id . '"><a title=' . $title . ' href="javascript:void(0)" data-bind="' . $service->collected_amount . '" style="text-decoration:none;" onclick="get_activity_new(' . $activity_status . ', ' . $service->booking_id . ', this, ' . number_format($service->collected_amount,2) . ', ' . $service->service_status . ',' . $service->payment_status . ',' . $pinkslip_no . ');"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
        } else {
            echo '<td style="line-height: 18px;text-align: center;" class="button-area-' . $service->booking_id . '"><a  title=' . $title . '  href="javascript:void(0)" data-bind="' . $service->collected_amount . '" style="text-decoration:none;" onclick="get_activity(' . $activity_status . ', ' . $service->booking_id . ', this);"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
        }
        /*. '<td style="line-height: 18px;">'
        . '<a class="btn btn-small btn-success start-stop" title="Start" href="javascript:void" ' . $play . '><i class="icon-play"> </i></a>'
        . '<a class="btn btn-small btn-info start-stop" title="Stop" href="#payment-modal" data-toggle="modal" ' . $stop . '><i class="icon-stop"> </i></a>'
        . '<a class="btn btn-small btn-warning start-stop" title="Transfer" href="#transfer-modal" data-toggle="modal" ' . $transfer . '><i class="icon-share"> </i></a>'
        . '<a class="btn btn-small btn-danger start-stop" title="Cancel" href="javascript:void" ' . $cancel . '><i class="icon-ban-circle"> </i></a>'
        . '</td>'*/
        echo '</tr>';
        
        if ($j+1 == $size-1) {
                echo '<tr>'
                . '<td colspan="4" style="line-height: 18px;"></td>'
                . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
                . '<td colspan="7" style="line-height: 18px;"></td>'
                 . '</tr>';
        }
        $j++;

    }
    echo '<tr style="font-weight:bold;">'
    . '<td style="line-height: 18px;"></td>'
    . '<td style="line-height: 18px;"></td>'
    . '<td style="line-height: 18px;">TOTALS</td>'
    . '<td style="line-height: 18px;"></td>'
    . '<td style="line-height: 18px; text-align : right;">' . $total_wrk_hrs . '</td>'
    . '<td style="line-height: 18px; text-align : right;"></td>'
    . '<td style="line-height: 18px; text-align : right;">' . number_format($total_fee, 2) . '&nbsp;&nbsp;(' . number_format($material_fee, 2) . ')</td>'
    . '<td style="line-height: 18px; text-align : right;">' . /*number_format($collected_amount, 2) . */'</td>'
    . '<td style="line-height: 18px; text-align : right;">' . number_format($collected_total_fee, 2) . '</td>'
     .'<td style="line-height: 18px;"></td>'
     . '</tr>';
} else {
    ?>
            <tr>
              <td colspan="10"  style="line-height: 18px; text-align:justify;">No records found</td>
            </tr>
            <?php
}

?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->

    </div>
    </div>


    <!-- /widget -->
  </div>
  <!-- /span12 -->
</div>







<div id="action-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Action</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body" style="padding:30px 30px 50px;">
      <form>
        <p>
          <a class="btn-success n-btn green-btn mr-2" id="start" title="Start" href="javascript:void(0)" style="padding: 10px 20px !important;">
            <i class="icon-play"></i>&nbsp; START</a>

          <a class="btn-primary n-btn purple-btn mr-2" id="restart" title="Restart" href="javascript:void(0)" style="padding: 10px 20px !important;">
            <i class="icon-play"></i>&nbsp; RESTART</a>

          <a class="btn-warning n-btn red-btn mr-2" title="Stop" href="javascript:void(0)" style="padding: 10px 20px !important;">
            <i class="icon-stop"></i>&nbsp; STOP</a>

          <a class="btn-info n-btn blue-btn" title="Transfer" href="javascript:void(0)" style="padding: 10px 20px !important;">
            <i class="icon-share"></i>&nbsp; TRANSFER</a>


        </p>
        <!--<div class="controls" id="payment-details">
          <label class="radio inline">
            <input id="paymnt" type="radio" name="payment_type" value="1"> Payment </label>


          <label class="radio inline">
            <input id="no-paymnt" type="radio" name="payment_type" value="0"> Payment Not Received </label>


          <label class="radio inline">
            <input id="no-service" type="radio" name="payment_type" value="3"> Service Not Done </label>

        </div>-->




        <div class="row m-0 n-field-main pt-4" id="payment-details">

          <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3 n-radi-check-main">
               <div class="n-end">
                	<input id="paymnt" type="radio" value="1" name="payment_type" class="">
                    <label for="paymnt"> <span class="border-radius-3"></span> <p>Payment</p></label>
               </div>
          </div>

          <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3 n-radi-check-main">

               <div class="n-end">
                    <input id="no-paymnt" type="radio" value="0" name="payment_type" class="">
                    <label for="no-paymnt"> <span class="border-radius-3"></span> <p>Payment Not Received</p> </label>
               </div>
          </div>

          <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3 n-radi-check-main">

               <div class="n-end">
                    <input id="no-service" type="radio" value="3" name="payment_type" class="">
                    <label for="no-service"> <span class="border-radius-3"></span> <p>Service Not Done</p></label>
               </div>
          </div>
        </div>




                 <div class="row m-0 n-field-main payment_mode_customer" style="display:none;" id="pinkid">
                    <div class="col-sm-4 pl-0 n-field-box">
                           <p>Payment method</p>
                           <select name="payment_mode_id" id="payment_mode_id" class="sel2" style="width:100%;">
                           <option value="">-- Select mode --</option>
                           <option value="0">Cash</option>
                           <option value="1">Card</option>
                           <option value="2">Cheque</option>
                           <option value="3">Online</option>
                                         
                          </select>
                      </div>
                 	  <div class="col-sm-4 pl-0 n-field-box">
                           <p>Paid Amount</p>
                           <input type="number" step="any" autocomplete="off" id="paid-amount" style="font-family: Arial, Helvetica, sans-serif; color: #333; font-weight: bold;">
                      </div>

                      <div class="col-sm-4 pl-0 n-field-box">
                           <p>PS No</p>
                           <input type="number" autocomplete="off" id="ps-no" style="font-family: Arial, Helvetica, sans-serif; color: #333; font-weight: bold;">
                      </div>
                 </div>




                <div class="col-sm-12 p-0 pt-4" id="frm-transfer">
                 <div class="row m-0 n-field-main" id="transfer-zone-id">
                 	  <p>Select Driver</p>
                      <div class="pl-0 n-field-box">

                           <select required class="sel2" style="width:100%;">
                                <option value="">-- Select Driver --</option> <?php
foreach ($tablets as $val) {
    echo '<option value="' . $val->tablet_id . '">' . $val->tablet_driver_name . '</option>';
}
?>
					       </select>
                      </div>
                 </div>
                 </div>









    </div>
    <div class="modal-footer">
      <!--<button class="btn red-btn" onclick="closeFancy()">Close</button>-->
      <button class="n-btn green-btn mb-0" href="javascript:void(0)" type="submit" onclick="add_activity();">Update</button>
    </div>
    </form>
  </div>
</div>
</div>













<!--<div id="activity-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 style="color:white; padding-bottom: 0px !important;">Action</h3>
  </div>
  <div class="modal-body">
  <form >
    <p> <a class="btn btn-medium btn-success" id="start" title="Start" href="javascript:void(0)" ><i class="icon-play"> </i>START</a> <a class="btn btn-medium btn-primary" id="restart" title="Restart" href="javascript:void(0)" ><i class="icon-play"> </i>RESTART</a> <a class="btn btn-medium btn-warning" title="Stop" href="javascript:void(0)" ><i class="icon-stop"> </i>STOP</a> <a class="btn btn-medium btn-info" title="Transfer" href="javascript:void(0)"><i class="icon-share"> </i>TRANSFER</a>

    </p>
    <div class="controls" id="payment-details">
      <label class="radio inline">
        <input id="paymnt" type="radio" name="payment_type" value="1">
        Payment </label>
      <label class="radio inline">
        <input id="no-paymnt" type="radio" name="payment_type" value="0">
        Payment Not Received </label>
      <label class="radio inline">
        <input id="no-service" type="radio" name="payment_type" value="3">
        Service Not Done </label>
    </div>
    <div style="padding: 15px 0; display:none;" id="pinkid">
      <label class="inline" style="display: inline-block;">
        <input class="inline" type="text" id="paid-amount" style="width: 50px;"/>
      </label>
      <label class="inline" style="display: inline-block; padding-left: 15px;">PS No
        <input type="text" id="ps-no" style="width: 100px;"/>
      </label>
    </div>
    <p id="frm-transfer">

      <br>
      <select id="transfer-zone-id" required>
        <option value="">-- Select Driver --</option>
        <?php
foreach ($tablets as $val) {
    echo '<option value="' . $val->tablet_id . '">' . $val->tablet_driver_name . '</option>';
}

?>
      </select>

    </p>
    </div>
    <div class="modal-footer">
      <button class="btn red-btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <button class="btn green-btn" href="javascript:void(0)" type="submit" onclick="add_activity();">Submit</button>
    </div>
  </form>
</div>
-->
<script> 
function number_format(number, decimals) {
    return number.toFixed(decimals);
}
function jobCard(self){
  var booking_id = $(self).attr('data-bookingid');
  var customer_payment_id = $(self).attr('data-customerpaymentid');
  var maid_id = $(self).attr('data-maid-id');
  var customer_id = $(self).attr('data-customer-id');
  var day_service_id = $(self).attr('data-dayserviceid'); 
  var maid_payment_id = $(self).attr('data-maidpaymentid');
  var service_date = $('#vehicle_date').val();
  $.ajax({
    type: "POST",
    url: _base_url + "activity/job_card_details",
    data: { cust_pay_id: customer_payment_id, maid_pay_id: maid_payment_id, maid_id: maid_id, customer_id:customer_id, 
      booking_id: booking_id, day_service_id: day_service_id, service_date: service_date },
    dataType: "text",
    cache: false,
    success: function (data) { 
      var resp = $.parseJSON(data);
      var maid_data = resp.maid_job_details;
      var maid_payment_details = resp.maid_payment_details;
      var cards = resp.cards;
      var keys = resp.keys;
      // console.log(maid_payment_details)
      var maid_name = maid_data[0].maid_name;
      var printWindow = window.open("", "", "height=400,width=800");
      printWindow.document.write("<html><head><title></title>");
      printWindow.document.write("<style>");
      // printWindow.document.write("body { border: 5px solid black; padding: 20px; font-family: Arial, sans-serif; font-size: 14px; }");
      printWindow.document.write("body { border: 5px solid black; padding: 20px; font-family: Arial, sans-serif; font-size: 10px; width: 148mm; height: 210mm; margin: 0; }"); // Set to A5 size

      printWindow.document.write("u { text-decoration: underline; }");
      printWindow.document.write(".container { display: flex; justify-content: space-between; width: 100%; }");
      printWindow.document.write(".left-content { text-align: left; width: 50%; }");
      printWindow.document.write(".right-content { text-align: right; width: 50%; }");
      printWindow.document.write(".center-content { text-align: center; width: 50%; }");
      printWindow.document.write(".spacing { margin-bottom: 10px; }");
      printWindow.document.write(".no-spacing { margin-bottom: 0px; }"); // No spacing class
      printWindow.document.write(".container-spacing { margin-bottom: 10px; }");
      printWindow.document.write(".flex-container { display: flex; justify-content: space-between; align-items: center; }");
      printWindow.document.write(".left-align { text-align: left; }");
      printWindow.document.write(".center-content { text-align: center; width: 34%; font-weight: bold; text-decoration: underline; padding: 5px; border: 1px solid black; }"); // Added border and padding to Job Slip
      // printWindow.document.write(".center-align { text-align: center; flex-grow: 1; }");
      printWindow.document.write(".center-align { text-align: center; flex-grow: 1; font-weight: bold; text-decoration: underline; }");
      printWindow.document.write(".right-align { text-align: right; flex: 1; }");
      printWindow.document.write(".line { display: block; width: 100%; border-bottom: 1px solid black; font-weight: bold; margin-top: 1px; }");
      printWindow.document.write(".box-style {");
      printWindow.document.write("  border: 1px solid #000;"); // Adds a black border around the box
      printWindow.document.write("  padding: 10px;"); // Adds padding to the inside of the container box
      // printWindow.document.write("  margin: 10px 0;"); // Optional: Adds space between the boxes
      // printWindow.document.write("  display: flex;"); // Ensures items inside are flexed (side by side)
      // printWindow.document.write("  justify-content: space-between;"); // Ensures space between items
      // printWindow.document.write("  width: 100%;"); // Ensures space between items
      printWindow.document.write("}");
      printWindow.document.write(".col-md-12 {width:100%;}");

      printWindow.document.write("</style>");
      printWindow.document.write("</head><body>");
      // Create a flex container for MOLLY MAID and www.mollymaidme.com
      printWindow.document.write('<div class="container  container-spacing">');
      // Left-aligned for MOLLY MAID
      printWindow.document.write('<div class="left-content"><b>MOLLY MAID</b></div>');
      // Center-aligned for www.mollymaidme.com
      printWindow.document.write('<div class="left-content"><u>www.mollymaidme.com</u></div>');
      printWindow.document.write('<div class="right-content spacing"><b>No:</b><span> '+ (maid_data[0].receipt_no ? maid_data[0].receipt_no : '') +'</span></div>');
      printWindow.document.write('</div>');  // Close the first container
      // Underlined text: Changing houses to homes over a decade
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-content"><u>Changing houses to homes over a decade</u></div>');
      printWindow.document.write('<div class="right-content spacing"><b>Date:</b> ' + service_date + '</div>');
      printWindow.document.write('</div>');
      printWindow.document.write("<br />");
      // Second container with left and right content (P O Box, Tel, Date, No, etc.)
      printWindow.document.write('<div style="display:flex;">');
      // Left-aligned information
      printWindow.document.write('<div class="left-content no-spacing">');
      printWindow.document.write('<div><b>P O Box 32903</b></div>');
      printWindow.document.write('<div><b>Email:</b> contact@mollymaidme.com</div>');
      printWindow.document.write('<div class="spacing"><b>Tel:</b> 04-3397799 <b>Fax:</b> 04-3397789</div>');
      printWindow.document.write('<div class="flex-container">');
      printWindow.document.write('<div class="left-align"><b>TRN:</b> 100366541900003</div>');
      printWindow.document.write('<div class="center-content"><u>Job Slip / Invoices</u></div>');
     //  printWindow.document.write('<div class="right-align"><b>card no:</b> </div>');
      printWindow.document.write('</div>');
      printWindow.document.write('</div>');
      // Right-aligned information
      printWindow.document.write('<div class="right-content">');
      printWindow.document.write('<div class="spacing"><b>Key No:</b> <span>'+(keys ? keys : '')+'</span></div>');
      printWindow.document.write('<div class="spacing"><b>Card no:</b> <span>'+(cards ? cards : '')+'</span></div>');
      printWindow.document.write('</div>');

      printWindow.document.write('</div>'); // Close second container
      printWindow.document.write('<span class="line"></span>');
      printWindow.document.write('<div class="container  container-spacing">');
      printWindow.document.write('<div class="left-content"><b>Maid:</b><span> '+ maid_name +'</span></div>');
      printWindow.document.write('<div class="left-content"><b>Driver:</b></div>');
      printWindow.document.write('<div class="right-content spacing"><b>Area:</b><span>  '+maid_data[0].area_name+'</span></div>');
      printWindow.document.write('</div>');  
      printWindow.document.write('<div class="box-style">');
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-content"><b>Customer No:</b><span> ' + maid_data[0].customer_code + '</span></div>');
      printWindow.document.write('<div class="right-content spacing"><b>Time:</b><span> ' + moment(maid_data[0].time_from, 'hh:mm A').format('hh:mm A') + ' - ' + moment(maid_data[0].time_to, 'hh:mm A').format('hh:mm A') + '</span></div>');
      printWindow.document.write('</div>'); // End of first row
      // printWindow.document.write("<br />"); // Line break between rows

      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-content"><b>Customer:</b><span> ' + maid_data[0].customer_name + '</span></div>');
      printWindow.document.write('<div class="right-content spacing"><b>Rate:</b> ' + maid_data[0]._total_amount + '</div>');
      printWindow.document.write('</div>'); // End of second row
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-align" style="padding-left: 72px;"><span> ' + maid_data[0].customer_address + '</span></div>');
      printWindow.document.write('<div class="right-content spacing"><b>No.of.hours:</b> ' + maid_data[0].working_minutes / 60 + '</div>');
      printWindow.document.write('</div>');  
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="right-content spacing" style="margin-left: 333px;"><b>Amount:</b> ' + maid_data[0]._total_amount + '</div>');
      printWindow.document.write('</div>');  
      printWindow.document.write('</div>'); 
      printWindow.document.write('<div class="box-style">');
      printWindow.document.write('<div class="container  container-spacing">');
      printWindow.document.write('<div class="left-content"><b>Receipt No:</b><span> '+ (maid_data[0].receipt_no ? maid_data[0].receipt_no : '') +'</span></div>');
      printWindow.document.write('<div class="left-content"><b>Amount:</b><span> '+ maid_data[0]._total_amount +'</span></div>');
      printWindow.document.write('<div class="right-content spacing"><b>Maid Sign:</b></div>');
      printWindow.document.write('</div>');  
      printWindow.document.write('</div>'); 
      printWindow.document.write('<table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">');
      printWindow.document.write('<thead>');
      printWindow.document.write('<th>Date</th>');
      printWindow.document.write('<th>Inv No</th>');
      printWindow.document.write('<th>Service</th>');
      printWindow.document.write('<th>Hrs</th>');
      printWindow.document.write('<th>Amount</th>');
      printWindow.document.write('<th>VAT</th>');
      printWindow.document.write('<th>Paid</th>');
      printWindow.document.write('<th>Balance</th>');
      printWindow.document.write('</thead>');
      printWindow.document.write('<tbody>');
      printWindow.document.write('<tr>');
      var totalAmount = 0;
      var vat_total = 0;
      var total_paid = 0;
      var total_hours = 0;
      let balance_tot = 0;
      if (maid_payment_details && maid_payment_details.length > 0) {
        maid_payment_details.forEach(item => {
        var working_minutes = item.working_minutes;
        var hours = working_minutes /60;
        var booking_type = item.booking_type;
        if(booking_type == "WE") {
          var service = "Regular schedule";
        } else {
          var service = "One off Schedule";
        }
        totalAmount += parseFloat(item._total_amount);
        vat_total += parseFloat(item._vat_amount);
        total_paid += (item.paid_amount ? parseFloat(item.paid_amount) : 0);
        const balance = number_format((parseFloat(item._total_amount) - (item.paid_amount ? parseFloat(item.paid_amount) : 0)), 2);
        // total_hours += hours;
        balance_tot += parseFloat(balance);
        printWindow.document.write('<tr>');
        printWindow.document.write('<td>' + item.service_date + '</td>');
        printWindow.document.write('<td>' + (item.receipt_no ? item.receipt_no : '') + '</td>');
        printWindow.document.write('<td>' + service + '</td>');
        printWindow.document.write('<td style="text-align:right;">' + hours + '</td>');
        printWindow.document.write('<td style="text-align:right;">' + item._total_amount + '</td>');
        printWindow.document.write('<td style="text-align:right;">' + (item._vat_amount ? item._vat_amount : '') + '</td>');
        printWindow.document.write('<td style="text-align:right;">' + (item.paid_amount ? item.paid_amount : '') + '</td>');
        printWindow.document.write('<td style="text-align:right;">' + (balance ? balance : '') + '</td>');
        printWindow.document.write('</tr>');
      });
      printWindow.document.write('<tr>');
      printWindow.document.write('<th colspan="4" class="text-right" style="text-align:right;">Total</th>');
      printWindow.document.write('<th class="text-right" style="text-align:right;">'+ number_format(totalAmount, 2) +'</th>');
      printWindow.document.write('<th class="text-right" style="text-align:right;">'+ number_format(vat_total, 2)+'</th>');
      printWindow.document.write('<th class="text-right" style="text-align:right;">'+ number_format(total_paid, 2)+'</th>');
      printWindow.document.write('<th class="text-right" style="text-align:right;">'+ number_format(balance_tot, 2)+'</th>');
      printWindow.document.write('</tr>');
      printWindow.document.write('</tr>');                                                         
      printWindow.document.write('</tbody>');                     
      printWindow.document.write('</table>'); 
    } else { 
      printWindow.document.write('<table width="100%">'); 
      printWindow.document.write('<tbody>'); 
      printWindow.document.write('<tr>');
      printWindow.document.write('<td colspan="8" style="text-align: center;">No records found.</td>');
      printWindow.document.write('</tr>');
      printWindow.document.write('</tbody>');
      printWindow.document.write('</table>');
    }
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-align" style="padding-left: 22px; margin-top:10px;"><b>Dr.Bal:<span> ' + number_format(totalAmount, 2)+ '</span></b> <span style="padding-left:48px;"><b>Cr.Bal: <span>000</span></b></span><span style="padding-left:68px;"><b>VAT: <span style="padding-left:35px;">'+number_format(vat_total, 2)+'</span></b></span><span style="padding-left:84px;"><b>Bal.Amt: <span style="padding-left:21px;">'+number_format(balance_tot, 2)+'  Dr</span></b></span></div>');
      printWindow.document.write('</div>');  
      printWindow.document.write('<span class="line"></span>');
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-align" style="padding-left: 22px; margin-top:10px;"><b>Receipt No:<span style="padding-left:35px;"> ' +(maid_data[0].receipt_no ? maid_data[0].receipt_no : '')+ '</span></b> <span style="padding-left:224px;"><b>'+maid_data[0].maid_name+'</b></span></div>');
      printWindow.document.write('</div>');  
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-align" style="padding-left: 87px; margin-top:19px;"><b>Amount:</b></div>');
      printWindow.document.write('<div class="left-content" style="padding-left: 87px; margin-top:23px;"><b><span class="line" style="padding-left:25px;"></span></b></div>');
     
      printWindow.document.write('</div>'); 
      printWindow.document.write('<div class="container">');
      printWindow.document.write('<div class="left-align" style="padding-left: 87px; margin-top:18px;"><input type="checkbox" value="cash"><span style="padding-left:10px;"><b>Cash:</b></span><b><span style="padding-left:25px;"><input type="checkbox" value="cheque"><span style="padding-left:1px;">Cheque</span></b></span></div>');
      printWindow.document.write('<div class="left-content" style="padding-left: 52px; margin-top:10px;">'+maid_data[0].customer_name+'<b><span style="padding-left:156px;">Cust Sign</span></b></span></div>');
      printWindow.document.write('</div>'); 
      printWindow.document.write("</body></html>");
      printWindow.document.close();
      printWindow.print();   
    },
    error: function (result) {
        
    }
  });
}
function verifyPayment(self){
  var booking_id = $(self).attr('data-bookingid');
  var customer_payment_id = $(self).attr('data-customerpaymentid');
  var maid_payment_id = $(self).attr('data-maidpaymentid');
  if (confirm('Are you sure you want to verify the payment?')) {
            $.ajax({
                type: "POST",
                url: _base_url + "activity/verify_maid_payment",
                data: { cust_pay_id: customer_payment_id, maid_pay_id: maid_payment_id },
                dataType: "text",
                cache: false,
                success: function (result) {
                    var _resp = $.parseJSON(result);
                    if (_resp.status == 'success') {
                      $('.verify-payment-btn-'+booking_id).hide();
                      $('.verified-payment-btn-'+booking_id).show();
                        _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Payment has been verified successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close alert-popup-close" style="float:none;" /></div></div>';
                    } else {
                        _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
                    }

                    if (_alert_html != '') {
                        $.fancybox.open({
                            autoCenter: true,
                            fitToView: false,
                            scrolling: false,
                            openEffect: 'fade',
                            openSpeed: 100,
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                                }
                            },
                            padding: 0,
                            closeBtn: false,
                            content: _alert_html
                        });
                    }
                },
                error: function (result) {
                    $('#verifpay' + maid_pay_id).html('Verify Payment');
                    _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
                    if (_alert_html != '') {
                        $.fancybox.open({
                            autoCenter: true,
                            fitToView: false,
                            scrolling: false,
                            openEffect: 'fade',
                            openSpeed: 100,
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                                }
                            },
                            padding: 0,
                            closeBtn: false,
                            content: _alert_html
                        });
                    }
                }
            });
        }
}
function generateInvoice(self) {
  var booking_id = $(self).attr('data-bookingid');
	if ($('#day-service-id-of-booking-' + booking_id).val()) {
    var day_service_id = $('#day-service-id-of-booking-' + booking_id).val();
		if (confirm('Are you sure you want to create the invoice?')) {
			$.ajax({
				type: "POST",
				url: _base_url + "activity/create_invoice",
				data: {
					service_id: day_service_id,
				},
				dataType: "text",
				cache: false,
				success: function(result) {
					var _resp = $.parseJSON(result);
					if (_resp.status == 'success') {
            $('.invoiced-btn-'+booking_id).show();
            $('.create-invoice-btn-'+booking_id).hide();
						_alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Invoice has been generated successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close alert-popup-close" style="float:none;" /></div></div>';
					} else {
						_alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
					}
					if (_alert_html != '') {
						$.fancybox.open({
							autoCenter: true,
							fitToView: false,
							scrolling: false,
							openEffect: 'fade',
							openSpeed: 100,
							helpers: {
								overlay: {
									css: {
										'background': 'rgba(0, 0, 0, 0.3)'
									},
									closeClick: false
								}
							},
							padding: 0,
							closeBtn: false,
							content: _alert_html
						});
					}
				}
			});
		}
	} else {
		alert('Day Service Not Found !');
	}
}
</script>