<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
</style>
<div id="disable-popup" style="display:none;">
	<div class="popup-main-box">
		<div class="col-md-12 col-sm-12 green-popup-head">
			<span id="b-maid-name">Confirm Disable ?</span>
			<span id="b-time-slot"></span>
			<span class="pop_close n-close-btn">&nbsp;</span>
		</div>
		<div class="modal-body">
			<h3>Are you sure you want to disable this customer ?</h3>
		</div>
		<div class="modal-footer">
			<button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
			<button type="button" class="n-btn red-btn mb-0" onclick="confirm_enable_disable()">Disable</button>
		</div>
	</div>
</div>
<div id="enable-popup" style="display:none;">
	<div class="popup-main-box">
		<div class="col-md-12 col-sm-12 green-popup-head">
			<span id="b-maid-name">Confirm Enable ?</span>
			<span id="b-time-slot"></span>
			<span class="pop_close n-close-btn">&nbsp;</span>
		</div>
		<div class="modal-body">
			<h3>Are you sure you want to enable this customer ?</h3>
		</div>
		<div class="modal-footer">
			<button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
			<button type="button" class="n-btn mb-0" onclick="confirm_enable_disable()">Enable</button>
		</div>
	</div>
</div>
<div id="avatar-popup" style="display:none;">
	<div class="popup-main-box">
		<div class="col-md-12 col-sm-12 green-popup-head">
			<span id="b-maid-name" class="avatar-popup-name"></span>
			<span id="b-time-slot"></span>
			<span class="pop_close n-close-btn">&nbsp;</span>
		</div>
		<div id="" class="col-12 p-0">
			<div class="modal-body">
				<div class="img-container">
					<img id="avatar-popup-image" src="#">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">

				<ul>
					<li>
						<i class="icon-th-list"></i>
						<h3>Customers</h3>
					</li>

					<li>
						<span style="margin-left:5px;"></span>
						<div class="mm-drop">
							<select style="width: 100px;" id="all-customers">
								<option value="">All</option>
								<option value="1" selected="selected">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>

					</li>

					<li>

						<span style="color: #fff;">From :</span>
						<input type="text" style="width: 100px;" id="cust_from_date" />

					</li>

					<li>
						<span style="color: #fff;">To :</span>
						<input type="text" style="width: 100px;" id="cust_to_date" />

					</li>

					<li>
						<div class="mm-drop">
							<select style="width: 160px;" id="payment_type">
								<option value="" selected="selected">All Pay Type</option>
								<option value="D">Daily</option>
								<option value="W">Weekly</option>
								<option value="M">Monthly</option>
							</select>
						</div>


					</li>

					<li>
						<div class="mm-drop">
							<select style="width: 160px;" id="sort_source">
								<option value="">Source</option>
								<option value="Rizek">Rizek</option>
								<option value="Facebook">Facebook</option>
								<option value="Google">Google</option>
								<option value="Yahoo">Yahoo</option>
								<option value="Bing">Bing</option>
								<option value="Direct Call">Direct Call</option>
								<option value="Flyers">Flyers</option>
								<option value="Referral">Referral</option>
								<option value="Watchman/Security Guard">Watchman/Security Guard</option>
								<option value="Maid">Maid</option>
								<option value="Driver">Driver</option>
								<option value="By email">By email</option>
								<option value="Schedule visit">Schedule visit</option>
								<option value="Website">Website</option>
								<option value="Referred by staff">Referred by staff</option>
								<option value="Referred by customer">Referred by customer</option>
								<option value="Justmop">Justmop</option>
								<option value="Matic">Matic</option>
								<option value="ServiceMarket">ServiceMarket</option>
								<option value="Emaar">Emaar</option>
								<option value="MyHome">MyHome</option>
								<option value="Tekram">Tekram</option>
							</select>
						</div>
					</li>
					<li>
						<div class="mm-drop">
							<select style="width: 160px;" id="sort_cust_type">
								<option value="">Customer</option>
								<option value="1">Regular</option>
								<option value="0">Non-Regular</option>
							</select>
						</div>


					</li>

					<li>
						<input type="text" id="keyword-search" Placeholder="Search customer" style="width: 160px;">


					</li>

					<li class="mr-0 float-right">


						<div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>customer/add" title="Add Customer"> <i class="fa fa-user-plus"></i></a> </div>





						<?php
						if (user_authenticate() == 1) {
						?>
							<!--<div class="topiconnew"><a href="<? php // echo base_url();
																	?>customer/toExcel/<? php // echo $active;
																				?>"><img src="<? php // echo base_url();
																								?>images/excel-icon.png" title="Add Customer"/></a></div>-->



							<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="customerexcelbtn"> <i class="fa fa-file-excel-o"></i></a> </div>

						<?php
						}
						?>

					</li>
				</ul>
			</div>
			<!--<div id="LoadingImage" style="text-align:center;display:none;position:absolute;top:133px;right:0px;width:100%;height:100%;background-position:center;"><img src="<?php echo base_url() ?>img/loader.gif"></div>-->
			<div class="widget-content" style="margin-bottom:30px">
				<table id="customerlisttable" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;">
								<center>Sl. No.</center>
							</th>
							<th style="width:50px;">Image</th>
							<th style="">Client Name</th>
							<th style="">Mobile</th>
							<th style="width:80px;">Per Hour</th>
							<th style="">Area</th>
							<th style="">Address</th>
							<th style="">Source</th>
							<th style="width:70px;">Last Job</th>
							<th style="">Added Date</th>
							<th style="width: 85px !important; text-align: center !important;">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>

		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>