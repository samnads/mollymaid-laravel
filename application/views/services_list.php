<link rel="stylesheet" href="https://booking.emaid.info:3443/emaid-demo/css/demo.css" />
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this service ?</h3>
      <input type="hidden" id="delete_zoneid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_id" id="delete_id">
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Service</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Service Type</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="service_type_name" name="service_type_name" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Service Rate</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="number" id="service_rate" name="service_rate" required>
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="services_sub">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Service</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Service Type</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_service_type_name" name="edit_service_type_name" required>
              <input type="hidden" id="edit_service_type_id" name="edit_service_type_id">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Service Rate</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="number" id="edit_service_rate" name="edit_service_rate" required>
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="services_edit">Update</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
            <ul>
            <li><i class="icon-th-list"></i>
              <h3>Services</h3></li>
              <li class="mr-0 float-right">
              
              
              <div class="topiconnew border-0 green-btn">
              	   <a onclick="newPopup();" title="Add"> <i class="fa fa-plus"></i></a>
              </div>
              
              </li>
              </ul>

            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table id="services-list-table" class="table table-striped table-bordered da-table">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px; text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px"> Services </th>
							<th style="line-height: 18px"> Service ID </th>
                            <th style="line-height: 18px"> Rate </th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
							<th style="line-height: 18px;" class="td-actions">Show in Web</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
if (count($services) > 0) {
    $i = 1;
    foreach ($services as $services_val) {
        $webshow = $services_val['web_status'];
        ?>
                        <tr>
                            <td style="line-height: 18px; width: 20px; text-align:center;"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $services_val['service_type_name']; ?></td>
							<td style="line-height: 18px"><?php echo $services_val['service_type_id']; ?></td>
                            <td style="line-height: 18px"><?php echo $services_val['service_rate']; ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_get(<?php echo $services_val['service_type_id']; ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if (user_authenticate() == 1) {?>
								<a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $services_val['service_type_id']; ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php }?>
							</td>
							<td style="line-height: 18px" class="td-actions">
								<label class="switch_tog">
								  <input type="checkbox"  class="show_web_tog" data-serid="<?php echo $services_val['service_type_id']; ?>"  <?php if ($webshow == 1) {echo 'checked';}?>>
								  <span class="slider_tog round"></span>
								</label>
							</td>
                        </tr>
                        <?php
$i++;
    }
}
?>

                    </tbody>
                </table>
            </div>
            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div>
    <!-- /span6 -->
    <div class="span6" id="edit_services" style="display: none;">
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Edit Services</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_services();"><img src="<?php echo base_url(); ?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">
					<label class="control-label" for="flatname">Service Type</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_service_type_name" name="edit_service_type_name" required="required">
                                                <input type="hidden" class="span3" id="edit_service_type_id" name="edit_service_type_id">
                                            </div> <!-- /controls -->
                                    </div> <!-- /control-group -->

                                    <div class="control-group">
					<label class="control-label" for="tablet_imei">Service Rate</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_service_rate" name="edit_service_rate" required="required">
                                            </div> <!-- /controls -->
                                    </div> <!-- /control-group -->




                                    <br />

                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="services_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
</div>
<script>
/*********************************************************************************** */
// zones - script
(function (a) {
    a(document).ready(function (b) {
        if (a('#services-list-table').length > 0) {
            a("table#services-list-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,
                "scrollY": true,
                "orderMulti": false,
                'bFilter':true,
                "lengthChange": true,
                "pageLength": 100,
                'columnDefs': [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function newPopup(){
  $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#new-popup'),
  });
}
function edit_get(id) {
  $('.mm-loader').show();
	$.ajax({
		type: "POST",
		url: _base_url + "settings/edit_services",
		data: {
			service_id: id
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			//alert(result);
			var obj = jQuery.parseJSON(result);
			$.each($.parseJSON(result), function(edit, value) {
				$('#edit_service_type_id').val(value.service_type_id);
				$('#edit_service_type_name').val(value.service_type_name);
				$('#edit_service_rate').val(value.service_rate);
			});
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'none',
				openSpeed: 1,
				autoSize: false,
				width: 450,
				height: 'auto',
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: $('#edit-popup'),
			});
      $('.mm-loader').hide();
		},
		error: function() {
      $('.mm-loader').hide();
			alert('Error !')
		}
	});
}
function closeFancy() {
	$.fancybox.close();
}
function confirm_delete_modal(id)
{
  $('#delete_id').val(id);
    $.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'none',
				openSpeed: 1,
				autoSize: false,
				width: 450,
				height: 'auto',
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: $('#delete-popup'),
			});
}
function confirm_delete() {
	$.ajax({
		type: "POST",
		url: _base_url + "settings/remove_services",
		data: {
			service_id: $('#delete_id').val()
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			window.location.assign(_base_url + 'settings/services');
		}
	});
}
/*********************************************************************************** */
</script>