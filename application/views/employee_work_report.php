<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Employee Work Report</h3>  
                    </li>
                    
                    <li>                 
                    <span style="margin-left:23px;">Select Maid :</span>
                    <select style=" width:160px; " id="maid" name="maid">
                        <option value="">-- Select Maids --</option>
                        <?php
                        if (!empty($maids) > 0) {
                            foreach ($maids as $maid) {
                                ?>
                                <option value="<?php echo $maid['maid_id']; ?>" <?php
                                if (!empty($search_condition)) {
                                    echo ($search_condition['maid_id'] == $maid['maid_id']) ? 'selected="selected"' : '';
                                }
                                ?>><?php echo $maid['maid_name']; ?></option>

                                <?php
                            }
                        }
                        ?>
                    </select>
                    </li>
                    
                    <li>
                    <span style="margin-left:23px;">Select Month :</span>
                    <select style=" width:160px; " name="month" id="month">
                        <option >-- Select Month --</option>
                        <option value="01" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '01') ? 'selected="selected"' : '';
                        }
                        ?>>January</option>
                        <option value="02" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '02') ? 'selected="selected"' : '';
                        }
                        ?>>February</option>
                        <option value="03" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '03') ? 'selected="selected"' : '';
                        }
                        ?>>March</option>
                        <option value="04" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '04') ? 'selected="selected"' : '';
                        }
                        ?>>April</option>
                        <option value="05" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '05') ? 'selected="selected"' : '';
                        }
                        ?>>May</option>
                        <option value="06" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '06') ? 'selected="selected"' : '';
                        }
                        ?>>June</option>
                        <option value="07" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '07') ? 'selected="selected"' : '';
                        }
                        ?>>July</option>
                        <option value="08" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '08') ? 'selected="selected"' : '';
                        }
                        ?>>August</option>
                        <option value="09" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '09') ? 'selected="selected"' : '';
                        }
                        ?>>September</option>
                        <option value="10" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '10') ? 'selected="selected"' : '';
                        }
                        ?>>October</option>
                        <option value="11" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '11') ? 'selected="selected"' : '';
                        }
                        ?>>November</option>
                        <option value="12" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '12') ? 'selected="selected"' : '';
                        }
                        ?>>December</option>

                    </select>
                    </li>
                    <li>
                    <span style="margin-left:23px;">Select Year :</span>
                    <select style=" width:160px;" id="year" name="year">
                        <option value="">-- Select Year --</option>
                        <?php
                        for ($i = 2014; $i <= 2025; $i++) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php
                            if (!empty($search_condition)) {
                                echo ($search_condition['year'] == $i) ? 'selected="selected"' : '';
                            }
                            ?>><?php echo $i; ?></option>

                            <?php
                        }
                        ?>
                    </select>
                    </li>
                    <li>
                    <span style="margin-left:15px;"></span>
                    <input type="submit" class="n-btn" value="Go" name="activity_report">
                    </li>
                    
                    <li class="mr-0 float-right">
                    
                    <div class="topiconnew border-0 green-btn">
                    	<a href="#" id="EmpWorkPrint" title="Print"> <i class="fa fa-print"></i></a>
                    </div>
                    
                    <div class="topiconnew border-0 green-btn">
                    	<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>
                    
                    </li>
                    </ul>
                    
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <div style="width: 100%; /*width:1180px;*/">
                    <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%" style="line-height:20px !important">
                        <thead>
                            <tr>
                                <th style="width: 10%" >Date</th>
                                <th > 08am-10am</th>
                                <th > 10am-12pm</th>
                                <th > 12pm-02pm</th>
                                <th > 02pm-04pm</th>
                                <th > 04pm-06pm</th>
                                <th > 06pm-08pm</th>
                                <th > 08pm-10pm</th>
                                <th > 10pm-12am</th>
                                <th > Total [Hrs]</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($reports)) {
                                //echo "<pre>";print_r($reports);exit;
                                $tot = 0;
                                foreach ($reports as $key => $value) {
                                    
                                    $newDate = date("d/m/Y", strtotime($key));
                                    $i = 0; $total_hrs_served = 0; 
                                    ?>
                                    <tr>
                                        <td><?php echo $newDate; ?></td>
                                        <td><?php
                                            
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];
                                                
                                                
                                                if (("08:00:00" >= $time_from && "08:00:00" < $time_to) || ("10:00:00" > $time_from && "10:00:00" <= $time_to)) {
                                                    if($val['time_diff'] > 2 && ("08:00:00" == $time_from && "10:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("09:00:00" == $time_from && "10:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("10:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("09:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                }
                                                /*if (("08:00:00" >= $time_from && "08:00:00" < $time_to) || ("10:00:00" > $time_from && "10:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("10:00:00" > $time_from && "10:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served = 2;
                                                    }
                                                    else if("10:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                }*/
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("10:00:00" >= $time_from && "10:00:00" < $time_to) || ("12:00:00" > $time_from && "12:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("10:00:00" == $time_from && "12:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("11:00:00" == $time_from && "12:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("12:00:00" > $time_to)
                                                    {
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("10:00:00")) / 60) / 60;
                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("11:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    //echo $total_hrs_served;
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    
                                                    /*if($val['time_diff'] > 2 && ("12:00:00" > $time_from && "12:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("12:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("10:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("12:00:00" >= $time_from && "12:00:00" < $time_to) || ("14:00:00" > $time_from && "14:00:00" <= $time_to)) {
                                                    
                                                    /*if($val['time_diff'] > 2 && ("14:00:00" > $time_from && "14:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("14:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                    if($val['time_diff'] > 2 && ("12:00:00" == $time_from && "14:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("13:00:00" == $time_from && "14:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("14:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("13:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("14:00:00" >= $time_from && "14:00:00" < $time_to) || ("16:00:00" > $time_from && "16:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("14:00:00" == $time_from && "16:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("15:00:00" == $time_from && "16:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("16:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("15:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("16:00:00" > $time_from && "16:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("16:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("16:00:00" >= $time_from && "16:00:00" < $time_to) || ("18:00:00" > $time_from && "18:00:00" <= $time_to)) {
                                                    if($val['time_diff'] > 2 && ("16:00:00" == $time_from && "18:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("17:00:00" == $time_from && "18:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("18:00:00" > $time_to)
                                                    { 
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("17:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("18:00:00" > $time_from && "18:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("18:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        
                                        <!--vishnu-->
                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("18:00:00" >= $time_from && "18:00:00" < $time_to) || ("20:00:00" > $time_from && "20:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("18:00:00" == $time_from && "20:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("19:00:00" == $time_from && "20:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("20:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("18:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("19:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("20:00:00" > $time_from && "20:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("20:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("18:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("20:00:00" >= $time_from && "20:00:00" < $time_to) || ("22:00:00" > $time_from && "22:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("20:00:00" == $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("21:00:00" == $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("22:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("20:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("21:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("22:00:00" > $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("22:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("20:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("22:00:00" >= $time_from && "23:00:00" <= $time_to) || ("23:00:00" > $time_from && "23:00:00" <= $time_to)) {
                                                    if($val['time_diff'] > 2 && ("22:00:00" == $time_from && "24:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("23:00:00" == $time_from && "24:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("24:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("22:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("23:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("22:00:00" > $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("22:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("20:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        <!-- ends -->


                                        <td>
                                            <?php 
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            } 
                                            ?>
                                        </td>
                                    </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><strong>Total</strong></td>
                                            <td align="center"><strong><?php echo $tot; ?></strong></td>
                                        </tr>
                             <?php           
                                        
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="employeeWorkPrint">
    <table border="1" width="100%" cellspacing="0" cellpadding="10">
        <thead>
            <tr>
                <th >Date</th>
                <th > 08am-10am</th>
                <th > 10am-12pm</th>
                <th > 12pm-02pm</th>
                <th > 02pm-04pm</th>
                <th > 04pm-06pm</th>
                <th > 06pm-08pm</th>
                <th > 08pm-10pm</th>
                <th > 10pm-12am</th>
                <th > Total [Hrs]</th>
            </tr>

        </thead>
        <tbody>
                            <?php
                            if (!empty($reports)) {
                                $tot = 0;
                                foreach ($reports as $key => $value) {
                                    $newDate = date("d/m/Y", strtotime($key));
                                    $i = 0; $total_hrs_served = 0; 
                                    ?>
                                    <tr>
                                        <td><?php echo $newDate; ?></td>
                                        <td><?php
                                            
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];
                                                
                                                
                                                if (("08:00:00" >= $time_from && "08:00:00" < $time_to) || ("10:00:00" > $time_from && "10:00:00" <= $time_to)) {
                                                    if($val['time_diff'] > 2 && ("08:00:00" == $time_from && "10:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("09:00:00" == $time_from && "10:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("10:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("09:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                }
                                                /*if (("08:00:00" >= $time_from && "08:00:00" < $time_to) || ("10:00:00" > $time_from && "10:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("10:00:00" > $time_from && "10:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served = 2;
                                                    }
                                                    else if("10:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                }*/
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("10:00:00" >= $time_from && "10:00:00" < $time_to) || ("12:00:00" > $time_from && "12:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("10:00:00" == $time_from && "12:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("11:00:00" == $time_from && "12:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("12:00:00" > $time_to)
                                                    {
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("10:00:00")) / 60) / 60;
                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("11:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    //echo $total_hrs_served;
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    
                                                    /*if($val['time_diff'] > 2 && ("12:00:00" > $time_from && "12:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("12:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("10:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("12:00:00" >= $time_from && "12:00:00" < $time_to) || ("14:00:00" > $time_from && "14:00:00" <= $time_to)) {
                                                    
                                                    /*if($val['time_diff'] > 2 && ("14:00:00" > $time_from && "14:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("14:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                    if($val['time_diff'] > 2 && ("12:00:00" == $time_from && "14:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("13:00:00" == $time_from && "14:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("14:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("13:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("14:00:00" >= $time_from && "14:00:00" < $time_to) || ("16:00:00" > $time_from && "16:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("14:00:00" == $time_from && "16:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("15:00:00" == $time_from && "16:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("16:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("15:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("16:00:00" > $time_from && "16:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("16:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("16:00:00" >= $time_from && "16:00:00" < $time_to) || ("18:00:00" > $time_from && "18:00:00" <= $time_to)) {
                                                    if($val['time_diff'] > 2 && ("16:00:00" == $time_from && "18:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("17:00:00" == $time_from && "18:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("18:00:00" > $time_to)
                                                    { 
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("17:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("18:00:00" > $time_from && "18:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("18:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        
                                        <!--vishnu-->
                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("18:00:00" >= $time_from && "18:00:00" < $time_to) || ("20:00:00" > $time_from && "20:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("18:00:00" == $time_from && "20:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("19:00:00" == $time_from && "20:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("20:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("18:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("19:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("20:00:00" > $time_from && "20:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("20:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("18:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("20:00:00" >= $time_from && "20:00:00" < $time_to) || ("22:00:00" > $time_from && "22:00:00" <= $time_to)) {
                                                    
                                                    if($val['time_diff'] > 2 && ("20:00:00" == $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("21:00:00" == $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("22:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("20:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("21:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("22:00:00" > $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("22:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("20:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            foreach ($value as $val) {
                                                //Payment Type
                                                if($val['payment_type'] == "D")
                                                {
                                                    $paytype = "(D)";
                                                } else if($val['payment_type'] == "W")
                                                {
                                                    $paytype = "(W)";
                                                } else if($val['payment_type'] == "M")
                                                {
                                                    $paytype = "(M)";
                                                } else
                                                {
                                                    $paytype = "";
                                                }
                                                
                                                $time_from = $val['time_from'];
                                                $time_to = $val['time_to'];

                                                if (("22:00:00" >= $time_from && "23:00:00" <= $time_to) || ("23:00:00" > $time_from && "23:00:00" <= $time_to)) {
                                                    if($val['time_diff'] > 2 && ("22:00:00" == $time_from && "24:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    } else if($val['time_diff'] > 2 && ("23:00:00" == $time_from && "24:00:00" < $time_to)){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else if("24:00:00" > $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("22:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    } else if("23:00:00" == $time_from){
                                                        $time_display = '1 Hr';
                                                        $total_hrs_served += 1;
                                                    } else {
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }
                                                    /*if($val['time_diff'] > 2 && ("22:00:00" > $time_from && "22:00:00" < $time_to)){
                                                        $time_display = '2 Hrs';
                                                        $total_hrs_served += 2;
                                                    }
                                                    else if("22:00:00" >= $time_to)
                                                    {                                                           
                                                        $time_display = ((strtotime($val['time_to']) - strtotime("20:00:00")) / 60) / 60;

                                                        $total_hrs_served += $time_display;
                                                        $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                    }
                                                    
                                                    if ($val['type'] === 'WE') {
                                                        echo '<p style="color:#9e6ab8">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    } else if ($val['type'] === 'OD') {
                                                        echo '<p style="color:#ff7223">' . $val['customer'] .' '.$paytype. '<br />[ ' . $time_display . ' ]</p>';
                                                    }*/
                                                }
                                            }
                                            ?>
                                        </td>
                                        <!-- ends -->


                                        <td>
                                            <?php 
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
                                                
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            } 
                                            ?>
                                        </td>
                                    </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><strong>Total</strong></td>
                                            <td align="center"><strong><?php echo $tot; ?></strong></td>
                                        </tr>
                             <?php 
                    }
                    ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("employeeWorkPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "EmployeeWorkReport.xls"); // Choose the file name
      return false;
  }  
</script>