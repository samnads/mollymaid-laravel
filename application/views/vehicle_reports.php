<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
    .topiconnew{cursor: pointer;}
    .show_full,.show_less{
        cursor:pointer;
    }
</style>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Vehicle Reports </h3>
                    </li>

                    <li class="mr-2">
                    <?php
if ($search['search_date'] == "") {
    ?>
                        <input type="text" style="width: 160px;" id="vehicle_date" name="vehicle_date" value="<?php echo date('d/m/Y', strtotime('-3 day')); ?>">

                        <?Php
} else {
    ?>
                        <input type="text" style="width: 160px;" id="vehicle_date" name="vehicle_date" value="<?php echo $search['search_date'] ?>">

                        </li>
                    <li>
                        <?php
}
$s_date2 = explode("/", $search['search_date']);
$t_dates = $s_date2[2] . '-' . $s_date2[1] . '-' . $s_date2[0];

$s_date4 = explode("/", $search['search_date_to']);
$t_dates2 = $s_date4[2] . '-' . $s_date4[1] . '-' . $s_date4[0];
$custtid = $search['customer'];
if ($search['search_date_to'] == "") {?>
                    <input type="text" style="width: 160px;" id="vehicle_date_to" name="vehicle_date_to" value="<?php echo date('d/m/Y'); ?>">

                    <?Php
} else {
    ?>
                     <input type="text" style="width: 160px;" id="vehicle_date_to" name="vehicle_date_to" value="<?php echo $search['search_date_to'] ?>">
                        <?php
}
?>
                    </li>
                    <li>

                    <span style="color:#FFF;">Zone :</span>

                    <?php
if ($search['search_zone'] == "") {
    ?>

                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
if (count($zones) > 0) {
        foreach ($zones as $zones_val) {
            ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>

                                    <?php
}
    }
    ?>
                        </select>

                        <?Php
} else {
    ?>
                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
if (count($zones) > 0) {
        foreach ($zones as $zones_val) {
            ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>" <?php echo isset($search['search_zone']) ? ($search['search_zone'] == $zones_val['zone_id'] ? 'selected="selected"' : '') : '' ?> ><?php echo $zones_val['zone_name']; ?></option>
                                    <?php
}
    }
    ?>
                        </select>
                        <?Php
}
?>
                    </li>
                    <li>
                    <span style="color:#FFF;">Customer :</span>
                    <select style="width:160px;" id="customers_vh_rep" name="customers_vh_rep">
                            <option value="0">-- Select Customer --</option>

                    </select>
                    </li>
                    <li class="mr-2">
                    <span style="color:#FFF;">Company :</span>
                    <?php $comp = $search['company_name'];?>
					<select style="width:160px;" id="company_vh_rep" name="company_vh_rep">
						<option value="">-- Select Company --</option>
						<option value="Justmop" <?php if ($comp == 'Justmop') {echo 'selected';}?>>Justmop</option>
						<!-- <option value="Matic" <?php if ($comp == 'Matic') {echo 'selected';}?>>Matic</option> -->
						<option value="ServiceMarket" <?php if ($comp == 'ServiceMarket') {echo 'selected';}?>>ServiceMarket</option>
						<!-- <option value="Helpling" <?php if ($comp == 'Helpling') {echo 'selected';}?>>Helpling</option> -->
						<!-- <option value="Urban Clap" <?php if ($comp == 'Urban Clap') {echo 'selected';}?>>Urban Clap</option> -->
						<option value="MyHome" <?php if ($comp == 'MyHome') {echo 'selected';}?>>MyHome</option>
						<option value="Rizek" <?php if ($comp == 'Rizek') {echo 'selected';}?>>Rizek</option>
					</select>

                    </li>
                    <li class="mr-2">
                    <input type="hidden" name="day" id="day" value="<?php echo $search['search_day'] ?>">
                    <input type="hidden" name="zone_name" id="zone_name" value="<?php echo $search['search_zone_name'] ?>">
                    </li>
                    <li>
                    <input type="submit" class="n-btn" value="Go" name="vehicle_report">
                    </li>

                    <li class="mr-0 float-right">

                    <div class="topiconnew border-0 green-btn">
                    	<a href="#" id="printButton" title="Print"> <i class="fa fa-print"></i></a>
                    </div>

                    <div class="topiconnew border-0 green-btn">
                    	<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>

                    </li>
                    </ul>

                </form>
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="width:45px"> Sl. No. </th>
                            <th> Source</th>
                            <th> Cleaner</th>
                            <th> Client Name</th>
                            <th style="width:99px"> Customer Type</th>
                        <!--<th> Ref Code</th>-->
                            <th style="width:200px;"> Location</th>
                            <th> MAT</th>
                            <th> M.O.P</th>
                            <th> HRS</th>
                            <th> Total</th>
                            <th> VAT</th>
                            <th style="width:99px"> Grand Total</th>
                            <th> Service Type</th>
                            <th> Timings</th>
                        <!--<th> Zone</th>-->
                            <th> Date</th>
                            <th style="width:100px"> Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
$sub_t = 0;
$vat_t = 0;
$grand_t = 0;
if ($vehicle_report != null) {
    $i = 1;
    foreach ($vehicle_report as $veh) {
        //Payment Type
        if ($veh['customer_paytype'] == "D") {
            $paytype = "Daily";
        } else if ($veh['customer_paytype'] == "W") {
            $paytype = "Weekly";
        } else if ($veh['customer_paytype'] == "M") {
            $paytype = "Monthly";
        } else {
            $paytype = "";
        }
        $t_shrt = $veh['time_to'];
        $cur_shrt = $veh['time_from'];
        $hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);

        if ($veh['cleaning_material'] == 'Y') {
            $mat = "Yes";
        } else {
            $mat = "No";
        }
        //price calculation
        $normal_hours = 0;
        $extra_hours = 0;
        $weekend_hours = 0;

        $normal_from = strtotime('08:00:00');
        $normal_to = strtotime('20:00:00');

        $shift_from = strtotime($veh['time_from']);
        $shift_to = strtotime($veh['time_to']);

        $total_hours = ($shift_to - $shift_from) / 3600;
        if ($veh['cleaning_material'] == 'Y') {
            $materialfee = ($total_hours * 10);
        } else {
            $materialfee = 0;
        }
        if ($search['search_date'] == "") {
            $schedule_date = date('d/m/Y');
        } else {
            $schedule_date = $search['search_date'];
        }
        $s_date = explode("/", $schedule_date);
        $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

        $today_week_day = date('w', strtotime($service_date));

        if ($today_week_day != 5) // except friday  case // changed on 23-01-2017
        {
            if ($shift_from < $normal_from) {
                if ($shift_to <= $normal_from) {
                    $extra_hours = ($shift_to - $shift_from) / 3600;
                }

                if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                    $extra_hours = ($normal_from - $shift_from) / 3600;
                    $normal_hours = ($shift_to - $normal_from) / 3600;
                }

                if ($shift_to > $normal_to) {
                    $extra_hours = ($normal_from - $shift_from) / 3600;
                    $extra_hours += ($shift_to - $normal_to) / 3600;
                    $normal_hours = ($normal_to - $normal_from) / 3600;
                }
            }

            if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                if ($shift_to <= $normal_to) {
                    $normal_hours = ($shift_to - $shift_from) / 3600;
                }

                if ($shift_to > $normal_to) {
                    $normal_hours = ($normal_to - $shift_from) / 3600;
                    $extra_hours = ($shift_to - $normal_to) / 3600;
                }
            }

            if ($shift_from > $normal_to) {
                $extra_hours = ($shift_to - $shift_from) / 3600;
            }
        } else {
            $weekend_hours = ($shift_to - $shift_from) / 3600;
        }

        $service_description = array();

        $service_description['normal'] = new stdClass();
        $service_description['normal']->hours = $normal_hours;
        if ($veh['booked_from'] == "W") {
            $service_description['normal']->fees = $normal_hours * $veh['price_per_hr'];
        } else {
            $service_description['normal']->fees = $normal_hours * $veh['price_hourly'];
        }
        $service_description['normal']->fees;
        $service_description['extra'] = new stdClass();
        $service_description['extra']->hours = $extra_hours;
        $service_description['extra']->fees = $extra_hours * $veh['price_extra'];

        $service_description['weekend'] = new stdClass();
        $service_description['weekend']->hours = $weekend_hours;
        $service_description['weekend']->fees = $weekend_hours * $veh['price_weekend'];

        $location_charge = 0;

        $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees + $location_charge - $veh['discount'];

        $vat_fee = ($total_fee * (5 / 100));

        $netpay = ($total_fee + $vat_fee);
        //ends
        $subtotal = ($veh['total_amount'] / 1.05);
        $vat = ($subtotal * 0.05);
        $sub_t += round($subtotal, 2);
        $vat_t += round($vat, 2);
        $grand_t += $veh['total_amount'];
        ?>
                                <tr>
                                    <td style="line-height: 18px;text-align:center"><?php echo $i; ?> </td>
                                    <td style="line-height: 18px;"><?php echo $veh['customer_source'] ?> </td>
                                    <td style="line-height: 18px;"><?php echo $veh['maid'] ?> </td>
                                    <td style="line-height: 18px">
                                            <?php

        if ($veh['booking_type'] == "OD") {
            ?>
                                                <p style="color: #ff7223;">
                                                     <?php echo $veh['customer'] ?>
                                                </p>

                                            <?php
} else if ($veh['booking_type'] == "WE") {
            ?>
                                            <p style="color: #9e6ab8;">
                                               <?php echo $veh['customer'] ?>
                                            </p>

                                            <?php
} else {
            ?>
											<p style="color: #9e6ab8;">
                                               <?php echo $veh['customer'] ?>
                                            </p>
											<?php }?>
                                    </td>

                                    <td style="line-height: 18px;"><?php echo $paytype ?> </td>

                                    <!--<td style="line-height: 18px;">
                                        <?php
if ($veh['customer_source'] == "Justmop") {
            if ($veh['booking_type'] == "OD") {
                $ref = $veh['justmop_reference'];
            } else {
                $ref = $veh['justmop_reference'];
            }
        } else {
            $ref = "";
        }
        echo $ref;
        ?>
                                    </td>-->
                                    <td style="line-height: 18px; text-align:left; overflow-wrap: break-word; word-break: break-all;"><?php echo $veh['customer_address'] ?></td>
                                    <td style="line-height: 18px;"><?php echo $mat; ?></td>
                                    <td style="line-height: 18px;"><?php echo $veh['payment_mode']; ?></td>
                                    <td style="line-height: 18px;"><?php echo $hours ?> </td>
                                    <td style="line-height: 18px;"><?php echo round($subtotal, 2); ?></td>
                                    <td style="line-height: 18px;"><?php echo round($vat, 2); ?></td>
                                    <td style="line-height: 18px;"><?php echo $veh['total_amount']; ?></td>
                                    <td style="line-height: 18px;"><?php echo $veh['service_type_name']; ?></td>
                                    <td style="line-height: 18px">
                                            <?php echo date("g:i A", strtotime($veh['time_from'])); ?> &nbsp; - &nbsp;
                                            <?php echo date("g:i A", strtotime($veh['time_to'])); ?>
                                            <?php //echo $veh['time_from'] ?> <?php //echo $veh['time_to'] ?>
                                    </td>
                                    <!--<td style="line-height: 18px;"><?php// echo $veh['zone_name'] ?> </td>-->
                                    <td style="line-height: 18px;"><?php echo $veh['veh_date'] ?> </td>
                                    <td style="line-height: 18px;">
                                    <?php
if (strlen($veh['booking_note']) > 30) {
            echo '<span class="half_text">' . substr($veh['booking_note'], 0, 30) . '...<p class="text-green show_full"> <i class="icon-chevron-sign-down"></i> more</p></span>';
            echo '<span class="full_text" style="display:none">' . $veh['booking_note'] . ' <p class="text-green show_less"> <i class="icon-chevron-sign-up"></i> less</p></span>';
        } else {
            echo $veh['booking_note'] ?: '-';
        }
        ?>
                                    </td>
                                    </tr>
                            <?php
$i++;
    }
    ?>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>Total</td>
								<td><?php echo $sub_t; ?></td>
								<td><?php echo $vat_t; ?></td>
								<td><?php echo $grand_t; ?></td>
								<td></td>
                                                                <td></td>
								<td></td>
								<td></td>
							</tr>
							<?php
}?>



                    </tbody>
                </table>
            </div><!-- /widget-content -->

        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>

<div id="divPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "0">
              <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px;"> Sl.No</th>
                            <th style="line-height: 18px; padding: 5px;"> Source</th>
                            <th style="line-height: 18px; padding: 5px;"> Cleaner</th>
                            <th style="line-height: 18px; padding: 5px;"> Client Name</th>
                            <th style="line-height: 18px; padding: 5px;"> Customer Type</th>
                            <th style="line-height: 18px; padding: 5px;"> Ref Code</th>
                            <th style="line-height: 18px; padding: 5px;"> Location</th>
                            <th style="line-height: 18px; padding: 5px;"> MAT</th>
                            <th style="line-height: 18px; padding: 5px;"> M.O.P</th>
                            <th style="line-height: 18px; padding: 5px;"> HRS</th>
                            <th style="line-height: 18px; padding: 5px;"> Total</th>
                            <th style="line-height: 18px; padding: 5px;"> VAT</th>
                            <th style="line-height: 18px; padding: 5px;"> Grand Total</th>
                            <th style="line-height: 18px; padding: 5px;"> Service<br>Type</th>
                            <th style="line-height: 18px; padding: 5px;;"> Timings</th>
                            <!--<th style="line-height: 18px; padding: 5px;"> Zone</th>-->
                            <th style="line-height: 18px; padding: 5px;"> Date</th>
                            <th style="line-height: 18px; padding: 5px;"> Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
$sub_t = 0;
$vat_t = 0;
$grand_t = 0;
if ($vehicle_report != null) {
    $i = 1;
    foreach ($vehicle_report as $veh) {
        //Payment Type
        if ($veh['customer_paytype'] == "D") {
            $paytype = "Daily";
        } else if ($veh['customer_paytype'] == "W") {
            $paytype = "Weekly";
        } else if ($veh['customer_paytype'] == "M") {
            $paytype = "Monthly";
        } else {
            $paytype = "";
        }
        $t_shrt = $veh['time_to'];
        $cur_shrt = $veh['time_from'];
        $hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);
        if ($veh['cleaning_material'] == 'Y') {
            $mat = "Yes";
        } else {
            $mat = "No";
        }

        //price calculation
        $normal_hours = 0;
        $extra_hours = 0;
        $weekend_hours = 0;

        $normal_from = strtotime('08:00:00');
        $normal_to = strtotime('20:00:00');

        $shift_from = strtotime($veh['time_from']);
        $shift_to = strtotime($veh['time_to']);

        $total_hours = ($shift_to - $shift_from) / 3600;
        if ($veh['cleaning_material'] == 'Y') {
            $materialfee = ($total_hours * 10);
        } else {
            $materialfee = 0;
        }
        if ($search['search_date'] == "") {
            $schedule_date = date('d/m/Y');
        } else {
            $schedule_date = $search['search_date'];
        }
        $s_date = explode("/", $schedule_date);
        $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];

        $today_week_day = date('w', strtotime($service_date));

        if (date('w') != 5) // except friday  case // changed on 23-01-2017
        {
            if ($shift_from < $normal_from) {
                if ($shift_to <= $normal_from) {
                    $extra_hours = ($shift_to - $shift_from) / 3600;
                }

                if ($shift_to > $normal_from && $shift_to <= $normal_to) {
                    $extra_hours = ($normal_from - $shift_from) / 3600;
                    $normal_hours = ($shift_to - $normal_from) / 3600;
                }

                if ($shift_to > $normal_to) {
                    $extra_hours = ($normal_from - $shift_from) / 3600;
                    $extra_hours += ($shift_to - $normal_to) / 3600;
                    $normal_hours = ($normal_to - $normal_from) / 3600;
                }
            }

            if ($shift_from >= $normal_from && $shift_from < $normal_to) {
                if ($shift_to <= $normal_to) {
                    $normal_hours = ($shift_to - $shift_from) / 3600;
                }

                if ($shift_to > $normal_to) {
                    $normal_hours = ($normal_to - $shift_from) / 3600;
                    $extra_hours = ($shift_to - $normal_to) / 3600;
                }
            }

            if ($shift_from > $normal_to) {
                $extra_hours = ($shift_to - $shift_from) / 3600;
            }
        } else {
            $weekend_hours = ($shift_to - $shift_from) / 3600;
        }

        $service_description = array();

        $service_description['normal'] = new stdClass();
        $service_description['normal']->hours = $normal_hours;
        if ($veh['booked_from'] == "W") {
            $service_description['normal']->fees = $normal_hours * $veh['price_per_hr'];
        } else {
            $service_description['normal']->fees = $normal_hours * $veh['price_hourly'];
        }

        $service_description['extra'] = new stdClass();
        $service_description['extra']->hours = $extra_hours;
        $service_description['extra']->fees = $extra_hours * $veh['price_extra'];

        $service_description['weekend'] = new stdClass();
        $service_description['weekend']->hours = $weekend_hours;
        $service_description['weekend']->fees = $weekend_hours * $veh['price_weekend'];

        $location_charge = 0;

        $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees + $location_charge - $veh['discount'];

        $vat_fee = ($total_fee * (5 / 100));

        $netpay = ($total_fee + $vat_fee);
        //ends

        $subtotal = ($veh['total_amount'] / 1.05);
        $vat = ($subtotal * 0.05);

        $sub_t += round($subtotal, 2);
        $vat_t += round($vat, 2);
        $grand_t += $veh['total_amount'];

        ?>
                                <tr>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo $i; ?> </td>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo $veh['customer_source'] ?> </td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $veh['maid'] ?> </td>
					<td style="line-height: 18px; padding: 5px;">
                                            <?php

        if ($veh['booking_type'] == "OD") {
            ?>
                                                <p style="color: #ff7223;">
                                                     <?php echo $veh['customer'] ?>
                                                </p>

                                            <?php
} else if ($veh['booking_type'] == "WE") {
            ?>
                                            <p style="color: #9e6ab8;">
                                               <?php echo $veh['customer'] ?>
                                            </p>

                                            <?php
} else {
            ?>
											<p style="color: #9e6ab8;">
                                               <?php echo $veh['customer'] ?>
                                            </p>
											<?php }?>
					</td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $paytype ?> </td>
					<td>
					<?php
if ($veh['customer_source'] == "Justmop") {
            if ($veh['booking_type'] == "OD") {
                $ref = $veh['justmop_reference'];
            } else {
                $ref = $veh['justmop_reference'];
            }
        } else {
            $ref = "";
        }
        echo $ref;
        ?>
					</td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $veh['customer_address'] ?></td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $mat; ?></td>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo $veh['payment_mode']; ?></td>
					<td style="line-height: 18px; padding: 5px;"><?php echo $hours ?> </td>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo round($subtotal, 2); ?></td>
					<td style="line-height: 18px; padding: 5px;"><?php echo round($vat, 2); ?></td>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo $veh['total_amount']; ?></td>
                                        <td style="line-height: 18px; padding: 5px;"><?php echo $veh['service_type_name']; ?></td>
                                        <td style="line-height: 18px; padding: 5px;">
                                            <?php echo date("g:i A", strtotime($veh['time_from'])); ?> &nbsp; - &nbsp;
                                            <?php echo date("g:i A", strtotime($veh['time_to'])); ?>
                                        </td>
					<!--<td style="line-height: 18px; padding: 5px;"><?php// echo $veh['zone_name'] ?> </td>-->
					<td style="line-height: 18px; padding: 5px;"><?php echo $veh['veh_date'] ?> </td>
					<td style="line-height: 18px; padding: 5px;">
							<?php
echo wordwrap($veh['booking_note'], 10);
        ?>
					</td>
                                    </tr>
                            <?php
$i++;
    }
    ?>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>

								<td>Total</td>
								<td><?php echo $sub_t; ?></td>
								<td><?php echo $vat_t; ?></td>
								<td><?php echo $grand_t; ?></td>
								<td></td>
                                                                <td></td>
								<td></td>
								<td></td>
							</tr>
							<?php
}?>



                    </tbody>
        </table>
    </div><!-- /widget-content -->
</div>
<script type="text/javascript">
function exportF(elem) {
	var table = document.getElementById("divPrint");
	var html = table.outerHTML;
	var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url
	elem.setAttribute("href", url);
	elem.setAttribute("download", "Vehicle Report.xls"); // Choose the file name
	return false;
}
$('.show_full').click(function() {
	$('.full_text').hide();
	$('.half_text').show();
	$(this).parent('span').hide();
	$(this).parent('span').parent('td').children('.full_text').show();
});
$('.show_less').click(function() {
	$('.full_text').hide();
	$('.half_text').show();
});
</script>