<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Salesman</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Salesman Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="salesman_name" autocomplete="off" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="description" autocomplete="off" >
            </div>
          </div>
         </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="salesman_sub">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Salesman</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Salesman Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_salesmanname" id="edit_salesmanname" autocomplete="off" required>
              <input type="hidden" name="edit_salesmanid" id="edit_salesmanid">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_description" id="edit_description" autocomplete="off" >
            </div>
          </div>
         </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="salesman_edit">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Salesman</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this salesman ?</h3>
      <input type="hidden" id="delete_salesmanid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Salesman List</h3>

              <div class="topiconnew border-0 green-btn">
              	   <a onclick="newModal()" title="Add"> <i class="fa fa-plus"></i></a>
              </div>

            </div>
            <!-- /widget-header -->
             <div class="widget-content">

                <table id="zone-list-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px"> Salesman Name </th>
                            <th style="line-height: 18px"> Description</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
if (count($salesman) > 0) {
    $i = 1;
    foreach ($salesman as $val) {
        ?>
                        <tr>
                            <td style="line-height: 18px; width: 20px;  text-align:center;"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $val['salesman_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $val['description'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_salesman_get(<?php echo $val['salesman_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if (user_authenticate() == 1) {?>
								<a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $val['salesman_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php }?>
							</td>
                        </tr>
                        <?php
$i++;
    }
}
?>

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div>
    <!-- /span6 -->
    <div class="span6" id="edit_zone" style="display: none;">

    </div> <!-- /span6 -->

</div>
<script>
/*********************************************************************************** */
(function (a) {
    a(document).ready(function (b) {
        if (a('#zone-list-table').length > 0) {
            a("table#zone-list-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,  "scrollY": true,
                "orderMulti": false,
                'bFilter':true,
                "lengthChange": false,
                'columnDefs': [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function edit_salesman_get(salesman_id){
  $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "salesman/edit_salesman",
        data: {salesman_id: salesman_id },
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_salesmanid').val(value.salesman_id)
                $('#edit_salesmanname').val(value.salesman_name)
                $('#edit_description').val(value.description)
            });
            fancybox_show('edit-popup', { width: 450 });
              $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
}
function closeFancy(){
  $.fancybox.close();
}
function confirm_delete_modal(salesman_id){
    $('#delete_salesmanid').val(salesman_id);
    fancybox_show('delete-popup', { width: 450 });
}
function confirm_delete(){
     $.ajax({
            type: "POST",
            url: _base_url + "salesman/remove_salesman",
            data: {salesman_id: $('#delete_salesmanid').val() },
            dataType: "text",
            cache: false,
            success: function (result) {
              if (result == 1) {
                    alert("Cannot Delete! ");
                }
                else {
                    alert("Salesman Deleted Successfully");
                    setTimeout(location.reload.bind(location), 1);
                }
               
            },
            error:function(data){
                alert(data.statusText);
                console.log(data);
            }
        });
}
function newModal(){
  fancybox_show('new-popup', { width: 450 });
}
/*********************************************************************************** */
</script>
