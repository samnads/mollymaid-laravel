<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Online Payment Reports</h3>                   
                  </li>
                  </ul>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl.NO</th>
                            <th style="line-height: 18px;"> Customer Name</th>
                            <th style="line-height: 18px;"> Maid Name</th>
                            <th style="line-height: 18px;">Amount</th>
                            <th style="line-height: 18px;">Transaction Id</th>
                            <th style="line-height: 18px;">Description</th>
                            <th style="line-height: 18px;"> Paid Date Time</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($online_payment_reports as $op_report)
                        {
                        ?>
                        <tr>
                            <td style="line-height: 18px;"><?php echo $i; ?></td>
                            <td style="line-height: 18px;"><?php echo $op_report->customer_name; ?></td>
                            <td style="line-height: 18px;"><?php echo $op_report->maid_name; ?></td>
                            <td style="line-height: 18px;"><?php echo $op_report->amount; ?></td>
                            <td style="line-height: 18px;"><?php echo $op_report->transaction_id; ?></td>
                            <th style="line-height: 18px;"><?php echo $op_report->description;?></th>
                            <td style="line-height: 18px;"><?php echo $op_report->payment_datetime; ?></td>
                        </tr>
                        <?php
                        $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>