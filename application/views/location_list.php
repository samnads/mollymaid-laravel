<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this location ?</h3>
      <input type="hidden" id="delete_areaid">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this area ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this location ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Location</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-location-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Location Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="locationname" autocomplete="off" id="locationname">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Area Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="area_id" id="area_id" class="sel2" style="width:100%">
                <option value="">-- Select Area--</option> <?php
                                                            if (count($areas) > 0) {
                                                              foreach ($areas as $areas_val) {
                                                            ?> <option value="<?php echo $areas_val['area_id']; ?>"> <?php echo $areas_val['area_name']; ?> </option> <?php
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                      ?>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea name="description" autocomplete="off" id="description"></textarea>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <div class="col-12">
            </div>
            <p>
              Minimum Hour

            </p>
            <div class="col-sm-12 p-0 n-field-box">
              <input class="no-arrows" type="number" id="new_min_booking_hour" name="new_min_booking_hour" autocomplete="off">
            </div>
          </div>

          <div class="row m-0 n-field-main">
            <p>Longitude</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input step="any" type="number" name="longitude" autocomplete="off" id="longitude">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Latitude</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input step="any" type="number" name="latitude" autocomplete="off" id="latitude">
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" name="location_sub">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="location-view-edit-title">Edit Location</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="edit-location-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Location Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_locationname" id="edit_locationname" autocomplete="off">
              <input type="hidden" class="span3" id="edit_locationid" name="edit_locationid" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Area Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="edit_area_id" id="edit_area_id" class="sel2" style="width:100%">
                <option value="">-- Select Area --</option> <?php
                                                            if (count($areas) > 0) {
                                                              foreach ($areas as $areas_val) {
                                                            ?> <option value="<?php echo $areas_val['area_id']; ?>"> <?php echo $areas_val['area_name']; ?> </option> <?php
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                      ?>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea name="edit_description" id="edit_description" autocomplete="off"></textarea>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <div class="col-12">
            </div>
            <p>
              Minimum Hour
            </p>
            <div class="col-sm-12 p-0 n-field-box">
              <input class="no-arrows" type="number" id="edit_min_booking_hour" name="edit_min_booking_hour" autocomplete="off">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Longitude</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="number" step="any" name="edit_longitude" id="edit_longitude" autocomplete="off">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Latitude</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="number" step="any" name="edit_latitude" id="edit_latitude" autocomplete="off">
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer" id="location-view-edit-footer">
      <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="location_edit">Save</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <ul>
          <li><i class="icon-th-list"></i>
            <h3>Location List</h3>
          </li>
          <li><select style="width:160px;" id="all_location" onchange="activefilter(this.value)">
              <option value="" <?php echo empty($status) ? 'selected' : ''; ?>>Select</option>
              <option value="0" <?php echo $status == '0' ? 'selected' : ''; ?>>Active</option>
              <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Inactive</option>
            </select></li>

          <li>

          </li>

          <li>

          </li>
          <li class="mr-0 float-right">

            <!-- <button type="button" class="n-btn mb-0 mr-3" onclick="confirm_disable_enable_all('enable')">Enable All Location</button>

                   <button type="button" class="n-btn purple-btn mb-0 mr-3" onclick="confirm_disable_enable_all('disable')">Block All Location</button> -->

            <div class="topiconnew border-0 green-btn">
              <a data-action="new-location-popup" title="Add"> <i class="fa fa-plus"></i></a>
            </div>

          </li>

      </div>
      <style>
        table.da-table tr td {
          padding: 0px 6px;
        }
      </style>
      <!-- /widget-header -->
      <div class="widget-content">
        <table id="area-list-table" class="table table-hover da-table">
          <!--                <table class="table table-striped table-bordered">-->
          <thead>
            <tr>
              <th>Sl. No.</th>
              <th style="">Location </th>
              <th style="">Area</th>
              <th style="">Zone</th>
              <th style="">Description</th>
              <th style="">Min Hour</th>

              <th style="width: 120px" class="td-actions">Actions</th>

            </tr>
          </thead>
          <tbody>
            <?php
            if (count($locations) > 0) {
              $i = 1;
              foreach ($locations as $areas_val) {
                //print_r($areas_val);die;
            ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td style="">
                    <?php echo $areas_val['location_name'] ?>
                  </td>
                  <td style=""><?php echo $areas_val['area_name'] ?></td>
                  <td style="">
                    <?php echo $areas_val['zone_name'] ?>
                  </td>
                  <td>
                    <?php echo $areas_val['description'] ?>
                  </td>
                  <td>
                    <?php echo $areas_val['min_booking_hour'] ?>
                  </td>

                  <td style="" class="td-actions">
                    <a href="javascript:;" class="n-btn-icon green-btn" data-action="view-location-popup" data-id="<?= $areas_val['location_id'] ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_location_get(<?php echo $areas_val['location_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                    <!-- <?php if (user_authenticate() == 1) { ?>
                      <a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $areas_val['location_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                    <?php } ?> -->
                    <!-- <a href="javascript:void(0)" class="n-btn-icon green-btn" style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);" title="" onclick="confirm_disable_enable_modal(<?php echo $areas_val['location_id'] ?>,<?php echo $areas_val['deleted_at'] ?>);">
                      <?php echo $areas_val['deleted_at'] == null ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?>
                    </a> -->

                    <a href="javascript:void(0)" class="n-btn-icon green-btn" style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);" title="" onclick="confirm_disable_enable_modal(<?php echo $areas_val['location_id'] ?>,'<?php echo $areas_val['deleted_at'] ?>');">
                      <?php echo $areas_val['deleted_at'] == null ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?>
                    </a>

                  </td>

                </tr>
            <?php
                $i++;
              }
            }
            ?>

          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div><!-- /span5 -->
</div>
<script>
  /*********************************************************************************** */
  var disable_areaid = null;
  var web_status = null;
  /*********************************************************************************** */
  var dataTable;

  $(function() {
    dataTable = $('#area-list-table').DataTable({
      'sPaginationType': "full_numbers",
      'bSort': true,
      'scrollY': true,
      'orderMulti': false,
      'bFilter': true,
      'lengthChange': true,
      'pageLength': 100,
      'columnDefs': [{
        'targets': [5],
        'orderable': false
      }],
      'language': {
        'zeroRecords': 'No location data found'
      }
    });

    activefilter($('#all_location').val());

    $('#all_location').on('change', function() {
      var status = $(this).val();
      activefilter(status);
    });
  });

  function activefilter(value) {
    if (value === "") {
        $("tbody tr").show();
    } else {
        $("tbody tr").hide();

        $("tbody tr").each(function() {
            var status = $(this).find("i.fa-toggle-on").length > 0 ? "0" : "1";
            if (status === value) {
                $(this).show();
            }
        });
    }
}


  /*********************************************************************************** */
  // function confirm_disable_enable_modal(area_id, status) {
  //   if (status == 0) {
  //     fancybox_show('enable-popup', {
  //       width: 450
  //     });
  //   } else {

  //     fancybox_show('disable-popup', {
  //       width: 450
  //     });
  //   }
  //   disable_areaid = area_id;
  //   web_status = status;
  // }

  function confirm_disable_enable_modal(location_id, status) {
    console.log(status);
    if (status !== "" && status !== null) {
      fancybox_show('enable-popup', {
        width: 450
      });
      status = 0;
    } else {
      fancybox_show('disable-popup', {
        width: 450
      });
      status = 1;
    }
    disable_areaid = location_id;
    web_status = status;
  }


  // *******************************************************************************************

  function confirm_delete_modal(location_id) {
    $('#delete_areaid').val(location_id);
    fancybox_show('delete-popup');

  }
  /*********************************************************************************** */
  function confirm_disable() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/locweb_status",
      data: {
        location_id: disable_areaid,
        location_status: web_status
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        window.location.assign(_base_url + 'location');
      }
    });
  }
  /*********************************************************************************** */
  function confirm_enable() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/locweb_status",
      data: {
        location_id: disable_areaid,
        location_status: web_status
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        window.location.assign(_base_url + 'location');
      }
    });
  }
  /*********************************************************************************** */
  function confirm_delete() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/remove_location",
      data: {
        location_id: $('#delete_areaid').val()
      },

      dataType: "text",
      cache: false,
      success: function(data) {
        location.reload();
      },
      error: function(data) {
        alert(data.statusText);
        console.log(data);
      }
    });
  }
  /*********************************************************************************** */
  function edit_location_get(location_id) {
    edit_location_validator.resetForm();
    $('#location-view-edit-title').html("Edit Location");
    $("#edit-popup :input").prop("disabled", false);
    $('#location-view-edit-footer').show();
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "settings/edit_location",
      data: {
        location_id: location_id
      },
      dataType: "json",
      cache: false,
      success: function(result) {
        $('#edit_locationid').val(result.location_id)
        $('#edit_locationname').val(result.location_name)
        $('#edit_area_id option[value="' + result.area_id + '"]').prop('selected', true);
        $('#edit_area_id').trigger('change');
        $('#edit_description').val(result.description)
        $('#edit_latitude').val(result.latitude)
        $('#edit_longitude').val(result.longitude)
        $('#edit_min_booking_hour').val(result.min_booking_hour)
        fancybox_show('edit-popup');
        $('.error-message').text('');
        $('.mm-loader').hide();
      },
      error: function(data) {
        toast('error', data.statusText);
      }
    });
  }
  /*********************************************************************************** */
  $('[data-action="new-location-popup"]').click(function(event) {
    new_location_validator.resetForm();
    $("#area_id").select2().val("").trigger("change");
    fancybox_show('new-popup');
    $('#add-new-location-form')[0].reset();
    $('.error-message').text('');
  });
  $('[data-action="view-location-popup"]').click(function(event) {
    edit_location_validator.resetForm();
    $('#location-view-edit-title').html("View Location");
    $("#edit-popup :input").prop("disabled", true);
    $('#location-view-edit-footer').hide();
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "settings/edit_location",
      data: {
        location_id: $(this).attr("data-id")
      },
      dataType: "json",
      cache: false,
      success: function(result) {
        $('#edit_locationid').val(result.location_id)
        $('#edit_locationname').val(result.location_name)
        $('#edit_area_id option[value="' + result.area_id + '"]').prop('selected', true);
        $('#edit_area_id').trigger('change');
        $('#edit_description').val(result.description)
        $('#edit_latitude').val(result.latitude)
        $('#edit_longitude').val(result.longitude)
        $('#edit_min_booking_hour').val(result.min_booking_hour)
        fancybox_show('edit-popup', {
          closeClick: false
        });
      },
      error: function(data) {
        toast('error', data.statusText);
      }
    });
  });
  $().ready(function() {
    new_location_validator = $('#add-new-location-form').validate({
      ignore: [],
      rules: {
        locationname: {
          required: true,
        },
        area_id: {
          required: true,
        },
        new_min_booking_hour: {
          required: true,
        }
      },
      messages: {
        locationname: "Enter location name.",
        area_id: "Select location area.",
        new_min_booking_hour: "Enter Minimim Hour",
      },
      errorPlacement: function(error, element) {
        if (element.attr("name") == "area_id") {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      },
      submitHandler: function(form) {
        //$('#add-new-location-form').submit();

        var locationname = $('#locationname').val();
        var area_id = $('#area_id').val();
        var longitude = $('#longitude').val();
        var latitude = $('#latitude').val();
        var description = $('#description').val();
        var new_min_booking_hour = $('#new_min_booking_hour').val();

        // Check if the zone name already exists
        $.ajax({
          url: "<?php echo base_url('settings/check_location_name'); ?>",
          type: "POST",
          dataType: "json",
          data: {
            locationname: locationname
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Location name already exists!');

            } else {
              // If the zone name is new, save the zone using AJAX
              $.ajax({
                url: "<?php echo base_url('settings/save_location'); ?>",
                type: "POST",
                dataType: "json",
                data: {
                  locationname: locationname,
                  description: description,
                  area_id: area_id,
                  longitude: longitude,
                  latitude: latitude,
                  new_min_booking_hour: new_min_booking_hour,
                },
                success: function(saveResponse) {

                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving location.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking location name.');
          }
        });


      }
    });
    edit_location_validator = $('#edit-location-form').validate({
      ignore: [],
      rules: {
        edit_locationname: {
          required: true,
        },
        edit_area_id: {
          required: true,
        },
        edit_locationid: {
          required: true,
        },
        edit_min_booking_hour: {
          required: true,
        }
      },
      messages: {
        locationname: "Enter location name.",
        area_id: "Select location area.",
        edit_min_booking_hour: "Enter Minimim Hour",
      },
      errorPlacement: function(error, element) {
        if (element.attr("name") == "edit_area_id") {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      },
      submitHandler: function(form) {
        // $('#edit-location-form').submit();

        var edit_locationname = $('#edit_locationname').val();
        var edit_locationid = $('#edit_locationid').val();
        var edit_area_id = $('#edit_area_id').val();
        var edit_longitude = $('#edit_longitude').val();
        var edit_latitude = $('#edit_latitude').val();
        var edit_description = $('#edit_description').val();
        var edit_min_booking_hour = $('#edit_min_booking_hour').val();

        $.ajax({
          url: "<?php echo base_url('settings/check_location_name_edit'); ?>",
          type: "POST",
          dataType: "json",
          data: {
            locationname: edit_locationname,
            edit_locationid: edit_locationid
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Location name already exists!');

            } else {

              $.ajax({
                url: "<?php echo base_url('settings/edit_location_new'); ?>",
                type: "POST",
                dataType: "json",
                data: {
                  edit_locationname: edit_locationname,
                  edit_locationid: edit_locationid,
                  edit_area_id: edit_area_id,
                  edit_longitude: edit_longitude,
                  edit_latitude: edit_latitude,
                  edit_description: edit_description,
                  edit_min_booking_hour: edit_min_booking_hour
                },
                success: function(saveResponse) {

                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving location.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking location name.');
          }
        });






      }
    });
  });
</script>