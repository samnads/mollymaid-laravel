<style>
    #area select{ width: 15% !important;}
</style>

<div class="row">
        
      
    <div class="span6" id="edit_sms_setting" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
        <ul>
        <li>
	      	<i class="icon-cogs"></i>
                    <h3>Edit SMS Settings</h3>
                    </li>
                    <li class="mr-0 float-right">
                   
                   
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="hideedit_sms_setting();" title="Hide"> <i class="fa fa-minus"></i></a>
                    </div> 
                    
                    </li>
                    </ul>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Sender ID </label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_sender_id" name="edit_sender_id" required="required">
                                            </div>   				
                                    </div>   
                                    <div class="control-group">											
					<label class="control-label" for="flatname">SMS API </label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_api" name="edit_api" required="required">
                                            </div>   				
                                    </div>   
                                    
                                    
                                                <input type="hidden" class="span3" id="edit_sms_id" name="edit_sms_id">
                                    
                                     
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">User Name</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_user" name="edit_user" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Password</label>
                                            <div class="controls">
                                                <input type="password" class="span3" id="edit_pass" name="edit_pass" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="sms_edit_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>SMS Settings</h3>
              <!--<a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_hourly_price();"><img src="<?php echo base_url();?>img/add.png" title="Add Services"/></a>-->
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Sender ID </th>
                            <th style="line-height: 18px"> API </th>
                            <th style="line-height: 18px"> User Name</th>
                            <th style="line-height: 18px"> Password</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($sms_settings) > 0) {
                            $i = 1;
                            foreach ($sms_settings as $sms) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $sms['sender_id'] ?></td>
                            <td style="line-height: 18px"><?php echo $sms['api_url'] ?></td>
                            <td style="line-height: 18px"><?php echo $sms['user'] ?></td>
                            <td style="line-height: 18px">*****<?php //echo $sms['pass'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
                                <a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_sms_settings(<?php echo $sms['id'] ?>);">
                                    <i class="btn-icon-only icon-pencil"> </i>
                                </a>
                                <!--<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_hourly_price(<?php echo $sms['id']  ?>);"><i class="btn-icon-only icon-remove"> </i></a>-->
                            </td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
  
</div>