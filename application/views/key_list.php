<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Key</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="new-key-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Key Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="key" id="key" autocomplete="off" placeholder="MM-K-001">
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="n-btn blue-btn mb-0" id="generateBtn">Generate Key</button>
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="key_sub">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Key</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="edit-key-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Key Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_key" id="edit_key" autocomplete="off">
              <input type="hidden" name="edit_keyid" id="edit_keyid">
            </div>
          </div>


        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="key_edit">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Key</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this key?</h3>
      <input type="hidden" id="delete_keyid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>

<div id="view-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">View Key</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Key</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="view_key" id="view_key" readonly>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-success-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Deleted</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Key Deleted Successfully!</h3>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Key List</h3>

        <div class="topiconnew border-0 green-btn">
          <a data-action="new-key-popup" title="Add"> <i class="fa fa-plus"></i></a>
        </div>

      </div>
      <!-- /widget-header -->
      <div class="widget-content">

        <table id="zone-list-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
              <th style="line-height: 18px"> Key</th>
              <th style="line-height: 18px"> Status</th>
              <th style="line-height: 18px" class="td-actions">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($keys) > 0) {
              $i = 1;
              foreach ($keys as $keys_val) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;"><?php echo $i; ?></td>
                  <td style="line-height: 18px"><?php echo $keys_val['code'] ?></td>
                  <td style="line-height: 18px; width: 75px">
                    <?php
                    if ($keys_val['is_assigned'] == 1 && empty($keys_val['returned_at'])) {
                      $css_class = 'warning'; // Assigned
                      $status = 'Assigned';
                    } else if ($keys_val['is_assigned'] == 1 && !empty($keys_val['returned_at'])) {
                      $css_class = 'success'; // Not Assigned
                      $status = 'Not Assigned';
                    } else {
                      $css_class = 'success';
                      $status = 'Not Assigned';
                    }
                    ?>
                    <span class="btn badge-<?php echo $css_class; ?>"><?php echo $status; ?></span>
                  </td>

                  <td style="line-height: 18px;width: 100px;" class="td-actions">
                    <a href="javascript:;" class="n-btn-icon blue-btn" onclick="view_key_get(<?php echo $keys_val['id'] ?>);"><i class="btn-icon-only fa fa-eye"> </i></a>
                    <a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_key_get(<?php echo $keys_val['id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                    <?php if (user_authenticate() == 1) { ?>
                      <a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $keys_val['id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                    <?php } ?>
                  </td>
                </tr>
            <?php
                $i++;
              }
            }
            ?>

          </tbody>
        </table>

      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
  <div class="span6" id="edit_zone" style="display: none;">

  </div> <!-- /span6 -->

</div>
<script>
  /*********************************************************************************** */
  (function(a) {
    a(document).ready(function(b) {
      if (a('#zone-list-table').length > 0) {
        a("table#zone-list-table").dataTable({
          'sPaginationType': "full_numbers",
          "bSort": true,
          "scrollY": true,
          "orderMulti": false,
          'bFilter': true,
          "lengthChange": false,
          'columnDefs': [{
            'targets': [-1],
            'orderable': false
          }, ]
        });
      }
    });
  })(jQuery);

  function edit_key_get(id) {
    $("#edit-key-form").trigger("reset");
    edit_key_validator.resetForm();
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "key/edit_key",
      data: {
        id: id
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        //alert(result);
        var obj = jQuery.parseJSON(result);
        $.each($.parseJSON(result), function(edit, value) {
          $('#edit_keyid').val(value.id)
          $('#edit_key').val(value.code)

        });
        fancybox_show('edit-popup', {
          width: 450
        });
        $('.error-message').text('');
        $('.mm-loader').hide();
      },
      error: function(data) {
        $('.mm-loader').hide();
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  function closeFancy() {
    $.fancybox.close();
  }

  function confirm_delete_modal(id) {
    $('#delete_keyid').val(id);
    fancybox_show('delete-popup', {
      width: 450
    });
  }


  function confirm_deletee() {
    $.ajax({
      type: "POST",
      url: _base_url + "key/remove_key",
      data: {
        key_id: $('#delete_keyid').val()
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        fancybox_show('delete-success-popup', {
          width: 450
        });
        location.reload();
      },
      error: function(data) {
        alert(data.statusText);
        console.log(data);
      }
    });
  }
  $('[data-action="new-key-popup"]').click(function(event) {
    $("#new-key-form").trigger("reset");
    new_key_validator.resetForm();
    fancybox_show('new-popup');
    $('.error-message').text('');

  });
  /*********************************************************************************** */

  $('#generateBtn').click(function() {
    $.ajax({
      url: '<?php echo base_url("key/get_last_code"); ?>',
      type: 'GET',
      dataType: 'json',
      success: function(response) {
        if (response.success) {
          // Increment the code value and fill the input box
          var lastCode = response.code;
          // var numericPart = parseInt(lastCode.split('-')[2]);
          // var newNumericPart = numericPart + 1;
          // var paddedNumericPart = newNumericPart.toString().padStart(3, '0');
          // var newCode = 'MM-K-' + paddedNumericPart;
          $('#key').val(lastCode);
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError);
      }
    });
  });



  /*********************************************************************************** */

  function view_key_get(id) {
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "key/edit_key",
      data: {
        id: id
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        //alert(result);
        var obj = jQuery.parseJSON(result);
        $.each($.parseJSON(result), function(edit, value) {
          $('#view_key').val(value.code)

        });
        fancybox_show('view-popup', {
          width: 450
        });
        $('.mm-loader').hide();
      },
      error: function(data) {
        $('.mm-loader').hide();
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  /*********************************************************************************** */
  $(document).ready(function() {
    new_key_validator = $('#new-key-form').validate({
      rules: {
        key: {
          required: true
        },

      },
      messages: {
        key: {
          required: 'Please enter the Key code.'
        },
      },
      submitHandler: function(form) {
        // Code to execute when the form is valid and submitted
        // form.submit();
        var key = $('#key').val();


        $.ajax({
          url: '<?php echo base_url("key/check_key_code"); ?>',
          type: "POST",
          dataType: "json",
          data: {
            key: key
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Key already exists!');

            } else {
              $.ajax({
                url: '<?php echo base_url("key/save_key"); ?>',
                type: "POST",
                dataType: "json",
                data: {
                  key: key
                },
                success: function(saveResponse) {
                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving key.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking key name.');
          }
        });





      }
    });
  });
  /*********************************************************************************** */
  $(document).ready(function() {
    edit_key_validator = $('#edit-key-form').validate({
      rules: {
        edit_key: {
          required: true
        },
      },
      messages: {
        edit_key: {
          required: 'Please enter the key code'
        },
      },
      submitHandler: function(form) {
        // Code to execute when the form is valid and submitted
        // form.submit();

        var edit_key = $('#edit_key').val();
        var edit_keyid = $('#edit_keyid').val();


        $.ajax({
          url: '<?php echo base_url("key/check_key_code_edit"); ?>',
          type: "POST",
          dataType: "json",
          data: {
            key: edit_key,
            edit_keyid: edit_keyid
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Key already exists!');

            } else {
              $.ajax({
                url: '<?php echo base_url("key/edit_key_new"); ?>',
                type: "POST",
                dataType: "json",
                data: {
                  edit_key: edit_key,
                  edit_keyid: edit_keyid
                },
                success: function(saveResponse) {

                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving key.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking key name.');
          }
        });




      }
    });
  });
  toastr.options = {
    positionClass: "toast-bottom-full-width"
  };

  function confirm_delete() {
    var keyId = $('#delete_keyid').val();

    // Check if 'returned_by_user' and 'returned_at' are null
    $.ajax({
      type: "POST",
      url: _base_url + "key/check_returned",
      data: {
        key_id: keyId
      },
      dataType: "json",
      cache: false,
      success: function(response) {
        if (response.status == 'null') {
          toast("error", "Can't delete this key.The key is already in use");
        } else {
          $.ajax({
            type: "POST",
            url: _base_url + "key/remove_key",
            data: {
              key_id: keyId
            },
            dataType: "text",
            cache: false,
            success: function(result) {
              fancybox_show('delete-success-popup', {
                width: 450
              });
              location.reload();
            },
            error: function(data) {
              alert(data.statusText);
              console.log(data);
            }
          });
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
  }
</script>