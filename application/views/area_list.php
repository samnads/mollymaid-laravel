<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }
</style>
<div id="delete-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Delete ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to delete this area ?</h3>
            <input type="hidden" id="delete_areaid">
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
        </div>
    </div>
</div>
<div id="disable-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Disable ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to disable this area ?</h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
        </div>
    </div>
</div>
<div id="disable-all-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Disable All ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to disable all areas ?</h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable_all()">Disable All</button>
        </div>
    </div>
</div>
<div id="enable-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Enable ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to enable this area ?</h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
        </div>
    </div>
</div>
<div id="enable-all-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Enable All ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to enable all areas ?</h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn mb-0" onclick="confirm_enable_all()">Enable All</button>
        </div>
    </div>
</div>
<div id="new-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">New Area</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div id="popup-booking" class="col-12 p-0">
            <form class="form-horizontal new-area-form" method="post" id="add-new-zone-form">
                <div class="modal-body">
                    <div class="row m-0 n-field-main">
                        <div class="col-12">
                            <div class="error-message" style="color:red;"></div>
                        </div>
                        <p>Area Name</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="areaname" id="areaname" autocomplete="off">
                        </div>
                    </div>
                    <div class="row m-0 n-field-main">
                        <p>Zone Name</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <select name="zone_id" id="zone_id" class="sel2" style="width:100%">
                                <option value="">-- Select Zone --</option> <?php
                                                                            if (count($zones) > 0) {
                                                                                foreach ($zones as $zones_val) {
                                                                            ?> <option value="<?php echo $zones_val['zone_id']; ?>"> <?php echo $zones_val['zone_name']; ?> </option> <?php
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                                        ?>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="row m-0 n-field-main">
                        <p>Minimum Hour</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input class="no-arrows" type="number" name="new_booking_hour" autocomplete="off" id="new_booking_hour">
                        </div>
                    </div> -->
                    <div class="row m-0 n-field-main">
                        <p>Description</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="description" autocomplete="off" id="description">
                        </div>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="submit" class="n-btn m-0" value="Submit" name="area_sub">Save</button>
        </div>
        </form>
    </div>
</div>
<div id="edit-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Edit Area</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div id="popup-booking" class="col-12 p-0">
            <form class="form-horizontal edit-area-form" method="post" id="add-new-zone-form">
                <div class="modal-body">
                    <div class="row m-0 n-field-main">
                        <div class="col-12">
                            <div class="error-message" style="color:red;"></div>
                        </div>
                        <p>Area Name</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="edit_areaname" id="edit_areaname" autocomplete="off">
                            <input type="hidden" class="span3" id="edit_areaid" name="edit_areaid">
                        </div>
                    </div>
                    <div class="row m-0 n-field-main">
                        <p>Zone Name</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <select name="edit_zone_id" id="edit_zone_id" class="sel2" style="width:100%">
                                <option value="">-- Select Zone --</option> <?php
                                                                            if (count($zones) > 0) {
                                                                                foreach ($zones as $zones_val) {
                                                                            ?> <option value="<?php echo $zones_val['zone_id']; ?>"> <?php echo $zones_val['zone_name']; ?> </option> <?php
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                                        ?>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="row m-0 n-field-main">
                        <p>Minimum Hour</p>
                        <div class="col-sm-12 p-0 n-field-box ">
                            <input class="no-arrows" type="number" name="edit_booking_hour" id="edit_booking_hour" class="edit_booking_hour" autocomplete="off">
                        </div>
                    </div> -->
                    <div class="row m-0 n-field-main">
                        <p>Description</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="edit_description" id="edit_description" autocomplete="off">
                        </div>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="submit" class="n-btn mb-0" value="Submit" id="area_edit" name="area_edit">Save</button>
        </div>
        </form>
    </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
                <ul>
                    <li><i class="icon-th-list"></i>
                        <h3>Area List</h3>
                    </li>

                    <li><select style="width:160px;" id="all_location" onchange="activefilter(this.value)">
                            <option value="" <?php echo empty($status) ? 'selected' : ''; ?>>Select</option>
                            <option value="0" <?php echo $status == '0' ? 'selected' : ''; ?>>Active</option>
                            <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Inactive</option>
                        </select></li>

                    <li>

                    </li>

                    <li>

                    </li>
                    <li class="mr-0 float-right">

                        <button type="button" class="n-btn mb-0 mr-3" onclick="confirm_disable_enable_all('enable')">Enable All Area</button>

                        <button type="button" class="n-btn purple-btn mb-0 mr-3" onclick="confirm_disable_enable_all('disable')">Block All Area</button>

                        <div class="topiconnew border-0 green-btn">
                            <a onclick="newPopup()" title="Add"> <i class="fa fa-plus"></i></a>
                        </div>

                    </li>

            </div>
            <style>
                table.da-table tr td {
                    padding: 0px 6px;
                }
            </style>
            <!-- /widget-header -->
            <div class="widget-content">
                <table id="area-list-table" class="table table-hover da-table" cellspacing="0" width="100%">
                    <!--                <table class="table table-striped table-bordered">-->
                    <thead>
                        <tr>
                            <th style="width: 45px;  text-align:center;">Sl. No.</th>
                            <th style="">Area </th>
                            <th style="">Zone</th>
                            <!-- <th style="">Minimum Hour</th> -->
                            <th style="width:80px">Web Status</th>
                            <th style="width: 120px" class="td-actions">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($areas) > 0) {
                            $i = 1;
                            foreach ($areas as $areas_val) {
                        ?>
                                <tr>
                                    <td class="text-center" style=""><?php echo $i; ?></td>
                                    <td style="">
                                        <?php echo $areas_val['area_name'] ?>
                                    </td>
                                    <td style=""><?php echo $areas_val['zone_name'] ?></td>
                                    <!-- <td style=""><?php echo $areas_val['min_booking_hour'] ?></td> -->
                                    <!-- <td style="">
                                        <?php echo $areas_val['driver_name'] ?>
                                    </td> -->
                                    <td>
                                        <?php if ($areas_val['web_status'] == 1) : ?>
                                            <span class="btn-block badge badge-success">Active</span>
                                        <?php elseif ($areas_val['web_status'] == 0) : ?>
                                            <span class="btn btn-block label label-danger badge">Inactive</span>
                                        <?php endif; ?>
                                    </td>
                                    <td style="width:180px" class="td-actions">
                                        <a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_area_get(<?php echo $areas_val['area_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                                        <?php if (user_authenticate() == 1) { ?>
                                            <a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $areas_val['area_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                                        <?php } ?>
                                        <!-- <a href="javascript:void(0)"  style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);border-radius: 15px;" title="Disable Web Status" onclick="disable_webstatus(this, <?php echo $areas_val['area_id'] ?>, <?php echo $areas_val['web_status'] ?>);"><?php echo $areas_val['web_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?></a> -->
                                        <a href="javascript:void(0)" class="n-btn-icon green-btn" style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);" title="" onclick="confirm_disable_enable_modal(<?php echo $areas_val['area_id'] ?>,<?php echo $areas_val['web_status'] ?>);">
                                            <?php echo $areas_val['web_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?>
                                        </a>
                                        <!-- <?php if ($areas_val['web_status'] == 1) {
                                                ?>
                                                                                                <a href="javascript:;" class="btn btn-success btn-small" onclick="areaweb_status(<?php echo $areas_val['area_id'] ?>,<?php echo $areas_val['web_status'] ?>);"><i class="btn-icon-only icon-ok"> </i></a>
                                                                                        <?php
                                                                                    } else {
                                                                                        ?>
                                                                                                <a href="javascript:;" class="btn btn-danger btn-small" onclick="areaweb_status(<?php echo $areas_val['area_id'] ?>,<?php echo $areas_val['web_status'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                                                                                            <?php
                                                                                        }
                                                                                            ?> -->

                                    </td>

                                </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>

                    </tbody>
                </table>
            </div>
            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div><!-- /span5 -->
</div>
<script>
    /*********************************************************************************** */
    function reloadPage() {
        //$.fancybox.close();
        window.location.reload();
    }
    /*********************************************************************************** */
    function disable_webstatus($this, area_id, web_status) {
        // alert(area_id);
        // alert(web_status); die;
        var _lblstatus = web_status == 1 ? 'disable' : 'enable';
        if (confirm('Are you sure you want to ' + _lblstatus + ' the Web status')) {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/remove_area_click",
                data: {
                    area_id: area_id,
                    web_status: web_status
                },
                dataType: "text",
                cache: false,
                success: function(result) {
                    //window.location.assign(_base_url + 'customers');
                    window.location.reload();
                    if (result == 1) {
                        $($this).attr('class', 'btn btn-success btn-small');
                        $($this).html('<i class="btn-icon-only icon-ok"></i>');
                    } else {
                        if (result == 'exist_bookings') {
                            alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                            result = 0;
                        } else {
                            $($this).attr('class', 'btn btn-danger btn-small');
                            $($this).html('<i class="btn-icon-only icon-remove"> </i>');
                        }
                    }
                    $($this).attr('onclick', 'delete_customer(this, ' + area_id + ', ' + result + ')');
                }
            });
        }
    }
    var disable_areaid = null;
    var web_status = null;
    /*********************************************************************************** */
    // (function(a) {
    //     a(document).ready(function(b) {
    //         if (a('#area-list-table').length > 0) {
    //             a("table#area-list-table").dataTable({
    //                 'sPaginationType': "full_numbers",
    //                 "bSort": true,
    //                 "scrollY": true,
    //                 "orderMulti": false,
    //                 'bFilter': true,
    //                 "lengthChange": true,
    //                 "pageLength": 100,
    //                 'columnDefs': [{
    //                     'targets': [-1],
    //                     'orderable': false
    //                 }, ]
    //             });
    //         }
    //     });
    // })(jQuery);
    var dataTable;

    $(document).ready(function() {
        dataTable = $('#area-list-table').DataTable({
            'sPaginationType': "full_numbers",
            'bSort': true,
            'scrollY': true,
            'orderMulti': false,
            'bFilter': true,
            'lengthChange': true,
            'pageLength': 100,
            'columnDefs': [{
                'targets': [5],
                'orderable': false
            }],
            'language': {
                'zeroRecords': 'No location data found'
            }
        });

        $('#all_location').on('change', function() {
            var status = $(this).val();
            activefilter(status);
        });
    });

    function activefilter(value) {
        if (value === "") {
            $("tbody tr").show();
        } else {
            $("tbody tr").hide();

            $("tbody tr").each(function() {
                var status = $(this).find("i.fa-toggle-on").length > 0 ? "0" : "1";
                if (status === value) {
                    $(this).show();
                }
            });
        }
    }



    /*********************************************************************************** */
    function confirm_delete_modal(area_id) {
        $('#delete_areaid').val(area_id);
        fancybox_show('delete-popup', {
            width: 450
        });
    }
    /*********************************************************************************** */
    function confirm_disable_enable_modal(area_id, status) {
        if (status == 0) {
            fancybox_show('enable-popup', {
                width: 450
            });
        } else {

            fancybox_show('disable-popup', {
                width: 450
            });
        }
        disable_areaid = area_id;
        web_status = status;
    }

    function confirm_disable_enable_all(action) {
        if (action == 'enable') {
            fancybox_show('enable-all-popup', {
                width: 450
            });
        } else {
            fancybox_show('disable-all-popup', {
                width: 450
            });
        }
    }

    function confirm_disable_all() {
        $.ajax({
            url: _base_url + 'settings/change_status/',
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                reloadPage();
            }
        });
    }

    function confirm_enable_all() {
        $.ajax({
            url: _base_url + 'settings/change_status_enable/',
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                reloadPage();
            }
        });
    }
    /*********************************************************************************** */
    function confirm_disable() {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/areaweb_status",
            data: {
                area_id: disable_areaid,
                web_status: web_status
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                window.location.assign(_base_url + 'areas');
            }
        });
    }
    /*********************************************************************************** */
    function confirm_enable() {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/areaweb_status",
            data: {
                area_id: disable_areaid,
                web_status: web_status
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                window.location.assign(_base_url + 'areas');
            }
        });
    }
    /*********************************************************************************** */
    function confirm_delete() {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_area",
            data: {
                area_id: $('#delete_areaid').val()
            },
            dataType: "text",
            cache: false,
            success: function(data) {
                location.reload();
            },
            error: function(data) {
                alert(data.statusText);
                console.log(data);
            }
        });
    }
    /*********************************************************************************** */
    function edit_area_get(area_id) {
        $('.mm-loader').show();
        $.ajax({
            type: "POST",
            url: _base_url + "settings/edit_area",
            data: {
                area_id: area_id
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                //alert(result);
                var obj = jQuery.parseJSON(result);
                $.each($.parseJSON(result), function(edit, value) {
                    // alert(value.zone_id);
                    $('#edit_areaid').val(value.area_id)
                    $('#edit_areaname').val(value.area_name)
                    $('#edit_zone_id option[value="' + value.zone_id + '"]').prop('selected', true);
                    $('#edit_zone_id').trigger('change');
                    $('#edit_description').val(value.description);
                    // $('.edit_booking_hour').val(value.min_booking_hour)
                });
                fancybox_show('edit-popup', {
                    width: 450
                });
                $('.error-message').text('');
                $('.mm-loader').hide();
            },
            error: function(data) {
                $('.mm-loader').hide();
                alert(data.statusText);
                console.log(data);
            }
        });
    }
    /*********************************************************************************** */
    function closeFancy() {
        $.fancybox.close();
    }
    /*********************************************************************************** */
    function newPopup() {
        fancybox_show('new-popup', {
            width: 450
        });
        $('.new-area-form')[0].reset();
        $('.error-message').text('');
    }

    // $("#edit_zone_id").on('change', function () {
    //     zoneId = $(this).val();
    //     areaId = $('#edit_areaid').val();
    //     $.ajax({
    //         url: _base_url + 'get_zone_area',
    //         type: 'POST',
    //         data: { zoneId: zoneId, areaId: areaId },
    //         dataType: 'json',
    //         success: function (data) {
    //             if (data.status == 'success') {
    //                 alert('success', data.message);
    //                 $('#area_edit').attr("disabled", true);

    //             }
    //             else {

    //                 // $('#form-error').html(data.message);
    //                 // $('#customer-save-btn').html('Saving...').attr("disabled", true);
    //                 // $('.mm-loader').hide();
    //             }
    //         },
    //     });
    // })

    /*********************************************************************************** */
    $(document).ready(function() {
        $('.new-area-form').validate({
            rules: {
                areaname: {
                    required: true
                },
                zone_id: {
                    required: true
                },

                description: {
                    required: true
                },
                new_booking_hour: {
                    number: true
                },

            },
            messages: {
                areaname: {
                    required: 'Please enter area name'
                },
                zone_id: {
                    required: 'Please select zone name'
                },
                description: {
                    required: 'Please enter the description'
                },
                new_booking_hour: {
                    number: 'Please enter a valid numeric value between 1 to 12'
                },
            },

            submitHandler: function(form) {
                // Code to execute when the form is valid and submitted
                //form.submit();
                var areaname = $('#areaname').val();
                var zone_id = $('#zone_id').val();
                var description = $('#description').val();
                var new_booking_hour = $('#new_booking_hour').val();

                // Check if the zone name already exists
                $.ajax({
                    url: "<?php echo base_url('settings/check_area_name'); ?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        area_name: areaname
                    },
                    success: function(response) {
                        if (response.exists) {
                            $('.error-message').text('Area name already exists!');

                        } else {
                            // If the zone name is new, save the zone using AJAX
                            $.ajax({
                                url: "<?php echo base_url('settings/save_area'); ?>",
                                type: "POST",
                                dataType: "json",
                                data: {
                                    areaname: areaname,
                                    zone_id: zone_id,
                                    description: description,
                                    new_booking_hour: new_booking_hour
                                },
                                success: function(saveResponse) {

                                    $.fancybox.close();
                                    location.reload();
                                },
                                error: function() {
                                    alert('Error saving area.');
                                }
                            });
                        }
                    },
                    error: function() {
                        alert('Error checking area name.');
                    }
                });








            }
        });
    });


    /*********************************************************************************** */
    $(document).ready(function() {
        $('.edit-area-form').validate({
            rules: {
                edit_areaname: {
                    required: true
                },

                edit_zone_id: {
                    required: true
                },
                // edit_booking_hour: {
                //     number: true
                // },

                edit_description: {
                    required: true
                }

            },
            messages: {
                edit_areaname: {
                    required: 'Please enter area name'
                },

                edit_zone_id: {
                    required: 'Please select zone name'
                },

                edit_description: {
                    required: 'Please enter the description'
                },
                // edit_booking_hour: {
                //     number: 'Please enter a valid numeric value between 1 to 12'
                // }

            },
            submitHandler: function(form) {
                // Code to execute when the form is valid and submitted
                // form.submit();

                var edit_areaname = $('#edit_areaname').val();
                var edit_areaid = $('#edit_areaid').val();
                var edit_description = $('#edit_description').val();
                var edit_zone_id = $('#edit_zone_id').val();
                // var edit_booking_hour = $('#edit_booking_hour').val();
                // alert(edit_booking_hour);
                // Check if the zone name already exists
                $.ajax({
                    url: "<?php echo base_url('settings/check_area_name_edit'); ?>",
                    type: "POST",
                    dataType: "json",
                    data: {
                        area_name: edit_areaname,
                        area_id: edit_areaid
                    },
                    success: function(response) {
                        if (response.exists) {
                            $('.error-message').text('Area name already exists!');
                        } else {
                            $.ajax({
                                url: "<?php echo base_url('settings/get_zone_area'); ?>",
                                type: "POST",
                                dataType: "json",
                                data: {
                                    edit_areaid: edit_areaid,
                                    edit_zone_id: edit_zone_id,
                                },
                                success: function(response1) {
                                    if (response1.status == 'success') {
                                        $('.error-message').text(response1.message);
                                    } else {
                                        // If the zone name is new, save the zone using AJAX

                                        $.ajax({
                                            url: "<?php echo base_url('settings/edit_area_new'); ?>",
                                            type: "POST",
                                            dataType: "json",
                                            data: {
                                                edit_areaname: edit_areaname,
                                                edit_areaid: edit_areaid,
                                                edit_zone_id: edit_zone_id,
                                                edit_description: edit_description
                                                // edit_booking_hour: edit_booking_hour
                                            },
                                            success: function(saveResponse) {

                                                $.fancybox.close();
                                                location.reload();
                                            },
                                            error: function() {
                                                alert('Error saving area.');
                                            }
                                        });
                                    }

                                },
                                error: function() {
                                    alert('Error');
                                }
                            });

                        }
                    },
                    error: function() {
                        alert('Error checking area name.');
                    }
                });









            }
        });

    });


    /*********************************************************************************** */
</script>