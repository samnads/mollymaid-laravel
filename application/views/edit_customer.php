<script src="https://maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&amp;libraries=places"></script>
<!-- this is replaced from PM -->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
<style>
    #edit-profile label, input, button, select, textarea{font-size: 12px !important;}
    .widget-content{min-height : 375px;}

</style>  
<script>
 var latlng=[];
</script>  
<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<div id="crop-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Crop Profile Image</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="image" src="#">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
        <button type="button" class="n-btn mb-0" id="crop">Crop</button>
      </div>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">      		
        <div class="widget ">
            <div class="widget-header">
            <ul>
            <li>
                <i class="icon-user"></i>
                <h3>Edit Customer</h3></li>
                <li class="mr-0 float-right">
                <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>customers" title="Customer List"> <i class="fa fa-users"></i></a> </div></li>
                </ul>
                
                
                
            </div> <!-- /widget-header -->
                    <div class="widget-content">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                                 <li><a href="#address-details" data-toggle="tab" onclick="resize_map();">Address Details</a></li>
                                <li><a href="#account" data-toggle="tab">Account and Other Details</a></li>
                                
                                <!-- <li><a href="#more" data-toggle="tab">More Details</a></li>-->
                            </ul>
                            <br>
                            <form id="edit-profile" class="form-horizontal" method="post" novalidate>      
                                <div class="tab-content">
                                    <div class="tab-pane active" id="personal">
                                        <!--   <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                        
                                        <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>                                                         
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Customer Details Updated Successfully.
                                                </div>
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                        
                                        
                                        <fieldset>   
                                        <div class="row m-0">         
                                            <div class="col-sm-4">        
                                                
                                                  <fieldset>
                                                  <div class="row n-field-main m-0">
                                                    <p>Customer</p>
                                                    <div class="n-field-box">
                                                      <input type="text" name="customer_name" class=" " id="customer_name" value="<?php echo $customer_details[0]['customer_name'] ?>" required  onchang="document.getElementById('customer_nick').value = this.value">
                                                      <input type="hidden" class=" " id="customer_id" name="customer_id" value="<?php echo $customer_details[0]['customer_id'] ?>">
                                                      <input type="hidden" id="quickbookid" name="quickbookid" value="<?php echo $customer_details[0]['quickbook_id'] ?>">
                                                      <input type="hidden" id="quickbooksyncid" name="quickbooksyncid" value="<?php echo $customer_details[0]['quickbookTokenId'] ?>">
                                                    </div>
                                                  </div>
  
  
                                                  <div class="row n-field-main m-0">
                                                    <p>Customer Nick Name</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="customer_nick" name="customer_nick" value="<?php echo $customer_details[0]['customer_nick_name'] ?>" required>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Is Flagged</p>
                                                    <div class="col-sm-6 n-field-box p-0">
                                                        <div class="n-end n-pick-maids-set">
                                                             <input id="flag" name="flag" type="radio" value="Y" class="" <?php echo $customer_details[0]['is_flag'] == 'Y' ?  'checked="checked"' : ''; ?>>
                                                             <label for="flag"> <span class="border-radius-3"></span><p>Yes</p></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 n-field-box p-0">
                                                        <div class="n-end n-pick-maids-set">
                                                             <input id="flag1" name="flag" type="radio" value="N" class="" <?php echo $customer_details[0]['is_flag'] == 'N' ?  'checked="checked"' : ''; ?>>
                                                             <label for="flag1"> <span class="border-radius-3"></span><p>No</p></label>
                                                        </div>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Reason For Flagging</p>
                                                    <div class="n-field-box">
                                                      <textarea class=" " rows="1" id="reasonflag" name="reasonflag"><?php echo $customer_details[0]['flag_reason'] ?></textarea>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Mobile Number 1</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="mobile_number1" name="mobile_number1" value="<?php echo $customer_details[0]['mobile_number_1'] ?>" required>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Mobile Number 2</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="mobile_number2" name="mobile_number2" value="<?php echo $customer_details[0]['mobile_number_2'] ?>">
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Mobile Number 3</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="mobile_number3" name="mobile_number3" value="<?php echo $customer_details[0]['mobile_number_3'] ?>">
                                                    </div>
                                                  </div>

                                                  <div class="row m-0 n-field-main">
                                                        <p>WhatsApp No.</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                              <input type="number" class="" id="whatsapp_no_1" name="whatsapp_no_1" value="<?php echo $customer_details[0]['whatsapp_no_1'] ?>">
                                                        </div>
                                                    </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Phone</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="phone" name="phone" value="<?php echo $customer_details[0]['phone_number'] ?>">
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Fax</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="fax" name="fax" value="<?php echo $customer_details[0]['fax_number'] ?>">
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>TRN#</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="trnnumber" name="trnnumber" value="<?php echo $customer_details[0]['trnnumber'] ?>">
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>VAT#</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="vatnumber" name="vatnumber" value="<?php echo $customer_details[0]['vatnumber'] ?>">
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Area</p>
                                                    <div class="n-field-box">
                                                      <select name="area_name" id="area" class=" " required >
                                                        <?php
                                                                  if (count($areas) > 0) {
                                                                      foreach ($areas as $areasVal) {
                                                                          ?>
                                                          <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                                                          <?php
                                                                      }
                                                                  }
                                                                  ?>
                                                      </select>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <p>Source</p>
                                                    <div class="n-field-box">
                                                      <select name="customer_source" id="customer_source" required>
                                                        <option value="">-Select-</option>
                                                        <option value="Live In" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Live In" ? 'selected="selected"' : '') : '' ?>>Live In</option>
                                                        <option value="Live Out" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Live Out" ? 'selected="selected"' : '') : '' ?>>Live Out</option>
                                                        <option value="Rizek" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Rizek" ? 'selected="selected"' : '') : '' ?>>Rizek</option>
                                                        <option value="Justmop" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Justmop" ? 'selected="selected"' : '') : '' ?>>Justmop</option>
                                                        <option value="Matic" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Matic" ? 'selected="selected"' : '') : '' ?>>Matic</option>
                                                        <option value="ServiceMarket" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "ServiceMarket" ? 'selected="selected"' : '') : '' ?>>ServiceMarket</option>
                                                        <option value="Helpling" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Helpling" ? 'selected="selected"' : '') : '' ?>>Helpling</option>
                                                        <option value="Urban Clap" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Urban Clap" ? 'selected="selected"' : '') : '' ?>>Urban Clap</option>
                                                        <option value="Emaar" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Emaar" ? 'selected="selected"' : '') : '' ?>>Emaar</option>
                                                        <option value="MyHome" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "MyHome" ? 'selected="selected"' : '') : '' ?>>MyHome</option>
                                                        <option value="Facebook" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Facebook" ? 'selected="selected"' : '') : '' ?>>Facebook</option>
                                                        <option value="Google" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Google" ? 'selected="selected"' : '') : '' ?>>Google</option>
                                                        <option value="Yahoo" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Yahoo" ? 'selected="selected"' : '') : '' ?>>Yahoo</option>
                                                        <option value="Bing" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Bing" ? 'selected="selected"' : '') : '' ?>>Bing</option>
                                                        <option value="Direct Call" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Direct Call" ? 'selected="selected"' : '') : '' ?>>Direct Call</option>
                                                        <option value="Flyers" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Flyers" ? 'selected="selected"' : '') : '' ?>>Flyers</option>
                                                        <option value="Referral" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Referral" ? 'selected="selected"' : '') : '' ?>>Referral</option>
                                                        <option value="Watchman/Security Guard" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Watchman/Security Guard" ? 'selected="selected"' : '') : '' ?>>Watchman/Security Guard</option>
                                                        <option value="Maid" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Maid" ? 'selected="selected"' : '') : '' ?>>Maid</option>
                                                        <option value="Driver" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Driver" ? 'selected="selected"' : '') : '' ?>>Driver</option>
                                                        <option value="By email" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "By email" ? 'selected="selected"' : '') : '' ?>>By email</option>
                                                        <option value="Schedule visit" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Schedule visit" ? 'selected="selected"' : '') : '' ?>>Schedule visit</option>
                                                        <option value="Website" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Website" ? 'selected="selected"' : '') : '' ?>>Website</option>
                                                        <option value="Referred by staff" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Referred by staff" ? 'selected="selected"' : '') : '' ?>>Referred by staff</option>
                                                        <option value="Referred by customer" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Referred by customer" ? 'selected="selected"' : '') : '' ?>>Referred by customer</option>
                                                        <option value="Tekram" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Tekram" ? 'selected="selected"' : '') : '' ?>>Tekram</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0">
                                                    <?php $style = $customer_details[0]['customer_source'] == "Maid" ? 'style="display:block;"' : 'style="display:none;"'?>
                                                    <p>Source Reference</p>
                                                    <div class="n-field-box">
                                                      <select name="customer_source_id" id="customer_source_id" class=" " >
                                                        <option value="">-- Select Maids --</option>
                                                        <?php
                                                                                                                          if (!empty($maids) > 0) {
                                                                                                                              foreach ($maids as $maid) { $selected = $maid['maid_id'] == $customer_details[0]['customer_source_id'] ? 'selected="selected"' : '';
                                                                                                                              ?>
                                                        <option value="<?php echo $maid['maid_id']; ?>" <?php echo $selected ?>><?php echo $maid['maid_name']; ?></option>
                                                        <?php
                                                                                                                              }
                                                                                                                          }
                                                                                                                          ?>
                                                      </select>
                                                    </div>
                                                  </div>
                                                  
                                                  
                                                  <div class="row n-field-main m-0"  id="customer_source_others" <?php echo $customer_details[0]['customer_source'] != 'Others' ? 'style="display: none;"' : ''; ?>>
                                                    <p>Source Reference</p>
                                                    <div class="n-field-box">
                                                      <input type="text" class=" " id="customer_source_others_val" name="customer_source_others_val" value="<?php echo $customer_details[0]['customer_source_val'] ?>">
                                                    </div>
                                                  </div>
                                                  
                                                  </fieldset>

                                                    
                                            </div> 


<div class="col-sm-1 p-0"></div>
                                            <div class="col-sm-4">

                                                        
                                                        
                                                            
                                                            
                                                        <div class="row n-field-main m-0">
                                                          <p>Customer Type</p>
                                                          <div class="n-field-box">
                                                               <select name="customer_type" class=" " required >
                                                                        <option value="HO" <?php echo isset($customer_details[0]['customer_type']) ? ($customer_details[0]['customer_type'] == "HO" ? 'selected="selected"' : '') : '' ?>>Home</option>
                                                                        <option value="OF" <?php echo isset($customer_details[0]['customer_type']) ? ($customer_details[0]['customer_type'] == "OF" ? 'selected="selected"' : '') : '' ?>>Office</option>
                                                                        <option value="WH" <?php echo isset($customer_details[0]['customer_type']) ? ($customer_details[0]['customer_type'] == "WH" ? 'selected="selected"' : '') : '' ?>>Warehouse</option>
                                                                    </select>
                                                          </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="row n-field-main m-0">
                                                          <p>Is Company</p>
                                                          
                                                          
                                                    
                                                    <div class="col-sm-6 n-field-box p-0">
                                                        <div class="n-end n-pick-maids-set">
                                                             <input id="company" name="company" type="radio" value="Y" class="" <?php echo $customer_details[0]['is_company'] == 'Y' ?  'checked="checked"' : ''; ?>>
                                                             <label for="company"> <span class="border-radius-3"></span><p>Yes</p></label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-6 n-field-box p-0">
                                                        <div class="n-end n-pick-maids-set">
                                                             <input id="company1" name="company" type="radio" value="N" class="" <?php echo $customer_details[0]['is_company'] == 'N' ?  'checked="checked"' : ''; ?>>
                                                             <label for="company1"> <span class="border-radius-3"></span><p>No</p></label>
                                                        </div>
                                                    </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        <div class="row n-field-main m-0" id="company_source" <?php echo $customer_details[0]['is_company'] == 'N' ? 'style="display: none;"' : ''; ?>>
                                                          <p>Select Company</p>
                                                          <div class="n-field-box">
                                                               <select name="company_name" id="company_name" class=" ">
                                                                    <option value="">-Select-</option>
                                                                    <option value="Live In" <?php echo $customer_details[0]['company_name'] == "Live In" ? 'selected="selected"' : ''; ?>>Live In</option>
                                                                    <option value="Live Out" <?php echo $customer_details[0]['company_name'] == "Live Out" ? 'selected="selected"' : ''; ?>>Live Out</option>
                                                                    <option value="Rizek" <?php echo $customer_details[0]['company_name'] == "Rizek" ? 'selected="selected"' : ''; ?>>Rizek</option>
                                                                    <option value="Justmop" <?php echo $customer_details[0]['company_name'] == "Justmop" ? 'selected="selected"' : ''; ?>>Justmop</option>
																	<option value="Matic" <?php echo $customer_details[0]['company_name'] == "Matic" ? 'selected="selected"' : ''; ?>>Matic</option>
																	<option value="ServiceMarket" <?php echo $customer_details[0]['company_name'] == "ServiceMarket" ? 'selected="selected"' : ''; ?>>ServiceMarket</option>
																	<option value="Helpling" <?php echo $customer_details[0]['company_name'] == "Helpling" ? 'selected="selected"' : ''; ?>>Helpling</option>
																	<option value="Urban Clap" <?php echo $customer_details[0]['company_name'] == "Urban Clap" ? 'selected="selected"' : ''; ?>>Urban Clap</option>
																	<option value="Emaar" <?php echo $customer_details[0]['company_name'] == "Emaar" ? 'selected="selected"' : ''; ?>>Emaar</option>
																	<option value="MyHome" <?php echo $customer_details[0]['company_name'] == "MyHome" ? 'selected="selected"' : ''; ?>>MyHome</option>
																</select>
                                                          </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                   
                                                   
                                                        <div class="row n-field-main m-0">
                                                          <p>Contact Person</p>
                                                          <div class="n-field-box">
                                                               <input type="text" class=" " id="contact_person" name="contact_person" value="<?php echo $customer_details[0]['contact_person'] ?>" required="required">
                                                          </div>
                                                        </div>


                                                        <div class="row n-field-main m-0">
                                                          <p>Website URL</p>
                                                          <div class="n-field-box">
                                                               <input type="text" class=" " id="website" name="website" value="<?php echo $customer_details[0]['website_url'] ?>">
                                                          </div>
                                                        </div>

                                                
                                                
                                                        
                                                        



                                                        
                                                        
                                                        
                                                        <div class="row n-field-main m-0">
                                                          <p>Email Address</p>
                                                          <div class="n-field-box">
                                                               <input type="email" class=" " id="email" name="email" value="<?php echo $customer_details[0]['email_address'] ?>">
                                                          </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        <div class="row n-field-main m-0">
                                                          <p>User Name</p>
                                                          <div class="n-field-box">
                                                               <input type="text" class=" " id="user_name" name="user_name" value="<?php echo $customer_details[0]['customer_username'] ?>" required>
                                                          </div>
                                                        </div>


                                                        <div class="row n-field-main m-0">
                                                          <p>Password</p>
                                                          <div class="n-field-box">
                                                               <input type="text" class=" " id="password1" name="password" value="<?php echo $customer_details[0]['customer_password'] ?>" required>
                                                          </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        
                                         
                                         
                                                            
                                                        <div class="row n-field-main m-0">
                                                          <p>Notes</p>
                                                          <div class="n-field-box">
                                                               <textarea class=" " rows="5" id="notes" name="notes"><?php echo $customer_details[0]['customer_notes'] ?></textarea>	
                                                          </div>
                                                        </div>


                                            </div>
                                            
                                            
                                            
                                            
                                            </div>

                                        </fieldset>            
                                    </div>
                                    
                                    <div class="tab-pane" id="address-details">
                                <fieldset>            
                                    <div class="col-sm-4">
                                    
                                         <?php
      if (count($customer_address) > 0) {
          $i=1;
          foreach ($customer_address as $customer) {
              ?>
                                    
                                         <div class="row n-field-main m-0">
                                              <p><label class="" for="basicinput">Area  <a onclick="remove_address(<?php echo $customer['customer_address_id']?>,<?php echo $customer['customer_id']?>)" class="float-right red-btn n-btn-sml ">Remove</a></label></p>
                                              <div class="n-field-box">
                                                   <select name="area[]" class=" " style="min-width: 270px;" required >
        <option value="" selected>Select Area</option>
        <?php
              if (count($areas) > 0) {
                 
                  foreach ($areas as $areasVal) {
                      if ($areasVal['area_id'] == $customer['area_id'])
                          $selected = "selected";
                      else
                          $selected = "";
                      ?>
        <option value="<?php echo $areasVal['area_id'] ?>" <?php echo $selected; ?>><?php echo $areasVal['area_name'] ?></option>
        <?php
                }
            }
            ?>
      </select>
                                              </div>
                                         </div>
                                         
                                         
                                         
                                         <div class="row n-field-main m-0">
                                              <p>Apartment No</p>
                                              <div class="n-field-box">
                                                   <input id="" type="text" name="apartment_no[]" value="<?php echo $customer['building'] ?>" class=" " style="min-width: 270px;"/>
                                              </div>
                                         </div>
                                         
                                         
                                         
                                         <div class="row n-field-main m-0">
                                              <p>Address</p>
                                              <div class="n-field-box">
                                                   <input id="address_<?php echo $customer['customer_address_id']?>" type="text" name="address[]" value="<?php echo $customer['customer_address'] ?>" onkeyup="loadLocationField('<?php echo "address_".$customer['customer_address_id']?>','<?php echo "m".$customer['customer_address_id'] ?>');" class=" " style="min-width: 270px;"/>
      
      
      <input type="hidden" name="address_id[]" value="<?php echo $customer['customer_address_id'] ?>"/>
      <input type="hidden" name="lat[]" id="latitude_m<?php echo $customer['customer_address_id'] ?>" value="<?php echo $customer['latitude'] ?>"/>
      <input type="hidden" name="lng[]" id="longitude_m<?php echo $customer['customer_address_id'] ?>" value="<?php echo $customer['longitude'] ?>"/>
                                              </div>
                                         </div>
                                         
                                         
                                         
                                         
                                         <div id="map_canvas_<?php echo "m".$customer['customer_address_id'] ?>" style="height: 200px;" ></div>
  
  
  <script>
    var map<?php echo $i ?>;
    
    var geocoder = new google.maps.Geocoder();
  
    var address = "<?php echo str_replace(' ', '', $customer['customer_address']); ?>";
    <?php if($customer['latitude']!="" && $customer['longitude']!="" ) {?>
      locationRio =  {lat:<?php echo $customer['latitude'] ?>, lng:<?php echo $customer['longitude'] ?>};
      latlng['<?php echo $i ?>']=locationRio;   
        map<?php echo $i ?> = new google.maps.Map(document.getElementById("map_canvas_m<?php echo $customer['customer_address_id']?>"), {
        zoom: 14,
        center:locationRio,
      });
               
    var marker = new google.maps.Marker({
    map: "map_canvas_m<?php echo $customer['customer_address_id']?>",
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: locationRio,
  });
  marker.setMap(map<?php echo $i ?>);
        
    <?php } else {?>    
    geocoder.geocode( { 'address': address}, function(results, status) {
        
        if (status == google.maps.GeocoderStatus.OK) {
          locationRio =  results[0].geometry.location
          latlng['<?php echo $i ?>']=locationRio;
          //var locationRio = {lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()};
            //var locationRio={lat:8.5581,lng:76.8816};
    map<?php echo $i ?> = new google.maps.Map(document.getElementById("map_canvas_m<?php echo $customer['customer_address_id']?>"), {
    zoom: 14,
    center:locationRio,
  });
    
  var marker = new google.maps.Marker({
    map: "map_canvas_m<?php echo $customer['customer_address_id']?>",
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: locationRio,
  });
  marker.setMap(map<?php echo $i ?>);
  //map.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng()))
   } 
        }); 
       
    
  <?php } ?>
    
  </script> 
  

<?php
          $i++;
      }
      
   }
   ?>
                   <div class="input_fields_wrap"></div>                      
                                         
                                         
                                         
                                         <div class="row n-field-main m-0 pt-3">
                                              <p class="text-right"><a class="add_field_button n-btn n-btn-sml">Add More Address</a></p>
                                         </div>
                                         
                                    </div> 
                                 </fieldset>
                              </div>

                                    <div class="tab-pane" id="account">
                                    
                                    <div class="row m-0">

                                        <fieldset>            
                                            <div class="col-sm-4" style="margin-left:0px;">
                                               
                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Customer Book Type</p>
                                                                  <div class="n-field-box">
                                                                       <select name="customers_type" id="customers_type" class=" " required >
                                                                        <option value="0" <?php echo isset($customer_details[0]['customer_booktype']) ? ($customer_details[0]['customer_booktype'] == "0" ? 'selected="selected"' : '') : '' ?>>Non Regular</option>
                                                                        <option value="1" <?php echo isset($customer_details[0]['customer_booktype']) ? ($customer_details[0]['customer_booktype'] == "1" ? 'selected="selected"' : '') : '' ?>>Regular</option>
                                                                    </select>
                                                                  </div>
                                                            </div>
                                         
                                         
                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Payment Type</p>
                                                                  <div class="n-field-box">
                                                                       <select name="payment_type" id="payment_type" class=" " required >
                                                                        <option value="D" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "D" ? 'selected="selected"' : '') : '' ?>>Daily Paying</option>
                                                                        <option value="W" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "W" ? 'selected="selected"' : '') : '' ?>>Weekly Paying</option>
                                                                        <option value="M" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "M" ? 'selected="selected"' : '') : '' ?>>Monthly Paying</option>
                                                                        <option value="MA" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "MA" ? 'selected="selected"' : '') : '' ?>>Monthly Advance</option>
                                                                    </select>
                                                                  </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Payment Mode</p>
                                                                  <div class="n-field-box">
                                                                       <select name="payment_mode" id="payment_mode" class=" " required >
																		<option value="">-Select-</option>
                                                                        <option value="Cash" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Cash" ? 'selected="selected"' : '') : '' ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Cheque" ? 'selected="selected"' : '') : '' ?>>Cheque</option>
                                                                        <option value="Online" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Online" ? 'selected="selected"' : '') : '' ?>>Online</option>
                                                                        <option value="Credit Card" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Credit Card" ? 'selected="selected"' : '') : '' ?>>Credit Card</option>
                                                                    </select>
                                                                  </div>
                                                            </div>
                                                            

                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Hourly</p>
                                                                  <div class="n-field-box">
                                                                       <input type="text" class=" " id="hourly" name="hourly" value="<?php echo $customer_details[0]['price_hourly'] ?>"> 
                                                                  </div>
                                                            </div>
                                                            

                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Extra</p>
                                                                  <div class="n-field-box">
                                                                       <input type="text" class=" " id="extra" name="extra" value="<?php echo $customer_details[0]['price_extra'] ?>">
                                                                  </div>
                                                            </div>


                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Weekend</p>
                                                                  <div class="n-field-box">
                                                                       <input type="text" class=" " id="weekend" name="weekend" value="<?php echo $customer_details[0]['price_weekend'] ?>">
                                                                  </div>
                                                            </div>
                                                            

                                                  
                                                            

                                            </div>
                                            
                                            <div class="col-sm-1 p-0"></div>


                                            <div class="col-sm-4">
                                            
                                            
                                                <div class="row n-field-main m-0">
                                                      <p>Latitude</p>
                                                      <div class="n-field-box">
                                                           <input type="text" class=" " id="latitude" name="latitude" value="<?php echo $customer_details[0]['latitude'] ?>">
                                                      </div>
                                                </div>
                                                           <div class="row n-field-main m-0">
                                                                  <p>Longitude</p>
                                                                  <div class="n-field-box">
                                                                       <input type="text" class=" " id="longitude" name="longitude" value="<?php echo $customer_details[0]['longitude'] ?>">
                                                                  </div>
                                                            </div>
														<?php
														if($customer_details[0]['initial_balance'] > 0)
														{
															$disable = 'readonly';
															$sele = 'disabled';
														} else {
															$disable = '';
															$sele = '';
														}
														?>
                                                            <div class="row m-0 n-field-main">
                                                        <p>Opening Balance</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" <?php echo $disable; ?> class="" id="initial_balance" name="initial_balance" value="<?php echo $customer_details[0]['initial_balance']; ?>">
                                                             <input type="hidden" id="initial_balance_hidden" name="initial_balance_hidden" value="<?php echo $customer_details[0]['initial_balance']; ?>">
                                                             <input type="hidden" id="totinvoiceamt" name="totinvoiceamt" value="<?php echo $customer_details[0]['total_invoice_amount']; ?>">
                                                             <input type="hidden" id="totbal" name="totbal" value="<?php echo $customer_details[0]['balance']; ?>">
                                                        </div>
                                                    </div>
													
													<div class="row m-0 n-field-main">
                                                        <p>Balance Till</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" <?php echo $disable; ?> class="" id="vehicle_date" name="initial_balance_date" value="<?= $customer_details[0]['initial_bal_date'] != "" ? DateTime::createFromFormat('Y-m-d', $customer_details[0]['initial_bal_date'])->format('d/m/Y') : ''; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="row m-0 n-field-main">
                                                        <p>Opening Balance Type</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <select name="initial_bal_sign" <?php echo $disable; ?> id="initial_bal_sign" class="">
                                                                <option value="Cr" <?= $customer_details[0]['initial_bal_sign'] == 'Cr' ? 'selected' : $sele ?>>Credit</option>
                                                                <option value="Dr" <?= $customer_details[0]['initial_bal_sign'] == 'Dr' ? 'selected' : $sele ?>>Debit</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                            
                                                            
                                                            
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Key</p>
                                                                  
                                                                  <div class="col-sm-6 n-field-box p-0">
                                                                      <div class="n-end n-pick-maids-set">
                                                                           <input id="key" name="key" type="radio" value="Y" class="" <?php echo $customer_details[0]['key_given'] == 'Y' ?  'checked="checked"' : ''; ?>>
                                                                           <label for="key"> <span class="border-radius-3"></span><p>Yes</p></label>
                                                                      </div>
                                                                  </div>
                                                                  
                                                                  <div class="col-sm-6 n-field-box p-0">
                                                                      <div class="n-end n-pick-maids-set">
                                                                           <input id="key1" name="key" type="radio" value="N" class="" <?php echo $customer_details[0]['key_given'] == 'N' ?  'checked="checked"' : ''; ?>>
                                                                           <label for="key1"> <span class="border-radius-3"></span><p>No</p></label>
                                                                      </div>
                                                                  </div>
                                                    
                                                                  
                                                            </div>


                                                          
                                                          

                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Rating Mail</p>
                                                                  
                                                                  <div class="col-sm-6 n-field-box p-0">
                                                                      <div class="n-end n-pick-maids-set">
                                                                           <input id="rating_mail_stat" name="rating_mail_stat" type="radio" value="Y" class="" <?php echo $customer_details[0]['rating_mail'] == 'Y' ?  'checked="checked"' : ''; ?>>
                                                                           <label for="rating_mail_stat"> <span class="border-radius-3"></span><p>Yes</p></label>
                                                                      </div>
                                                                  </div>
                                                                  
                                                                  <div class="col-sm-6 n-field-box p-0">
                                                                      <div class="n-end n-pick-maids-set">
                                                                           <input id="rating_mail_stat1" name="rating_mail_stat" type="radio" value="N" class="" <?php echo $customer_details[0]['rating_mail'] == 'N' ?  'checked="checked"' : ''; ?>>
                                                                           <label for="rating_mail_stat1"> <span class="border-radius-3"></span><p>No</p></label>
                                                                      </div>
                                                                  </div>
                                                                  
                                                                  
                                                                  
                                                            </div>
                                                            
                                                            
                                                            
                                                            


                                                            
                                                            
                                                            <!--<div class="row n-field-main m-0">
                                                                  <p>Current Photo</p>
                                                                  <div class="n-field-box">
                                                                       <span style=" cursor:pointer;"> 
                                                                        <?php
                                                                        if ($customer_details[0]['customer_photo_file'] == "") {
                                                                            $image = base_url() . "img/no_image.jpg";
                                                                        } else {
                                                                            $image = base_url() . "customer_img/" . $customer_details[0]['customer_photo_file'];
                                                                        }
                                                                        ?>

                                                                        <img src="<?php echo $image; ?>" style="height: 100px; width: 100px"/> 
                                                                        <input type="hidden" name="old_image" id="old_image" value="<?php echo $customer_details[0]['customer_photo_file'] ?>"/>
                                                                    </span> 
                                                                  </div>
                                                            </div>
                                                            
                                                            
                                                           
                                                           
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                  <p>Change Photo</p>
                                                                  <div class="n-field-box">
                                                                       <div id="me" class="styleall" style=" cursor:pointer;"> 
                                                                        <span style=" cursor:pointer;">                             
                                                                            <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> 
                                                                        </span> 
                                                                    </div>
                                                                  </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main pt-3">
                                                                <p>Upload Photo</p>
                                    <label>
                                        <img id="avatar_img" src="<?php echo base_url($customer_details[0]['customer_photo_file'] ? CUSTOMER_AVATAR_PATH . $customer_details[0]['customer_photo_file'] : DEFAULT_AVATAR); ?>" style="width:100px;">
                                            <input type="file" class="sr-only" id="input-image" accept="image/*">
                                    </label>
                                    <input type="hidden" name="avatar_base64" id="avatar_base64">
                                    </div>
                                                            
                                                            


                                                            
                                                            
                                                           <!-- 
                                                            <div class="row n-field-main m-0">
                                                                  <p></p>
                                                                  <div class="n-field-box">
                                                                       
                                                                  </div>
                                                            </div>
                                                    -->
                                                    
                                         

                                            </div>
                                        </fieldset>  
                                        
                                        </div>          

                                        <div class="col-sm-12">
                                        <div class="form-actions pl-0">
                                            <input type="hidden" name="call_method" id="call_method" value="customer/editcustomerimgupload"/>
                                            <input type="hidden" name="img_fold" id="img_fold" value="customer_img"/>
                                            <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            <input type="submit" class="n-btn" value="Update" name="customer_edit" onclick="return validate_customer();">
                                        </div> 
                                        </div>  
                                    </div>
                                </div>	
                            </form>
                        </div>
                    </div> <!-- /widget-content -->	
       
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
<script>
var status = '<?php echo $quickbookmessage["status"]; ?>';
var messagess = '<?php echo $quickbookmessage["message"]; ?>';
if(status !="")
{
	if(status =="success")
	{
		toastr.success(messagess)
	} else {
		toastr.error(messagess)
	}
}

// toast('error',messagess);
function resize_map(){
//alert(locationRio);
var i;
setTimeout(function(){window.dispatchEvent(new Event('resize'));

    if("1" in latlng){map1.setCenter(latlng[1]);}
    if("2" in latlng){map2.setCenter(latlng[2]);}
    if("3" in latlng){map3.setCenter(latlng[3]);}
    if("4" in latlng){map4.setCenter(latlng[4]);}
    if("5" in latlng){map5.setCenter(latlng[5]);}
    if("6" in latlng){map6.setCenter(latlng[6]);}
    if("7" in latlng){map7.setCenter(latlng[7]);}
    if("8" in latlng){map8.setCenter(latlng[8]);}
    if("9" in latlng){map9.setCenter(latlng[9]);}
    if("10" in latlng){map10.setCenter(latlng[10]);}
    //}
}, 1000);

}
var cropper;
function showCropper() {
	cropper = new Cropper(image, {
		aspectRatio: 300 / 300,
		viewMode: 1,
	});
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#crop-popup'),
	});
}

function closeCropper() {
	cropper.destroy();
	cropper = null;
	$.fancybox.close();
}
window.addEventListener('DOMContentLoaded', function() {
	var avatar_img = document.getElementById('avatar_img');
	var image = document.getElementById('image');
	var input = document.getElementById('input-image');
	input.addEventListener('change', function(e) {
		var files = e.target.files;
		var done = function(url) {
			input.value = '';
			image.src = url;
			showCropper();
		};
		var reader;
		var file;
		var url;
		if (files && files.length > 0) {
			file = files[0];
			if (URL) {
				done(URL.createObjectURL(file));
			} else if (FileReader) {
				reader = new FileReader();
				reader.onload = function(e) {
					done(reader.result);
				};
				reader.readAsDataURL(file);
			}
		}
	});
	document.getElementById('crop').addEventListener('click', function() {
		var initialpromo_imgURL;
		var canvas;
		if (cropper) {
			canvas = cropper.getCroppedCanvas({
				width: 300,
				height: 300,
			});
			initialpromo_imgURL = avatar_img.src;
			avatar_img.src = canvas.toDataURL();
			$('#avatar_base64').val(avatar_img.src);
		}
		closeCropper();
	});
});
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('customers'); ?>') {
			$this.addClass('active');
		}
	})
})
</script>    