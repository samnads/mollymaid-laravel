<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }
  /* table.da-table tr td {
    padding: 0px 6px;
  } */
</style>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this package ?</h3>
      <input type="hidden" id="disable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this package ?</h3>
      <input type="hidden" id="enable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div id="new-vehicle-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="new-or-edit">New Vehicle</span>
      <span id=""></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="new-vehicle-form">
        <input type="hidden" name="vehicle_id" id="vehicle_id"/>
        <div class="modal-body">
          <div class="form-row">
            <div id="" class="col-md-4">
              <div class="row m-0 n-field-main">
                <p>Licence Plate Number<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="licence_number" autocomplete="off"/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Brand<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="brand_name" autocomplete="off"/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Model<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="model_name" autocomplete="off"/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Color<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="color" autocomplete="off"/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Model Year<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <select name="model_year" id="model_year" class="sel2" data-placeholder="-- Select year --" style="width:100%">
                  <?php for($year=date('Y');$year>=1960;$year--): ?>
                    <option value="">-- Select year --</option>
                    <option value="<?= $year ?>"><?= $year ?></option>
                  <?php endfor; ?>
                </select>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Engine No.<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="engine_number" autocomplete="off"/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Chassis No.<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="chassis_number" autocomplete="off"/>
                </div>
              </div>
            </div>
            <div id="" class="col-md-4">
              <div class="row m-0 n-field-main">
                <p>Fitness From<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="fitness_start_date" placeholder="dd/mm/yyyy" readonly/>
                </select>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Fitness Valid Upto<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                   <input type="text" name="fitness_expiry_date" placeholder="dd/mm/yyyy" readonly/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Horse Power<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <label class=" position-relative">
                    <div class="n-right-position">HP</div>
                    <input type="number" step="any" name="horsepower" class="no-arrows" autocomplete="off"/>
                  </label>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Fuel Type<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <select name="fuel_type" id="fuel_type" class="sel2" data-placeholder="-- Select fuel type --" style="width:100%">
                    <option value="">-- Select fuel type --</option>
                    <option value="Electric">Electric</option>
                    <option value="Petrol">Petrol</option>
                    <option value="Diesel">Diesel</option>
                    <option value="Gas">Gas</option>
                </select>
                </div>
              </div>
            </div>
            <div id="" class="col-md-4">
              <div class="row m-0 n-field-main">
                <p>Insurance No.<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="insurance_number" autocomplete="off"/>
                </select>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Insurance From<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                   <input type="text" name="insurance_start_date" placeholder="dd/mm/yyyy" readonly/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Insurance Valid Upto<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="insurance_expiry_date" placeholder="dd/mm/yyyy" readonly/>
                </div>
              </div>
              <div class="row m-0 n-field-main">
                <p>Insurance Type<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="insurance_type" autocomplete="off"/>
                </div>
              </div>
              <!--<div class="row m-0 n-field-main">
                <p>Insurance File Upload</p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" name="insurance_file_name" />
                </div>
              </div>-->
              <div class="row m-0 n-field-main">
                <p>Insurance Notes<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <textarea type="text" name="insurance_note" id="insurance_note"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-12">
            <p class="text-danger" id="new-vehicle-form-error"></p>
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" id="vehicle-save-btn">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="driver-assign-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Assign Driver</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="driver-assign-form">
        <input name="assign_vehicle_id" id="assign_vehicle_id" type="hidden"/>
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Vehicle</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select data-placeholder="Select customer" style="width:100%" readonly>
              <option class="licence_number"></option>
            </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Driver</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select style="width:100%" name="employee_id" id="employee_id" data-placeholder="-- Select driver --" readonly>
              </select>
            </div>
          </div>
		  <div class="row m-0 n-field-main">
            <p>Notes</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea type="text" name="driver_assign_note" id="driver_assign_note"></textarea>
            </div>
          </div>
          <p class="text-danger" id="driver-assign-error">&nbsp;</p>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" id="driver-assign-btn">Assign</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Vehicles</h3></li>

        <li><select style="width:160px;" id="all-maids" onchange="statusFilter(this.value)">
          <option value="" <?php echo $active == null ? 'selected' : ''; ?>>All</option>
          <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Active</option>
          <option value="0" <?php echo $status === '0' ? 'selected' : ''; ?>>Returned</option>
        </select></li>

        <li class="mr-0 float-right">

        <div class="topiconnew border-0 green-btn">
        	<a href="#" title="New" data-action="new-vehicle"> <i class="fa fa-plus"></i></a>
        </div>

        </li>
        </ul>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="vehicle-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th style="width:45px">
                <center>Sl. No.</center>
              </th>
              <th>
                Licence Number
              </th>
              <th>
                Driver
              </th>
              <th>
                Insurance Expiry
              </th>
              <th>
                Fitness Expiry
              </th>
              <th>
                Brand
              </th>
              <th>
               Model
              </th>
              <th>
               Make Year
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($rows as $key => $row): ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo $row->licence_number ?></td>
                  <td><?php echo $row->driver_name ?: "-" ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d', $row->insurance_expiry_date)->format('d M Y') ?></td>
                  <td><?php echo DateTime::createFromFormat('Y-m-d', $row->fitness_expiry_date)->format('d M Y') ?></td>
                  <td><?php echo $row->brand_name ?></td>
                  <td><?php echo $row->model_name ?: "-" ?></td>
                  <td><?php echo $row->model_year ?: "-"; ?></td>
                  <td style="line-height: 18px; width: 158px" class="td-actions">
                  	<center>
                      <a class="n-btn-icon btn-success" href="#" title="Add Driver" data-action="assign-driver" data-id="<?= $row->vehicle_id ?>"><i class="btn-icon-only fa fa-user"> </i></a>
                      <a class="n-btn-icon blue-btn" href="#" title="View Vehicle" data-action="view-vehicle" data-id="<?= $row->vehicle_id ?>"><i class="btn-icon-only fa fa-eye"> </i></a>
                      <a class="n-btn-icon purple-btn" href="#" title="Edit Vehicle" data-action="edit-vehicle" data-id="<?= $row->vehicle_id ?>"><i class="btn-icon-only icon-pencil"> </i></a>
                      <a href="#" class="n-btn-icon green-btn" title="Disbale" onclick="#"><i class="btn-icon-only fa fa-toggle-on"> </i></a>
                    </center>
                  </td>
                </tr>
              <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->