<?php
//        echo '<pre>';
//        print_r($reports);
//        exit();
?>
<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">  
    <div class="col-md-12">
    
    
        <div class="widget widget-table action-table" style="margin-bottom:30px">
        
        <div class="widget-header">
        <form id="edit-profile" class="form-horizontal" method="post" >
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Activity Summary Report</h3></li>
              
              <li>
                  <span style="margin-left:23px;">From :</span>
                    <input type="text" style="width: 160px;" id="ActFromDate" name="from_date" value="<?php echo isset($from_date) ? $from_date : date('d/m/Y'); ?>">
              </li>
      
      
      
              <li class="mr-2">
                  <span style="margin-left:23px;">To :</span>
                    <input type="text" style="width: 160px;" id="ActToDate" name="to_date" value="<?php echo isset($to_date) ? $to_date : date('d/m/Y'); ?>">
              </li>
      
      
      
              <li>
                  <input type="submit" class="n-btn" value="Go" name="activity_report">
              </li>
      
      
      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                    	<a href="#" id="ActivityPrint" title="Print"> <i class="fa fa-print"></i></a>
                    </div>
                    
             
             
                    
                    <div class="topiconnew border-0 green-btn">
                    	<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>
              </li>
      
      
      
           
              <div class="clear"></div>
            </ul>
     </div>
     </form>
</div>


     
            
            

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Date</th>
                            <th style="line-height: 18px">Total Booking Hours(s)</th>
                            <th style="line-height: 18px" colspan="2">One Day Booking(s)</th>
                            <th style="line-height: 18px" colspan="2">Long Term Booking(s)</th>
                            <th style="line-height: 18px">Total Payment</th>
                            <th style="line-height: 18px">Total Invoice</th>
                            <th style="line-height: 8px">View</th>
                        </tr>
                        <tr>
                            <th style="line-height: 18px;"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px">Booking(s)</th>
                            <th style="line-height: 18px">Booking Hrs</th>
                            <th style="line-height: 18px">Booking(s)</th>
                            <th style="line-height: 18px">Booking Hrs</th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 8px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total_hrs = 0;
                        $total_OD = 0;
                        $total_OD_hrs = 0;
                        $total_WE = 0;
                        $total_WE_hrs = 0;
                        $total_payment = 0;
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $key => $report) {
                                $newDate = date("d/m/Y", strtotime($key));
                                $total_hrs += $report['hours'];
                                $total_OD += $report['OD'];
                                $total_OD_hrs += $report['OD_hrs'];
                                $total_WE += $report['WE'];
                                $total_WE_hrs += $report['WE_hrs'];
                                $total_payment += $report['payment'];
                                
                                $i++;
                                ?>
                                <tr>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $newDate; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['hours']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['OD']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['OD_hrs']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['WE']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['WE_hrs']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['payment']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">

                                    </td>
                                    <td style="line-height: 8px;text-align: center;" class="td-actions ">
                                    <center>
                                        <a class="n-btn-icon green-btn" target="_blank" href="<?php echo base_url() . 'reports/activity_summary_view/'. $key ?>" ><i class="btn-icon-only icon-search"> </i></a>
                                    </center>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                                <tr>
                        <td style="line-height: 18px;text-align: center;"></td>
                        <td style="line-height: 18px;text-align: center;"><b>Total</b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_hrs; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_OD; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_OD_hrs; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_WE; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_WE_hrs; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"><b><?php echo $total_payment; ?></b></td>
                        <td style="line-height: 18px;text-align: center;"> </td>
                        <td style="line-height: 18px;text-align: center;"> </td>
                                    
                    </tr>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="ActivityReportPrint"> 
    <table cellpadding="0" border="1" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl No</th>
                <th style="line-height: 18px">Date</th>
                <th style="line-height: 18px">Total Booking Hours(s)</th>
                <th style="line-height: 18px" colspan="2">One Day Booking(s)</th>
                <th style="line-height: 18px" colspan="2">Long Term Booking(s)</th>
                <th style="line-height: 18px">Total Payment</th>
                <th style="line-height: 18px">Total Invoice</th>
            </tr>
            <tr>
                <th style="line-height: 18px;"></th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px">Booking(s)</th>
                <th style="line-height: 18px">Booking Hrs</th>
                <th style="line-height: 18px">Booking(s)</th>
                <th style="line-height: 18px">Booking Hrs</th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total_hrs = 0;
            $total_OD = 0;
            $total_OD_hrs = 0;
            $total_WE = 0;
            $total_WE_hrs = 0;
            $total_payment = 0;
            
            if (!empty($reports)) {
                $i = 0;                            
                
                
                foreach ($reports as $key => $report) {
                    $newDate = date("d/m/Y", strtotime($key));
                    $total_hrs += $report['hours'];
                    $total_OD += $report['OD'];
                    $total_OD_hrs += $report['OD_hrs'];
                    $total_WE += $report['WE'];
                    $total_WE_hrs += $report['WE_hrs'];
                    $total_payment += $report['payment'];
                    
                    $i++;
                    ?>
                    <tr>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $newDate; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['hours']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['OD']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['OD_hrs']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['WE']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['WE_hrs']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['payment']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">

                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
                    <tr>
                        <td style="line-height: 18px;text-align: center;"></td>
                        <td style="line-height: 18px;text-align: center;">Total</td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_hrs; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_OD; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_OD_hrs; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_WE; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_WE_hrs; ?></td>
                        <td style="line-height: 18px;text-align: center;"><?php echo $total_payment; ?></td>
                    </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("ActivityReportPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "ActivitySummaryReport.xls"); // Choose the file name
      return false;
  }  
</script>