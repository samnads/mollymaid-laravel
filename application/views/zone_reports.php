<style>
#divToPrint p {
	margin:0px;
	padding:0px;	
}
.ptable td{
	padding:0px;
	margin:0px;	
}
.topiconnew{
    cursor: pointer;
}

table.da-table tr.even {
  background-color: #FFF;
}

.table > tbody > tr > td {border-top: 1px solid #FFF;}
table.da-table tbody tr:hover, table.da-table tbody tr:hover td { background: #f6f6f6;}

table.da-table tr td {
  border-right: 1px solid #fff;}
  
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row m-0">  
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                	<ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Zone Report from Booking</h3>  
                    </li>  
                    <li>               
                    <?php
                    //print_r($search);
                    //echo "DATE ". $search['search_date'] . "<br />";
                    if ($search['search_date'] == "") {
                        ?>
                        <input type="text" style="width:160px;" id="zone_date" name="zone_date" value="<?php echo date('d/m/Y') ?>">

                        <?Php
                    } else {
                        ?>
                        <input type="text" style="width:160px;" id="zone_date" name="zone_date" value="<?php echo $search['search_date'] ?>">
                        <?Php
                    }
                    ?>
                    
                    </li>  
                    <li>

                    <span style="margin-left:23px;color: #FFF;">Zone :</span>

                    <?php
                    if ($search['search_zone'] == "") {
                        ?>

                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>

                        <?Php
                    } else {
                        ?>
                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>" <?php echo isset($search['search_zone']) ? ($search['search_zone'] == $zones_val['zone_id'] ? 'selected="selected"' : '') : '' ?> ><?php echo $zones_val['zone_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?Php
                    }
                    ?>
                    
                    </li>  
                    <li>


                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day" value="<?php echo $search['search_day'] ?>">
                    <input type="hidden" name="zone_name" id="zone_name" value="<?php echo $search['search_zone_name'] ?>">
                    
                    </li>  
                    <li>
                    <input type="submit" class="n-btn" value="Go" name="zone_report">
                    
                    </li>  
                    
                    <li class="mr-0 float-right">
                   <div class="topiconnew border-0 green-btn"> <a onclick="exportF(this)" target="_blank" title="Download to Excel"> <i class="fa fa-file-excel-o"></i></a> </div>
                   <div class="topiconnew border-0 green-btn"> <a href="" id="printButton" target="_blank"><i class="fa fa-print"></i></a> </div>
                   
                   </li>
                   </ul>
                   
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <div style="width: 100%; /*width:1180px;*/">
<!--                <table id="da-datatable" class="table da-table" cellspacing="0" width="100%">-->
<!--                <div class="da-panel collapsible scrollable">-->
                <!--<table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    
                    
                    
                    <thead>
                        <tr>
                            <th style="line-height: 30px;"> <center>Employee</center></th>
                            <th style="line-height: 30px"> <center>08am-10am</center></th>
                            <th style="line-height: 30px"> <center>10am-12pm</center></th>
                            <th style="line-height: 30px"> <center>12pm-02pm</center></th>
                            <th style="line-height: 30px"> <center>02pm-04pm</center></th>
                            <th style="line-height: 30px"> <center>04pm-06pm</center></th>
                            <th style="line-height: 30px"> <center>Total [Hrs]</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo "<pre>";
                        //print_r($zone_report);

						$tot = 0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {

                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
                                    ?>
                                    <tr>
                                        <td><?php echo $maids_val['maid_name'] ?> </td>
                                        <td>
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("08:00:00" >= $time_from && "08:00:00" < $time_to) || ("10:00:00" > $time_from && "10:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("10:00:00" > $time_from && "10:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                
                                                                if("08:00:00" < $time_from)
                                                                {      
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("08:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("08:00:00" < $time_to && '10:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("08:00:00" < $time_to && '10:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                                
                                                            }                                                            
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                            
                                                        }
                                                        else if("08:00:00" < $time_from && '10:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("08:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("10:00:00" >= $time_to)
                                                        {    
                                                            if($time_from > "08:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("08:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #fd8e41; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td>

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("10:00:00" >= $time_from && "10:00:00" < $time_to) || ("12:00:00" > $time_from && "12:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("12:00:00" > $time_from && "12:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("10:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("10:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("10:00:00" < $time_to && '12:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("10:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("10:00:00" < $time_to && '12:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }
                                                        else if("10:00:00" < $time_from && '12:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("10:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("12:00:00" >= $time_to)
                                                        {   
                                                            if($time_from > "10:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("10:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #fd8e41; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>


                                        </td>

                                        <td>


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("12:00:00" >= $time_from && "12:00:00" < $time_to) || ("14:00:00" > $time_from && "14:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("14:00:00" > $time_from && "14:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("12:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("12:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("12:00:00" < $time_to && '14:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("12:00:00" < $time_to && '14:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }         
                                                        else if("12:00:00" < $time_from && '14:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("12:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("14:00:00" >= $time_to)
                                                        {          
                                                            if($time_from > "12:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("12:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #fd8e41; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>




                                        </td>

                                        <td>


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("14:00:00" >= $time_from && "14:00:00" < $time_to) || ("16:00:00" > $time_from && "16:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("16:00:00" > $time_from && "16:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("14:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("14:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("14:00:00" < $time_to && '16:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("14:00:00" < $time_to && '16:00:00' < $time_to)
                                                                {
                                                                    $time_display = '2 Hrs';
                                                                    $total_hrs_served += 2;
                                                                }
                                                                
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }
                                                        else if("14:00:00" < $time_from && '16:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("14:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("16:00:00" >= $time_to)
                                                        {    
                                                            if($time_from > "14:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("14:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #fd8e41; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>


                                        </td>


                                        <td>

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    if (("16:00:00" >= $time_from && "16:00:00" < $time_to) || ("18:00:00" > $time_from && "18:00:00" <= $time_to)) {
                                                        if($arr['time_diff'] > 2 && ("18:00:00" > $time_from && "18:00:00" < $time_to)){
                                                            if($arr['time_diff'] % 2 !== 0)
                                                            {
                                                                if("16:00:00" < $time_from)
                                                                {                                                           
                                                                    $time_display = ((strtotime($arr['time_from']) - strtotime("16:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("16:00:00" < $time_to && '18:00:00' > $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                                else if("16:00:00" < $time_to && '18:00:00' < $time_to)
                                                                {
                                                                    $time_display = ((strtotime($arr['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                                    $total_hrs_served += $time_display;
                                                                    $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $time_display = '2 Hrs';
                                                                $total_hrs_served += 2;
                                                            }
                                                        }
                                                        else if("16:00:00" < $time_from && '18:00:00' < $time_to)
                                                        {
                                                            $time_display = ((strtotime($arr['time_from']) - strtotime("16:00:00")) / 60) / 60;

                                                            $total_hrs_served += $time_display;
                                                            $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                        }
                                                        else if("18:00:00" >= $time_to)
                                                        {    
                                                            if($time_from > "16:00:00")
                                                            {                                                           
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime($arr['time_from'])) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                            else
                                                            {
                                                                $time_display = ((strtotime($arr['time_to']) - strtotime("16:00:00")) / 60) / 60;

                                                                $total_hrs_served += $time_display;
                                                                $time_display .= $time_display > 1 ? ' Hrs' : ' Hr';
                                                            }
                                                        }
                                                         
                                                        if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #fd8e41; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '</br>&nbsp; (' . $area . ')<br /><label style="background: #fd8e41; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>



                                        </td>                                     
                                        

                                        <td>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?>
                                        </td>
                                    </tr>

                                            <?php
                                        }
                                    }
                                } else {

                                    if (count($maids) != 0) {
                                        foreach ($maids as $maids_val) {
                                            ?>
                                    <tr>
                                        <td style="line-height: 18px; width: 200px">&nbsp;</td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"><span style="line-height: 18px; width: 200px"><?php echo $maids_val['maid_name'] ?></span></td>
                                        
                                    </tr>

            <?php
        }
    }
}
?>
					<tr>
                    	<td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td align="center"><strong><?php echo $tot; ?></strong></td>
                    </tr>


                    </tbody>
                    
                   
                    
                </table>-->
				
				
				
				
				<table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    
                    
                    
                    <thead>
                        <tr>
							<th style="line-height: 30px; width: 30px;"><center>No</center></th>
                            <th style="line-height: 30px; width: 200px;">Employee</th>
                            <th style="line-height: 30px; width: 300px;">Booking 1</th>
                            <th style="line-height: 30px; width: 300px;">Booking 2</th>
                            <th style="line-height: 30px; width: 300px;">Booking 3</th>
                            <th style="line-height: 30px">Booking 4</th>
                            <th style="line-height: 30px">Booking 5</th>
                            <th style="line-height: 30px">Total [Hrs]</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo "<pre>";
                        //print_r($zone_report);

						$tot = 0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {
								$displayed_bookids=array();
								$slno=1;
                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
                                    ?>
                                    <tr>
										<td bgcolor="#fff"><center><?php echo $slno;?></center></td>
                                        <td bgcolor="#f4f4f4"><?php echo $maids_val['maid_name'] ?> </td>
                                        <td>
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_od; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                            
                                                            
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_od.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_od.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_we; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_we.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_we.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }else{?>
                                                             <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_bw; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                             
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_bw.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_bw.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>
                                                        <?php }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td>

                                             <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_od; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_od.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_od.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_we; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_we.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_we.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else{?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_bw; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                               <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_bw.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_bw.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                           </p>
                                                       <?php }
                                                       
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>

											 <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_od; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_od.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_od.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_we; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_we.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_we.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else{?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_bw; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                               <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_bw.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_bw.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                           </p>
                                                       <?php }
                                                       
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>
                                        </td>

                                        <td>
												
											 <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_od; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_od.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_od.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_we; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_we.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_we.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }else{?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_bw; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                               <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_bw.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_bw.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                           </p>
                                                       <?php }
                                                       
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>
											
                                        </td>


                                        <td>
											
											 <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_od; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_od.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_od.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_we; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_we.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_we.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else{?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid <?= $settings->color_bg_booking_bw; ?>; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                               <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: '.$settings->color_bg_booking_bw.'; display: inline-block; float: right; color: '.$settings->color_bg_text_booking_bw.'; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                           </p>
                                                       <?php }
                                                       
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>

                                        </td>                                     
                                        

                                        <td><center>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?></center>
                                        </td>
                                    </tr>

                                            <?php
                                        $slno++;
										}
                                    }
                                } else {

                                    if (count($maids) != 0) {
                                        foreach ($maids as $maids_val) {
                                            ?>
                                    <tr>
                                        <td style="line-height: 18px; width: 200px">&nbsp;</td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"><span style="line-height: 18px; width: 200px"><?php echo $maids_val['maid_name'] ?></span></td>
                                        
                                    </tr>

            <?php
        }
    }
}
?>
					


                    </tbody>
                    
                   <tfoot>
				   <tr>
						<td></td>
                    	<td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><center><strong>Total</strong></center></td>
                        <td align="center"><center><strong><?php echo $tot; ?></strong></center></td>
                    </tr>
				   </tfoot>
                    
                </table>
				
				
				
				
                </div>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
                  
                    <thead>
                        <tr>
                        	<th> Sl No.</th>
                            <th> Employee</th>
                            <th> Booking 1</th>
                            <th> Booking 2</th>
                            <th> Booking 3</th>
                            <th> Booking 4</th>
                            <th> Booking 5</th>
                            <th> Total [Hrs]</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //echo "<pre>";
                        //print_r($zone_report);
						$tot = 0;
						$p=0;
                        if ($zone_report != NULL) {
                            if (count($maids) != 0) {
                                $displayed_bookids=array();
                                foreach ($maids as $maids_val) { $total_hrs_served = 0;
									$p++;
                                    ?>
                                    <tr>
                                    	<td><?php echo $p; ?></td>
                                        <td style="width: 200px"><?php echo $maids_val['maid_name'] ?> </td>
                                        <td style="padding:3px;">
                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #8793d6; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #7ac255; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #7ac255; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>

                                        </td>

                                        <td style="padding:3px;">

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #8793d6; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #7ac255; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #7ac255; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>


                                        </td>

                                        <td style="padding:3px;">


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #8793d6; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #7ac255; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #7ac255; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>




                                        </td>

                                        <td style="padding:3px;">


                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #8793d6; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #7ac255; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #7ac255; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>


                                        </td>


                                        <td style="padding:3px;">

                                            <?php
                                            $time_dif = 0;
                                            foreach ($zone_report as $arr) {
                                                if ($maids_val['maid_id'] == $arr['maid_id']) {
                                                    $booking_type = $arr['booking_type'];
                                                    $customer = $arr['customer'];
                                                    $customer_pay = $arr['customer_paytype'];
                                                    //Payment Type
                                                    if($customer_pay == "D")
                                                    {
                                                        $customer_paytype = "(D)";
                                                    } else if($customer_pay == "W")
                                                    {
                                                        $customer_paytype = "(W)";
                                                    } else if($customer_pay == "M")
                                                    {
                                                        $customer_paytype = "(M)";
                                                    } else
                                                    {
                                                        $customer_paytype = "";
                                                    }
                                                    //ends
                                                    $zone = $arr['zone'];
                                                    $area = $arr['area'];
                                                    $time_display = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
													$time_det = date('h:i a', strtotime($arr['time_from'])) . '-' . date('h:i a', strtotime($arr['time_to']));
                                                    $time_from = $arr['time_from'];
                                                    $time_to = $arr['time_to'];
                                                    $time_diff = $arr['time_diff'];
                                                    $class = $booking_type == 'OD' ? 'yell' : 'yell-long';
                                                    $time_dif += $time_diff;

                                                    $bookingid=$arr['booking_id'];
													if(!in_array($bookingid, $displayed_bookids))
													{
														$total_hrs_served+=$time_diff;
														$time_display = $time_diff > 1 ? $time_diff.' Hrs' : $time_diff.' Hr';
														if ($booking_type == "OD") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #8793d6; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #8793d6; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        } else if ($booking_type == "WE") {
                                                            ?>
                                                            <p style="background: #FFF; font-family: Arial, Helvetica, sans-serif; color: #555 !important; padding: 5px 6px 5px 10px  !important; border-radius: 5px; margin: 0px; border: solid #7ac255; border-width: 1px 1px 1px 7px;  box-shadow: 2px 2px 5px 0 rgba(0,0,0,0.1); text-align: left">
                                                                <?php echo $customer.' '.$customer_paytype . '</br>' . $zone . '&nbsp; (' . $area . ')<br /><b>'.$time_det.'</b> <label style="background: #7ac255; display: inline-block; float: right; color: #FFF; padding: 1px 6px; border-radius: 3px;">' . $time_display . '</label>' ?>
                                                            </p>

                                                            <?php
                                                        }
														
														array_push($displayed_bookids,$bookingid);
														break;
													}
												
                                                }
                                            }
                                            ?>



                                        </td>
                                        
                                        <td style="padding:3px;"><center>

                                            <?php
                                            if ($total_hrs_served != 0) {//if ($time_dif != 0) {

                                                echo $total_hrs_served;
												
                                                $tot += $total_hrs_served;
                                            } else {
                                                echo "";
                                            }
                                            ?></center>
                                        </td>


                                    </tr>

                                            <?php
                                        }
                                    }
                        }

//                                else {
//
//                                    if (count($maids) != 0) {
//                                        foreach ($maids as $maids_val) {
//                                            ?>
<!--                                    <tr>
                                        <td style="line-height: 18px; width: 200px"><?php //echo $maids_val['maid_name'] ?> </td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>
                                        <td style="line-height: 18px"></td>                                    
                                        <td style="line-height: 18px"></td>
                                    </tr>-->

            <?php
//        }
//    }
//}
?>



                    </tbody>
                    
                   <tr>
                    	<td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td align="center"><strong><?php echo $tot; ?></strong></td>
                    </tr>
                    
                </table>
    </div><!-- /widget-content --> 
</div>


<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "ZoneReport.xls"); // Choose the file name
      return false;
  }  
</script>