<script src="http://maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&amp;libraries=places"></script>
<section>
    <div class="row invoice-field-wrapper no-left-right-margin">
        <div class="widget-header">
        <ul>
        <li>
            <i class="icon-th-list"></i>
            <h3>View Invoice</h3>
            </li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 invoice-box-main  box-center no-left-right-padding">
            
            <!--<h2 class="text-center">Invoice Details</h2>-->
            <div class="col-md-8 col-sm-12 invoice-box-right-main no-left-padding">
                <div class="col-md-12 col-sm-12 invoice-box-right">
                    <div class="col-md-12 col-sm-12 invoice-logo-box">
                        <div class="invoice-logo"><img src="<?php echo base_url(); ?>images/MyMaid-Logo.png"></div>
                    </div>
                    <!--<form name="invoiceaddform" action="<?php// echo base_url(); ?>invoice/add_invoice" id="invoiceaddform" method="POST">-->
                        <!--<input type="hidden" value="<?php// echo $jobdetail[$bookings_id]->customer_id; ?>" name="jobhiddencustomerid" id="jobhiddencustomerid" />    -->
                        <div class="col-md-12 col-sm-12 invoice-address-box">
                            <div class="col-md-6 col-sm-12 invoice-address no-left-right-padding">
                                <p><strong>My Maid Cleaning Services</strong><br>
                                    Sheikh Zayed road<br>
                                    Dubai, UAE.<br><br>
                                    Tel &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    : &nbsp; <strong> +971 (04) 3300222</strong><br>
                                    Email &nbsp;&nbsp;&nbsp;&nbsp; : &nbsp; <strong>info@mymaid.ae</strong>
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-12 invoice-date no-left-right-padding">
                                <div class="col-md-6 col-sm-12 no-left-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Issue date :</p>
                                        <!--<input name="invoiceissuedate" id="invoiceissuedate" class="text-field" type="text">-->
                                        <p><?php echo $invoiceissuedate; ?></p>
                                    </div><!--text-field-main end-->
                                </div>
                                <div class="col-md-6 col-sm-12 no-right-padding">  
                                    <div class="text-field-main no-bottom-padding">
                                        <p>Due date :</p>
<!--                                        <input name="invoiceduedate" id="invoiceduedate" class="text-field" type="text">-->
                                        <p><?php echo $invoiceduedate; ?></p>
                                    </div><!--text-field-main end-->
                                </div>
                                <!--<div class="col-md-12 col-sm-12 no-right-padding">  
                                    <div class="text-field-main">
                                        <p class="text-right">Amount Due : <strong>$0.00</strong></p>
                                    </div><!--text-field-main end-->
                                <!--</div>-->
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-to-address no-left-right-padding">
                                <p><strong>To,</strong><br>
                                <span><strong><?php echo $jobdetail[$bookings_id]->customer_name; ?></strong><br>
                                <?php echo $jobdetail[$bookings_id]->customer_address; ?><br>
                                </span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-job-det no-left-right-padding">
                                <p><strong>Job</strong><br>
                                <?php
                                $nameOfDay = date('l', strtotime($scheduledates));
                                $day = date('d', strtotime($scheduledates));
                                $month = date('F', strtotime($scheduledates));
                                $year = date('Y', strtotime($scheduledates));
                                ?>
                                <span>Work carried out at: <?php echo $jobdetail[$bookings_id]->customer_address; ?>. On: <?php echo $nameOfDay; ?> <?php echo $day; ?> <?php echo $month; ?> <?php echo $year; ?></span></p>
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-table no-left-right-padding">
                                <div class="Table table-top-style-box no-top-border">
                                    <div class="Heading table-head">
                                        <div class="Cell">
                                            <p><strong>Maid</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Time</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Amount</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Paid Amount</strong></p>
                                        </div>
                                        <div class="Cell">
                                            <p><strong>Balance Amount</strong></p>
                                        </div>
                                    </div>
                                    <?php
                                    $total_fee_amount = 0;
                                    $total_paid_amount = 0;
                                    $balance_amont = 0;
                                    foreach ($jobdetail as $jobs)
                                    {
                                        if($jobs->collected_amount != 0 && $jobs->collected_amount != "" && $jobs->collected_amount >0)
                                        {
                                            $status = 1;
                                        } else {
                                            $status = 0;
                                        }
                                        
                                    ?>
<!--                                    <input type="hidden" value="<?php// echo $invoice_number; ?>" name="invoice_num[]" id="" />
                                    <input type="hidden" value="<?php// echo $jobs->customer_id; ?>" name="customer_id[]" id="" />
                                    <input type="hidden" value="<?php// echo $jobs->day_service_id; ?>" name="day_service_id[]" id="" />
                                    <input type="hidden" value="<?php// echo $status; ?>" name="invoice_status[]" id="" />
                                    <input type="hidden" value="<?php// echo $jobs->booking_id; ?>" name="booking_id[]" id="" />
                                    <input type="hidden" value="<?php// echo $jobs->service_date; ?>" name="service_date[]" id="" />-->
                                    <div class="Row">
                                        <div class="Cell">
                                            <p><?php echo $jobs->maid_name; ?></p>
                                        </div>
                                        <div class="Cell">
                                            <p><?php echo $jobs->time_from; ?> - <?php echo $jobs->time_to; ?></p>
                                        </div>
                                        <div class="Cell">
                                            <p><?php
                                            $totalfeee = ($jobs->total_fee + $jobs->material_fee);
                                            $total_fee_amount += $totalfeee;
                                            ?>
                                            <?php echo $totalfeee; ?> DHS
                                            <!--<input type="hidden" value="<?php// echo $totalfeee; ?>" name="billed_amount[]" id="" /></p>-->
                                        </div>
                                        <div class="Cell">
                                              <?php
                                              $total_paid_amount += $jobs->collected_amount;
                                              ?>
                                              <p><?php echo $jobs->collected_amount; ?> DHS
                                            <!--<input type="hidden" value="<?php// echo $jobs->collected_amount; ?>" name="received_amount[]" id="" /></p>-->
                                        </div>
                                        <div class="Cell">
                                              <?php
                                              $bal_amt = ($totalfeee - $jobs->collected_amount);
                                              $balance_amont += $bal_amt;
                                              ?>
                                              <p><?php echo $bal_amt; ?> DHS
                                            <!--<input type="hidden" value="<?php// echo $bal_amt; ?>" name="balance_amount[]" id="" /></p>-->
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="Row">
                                        <div class="Cell total-box no-right-border">
                                          &nbsp;
                                        </div>
                                        <div class="Cell total-box no-left-border">
                                          &nbsp;
                                          <span class="total-text text-right"><b class="pull-right">Total &nbsp;&nbsp; </b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                           <span class="total-text text-right"><b><?php echo $total_fee_amount; ?> DHS </b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b><?php echo $total_paid_amount; ?> DHS </b></span>
                                        </div>
                                        <div class="Cell total-box light-green">
                                            <span class="total-text text-right"><b><?php echo $balance_amont; ?> DHS </b></span>
                                        </div>
                                    </div>
                                </div><!--Table table-top-style-box end--> 
                            </div>
                            <div class="col-md-12 col-sm-12 invoice-det-note no-left-right-padding">
                                <p><strong>Note</strong><br></p>
                                <span><?php echo $notes; ?></span>
                                <!--<textarea name="invoice_note" cols="" rows="" class="text-field-big"></textarea>-->
                            </div>
<!--                            <div class="col-md-12 col-sm-12 invoice-button-main no-left-right-padding">
                                <div class="col-md-6 col-sm-12 invoice-button">
                                   &nbsp;
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-left-padding">
                                    <input value="CANCEL" class="text-field-but dark" type="button">
                                </div>
                                <div class="col-md-3 col-sm-6 invoice-button no-right-padding">
                                    <input value="SAVE" name="jobinvoicesave" id="jobinvoicesave" class="text-field-but" type="submit">
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div><!--invoice-box-right end-->
            </div><!--invoice-box-right-main end-->
            <div class="col-md-4 col-sm-12 invoice-box-left-main no-right-padding">
                <div class="col-md-12 col-sm-12 invoice-box-left no-left-right-padding">
                    <div class="col-md-12 col-sm-12 invoice-map no-left-right-padding">
                        <div id="jobmap" style="width: 100%; height: 300px;"></div>
<!--                 	<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d40875.09206418169!2d55.188899401431456!3d25.105860601136317!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1511244532294" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen>
                        </iframe>	-->
                    </div>
                    <div class="col-md-12 col-sm-12 invoice-left-cont">
                        <div class="col-md-12 col-sm-12 invoice-left-address">
                            <p style="font-size:16px;"><strong>Job for :  <?php echo $jobdetail[$bookings_id]->customer_name; ?></strong></p>
                            <p><?php echo $jobdetail[$bookings_id]->customer_address; ?><br></p>
                        </div>
                        <div class="col-md-12 col-sm-12 invoice-left-schedule">
                            <p><strong>Schedule</strong><br>
                            <span><strong>At <?php echo $jobdate; ?> (<?php echo $jobdetail[$bookings_id]->time_from; ?> - <?php echo $jobdetail[$bookings_id]->time_to; ?>)</strong><br>
                            By <?php echo $jobdetail[$bookings_id]->user_fullname; ?><br>
                            <?php
                            if($jobdetail[$bookings_id]->service_status == 1)
                            {
                                $stats = "Started";
                            } else if($jobdetail[$bookings_id]->service_status == 2)
                            {
                                $stats = "Finished";
                            }
                            ?>
                            Status: <?php echo $stats; ?></span></p>
                        </div>
                        <div class="col-md-12 col-sm-12 invoice-left-schedule">
                            <p><strong><?php echo $jobdetail[$bookings_id]->customer_name; ?></strong><br>
                            <span>Tel &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    : &nbsp; <strong> <?php echo $jobdetail[$bookings_id]->mobile_number_1; ?></strong><br>
                            Email &nbsp;&nbsp;&nbsp;&nbsp; : &nbsp; <strong><?php echo $jobdetail[$bookings_id]->email_address; ?></strong></span></p>
                        </div>
                        <div class="col-md-12 col-sm-12 invoice-button-main">
                            <div class="col-md-6 col-sm-6 invoice-button no-left-padding">
                                <input value="Job Details" class="text-field-but" type="submit">
                            </div>
                            <div class="col-md-6 col-sm-6 invoice-button no-left-right-padding">
                                <input value="Customer Details" class="text-field-but" type="submit">
                            </div>
                        </div>
                    </div>
                </div><!--invoice-box-left end-->
            </div><!--invoice-box-left-main end-->
        </div><!--job-field-main end--> 
    </div><!--row job-field-wrapper end--> 
</section><!--Job Section end-->
<script type="text/javascript">
    function initialize() {
        var joblat = <?php if($jobdetail[$bookings_id]->latitude != ""){ echo $jobdetail[$bookings_id]->latitude; } else { echo "25.2048"; } ?>;
        var joblng = <?php if($jobdetail[$bookings_id]->longitude != ""){ echo $jobdetail[$bookings_id]->longitude; } else { echo "55.2708"; } ?>;
       var latlng = new google.maps.LatLng(joblat,joblng);
       console.log(latlng);
        var map = new google.maps.Map(document.getElementById('jobmap'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();
        var addressss = '<?php if($jobdetail[$bookings_id]->latitude != ""){ echo $jobdetail[$bookings_id]->customer_address; } else { echo "Dubai"; } ?>';
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : ' + addressss + '</div></div>';
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>