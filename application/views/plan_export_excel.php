<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=planReport.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th style="line-height: 18px; padding: 10px;">Sl No</th>
			<th style="line-height: 18px; padding: 10px;">Maid</th>
			<th style="line-height: 18px; padding: 10px;">Customer</th>
			<th style="line-height: 18px; padding: 10px;">Working Hours</th>
			<th style="line-height: 18px; padding: 10px;">M.O.P</th>
			<th style="line-height: 18px; padding: 10px;">Billed Amount</th>
			<th style="line-height: 18px; padding: 10px;">Collected Amount</th>
			<th style="line-height: 18px; padding: 10px;">Balance Amount</th>
			<th style="line-height: 18px; padding: 10px;">Receipt No</th>
			<th style="line-height: 18px; padding: 10px;">Status</th>
        </tr>
    </thead>
    <tbody>
		<?php
		if(!empty($dayservices))
		{
			$i = 0;                          
			$total_fee = 0;
			$material_fee = 0;
			$collected_total_fee = 0;
			$total_wrk_hrs = 0;
			$collected_amount = 0;
			$ztotal_wrk_hrs = 0;
			foreach ($dayservices as $service)
            {
            	$check_buk_exist = $this->bookings_model->get_booking_exist($service->booking_id,$formatted_date);
				if(!empty($check_buk_exist))
				{
					$mop = $check_buk_exist->mop;
				} else {
					if($service->pay_by!="")
					{
						$mop = $service->pay_by;
					} else {
						$mop = $service->payment_mode;
					}
				}
				if($service->payment_type == "D")
				{
					$paytype = "(D)";
				} else if($service->payment_type == "W")
				{
					$paytype = "(W)";
				} else if($service->payment_type == "M")
				{
					$paytype = "(M)";
				} else
				{
					$paytype = "";
				}
				$service_status = $service->service_status == 1 ? 'On Going' : ($service->service_status == 2 ? 'Finished' : ($service->service_status == 3 ? 'Cancelled' : 'Not Started'));
				$transfer_text=(strlen($service->transferred_zone)>0)?"<br><b> - Transferred Zone - ".$service->transferred_zone."</b>":"";
				if($service->total_fee == "")
				{
					$servicetotalfee = $service->total_amount;
				} else {
					$servicetotalfee = $service->total_fee;
				}
				$total_fee += $servicetotalfee;
				$material_fee += $service->material_fee;
				$collected_total_fee += $service->collected_amount;
				$total_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600);
		?>
		<tr>
			<td style="line-height: 18px;"><?php echo ++$i; ?></td>
			<td style="line-height: 18px;"><?php echo $service->maid_name; ?></td>
			<td style="line-height: 18px;"><?php echo $service->customer_name; ?>(<?php echo ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) ?>)<?php echo $paytype.$transfer_text; ?></td>
			<td style="line-height: 18px;"><?php echo $service->time_from; ?> - <?php echo $service->time_to; ?> [<?php echo ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600); ?>]</td>
			<td style="line-height: 18px;"><?php echo $mop; ?></td>
			<td style="line-height: 18px;"><?php echo $servicetotalfee; ?></td>
			<td style="line-height: 18px;"><?php echo $service->collected_amount; ?></td>
			<td style="line-height: 18px;"><?php echo $service->balance.$service->signed; ?></td>
			<td style="line-height: 18px;"><?php echo $service->ps_no; ?></td>
			<td style="line-height: 18px;"><?php echo $service_status; ?></td>
		</tr>
		<?php
			}
		}
		?>
	</tbody>
	<tfoot>
		<tr style="font-weight:bold;">
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;">TOTALS</td>
			<td style="line-height: 18px;"><?php echo $total_wrk_hrs; ?></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"><?php echo number_format($total_fee, 2) . '('. number_format($material_fee, 2) .')'; ?></td>
			<td style="line-height: 18px;"><?php echo number_format($collected_total_fee, 2); ?></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
			<td style="line-height: 18px;"></td>
		</tr>
	</tfoot>
</table>