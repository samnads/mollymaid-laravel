<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $settings->site_name; ?> Invoice - <?= $invoice_detail[0]->invoice_num; ?></title>
</head>
<style>
@page {
	margin: 0px 0px 0px 0px !important;
	padding: 0px 0px 0px 0px !important;
}


</style>
<body style="padding: 0px; margin: 0px;">

<div class="main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">

    <header style="height: 94px; overflow: hidden; position: fixed; left:0; top:0; z-index: 999;">
            
            <div class="header" style="width:100%; height:auto; padding: 5px 0px 10px 0px; margin: 0px; background: #eee;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="22%" rowspan="2" align="right"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->invoice_logo, 'invoice-logo.png'); ?>" height="60" style="padding: 10px 0px 10px 50px;"/></td>
                  <td style=" padding: 5px 0px 0px 0px;"></td>
                  <td width="32%" rowspan="2" valign="bottom">
                  
                  
                  
                         <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                                      <tr>
                                        <td width="50%" style="padding: 0px 0px 0px 0px"><strong>Phone</strong></td>
                                        <td width="5%">:</td>
                                        <td width="45%" valign="middle" style="padding: 0px 25px 0px 0px"><strong><?= $settings->company_tel_number ?></strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Email </td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px"><?= $this->config->item('mail_admin'); ?></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">TRN</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px"><?= $this->config->item('company_trn'); ?></td>
                                      </tr>
                                      
                             </table>
                  
                  
                  
                  </td>
                </tr>
                <tr>
                  <td width="50%"><p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: -5px 25px 0px 20px; margin: 0px;">
                            <b><?= $settings->company_name ?></b>
                            <br />
                            Address Line 1<br>
                            Address Line 2
                          </p>
                  </td>
                </tr>
              </table>

     </div>
            
    </header>
    
  <section style="width:100%; height: 700px;  padding: 170px 30px 0px 30px;">
             <div style="width: 100%; height:auto; margin: 0px; padding: 0px;">
                 <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                      
                      <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
                         Recipient :
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 20px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                         <b><?php echo $invoice_detail[0]->customer_name; ?></b>,
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                        <?php echo $invoice_detail[0]->bill_address; ?>
                     </p>
                     
                     
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 50px 0px 10px 0px; margin: 0px; text-transform: uppercase;">
                         Service Address :
                     </p>
                     
                     <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; color: #333; padding: 0px 0px 0px 20px; margin: 0px;">
                        <?php echo $invoice_detail[0]->bill_address; ?>
                     </p>
                     
                 </div>
                 
                 <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                   <div style="width: 100%; height:auto; margin: 0px; padding: 0px; background: #0097db;">
                           <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 10px 20px; margin: 0px;">
                         Tax Invoice :	 <?php echo str_replace("INV-", "", $invoice_detail[0]->invoice_num); ?>
                     </p>
                      </div>
                      
                      
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 25px; color: #333; padding: 5px 0px 0px; margin: 0px; background: #eee;">
                          <tr>
                            <td width="50%" style="padding-left: 20px;">Issued</td>
                            <td width="50%" align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($invoice_detail[0]->invoice_date)) ?></td>
                          </tr>
                          <tr>
                            <td style="padding-left: 20px;">Due</td>
                            <td align="right" style="padding-right: 20px;"><?php echo date('d F - Y',strtotime($invoice_detail[0]->invoice_due_date)) ?></td>
                          </tr>
                          <tr>
                            <td style="padding-left: 20px;padding-bottom: 10px;">Attn</td>
                            <td align="right" style="padding-right: 20px; line-height:15px; padding-bottom: 5px;">Finance and Accounts Department</td>
                          </tr>
                          <?php
                          if($invoice_detail[0]->customer_trn != "")
                          {
                          ?>
                          <tr>
                            <td style="padding-left: 20px; padding-bottom: 10px;">TRN</td>
                            <td align="right" style="padding-right: 20px; padding-bottom: 5px;"><?php echo $invoice_detail[0]->customer_trn; ?></td>
                          </tr>
                          <?php
                          }
                          ?>
                          <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
                            <td style="padding: 15px 20px; background: #8dc73f; ">Total</td>
                            <td align="right" style="padding: 15px 20px; background: #8dc73f; "><span style="font-size: 14px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_net_amount,2); ?></td>
                          </tr>
                        </table>
               </div>
             <div style="clear:both;"></div>
             </div>
             
             
             
             
             <div style="width: 100%; height:auto; margin: 0px; padding: 50px 0px 10px 0px;">
                  <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                       <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
                           <!-- For Services Rendered : -->
                       </p>
                  </div>
                  <div style="width: 50%; height:auto; float: left; margin: 0px; padding: 0px;">
                    <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 14px; line-height: 16px; color: #333; text-align: right; padding: 0px; margin: 0px;">
                      <!-- <span style="color: #777;">For the Month of</span> July 2020 -->
                    </p>
                  </div>
                  <div style="clear:both;"></div>
             </div>
             
             
             
             
             <div style="width: 100%; height:auto; margin: 0px; padding: 0px 0px 0px 0px;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #333; padding: 0px; margin: 0px;">
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; line-height: 20px; color: #FFF; background: #0097db; ">
                      <td width="15%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">DATE</td>
                      <td width="22%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">PRICE PER HOUR</td>
                      <td width="23%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">PRODUCT/SERVICES</td>
                      <td width="35%" style="padding: 7px 0px 7px 10px; border-right: 1px solid; border-color: #FFF;">DESCRIPTION</td>
                      <td width="12%" align="center" style="border-right: 1px solid; border-color: #FFF;">HOUR(s)</td>
                      <!-- <td width="12%" align="center" style="border-right: 1px solid; border-color: #FFF;">Unit Cost</td> -->
                      <td width="15%" align="center">TOTAL</td>
                    </tr>
                    
                    



                    <?php
                    $i = 1;
                    foreach ($invoice_detail as $jobs)
                    {
                      $tot_hrs = $jobs->service_hrs;
                    ?>
                    <tr>
                    <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo date('d-m-Y',strtotime($jobs->service_date)); ?></td>
                    <td align="center" style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo "AED $jobs->inv_unit_price"; ?></td>

                      <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-left: 1px solid; border-color: #CCC;">
                          <!-- <span style="font-size: 13px; font-weight: bold;"><?php echo date('M d,Y', strtotime($jobs->service_date)); ?></span><br /> -->
                          <span><?php echo $jobs->monthly_product_service; ?>
                          </span>
                      </td>
                      <td style="padding: 10px; border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $jobs->description; ?></td>
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><?php echo $tot_hrs; ?></td>
                      <!-- <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">AED</span> 25:00</td> -->
                      <td align="center" style="border-bottom: 1px solid; border-right: 1px solid; border-color: #CCC;"><span style="font-size: 11px;">AED</span> <?php echo number_format($jobs->line_amount,2); ?></td>
                    </tr>
                     <?php
                      $i++; } 
                    ?>
                    
                    
                  </table>
                  
                  
                  
                  
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 20px; color: #333; font-weight: bold; padding: 0px; margin: 0px;">
                    <tr style="">
                      <td width="30%">&nbsp;</td>
                      <td width="35%" style="padding:7px 15px; background: #eee; border-left: 1px solid; border-color: #CCC;">Subtotal</td>
                      <td width="35%" align="right" style="padding: 7px 15px; background: #eee; border-right: 1px solid; border-color: #CCC;"><span style="font-size:11px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_total_amount,2); ?></td>
                    </tr>
                    
                    
                    <tr>
                      <td>&nbsp;</td> 
                       <?php
                      $div = $invoice_detail[0]->invoice_tax_amount/$invoice_detail[0]->invoice_total_amount;
                      $div = is_nan($div) ? 0 : $div;
                      $service_vat_percentage = number_format($div*100,2);
                      ?>
                      <td style="padding: 7px 15px; border-left: 1px solid; border-color: #CCC;">VAT(<?= $service_vat_percentage; ?>%)</td>
                      <td align="right" style="padding: 7px 15px; border-right: 1px solid; border-color: #CCC;"><span style="font-size:11px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_tax_amount,2); ?></td>
                    </tr>
                    
                    
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; line-height: 16px; color: #FFF; padding: 0px; margin: 0px;">
                        <td>&nbsp;</td> 
                        <td style="padding: 15px 15px; background: #8dc73f; ">Total</td>
                        <td align="right" style="padding: 15px 15px; background: #8dc73f; "><span style="font-size: 14px;">AED</span> <?php echo number_format($invoice_detail[0]->invoice_net_amount,2); ?></td>
                    </tr>
                    </table>
                  
                  

             </div>
    
    
    
    <?php if ($invoice_detail[0]->invoice_status == 2): ?>
    <div style="height:auto; float: left;padding: 50px 20px 0px 300px;">
                 <img src="<?=base_url('images/invoice-cancelled-icon.png');?>" width="150"/>
            </div>
    <?php endif;?>
    
    
    
    
             
             
    </section>
    
    <footer style="height: 98px; overflow: hidden; position: fixed; left:0; bottom:0; z-index: 999;">
          
                  <div class="bot-text" style="width:90%; height:auto; padding: 0px 0px 50px 0px; margin: 0px auto;">
               
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 5px 0px 0px 0px; margin: 0px;">Accountant Signature</p>
                    </td>
                    <td align="right" valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 5px 0px 0px 0px; margin: 0px;">
                            Customer Signature
                        </p>
                    </td>
                  </tr>
                </table>

          </div>
          
          
          
          
                  <div class="footer" style="width:100%; height:auto; padding: 5px 0px; background: #ffe0e0; margin: 0px; text-align:center;">
               <a href="<?= $settings->company_website_url; ?>" target="_blank" style="font-family: Roboto, sans-serif; font-size:12px; color: #555; line-height: 16px; text-decoration: none; padding: 0px 0px 0px 0px; margin: 0px;"><?= $settings->company_website_label; ?></a>
          </div>
          
          
          </footer>
    
</div>

</body>
</html>
