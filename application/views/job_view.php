<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk&amp;libraries=places"></script>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">
   
            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <div class="widget-header"> 
                
                <ul>
                <li>
                    
                        <i class="icon-th-list"></i>
                        <h3>Job Dashboard</h3> 
                        
                       </li>
                       
                        <li class="mr-0 float-right">
                        <?php
						if($jobdetail[0]->customer_address == "")
						{
							$a_address = 'Building - '.$jobdetail[0]->building.', '.$jobdetail[0]->unit_no.''.$jobdetail[0]->street;
						} else {
							$a_address = $jobdetail[0]->customer_address;
						}
                        if($jobdetail[0]->service_status == 2)
                        {
                        ?>
                        
                         <div class="topiconnew border-0 green-btn">
                             <a class="invoicejobbutton" id="invoicejobbutton" href="<?php echo base_url(); ?>invoice/<?php echo $scheduledates; ?>/<?php echo $jobdetail[0]->booking_id; ?>/<?php echo $jobdetail[0]->customer_id; ?>"><i class="fa fa-print"></i> Invoice</a>
                        </div>
                        
                        <?php } ?>
                        
                        
                       
                        
                        </li>
                        </ul>
                         
                </div>
                <div id="map" style="width: 100%; height: 300px;"></div>
                <div class="col-md-5 col-sm-5 no-left-right-padding">
                    <!--<h3 class="form-section"><?php // echo $jobdetail[0]->customer_name; ?></h3>-->
                    
                    
                    <input type="hidden" value="<?php echo $jobdetail[0]->booking_id; ?>" id="hiddenbookingid" />
                    <input type="hidden" value="<?php echo $jobdetail[0]->booking_type; ?>" id="hiddenbookingtype" />
                    <input type="hidden" value="<?php echo $scheduledates; ?>" id="hiddenbookingdate" />
                    <div class="contact-box">
                        <div class="caption" style="font-size: 20px; color: #4897e6; padding: 30px 0px 10px 0px; ">
                            <i class="fa fa-users"></i> &nbsp;
                            Contacts
                        </div>
                        <div class="contact-body" style="color: #666;">
                            <h4><i class="fa fa-map-marker"></i> &nbsp;<?php echo $a_address; ?></h4><br>
                            
                            <span><i class="fa fa-user"></i> &nbsp;<strong><?php echo $jobdetail[0]->customer_name; ?></strong></span>
                          
                            <span><i class="fa fa-phone"></i> &nbsp;<?php echo $jobdetail[0]->mobile_number_1; ?></span>
                            
                            <span><i class="fa fa-envelope"></i> &nbsp;<?php echo $jobdetail[0]->email_address; ?></span>
                            <p><?php echo $jobdetail[0]->booking_note; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-7 no-left-right-padding">
                    <div class="caption" style="font-size: 20px; color: #4897e6; padding: 30px 0px 10px 0px; ">
                        <i class="fa fa-briefcase"></i> &nbsp;
                        Schedule
                    </div>
                    <!--<h3 class="form-section">Schedule</h3>-->
                    <div class="contact-body" style="color: #666;">
                        <h4><i class="fa fa-calendar"></i> <?php echo $jobdate; ?> (<?php echo $jobdetail[0]->time_from; ?> - <?php echo $jobdetail[0]->time_to; ?>) </h4><br>
                        <?php
                        if($jobdetail[0]->booking_type == "WE")
                        {
                            $btypes = "Weekly";
                        } else {
                            $btypes = "One Day";
                        }
                        ?>
                        <span><i class="fa fa-calendar-o"></i> &nbsp;<strong><?php echo $btypes; ?></strong></span>

                        <!--<span><i class="fa fa-phone"></i> &nbsp;<?php echo $jobdetail[0]->mobile_number_1; ?></span>-->

                        <!--<span><i class="fa fa-envelope"></i> &nbsp;<?php echo $jobdetail[0]->email_address; ?></span>-->
                        <!--<p><?php echo $jobdetail[0]->booking_note; ?></p>-->
                    </div>
                    
                    <br>
                    <br>
                    by
                    <br>
                    <h4><?php echo $jobdetail[0]->user_fullname; ?></h4>
                    <?php
                    if($jobdetail[0]->booking_status == 0)
                    {
                    ?>
                    <p>Job is not scheduled yet.</p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <div class="col-md-5 col-sm-5 no-left-right-padding startfinishbox">
                    
                    <?php
                    if($jobdetail[0]->booking_status == 0)
                    {
                        $stats = "New";
                    } else if($jobdetail[0]->booking_status == 1)
                    {
                        if($jobdetail[0]->service_status == 1)
                        {
                            $stats = "In Progress";
                        } else if($jobdetail[0]->service_status == 2)
                        {
                            $stats = "Finished";
                        } else if($jobdetail[0]->service_status == 3)
                        {
                            $stats = "Cancelled";
                        } else {
                            $stats = "Scheduled";
                        }
                    } else if($jobdetail[0]->booking_status == 2)
                    {
                        $stats = "Deleted";
                    } 
                    ?>
                    <h4>Status: <?php echo $stats; ?>
                        <?php
                    if($jobdetail[0]->service_status == 1)
                    {
                    ?>
                        <span id="timerjob"></span>
                    <?php } ?></h4>
                    <?php
                    if($jobdetail[0]->service_status == 1)
                    {
                    ?>
                    <h4>Started at: <?php echo date("g:i a", strtotime($jobdetail[0]->starting_time)); ?></h4>
                    <?php } else if($jobdetail[0]->service_status == 2){
                    $datetime1 = new DateTime(date("g:i a", strtotime($jobdetail[0]->starting_time)));
                    $datetime2 = new DateTime(date("g:i a", strtotime($jobdetail[0]->ending_time)));
                    $interval = $datetime1->diff($datetime2);
                    
                    ?>
                    <h4>Started at: <?php echo date("g:i a", strtotime($jobdetail[0]->starting_time)); ?></h4>
                    <h4>Finished at: <?php echo date("g:i a", strtotime($jobdetail[0]->ending_time)); ?></h4>
                    <h4>Took <?php echo $interval->format('%h')." Hours ".$interval->format('%i')." Minutes"; ?></h4>
                    <?php } ?>
                    <?php
                    if($jobdetail[0]->ps_no != ""){
                        $pinkslip_no = $jobdetail[0]->ps_no;
                    } else {
                        $pinkslip_no = "0";
                    }
                    if($jobdetail[0]->booking_status == 1)
                    {
                        if($jobdetail[0]->service_status != 2 && $jobdetail[0]->service_status != 3)
                        {
                            $activity_status = $jobdetail[0]->service_status == 1 ? 1 : ($jobdetail[0]->service_status == 2 ? 2 : ($jobdetail[0]->service_status == 3 ? 3 : 0));
                    ?>
                    <div class="btn-group btn-group-circle btn-group-solid eventstart-but-main">
                        <button class="btn eventstart-but <?php if($jobdetail[0]->service_status == 1){ ?>red<?php } ?>" <?php if($jobdetail[0]->service_status == 1){ ?>style="pointer-events: none;"<?php } ?> onclick="get_activity_new_job(<?php echo $activity_status; ?>,<?php echo $jobdetail[0]->booking_id; ?>);">Start &nbsp; <i class="fa fa-play"></i></button>
                        <button class="btn eventfinish-but" onclick="get_activity_new_job_finish(<?php echo $activity_status; ?>,<?php echo $jobdetail[0]->booking_id; ?>,<?php echo $jobdetail[0]->collected_amount; ?>,<?php echo $jobdetail[0]->service_status; ?>,<?php echo $jobdetail[0]->payment_status; ?>,<?php echo $pinkslip_no; ?>);">Finish &nbsp; <i class="fa fa-check"></i></button>
                    </div>
                    <?php
                        }
                    }
                    ?>
                </div>
                <div class="col-md-7 col-sm-7 no-left-right-padding">
                    
                </div>
            </div>
       
    </div>
</section>
<div id="activity-modal_job" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 style="color:white; padding-bottom: 0px !important;">Activity</h3>
    </div>
    <div class="modal-body">
        <p> 
            <a class="btn btn-medium mm-btn-success" id="start_job" title="Start" href="javascript:void(0)" ><i class="icon-play"> </i>START</a>
            <a class="btn btn-medium btn-primary" style="background-color: #7D7D7D !important;" id="restart_job" title="Restart" href="javascript:void(0)" ><i class="icon-play"> </i>RESTART</a>
            <a class="btn btn-medium mm-btn-warning" id="stop_job" title="Stop" href="javascript:void(0)" ><i class="icon-stop"> </i>STOP</a>
            <a class="btn btn-medium mm-btn-info" id="transfer_job" title="Transfer" href="javascript:void(0)" ><i class="icon-share"> </i>TRANSFER</a>
            <!--<a class="btn btn-medium btn-danger" title="Cancel" href="javascript:void" ><i class="icon-ban-circle"> </i>CANCEL</a>-->
        </p>
        
        <div class="controls" id="payment-details-job">
            <label class="radio inline">
                <input id="paymnt-job" type="radio" name="payment_type" value="1"> Payment
            </label>

            <label class="radio inline">
                <input id="no-paymnt-job" type="radio" name="payment_type" value="0"> Payment Not Received
            </label>
            <label class="radio inline">
                <input id="no-service-job" type="radio" name="payment_type" value="3"> Service Not Done
            </label>
        </div>
        <div style="padding: 15px 0;" id="pinkid-job">
            <label class="inline" style="display: inline-block;">
                <input class="inline" type="text" id="paid-amount-job" style="width: 50px;"/>
            </label>
            <label class="inline" style="display: inline-block; padding-left: 15px;">PS No
                <input type="text" id="ps-no-job" style="width: 100px;"/>
            </label>
        </div>
        <p id="frm-transfer-job"> 
            <!--<form id="frm-transfer">-->
                <select id="transfer-zone-id-job">
                    <option value="">Select Driver</option>
                    <?php
                    foreach ($tablets as $val)
                    {
                        echo '<option value="' . $val->tablet_id . '">' . $val->tablet_driver_name . '</option>';
                    }

                    ?>
                </select>
            <!--</form>-->
        </p>
       
        
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a class="save-but-job" style="text-decoration: none;" href="javascript:void(0)" onclick="add_activity_job();">Submit</a>
    </div>
</div>
    <script type="text/javascript">
    function initialize() {
        var joblat = <?php if($jobdetail[0]->latitude != ""){ echo $jobdetail[0]->latitude; } else { echo "25.2048"; } ?>;
        var joblng = <?php if($jobdetail[0]->longitude != ""){ echo $jobdetail[0]->longitude; } else { echo "55.2708"; } ?>;
       var latlng = new google.maps.LatLng(joblat,joblng);
       console.log(latlng);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();
        var addressss = '<?php if($jobdetail[0]->latitude != ""){ echo $a_address; } else { echo "Dubai"; } ?>';
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : ' + addressss + '</div></div>';
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <?php
    if($jobdetail[0]->service_status == 1)
    {
    ?>
    <script type="text/javascript">
        startTime();
        function startTime(){
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            var nn = '<?php echo $jobdate; ?> <?php echo $jobdetail[0]->starting_time; ?>';
            var d = new Date(nn);
            //alert(d);
            var t=today.getTime()-d.getTime();
            var time=convertMS(t);

            document.getElementById('timerjob').innerHTML =time.h+":"+time.m+":"+time.s;
            //hour+ ":" + m + ":" + seconds;
            var t = setTimeout(startTime, 500);
        }
        function convertMS(ms) {
            var d, h, m, s;
            s = Math.floor(ms / 1000);
            m = Math.floor(s / 60);
            s = s % 60;
            h = Math.floor(m / 60);
            m = m % 60;
            d = Math.floor(h / 24);
            h = h % 24;
            if(h<10) {h = "0" + h  };
            if(m<10) {m = "0" + m  };
            if(s<10) {s = "0" + s  };
            return {d: d,h:h, m: m, s: s };
        };
        </script>
    <?php } ?>