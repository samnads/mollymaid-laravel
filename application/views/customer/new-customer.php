<script>
   var customer_id = <?= $customer ? $customer->customer_id : 'null' ?>;
</script>
<style>
   #edit-profile label,
   input,
   button,
   select,
   textarea {
      font-size: 12px !important;
   }

   .flexdatalist-multiple.flex0 {
      margin-bottom: 0px;
   }

   .flexdatalist-results li {
      margin: 0px !important;
      height: auto !important;
      overflow-y: hidden;
   }

   .error-message {
      color: red;
      font-size: 12px;
   }
</style>
<div id="new-access-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span id="b-maid-name">Add Customer Access</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div id="popup-booking" class="col-12 p-0">
         <form class="form-horizontal" method="post" id="new-access-form">
            <div class="modal-body">
               <!--<div class="row m-0 n-field-main">
            <p>Customer</p>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="customer_id" id="customer_id" data-placeholder="Select customer" style="width:100%">
            </select>
            </div>
          </div>-->
               <div class="row m-0 n-field-main">
                  <p>Access Type</p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="access_type" class="sel2" data-placeholder="Select type" style="width:100%">
                        <option value="">-- Select type --</option>
                        <option value="1">Key</option>
                        <option value="2">Card</option>
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <p>Access Code</p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="access" class="sel2" data-placeholder="Select access" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <p>Receive Notes</p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <textarea type="text" name="received_notes"></textarea>
                  </div>
               </div>
               <p class="text-danger" id="form-error">&nbsp;</p>
            </div>
      </div>
      <div class="modal-footer">
         <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
         <button type="submit" class="n-btn m-0" value="Submit" data-action="access-assign-btn">Add</button>
      </div>
      </form>
   </div>
</div>
<div id="crop-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span id="b-maid-name">Image Cropper</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
      </div>
      <div id="" class="col-12 p-0">
         <div class="modal-body">
            <div class="img-container">
               <img id="image" src="#">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
            <button type="button" class="n-btn mb-0" id="crop">Crop</button>
         </div>
      </div>
   </div>
</div>
<div class="row m-0">
   <div class="col-sm-12">
      <div class="widget">
         <!-- /widget-header -->
         <div class="widget-header">
            <div class="book-nav-top">
               <ul>
                  <li>
                     <i class="icon-th-list"></i>
                     <h3><?= $customer ? "Edit Customer - " . $customer->customer_name : "New Customer"; ?></h3>
                  </li>
                  <li class="mr-0 float-right">
                     <div class="topiconnew border-0 green-btn">
                        <a href="<?php echo base_url('customer/list'); ?>" title="Customers List"> <i class="fa fa-list"></i></a>
                     </div>
                  </li>
                  <div class="clear"></div>
               </ul>
            </div>
         </div>
         <div class="widget-content">
            <div class="tabbable">
               <ul class="nav nav-tabs pb-4" id="mytabs">
                  <li id="personal-li" data-tab="personal"><a href="#personal" data-toggle="tab">Basic Details</a></li>
                  <li id="accounts-li" data-tab="accounts"><a href="#accounts" data-toggle="tab">Account Info</a></li>
                  <li id="prefmaids-li" data-tab="prefmaids"><a href="#prefmaids" data-toggle="tab">Pref. Maids</a></li>
                  <li id="address-details-li" data-tab="address-details"><a href="#address-details" data-toggle="tab">Location</a></li>
                  <li id="access-li" data-tab="access"><a href="#access" data-toggle="tab">Keys/ Cards</a></li>
                  <li id="other-li" data-tab="other"><a href="#other" data-toggle="tab">Other</a></li>
               </ul>
               <form id="<?= $customer ? "edit_customer_form" : "new_customer_form"; ?>" class="form-horizontal" method="post">
                  <input type="hidden" value="<?= $customer->customer_id; ?>" name="customer_id">
                  <div class="tab-content">
                     <div class="tab-pane tab-panel" data-panel="personal">
                        <fieldset>
                           <div class="col-sm-4">
                              <fieldset>
                                 <div class="row m-0 n-field-main">
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <div class="row m-0">
                                          <div class="col-sm-4 n-radi-check-main p-0">
                                             <input type="radio" value="1" id="is_company_1" name="customer_type_id" class="" <?= !isset($customer) || $customer->customer_type_id == 1 ? "checked" : "" ?>>
                                             <label for="is_company_1">
                                                <span class="border-radius-3"></span>
                                                <p>Indivdual</p>
                                             </label>
                                          </div>
                                          <div class="col-sm-4 n-radi-check-main p-0">
                                             <input type="radio" value="2" id="is_company_2" name="customer_type_id" class="" <?= $customer->customer_type_id == 2 ? "checked" : "" ?>>
                                             <label for="is_company_2">
                                                <span class="border-radius-3"></span>
                                                <p>Company</p>
                                             </label>
                                          </div>
                                          <div class="col-sm-4 n-radi-check-main p-0">
                                             <input type="radio" value="3" id="is_company_3" name="customer_type_id" class="" <?= $customer->customer_type_id == 3 ? "checked" : "" ?>>
                                             <label for="is_company_3">
                                                <span class="border-radius-3"></span>
                                                <p>Dealer</p>
                                             </label>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main" id="customer_company" style="display:<?= !isset($customer) || $customer->customer_type_id != 2 ? "none" : 'block' ?>;">
                                    <p>Select Company<rf>
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <select name="customer_company_id" id="customer_company_id" class="sel2" style="width:100%">
                                          <option value="">-- Select company --</option>
                                          <?php foreach ($customer_companies as $company) : ?>
                                             <option value="<?= $company->customer_company_id ?>" <?= ($customer->customer_company_id == $company->customer_company_id) ? "selected" : "" ?>><?= $company->company_name ?></option>
                                          <?php endforeach; ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main" id="customer_dealer" style="display:<?= !isset($customer) || $customer->customer_type_id != 3 ? "none" : 'block' ?>;">
                                    <p>Select Dealer</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <select name="customer_dealer_id" id="customer_dealer_id" class="sel2" style="width:100%">
                                          <option value="">-- Select dealer --</option>
                                          <?php foreach ($customer_dealers as $dealer) : ?>
                                             <option value="<?= $dealer->customer_dealer_id ?>" <?= $customer->customer_dealer_id == $dealer->customer_dealer_id ? "selected" : "" ?>><?= $dealer->dealer_name ?></option>
                                          <?php endforeach; ?>
                                       </select>
                                    </div>
                                 </div>

                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Customer Code
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" name="customer_code" id="customer_code" autocomplete="off" value="<?= $customer->customer_code; ?>">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Customer Name
                                       <rf>
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" name="customer_name" id="customer_name" autocomplete="off" value="<?= $customer->customer_name; ?>">
                                    </div>
                                 </div>
                                 <!-- <?php if ($customer) : ?>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Customer code
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="text" name="customer_code" id="customer_code" autocomplete="off" value="<?= $customer->customer_code; ?>" disabled>
                                       </div>
                                    </div>
                                 <?php endif; ?> -->
                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Customer Nick Name
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" class="" id="customer_nick_name" name="customer_nick_name" autocomplete="off" value="<?= $customer->customer_nick_name; ?>">
                                    </div>
                                 </div>


                                 <div class="row m-0 n-field-main flag_reason" style="display:<?= (!isset($customer->is_flag) || $customer->is_flag == "N") ? "none" : "block" ?>">
                                    <p>Flag Reason</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <textarea class="" rows="1" id="flag_reason" name="flag_reason"><?= $customer->flag_reason; ?></textarea>
                                    </div>
                                 </div>

                                 <div class="row m-0 n-field-main blocked_new_booking_reason" style="display:<?= (!isset($customer->is_allowed_new_booking) || $customer->is_allowed_new_booking == "YES") ? "none" : "block" ?>">
                                    <p>Block New Booking Reason</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <textarea class="" rows="1" id="blocked_new_booking_reason" name="blocked_new_booking_reason"><?= $customer ? $customer->blocked_new_booking_reason : ""; ?></textarea>
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>Mobile Number<rf>
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="number" class="" id="mobile_number_1" name="mobile_number_1" autocomplete="off" value="<?= $customer->mobile_number_1; ?>">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>
                                       WhatsApp No.
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="number" class="" id="whatsapp_no_1" name="whatsapp_no_1" autocomplete="off" value="<?= $customer->whatsapp_no_1; ?>">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>Email Address</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="email" id="email" name="email" autocomplete="off" value="<?= $customer->email_address; ?>">
                                    </div>
                                 </div>

                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Contact Person
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" class="" id="contact_person" name="contact_person" autocomplete="off" value="<?= $customer->contact_person; ?>">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Contact Number
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="number" class="no-arrows" id="mobile_number_2" name="mobile_number_2" value="<?= $customer->mobile_number_2; ?>" autocomplete="off" value="">
                                    </div>
                                 </div>

                              </fieldset>
                           </div>
                        </fieldset>
                        <div class="col-sm-12"><button type="button" class="n-btn" data-action="next">Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button></div>
                     </div>
                     <div class="tab-pane tab-panel" data-panel="accounts" style="display:none;">
                        <fieldset>
                           <div class="col-sm-4">
                              <fieldset>
                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Payment Frequency
                                       <rf>
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <select name="payment_frequency_id" id="payment_frequency_id" class="sel2" style="width:100%;">
                                          <option value="">-- Select frequency --</option>
                                          <?php foreach ($payment_frequencies as $pf) : ?>
                                             <option value="<?= $pf->payment_frequency_id ?>" <?= ($customer->payment_frequency_id == $pf->payment_frequency_id) ? "selected" : "" ?>><?= $pf->payment_frequency ?></option>
                                          <?php endforeach; ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>
                                       Payment Mode
                                       <rf>
                                    </p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <select name="payment_mode_id" id="payment_mode_id" class="sel2" style="width:100%;">
                                          <option value="">-- Select mode --</option>
                                          <?php foreach ($payment_modes as $mode) : ?>
                                             <option value="<?= $mode->payment_mode_id ?>" <?= ($customer->payment_mode_id == $mode->payment_mode_id) ? "selected" : "" ?>><?= $mode->payment_mode ?></option>
                                          <?php endforeach; ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>Hourly</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" class="" id="hourly" step="any" name="hourly" value="<?= $customer->price_hourly ?: 35 ?>" autocomplete="off">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>Extra</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" class="" id="extra" step="any" name="extra" value="<?= $customer->price_extra ?: 35 ?>" autocomplete="off">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>Weekend</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                       <input type="text" class="" id="weekend" step="any" name="weekend" value="<?= $customer->price_weekend ?: 35 ?>" autocomplete="off">
                                    </div>
                                 </div>
                                 <div class="row m-0 n-field-main">
                                    <p>Customer Booking Type</p>
                                    <select class="col-sm-12" name="customers_book_type" id="customers_book_type" class="">
                                       <option value="0" <?php echo isset($customer->customer_booktype) && $customer->customer_booktype == '0' ? 'selected' : ''; ?>>One-off</option>
                                       <option value="1" <?php echo isset($customer->customer_booktype) && $customer->customer_booktype == '1' ? 'selected' : ''; ?>>Regular</option>
                                    </select>
                                 </div>
                              </fieldset>
                           </div>
                           <div class="col-sm-4">
                              <div id="target-2" class="">
                                 <fieldset>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Opening Balance&nbsp;
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="number" step="any" class="" id="initial_balance" name="initial_balance" value="<?= $customer->initial_balance ?: 0 ?>" autocomplete="off">
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>Balance Till</p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="text" placeholder="dd/mm/yyyy" id="initial_balance_date" name="initial_balance_date" value="<?= $customer->initial_bal_date ? DateTime::createFromFormat('Y-m-d', $customer->initial_bal_date)->format('d/m/Y') : "" ?>" autocomplete="off" readonly>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>Opening Balance Type</p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <select name="initial_bal_sign" id="initial_bal_sign" class="sel2" style="width:100%;">
                                             <option value="Cr" <?= (!isset($customer->initial_bal_sign) || $customer->initial_bal_sign == "Cr") ? "selected" : "" ?>>Credit</option>
                                             <option value="Dr" <?= $customer->initial_bal_sign == "Dr" ? "selected" : "" ?>>Debit</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>Profile Photo</p>
                                       <label style="width: 100px;">
                                          <img class="img-thumbnail" id="customer_avatar_show" src="<?= $customer->customer_photo_file != "" ? base_url('uploads/images/avatars/customer/' . $customer->customer_photo_file) : base_url('uploads/images/default/maid-avatar-upload.png'); ?>" style="width: 100%;">
                                          <input type="file" class="input-image" accept="image/*" data-selector="customer_avatar" data-crop-width="250" data-crop-height="250">
                                       </label>
                                       <input type="hidden" name="customer_avatar_base64" id="customer_avatar_base64">
                                    </div>
                                    <br />
                                 </fieldset>
                              </div>
                           </div>
                           <div class="col-sm-12"><button type="button" class="n-btn" data-action="next">Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button></div>
                        </fieldset>
                        <div class="col-sm-12">
                           <input type="hidden" name="call_method" id="call_method" value="customer/customerimgupload" />
                           <input type="hidden" name="img_fold" id="img_fold" value="customer_img" />
                           <input type="hidden" name="img_name_resp" id="img_name_resp" />
                        </div>
                     </div>
                     <div class="tab-pane tab-panel" data-panel="address-details" style="display:none;">
                        <div class="row n-field-main m-0 pt-3" id="address-list">
                           <div class="col-sm-4" id="address-single" style="border: 1px solid #ccc">
                              <input type="hidden" name="customer_address[]" value="JUST FOR BACKEND" />
                              <div class="col-sm-6 p-0 mb-3 pr-2">
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <p>
                                       Zone&nbsp;
                                    </p>
                                    <select name="address_zone_ids[]" id="address_zone_id_0" style="width:100%;">
                                       <option value="">-- Select zone --</option>
                                       <?php foreach ($zones as $zone) : ?>
                                          <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-sm-6 p-0 mb-3">
                                 <p>
                                    Area&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="address_area_ids[]" id="address_area_id_0" style="width:100%;">
                                       <option value="">-- Select area --</option>
                                       <?php foreach ($areas as $area) : ?>
                                          <option value="<?= $area->area_id ?>"><?= $area->area_name ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Location&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="address_location_ids[]" id="address_location_id_0" style="width:100%;">
                                       <option value="">-- Select location --</option>
                                       <?php foreach ($locations as $location) : ?>
                                          <option value="<?= $location->location_id ?>"><?= $location->location_name ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Landmark</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="address_landmark_ids[]" id="address_landmark_id_0" style="width:100%;">
                                       <option value="">-- Select landmark --</option>
                                       <?php foreach ($landmarks as $landmark) : ?>
                                          <option value="<?= $landmark->landmark_id ?>"><?= $landmark->landmark_name ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Residence Type&nbsp;
                                    <rf>
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="address_residence_type_ids[]" id="address_residence_id_0">
                                       <option value="">-- Select type --</option>
                                       <?php foreach ($residence_types as $residence_type) : ?>
                                          <option value="<?= $residence_type->residence_type_id ?>"><?= $residence_type->residence_type ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Building&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" name="address_building[]" id="address_building_id_0" />
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Unit No&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" name="address_unit_nos[]" id="address_unit_nos_id_0" class="" style="min-width: 270px;" />
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Address&nbsp;
                                    <rf>
                                 </p>
                                 <button type="button" name="add_address[]" id="add_address_id_0" class="text-right btn btn-suceess" data-action="add-address">Add</button>
                                 <button type="button" name="copy_address[]" id="copy_address_id_0" class="text-right btn btn-suceess" data-action="copy-address">copy</button>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <textarea type="text" name="address_addresses[]" id="address_addresses_id_0" class="" style="min-width: 270px;"></textarea>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Makkani Number&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="number" class="" name="address_makkani_numbers[]">
                                 </div>
                              </div>
                              <!-- <div class="row m-0 n-field-main">
                                 <p>
                                    Zip Code&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" class="" name="address_zip_codes[]">
                                 </div>
                              </div> -->
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Latitude
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" class="" name="address_latitudes[]">
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Longitude
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" class="" name="address_longitudes[]">
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Address Status
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="address_status[]" id="address_status_id_0" value="">
                                       <option value="1">Active</option>
                                       <option value="0">In Active</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <div id="map_canvas_1"></div>
                                 <div class="input_fields_wrap"></div>
                              </div>
                           </div>
                           <?php if (isset($customer)) : ?>
                              <?php foreach ($customer_addresses as $key => $address) : ?>
                                 <input type="hidden" name="customer_address[]" value="JUST FOR BACKEND" />
                                 <input type="hidden" name="customer_address_ids[]" value="<?= $address->customer_address_id; ?>" />
                                 <div class="col-sm-4" style="border: 1px solid #ccc">
                                    <div class="col-sm-6 p-0 mb-3 pr-2">
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <p>
                                             Zone&nbsp;
                                          </p>
                                          <select name="address_zone_ids[]" id='address_zone_id_<?= $key ?>' style="width:100%;">
                                             <option value="">-- Select zone --</option>
                                             <?php foreach ($zones as $zone) : ?>
                                                <option value="<?= $zone->zone_id ?>" <?= ($address->zone_id == $zone->zone_id) ? "selected" : "" ?>><?= $zone->zone_name ?></option>
                                             <?php endforeach; ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-sm-6 p-0 mb-3">
                                       <p>
                                          Area&nbsp;
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <select name="address_area_ids[]" id="address_area_id_<?= $key ?>" style="width:100%;">
                                             <option value="">-- Select area --</option>
                                             <?php foreach ($areas as $area) : ?>
                                                <option value="<?= $area->area_id ?>" <?= ($address->area_id == $area->area_id) ? "selected" : "" ?>><?= $area->area_name ?></option>
                                             <?php endforeach; ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Location&nbsp;
                                          <rf>
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <select name="address_location_ids[]" id="address_location_id_<? $key ?>" style="width:100%;">
                                             <option value="">-- Select location --</option>
                                             <?php foreach ($locations as $location) : ?>
                                                <option value="<?= $location->location_id ?>" <?= ($address->location_id == $location->location_id) ? "selected" : "" ?>><?= $location->location_name ?></option>
                                             <?php endforeach; ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>Landmark<rf>
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <select name="address_landmark_ids[]" id="address_landmark_id_<?= $key ?>" style="width:100%;">
                                             <option value="">-- Select landmark --</option>
                                             <?php foreach ($landmarks as $landmark) : ?>
                                                <option value="<?= $landmark->landmark_id ?>" <?= ($address->landmark_id == $landmark->landmark_id) ? "selected" : "" ?>><?= $landmark->landmark_name ?></option>
                                             <?php endforeach; ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Residence Type&nbsp;
                                          <rf>
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <select name="address_residence_type_ids[]" id="address_residence_id_<?= $key ?>">
                                             <option value="">-- Select type --</option>
                                             <?php foreach ($residence_types as $residence_type) : ?>
                                                <option value="<?= $residence_type->residence_type_id ?>" <?= ($address->residence_type == $residence_type->residence_type_id) ? "selected" : "" ?>><?= $residence_type->residence_type ?></option>
                                             <?php endforeach; ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Building&nbsp;
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="text" name="address_building[]" id="address_building_id_<?= $key ?>" value="<?= $address->building; ?>" />
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Unit No&nbsp;
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="text" name="address_unit_nos[]" id="address_unit_nos_id_<?= $key ?>" value="<?= $address->unit_no; ?>" />
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Address&nbsp;
                                          <rf>
                                       </p>
                                       <button type="button" name="add_address[]" id="add_address_id_<?= $key ?>" class="text-right btn btn-suceess" data-action="add-address">Add</button>
                                       <button type="button" name="copy_address[]" id="copy_address_id_<?= $key ?>" class="text-right btn btn-suceess" data-action="copy-address">copy</button>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <textarea type="text" name="address_addresses[]" id="address_addresses_id_<?= $key ?>"><?= $address->customer_address; ?></textarea>
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Makkani Number&nbsp;
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="number" class="" name="address_makkani_numbers[]" value="<?= $address->makkani_number; ?>">
                                       </div>
                                    </div>
                                    <!-- <div class="row m-0 n-field-main">
                                 <p>
                                    Zip Code&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" class="" name="address_zip_codes[]" value="<?= $address->zip_code; ?>">
                                 </div>
                              </div> -->
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Latitude
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="text" class="" name="address_latitudes[]" value="<?= $address->latitude; ?>">
                                       </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                       <p>
                                          Longitude
                                       </p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <input type="text" class="" name="address_longitudes[]" value="<?= $address->longitude; ?>">
                                       </div>
                                    </div>
                                    
                                    <div class="row m-0 n-field-main">
                                       <p>Address Status</p>
                                       <div class="col-sm-12 p-0 n-field-box">
                                          <select name="address_status[]" id="address_status_id_<?= $key ?>">
                                             <option value="1" <?= ($address->address_status == 1) ? "selected" : "" ?>>Active</option>
                                             <option value="0" <?= ($address->address_status == 0) ? "selected" : "" ?>>In Active</option>
                                          </select>
                                       </div>
                                    </div>

                                    <div class="row m-0 n-field-main">
                                       <div id="map_canvas_1"></div>
                                       <div class="input_fields_wrap"></div>
                                    </div>
                                 </div>
                              <?php endforeach; ?>
                           <?php else : ?>
                           <?php endif; ?>
                        </div>
                        <div class="col-sm-12 p-3"><button type="button" class="text-right btn btn-suceess" data-action="add-new-address"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add New</button></div>
                        <div class="col-sm-12"><button type="button" class="n-btn" data-action="next">Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button></div>
                     </div>
                     <div class="tab-pane tab-panel" data-panel="prefmaids" style="display:none;">
                        <fieldset>
                           <div class="col-sm-3">
                              <div class="row m-0 n-field-main">
                                 <p>Select Maids</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select class="sel2" style="width:100%" value="<?= implode($pref_maid_ids, ",") ?>" data-searchContain='true' multiple='multiple' name='pref_maids[]' id='pref_maids' autocomplete="off">
                                       <?php foreach ($pref_maids as $maid) : ?>
                                          <option value="<?= $maid->maid_id ?>" <?= in_array($maid->maid_id, $pref_maid_ids) ? 'selected' : '' ?>><?= $maid->maid_name ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                           </div>

                           <div class="col-sm-3">
                              <div class="row m-0 n-field-main">
                                 <p>Flag Maid</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select class="sel2" style="width:100%" id="flaged_maid" name='flaged_maid[]' multiple data-searchContain='true' autocomplete="off">
                                       <?php foreach ($flaged_maids as $flag) : ?>
                                          <option value="<?= $flag->maid_id ?>" <?= in_array($flag->maid_id, $flag_maid_ids) ? 'selected' : '' ?>><?= $flag->maid_name ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                           </div>

                        </fieldset>
                        <div class="col-sm-12"><button type="button" class="n-btn" data-action="next">Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button></div>
                     </div>
                     <div class="tab-pane tab-panel" data-panel="access" style="display:none;">
                        <fieldset>
                           <div class="row m-0 n-field-main">
                              <div class="col-sm-3">
                                 <div class="n-radi-check-main" id="access-holder">
                                    <?php if ($active_accesses) : ?>
                                       <b>Active Access Keys</b>
                                       <hr>
                                       <?php foreach ($active_accesses as $key => $access) : ?>
                                          <input id="access_<?= $access->customer_access_id ?>" type="checkbox" value="<?= $access->customer_access_id ?>" name="access[]" checked disabled>
                                          <label for="access_<?= $access->customer_access_id ?>">

                                             <p><?= $access->code ?></p>
                                          </label>
                                       <?php endforeach; ?>
                                       <br>
                                    <?php endif; ?>
                                    <b>New Access Keys</b>
                                    <hr>
                                 </div>
                              </div>
                           </div>
                           <div class="row m-0 n-field-main">
                              <div class="col-sm-3">
                                 <button type="button" data-action="new-access-popup" class="btn btn-suceess"><i class="fa fa-plus" aria-hidden="true"></i> New Access</button>
                              </div>
                           </div>

                        </fieldset>
                        <div class="col-sm-12"><button type="button" class="n-btn" data-action="next">Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button></div>
                     </div>
                     <div class="tab-pane tab-panel" data-panel="other" style="display:none;">
                        <fieldset>

                           <div class="col-sm-3">
                              <div class="row m-0 n-field-main">
                                 <p>User Name</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" class="" id="username" name="username" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" value="<?= $customer->customer_username; ?>">
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Password</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="password" class="" id="password1" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" value="<?= $customer->customer_password; ?>">
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Flagged Customer ?</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <div class="row m-0">
                                       <div class="col-sm-6 n-radi-check-main p-0">
                                          <input type="radio" value="N" id="is_flagged_n" name="is_flagged" <?= (!isset($customer->is_flag) || $customer->is_flag == "N") ? "checked" : "" ?>>
                                          <label for="is_flagged_n">
                                             <span class="border-radius-3"></span>
                                             <p>No</p>
                                          </label>
                                       </div>
                                       <div class="col-sm-6 n-radi-check-main p-0">
                                          <input type="radio" value="Y" id="is_flagged_y" name="is_flagged" <?= ($customer->is_flag == "Y") ? "checked" : "" ?>>
                                          <label for="is_flagged_y">
                                             <span class="border-radius-3"></span>
                                             <p>Yes</p>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Pets?</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <div class="row m-0">
                                       <div class="col-sm-6 n-radi-check-main p-0">
                                          <input type="radio" value="N" id="pets_no" name="pet" checked onchange="petselection('N')" <?= (!isset($customer->pet) || $customer->pet == "N") ? "checked" : "" ?>>
                                          <label for="pets_no">
                                             <span class="border-radius-3"></span>
                                             <p>No</p>
                                          </label>
                                       </div>
                                       <div class="col-sm-6 n-radi-check-main p-0">
                                          <input type="radio" value="Y" id="pets_yes" name="pet" onchange="petselection('Y')" <?= ($customer->pet == "Y") ? "checked" : "" ?>>
                                          <label for="pets_yes">
                                             <span class="border-radius-3"></span>
                                             <p>Yes</p>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="row m-0 n-field-main" id="pettype" <?= (isset($customer->pet_type)) ? "" : "style='display: none;'" ?>>
                                 <p>Pet Type</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" name="pet_type" id="pet_type" placeholder="Enter pet type" value="<?= isset($customer->pet_type) ? $customer->pet_type : "" ?>">

                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Allow New Booking ?</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <div class="row m-0">
                                       <div class="col-sm-6 n-radi-check-main p-0">
                                          <input type="radio" value="YES" id="is_allowed_new_booking_0" name="is_allowed_new_booking" <?= (!isset($customer->is_allowed_new_booking) || ($customer->is_allowed_new_booking == "YES")) ? "checked" : "" ?>>
                                          <label for="is_allowed_new_booking_0">
                                             <span class="border-radius-3"></span>
                                             <p>Allow</p>
                                          </label>
                                       </div>
                                       <div class="col-sm-6 n-radi-check-main p-0">
                                          <input type="radio" value="BOOKING_BLOCKED" id="is_allowed_new_booking_1" name="is_allowed_new_booking" <?= (isset($customer->is_allowed_new_booking) && $customer->is_allowed_new_booking != "YES") ? "checked" : "" ?>>
                                          <label for="is_allowed_new_booking_1">
                                             <span class="border-radius-3"></span>
                                             <p>Block</p>
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Gender
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="gender_id" id="gender_id" class="sel2" style="width:100%">
                                       <option value="">-- Select gender --</option>
                                       <?php foreach ($genders as $gender) : ?>
                                          <option value="<?= $gender->gender_id ?>" <?= ($customer->gender_id == $gender->gender_id) ? "selected" : "" ?>><?= $gender->gender ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Refer Source
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <select name="refer_source_id" id="refer_source_id" class="sel2" style="width:100%">
                                       <option value="">-- Select source --</option>
                                       <?php foreach ($refer_sources as $source) : ?>
                                          <option value="<?= $source->refer_source_id ?>" <?= ($customer->refer_source_id == $source->refer_source_id) ? "selected" : "" ?>><?= $source->refer_source ?></option>
                                       <?php endforeach; ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Source Reference Note
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <textarea type="text" class="" id="refer_source_note" name="refer_source_note" autocomplete="off"><?= $customer->refer_source_note; ?></textarea>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Booking Notes</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <textarea class="" rows="5" id="booking_note" name="booking_note"><?= $customer->booking_note; ?></textarea>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>Driver Notes</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <textarea class="" rows="5" id="driver_note" name="driver_note"><?= $customer->driver_note; ?></textarea>
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    TRN #
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" id="trnnumber" name="trnnumber" autocomplete="off" value="<?= $customer->trnnumber; ?>">
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Fax&nbsp;
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" id="fax" name="fax" autocomplete="off" value="<?= $customer->fax_number; ?>">
                                 </div>
                              </div>

                              <div class="row m-0 n-field-main">
                                 <p>Website URL</p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" class="" id="website_url" name="website_url" autocomplete="off" value="<?= $customer->website_url; ?>">
                                 </div>
                              </div>
                              <div class="row m-0 n-field-main">
                                 <p>
                                    Notes
                                 </p>
                                 <div class="col-sm-12 p-0 n-field-box">
                                    <textarea class="" rows="5" id="notes" name="notes"><?= $customer->customer_notes; ?></textarea>
                                 </div>
                              </div>
                           </div>
                        </fieldset>
                     </div>

                  </div>
                  <div class="col-sm-12">
                     <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                     <button type="button" class="n-btn" id="customer-save-btn"><?= isset($customer) ? "Update" : "Save" ?></button>
                  </div>
               </form>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script>
   $(function() {
      let current = window.location.href;
      $('#primary_nav_wrap li a').each(function() {
         var $this = $(this);
         // if the current path is like this link, make it active
         if ($this.attr('href') === _base_url + 'customer') {
            $this.addClass('active');
         }
      })
   })
   var cropper;

   function showCropper(cropWidth, cropHeight) {
      cropper = new Cropper(image, {
         aspectRatio: cropWidth / cropHeight,
         viewMode: 0,
      });
      fancybox_show('crop-popup', {
         width: 600
      });
   }
   window.addEventListener('DOMContentLoaded', function() {
      var selector = null;
      var selectorWidth = null;
      var selectorHeight = null;
      var image = document.getElementById('image');
      [...document.querySelectorAll('.input-image')].forEach(function(item) {
         item.addEventListener('change', function(e) {
            selector = e.target.getAttribute('data-selector');
            selectorWidth = e.target.getAttribute('data-crop-width');
            selectorHeight = e.target.getAttribute('data-crop-height');
            var files = e.target.files;
            var done = function(url) {
               item.value = '';
               image.src = url;
               showCropper(selectorWidth, selectorHeight);
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
               file = files[0];
               if (URL) {
                  done(URL.createObjectURL(file));
               } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function(e) {
                     done(reader.result);
                  };
                  reader.readAsDataURL(file);
               }
            }
         });
      });
      document.getElementById('crop').addEventListener('click', function() {
         var canvas;
         if (cropper) {
            canvas = cropper.getCroppedCanvas({
               width: selectorWidth,
               height: selectorHeight,
            });
            var src = canvas.toDataURL();
            document.getElementById(selector + "_base64").value = src; // reflect changed value
            document.getElementById(selector + "_show").src = src; // show changed image
         }
         closeCropper();
      });
   });

   function closeCropper() {
      cropper.destroy();
      cropper = null;
      $.fancybox.close();
   }
   // $("#address_zone_id_0").on('change', function(){
   //    var zoneId = $(this).val();
   //    $.ajax({
   //       url: '<?php echo base_url("customer/getarea"); ?>',
   //       type: 'POST',
   //       data: {zoneId: zoneId},
   //       dataType: 'json',
   //       success: function(response) {
   //          // Clear the previous options
   //          $('#address_area_id_0').html('<option value="">-- Select area --</option>')

   //          // Add the new options based on the response
   //          $.each(response, function(index, area) {

   //             $('#address_area_id_0').append('<option value="' + area.area_id + '">' + area.area_name + '</option>');

   //          });
   //       }
   //    });
   // })

   $("#address_area_id_0").on('change', function() {
      var area_id = $(this).val();
      $.ajax({
         url: '<?php echo base_url("customer/getlocation"); ?>',
         type: 'POST',
         data: {
            area_id: area_id
         },
         dataType: 'json',
         success: function(response) {
            // Clear the previous options
            $('#address_location_id_0').html('<option value="">-- Select location --</option>');


            // Add the new options based on the response
            $.each(response, function(index, location) {

               $('#address_location_id_0').append('<option value="' + location.location_id + '">' + location.location_name + '</option>');

            });
         }
      });
   })

   $("#address_location_id_0").on('change', function() {
      var location_id = $(this).val();
      $.ajax({
         url: '<?php echo base_url("customer/getlandmark"); ?>',
         type: 'POST',
         data: {
            location_id: location_id
         },
         dataType: 'json',
         success: function(response) {
            // Clear the previous options
            $('#address_landmark_id_0').empty();

            // Add the new options based on the response
            $.each(response, function(index, landmark) {
               $('#address_landmark_id_0').append('<option value="' + landmark.landmark_id + '">' + landmark.landmark_name + '</option>');
            });
         }
      });
   })
</script>