<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
          <ul>
            <li><i class="icon-th-list"></i>
              <h3>Customers</h3>
            </li>

            <li><select style="width:160px;" name="filter_customer_type" id="filter_customer_type">
                <option value="" selected>-- Customer Type --</option>
                <option value="1" <?php echo $ctype == 1 ? 'selected="selected"' : ''; ?>>Individual</option>
                <option value="2" <?php echo $ctype == 2 ? 'selected="selected"' : ''; ?>>Company</option>
                <option value="3" <?php echo $ctype == 3 ? 'selected="selected"' : ''; ?>>Dealer</option>
              </select>
            </li>
            <li><select style="width:160px;" name="filter_payment_type">
                <option value="" selected>-- Payment Type --</option>
                <option value="1" <?php echo $ptype == 1 ? 'selected="selected"' : ''; ?>>Cash</option>
                <option value="2" <?php echo $ptype == 2 ? 'selected="selected"' : ''; ?>>Card</option>
              </select>
            </li>
            <li><select style="width:160px;" name="filter_zone_type" id="filter_zone_type">
                <option value="" selected>-- Zone --</option>
                <?php foreach ($zones as $zones) : ?>
                  <option value="<?php echo $zones->zone_id; ?>" <?php if ($ztype == $zones->zone_id) echo 'selected'; ?>><?php echo $zones->zone_name; ?></option>
                <?php endforeach; ?>
              </select>
            </li>
            <li><select style="width:160px;" name="filter_area_type" id="filter_area_type">
                <option value="" selected>-- Area --</option>
                <?php foreach ($areas as $areas) : ?>
                  <option value="<?php echo $areas->area_id; ?>" <?php if ($atype == $areas->area_id) echo 'selected'; ?>><?php echo $areas->area_name; ?></option>
                <?php endforeach; ?>
              </select>
            </li>
            <li><select style="width:160px;" name="filter_location_type">
                <option value="" selected>-- Location --</option>
                <?php foreach ($locations as $locations) : ?>
                  <option value="<?php echo $locations->location_id; ?>" <?php if ($ltype == $locations->location_id) echo 'selected'; ?>><?php echo $locations->location_name; ?></option>
                <?php endforeach; ?>
              </select>
            </li>
            <li><select style="width:160px;" name="filter_status">
                <option value="<?=$status?>" selected>-- Status --</option>
                <option value="1">Active</option>
                <option value="2">Inactive</option>
              </select>
            </li>
            <li>
              <input type="submit" class="btn" value="Go" name="customer_filter" style="margin-bottom: 4px;">
            </li>

            <li class="mr-0 float-right">

              <div class="topiconnew border-0 green-btn">
                <a href="<?php echo base_url('customer/new'); ?>" title="New Customer"> <i class="fa fa-plus"></i></a>
              </div>

            </li>
          </ul>
        </form>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="package-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th style="width:45px">
                <center>Sl. No.</center>
              </th>
              <th>
                Code
              </th>
              <th>
                Name
              </th>
              <th>
                Mobile
              </th>
              <th>
                Email
              </th>
              <th style="line-height: 20px; width: 60px;">
                Zone
              </th>
              <th>
                Area
              </th>
              <th>
                Default Booking Location
              </th>
              <th>
                Source
              </th>
              <th>
                Type
              </th>
              <th>
                Pay Frequency
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($rows) > 0) {
              foreach ($rows as $key => $row) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo $row->customer_code ?: "-" ?></td>
                  <td><?php echo $row->customer_name ?></td>
                  <td><?php echo $row->mobile_number_1 ?></td>
                  <td><?php echo $row->email_address ?: '-' ?></td>
                  <td><?php
                    echo $row->zone_name ? : '-';?>
                  </td>
                  <td><?php
                    echo $row->area_name ? : '-'; ?>
                  </td>
                  <td><?php echo $row->default_booking_address_location ?></td>
                  <td><?php echo $row->refer_source ?: "-" ?></td>
                  <td><?php echo $row->customer_type ?></td>
                  <td><?php echo $row->payment_frequency ?></td>
                  <td style="line-height: 18px; width: 158px" class="td-actions">
                    <center>
                      <a class="n-btn-icon blue-btn" href="<?php echo base_url('customer/view/' . $row->customer_id); ?>" title="View"><i class="btn-icon-only fa fa-eye"> </i></a>
                      <?php if ($package['maid_status'] != 2) { ?>
                        <a class="n-btn-icon purple-btn" href="<?php echo base_url('customer/edit/' . $row->customer_id); ?>" title="Edit"><i class="btn-icon-only icon-pencil"> </i></a>
                      <?php } ?>
                      <?php if ($package['maid_status'] == 2) { ?>
                        <p class="btn btn-danger" style="cursor:default;" title="Deleted"><i class="fa fa-ban"></i></p>
                      <?php } ?>
                      <?php
                      //if(user_authenticate() == 1)
                      if ($this->session->userdata('user_logged_in')['user_admin'] == 'Y') {
                        if ($package['status'] == "Active") {
                      ?>
                          <a href="javascript:void(0)" class="n-btn-icon green-btn" title="Disable" onclick="confirm_disable_enable_modal(<?php echo $package['package_id'] ?>,'<?= $package['status'] ?>');"><i class="btn-icon-only fa fa-toggle-on"> </i></a>
                        <?php
                        } else if ($package['status'] == "Inactive") {
                        ?>
                          <a href="javascript:void(0)" class="n-btn-icon red-btn" title="Enable" onclick="confirm_disable_enable_modal(<?php echo $package['package_id'] ?>,'<?= $package['status'] ?>');"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                        <?php
                        }
                        //                                         if ($package['maid_status']!=2) {
                        ?>

                        <!--                                        <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="change_status(<?php echo $package['maid_id'] ?>,'2');" title="Delete"><i class="btn-icon-only icon-trash"></i> </a>    -->

                      <?php
                        //                                         }
                      }
                      ?>
                    <?php if ($row->deleted_at == null) { ?>
                      <a class="n-btn-icon btn-danger" href="#" data-id="<?= $row->id ?>" onclick="delete_customer(<?php echo $row->customer_id ?>);" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                  <?php } ?>
                    </center>
                  </td>
                </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
    <div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Customer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this Customer ?</h3>
      <input type="hidden" id="customer_id">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
  function statusFilter(status) {
    if (status) window.open("package?status=" + status, "_self")
    else window.open("package", "_self")
  }

  function confirm_disable_enable_modal(id, status) {
    if (status == 'Active') {
      $('#disable_id').val(id);
      $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        autoSize: false,
        width: 450,
        height: 'auto',
        helpers: {
          overlay: {
            css: {
              'background': 'rgba(0, 0, 0, 0.3)'
            },
            closeClick: false
          }
        },
        padding: 0,
        closeBtn: false,
        content: $('#disable-popup'),
      });
    } else {
      $('#enable_id').val(id);
      $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'none',
        openSpeed: 1,
        autoSize: false,
        width: 450,
        height: 'auto',
        helpers: {
          overlay: {
            css: {
              'background': 'rgba(0, 0, 0, 0.3)'
            },
            closeClick: false
          }
        },
        padding: 0,
        closeBtn: false,
        content: $('#enable-popup'),
      });
    }
  }

  function confirm_disable() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    status = params.status ? params.status : ''; // "some_value"
    $('body').append($('<form/>').attr({
      'action': _base_url + "package/disable?status=" + status,
      'method': 'post',
      'id': 'form_asdf'
    }).append($('<input/>').attr({
      'type': 'hidden',
      'name': 'id',
      'value': $('#disable_id').val()
    }))).find('#form_asdf').submit();
  }

  function confirm_enable() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    status = params.status ? params.status : ''; // "some_value"
    $('body').append($('<form/>').attr({
      'action': _base_url + "package/enable?status=" + status,
      'method': 'post',
      'id': 'form_asdf'
    }).append($('<input/>').attr({
      'type': 'hidden',
      'name': 'id',
      'value': $('#enable_id').val()
    }))).find('#form_asdf').submit();
  }

  function closeFancy() {
    $.fancybox.close();
  }
  (function(a) {
    a(document).ready(function(b) {
      if (a('#package-list-table').length > 0) {
        a("table#package-list-table").dataTable({
          'sPaginationType': "full_numbers",
          "bSort": true,
          "iDisplayLength": 100,
          "scrollY": true,
          "orderMulti": false,
          "scrollX": true,
          'columnDefs': [{
            'targets': [-1],
            'orderable': false
          }, ]
        });
      }
    });

  })(jQuery);

  // $(document).ready(function() {
  //     var table = $("#package-list-table").DataTable({
  //       'sPaginationType': "full_numbers",
  //       "bSort": true,
  //       "iDisplayLength": 100,
  //       "scrollY": true,
  //       "orderMulti": false,
  //       "scrollX": true,
  //       'columnDefs': [{
  //         'targets': [-1],
  //         'orderable': false
  //       }]
  //     });


  //     $("select[name='filter_customer_typee']").on("change", function() {
  //       var selectedValue = $(this).val();

  //       table.column(7).search(selectedValue).draw();
  //     });
  //   });

  $(document).on('change', '#filter_customer_typee', function() {
    var type = $(this).val();
    if (type != "") {
      window.location = _base_url + 'customer/list/' + type;
    } else {
      window.location = _base_url + 'customer/list';
    }
  });

  $("#filter_zone_type").on('change', function () {
    $.ajax({
      url: _base_url + 'customer/getarea',
      type: 'POST',
      data: { zoneId: $(this).val() },
      dataType: 'json',
      success: function (response) {
        $('#filter_area_type').html('<option value="">-- Area --</option>');
        // Add the new options based on the response
        $.each(response, function (index, area) {

          $('#filter_area_type').append('<option value="' + area.area_id + '">' + area.area_name + '</option>');
        });

      }
    });
});

  function delete_customer(customer_id) {
    $('#customer_id').val(customer_id);
    fancybox_show('delete-popup', {
      width: 450
    });
  }

  function confirm_delete() {
        var customer_id = $('#customer_id').val();
        $.ajax({
            type: "POST",
            url: _base_url + "customer/delete_customer",
            data: {
                customer_id: customer_id
            },
            dataType: "json",
            cache: false,
            success: function(result) {
                if (result.status === "success") {
                  toast('success', result.message);
                    location.reload();
                } else {
                  toast('error', result.message);
                }
            },
            error: function(xhr, status, error) {
                toast('error',"An error occurred while deleting the customer: " + error);
                console.log(xhr);
            }
        });
    }
</script>