<script>
  var service_week_day = <?=date('w', strtotime($service_date))?>;
</script>
<style type="text/css">
.bg-od,.schedule_bubble.od {
  background-color: <?=$settings->color_bg_booking_od;?> !important;
  color: <?=$settings->color_bg_text_booking_od;?> !important;
}
.schedule_bubble.we {
  background-color: <?=$settings->color_bg_booking_we;?> !important;
  color: <?=$settings->color_bg_text_booking_we;?> !important;
}
.bg-bw,.schedule_bubble.bw {
  background-color: <?=$settings->color_bg_booking_bw;?> !important;
  color: <?=$settings->color_bg_text_booking_bw;?> !important;
}
.schedule_bubble.od:after {
    border-color: transparent <?=$settings->color_bg_booking_od;?>;
}
.schedule_bubble.we:after {
    border-color: transparent <?=$settings->color_bg_booking_we;?>;
}
.schedule_bubble.bw:after {
    border-color: transparent <?=$settings->color_bg_booking_bw;?>;
}
</style>
<div id="schedule_reactivate-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="schedule_reactivate" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Reactivate</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want re-activate this booking?</h3>
        <input type="hidden" id="booking_id" name="booking_id">

        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="button" id="schedule_reactivate_btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-one-day-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_one_day_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_employee">
              <input type="checkbox" id="pay_employee" value="Y" class="week_day" name="pay_employee">
              <label for="pay_employee">
                <span class="border-radius-3"></span>
                <p>Pay employee</p>
              </label>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_customer">
              <input type="checkbox" id="pay_customer" value="Y" class="week_day" name="pay_customer">
              <label for="pay_customer">
                <span class="border-radius-3"></span>
                <p>charge customer</p>
              </label>
            </div>
          </div>
        </div>
        <div class="row m-0 n-field-main" id="pay_amount1" style="display:none;">
          <p>Rate</p>
          <div class="col-sm-12 p-0">
          <input type="text" name="rate1" id="rate1">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Note</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-one-day-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-one-day-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_one_day_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <!-- <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div> -->
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_employee1">
              <input type="checkbox" id="pay_employee1" value="Y" class="week_day" name="pay_employee1">
              <label for="pay_employee1">
                <span class="border-radius-3"></span>
                <p>Pay employee</p>
              </label>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_customer1">
              <input type="checkbox" id="pay_customer1" value="Y"  class="week_day" name="pay_customer1">
              <label for="pay_customer1">
                <span class="border-radius-3"></span>
                <p>charge customer</p>
              </label>
            </div>
          </div>
        </div>
        <div class="row m-0 n-field-main" id="pay_amount" style="display:none;">
          <p>Rate</p>
          <div class="col-sm-12 p-0">
          <input type="text" name="rate" id="rate">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Note</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-one-day-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-permeantly-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_permeantly_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel Permanently?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">

        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-permeantly-we-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-permeantly-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_permeantly_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel permanently?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">

        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-permeantly-od-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-date-range-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_date_range_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-date-range-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-date-range-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_date_range_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-date-range-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-one-day-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_one_day_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-one-day-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-one-day-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_one_day_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <!-- <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div> -->
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-one-day-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="bookings-for-dispatch" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="title">Bookings for Dispatch</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <form id="bookings-dispatch-form">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-dispatch-list">



        <div class="row m-0 n-field-main" style="border-bottom: 1px solid #ddd;">
          <div class="col-sm-3 n-field-box pl-0">
            <div class="n-end n-pick-maids-set">
              <input id="check_all" type="checkbox">
              <label for="check_all">
                <span class="border-radius-3"></span>
                <p>Check All</p>
              </label>
            </div>
          </div>
        </div>
        <div class="inner p-2" style="height: 260px;overflow-x:hidden;overflow-y:scroll;">
        </div>
        </div>
        <!-- <label class="radio inline"></label>-->
        <!--<label class="radio inline"><input type="radio" name="same_zone" value="0" checked="checked">
                  All </label><label class="radio inline"><input type="radio" name="same_zone" value="1">
                  Same Zone </label>-->
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0 pop_close">Cancel</button>
      <button type="button"  data-action="dispatch-now" class="n-btn mb-0" style="display:none">Dispatch</button>
    </div>
    </form>
  </div>
</div>
<div class="content-wrapper" style="width: 100%; margin-left: 0px;">
  <div id="schedule-wrapper"><!--style="min-height: px;"-->
    <div id="schedule" class="mt-0" style="width: 1644px; display: block;">
      <div class="scroll-top-fix" style="position: relative;">
        <div id="schedule-top" class="pt-0">
          <div class="row">
            <div class="container" style="width:100%;">
              <div class="book-nav-top">
                <ul id="schedule-menu">
                  <li>
                    <div class="mm-color-box bg-slot-booked bg-we"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Regular</div>
                  </li>
                   <li>
                    <div class="mm-color-box bg-slot-booked bg-od"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;One Time</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-cancelled"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;Cancelled</div>
                  </li>
                  <li>
                    <div class="hed-main-date">
                      <div class="row">
                        <div class="prev_day"><span class="hed-date-left-arrow"></span></div>
                        <div class="hed-date-main-box text-center"><?=date("d/m/Y", strtotime($service_date))?></div>
                        <div class="next_day"><span class="hed-date-right-arrow"></span></div>
                        <div class="hed-date-calender-icon">
                          <input type="hidden" name="date" id="date" value="<?=$service_date?>">
                          <span class="datepicker" data-date="<?=$service_date?>" data-date-format="yyyy-mm-dd"></span>
                        </div>
                      </div>
                    </div>
                  </li>
                   <li class="no-right-margin">
                    <button class="btn btn-info" data-action="show-bookings-for-dispatch" data-dispatch-date="<?=DateTime::createFromFormat('Y-m-d', $service_date)->format('Y-m-d');?>">Dispatch</button>
                  </li>
                   <li class="mr-0 float-left">
                     <div class="topiconnew border-0 green-btn" style="margin-left: 5px;margin-top: -2px;!important">
                        <a href="<?php echo base_url('schedule/day_view'); ?>" title="Grid View"> <i class="fa fa-th"></i></a>
                     </div>
                  </li>
                  <li class="no-right-margin" style="float: right;">
                    &nbsp;
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        
        <!-- <div class="booking-position"></div> -->

        <div class="d-flex justify-content-between top-summary-section">
          <div>Maids : <?php echo count($maids)?> </div>
          <div>Presents : <span id="presents_count"></div>
          <div>Absents : <span id="absents_count"></span></div>
          <div>Training Hours : <span id="training_hours"></span></div>
          <div>Free Hours : <span id="free_hours"></span></div>
          <div>Emergency Leaves : <span id="emergency_leave_count"></span></div>
          <div>Vacations : <span id="vacations"></span></div>
          <div>Holidays : <span id="holidays"></span></div>
          <div>Medical Leaves : <span id="medical_leaves"></span></div>
          <div>Total hours  : <span id="total_hours"></span></div>
          <div>Assigned : <span id="gained_hours"></span></div>
          <div>Unassigned : <span id="lost_hours"></span></div>

          <div class="booking-summary-total-main">
            <div class="row booking-summary-total p-0 m-0">
              <div class="col-sm-6 summary-number-total p-0" id="grand_total"></div>
              <div class="col-sm-6 summary-percentage-total p-0" id="grand_percentage"></div>
            </div>
          </div>

        </div>

        <div class="widget-content">
        <table id="package-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                <center>Sl. No.</center>
              </th>
              <th>
                Customer Code
              </th>
              <th>
                Customer Name
              </th>
              <th>
                Customer Type
              </th>
              <th>
                Service Start Date
              </th>
              <th>
                Booking Type
              </th>
              <th>
                Maid name
              </th>
              <th>
                Time From
              </th>
              <th>
                Time To
              </th>
              <th>
                Total Hours
              </th>
              <th>
                Service Rate
              </th>
              <th>
                Total Amount
              </th>
              <th>
                Zone
              </th>
              <th>
               Area
              </th>
              <th>
                Booking Status
              </th>
              <th class="td-actions" style="line-height: 18px; width: 158px">
                Action
              </th>
            </tr>
          </thead>
          <tbody id="booking_customer">
           
          </tbody>
        </table>
      </div>
          
          
      </div>
    </div>
  </div>
</div>


