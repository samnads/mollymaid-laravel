<div id="reassign-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="re_assign_schedule_form" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Reassign</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to re-assign?</h3>
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="maid_name" name="maid_name">
        <input type="hidden" id="new_maid_name" name="new_maid_name">
        <input type="hidden" id="maid_id" name="maid_id">
        <input type="hidden" id="delete_date_from" name="delete_date_from">
        <input type="hidden" id="delete_date_to" name="delete_date_to">
        <input type="hidden" id="time_from" name="time_from">
        <input type="hidden" id="time_to" name="time_to">
        <!-- <div class="row m-0">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div> -->
        <div class="col-sm-12 p-0">
          <textarea name="reason" placeholder="Please enter reason." id="reason"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="button" id="reassign-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div class="row m-0">
  <div class="col-sm-12">
    <div class="widget">
      <div class="widget-header">
        <div class="book-nav-top">
          <ul>
            <li>
              <i class="icon-user"></i>
              <h3>Reassign suspended schedule</h3>
            </li>
            <div class="clear"></div>
          </ul>
        </div>
      </div>
      <div class="widget-content">
        <form id="suspended_schedule_reassign_form" class="form-horizontal" enctype="multipart/form-data" method="post">
          <input type="hidden" name="booking_id" value="<?= $booking_id?>">
          <input type="hidden" name="maid_id" value="<?= $maid_id?>">
          <input type="hidden" name="maid_name" value="<?= $maid_name?>">
          <div class="col-sm-10 p-0 pb-3">
            <div class="row m-0 input-daterange">
        
              <div class="col-sm-3 n-field-main">
                <p>Date From</p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" autocomplete="off" name="date_from" id="date_from" readonly value="<?= date('d/m/Y', strtotime($delete_from_date)) ?>">
                </div>
              </div>
              <div class="col-sm-3 n-field-main">
                <p>Date To</p>
                <div class="col-sm-12 p-0 n-field-box">
                  <input type="text" autocomplete="off" name="date_to" id="date_to" readonly value="<?= date('d/m/Y', strtotime($delete_to_date)) ?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-10 p-0 pb-3">
            <div class="row m-0">
        
              <div class="col-sm-3 n-field-main">
                <p>Time From</p>
                <select name="time_from" class="sel2" style="width:100%">
                  <option value="">-- Select start time --</option>
                  <?php foreach ($time_slots as $time => $label) : ?>
                    <option value="<?= $time; ?>" <?= isset($time_from) && $time_from == $time ? "selected" : "" ?>><?= $label; ?></option>
                  <?php endforeach; ?>
                </select>
                <!-- <div class="col-sm-12 p-0 n-field-box">
                    <input type="text" autocomplete="off" name="time_from" id="time_from" readonly value="<?= $time_from ?>">
                </div> -->
              </div>
              <div class="col-sm-3 n-field-main">
                <p>Time To</p>
                <select name="time_to" class="sel2" style="width:100%">
                  <?php foreach ($time_slots as $time => $label) : ?>
                    <option value="<?= $time; ?>" <?= isset($time_to) && $time_to == $time ? "selected" : "" ?>><?= $label; ?></option>
                  <?php endforeach; ?>
                </select>
                <!-- <div class="col-sm-12 p-0 n-field-box">
                    <input type="text" autocomplete="off" name="time_to" id="time_to" readonly value="<?=  $time_to ?>">
                </div> -->
              </div>
            </div>
          </div>
          <div class="col-sm-12" id="search">
            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
            <button type="button" class="n-btn" id="schedule-search-btn">Search Maid</button>
          </div>
        </form>
        <div id="maid_search" style="border: medium none !important;"></div>
      </div>
    </div>
  </div>
</div>