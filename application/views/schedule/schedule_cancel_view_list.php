<style type="text/css">
.bg-od,.schedule_bubble.od {
  background-color: <?=$settings->color_bg_booking_od;?> !important;
  color: <?=$settings->color_bg_text_booking_od;?> !important;
}
/* .bg-we,.schedule_bubble.we {
  background-color: <?=$settings->color_bg_booking_we;?> !important;
  color: <?=$settings->color_bg_text_booking_we;?> !important;
} */
.bg-bw,.schedule_bubble.bw {
  background-color: <?=$settings->color_bg_booking_bw;?> !important;
  color: <?=$settings->color_bg_text_booking_bw;?> !important;
}
.schedule_bubble.od:after {
    border-color: transparent <?=$settings->color_bg_booking_od;?>;
}
.schedule_bubble.we:after {
    border-color: transparent <?=$settings->color_bg_booking_we;?>;
}
.schedule_bubble.bw:after {
    border-color: transparent <?=$settings->color_bg_booking_bw;?>;
}
.bg-slot-free {
  background-color: #71797E !important;
  color: #f7fdfd !important;
}
.bg-slot-cancelled {
  background-color: #F9A602 !important;
}
.bg-slot-suspended {
  background-color: #fff48f !important;
  padding: 5px 13px !important;
  color: #294C5A;
}
</style>

<div id="new-booking-form-popup" style="display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">New Booking</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="new_booking_form">
    <input name="booking_type" type="hidden" value="WE">
    <input name="maid_id" type="hidden">
    <input name="service_week_day" type="hidden">
    <input name="booking_id" type="hidden">
    <input name="delete_from_date" type="hidden">
    <input name="delete_to_date" type="hidden">
    <input name="customer_address_id" type="hidden">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-confirm-dispatch-list">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <select name="customer_id" data-placeholder="-- Select Customer --" style="width:100%">
                  </select>
                  <input name="customer_name" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type<rf></p>
                <div class="n-field-box">
                  <select name="service_type_id" class="sel2" data-placeholder="-- Select Service --" style="width:100%">
                    <option value="">-- Select Service Type --</option>
                    <?php foreach ($services as $key => $service): ?>
                      <option value="<?=$service['service_type_id'];?>"><?=$service['service_type_name'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid Name</p>
                <div class="n-field-box">
                  <input name="maid_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time<rf></p>
                <div class="n-field-box">
                  <select name="time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                  <?php
$from_time = DateTime::createFromFormat('H:i:s', "06:30:00");
$to_time = DateTime::createFromFormat('H:i:s', "19:30:00");
$i = 0;
for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
    $time_clone = clone $time;
    $i++;
    echo '<option value="' . $time->format('H:i:s') . '">' . $time->format('h:i A') . '</option>';
}
?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time<rf></p>
                <div class="n-field-box">
                  <select name="time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Start Date<rf></p>
                <div class="n-field-box">
                  <input type="text" name="service_start_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service End Date&nbsp;<span tabindex="0" class="badge badge-primary p-0" role="button" data-toggle="popover" data-trigger="focus" data-title="Service End Date" data-content="Please keep the service end date empty for never ending services or minimum 3 service weeks required.">?</span>
                <div class="n-field-box">
                  <input type="text" name="service_end_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Week</p>
                <div class="n-field-box">
                  <input name="service_week_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-field-box">
              <p>Service Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="cleaning_material_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows" value="10" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Want Cleaning Material ?</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" value="0" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount (<span class="vat_percentage">-</span>%)</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" value="0"readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Total Amount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="taxed_total" value="0" class="text-center no-arrows" readonly>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer new">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
    <div class="modal-footer edit">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="button" id="schedule_re_assign" data-action="" class="n-btn mb-0">Reassign</button>
      <!-- <button type="button" id="schedulte_delete" class="n-btn mb-0 red-btn">Delete</button> -->

    </div>
  </form>
</div>
</div>
<div class="content-wrapper" style="width: 100%; margin-left: 0px;">
  <div id="schedule-wrapper"><!--style="min-height: px;"-->
    <div id="schedule" class="mt-0" style="width: 1896px; display: block;">
      <div class="scroll-top-fix" style="position: relative;">
        <div id="schedule-top" class="pt-0">
          <div class="row">
            <div class="container" style="width:100%;">
              <div class="book-nav-top">
                <ul id="schedule-menu">
                  <li>
                    <div class="mm-color-box bg-slot-booked"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;BOOKED</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-free"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;FREE SLOTS</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-cancelled"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;CANCELLED</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-suspended"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;SUSPENDED</div>
                  </li>
                  <!--<li>
                    <div class="hed-main-date">
                      <div class="row">
                        <div class="prev_day"><span class="hed-date-left-arrow"></span></div>
                        <div class="hed-date-main-box text-center"><?=date("d/m/Y")?></div>
                        <div class="next_day"><span class="hed-date-right-arrow"></span></div>
                        <div class="hed-date-calender-icon">
                          <input type="hidden" name="date" id="date" value="<?=date("Y-m-d")?>">
                          <span class="datepicker" data-date="<?=date("Y-m-d")?>" data-date-format="yyyy-mm-dd"></span>
                        </div>
                      </div>
                    </div>
                  </li>-->
                  <li>
                    <select class="sel2" name="filter_maid_id" style="width:100%">
                      <option value="">-- All Maids --</option>
                      <?php foreach ($maids as $key => $maid): ?>
                      <option value="<?=$maid->maid_id;?>"><?=$maid->maid_name;?></option>
                      <?php endforeach;?>
                    </select>
                  </li>
                  <li>
                    <select class="sel2" name="filter_week_day" style="width:100%">
                    <option value="">-- All Weeks --</option>
                      <?php foreach ($week_days as $key => $week_day): ?>
                          <option value="<?=$week_day->week_day_id;?>"><?=$week_day->week_name;?></option>
                      <?php endforeach;?>
                    </select>
                  </li>
                  <li>
                    <div class="n-field-box">
                      <label class="position-relative">
                        <div class="n-left-position">From</div>
                        <input class="text-right cursor-pointer" type="text" id="filter_start_date" placeholder="Start Date" value="<?=date("d/m/Y", strtotime($filter_start_date))?>" readonly>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="n-field-box">
                      <label class="position-relative">
                        <div class="n-left-position">To</div>
                        <input class="text-right cursor-pointer" type="text" id="filter_end_date" placeholder="Never End" value="<?=$filter_end_date ? date("d/m/Y", strtotime($filter_end_date)) : ''?>" readonly>
                      </label>
                    </div>
                  </li>
                  <li class="mr-0 float-left">
                    <div class="topiconnew border-0 green-btn" style="margin-left: 5px;margin-top: -2px;!important">
                      <a href="<?php echo base_url('schedule/cancel_view'); ?>" title="Grid View"> <i class="fa fa-th"></i></a>
                    </div>
                  </li>
                  <li class="no-right-margin" style="float: right;">
                    &nbsp;
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="booking-position"></div>

        <div class="d-flex justify-content-between top-summary-section">
          <div>Maids : <?php echo count($maids)?> </div>
          <div>Total Cancelled Schedules : <span id="total_cancelled_schedules"></span></div>
          <div>Total Cancelled Schedules Hours: <span id="total_cancelled_schedules_hours"></span></div>
          <div>Maid on leave  : <span id="maid_absent_count"></span></div>
          <div>Cancelled  by Customer  : <span id="by_customer"></span></div>
          <div>Cancelled  by Office : <span id="by_office"></span></div>
          <div>Driver mistake : <span id="by_driver_mistake"></span></div>
          <!-- <div>Assigned : <span id="assigned_hours"></span></div>
          <div>Unassigned : <span id="unassigned_hours"></span></div> -->
          <div>Reassigned : <span id="reassigned_count"></span></div>
          <div>Unassigned : <span id="unassigned_count"></span></div>
        </div>

       
      <div class="book-mid-det-lt-box">
      <div class="widget-content">
          <table id="package-list-table" class="table table-hover da-table" width="100%">
            <thead>
              <tr>
                <th>
                  <center>Sl. No.</center>
                </th>
                <th>
                  Customer Code
                </th>
                <th>
                  Customer Name
                </th>
                <th>
                  Customer Type
                </th>
                <th>
                  Day
                </th>
                <th>
                  Booking Type
                </th>
                <th>
                  Maid name
                </th>
                <th>
                  Time From
                </th>
                <th>
                  Time To
                </th>
                <th>
                  Total Hours
                </th>
                <th>
                  Service Rate
                </th>
                <th>
                  Total Amount
                </th>
                <th>
                  Zone
                </th>
                <th>
                Area
                </th>
                <th>
                  Booking Status
                </th>
              </tr>
            </thead>
            <tbody id="booking_customer">
            
            </tbody>
          </table>
        </div>
          
          
      </div>
       

        
      <div class="clear"></div>
      </div>
    </div>
  </div>
</div>