<style>
    .bg-od {
         background-color: #018e98 !important;
        color: #f7fdfd !important;
    }
    .bg-we {
        background-color: #026269 !important;
        color: #f7fdfd !important;
    }
    .bg-free {
        background-color: #71797E !important;
        color: #f7fdfd !important;
        padding:25px 16px 7px !important;;
    }
    .new-radio-sub-box {width: 20% !important;}
    .new-radio-sub-box .radio { margin: 0px 16px 0px 8px; }
    .color-box { padding:  5px 10px; border-radius: 3px; line-height: 20px; display: inline-block;margin: 5px 5px 0px 5px;}
    .table th, .table td { line-height:  normal !important ;}
    .widget-header th h3{color: #fff; display: inline-block; font-size: 14px; font-weight: 800; line-height: 25px; text-shadow: 1px 1px 2px rgba(255, 255, 255, 0.5); left: 0px; position: static !important; margin-right: 0;}
</style>

<table class="table table-striped table-bordered">
    <thead>
        <tr class="widget-header">
            <th><center> <h3 style="top: 10px !important;">Sl.No.</h3></center> </th>
            <th> <center><h3 style="top: 10px !important;">Maid</h3></center></th>
            <th> <center><h3 style="top: 10px !important;">Free Slot</h3></center></th>
            <th> <center><h3 style="top: 10px !important;">Booking shift</h3></center></th>
            <th colspan="3"> <center><h3 style="top: 10px !important;">Action</h3></center></th>
        </tr>
    </thead>                                               
    <tbody>
        <tr>                                    
            <td></td>
            
            <td></td>
        </tr>   
        <?php     
        if (!empty($free_maid_dtls)) { 
            $maid_slots = []; 
            $service_date = new DateTime($service_date);
            $service_end_date = new DateTime($service_end_date);
            // $interval = new DateInterval('P1D'); // Interval of 1 day
            $dateRange = new DatePeriod($service_date, new DateInterval('P1D'), $service_end_date->modify('+1 day')); // Add one day to include the end date                                      
            $i=1;
            foreach($free_maid_dtls as $list)
            {
                
                if(!in_array($list->maid_id, $leave_maid_ids)) {// Maid Leave
                    $all_time_slots = []; // Array to store all time slots
                    $maid_id = $list->maid_id;
                    $maid_name = $list->maid_name;
                    if (!isset($maid_slots[$maid_id])) {
                        $maid_slots[$maid_id] = [
                            'name' => $maid_name,
                            'slots' => []
                        ];
                    }
                    foreach ($dateRange as $date) {
                        $current_service_date = $date->format('Y-m-d');
            
                        // Retrieve shifts for the current date
                        if ($booking_type == 'OD' || $booking_type == 'WE') {
                            $time = $this->bookings_model->get_shifts_maid_new($current_service_date, $list->maid_id);
                        }
            
                        // Collect time slots
                        if (is_array($time) || $time instanceof Countable) {
                            
                            if (count($time) > 0) {
                                // Check for shifts and book type
                                if (isset($time[0]->shifts) && !empty($time[0]->shifts)) {
                                    $timeRanges2 = explode(",", $time[0]->shifts);
                                    $book_type = explode(",", $time[0]->area_name); // Get book types
                                    foreach ($timeRanges2 as $key => $shift) {
                                        if (isset($book_type[$key])) {
                                            // Here you can use $book_type[$key] along with $shift
                                            $all_time_slots = array_merge($all_time_slots, $timeRanges2);
                                            $all_time_slots1[] = [
                                                'shift' => $shift,
                                                'type' => $book_type[$key] // Store corresponding book type
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                        if (is_array($time) && count($time) > 0) {
                            foreach ($time as $slot) {
                                $maid_slots[$maid_id]['slots'][] = [
                                    'shift' => $slot->shifts,
                                    'type' => $slot->booking_type,
                                    'area_name' => $slot->area_name,
                                    'start_date' => $slot->service_start_date
                                ];
                            }
                        }
                    }
                   
                    ?>
                
                    <tr>
                    <td><?php echo $i ?></td>
                    <td>
                    
                        <span id="name_<?php echo $list->maid_id  ?>"><?php echo $list->maid_name ?></span>
                    </td>
                    <td><?php
                    // if (isset($time[0]->shifts) && !empty($time[0]->shifts)) {
                    //     $timeRanges2 = explode(",", $time[0]->shifts);
                    //     $all_time_slots = array_merge($all_time_slots, $timeRanges2);
                    // } else {
                    //     // Handle case where shifts property is empty or not set
                    //     $timeRanges2 = ""; // Or set to a default value
                    // }
                    // $timeRanges2=explode(",",$timeRanges2);
                    // print_r($timeRanges2);
                    $begin = DateTime::createFromFormat('H:i:s', "08:00:00");
                    $end = DateTime::createFromFormat('H:i:s', "19:59:59");
                    $timeRanges1 = [];
                    while($begin < $end) {
                    
                        $output = $begin->format('h:i A')."-";
                        $begin->modify('+30 minutes');          /** Note, it modifies time by 15 minutes */
                        $output .= $begin->format('h:i A');
                    
                        $timeRanges1[] = $output;
                    }

                    // Array to store overlapping intervals
                    $overlappingIntervals = [];
                    
                    // Compare each interval from the first array with each interval from the second array
                    foreach ($timeRanges1 as $interval1) {
                        // foreach ($timeRanges2 as $interval2) {
                            // Split each interval into start and end times
                            foreach ($all_time_slots as $interval2) {
                                $parts1 = explode('-', $interval1);
                                $parts2 = explode('-', $interval2);
                                if (isset($parts1[0]) && isset($parts2[0])) {
                                    $start1 = DateTime::createFromFormat('h:i A', trim($parts1[0]));
                                    $end1 = DateTime::createFromFormat('h:i A', trim($parts1[1]));
                                    if (isset($parts2[0]) && isset($parts2[1])) {
                                        $start2 = DateTime::createFromFormat('h:i A', trim($parts2[0]));
                                        $end2 = DateTime::createFromFormat('h:i A', trim($parts2[1]));
                
                                        // Check if the intervals overlap
                                        if ($start1 < $end2 && $end1 > $start2) {
                                            // Calculate the overlapping interval
                                            $overlapStart = max($start1, $start2);
                                            $overlapEnd = min($end1, $end2);
                                            $overlappingIntervals[] = $overlapStart->format('h:i A') . '-' . $overlapEnd->format('h:i A');
                                        }
                                    }
                                }
                            }
                        // }
                    }
                    
                    // Array to store free time slots
                    $freeTimeSlots = [];
                
                    // Iterate through overlapping intervals to find free time slots
                    $previousEnd = DateTime::createFromFormat('h:i A', '08:00 AM'); // Initialize with the start of the day
                    foreach ($overlappingIntervals as $overlapInterval) {
                        $parts = explode('-', $overlapInterval);
                        $overlapStart = DateTime::createFromFormat('h:i A', trim($parts[0]));
                        $overlapEnd = DateTime::createFromFormat('h:i A', trim($parts[1]));
                    
                        // Add free time slot if there is a gap between previous end and current overlap start
                        if ($previousEnd < $overlapStart) {
                            $bufferStart = $overlapStart->modify('-30 minutes');
                            if ($previousEnd < $bufferStart) {
                                $freeTimeSlots[] = $previousEnd->format('h:i A') . '-' . $bufferStart->format('h:i A');
                            }
                            // $freeTimeSlots[] = $previousEnd->format('h:i A') . '-' . $overlapStart->format('h:i A');
                        }
                    
                        // Update previous end with current overlap end for next iteration
                        $previousEnd = $overlapEnd->modify('+30 minutes');
                    }
                
                    // If the last overlap doesn't reach the end of the day, add the remaining time as a free time slot
                    if ($previousEnd < DateTime::createFromFormat('h:i A', '07:30 PM')) {
                        $freeTimeSlots[] = $previousEnd->format('h:i A') . '-07:30 PM';
                    }
                    // Print the free time slots
                    foreach ($freeTimeSlots as $freeSlot) {
                        // echo $freeSlot;
                        ?>
                        <span class="color-box bg-free slot2">
                            <?php echo $freeSlot;?>
                        </span>
                        <?php 
                    }
                    ?>
                    </td>
                    <td>
                    <?php
                   
                    if (is_array($book_type) && count($book_type) > 0) {
                        $k = 0; // Initialize $k here
                        foreach ($book_type as $val) {
                            if (count($time) > 0) {
                                $shift_time_shifts = isset($time[0]->shifts) ? $time[0]->shifts : "";
                                $area_area_id = isset($time[0]->area_id) ? $time[0]->area_id : "";
                                $area_name_area_name = isset($time[0]->area_name) ? $time[0]->area_name : "";
                            } else {
                                $shift_time_shifts = "";
                                $area_area_id = "";
                                $area_name_area_name = "";
                            }
                
                            $btype = trim($val);
                            $shift_time = explode(",", $shift_time_shifts);
                            $area = explode(",", $area_area_id);
                            $area_name = explode(",", $area_name_area_name);
                
                            if ($btype == "WE" || $btype == "OD") {
                                
                            // if($btype=="WE")
                            // {
                                ?>    
                                <!-- <span class="color-box bg-we slot2" data-area="<?php echo $area[$k] ?>" >
                                    <?php echo $shift_time[$k];?><br>
                                    <?php echo $area_name[$k];?>
                                </span>  -->

                                <?php
                            }
                       
                            $k++;
                        
                        } 
           
                        foreach ($maid_slots as &$maid_info) {
                            $unique_slots = [];
                            foreach ($maid_info['slots'] as $slot) {
                                // Create a unique key based on shift and start date
                                $key = $slot['shift'] . '|' . $slot['start_date'];
                        
                                if (!isset($unique_slots[$key])) {
                                    $unique_slots[$key] = $slot; // Store unique slot
                                }
                            }
                            $maid_info['slots'] = array_values($unique_slots); // Reset the slots to only unique ones
                        }

                        
                        if (!empty($maid_info['slots'])) {

                            foreach ($maid_info['slots'] as $slot) { 
                                $shifts = explode(', ', $slot['shift']);
                                $types = explode(', ', $slot['type']);
                                $areas = explode(', ', $slot['area_name']);

                                // Display each shift, type, and area
                                foreach ($shifts as $index => $shift) {
                                    // Ensure we don't go out of bounds
                                    $type = isset($types[$index]) ? $types[$index] : '';
                                    $area = isset($areas[$index]) ? $areas[$index] : '';

                                    if (trim($type) === "WE") {
                                        ?>
                                        <span class="color-box bg-we">
                                            <?php echo htmlspecialchars($shift); ?><br><?php echo htmlspecialchars($area); ?>
                                        </span>
                                        <?php
                                    } elseif (trim($type) === "OD") {
                                        ?>
                                        <span class="color-box bg-od">
                                            <?php echo htmlspecialchars($shift); ?><br><?php echo htmlspecialchars($area); ?>
                                        </span>
                                        <?php
                                    }
                                }
                            }
                        } 
                      
                    
                    
                          
                    }
                        
                        
                    ?>
                    </td>
                    <td>
                        <span class="color-box" style='background:#00A642; padding:5px 27px 3px;color:#fff;'>Free</span>
                    </td>
                    <td>
                        <input type='button' class='save-but book_maid_list' id='btn-book-maid_<?php echo $list->maid_id ?>' onclick="create_reassign_maid('<?php echo $list->maid_id ?>', '<?php echo $list->maid_name ?>')"value='Book'/>
                    </td>
                    </tr> 
                    <?php
                    $i++;
                }
        
            }  
        }                                                                                                                                                                                                                                                                            
        else { ?>
            <tr><td colspan="4" style="text-align: center;color: #f00; padding: 20px 0px;font-weight: bold;"><?php echo $message?></td></tr> 
        <?php }
        ?>
            
    </tbody> 
</table>