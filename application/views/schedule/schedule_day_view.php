<script>
  var service_week_day = <?=date('w', strtotime($service_date))?>;
</script>
<style type="text/css">
.bg-od,.schedule_bubble.od {
  background-color: <?=$settings->color_bg_booking_od;?> !important;
  color: <?=$settings->color_bg_text_booking_od;?> !important;
}
.schedule_bubble.we {
  background-color: <?=$settings->color_bg_booking_we;?> !important;
  color: <?=$settings->color_bg_text_booking_we;?> !important;
}
.bg-bw,.schedule_bubble.bw {
  background-color: <?=$settings->color_bg_booking_bw;?> !important;
  color: <?=$settings->color_bg_text_booking_bw;?> !important;
}
.schedule_bubble.od:after {
    border-color: transparent <?=$settings->color_bg_booking_od;?>;
}
.schedule_bubble.we:after {
    border-color: transparent <?=$settings->color_bg_booking_we;?>;
}
.schedule_bubble.bw:after {
    border-color: transparent <?=$settings->color_bg_booking_bw;?>;
}
.bg-slot-cancelled {
  background-color: #F9A602 !important;
  color: #294C5A;
}
.bg-slot-suspended {
  background-color: #fff48f !important;
  padding: 5px 13px !important;
  color: #294C5A;
}
</style>
<div id="cancel-one-day-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_one_day_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_employee">
              <input type="checkbox" id="pay_employee" value="Y" class="week_day" name="pay_employee">
              <label for="pay_employee">
                <span class="border-radius-3"></span>
                <p>Pay employee</p>
              </label>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_customer">
              <input type="checkbox" id="pay_customer" value="Y" class="week_day" name="pay_customer">
              <label for="pay_customer">
                <span class="border-radius-3"></span>
                <p>charge customer</p>
              </label>
            </div>
          </div>
        </div>
        <div class="row m-0 n-field-main" id="pay_amount1" style="display:none;">
          <p>Rate</p>
          <div class="col-sm-12 p-0">
          <input type="text" name="rate1" id="rate1">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Note</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-one-day-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-one-day-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_one_day_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <!-- <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div> -->
        <!-- <div class="row m-0 n-field-main">
          <div class="n-radi-check-main">
            <input type="hidden" value="5" name="pay_employee1">
            <input type="checkbox" id="pay_employee1" value="Y" class="week_day" name="pay_employee1">
            <label for="pay_employee1">
              <span class="border-radius-3"></span>
              <p>Pay employee</p>
            </label>
          </div>
        </div> -->
        <div class="row m-0 n-field-main">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_employee1">
              <input type="checkbox" id="pay_employee1" value="Y" class="week_day" name="pay_employee1">
              <label for="pay_employee1">
                <span class="border-radius-3"></span>
                <p>Pay employee</p>
              </label>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="n-radi-check-main">
              <input type="hidden" value="5" name="pay_customer1">
              <input type="checkbox" id="pay_customer1" value="Y"  class="week_day" name="pay_customer1">
              <label for="pay_customer1">
                <span class="border-radius-3"></span>
                <p>charge customer</p>
              </label>
            </div>
          </div>
        </div>
        <div class="row m-0 n-field-main" id="pay_amount" style="display:none;">
          <p>Rate</p>
          <div class="col-sm-12 p-0">
          <input type="text" name="rate" id="rate">
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Note</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-one-day-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-permeantly-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_permeantly_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel Permanently?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Note</p>
          <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-permeantly-we-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-permeantly-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_permeantly_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel permanently?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Note</p>
          <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-permeantly-od-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-date-range-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_date_range_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-date-range-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-date-range-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_date_range_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-date-range-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-one-day-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_one_day_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-one-day-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-one-day-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_one_day_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <!-- <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div> -->
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-one-day-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-regular-schedule-popup" style="display:none;">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span id="b-maid-name">Cancel</span>
    <span id="b-time-slot"></span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <div class="content">
    <div class="row m-0 n-delete-section-main pl-4 pt-5 pr-4">
      <div class="col-sm-6 pl-0">
        <input type="button" value="Cancel One Day" class="n-delete-set-left n-btn red-btn">
      </div>
      <div class="col-sm-6 pr-0">
        <input type="button" value="Cancel Permanently" class="n-delete-set-right n-btn red-btn de-select">
      </div>
      <div class="n-delete-set-left-cont">
        <form id="cancel_one_schedule" class="form-horizontal" method="post">
          <div class="col-sm-12 p-0 pt-2" style="font-size: 18px;text-align: left;">Are you sure want to cancel it for one day?</div>
          <span id="cancelremarks_book_day" style="color:red; display:none;"></span>
          <div class="row m-0 input-daterange">
            <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Date From</p>
                <div class="n-field-box">
                  <input name="delete_date_from" id="delete_date_from" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-5 pt-2 n-form-set-left">
                <div class="row m-0 n-field-main">
                <p>Date To</p>
                  <div class="n-field-box">
                    <input name="delete_date_to" id="delete_date_to" readonly>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="cancel_remark_day"></textarea>
            <input type="hidden" id="booking_id_day" name="booking_id_day">
          </div>
          <div class="col-sm-6 pl-0 pt-3">
            <input type="button" value="Yes" class="cancel_yes_book_day n-btn green-btn"/>
          </div>
          <div class="col-sm-12 p-0 pt-3"></div>
        </form>
      </div>
      <div class="n-delete-set-right-cont">
        <form id="cancel_permanent_schedule" class="form-horizontal" method="post">
          <div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to cancel it for permanently?</div>
          <span id="cancelremarks_book_day" style="color:red; display:none;"></span>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="cancel_remark_perm"></textarea>
            <input type="hidden" id="booking_id_perment" name="booking_id_perment">
          </div>
          <div class="col-sm-6 pt-3 pl-0">
            <input type="button" value="Yes" class="cancel_yes_book n-btn green-btn"/>
          </div>
          <div class="col-sm-12 p-0 pt-3"></div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="cancel-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_day_schedule" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">

        <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="delete-regular-schedule-popup" style="display:none;">
<div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
  <div class="content">
    <div class="row m-0 n-delete-section-main pl-4 pt-5 pr-4">
      <div class="col-sm-6 pl-0">
        <input type="button" value="Suspend One Day" class="n-delete-set-left n-btn red-btn">
      </div>
      <div class="col-sm-6 pr-0">
        <input type="button" value="Suspend Permanently" class="n-delete-set-right n-btn red-btn de-select">
      </div>
      <div class="n-delete-set-left-cont">
        <form id="delete_one_schedule" class="form-horizontal" method="post">
          <div class="col-sm-12 p-0 pt-2" style="font-size: 18px;text-align: left;">Are you sure want to suspend it for one day?</div>
          <span id="deleteremarks_book_day" style="color:red; display:none;"></span>
          <div class="row m-0 input-daterange">
            <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Date From</p>
                <div class="n-field-box">
                  <input name="delete_date_from" id="delete_date_from" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-5 pt-2 n-form-set-left">
                <div class="row m-0 n-field-main">
                <p>Date To</p>
                  <div class="n-field-box">
                    <input name="delete_date_to" id="delete_date_to" readonly>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="delete_remark_day"></textarea>
            <input type="hidden" id="booking_id_day" name="booking_id_day">
          </div>
          <div class="col-sm-6 pl-0 pt-3">
            <input type="button" value="Yes" class="delete_yes_book_day n-btn green-btn"/>
          </div>
          <div class="col-sm-12 p-0 pt-3"></div>
        </form>
      </div>
      <div class="n-delete-set-right-cont">
        <form id="delete_permeant_schedule" class="form-horizontal" method="post">
          <div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to suspend it for permanently?</div>
          <span id="deleteremarks_book_day" style="color:red; display:none;"></span>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="delete_remark_perm"></textarea>
            <input type="hidden" id="booking_id_perment" name="booking_id_perment">
          </div>
          <div class="col-sm-6 pl-0 pt-3">
            <input type="button" value="Yes" class="delete_yes_book n-btn green-btn"/>
          </div>
          <div class="col-sm-12 p-0 pt-3"></div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="delete_day_schedule" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">

        <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="delete-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<!-- -->
<div id="od-booking-form-popup" style="display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head bg-od">
    <span class="title">New One Time Schedule</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="od_booking_form">
    <input name="maid_id" type="hidden">
    <input name="service_week_day" type="hidden">
    <input name="booking_id" type="hidden">
    <input name="booking_type" type="hidden" value="OD">
    <input name="service_start_date" type="hidden">
    <input name="service_end_date" type="hidden">
    <input name="customer_address_id" type="hidden">
    <div class="modal-body">
      <div class="controls">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <select name="customer_id" id="b-customer-id" data-placeholder="-- Select Customer --" style="width:100%">
                  </select>
                  <input name="customer_name" readonly>
                  <div id="customer-picked-address"></div>
                </div>
              </div>
            </div>
            <div id="customer-address-panel-user">
              <div class="head">Pick One Address <span class="close">close</span></div>

              <div class="inner">
                      Loading<span class="dots_loader"></span>
              </div>			
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid Name<rf></p>
                <div class="n-field-box">
                  <input name="maid_name" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type<rf></p>
                <div class="n-field-box">
                  <select name="service_type_id" class="sel2" data-placeholder="-- Select Service --" style="width:100%">
                    <option value="">-- Select Service Type --</option>
                    <?php foreach ($services as $key => $service): ?>
                      <option value="<?=$service['service_type_id'];?>"><?=$service['service_type_name'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time<rf></p>
                <div class="n-field-box">
                  <select name="time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                  <?php
$from_time = DateTime::createFromFormat('H:i:s', "07:30:00");
$to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
$i = 0;
for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
    $time_clone = clone $time;
    $i++;
    echo '<option value="' . $time->format('H:i:s') . '">' . $time->format('h:i A') . '</option>';
}
?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time<rf></p>
                <div class="n-field-box">
                  <select name="time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-field-box">
              <p>Service Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-field-box">
              <p>Want Cleaning Material ?</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box" id="material" style="display:none;">
              <p>Cleaning Material</p>
              <div class="row m-0">
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="0" id="material_fixed" name="material_cost" checked>
                  <label for="material_fixed">
                    <span class="border-radius-3"></span>
                    <p>Fixed</p>
                  </label>
                </div>
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="1" id="material_flat" name="material_cost">
                  <label for="material_flat">
                    <span class="border-radius-3"></span>
                    <p>Flat</p>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="cleaning_material_rate_per_hour1" type="number" step="any" autocomplete="off" class="text-center no-arrows" value="10" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" value="0" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount (<span class="vat_percentage">-</span>%)</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" value="0"readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Total Amount<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="taxed_total" value="0" class="text-center no-arrows" readonly>
              </label>
            </div>
            <!-- <div class="col-sm-4 n-field-box">
              <p>Free Service</p>
              <div class="switch-main">
                <label class="switch">
                  <input type="checkbox" name="free_service" id="free-service">
                  <span class="slider round"></span>
                </label>
              </div>
            </div> -->
          </div>
          <div class="col-sm-4 n-field-box">
            <p>Charge type</p>
            <div class="row m-0">
              <div class="col-sm-4 n-radi-check-main p-0">
                <input type="radio" value="0" id="charge_type_n1" name="charge_type1" checked>
                <label for="charge_type_n1">
                  <span class="border-radius-3"></span>
                  <p>Chargable</p>
                </label>
              </div>
              <div class="col-sm-4 n-radi-check-main p-0">
                <input type="radio" value="1" id="charge_type_foc1" name="charge_type1">
                <label for="charge_type_foc1">
                  <span class="border-radius-3"></span>
                  <p>FOC</p>
                </label>
              </div>
              <div class="col-sm-4 n-radi-check-main p-0">
                <input type="radio" value="2" id="charge_type_t1" name="charge_type1">
                <label for="charge_type_t1">
                  <span class="border-radius-3"></span>
                  <p>Traning</p>
                </label>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="modal-footer new">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
    <div class="modal-footer edit">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Update</button>
    </div>
  </form>
</div>
</div>
<div id="new-booking-form-popup" style="display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head bg-we">
    <span class="title">New Booking</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="new_booking_form">
    <input name="booking_id" type="hidden">
    <input name="service_date" type="hidden">
    <input name="day_service_id" type="hidden">
    <input name="day_service_status" type="hidden">
    <input name="booking_type" id="booking_type" type="hidden">
    <input name="service_vat_percentage" type="hidden">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-confirm-dispatch-list">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <select name="customer_id" data-placeholder="-- Select Customer --" style="width:100%">
                  </select>
                  <input name="customer_name" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid Name<rf></p>
                <div class="n-field-box">
                  <input name="maid_name" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type<rf></p>
                <div class="n-field-box">
                  <select name="service_type_id" class="sel2" data-placeholder="-- Select Service --" style="width:100%">
                    <option value="">-- Select Service Type --</option>
                    <?php foreach ($services as $key => $service): ?>
                      <option value="<?=$service['service_type_id'];?>"><?=$service['service_type_name'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time<rf></p>
                <div class="n-field-box">
                  <select name="time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                  <?php
$from_time = DateTime::createFromFormat('H:i:s', "07:30:00");
$to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
$i = 0;
for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
    $time_clone = clone $time;
    $i++;
    echo '<option value="' . $time->format('H:i:s') . '">' . $time->format('h:i A') . '</option>';
}
?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time<rf></p>
                <div class="n-field-box">
                  <select name="time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
          </div>

            <div class="col-sm-3 n-form-set-left">
            </div>
            <div class="col-sm-3 n-form-set-left">
            </div>
            <div class="col-sm-3 n-form-set-left">
            </div>
            <div class="col-sm-3 n-form-set-left">
            </div>


          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-field-box">
              <p>Service Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0 mb-3">
          <div class="col-sm-4 n-field-box">
              <p>Want Cleaning Material ?</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box" id="material" style="display:none;">
              <p>Cleaning Material</p>
              <div class="row m-0">
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="0" id="material_fixed1" name="material_cost1">
                  <label for="material_fixed1">
                    <span class="border-radius-3"></span>
                    <p>Fixed</p>
                  </label>
                </div>
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="1" id="material_flat1" name="material_cost1">
                  <label for="material_flat1">
                    <span class="border-radius-3"></span>
                    <p>Flat</p>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="cleaning_material_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" value="0" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount (<span class="vat_percentage">-</span>%)</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" value="0" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Amount</p>
                <div class="n-field-box">
                  <input name="taxed_total" value="0"readonly>
                </div>
              </div>
            </div>
            <!-- <div class="col-sm-4 n-field-box">
              <p>Free Service</p>
              <div class="switch-main">
                <label class="switch">
                  <input type="checkbox" name="free_service" id="free-service">
                  <span class="slider round"></span>
                </label>
              </div>
            </div> -->
            <div class="col-sm-4 n-field-box">
              <p>Charge type</p>
              <div class="row m-0">
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="0" id="charge_type_n" name="charge_type">
                  <label for="charge_type_n">
                    <span class="border-radius-3"></span>
                    <p>Chargable</p>
                  </label>
                </div>
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="1" id="charge_type_foc" name="charge_type">
                  <label for="charge_type_foc">
                    <span class="border-radius-3"></span>
                    <p>FOC</p>
                  </label>
                </div>
                <div class="col-sm-4 n-radi-check-main p-0">
                  <input type="radio" value="2" id="charge_type_t" name="charge_type">
                  <label for="charge_type_t">
                    <span class="border-radius-3"></span>
                    <p>Traning</p>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer new">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
    <div class="modal-footer edit">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Update Permanently</button>
      <button type="button" id="update_one_day" class="n-btn mb-0">Update One Day</button>
      <!-- <button type="button" id="schedule_suspend_one_day" class="n-btn mb-0 red-btn">Suspend One Day</button> -->
      <button type="button" id="schedule_suspend_date_range" class="n-btn mb-0 red-btn">Suspend Date</button>
      <button type="button" id="cancel_one_day" class="n-btn mb-0 p-2 red-btn">Cancel One Day</button>
      <button type="button" id="cancel_permeantly" class="n-btn mb-0 p-2 red-btn">Cancel Permanently</button>
      <!-- <button type="button" id="schedulte_delete" class="n-btn mb-0 red-btn">Suspend</button> -->
      <!-- <button type="button" id="schedule_cancel" class="n-btn mb-0 red-btn">Cancel</button> -->
    </div>
  </form>
</div>
</div>
<div id="bookings-for-dispatch" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="title">Bookings for Dispatch</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <form id="bookings-dispatch-form">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-dispatch-list">



        <div class="row m-0 n-field-main" style="border-bottom: 1px solid #ddd;">
          <div class="col-sm-3 n-field-box pl-0">
            <div class="n-end n-pick-maids-set">
              <input id="check_all" type="checkbox">
              <label for="check_all">
                <span class="border-radius-3"></span>
                <p>Check All</p>
              </label>
            </div>
          </div>
        </div>
        <div class="inner p-2" style="height: 260px;overflow-x:hidden;overflow-y:scroll;">
        </div>
        </div>
        <!-- <label class="radio inline"></label>-->
        <!--<label class="radio inline"><input type="radio" name="same_zone" value="0" checked="checked">
                  All </label><label class="radio inline"><input type="radio" name="same_zone" value="1">
                  Same Zone </label>-->
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0 pop_close">Cancel</button>
      <button type="button"  data-action="dispatch-now" class="n-btn mb-0" style="display:none">Dispatch</button>
    </div>
    </form>
  </div>
</div>
<div class="content-wrapper" style="width: 100%; margin-left: 0px;">
  <div id="schedule-wrapper"><!--style="min-height: px;"-->
    <!-- <div id="schedule" class="mt-0" style="width: 1896px; display: block;"> -->
    <!-- <div id="schedule" class="mt-0" style="width: 1644px; display: block;"> -->
    <div id="schedule" class="mt-0" style="display: block;">
      <div class="scroll-top-fix" style="position: relative;">
        <div id="schedule-top" class="pt-0">
          <div class="row">
            <div class="container" style="width:100%;">
              <div class="book-nav-top">
                <ul id="schedule-menu">
                  <li>
                    <div class="mm-color-box bg-slot-booked bg-we"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Regular</div>
                  </li>
                   <li>
                    <div class="mm-color-box bg-slot-booked bg-od"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;One Time</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-cancelled"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;Cancelled</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-suspended"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;SUSPENDED</div>
                  </li>
                  <li>
                    <div class="hed-main-date">
                      <div class="row">
                        <div class="prev_day"><span class="hed-date-left-arrow"></span></div>
                        <div class="hed-date-main-box text-center"><?=date("d/m/Y", strtotime($service_date))?></div>
                        <div class="next_day"><span class="hed-date-right-arrow"></span></div>
                        <div class="hed-date-calender-icon">
                          <input type="hidden" name="date" id="date" value="<?=$service_date?>">
                          <span class="datepicker" data-date="<?=$service_date?>" data-date-format="yyyy-mm-dd"></span>
                        </div>
                      </div>
                    </div>
                  </li>
                   <li class="no-right-margin">
                    <button class="btn btn-info" data-action="show-bookings-for-dispatch" data-dispatch-date="<?=DateTime::createFromFormat('Y-m-d', $service_date)->format('Y-m-d');?>">Dispatch</button>
                  </li>
                  <li class="mr-0 float-left">
                     <div class="topiconnew border-0 green-btn" style="margin-left: 5px;margin-top: -2px;!important">
                        <a href="<?php echo base_url('schedule/day_view_list'); ?>" title="List View"> <i class="fa fa-list"></i></a>
                     </div>
                  </li>
                  <!--<li>
                    <select class="sel2" name="filter_maid_id" style="width:100%">
                      <option value="">-- All Maids --</option>
                      <?php foreach ($maids as $key => $maid): ?>
                      <option value="<?=$maid->maid_id;?>"><?=$maid->maid_name;?></option>
                      <?php endforeach;?>
                    </select>
                  </li>-->
                  <li class="no-right-margin" style="float: right;">
                    &nbsp;
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="booking-position"></div>

        <div class="d-flex justify-content-between top-summary-section">
          <div>Maids : <?php echo count($maids)?> </div>
          <div>Presents : <span id="presents_count"></div>
          <div>Absents : <span id="absents_count"></span></div>
          <div>Training Hours : <span id="training_hours"></span></div>
          <div>Free Hours : <span id="free_hours"></span></div>
          <div>Emergency Leaves : <span id="emergency_leave_count"></span></div>
          <div>Vacations : <span id="vacations"></span></div>
          <div>Holidays : <span id="holidays"></span></div>
          <div>Medical Leaves : <span id="medical_leaves"></span></div>
          <div>Total hours  : <span id="total_hours"></span></div>
          <div>Assigned : <span id="gained_hours"></span></div>
          <div>Unassigned : <span id="lost_hours"></span></div>

          <div class="booking-summary-total-main">
            <div class="row booking-summary-total p-0 m-0">
              <div class="col-sm-6 summary-number-total p-0" id="grand_total"></div>
              <div class="col-sm-6 summary-percentage-total p-0" id="grand_percentage"></div>
            </div>
          </div>

        </div>

        <div class="booking-calender-section" id="booking-calender-section">
        <div class="head">Week</div>
        <div id="tb-slide-left" class="time-nav previous-time" title="Previous Time">&nbsp;</div>
        <div id="tb-slide-right" class="time-nav next-time" title="Next Time">&nbsp;</div>
        <!-- <div class="time_line" style="width: 1674px;"> -->
        <div class="time_line" style="width: 1455px;">
          <div class="time_slider">
            <!-- <div style="width: 4820px;"> -->
            <div style="width: 100%;">
              <!-- <div><span>12:00 AM</span></div>
              <div><span>12:30 AM</span></div>
              <div><span>1:00 AM</span></div>
              <div><span>1:30 AM</span></div>
              <div><span>2:00 AM</span></div>
              <div><span>2:30 AM</span></div>
              <div><span>3:00 AM</span></div>
              <div><span>3:30 AM</span></div>
              <div><span>4:00 AM</span></div>
              <div><span>4:30 AM</span></div>
              <div><span>5:00 AM</span></div>
              <div><span>5:30 AM</span></div>
              <div><span>6:00 AM</span></div>
              <div><span>6:30 AM</span></div>
              <div><span>7:00 AM</span></div> -->
              <div><span>7:30 AM</span></div>
              <div><span>8:00 AM</span></div>
              <div><span>8:30 AM</span></div>
              <div><span>9:00 AM</span></div>
              <div><span>9:30 AM</span></div>
              <div><span>10:00 AM</span></div>
              <div><span>10:30 AM</span></div>
              <div><span>11:00 AM</span></div>
              <div><span>11:30 AM</span></div>
              <div><span>12:00 PM</span></div>
              <div><span>12:30 PM</span></div>
              <div><span>1:00 PM</span></div>
              <div><span>1:30 PM</span></div>
              <div><span>2:00 PM</span></div>
              <div><span>2:30 PM</span></div>
              <div><span>3:00 PM</span></div>
              <div><span>3:30 PM</span></div>
              <div><span>4:00 PM</span></div>
              <div><span>4:30 PM</span></div>
              <div><span>5:00 PM</span></div>
              <div><span>5:30 PM</span></div>
              <div><span>6:00 PM</span></div>
              <div><span>6:30 PM</span></div>
              <div><span>7:00 PM</span></div>
              <div><span>7:30 PM</span></div>
              <!-- <div><span>8:00 PM</span></div>
              <div><span>8:30 PM</span></div>
              <div><span>9:00 PM</span></div>
              <div><span>9:30 PM</span></div>
              <div><span>10:00 PM</span></div>
              <div><span>10:30 PM</span></div>
              <div><span>11:00 PM</span></div>
              <div><span>11:30 PM</span></div> -->
            </div>
          </div>
        </div>
        <!-- <div id="tb-slide-right" class="time-nav next-time" title="Next Time">&nbsp;</div> -->
        <div class="head-last">Percentage</div>
        <div class="next-time-bg">&nbsp;</div>
        <div class="clear"></div>
        </div>
      </div>
      <div class="book-mid-det-lt-box">
        <div class="maids" id="weeks">
          <?php foreach ($maids as $key => $maid): ?>
            <div class="maid_name_week" data-maid="<?=$maid->maid_id;?>" style="background:#ffdfdf;padding-left:20px;"><i class="fa fa-user" style="color:#787878" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<b><?=$maid->maid_name;?></b></div>
          <?php foreach ($week_days as $key => $week_day): ?>
              <div class="maid week" data-id="<?=$week_day->week_day_id;?>" data-maid="<?=$maid->maid_id;?>" data-week-name="<?=$week_day->week_name;?>"><?=strtoupper(substr($week_day->week_name, 0, 3));?></div>
              <?php endforeach;?>
          <?php endforeach;?>
        </div>
        
        <div class="maids booking-summary-box " id="booking-summary-box">

            <div class="maids-weekly-summary">
            <?php foreach ($maids as $key => $maid): ?>
              
              <div class="row maid-name-summary m-0">
                <div class="col-sm-6 summary-number p-0" data-maid-week-id="<?= $maid->maid_id ?>"></div>
                <div class="col-sm-6 summary-percentage p-0" data-maid-id-week-id="<?= $maid->maid_id ?>"></div>
              </div>
            
                <div class="row week-day-summary m-0">
                  <div class="col-sm-6 summary-number p-0" data-maid-id="<?= $maid->maid_id ?>"></div>
                  <div class="col-sm-6 summary-percentage p-0" data-maid-id-percentage="<?= $maid->maid_id ?>"></div>
                </div>
                
                <?php endforeach;?>
            </div>
        </div>
        <!-- <div class="time_grid" style="background: rgb(249, 246, 241); height: 710px; width: 1706px;"> -->
        <div class="time_grid" style="background: rgb(249, 246, 241); height: 710px; width: 1268px;">
          <div class="grids">
            <div id="schedule-grid-rows">
                <div class="row tb-slider">
                  <?php foreach ($maids as $key => $maid): ?>
                  <div class="maid_name" data-maid="<?=$maid->maid_id;?>" data-leave-maid="<?=$maid->maid_id;?>" style="background:#ffdfdf;"><b>&nbsp;</b></div>
                  <!-- <div class="maid_name" data-maid="<?=$maid->maid_id;?>" data-leave-maid="<?=$maid->maid_id;?>" style="background:#ffdfdf;padding-left:1136px;"><b>&nbsp;</b></div> -->
                  <?php foreach ($week_days as $key => $week_day): ?>
                    <?php
                    $is_on_leave = false;

                    // Check if the maid is on leave based on leave types
                    $leave_type = '';

                    foreach ($leave_maid_types as $typevalue) {
                      if ($typevalue['maid_id'] == $maid->maid_id && $service_week_day == $week_day->week_day_id) {
                        $leave_type = $typevalue['maid_type'];
                        $is_on_leave = true;
                        break; // No need to check further once leave type is found
                      }
                    }
                    switch ($leave_type) {
                      case "leave":
                        echo '<div class="leavetypes slot dayoff" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '"><div class="leaves">Leave</div></div>';
                        $is_on_leave = true;
                        break;
                      case "emergency_leave":
                        echo '<div class="leavetypes slot dayoff emergency_leave" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '"><div class="leaves">Emergency Leaves</div></div>';
                        $is_on_leave = true;
                        break;
                      case "vacations":
                        echo '<div class="leavetypes slot dayoff vacations" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '"><div class="leaves">Vacations</div></div>';
                        $is_on_leave = true;
                        break;
                      case "holidays":
                        echo '<div class="leavetypes slot dayoff holidays" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '"><div class="leaves">Holidays</div></div>';
                        $is_on_leave = true;
                        break;
                      case "medical_leaves":
                        echo '<div class="leavetypes slot dayoff medical_leaves" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '"><div class="leaves">Medical Leaves</div></div>';
                        $is_on_leave = true;
                        break;
                    }
              
    

                    // If the maid is not on leave, render the time slots selector
                      ?>
                      <div class="selector slot ui-selectable slots" data-maid-week="<?=$maid->maid_id . "-" . $week_day->week_day_id;?>" data-maid="<?=$maid->maid_id;?>" data-week="<?=$week_day->week_day_id;?>" data-maid-name="<?=$maid->maid_name;?>">
                          <?php
                          // Generate time slots
                          $from_time = DateTime::createFromFormat('H:i:s', "07:30:00");
                          $to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
                          $i = 0;
                          for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
                              $time_clone = clone $time;
                              $i++;
                              echo '<div class="cell ui-selectee" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '" data-time-from="' . $time->format('H:i:s') . '" data-time-to="' . $time_clone->format('H:i:s') . '"></div>';
                          }
                          ?>
                      </div>
                  <?php endforeach;?>
                  <?php endforeach;?>
                </div>

            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>