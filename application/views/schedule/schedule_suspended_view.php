<style type="text/css">
.bg-od,.schedule_bubble.od {
  background-color: <?=$settings->color_bg_booking_od;?> !important;
  color: <?=$settings->color_bg_text_booking_od;?> !important;
}
/* .bg-we,.schedule_bubble.we {
  background-color: <?=$settings->color_bg_booking_we;?> !important;
  color: <?=$settings->color_bg_text_booking_we;?> !important;
} */
.bg-bw,.schedule_bubble.bw {
  background-color: <?=$settings->color_bg_booking_bw;?> !important;
  color: <?=$settings->color_bg_text_booking_bw;?> !important;
}
.schedule_bubble.od:after {
    border-color: transparent <?=$settings->color_bg_booking_od;?>;
}
.schedule_bubble.we:after {
    border-color: transparent <?=$settings->color_bg_booking_we;?>;
}
.schedule_bubble.bw:after {
    border-color: transparent <?=$settings->color_bg_booking_bw;?>;
}
.bg-slot-free {
  background-color: #71797E !important;
  color: #f7fdfd !important;
}
.bg-slot-cancelled {
  background-color: #F9A602 !important;
}
.bg-slot-suspended {
  background-color: #fff48f !important;
  padding: 5px 13px !important;
  color: #294C5A;
}
</style>
<div id="schedule_reactivate-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="schedule_reactivate" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Reactivate</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want re-activate this booking?</h3>
        <input type="hidden" id="booking_id" name="booking_id">

        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="schedule_reactivate_btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-extend-date-range-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="extend_suspend_date_range_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="extend-suspend-date-range-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-extend-date-range-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="extend_suspend_date_range_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input type="hidden" id="delete_date_range" name="delete_date_range">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="extend-suspend-date-range-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="leave-extend-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="maid_leave_extend" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Leave</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Enter Leave information</h3>
        <input type="hidden" id="maid_id" name="maid_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="booking_type" name="booking_type">
        <input type="hidden" id="booking_type" name="booking_type">
        <input type="hidden" id="delete_from_date1" name="delete_from_date1">
        <input type="hidden" id="delete_to_date1" name="delete_to_date1">
       
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Leave Type</p>
          <div class="col-sm-12 p-0 ">
          <select name="leave_type" id="leave_type" style="margin-bottom: 4px;">
            <option value="1">Full Day</option>
            <option value="2">Half Day</option>
          </select>

          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Leaves</p>
          <div class="col-sm-12 p-0">
          <select name="leave_type_new" id="leave_type_new" style="margin-bottom: 4px;">
          <option value="leave">Leave</option>
          <option  value="emergency_leave">Emergency Leaves</option>
          <option value="vacations">Vacations</option>
          <option value="holidays">Holidays</option>
          <option value="medical_leaves">Medical Leaves</option>
        </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="button" id="leave-extend-btn" class="n-btn mb-0">Leave Extend</button>
      </div>
    </form>
  </div>
</div>
<div id="new-booking-form-popup" style="display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">New Booking</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="new_booking_form">
    <input name="booking_type" id="booking_type" type="hidden">
    <input name="service_date" type="hidden">
    <input name="maid_id" type="hidden">
    <input name="service_week_day" type="hidden">
    <input name="booking_id" type="hidden">
    <input name="delete_from_date" type="hidden">
    <input name="delete_to_date" type="hidden">
    <input name="delete_date_range" type="hidden">
    <input name="customer_address_id" type="hidden">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-confirm-dispatch-list">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer<rf></p>
                <div class="col-sm-12 p-0 n-field-box">
                  <select name="customer_id" data-placeholder="-- Select Customer --" style="width:100%">
                  </select>
                  <input name="customer_name" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type<rf></p>
                <div class="n-field-box">
                  <select name="service_type_id" class="sel2" data-placeholder="-- Select Service --" style="width:100%">
                    <option value="">-- Select Service Type --</option>
                    <?php foreach ($services as $key => $service): ?>
                      <option value="<?=$service['service_type_id'];?>"><?=$service['service_type_name'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid Name</p>
                <div class="n-field-box">
                  <input name="maid_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time<rf></p>
                <div class="n-field-box">
                  <select name="time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                  <?php
$from_time = DateTime::createFromFormat('H:i:s', "07:30:00");
$to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
$i = 0;
for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
    $time_clone = clone $time;
    $i++;
    echo '<option value="' . $time->format('H:i:s') . '">' . $time->format('h:i A') . '</option>';
}
?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time<rf></p>
                <div class="n-field-box">
                  <select name="time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Start Date<rf></p>
                <div class="n-field-box">
                  <input type="text" name="service_start_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service End Date&nbsp;<span tabindex="0" class="badge badge-primary p-0" role="button" data-toggle="popover" data-trigger="focus" data-title="Service End Date" data-content="Please keep the service end date empty for never ending services or minimum 3 service weeks required.">?</span>
                <div class="n-field-box">
                  <input type="text" name="service_end_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Week</p>
                <div class="n-field-box">
                  <input name="service_week_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-field-box">
              <p>Service Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="cleaning_material_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows" value="10" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Want Cleaning Material ?</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" value="0" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount (<span class="vat_percentage">-</span>%)</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" value="0"readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Total Amount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="taxed_total" value="0" class="text-center no-arrows" readonly>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer new">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
    <div class="modal-footer edit">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="button" id="schedule_re_assign" data-action="" class="n-btn mb-0">Reassign</button>
      <button type="button" id="extend_leave" data-action="" class="n-btn mb-0">Leave Extend</button>
      <button type="button" id="extend_suspend" data-action="" class="n-btn mb-0">Suspend Extend</button>
      <button type="button" id="reactivate_suspend" data-action="" class="n-btn mb-0">Reactivate</button>
      <!-- <button type="button" id="schedulte_delete" class="n-btn mb-0 red-btn">Delete</button> -->

    </div>
  </form>
</div>
</div>


<div class="content-wrapper suspended_view_schedule" style="width: 100%; margin-left: 0px;">
  <div id="schedule-wrapper"><!--style="min-height: px;"-->
    <!-- <div id="schedule" class="mt-0" style="width: 1896px; display: block;"> -->
    <div id="schedule" class="mt-0" style="display: block;">
      <div class="scroll-top-fix" style="position: relative;">
        <div id="schedule-top" class="pt-0">
          <div class="row">
            <div class="container" style="width:100%;">
              <div class="book-nav-top">
                <ul id="schedule-menu">
                  <li>
                    <div class="mm-color-box bg-slot-booked"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;BOOKED</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-free"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;FREE SLOTS</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-cancelled"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;CANCELLED</div>
                  </li>
                  <li>
                    <div class="mm-color-box bg-slot-suspended"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp;SUSPENDED</div>
                  </li>
                  <!--<li>
                    <div class="hed-main-date">
                      <div class="row">
                        <div class="prev_day"><span class="hed-date-left-arrow"></span></div>
                        <div class="hed-date-main-box text-center"><?=date("d/m/Y")?></div>
                        <div class="next_day"><span class="hed-date-right-arrow"></span></div>
                        <div class="hed-date-calender-icon">
                          <input type="hidden" name="date" id="date" value="<?=date("Y-m-d")?>">
                          <span class="datepicker" data-date="<?=date("Y-m-d")?>" data-date-format="yyyy-mm-dd"></span>
                        </div>
                      </div>
                    </div>
                  </li>-->
                  <li>
                    <select class="sel2" name="filter_maid_id" style="width:100%">
                      <option value="">-- All Maids --</option>
                      <?php foreach ($maids as $key => $maid): ?>
                      <option value="<?=$maid->maid_id;?>"><?=$maid->maid_name;?></option>
                      <?php endforeach;?>
                    </select>
                  </li>
                  <li>
                    <select class="sel2" name="filter_week_day" style="width:100%">
                    <option value="">-- All Weeks --</option>
                      <?php foreach ($week_days as $key => $week_day): ?>
                          <option value="<?=$week_day->week_day_id;?>"><?=$week_day->week_name;?></option>
                      <?php endforeach;?>
                    </select>
                  </li>
                  <li>
                    <div class="n-field-box">
                      <label class="position-relative">
                        <div class="n-left-position">From</div>
                        <input class="text-right cursor-pointer" type="text" id="filter_start_date" placeholder="Start Date" value="<?=date("d/m/Y", strtotime($filter_start_date))?>" readonly>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="n-field-box">
                      <label class="position-relative">
                        <div class="n-left-position">To</div>
                        <input class="text-right cursor-pointer" type="text" id="filter_end_date" placeholder="Never End" value="<?=$filter_end_date ? date("d/m/Y", strtotime($filter_end_date)) : ''?>" readonly>
                      </label>
                    </div>
                  </li>
                  <li class="mr-0 float-left">
                    <div class="topiconnew border-0 green-btn" style="margin-left: 5px;margin-top: -2px;!important">
                      <a href="<?php echo base_url('schedule/suspended_view_list'); ?>" title="List View"> <i class="fa fa-list"></i></a>
                    </div>
                  </li>
                  <li class="no-right-margin" style="float: right;">
                    &nbsp;
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="booking-position"></div>

        <div class="d-flex justify-content-between top-summary-section">
          <div>Maids : <?php echo count($maids)?> </div>
          <div>Total Suspended Schedules : <span id="total_suspended_schedules"></span></div>
          <div>Maid on leave  : <span id="maid_absent_count"></span></div>
          <div>Suspended by Customer  : <span id="by_customer"></span></div>
          <div>Suspended by Office : <span id="by_office"></span></div>
          <!-- <div>Assigned : <span id="assigned_hours"></span></div>
          <div>Unassigned : <span id="unassigned_hours"></span></div> -->
          <div>Reassigned : <span id="reassigned_count"></span></div>
          <div>Unassigned : <span id="unassigned_count"></span></div>
        </div>

        <div class="booking-calender-section">
        <div class="head">Week</div>
        <div id="tb-slide-left" class="time-nav previous-time" title="Previous Time">&nbsp;</div>
        <div class="suspended_view time_line">
          <div class="time_slider">
            <!-- <div style="width: 4820px;"> -->
            <div style="width: 100%;">
              <!-- <div><span>12:00 AM</span></div>
              <div><span>12:30 AM</span></div>
              <div><span>1:00 AM</span></div>
              <div><span>1:30 AM</span></div>
              <div><span>2:00 AM</span></div>
              <div><span>2:30 AM</span></div>
              <div><span>3:00 AM</span></div>
              <div><span>3:30 AM</span></div>
              <div><span>4:00 AM</span></div>
              <div><span>4:30 AM</span></div>
              <div><span>5:00 AM</span></div>
              <div><span>5:30 AM</span></div>
              <div><span>6:00 AM</span></div>
              <div><span>6:30 AM</span></div>
              <div><span>7:00 AM</span></div> -->
              <div><span>7:30 AM</span></div>
              <div><span>8:00 AM</span></div>
              <div><span>8:30 AM</span></div>
              <div><span>9:00 AM</span></div>
              <div><span>9:30 AM</span></div>
              <div><span>10:00 AM</span></div>
              <div><span>10:30 AM</span></div>
              <div><span>11:00 AM</span></div>
              <div><span>11:30 AM</span></div>
              <div><span>12:00 PM</span></div>
              <div><span>12:30 PM</span></div>
              <div><span>1:00 PM</span></div>
              <div><span>1:30 PM</span></div>
              <div><span>2:00 PM</span></div>
              <div><span>2:30 PM</span></div>
              <div><span>3:00 PM</span></div>
              <div><span>3:30 PM</span></div>
              <div><span>4:00 PM</span></div>
              <div><span>4:30 PM</span></div>
              <div><span>5:00 PM</span></div>
              <div><span>5:30 PM</span></div>
              <div><span>6:00 PM</span></div>
              <div><span>6:30 PM</span></div>
              <div><span>7:00 PM</span></div>
              <div><span>7:30 PM</span></div>
              <!-- <div><span>8:00 PM</span></div>
              <div><span>8:30 PM</span></div>
              <div><span>9:00 PM</span></div>
              <div><span>9:30 PM</span></div>
              <div><span>10:00 PM</span></div>
              <div><span>10:30 PM</span></div>
              <div><span>11:00 PM</span></div>
              <div><span>11:30 PM</span></div> -->
            </div>
          </div>
        </div>
        <div id="tb-slide-right" class="time-nav next-time" title="Next Time">&nbsp;</div>
        <div class="next-time-bg">&nbsp;</div>
        <div class="clear"></div>
        </div>
      </div>
      <div class="book-mid-det-lt-box">
        <div class="maids" id="weeks">
          <?php foreach ($maids as $key => $maid): ?>
            <div class="maid_name_week" data-maid="<?=$maid->maid_id;?>" style="background:#ffdfdf;padding-left:20px;"><i class="fa fa-user" style="color:#787878" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<b><?=$maid->maid_name;?></b></div>
          <?php foreach ($week_days as $key => $week_day): ?>
              <div class="maid week" data-id="<?=$week_day->week_day_id;?>" data-maid="<?=$maid->maid_id;?>" data-week-name="<?=$week_day->week_name;?>"><?=strtoupper(substr($week_day->week_name, 0, 3));?></div>
              <?php endforeach;?>
          <?php endforeach;?>
        </div>


        <!-- <div class="time_grid" style="background: rgb(249, 246, 241); height: 710px; width: 1706px;"> -->

        <div class="suspended_view time_grid">
          <div class="grids">
            <div id="schedule-grid-rows">
                <div class="row tb-slider">
                  <?php foreach ($maids as $key => $maid): ?>
                  <div class="maid_name" data-maid="<?=$maid->maid_id;?>" style="background:#ffdfdf;padding-left:1136px;"><b>&nbsp;</b></div>
                  <?php foreach ($week_days as $key => $week_day): ?>
                  <div class="selector slot ui-selectable slots" data-maid-week="<?=$maid->maid_id . "-" . $week_day->week_day_id;?>" data-maid="<?=$maid->maid_id;?>" data-week="<?=$week_day->week_day_id;?>" data-maid-name="<?=$maid->maid_name;?>">
                    <?php
$from_time = DateTime::createFromFormat('H:i:s', "07:30:00");
$to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
$i = 0;
for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
    $time_clone = clone $time;
    $i++;
    echo '<div class="cell ui-selectee" data-maid="' . $maid->maid_id . '" data-week="' . $week_day->week_day_id . '" data-time-from="' . $time->format('H:i:s') . '" data-time-to="' . $time_clone->modify('+30 minutes')->format('H:i:s') . '"></div>
                      ';
}
?>
                  </div>
                  <?php endforeach;?>
                  <?php endforeach;?>
                </div>

            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>