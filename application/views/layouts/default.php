<!DOCTYPE html>
<html lang="en" class="header">

<head>
    <meta charset="utf-8">
    <title><?php echo isset($page_title) ? html_escape($page_title) : $settings->site_name; ?> - <?=$settings->site_name;?></title>
    <!--<link rel="manifest" href="img/fav/manifest.json">-->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" type="image/x-icon" href="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->site_favicon, 'favicon.ico'); ?>">
    <link href="<?php echo base_url('css/newstyle.css?v=').CSS_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">-->
    <link href="<?php echo base_url('css/font-awesome.css'); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('css/slider.css'); ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('css/animation.css'); ?>" type="text/css">
    <link href="<?php echo base_url('css/style.css?v=').CSS_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/pages/dashboard.css?v=').CSS_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/hm.css?v=').CSS_VERSION; ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('css/jquery.dataTables.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('js/jquery-2.1.0.min.js'); ?>"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/toastr.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('css/jquery.fancybox.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('css/main.css?v=').CSS_VERSION; ?>" />
    <?php
    if (isset($css_files) && is_array($css_files)) {
        foreach ($css_files as $css_file) {
            echo '<link rel="stylesheet" href="' . base_url() . 'css/' . $css_file . '" />';
        }
    }
    $extra_navbar_inner_style = '';
    $extra_subnavbar_inner_style = '';
    $body_background = '';
    if (strtolower($page_title) == 'bookings' || strtolower($page_title) == 'schedule') {
        $extra_navbar_inner_style = 'style="width: 104%;margin-left: -20px;"';
        $extra_subnavbar_inner_style = 'style="margin-top:54px;"';
        $body_background = 'style="background-color:#FFF;"';
    }
    ?>

<?php $this->load->view('includes/settings_js_css'); ?>

</head>

<body <?php echo $body_background; ?>>
    <?php
    if (is_user_loggedin()) {
        ?>
        <header class="header">
            <div class="row header-wrapper no-left-right-margin">

                <div class="col-md-12 col-sm-12 search-wrapper">
  <ul class="header-top">
    <li>
      <div class="logo"> <a href="<?php echo base_url(); ?>"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->website_logo, 'website-logo.png'); ?>" alt=""></a> </div>
    </li>
    
    <li class="text-left">
      <form class="navbar-search">
        <select class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
          <option value="0">Select Customer</option>
        </select>
      </form>
    </li>
    
    <li class="text-left">
      <div class="admin-drp-main">
        <nav id="primary_nav_wrap">
          <ul>
            <li><a href="#" class="e-hed-icon"><i class="fa fa-user"></i> <?php echo user_authenticate_name() ?> </a>
              <ul class="last-ul">
                <li><a href="<?php echo base_url() . 'dashboard/changepassword' ?>">Change
                  Password</a></li>
                <li><a href="<?php echo base_url() . 'logout' ?>">Logout</a></li>
              </ul>
            </li>
            <?php if (user_authenticate() == 1) { ?>
            <li><a href="#" class="e-hed-icon"><i class="fa fa-cogs"></i></a>
              <ul>
                <!--<li><a href="<?php // echo base_url() . 'sms-settings' 
                                                        ?>">SMS Configuration</a></li>
                                                <li><a href="<?php // echo base_url() . 'email-settings' 
                                                        ?>">Email Configuration</a></li>
                                                <li><a href="<?php // echo base_url() . 'tax-settings' 
                                                        ?>">Tax Configuration</a></li>-->
                
                <?php if (user_permission(user_authenticate(), 22)) { ?>
                <li><a href="<?php echo base_url() . 'users'; ?>">Users</a></li>
                <?php } ?>
                <!--<li><a href="javascript:;">Settings</a></li>
                                                <li><a href="javascript:;">Help</a></li>-->
                
              </ul>
            </li>
            <?php } ?>
          </ul>
        </nav>
      </div>
    </li>
    
    <li>
      <div class="mob-menu-icon mob-menu-view-only"><img src="<?php echo base_url(); ?>images/menu.png"></div>
    </li>
  </ul>
</div>





                <div class="col-md-12 col-sm-12 no-left-right-padding menu">
                    <?php $this->load->view('layouts/primary_navigation'); ?>
                </div>

            </div>
            <!--row header-wrapper end-->
        </header>
        <!--header section end-->

        <div class="main fortoppadding" style="min-height: 572px;">
            <div class="main-inner">
                <div class="booking-fl-box full-width"><!--container-->

                    <?php
                    echo $content_body;
                    ?>
                </div>

            </div>

        </div>
        <?php
    } else {
        echo $content_body;
    }
    ?>
    <?php
    if (is_user_loggedin()) {
        ?>
        <div class="footer">
            <div class="footer-inner">
                <div class="container">
                    <div class="row m-0">
                        <div class="col-md-12"><?=$settings->site_footer_copyright_line_html;?></div>

                    </div>

                </div>

            </div>

        </div>
    <?php } ?>


    <!-- Le javascript
        ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->




    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>




    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('js/hm.js?v='.JS_VERSION); ?>"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/js/sample.js"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.fancybox.pack.js'); ?>"></script>

    <script>
        if ($("#editor").length > 0) {
            initSample();

        }
    </script>
    <!----------------------------------------------------------------------------- -->
    <?php
    if (isset($external_js_files) && is_array($external_js_files)) {
        foreach ($external_js_files as $external_js_file) {
            echo '<script src="' . $external_js_file . '"></script>' . "\n";
        }
    }
    if (isset($js_files) && is_array($js_files)) {
        foreach ($js_files as $js_file) {
            echo '<script type="text/javascript" src="' . base_url('js/' . $js_file.'?v=') .JS_VERSION.'"></script>' . "\n";
        }
    }
    ?>
    <div class="mm-loader"></div>
    <script type="text/javascript" src="<?= base_url('js/main.js?v=') .JS_VERSION; ?>"></script>
</body>
</html>