<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo isset($page_title) ? html_escape($page_title) : $settings->site_name; ?> - <?=$settings->site_name;?></title>

    <link rel="icon" type="image/x-icon" href="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->site_favicon, 'favicon.ico'); ?>">
    <!--<link rel="manifest" href="img/fav/manifest.json">-->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?php echo base_url(); ?>css/newstyle.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.flexdatalist.css" type="text/css" />
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">-->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/slider.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/animation.css" type="text/css">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/pages/dashboard.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/hm.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.flexdatalist.js"></script>
    <!--<link href="<?php // echo base_url(); 
                    ?>css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo base_url(); ?>css/dataTables.checkboxes.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>css/select2.min.css" rel="stylesheet" type="text/css" />
    <?php
    if (isset($css_files) && is_array($css_files)) {
        foreach ($css_files as $css_file) {
            echo '<link rel="stylesheet" href="' . base_url() . 'css/' . $css_file . '" />';
        }
    }
    $extra_navbar_inner_style = '';
    $extra_subnavbar_inner_style = '';
    $body_background = '';
    if (strtolower($page_title) == 'bookings' || strtolower($page_title) == 'schedule') {
        $extra_navbar_inner_style = 'style="width: 104%;margin-left: -20px;"';
        $extra_subnavbar_inner_style = 'style="margin-top:54px;"';
        $body_background = 'style="background-color:#FFF;"';
    }
    ?>

<?php $this->load->view('includes/settings_js_css'); ?>

</head>

<body <?php echo $body_background; ?>>
    <?php
    if (is_user_loggedin()) {
    ?>
        <header>
            <div class="row header-wrapper no-left-right-margin">

                <div class="col-md-12 col-sm-12 search-wrapper">


                    <div class="col-md-3 col-sm-12 no-left-right-padding logo-section">
                        <div class="logo"><a href="<?php echo base_url(); ?>"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->website_logo, 'website-logo.png'); ?>" alt=""></a></div>
                        <form class="navbar-search mob-menu-view-only">
                            <select style="margin-top: 6px; width:220px; margin-bottom: 9px;" class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
                                <option value="0">-- Select Customer --</option>
                            </select>
                            <!--<input class="search-query" id="head-search-f" placeholder="Search Customer" autocomplete="off" type="text">-->
                        </form>
                        <div class="clear"></div>
                    </div>

                    <div class="col-md-9 col-sm-6 no-left-right-padding search-section">

                        <div class="admin-drp-main">
                            <nav id="primary_nav_wrap">
                                <ul>
                                    <li><a href="#" class="e-hed-icon"><i class="fa fa-user"></i> <?php echo user_authenticate_name() ?></a>

                                        <ul class="last-ul">
                                            <li><a href="<?php echo base_url() . 'dashboard/changepassword' ?>">Change Password</a></li>
                                            <li><a href="<?php echo base_url() . 'logout' ?>">Logout</a></li>
                                        </ul>

                                    </li>



                                    <?php if (user_authenticate() == 1) { ?>
                                        <li><a href="#" class="e-hed-icon"><i class="fa fa-cogs"></i></a>

                                            <ul>
                                                <!--<li><a href="<? php // echo base_url() . 'sms-settings' 
                                                                    ?>">SMS Configuration</a></li>
                        <li><a href="<? php // echo base_url() . 'email-settings' 
                                        ?>">Email Configuration</a></li>
                        <li><a href="<? php // echo base_url() . 'tax-settings' 
                                        ?>">Tax Configuration</a></li>-->

                                                <?php if (user_permission(user_authenticate(), 22)) { ?><li><a href="<?php echo base_url() . 'users'; ?>">Users</a></li><?php } ?>
                                                <!--<li><a href="javascript:;">Settings</a></li>
                        <li><a href="javascript:;">Help</a></li>-->

                                            </ul>

                                        </li>
                                    <?php } ?>
                                    <li style="width: 25px; padding: 0px;">
                                        <div class="mob-menu-icon mob-menu-view-only"><img src="<?php echo base_url(); ?>images/menu.png"></div><!--logo end-->
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <form class="navbar-search pc-menu-view-only">
                            <!--<input class="search-query" id="head-search-m" placeholder="Search Customer" autocomplete="off" type="text">-->
                            <select style="margin-top: 6px; width:220px; margin-bottom: 9px;" class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
                                <option value="0">-- Select Customer --</option>
                            </select>
                        </form>

                        <div class="clear"></div>
                    </div>

                </div>




                <div class="col-md-12 col-sm-12 no-left-right-padding menu">
                    <nav id="primary_nav_wrap">


                        <ul>
                            <li><a <?php echo isset($dashboard_active) ? 'class="active"' : ''; ?> href="<?php echo base_url() . 'dashboard'; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                            <?php if (user_permission(user_authenticate(), 2)) { ?>
                                <li><a href="<?php echo base_url() . 'booking' ?>" <?php echo isset($bookings_active) ? 'class="active"' : ''; ?>><i class="fa fa-calendar"></i> Schedule &nbsp; <i class="fa fa-caret-down"></i></a>
                                    <ul>
                                        <?php if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'booking' ?>"><i class="fa fa-calendar"></i> Booking</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 2)) { ?><li><a href="<?php echo base_url() . 'booking/approvallist' ?>"><i class="fa fa-calendar"></i> Web Bookings</a></li><?php } ?>
                                        <!--<? php // if(user_permission(user_authenticate(), 2)) {
                                            ?><li><a href="<? php // echo base_url() . 'justmop' 
                                                            ?>"><i class="fa fa-calendar"></i> Upload From Justmop</a></li><? php // } 
                                                                                                                                                                                ?>
                <? php // if(user_permission(user_authenticate(), 2)) {
                ?><li><a href="<? php // echo base_url() . 'justmop/approvallist' 
                                ?>"><i class="fa fa-calendar"></i> Justmop Bookings</a></li><? php // } 
                                                                                                                                                ?>-->
                                        <?php if (user_permission(user_authenticate(), 4)) { ?><li><a href="<?php echo base_url() . 'maid/schedule' ?>"><i class="fa fa-calendar"></i> Maid Schedule</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 4)) { ?><li><a href="<?php echo base_url() . 'booking/new_booking' ?>"><i class="fa fa-calendar"></i> New Booking</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 4)) { ?><li><a href="<?php echo base_url() . 'booking/block_timing' ?>"><i class="fa fa-calendar"></i> Block Timing</a></li><?php } ?>
                                        <li><a href="<?php echo base_url() . 'reports/schedule-report' ?>"><i class="fa fa-calendar"></i> Schedule Report</a></li>
                                        <!--<? php // if(user_permission(user_authenticate(), 4)) {
                                            ?><li><a href="<? php // echo base_url() . 'booking/locations' 
                                                            ?>"><i class="fa fa-map-marker"></i> Booking Locations</a></li><? php // } 
                                                                                                                                                                                ?>-->
                                    </ul>
                                <?php } ?>
                                <!--<? php // if(user_permission(user_authenticate(), 5)) {
                                    ?><!--<li><a href="<? php // echo base_url() . 'activity' 
                                                        ?>"><i class="fa fa-tasks"></i> Plan</a></li><? php // } 
                                                                                                                                                            ?>-->
                                <!--<? php // if(user_permission(user_authenticate(), 5)) {
                                    ?><li><a href="<? php // echo base_url() . 'activity/activitylist' 
                                                    ?>"><i class="fa fa-tasks"></i> Activity</a></li><? php // } 
                                                                                                                                                            ?>-->
                                <li><a <?php echo isset($maps_active) ? 'class="active"' : ''; ?> href="<?php echo base_url() . 'booking/locations' ?>"><i class="fa fa-map-marker"></i> Maps</a></li>
                                <?php if (user_permission(user_authenticate(), 5)) { ?><li><a <?php echo isset($jobs_active) ? 'class="active"' : ''; ?> href="<?php echo base_url() . 'activity/jobs' ?>"><i class="fa fa-tasks"></i> Jobs</a></li><?php } ?>
                                <?php if (user_permission(user_authenticate(), 6)) { ?><li><a href="<?php echo base_url() . 'customers' ?>"><i class="fa fa-user"></i> Customers</a></li><?php } ?>
                                <?php if (user_permission(user_authenticate(), 3)) { ?><li><a href="<?php echo base_url() . 'maids' ?>"><i class="fa fa-group"></i> Maids</a></li><?php } ?>
                                <li><a href="#"><i class="fa fa-cogs"></i> Settings &nbsp; <i class="fa fa-caret-down"></i></a>

                                    <ul>
                                        <?php if (user_permission(user_authenticate(), 7)) { ?><li><a href="<?php echo base_url() . 'zones' ?>">Zones</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 8)) { ?><li><a href="<?php echo base_url() . 'areas' ?>">Areas</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 9)) { ?><li><a href="<?php echo base_url() . 'flats' ?>">Flats</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 10)) { ?><li><a href="<?php echo base_url() . 'tablets' ?>">Tablets</a></li><?php } ?>
                                        <!--<li><a href="<? php // echo base_url() . 'teams' 
                                                            ?>">Teams</a></li>
                        <li><a href="<? php // echo base_url() . 'payment-settings' 
                                        ?>">Payment Settings</a></li>-->
                                        <?php if (user_permission(user_authenticate(), 11)) { ?><li><a href="<?php echo base_url() . 'services' ?>">Services</a></li><?php } ?>
                                        <?php
                                        if (user_authenticate() == 1) {
                                        ?>

                                            <li><a href="<?php echo base_url() . 'users'; ?>"><span>User Management</span></a></li>
                                        <?php } ?>
                                        <li><a href="<?php echo base_url() . 'sms-list-upload'; ?>"><span>SMS List Upload</span></a></li>
                                        <?php
                                        if (user_authenticate() == 1) {
                                        ?>
                                            <li><a href="<?php echo base_url() . 'bulk-sms'; ?>"><span>Bulk SMS</span></a></li>
                                        <?php
                                        }
                                        ?>
                                        <li><a href="<?php echo base_url() . 'email-list-upload'; ?>"><span>Email List Upload</span></a></li>
                                        <?php
                                        if (user_authenticate() == 1) {
                                        ?>
                                            <li><a href="<?php echo base_url() . 'customer/customer_bulk_email' ?>"><span>Bulk Email</span></a></li>
                                        <?php
                                        }
                                        ?>
                                        <li><a href="<?php echo base_url() . 'coupons'; ?>"><span>Coupon Management</span></a></li>
                                        <!--<li><a href="#" target="_blank"><span>Offer Popup</span></a></li>-->
                                        <li>
                                            <a href="javascript:{}" onclick="document.getElementById('fcm_act').submit();">Notification Activate</a>
                                            <form action="" target="_blank" id="fcm_act" method="POST">
                                                <input type="hidden" name="ws" value="ef3ba09609d17359d1ac3d2516d0b7f2" />
                                                <input type="hidden" name="uid" value="<?php echo user_authenticate(); ?>" />
                                            </form>
                                        </li>
                                    </ul>

                                </li>
                                <li><a <?php echo isset($accounts_active) ? 'class="active"' : ''; ?> href="<?php echo base_url() . 'booking/delayed_report' ?>"><i class="fa fa-file-text-o"></i> Delay Report &nbsp; </a>
                                    <!--<ul>
                        <li><a href="<? php // echo base_url() . 'booking/delayed_report' 
                                        ?>">Pending & Delayed</a></li>
                        <li><a href="<? php // echo base_url() . 'booking/delayed_booking' 
                                        ?>">Delayed Booking</a></li>
                        <!--<li><a href="<? php // echo base_url() . 'invoices' 
                                            ?>">Customer Invoices</a></li>
                        <li><a href="<? php // echo base_url() . 'reports/payment' 
                                        ?>">Customer Payments</a></li>
                        <li><a href="<? php // echo base_url() . 'customer_statement' 
                                        ?>">Customer Statement</a></li>-->
                                    <!--<li><a href="<? php // echo base_url() . 'receivable_payments' 
                                                        ?>">Receivable Payments</a></li>-->
                                    <!--<li><a href="<? php // echo base_url() . 'reports/online_payment_report' 
                                                        ?>">Online Payments</a></li>
                    </ul>-->

                                </li>
                                <li><a href="<?php echo base_url() . 'activity' ?>"><i class="fa fa-file-text-o"></i> Plan</a></li>
                                <!--<li><a <? php // echo isset($invoice_active) ? 'class="active"' : ''; 
                                            ?>  href="<? php // echo base_url() . 'invoices' 
                                                        ?>"><i class="fa fa-file-text-o"></i>  Invoice</a></li>-->
                                <li><a href="#"><i class="fa fa-copy"></i> Reports &nbsp; <i class="fa fa-caret-down"></i></a>

                                    <ul class="last-ul">
                                        <?php if (user_permission(user_authenticate(), 13)) { ?><li><a href="<?php echo base_url() . 'reports/zone' ?>">Zone Reports</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 14)) { ?><li><a href="<?php echo base_url() . 'reports/vehicle' ?>">Vehicle Reports</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 14)) { ?><li><a href="<?php echo base_url() . 'reports/newreport' ?>">Driver Reports</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 13)) { ?><li><a href="<?php echo base_url() . 'booking/booking_reports' ?>">Booking Reports</a></li><?php } ?>
                                        <!--<? php // if(user_permission(user_authenticate(), 15)) {
                                            ?><li><a href="<? php // echo base_url() . 'reports/payment' 
                                                            ?>">Payment Reports</a></li><? php // } 
                                                                                                                                                ?>-->

                                        <!--<? php // if(user_permission(user_authenticate(), 23)) {
                                            ?><li><a href="<? php // echo base_url() . 'reports/call-report' 
                                                            ?>">Call Report</a></li><? php // } 
                                                                                                                                            ?>-->

                                        <?php if (user_permission(user_authenticate(), 16)) { ?><li><a href="<?php echo base_url() . 'reports/oneday' ?>">One Day Cancel Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 16)) { ?><li><a href="<?php echo base_url() . 'reports/booking/cancel' ?>">Booking Cancel Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 17)) { ?><li><a href="<?php echo base_url() . 'reports/work' ?>">Work Report Of an Employee</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 17)) { ?><li><a href="<?php echo base_url() . 'reports/work/all' ?>">Employees Work Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 18)) { ?><li><a href="<?php echo base_url() . 'reports/maidattendance' ?>">Maid Attendance Report</a></li><?php } ?>
                                        <!--<? php // if(user_permission(user_authenticate(), 19)) {
                                            ?><li><a href="<? php // echo base_url() . 'reports/driveractivity' 
                                                            ?>">Driver Activity Report</a></li><? php // } 
                                                                                                                                                        ?>-->
                                        <?php if (user_permission(user_authenticate(), 19)) { ?><li><a href="<?php echo base_url() . 'reports/schedule-report' ?>">Driver Activity Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 19)) { ?><li><a href="<?php echo base_url() . 'reports/useractivity' ?>">User Activity Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 20)) { ?><li><a href="<?php echo base_url() . 'reports/activity' ?>">Activity Summary Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 21)) { ?><li><a href="<?php echo base_url() . 'reports/vehicleattendance' ?>">Maid Vehicle Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 21)) { ?><li><a href="<?php echo base_url() . 'reports/zone-wise-booking-report' ?>">Zone Wise Booking Report</a></li><?php } ?>
                                        <?php if (user_permission(user_authenticate(), 21)) { ?><li><a href="<?php echo base_url() . 'reports/maid-leave-report' ?>">Maid Leave Report</a></li><?php } ?>
                                        <li><a href="<?php echo base_url() . 'reports/maidhours' ?>">Maid Hours Report</a></li>
                                        <li><a href="<?php echo base_url() . 'backpayment' ?>">Back Payments</a></li>
                                        <li><a href="<?php echo base_url() . 'rating-review' ?>">Rating & Review</a></li>
                                    </ul>

                                </li>



                        </ul>






                    </nav>
                </div>

            </div><!--row header-wrapper end-->
        </header><!--header section end-->

        <div class="main" style="min-height: 572px;">
            <div class="main-inner">
                <div class="container" style="width: 100%;">

                    <?php
                    echo $content_body;
                    ?>
                </div>

            </div>

        </div>
    <?php
    } else {
        echo $content_body;
    }
    ?>
    <?php
    if (is_user_loggedin()) {
    ?>
        <div class="footer">
            <div class="footer-inner">
                <div class="container">
                    <div class="row">
                        <div class="span12"><?=$settings->site_footer_copyright_line_html;?></div>

                    </div>

                </div>

            </div>

        </div>
    <?php } ?>


    <!-- Le javascript
        ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script language="javascript">
        var _base_url = '<?php echo base_url(); ?>';
        var _page_url = '<?php echo current_url(); ?>';
    </script>
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <!--<script src="<? php // echo base_url(); 
                        ?>js/jquery-ui-1.10.4.custom.min.js"></script>-->

    <script type="text/javascript" src="<?php echo base_url(); ?>js/animation.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/classie.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/hm.js"></script>

    <script type="text/javascript">
        $(function() {
            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>
    <?php
    if (isset($external_js_files) && is_array($external_js_files)) {
        foreach ($external_js_files as $external_js_file) {
            echo '<script src="' . $external_js_file . '"></script>' . "\n";
        }
    }
    if (isset($js_files) && is_array($js_files)) {
        foreach ($js_files as $js_file) {
            echo '<script type="text/javascript" src="' . base_url() . 'js/' . $js_file . '"></script>' . "\n";
        }
    }
    ?>
    <div class="mm-loader"></div>
</body>

</html>