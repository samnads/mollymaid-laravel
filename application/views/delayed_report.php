<style>
    .book-nav-top li{
        margin: 0 10px 0 0 !important;
    }
    .select2-arrow{visibility : hidden;}
    .select2-container .select2-choice{
	-moz-appearance: none;
        background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #ccc;
        border-radius: 3px;
        cursor: pointer;
        font-size: 12px;
        height: 30px;
        line-height: 24px;
        padding: 3px 0 3px 10px;
        text-indent: 0.01px;
    }
    .select2-results li{margin-left: 0px !important;}
/*    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }*/
/*notification*/

.navbar--nav .nav-item, .navbar--search, .sidebar--nav li {
    position: relative;
    z-index: 0;
}


.no-outlines .btn-link, .no-outlines .btn-link:active, .no-outlines .btn-link:focus, .no-outlines .btn-link:hover, .no-outlines .btn-link:link, .no-outlines .btn-link:visited, .no-outlines a, .no-outlines a:active, .no-outlines a:focus, .no-outlines a:hover, .no-outlines a:link, .no-outlines a:visited, .no-outlines button:focus {
    outline: 0 none;
}
.navbar--nav .nav-link {
    padding-left: 30px;
    padding-right: 30px;
}
.navbar--nav .nav-link {
    font-size: 18px;
    line-height: 50px;
    padding: 20px 15px;
}
.nav li a {
    color: inherit;
}

.navbar--nav .nav-link .badge {
    left: 20px;
}
.navbar--nav .nav-link .badge {
    left: 5px;
    position: absolute;
    top: 23px;
}
.badge {
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
    font-size: 14px;
    line-height: 17px;
	position: absolute;
    
    
	 right: -10px;
    top: -10px;
}
.badge, .label {
    font-weight: 400;
}
.bg-blue {
    background-color: #2bb3c0;
}
.text-white {
    color: #fff;
}
.badge {
    border-radius: 50%;
    display: inline-block;
    font-size: 75%;
    font-weight: 700;
    padding: 2px 0 0 0 !important;
	width:23px;
	height:23px;
    text-align: center;
    vertical-align: baseline;
    white-space: nowrap;
}
.nav-item {

    padding-right: 18px;
    padding-top: 20px;
	float: right !important;
}
.nav-link {

    position: relative;
}

.table > tbody > tr > td { border-top: 1px solid #fff;}
.table-bordered td { border-left: 1px solid #fff;}

</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">
        <div class="col-md-12 col-sm-12 no-left-right-padding">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header"> 
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>booking/delayed_report">
                    <div class="book-nav-top">
                        <ul>
                            <li>
                                <i class="icon-th-list"></i>
                                <h3>Pending & Delayed Booking</h3>
                            </li>
                            
                            <li>
                                <input type="text" id="cstatemnet-date-from" name="cstatemnet-date-from" style="width: 160px;" data-date="<?php echo $currentdate; ?>" readonly value="<?php echo $currentdate; ?>" data-date-format="dd/mm/yyyy"/> 
                            </li>
                            
                            <li>
                                <input type="submit" class="n-btn" id="customer-statement-search" value="Search"/>
                            </li>

                          
                          <li class="mr-0 float-right">
                            
                         <div class="topiconnew border-0 green-btn"> <a onclick="download_delay_report()"><i class="fa fa-file-excel-o"></i></a></div>
                            
                         
                         <div class="topiconnew border-0 green-btn n-notify-icon"> <a onclick="#"><i class="fa fa-bell"></i><span><?php echo count($current_booking); ?></span></a></div>   
                         
                         </li>

                            <div class="clear"></div>
                            <!-- Statement ends -->
                        </ul>
                    </div>
                              
                </form>
            </div>
            
            
            
            
            
            
            
            
            
            
            <div id="statement_content" class="">	
                <table id="statement-content-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <!--<th class="no-left-border"></th>-->
                            <th>Sl</th>
                            <th>Customer</th>
                            <th>Maid</th>
                            <th>Booking Time</th>
                            <th>Drop & Pick Time</th>
                            <th>Zone(Driver)</th>
                            <th>Amount</th>
                            <th>Company</th>
                            <th>Location</th>
                            <th>SMS</th>
                        </tr>
                    </thead>
                    <tbody id="invoice-tabtbody1">
                        <?php
                        if(!empty($current_booking))
                        {

                        $sln = 1;
                        foreach($current_booking as $val)
                        {
							$sms_button="";
							$newtime = date('H:i:s',strtotime($val['starttime'] . ' -15 minutes'));
                     
                            $starttime=date('H:i:s',strtotime($val['from_time'] . ' +15 minutes'));
                            $time_after_15=date('h:i a',  strtotime($starttime));
							//echo $currenttimeold.','.$newtime.','.$val['starttime'];
							//echo '<br>';
							if($currenttimeold > $newtime && $currenttimeold < $val['starttime'])
							{
								$style = 'style="background:#78b7ed !important;"';
							} else
							{
								if($val['drop_time'] == "")
								{
									$style = 'style="background:#ffd9d9 !important;"';
                               
								} else {
									if($val['drop_time_new'] > $val['starttime'] && $val['drop_time_new'] < $val['starttimenew'])
									{
										$style = 'style="background:#78b7ed !important;"';
									} else {
										$style = "";
    
									}
								}
							}
                            $sms_button="<a href='javascript:void(0);' onclick='send_delay_sms(\"".$val["mobile_number_1"]."\",\"".$time_after_15."\");' title='Send SMS'><i class='fa fa-mobile' style='font-size:20px;'></i></a>";
                        ?>
                        <tr <?php echo $style; ?>>
                            <td <?php echo $style; ?>><?php echo $sln++; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['customer_name']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['maid_name']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['from_time']; ?> - <?php echo $val['to_time']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['drop_time']; ?><?php echo $val['pick_time']; ?></td>
                            <td <?php echo $style; ?>>
								<?php echo $val['zone_name']; ?> <?php if($val['trans_driver_name'] != ""){ ?>(<?php echo $val['trans_driver_name']; ?>) <?php } ?></td>
                            <td <?php echo $style; ?>><?php echo $val['total_amount']; ?></td>
                            <td <?php echo $style; ?>><?php echo $val['company_name']; ?></td>
							<?php
							if($val['cust_latitude'] != "")
							{
							?>
                            <!--<td <?php// echo $style; ?>><a target="_blank" href="https://www.google.com/maps/?q=<?php// echo $val['tab_latitude']; ?>,<?php// echo $val ['tab_longitude']; ?>"><i class="icon-map-marker"><i></a></td>-->
							<td <?php echo $style; ?>><a target="_blank" href="https://www.google.com/maps/dir/<?php echo $val['tab_latitude']; ?>,<?php echo $val ['tab_longitude']; ?>/<?php echo $val ['cust_latitude']; ?>,<?php echo $val ['cust_longitude']; ?>"><i class="icon-map-marker"></i></a></td>
                         
                            <?php
							} else {
							?>
							<td <?php echo $style; ?>><a target="_blank" href="https://www.google.com/maps/dir/<?php echo $val['tab_latitude']; ?>,<?php echo $val ['tab_longitude']; ?>/<?php echo $val ['customer_address']; ?>"><i class="icon-map-marker"></i></a></td>
							<?php
							}

							?>
                            <td <?php echo $style; ?>><?php echo  $sms_button; ?></td>
							
							<!--<td <?php// echo $style; ?>><a target="_blank" href="<?php// echo base_url(); ?>map?lat=<?php// echo $val['tab_latitude']; ?>&long=<?php// echo $val ['tab_longitude']; ?>"><i class="icon-map-marker"><i></a></td>-->
                        </tr>
                        <?php
                        }
                        ?>
                        
                        <?php
                        }
                        ?>
                    </tbody>
                 </table> 
            </div>
        </div><!--welcome-text-main end-->
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->

<script type="text/javascript">
setTimeout(function(){
   window.location.reload(1);
}, 60000);    
</script>