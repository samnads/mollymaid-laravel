<div id="delete-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Confirm Delete ?</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div class="modal-body">
            <h3>Are you sure you want to delete this staff ?</h3>
            <input type="hidden" id="delete_id">
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
        </div>
    </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget ">
            <div class="widget-header mb-0">
                <ul>
                    <li>
                        <i class="icon-user"></i>
                        <h3>Staff Details</h3>

                    </li>




                    <li class="mr-0 float-right">

                        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>maids" title="Maid List"> <i class="fa fa-users"></i></a> </div>


                        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>employee/edit/<?php echo $link_maid_id; ?>" title="Edit Maid"> <i class="fa fa-pencil"></i></a> </div>

                    </li>
                </ul>


            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">

                    <ul class="nav nav-tabs pb-4">
                        <li>
                            <a data-target="#tab1" data-toggle="tab" data-tab="tab1">Basic Details</a>
                        </li>
                        <li>
                            <a data-target="#tab2" data-toggle="tab" data-tab="tab2">Address</a>
                        </li>
                        <li>
                            <a data-target="#tab3" data-toggle="tab" data-tab="tab3">Skills</a>
                        </li>
                        <li>
                            <a data-target="#tab4" data-toggle="tab" data-tab="tab4">Daily Preference</a>
                        </li>
                        <!-- <li>
                            <a data-target="#tab5" data-toggle="tab" data-tab="tab5">Documents</a>
                        </li> -->
                        <li>
                            <a data-target="#tab6" data-toggle="tab" data-tab="tab6">Complaints</a>
                        </li>

                        <li>
                            <a data-target="#tab7" data-toggle="tab" data-tab="tab7">Pay Employee</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <?php
                        foreach ($maid_details as $maid_val) {
                        ?>
                            <div id="tab1" class="tab-pane active">

                                <table>

                                    <tbody>

                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Name</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_name'] ?: '-'  ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Employee Type</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['employee_type'] ?: '-'  ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Employee Image</b></td>
                                            <td style="line-height: 20px; width: 200px" rowspan="7">
                                                <center><img class="thumbnail" src="<?= check_and_get_img_url('./uploads/images/avatars/employee/' . $maid_val['maid_photo_file'], 'maid-avatar.png'); ?>" style="height: 150px; width: 150px" /></center>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>gender</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['gender'] ?: '-'  ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Team</b></td>
                                            <td style="line-height: 20px; width: 300px"><?php echo $maid_val['team_name'] ?: '-' ?></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Nationality</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['country'] ?: '-'  ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Flat</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['flat_name'] ?: '-' ?></td>

                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Mobile Number 1</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_mobile_1'] ?: '-'  ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Mobile Number 2</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_mobile_2'] ?: '-' ?></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>WhatsApp No.</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_whatsapp_no_1'] ?: '-' ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Priority No.</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_priority'] ?: '-' ?></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Present Address</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_present_address'] ?: '-' ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Permanent Address</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_permanent_address'] ?: '-'  ?></td>

                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Joining Date</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['maid_joining'] ?: '-'  ?></td>


                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Username</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['username'] ?: '-' ?></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Password</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $maid_val['password'] ?: '-' ?></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 20px;" colspan="2"><b>
                                                    <center>Services</center>
                                                    <hr>
                                                </b></td>
                                            <td style="line-height: 20px;" colspan="2"><b>
                                                    <center>Notes</center>
                                                    <hr>
                                                </b></td>
                                        </tr>

                                        <tr>
                                            <td style="line-height: 20px;" rowspan="2" colspan="2">

                                                <?php
                                                if (count($maid_services) > 0) {
                                                    $j = 1;
                                                    foreach ($maid_services as $maid_services_val) {
                                                ?>
                                                        <?= $j++; ?> .&nbsp;
                                                        <?= $maid_services_val['service_type_name'] ?: '-'  ?> &nbsp; <br>

                                                <?php }
                                                } ?>
                                            </td>
                                            <td style="line-height: 20px;" rowspan="2" colspan="2"><?php echo $maid_val['maid_notes'] ?: '-' ?></td>
                                            <td align="center" valign="middle">

                                                <?php
                                                if (($maid_val['maid_status'] != 2) && (user_authenticate() == 1)) {
                                                ?>

                                                    <input class="n-btn red-btn" value="Delete" name="maid_delete" onclick="return confirm_delete_modal(<?php echo $maid_val['maid_id'] ?>);" type="button" style="display: block; float: none; margin: 40% auto !important;">

                                                <?php
                                                }
                                                ?>


                                            </td>
                                        </tr>

                                    </tbody>

                                </table>

                            </div>
                        <?php } ?>

                        <div id="tab2" class="tab-pane">
                            <table>

                                <tbody>
                                    <?php
                                    foreach ($maid_address as $key => $addres) {
                                        $address_grouped[$addres['address_type']][] = $addres;
                                    }

                                    echo "<table style='width:100%;'>";
                                    foreach ($address_grouped as $address_type => $addresses) {
                                        echo "<tr>";
                                        echo "<th colspan='2' style='font-size: 18px; font-weight: bold; text-transform: capitalize; padding: 10px;'>" . getFormattedAddressType($address_type) . "</th>";
                                        echo "</tr>";

                                        foreach ($addresses as $addres) {
                                            echo "<tr>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Street</th><td style='padding: 5px;'>" . ($addres['street'] ? $addres['street'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>House Number</th><td style='padding: 5px;'>" . ($addres['house_number'] ? $addres['house_number'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Country</th><td style='padding: 5px;'>" . ($addres['country'] ? $addres['country'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Mobile</th><td style='padding: 5px;'>" . ($addres['mobile'] ? $addres['mobile'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Whatsapp</th><td style='padding: 5px;'>" . ($addres['whatsapp'] ? $addres['whatsapp'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Town</th><td style='padding: 5px;'>" . ($addres['town'] ? $addres['town'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Relationship</th><td style='padding: 5px;'>" . ($addres['relationship'] ? $addres['relationship'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>City</th><td style='padding: 5px;'>" . ($addres['city'] ? $addres['city'] : '-') . "</td>";
                                            echo "<th style='width: 120px; text-align: left; font-weight: bold; padding: 5px;'>Emirate</th><td style='padding: 5px;'>" . ($addres['emirate'] ? $addres['emirate'] : '-') . "</td>";
                                            echo "</tr>";
                                        }
                                    }
                                    echo "</table>";

                                    function getFormattedAddressType($address_type)
                                    {
                                        // Adjust the address type headings as needed
                                        if (strpos($address_type, 'Emergency Contact - Local') !== false) {
                                            return 'Emergency Local';
                                        } elseif (strpos($address_type, 'Emergency Contact - Home Country') !== false) {
                                            return 'Emergency Home';
                                        } else {
                                            return $address_type;
                                        }
                                    }
                                    ?>


                                </tbody>
                            </table>
                        </div>

                        <div id="tab3" class="tab-pane">
                            <table>
                                <tbody>
                                    <?php
                                    foreach ($maid_skills as $skills) {
                                    ?>
                                        <tr>
                                            <td style="line-height: 20px; width: 200px"><b>Skills </b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $skills['skill'] ?: '-' ?></td>
                                            <td style="line-height: 20px; width: 200px"><b>Skill level</b></td>
                                            <td style="line-height: 20px; width: 300px"><?= $skills['rating_level'] ?: '-' ?></td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <div id="tab4" class="tab-pane">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Day</th>
                                        <th>Zone Name</th>
                                        <th>Area Name</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $dayMap = array(
                                        1 => 'Monday',
                                        2 => 'Tuesday',
                                        3 => 'Wednesday',
                                        4 => 'Thursday',
                                        5 => 'Friday',
                                        6 => 'Saturday',
                                        0 => 'Sunday',
                                    );

                                    foreach ($dayMap as $dayNumber => $dayName) {
                                        $availability = null;
                                        foreach ($maid_availability as $entry) {
                                            if ($entry['week_day'] == $dayNumber) {
                                                $availability = $entry;
                                                break;
                                            }
                                        }
                                        echo '<tr style="line-height: 20px; width: 200px">';
                                        echo '<td style="line-height: 20px; width: 200px">' . $dayName . '</td>';
                                        if ($availability) {
                                            echo '<td style="line-height: 20px; width: 200px">' . $availability['zone_name'] . '</td>';
                                            echo '<td style="line-height: 20px; width: 200px">' . $availability['area_name'] . '</td>';
                                            $startTime = date("h:i A", strtotime($availability['time_from']));
                                            $endTime = date("h:i A", strtotime($availability['time_to']));
                                            echo '<td style="line-height: 20px; width: 200px">' . $startTime . '</td>';
                                            echo '<td style="line-height: 20px; width: 200px">' . $endTime . '</td>';
                                        } else {
                                            echo '<td style="line-height: 20px; width: 200px">-</td>';
                                            echo '<td style="line-height: 20px; width: 200px">-</td>';
                                            echo '<td style="line-height: 20px; width: 200px">-</td>';
                                            echo '<td style="line-height: 20px; width: 200px">-</td>';
                                        }
                                        echo '</tr>';
                                    }
                                    ?>
                            </table>
                        </div>

                        <div id="tab5" class="tab-pane">
                            <table>
                                <tbody>
                                    <?php
                                    $documentTypes = [
                                        'Passport' => [],
                                        'Visa' => [],
                                        'Labor Card' => [],
                                        'Emirates ID' => [],
                                    ];

                                    foreach ($maid_documents as $document) {
                                        $documentTypes[$document['document']][] = $document;
                                    }

                                    foreach ($documentTypes as $documentType => $documents) {
                                        if (!empty($documents)) {

                                            foreach ($documents as $document) { ?>
                                                <tr>
                                                    <td style="line-height: 20px; width: 200px"><b><?= $document['document'] ?> Number</b></td>
                                                    <td style="line-height: 20px; width: 300px"><?= $document['document_number'] ?: '-' ?></td>
                                                    <td style="line-height: 20px; width: 250px"><b><?= $document['document'] ?> Expiry date</b></td>
                                                    <td style="line-height: 20px; width: 300px"><?= $document['expiry_date'] ?: '-' ?></td>
                                                    <td style="line-height: 20px; width: 250px"><b><?= $document['document'] ?> Grace date</b></td>
                                                    <td style="line-height: 20px; width: 300px"><?= $document['grace_period'] ?: '-' ?></td>
                                                    <td style="line-height: 20px; width: 200px"><b><?= $document['document'] ?> image</b></td>

                                                    <?php if ($document['file_name']) { ?>
                                                        <td colspan="8" style="line-height: 20px; width: 200px">
                                                            <center>
                                                                <a href="<?= base_url('./uploads/employee/documents/' . $document['file_name']); ?>" target="_blank">
                                                                    <img class="thumbnail" src="<?= base_url('./uploads/employee/documents/' . $document['file_name']); ?>" />
                                                                </a>
                                                            </center>
                                                        <?php } else { ?>
                                                            No Document Available
                                                        <?php } ?>
                                                        </td>
                                                </tr>
                                    <?php }
                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>

                        <div id="tab6" class="tab-pane">
                            <table id="cal_comp" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="line-height: 18px; width: 20px">
                                        <center>
                                            Sl.No.
                                        </center>
                                        </th>
                                        <th style="line-height: 18px; width: 20px">
                                        <center>
                                            Call ID
                                        </center>
                                        </th>
                                        <th style="line-height: 18px; width: 20px">
                                        <center>
                                            Customer ID
                                        </center>
                                        </th>
                                        <th style="line-height: 18px; width: 20px">
                                        <center>
                                            Complaint ID
                                        </center>
                                        </th>
                                        <th style="line-height: 18px">
                                        <center>
                                            Customer Name
                                        </center>
                                        </th>
                                        <th style="line-height: 18px">
                                        <center>
                                            Complaint Added Date
                                        </center>
                                        </th>
                                        <th style="line-height: 18px">
                                        <center>
                                            Complaint Service Date
                                        </center>
                                        </th>
                                        <th style="line-height: 18px">
                                        <center>
                                            Complaint
                                        </center>
                                        </th>
                                        <th style="line-height: 18px">
                                        <center>
                                            Action Taken
                                        </center>
                                        </th>
                                        <th style="line-height: 18px">
                                        <center>
                                            Status
                                        </center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($call_complaint_report)) {
                                    $i = 0;
                                    foreach ($call_complaint_report as $ccr) {

                                        if ($ccr->complaint_against == 'D') {
                                            $comp_against = 'Driver';
                                        } else if ($ccr->complaint_against == 'U') {
                                            $comp_against = 'User';
                                        } else if ($ccr->complaint_against == 'C') {
                                            $comp_against = 'Staff';
                                        }
                                        ?>
                                        <tr>
                                            <td style="line-height: 18px; width: 20px">
                                                <center><?= ++$i ?></center>
                                            </td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->call_history_id ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->customer_code ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint_no ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->customer_name ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= date('d-m-Y', strtotime($ccr->added_date_time)) ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= date('d-m-Y', strtotime($ccr->service_date)) ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->action_taken ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->status ?></td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                    <td style="line-height: 18px;" colspan="13">
                                        <center>No Records!</center>
                                    </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                    <div id="tab7" class="tab-pane">
                        <table id="cal_comp" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="line-height: 18px; width: 20px">
                                    <center>
                                        Sl.No.
                                    </center>
                                    </th>
                                    <th style="line-height: 18px; width: 20px">
                                        <center>
                                        Booking Id
                                        </center>
                                    </th>
                                    <th style="line-height: 18px; width: 20px">
                                        <center>
                                        Customer Name
                                        </center>
                                    </th>
                                    <th style="line-height: 18px; width: 20px">
                                        <center>
                                        Total hours
                                        </center>
                                    </th>
                                    <th style="line-height: 18px; width: 20px">
                                        <center>
                                        Total amount
                                        </center>
                                    </th>           
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($pay_employee)) {
                                    $i = 0;
                                    foreach ($pay_employee as $pe) {
                                        ?>
                                        <tr>
                                            <td style="line-height: 18px; width: 20px">
                                                <center><?= ++$i ?></center>
                                            </td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $pe->booking_id ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $pe->customer_name ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $pe->total_hours ?></td>
                                            <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $pe->total_amount ?></td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                    <td style="line-height: 18px;" colspan="13">
                                        <center>No Records!</center>
                                    </td>
                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                    </div>

                </div>
            </div><!-- /widget content -->

        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div> <!-- /row -->
<script>
    $(function() {
        var current = window.location.href;
        $('#primary_nav_wrap li a').each(function() {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href') === _base_url + 'maids') {
                $this.addClass('active');
            }
        })
    })

    function confirm_delete_modal(id) {
        $('#delete_id').val(id);
        fancybox_show('delete-popup', {
            width: 450
        });
    }

    function closeFancy() {
        $.fancybox.close();
    }

    function confirm_delete() {
        $.ajax({
            type: "POST",
            url: _base_url + "maid/change_status",
            data: {
                maid_id: $('#delete_id').val(),
                status: 2
            },
            dataType: "text",
            cache: false,
            success: function(result) {
                window.location.assign(_base_url + 'maids');
            }
        });
    }
</script>