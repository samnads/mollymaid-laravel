<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }
  

  table.da-table tr td {
    padding: 0px 6px;
  }
</style>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this package ?</h3>
      <input type="hidden" id="disable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this package ?</h3>
      <input type="hidden" id="enable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Packages</h3></li>
        
        <li><select style="width:160px;" id="all-maids" onchange="statusFilter(this.value)">
          <option value="" <?php echo $active == null ? 'selected' : ''; ?>>All</option>
          <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Active</option>
          <option value="0" <?php echo $status === '0' ? 'selected' : ''; ?>>Inactive</option>
        </select></li>
        
        <li class="mr-0 float-right">
        
        <div class="topiconnew border-0 green-btn">
        	<a href="<?php echo base_url('package/new'); ?>" title="Add Customer"> <i class="fa fa-plus"></i></a>
        </div>
        
        </li>
        </ul>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="package-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                <center>No.</center>
              </th>
              <th>
                Package Name
              </th>
              <th>
                Package Type
              </th>
              <th>
                Service Type
              </th>
              <th>
                Amount
              </th>
              <th>
                Start Date
              </th>
              <th>
                End Date
              </th>
              <th>
                Status
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($packages) > 0) {
              foreach ($packages as $key => $package) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;">
                    <center><?php echo $key + 1; ?></center>
                  </td>
                  <td><?php echo $package['package_name'] ?></td>
                  <td><?php echo $package['booking_type'] ?></td>
                  <td><?php echo $package['service_type_name'] ?></td>
                  <td><?php echo $package['amount'] ?></td>
                  <td><?php echo date("d-m-Y", strtotime($package['subscription_start_date'])); ?></td>
                  <td><?php echo date("d-m-Y", strtotime($package['subscription_end_date'])); ?></td>
                  <td>
                    <?php
                    if ($package['status'] == 'Active') {
                      $css_class = 'success';
                    } else if ($package['status'] == 'Inactive') {
                      $css_class = 'error';
                    } else {
                      $css_class = 'warning';
                    }
                    ?>
                    <span class="btn-block badge badge-<?php echo $css_class; ?>"><?php echo $package['status']; ?></span>
                  </td>
                  <td style="line-height: 18px; width: 158px" class="td-actions">
                  	<center>
                      <a class="n-btn-icon blue-btn" href="<?php echo base_url(); ?>package/view/<?php echo $package['package_id'] ?>" title="View Maid"><i class="btn-icon-only fa fa-eye"> </i></a>
                    <?php if ($package['maid_status'] != 2) { ?>
                      <a class="n-btn-icon purple-btn" href="<?php echo base_url(); ?>package/edit/<?php echo $package['package_id'] ?>" title="Edit Package"><i class="btn-icon-only icon-pencil"> </i></a>
                    <?php } ?>
                    <?php if ($package['maid_status'] == 2) { ?>
                      <p class="btn btn-danger" style="cursor:default;" title="Deleted"><i class="fa fa-ban"></i></p>
                    <?php } ?>
                    <?php
                    //if(user_authenticate() == 1)
                    if ($this->session->userdata('user_logged_in')['user_admin'] == 'Y') {
                      if ($package['status'] == "Active") {
                    ?>
                        <a href="javascript:void(0)" class="n-btn-icon green-btn" title="Disbale" onclick="confirm_disable_enable_modal(<?php echo $package['package_id'] ?>,'<?= $package['status'] ?>');"><i class="btn-icon-only fa fa-toggle-on"> </i></a>
                      <?php
                      } else if ($package['status'] == "Inactive") {
                      ?>
                        <a href="javascript:void(0)" class="n-btn-icon red-btn" title="Enable" onclick="confirm_disable_enable_modal(<?php echo $package['package_id'] ?>,'<?= $package['status'] ?>');"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                      <?php
                      }
                      //                                         if ($package['maid_status']!=2) {
                      ?>

                      <!--                                        <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="change_status(<?php echo $package['maid_id'] ?>,'2');" title="Delete"><i class="btn-icon-only icon-trash"></i> </a>    -->

                    <?php
                      //                                         }
                    }
                    ?>
                    </center>
                  </td>
                </tr>
              <?php
              }
            } else {
              ?>
              <tr>
                <td class="bg-info" colspan="9">
                  <p class="text-center">No Data Found !</p>
                </td>
              </tr>
            <?php
            }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
function statusFilter(status) {
	if (status) window.open("package?status=" + status, "_self")
	else window.open("package", "_self")
}

function confirm_disable_enable_modal(id, status) {
	if (status == 'Active') {
		$('#disable_id').val(id);
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'none',
			openSpeed: 1,
			autoSize: false,
			width: 450,
			height: 'auto',
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: $('#disable-popup'),
		});
	} else {
		$('#enable_id').val(id);
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'none',
			openSpeed: 1,
			autoSize: false,
			width: 450,
			height: 'auto',
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: $('#enable-popup'),
		});
	}
}

function confirm_disable() {
	const params = new Proxy(new URLSearchParams(window.location.search), {
		get: (searchParams, prop) => searchParams.get(prop),
	});
	status = params.status ? params.status : ''; // "some_value"
	$('body').append($('<form/>').attr({
		'action': _base_url + "package/disable?status=" + status,
		'method': 'post',
		'id': 'form_asdf'
	}).append($('<input/>').attr({
		'type': 'hidden',
		'name': 'id',
		'value': $('#disable_id').val()
	}))).find('#form_asdf').submit();
}

function confirm_enable() {
	const params = new Proxy(new URLSearchParams(window.location.search), {
		get: (searchParams, prop) => searchParams.get(prop),
	});
	status = params.status ? params.status : ''; // "some_value"
	$('body').append($('<form/>').attr({
		'action': _base_url + "package/enable?status=" + status,
		'method': 'post',
		'id': 'form_asdf'
	}).append($('<input/>').attr({
		'type': 'hidden',
		'name': 'id',
		'value': $('#enable_id').val()
	}))).find('#form_asdf').submit();
}

function closeFancy() {
	$.fancybox.close();
}
(function(a) {
	a(document).ready(function(b) {
		if (a('#package-list-table').length > 0) {
			a("table#package-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"iDisplayLength": 100,
				"scrollY": true,
				"orderMulti": false,
				"scrollX": true,
				'columnDefs': [{
					'targets': [-1],
					'orderable': false
				}, ]
			});
		}
	});
})(jQuery);
</script>