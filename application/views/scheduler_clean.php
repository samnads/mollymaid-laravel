<section>
	<div class="row dash-top-wrapper no-left-right-margin">
		<div class="col-md-12 col-sm-12 no-left-right-padding">
			<div class="widget-header"> 
				<form class="form-horizontal" method="POST" enctype="multipart/form-data">
					<div class="book-nav-top">
						<ul>
							<li>
								<i class="icon-th-list"></i>
								<h3>Block Timings</h3>
							</li>
							<div class="clear"></div>
						</ul>
					</div>
				</form>
                        
                        <style>
						.fc-toolbar { margin-bottom: 0px !important;}
						</style>
			</div>
			
			<div id="invoice-exTab2" class="bg-white">	
				<div id="calendar_cln" class="col-centered">
                </div>
			</div>
			
		</div><!--welcome-text-main end--> 
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->