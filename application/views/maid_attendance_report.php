<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post">
                <ul><li>
                    <i class="icon-th-list"></i>
                    <h3>Maid Attendance Reports</h3>  
                    
                    </li>
                    <li>                 
                   
                        <input type="text" style="width: 160px;" id="vehicle_date" name="attendance_date" value="<?php echo $attendance_date ?>">   
                    </li>
                    <li>                    
                    
                    <input type="submit" class="n-btn" value="Go" name="vehicle_report">
                    
                    </li>
                    
                    <li class="mr-0 float-right">
                    
                    <div class="topiconnew border-0 green-btn">
                    	<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div> 
                    </li>
                    </ul>           
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px;"> Sl No.</th>
                            <th style="line-height: 18px; width: 70px;"> Maid</th>
                            <th style="line-height: 18px; width: 50px;"> Zone</th>
                            <th style="line-height: 18px; width: 50px;"> Maid In</th>
							<th style="line-height: 18px; width: 50px;"> Maid Out</th>
                            <th style="line-height: 18px; width: 50px;"> Attendance</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($attendance_report))
                        {
                            $i = 0;
                            foreach ($attendance_report as $attendance)
                            {
								if($attendance->attandence_status == 1)
								{
									$stat = "Maid In";
								} else if($attendance->attandence_status == 2)
								{
									$stat = "Maid Out";
								} else {
									$stat ="";
								}
								$maid_in_time = date('h:i:s a', strtotime($attendance->maid_in_time));
								if($attendance->maid_out_time != "00:00:00")
								{
									$maid_out_time = date('h:i:s a', strtotime($attendance->maid_out_time));
								} else {
									$maid_out_time = "";
								}
								
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $attendance->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $attendance->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_in_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_out_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $stat . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="4">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
            <thead>
                <tr>
                    <th style="line-height: 18px; width: 30px;"> Sl No.</th>
                    <th style="line-height: 18px; width: 70px;"> Maid</th>
                    <th style="line-height: 18px; width: 50px;"> Zone</th>
                    <th style="line-height: 18px; width: 50px;"> Maid In</th>
                    <th style="line-height: 18px; width: 50px;"> Maid Out</th>
                    <th style="line-height: 18px; width: 50px;"> Attendance</th>                            
                </tr>
            </thead>
            <tbody>
                <?php
                if(!empty($attendance_report))
                {
                    $i = 0;
                    foreach ($attendance_report as $attendance)
                    {
                        if($attendance->attandence_status == 1)
                        {
                            $stat = "Maid In";
                        } else if($attendance->attandence_status == 2)
                        {
                            $stat = "Maid Out";
                        } else {
                            $stat ="";
                        }
                        $maid_in_time = date('h:i:s a', strtotime($attendance->maid_in_time));
                        if($attendance->maid_out_time != "00:00:00")
                        {
                            $maid_out_time = date('h:i:s a', strtotime($attendance->maid_out_time));
                        } else {
                            $maid_out_time = "";
                        }
                        
                        echo '<tr>'
                                . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                . '<td style="line-height: 18px;">' . $attendance->maid_name . '</td>'
                                . '<td style="line-height: 18px;">' . $attendance->zone_name . '</td>'
                                . '<td style="line-height: 18px;">' . $maid_in_time . '</td>'
                                . '<td style="line-height: 18px;">' . $maid_out_time . '</td>'
                                . '<td style="line-height: 18px;">' . $stat . '</td>'
                            .'</tr>';
                    }
                }
                else
                {
                    //echo '<tr><td colspan="4">No Results!</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "BookingReport.xls"); // Choose the file name
      return false;
  }  
</script>