<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<style>
  select[multiple] {
    height: 100px;
    width: 500px;
  }
</style>
<div id="crop-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Image Cropper</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="image" src="#">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
        <button type="button" class="n-btn mb-0" id="crop">Crop</button>
      </div>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget ">
            <div class="widget-header mb-0">
            
            <ul>
            <li>
                <i class="icon-user"></i>
                <h3>Edit Staff - <?php echo $maid_details[0]['maid_name'] ?></h3>
             </li>
             <li class="mr-0 float-right">



                <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url('maids'); ?>" title="Maid List"> <i class="fa fa-users"></i></a> </div>
                
                </li>
                </ul>



            </div> <!-- /widget-header -->
            <?php
if (count($maid_details) > 0) {
    foreach ($maid_details as $maid_val) {
    
        ?>
                    <div class="widget-content">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active" id="firstli"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                                <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                                <li id="secondli" ><a href="#addresstab" data-toggle="tab">Address</a></li>
                                <li id="four"><a href="#attachments" data-toggle="tab">Daily Preference</a></li>
                                <li id="five"><a href="#skills" data-toggle="tab">Skills</a></li>
                        

                            </ul>
                            <br>
                            <?php
if ($message == "success") {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Staff Details Updated Successfully.
                                                </div>
                                            </div>
                                    </div> <!-- /control-group -->
                               <?php
}
        ?>
                            <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="personal">
                                        
                                   










                                        <fieldset>
                                            <div class="col-sm-4">


                                                        <fieldset>

                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname"> &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="maid_name" name="maid_name" value="<?php //echo $maid_val['maid_name'] ?>">
                                                                    <input type="hidden" class="span3" id="maid_id" name="maid_id" value="<?php //echo $maid_val['maid_id'] ?>">
                                                                </div>
                                                            </div>	-->

                                                            
                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Employee Type</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <select name="employee_type" id="employee_type" class="" required >
                                                                 <option value="">Select Employee Type</option>
                                                                    <?php
                                                            if (count($employee_type) > 0) {
                                                                        foreach ($employee_type as $flatsVal) {
                                                                           
                                                                            if ($flatsVal['employee_type_id'] == $maid_val['employee_type_id']) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['employee_type_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['employee_type'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>




                                                                </select>
                                                            </div>
                                                        </div>

                                                            <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Staff Name</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                     <input type="text" class="" id="maid_name" name="maid_name" value="<?php echo $maid_val['maid_name'] ?>">
                                                                    <input type="hidden" class="" id="maid_id" name="maid_id" value="<?php echo $maid_val['maid_id'] ?>">
                                                                </div>
                                                            </div>


                                                            <!--<div class="control-group">
                                                                <label class="control-label">Gender &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <label class="radio inline">
                                                                        <input type="radio" name="gender" id="gender" value="M" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "M" ? 'checked="checked"' : '') : '' ?>>
                                                                        Male
                                                                    </label>
                                                                    <label class="radio inline">
                                                                        <input type="radio" name="gender" id="gender" value="F" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "F" ? 'checked="checked"' : '') : '' ?>>
                                                                        Female
                                                                    </label>
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Gender</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                   <div class="row m-0">
                                                                      <div class="col-sm-6 n-radi-check-main p-0">
                                                                           <input type="radio" value="M" id="gender" name="gender" class="" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "M" ? 'checked="checked"' : '') : '' ?>>
                                                                           <label for="gender"> <span class="border-radius-3"></span> <p>Male</p></label>
                                                                      </div>
                                                                      <div class="col-sm-6 n-radi-check-main p-0">
                                                                           <input type="radio" value="F" id="gender-f" name="gender" class="" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "F" ? 'checked="checked"' : '') : '' ?>>
                                                                           <label for="gender-f"> <span class="border-radius-3"></span> <p>Female</p></label>
                                                                      </div>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Nationality &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="nationality" name="nationality" value="<?php echo $maid_val['maid_nationality'] ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Nationality</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                     <input type="text" class="" id="nationality" name="nationality" value="<?php echo $maid_val['maid_nationality'] ?>">
                                                                </div>
                                                            </div>


                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="basicinput">Current Photo</label>
                                                                <div class="controls">

                                                                    <span style=" cursor:pointer;">
                                                                        <?php
if ($maid_val['maid_photo_file'] == "") {
            $image = base_url() . "img/no_image.jpg";
        } else {
            $image = base_url() . "maidimg/" . $maid_val['maid_photo_file'];
        }
        ?>

                                                                        <img src="<?php //echo $image; ?>" style="height: 100px; width: 100px"/>
                                                                        <input type="hidden" name="old_image" id="old_image" value="<?php echo $maid_val['maid_photo_file'] ?>"/>
                                                                    </span>

                                                                </div>
                                                            </div>-->

                                                            <!--<div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Current Photo</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                     <span style=" cursor:pointer;">
                                                                        <?php
if ($maid_val['maid_photo_file'] == "") {
            $image = base_url() . "img/no_image.jpg";
        } else {
            $image = base_url() . "maidimg/" . $maid_val['maid_photo_file'];
        }
        ?>

                                                                        <img src="<?php echo $image; ?>" style="height: 100px; width: 100px"/>
                                                                        <input type="hidden" name="old_image" id="old_image" value="<?php echo $maid_val['maid_photo_file'] ?>"/>
                                                                    </span>
                                                                </div>
                                                            </div>-->


                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="basicinput">Change Photo</label>
                                                                <div class="controls">
                                                                    <div id="me" class="styleall" style=" cursor:pointer;">
                                                                        <span style=" cursor:pointer;">
                                                                            <img src="<?php //echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>  -->

                                                            <!--<div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Change Photo</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                     <div id="me" class="styleall" style=" cursor:pointer;">
                                                                        <span style=" cursor:pointer;">
                                                                            <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>-->








                                                            <div class="row m-0 n-field-main">
                                                                <p>Change Photo</p>
                                    <label>
                                        <img class="thumbnail" id="avatar_img" src="<?= check_and_get_img_url('./upload/maid_avatars/'.$maid_val['maid_photo_file'], 'maid-avatar-upload.png'); ?>" style="width:80px;" alt="Promotion Image">
                                            <input type="file" class="sr-only" id="input-image" accept="image/*">
                                    </label>
                                    <input type="hidden" name="avatar_base64" id="avatar_base64">
                                    </div>


                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Present Address &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <textarea class="" rows="5" id="present_address" name="present_address"><?php echo $maid_val['maid_present_address'] ?></textarea>
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Present Address</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                     <textarea class="" rows="5" id="present_address" name="present_address"><?php echo $maid_val['maid_present_address'] ?></textarea>
                                                                </div>
                                                            </div>


                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Permanent Address &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <textarea class="" rows="5" id="permanent_address" name="permanent_address"><?php echo $maid_val['maid_permanent_address'] ?></textarea>
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                                <p>Permanent Address</p>
                                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                     <textarea class="" rows="5" id="permanent_address" name="permanent_address"><?php echo $maid_val['maid_permanent_address'] ?></textarea>
                                                                </div>
                                                            </div>




                                                             <!--<div class="control-group">
                                                            <label class="control-label" for="firstname">Mobile Number 1 &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <input type="text" class="" id="mobile1" name="mobile1" value="<?php echo $maid_val['maid_mobile_1'] ?>">
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Mobile Number 1</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <input type="text" class="" id="mobile1" name="mobile1" value="<?php echo $maid_val['maid_mobile_1'] ?>">
                                                            </div>
                                                        </div>




                                                        <!--<div class="control-group">
                                                            <label class="control-label" for="firstname">Mobile Number 2</label>
                                                            <div class="controls">
                                                                <input type="text" class="" id="mobile2" name="mobile2" value="<?php echo $maid_val['maid_mobile_2'] ?>">
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Mobile Number 2</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <input type="text" class="" id="mobile2" name="mobile2" value="<?php echo $maid_val['maid_mobile_2'] ?>">
                                                            </div>
                                                        </div>

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>WhatsApp No.</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <input type="text" class="" id="whatsapp_number_1" name="whatsapp_number_1" value="<?php echo $maid_val['maid_whatsapp_no_1'] ?>">
                                                            </div>
                                                        </div>




                                                        <!--<div class="control-group">
                                                            <label class="control-label" for="basicinput">Flat &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <select name="flat" id="flat" class="" required >
                                                                    <?php
if (count($flats) > 0) {
            foreach ($flats as $flatsVal) {
                if ($flatsVal['flat_id'] == $maid_val['flat_id']) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }

                ?>
                                                                            <option value="<?php //echo $flatsVal['flat_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['flat_name'] ?></option>
                                                                            <?php
}
        }
        ?>




                                                                </select>
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Flat</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <select name="flat" id="flat" class="" required >
                                                                    <?php
                                                            if (count($flats) > 0) {
                                                                        foreach ($flats as $flatsVal) {
                                                                            if ($flatsVal['flat_id'] == $maid_val['flat_id']) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['flat_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['flat_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>




                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row m-0 n-field-main payment_mode_customer1" style="display:none;" >
                                                            <p>Driver location</p>
                                                            <div class="col-sm-12 p-0 n-field-box">
                                                            <select id="d_location" name="d_location">
                                                            <option value="">select location</option>
                                                            <?php
                                                           
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                                    if ($teamsVal['location_id'] == $driver_availability[0]['location_id']) {
                                                                        $selected = "selected";
                                                                    } else {
                                                                        $selected = "";
                                                                    }
                                                                    ?>
                                                            <option value="<?php echo $teamsVal['location_id']; ?>" <?php echo $selected; ?> ><?php echo $teamsVal['location_name'] ?></option>
                                                                    <?php
                                                                        }
                                                                        }
                                                                        ?>
                                                            </select>
                                                          
                                                         </div>
                                                        </div>
                                                        <div class="row m-0 n-field-main payment_mode_customer2" style="display:none;" >
                                                            <p>From Date</p>
                                                            <div class="col-sm-12 p-0 n-field-box">
                                                                 <input type="date" id="from_date" name="from_date" value="<?php echo $driver_availability[0]['date_from']; ?>" autocomplete="off" >
                                                            </div>
                                                        </div>

                                                        


                                                  



                                                        <!--<div class="control-group">
                                                            <label class="control-label" for="basicinput">Team &nbsp;</label>
                                                            <div class="controls">
                                                                <select name="team" id="team" class="">
                                                                    <option value="">Select Team</option>
                                                                    <?php
// if (count($teams) > 0) {
        // foreach ($teams as $teamsVal) {
        // if ($teamsVal->team_id == $maid_val['team_id'])
        // $selected = "selected";
        // else
        // $selected = "";
        // ?>
                                                                            // <option value="<?php// echo $teamsVal->team_id ?>" <?php// echo $selected; ?>><?php// echo $teamsVal->team_name; ?></option>
                                                                            // <?php
// }
        // }
        ?>




                                                                </select>
                                                            </div>
                                                        </div>-->

                                                        <!--<div class="control-group">
                                                            <label class="control-label">Services &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <?php
if (count($services) > 0) {
            foreach ($services as $serviceVal) {
                ?>
                                                                        <label class="checkbox">


                                                                            <input type="checkbox" class="services" name="services[]" value="<?php //echo $serviceVal['service_type_id'] ?>" <?php echo (in_array($serviceVal['service_type_id'], $service_maid)) ? "checked='checked'" : ''; ?>> <?php //echo $serviceVal['service_type_name'] ?>
                                                                        </label>
                                                                        <?php
}
        }
        ?>

                                                            </div>
                                                        </div>-->


                                                        </fieldset>



                                            </div>




                                            <div class="col-sm-1">&nbsp;</div>




                                            <div class="col-sm-4">
                                                <div id="target-2" class="">





                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Services</p>

                                                                 <?php
if (count($services) > 0) {
            foreach ($services as $key => $serviceVal) {
                ?>
                                                                        <!--<label class="checkbox">


<input type="checkbox" class="services" name="services[]" value="<?php //echo $serviceVal['service_type_id'] ?>" <?php //echo (in_array($serviceVal['service_type_id'], $service_maid)) ? "checked='checked'" : ''; ?>> <?php //echo $serviceVal['service_type_name'] ?>
                                                                        </label>-->

<div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
    <div class="col-sm-12 pr-0 pl-0 n-radi-check-main">
    	<input type="checkbox" value="<?php echo $serviceVal['service_type_id'] ?>" id="services-<?=$key;?>" name="services[]" class="services" <?php echo (in_array($serviceVal['service_type_id'], $service_maid)) ? "checked='checked'" : ''; ?>>
    	<label for="services-<?=$key;?>"> <span class="border-radius-3"></span><p><?php echo $serviceVal['service_type_name'] ?></p></label>
    </div>
</div>

                                                                        <?php
}
        }
        ?>

                                                        </div>




                                                        <!--<div class="control-group">
                                                            <label class="control-label" for="firstname">Notes</label>
                                                            <div class="controls">
                                                                <textarea class="" rows="5" id="notes" name="notes"><?php echo $maid_val['maid_notes'] ?></textarea>
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Notes</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <textarea class="" rows="5" id="notes" name="notes"><?php echo $maid_val['maid_notes'] ?></textarea>
                                                            </div>
                                                        </div>


														<!--<div class="control-group">
                                                            <label class="control-label" for="username">Username</label>
                                                            <div class="controls">
                                                                <input type="text" class="" id="username" name="username" value="<?php //echo $maid_val['username'] ?>">
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Username</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <input type="text" class="" id="username" name="username" value="<?php echo $maid_val['username'] ?>">
                                                            </div>
                                                        </div>


														<!--<div class="control-group">
                                                            <label class="control-label" for="password">Password</label>
                                                            <div class="controls">
                                                                <input type="text" class="" id="password" name="password" value="<?php echo $maid_val['password'] ?>">
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Password</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <input type="text" class="" id="password" name="password" value="<?php echo $maid_val['password'] ?>">
                                                            </div>
                                                        </div>


														<!--<div class="control-group">
                                                            <label class="control-label" for="basicinput">App Login Status &nbsp;</label>
                                                            <div class="controls">
                                                                <select name="appstatus" id="appstatus" class="">
                                                                    <option value="0" <?php //echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "0" ? 'selected="selected"' : '') : '' ?>>Inactive</option>
                                                                    <option value="1" <?php //echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "1" ? 'selected="selected"' : '') : '' ?>>Active</option>
																</select>
                                                            </div>
                                                        </div>-->

                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>App Login Status</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <select name="appstatus" id="appstatus" class="">
                                                                    <option value="0" <?php echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "0" ? 'selected="selected"' : '') : '' ?>>Inactive</option>
                                                                    <option value="1" <?php echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "1" ? 'selected="selected"' : '') : '' ?>>Active</option>
																</select>
                                                            </div>
                                                        </div>
                                                        <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                            <p>Priority Number</p>
                                                            <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                                 <input type="text" id="maid-priority" name="maid_priority" value="<?php echo $maid_val['maid_priority'] ?>">
                                                            </div>
                                                        </div>



                                                     
                                                        <div class="row m-0 n-field-main payment_mode_customer3" style="display:none;" >
                                                            <p>To Date</p>
                                                            <div class="col-sm-12 p-0 n-field-box">
                                                                 <input type="date" id="to_date" name="to_date"  autocomplete="off" value="<?php echo $driver_availability[0]['date_to'];  ?>" >
                                                            </div>
                                                        </div>
                                                      




                                                    </div>

                                            </div>





                                        </fieldset>








                                    <div class="col-sm-12 mt-2 mb-4">

                                            <!-- <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload"/>
                                            <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                            <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            <input type="submit" class="n-btn" value="Update" name="maid_edit" onclick="return validate_maid();"> -->
                                            <div class="col-sm-12"><button type="button" class="n-btn" id="clickfirst">Next</button></div>
                                        </div>
                                    </div>





                                    
                            <div class="tab-pane" id="attachments">
    <fieldset>
        <div class="row">
            <div class="col-sm-3">
                <fieldset>
                    <?php $role_permissions =$user_modules;
$r_permissions_id = array_column($role_permissions, "week_day");   ?>
                    <div class="n-field-main">
                        <!-- <p>Day</p> -->
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(0, $r_permissions_id )): ?>
                                <input id="sunday-checkbox" type="checkbox" value="0" name="days[]" checked>
                                <?php else: ?>
                                    <input id="sunday-checkbox" type="checkbox" value="0" name="days[]">
                            <?php endif; ?>
                                <label for="sunday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Sunday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(1, $r_permissions_id )): ?>
                                <input id="monday-checkbox" type="checkbox" value="1" name="days[]" checked>
                                <?php else: ?>
                                <input id="monday-checkbox" type="checkbox" value="1" name="days[]" >
                                <?php endif; ?>
                                <label for="monday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Monday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(2, $r_permissions_id )): ?>
                                <input id="tuesday-checkbox" type="checkbox" value="2" name="days[]" checked>
                                <?php else: ?>
                                <input id="tuesday-checkbox" type="checkbox" value="2" name="days[]" >
                                <?php endif; ?>
                                <label for="tuesday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Tuesday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(3, $r_permissions_id )): ?>
                                <input id="Wednesday-checkbox" type="checkbox" value="3" name="days[]" checked>
                                <?php else: ?>
                                <input id="Wednesday-checkbox" type="checkbox" value="3" name="days[]" >
                                <?php endif; ?>
                                <label for="Wednesday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Wednesday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(4, $r_permissions_id )): ?>
                                <input id="thursday-checkbox" type="checkbox" value="4" name="days[]" checked>
                                <?php else: ?>
                                <input id="thursday-checkbox" type="checkbox" value="4" name="days[]">
                                <?php endif; ?>
                                <label for="thursday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Thursday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(5, $r_permissions_id )): ?>
                                <input id="friday-checkbox" type="checkbox" value="5" name="days[]" checked>
                                <?php else: ?>
                                <input id="friday-checkbox" type="checkbox" value="5" name="days[]">
                                <?php endif; ?>
                                <label for="friday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Friday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <div class="n-radi-check-main">
                            <?php if (in_array(6, $r_permissions_id )): ?>
                                <input id="saturday-checkbox" type="checkbox" value="6" name="days[]" checked>
                                <?php else: ?>
                                <input id="saturday-checkbox" type="checkbox" value="6" name="days[]">
                                <?php endif; ?>
                                <label for="saturday-checkbox">
                                    <span class="border-radius-3"></span>
                                    <p>Saturday</p>
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col-sm-3">
                <fieldset>
                    <div class="n-field-main">
                     
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                          
                                        <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $sunday) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main">
                       
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                            <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $monday) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                <div class="n-field-main">
                       
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                            <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $tuesday) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                <div class="n-field-main">
                       
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                            <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $wed) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                <div class="n-field-main">
                       
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                            <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $thursday) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                <div class="n-field-main"  style="margin-top: 10px;">
                       
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                            <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $friday) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                <div class="n-field-main"  style="margin-top: 10px;">
                       
                        <div class="n-field-box">
                            <select id="" name="locations[]">
                            <option value="">select location</option>
                            <?php  
                                                            if (count($locations) > 0) {
                                                                        foreach ($locations as $flatsVal) {
                                                                            if ($flatsVal['location_id'] == $sat) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['location_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['location_name'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>
                            </select>
                        </div>
                    </div>
                    </fieldset>
                    
                    
            </div>
            <div class="col-sm-3">
                <fieldset>
                    <div class="n-field-main">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $sun_time_from;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $mon_time_from;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $tues_time_from;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $wed_time_from;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $thurs_time_from;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main"  style="margin-top: 10px;">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $friday_time_from;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                <div class="n-field-main" style="margin-top: 10px;">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_from[]" placeholder="Time From" value="<?php echo $sat_time_from;?>">
                        </div>
                    </div>
                </fieldset>

            </div>
            <div class="col-sm-3">
           
                <fieldset>
                
                    <div class="n-field-main">
                        <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $sun_time_to;?>" >
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $mon_time_to;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $tues_time_to;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $wed_time_to;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $thurs_time_to;?>">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $friday_time_to;?>" >
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="n-field-main"  style="margin-top: 10px;">
                       <div class="n-field-box">
                            <input type="time" id="" name="time_to[]" placeholder="Time to" value="<?php echo $sat_time_to;?>">
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </fieldset>
    <!-- <div class="col-sm-12 mt-2 mb-4">

 <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload"/>
<input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
<input type="hidden" name="img_name_resp" id="img_name_resp"/>
<input type="submit" class="n-btn" value="Update" name="maid_edit" onclick="return validate_maid();"> 
</div> -->
</div>
</div>




<div class="tab-pane" id="skills">
                            <div class="container">
  <div class="row">
    <div class="col-sm-3">
      <div class="tab-pane" id="skills">
        <fieldset>
        <div class="row m-0 n-field-main">
                                                        <p style="color:red;">Select Skills</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
    <select id="skills" name="skills[]" multiple>
        <option value="" >Select Skill</option>
        <?php
        if (count($skills) > 0) {
            foreach ($skills as $teamsVal) {
                $selected = '';
                foreach ($selectedSkills as $selectedSkill) {
                    if ($selectedSkill['skill_id'] == $teamsVal['skill_id']) {
                        $selected = 'selected';
                        break;
                    }
                }
                ?>
                <option value="<?php echo $teamsVal['skill_id']; ?>" <?php echo $selected; ?>><?php echo $teamsVal['skill']; ?></option>
                <?php
            }
        }
        ?>
    </select>
</div><br>

                                    <div class="row m-0 n-field-main">
                                                        <p style="color:red;"> Select Level</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                        <select id="level" name="levels[]" multiple>
    <option value="">Select Rating Level</option>
    <?php
    if (count($levels) > 0) {
        foreach ($levels as $teamsVal) {
            $selected = '';
            foreach ($selectedLevels as $selectedLevel) {
                if ($selectedLevel['rating_level_id'] == $teamsVal['rating_level_id']) {
                    $selected = 'selected';
                    break;
                }
            }
            ?>
            <option value="<?php echo $teamsVal['rating_level_id']; ?>" <?php echo $selected; ?>><?php echo $teamsVal['rating_level']; ?></option>
            <?php
        }
    }
    ?>
</select>
                                                        </div>
                                                    </div></br>

                                                    <div class="row m-0 n-field-main">
                                                    <p>Note</p>
                                                    <div class="col-sm-12 p-0 n-field-box">
                                                       
                                                        <textarea class="" id="" name="note[]"><?php echo $note; ?></textarea>
                                                       
                                                    </div>
                                                </div>



                                    </fieldset>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-12">
 
 <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
 <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
 <input type="hidden" name="img_name_resp" id="img_name_resp"/>
 <input type="submit" class="n-btn" value="Update" name="maid_edit" onclick="return validate_maid();">



</div>
                                    </div>
                                    </div>









                                    <div class="tab-pane" id="addresstab">
                            <div class="container">
  <div class="row">
    <div class="col-sm-3">
      <div class="tab-pane" id="addresstab">
        <fieldset>
    
          <p style="color:red;">Local Address</p>
          <div class="row m-0 n-field-main">
            <p>House Number</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="local_house" name="local_house"  value="<?php echo isset($maid_address_local[0]['house_number']) ? $maid_address_local[0]['house_number'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Street</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="local_street" name="local_street" value="<?php echo isset($maid_address_local[0]['street']) ? $maid_address_local[0]['street'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Town</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="local_town" name="local_town" value="<?php echo isset($maid_address_local[0]['town']) ? $maid_address_local[0]['town'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>City</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="local_city" name="local_city" value="<?php echo isset($maid_address_local[0]['city']) ? $maid_address_local[0]['city'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Country</p>
            <div class="col-sm-8 p-0 n-field-box">
              <!-- <input type="text" class="" id="local_country" name="local_country"> -->
              <select name="local_country" id="local_country" class="">
                                                            <option value="">Select Country</option>
                                                             <?php
                                                            if (count($countries) > 0) {
                                                                        foreach ($countries as $flatsVal) {
                                                                            if ($flatsVal['id'] == $maid_address_local[0]['country_id']) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['country'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>




                                                                </select>
            </div>
          </div>
        
        </fieldset>
      </div>
    </div>
    <div class="col-sm-3">
      <div id="target-2">
        <fieldset>
      
          <p style="color:red;">Permanent Address</p>
          <!-- Address fields -->
          <div class="row m-0 n-field-main">
            <p>House Number</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="perm_house" name="perm_house" value="<?php echo isset($maid_address_perm[0]['house_number']) ? $maid_address_perm[0]['house_number'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Street</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="perm_street" name="perm_street" value="<?php echo isset($maid_address_perm[0]['street']) ? $maid_address_perm[0]['street'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Town</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="perm_town" name="perm_town" value="<?php echo isset($maid_address_perm[0]['town']) ? $maid_address_perm[0]['town'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>City</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="perm_city" name="perm_city" value="<?php echo isset($maid_address_perm[0]['city']) ? $maid_address_perm[0]['city'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Country</p>
            <div class="col-sm-8 p-0 n-field-box">
              <!-- <input type="text" class="" id="perm_country" name="perm_country"> -->
              <select name="perm_country" id="perm_country" class="">
                                                            <option value="">Select Country</option>
                                                            <?php
                                                            if (count($countries) > 0) {
                                                                        foreach ($countries as $flatsVal) {
                                                                            if ($flatsVal['id'] == $maid_address_perm[0]['country_id']) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['country'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>




                                                                </select>
            </div>
          </div>
         
        </fieldset>
      </div>
    </div>
    <div class="col-sm-3">
      <div id="target-3">
        <fieldset>
     
          <p style="color:red;">Emergency Contact-Local</p>
          <!-- Address fields -->
          <div class="row m-0 n-field-main">
            <p>Name</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_local_name" name="emergency_local_name" value="<?php echo isset($maid_address_emer_local[0]['name']) ? $maid_address_emer_local[0]['name'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Relationship</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_local_relationship" name="emergency_local_relationship" value="<?php echo isset($maid_address_emer_local[0]['relationship']) ? $maid_address_emer_local[0]['relationship'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Contact No</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_local_contact" name="emergency_local_contact"  value="<?php echo isset($maid_address_emer_local[0]['mobile_no_1']) ? $maid_address_emer_local[0]['mobile_no_1'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Whatsapp No</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_local_whatsapp" name="emergency_local_whatsapp"  value="<?php echo isset($maid_address_emer_local[0]['whatsapp_no_1']) ? $maid_address_emer_local[0]['whatsapp_no_1'] : ''; ?>">
            </div>
          </div>
        </fieldset>
      </div>
    </div>
    <div class="col-sm-3">
      <div id="target-4">
        <fieldset>

          <p style="color:red;">Emergency Contact-Home Country</p>
          <!-- Address fields -->
          <div class="row m-0 n-field-main">
            <p>Name</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_home_name" name="emergency_home_name" value="<?php echo isset($maid_address_emer_home[0]['name']) ? $maid_address_emer_home[0]['name'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Relationship</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_home_relationship" name="emergency_home_relationship" value="<?php echo isset($maid_address_emer_home[0]['relationship']) ? $maid_address_emer_home[0]['relationship'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Contact No</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_home_contact" name="emergency_home_contact" value="<?php echo isset($maid_address_emer_home[0]['mobile_no_1']) ? $maid_address_emer_home[0]['mobile_no_1'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Whatsapp No</p>
            <div class="col-sm-8 p-0 n-field-box">
              <input type="text" class="" id="emergency_home_whatsapp" name="emergency_home_whatsapp" value="<?php echo isset($maid_address_emer_home[0]['whatsapp_no_1']) ? $maid_address_emer_home[0]['whatsapp_no_1'] : ''; ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Home Country</p>
            <div class="col-sm-8 p-0 n-field-box">
              <!-- <input type="text" class="" id="emergency_home_country" name="emergency_home_country"> -->
              <select name="emergency_home_country" id="emergency_home_country" class="">
                                                            <option value="">Select Country</option>
                                                            <?php
                                                            if (count($countries) > 0) {
                                                                        foreach ($countries as $flatsVal) {
                                                                            if ($flatsVal['id'] == $maid_address_emer_home[0]['country_id']) {
                                                                                $selected = "selected";
                                                                            } else {
                                                                                $selected = "";
                                                                            }

                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['country'] ?></option>
                                                                            <?php
                                                                            }
                                                                                    }
                                                                                    ?>




                                                                </select>
            </div>
          </div>
          
        </fieldset>
      </div>
    </div>
  </div>
  <!-- <div class="col-sm-12 mt-2 mb-4">

 <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload"/>
<input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
<input type="hidden" name="img_name_resp" id="img_name_resp"/>
<div class="col-sm-12"><button type="button" class="n-btn" id="clicksecond">Next</button>
<input type="submit" class="n-btn" value="Update" style="display: none;" name="maid_edit" onclick="return validate_maid();" id="submitBtn"> </div>
</div> -->
</div>






                                    <div class="tab-pane" id="attachmentss">
                                        <fieldset>

                                            <div class="span5">
                                                <div class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Passport Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="passport_number" name="passport_number" value="<?php echo $maid_val['maid_passport_number'] ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Passport Number</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="passport_number" name="passport_number" value="<?php echo $maid_val['maid_passport_number'] ?>">
                                                                </div>
                                                            </div>


                                                            <?php
if ($maid_val['maid_passport_expiry_date'] != '' && $maid_val['maid_passport_expiry_date'] != '0000-00-00') {
            list($year, $month, $day) = explode("-", $maid_val['maid_passport_expiry_date']);
            $passport_expiry = "$day/$month/$year";
        } else if ($maid_val['maid_passport_expiry_date'] = '0000-00-00') {
            $passport_expiry = "";
        }
        ?>
                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Passport Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="passport_expiry" name="passport_expiry" value="<?php echo $passport_expiry ?>">
                                                                </div>
                                                            </div> -->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Passport Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="passport_expiry" name="passport_expiry" value="<?php echo $passport_expiry ?>">
                                                                </div>
                                                            </div>




                                                            <?php
if ($maid_val['maid_passport_file'] != '') {

            $passport_image = base_url() . "maid_passport/" . $maid_val['maid_passport_file'];

            ?>

                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php //echo $passport_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_passport" name="old_attach_passport" value="<?php echo $maid_val['maid_passport_file'] ?>">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                <p>Current Attachment</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <span><img src="<?php echo $passport_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_passport" name="old_attach_passport" value="<?php echo $maid_val['maid_passport_file'] ?>">
                                                                </div>
                                                            </div>


                                                                <?php
//
        } else {
            $passport_image = base_url() . "img/no_image.jpg";

            ?>
                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php //echo $passport_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_passport" name="old_attach_passport" value="">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                <p>Current Attachment</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <span><img src="<?php echo $passport_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_passport" name="old_attach_passport" value="">
                                                                </div>
                                                            </div>

                                                                <?php
}
        ?>


                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Attach Passport</label>
                                                                <div class="controls">
                                                                    <input type="file" class="" id="attach_passport" name="attach_passport">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Passport</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="file" class="" id="attach_passport" name="attach_passport">
                                                                </div>
                                                            </div>



                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="email">Visa Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="visa_number" name="visa_number" value="<?php //echo $maid_val['maid_visa_number'] ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Visa Number</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="visa_number" name="visa_number" value="<?php echo $maid_val['maid_visa_number'] ?>">
                                                                </div>
                                                            </div>

                                                            <?php
if ($maid_val['maid_visa_expiry_date'] != '' && $maid_val['maid_visa_expiry_date'] != '0000-00-00') {
            list($year, $month, $day) = explode("-", $maid_val['maid_visa_expiry_date']);
            $visa_expiry = "$day/$month/$year";
        } else if ($maid_val['maid_visa_expiry_date'] = '0000-00-00') {
            $visa_expiry = "";
        }
        ?>

                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Visa Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="visa_expiry" name="visa_expiry" value="<?php //echo $visa_expiry ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Visa Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="visa_expiry" name="visa_expiry" value="<?php echo $visa_expiry ?>">
                                                                </div>
                                                            </div>

                                                            <?php
if ($maid_val['maid_visa_file'] != '') {

            $visa_image = base_url() . "maid_visa/" . $maid_val['maid_visa_file'];

            ?>

                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php //echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_visa" name="old_attach_visa" value="<?php echo $maid_val['maid_visa_file'] ?>">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                <p>Current Attachment</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_visa" name="old_attach_visa" value="<?php echo $maid_val['maid_visa_file'] ?>">
                                                                </div>
                                                            </div>


                                                                <?php
//                                                                $maid_visa_file = $maid_val['maid_visa_file'];
        } else {

            $visa_image = base_url() . "img/no_image.jpg";

            ?>
                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php //echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_visa" name="old_attach_visa" value="">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                <p>Current Attachment</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_visa" name="old_attach_visa" value="">
                                                                </div>
                                                            </div>


                                                                <?php
}
        ?>

                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Attach Visa</label>
                                                                <div class="controls">
                                                                    <input type="file" class="" id="attach_visa" name="attach_visa">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Visa</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="file" class="" id="attach_visa" name="attach_visa">
                                                                </div>
                                                            </div>


                                                        </fieldset>

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->


                                            <div class="span5">
                                                <div id="target-2" class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="email">Labour Card Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="labour_number" name="labour_number" value="<?php //echo $maid_val['maid_labour_card_number'] ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Labour Card Number</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="labour_number" name="labour_number" value="<?php echo $maid_val['maid_labour_card_number'] ?>">
                                                                </div>
                                                            </div>


                                                            <?php
if ($maid_val['maid_labour_card_expiry_date'] != '' && $maid_val['maid_labour_card_expiry_date'] != '0000-00-00') {
            list($year, $month, $day) = explode("-", $maid_val['maid_labour_card_expiry_date']);
            $labour_expiry = "$day/$month/$year";
        } else if ($maid_val['maid_labour_card_expiry_date'] = '0000-00-00') {
            $labour_expiry = "";
        }
        ?>

                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Labour Card Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="labour_expiry" name="labour_expiry" value="<?php echo $labour_expiry ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Labour Card Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="labour_expiry" name="labour_expiry" value="<?php echo $labour_expiry ?>">
                                                                </div>
                                                            </div>



                                                            <?php
if ($maid_val['maid_labour_card_file'] != '') {

            $labour_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];

            ?>

                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_labour" name="old_attach_labour" value="<?php echo $maid_val['maid_labour_card_file'] ?>">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                         <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_labour" name="old_attach_labour" value="<?php echo $maid_val['maid_labour_card_file'] ?>">
                                                                    </div>
                                                                </div>


                                                                <?php
//
        } else {

            $labour_image = base_url() . "img/no_image.jpg";

            ?>
                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_labour" name="old_attach_labour" value="">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                    <p>Current Attachment</p>
                                                                    <div class="col-sm-12 p-0 n-field-box">
                                                                         <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_labour" name="old_attach_labour" value="">
                                                                    </div>
                                                                </div>

                                                                <?php
}
        ?>





                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Attach Labour Card</label>
                                                                <div class="controls">
                                                                    <input type="file" class="" id="attach_labour" name="attach_labour" >
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Labour Card</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="file" class="" id="attach_labour" name="attach_labour" >
                                                                </div>
                                                            </div>




                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="email">Emirates Id</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="emirates_id" name="emirates_id" value="<?php echo $maid_val['maid_emirates_id'] ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Emirates Id</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="emirates_id" name="emirates_id" value="<?php echo $maid_val['maid_emirates_id'] ?>">
                                                                </div>
                                                            </div>

                                                            <?php
if ($maid_val['maid_emirates_expiry_date'] != '' && $maid_val['maid_emirates_expiry_date'] != '0000-00-00') {
            list($year, $month, $day) = explode("-", $maid_val['maid_emirates_expiry_date']);
            $emirates_expiry = "$day/$month/$year";
        } else if ($maid_val['maid_emirates_expiry_date'] = '0000-00-00') {
            $emirates_expiry = "";
        }
        ?>


                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Emirates Id Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="" id="emirates_expiry" name="emirates_expiry" value="<?php echo $emirates_expiry ?>">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Emirates Id Expiry</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="text" class="" id="emirates_expiry" name="emirates_expiry" value="<?php echo $emirates_expiry ?>">
                                                                </div>
                                                            </div>



                                                            <?php
if ($maid_val['maid_emirates_file'] != '') {

            $emirates_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];

            ?>

                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php //echo $emirates_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_emirates" name="old_attach_emirates" value="<?php //echo $maid_val['maid_emirates_file'] ?>">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                <p>Current Attachment</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_emirates" name="old_attach_emirates" value="<?php echo $maid_val['maid_emirates_file'] ?>">
                                                                </div>
                                                            </div>


                                                                <?php
//
        } else {

            $emirates_image = base_url() . "img/no_image.jpg";

            ?>
                                                                <!--<div class="control-group">
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_emirates" name="old_attach_emirates" value="">
                                                                    </div>
                                                                </div>-->

                                                                <div class="row m-0 n-field-main">
                                                                <p>Current Attachment</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="" id="old_attach_emirates" name="old_attach_emirates" value="">
                                                                </div>
                                                            </div>

                                                                <?php
}
        ?>







                                                            <!--<div class="control-group">
                                                                <label class="control-label" for="firstname">Attach Emirates Card</label>
                                                                <div class="controls">
                                                                    <input type="file" class="" id="attach_emirates" name="attach_emirates">
                                                                </div>
                                                            </div>-->

                                                            <div class="row m-0 n-field-main">
                                                                <p>Attach Emirates Card</p>
                                                                <div class="col-sm-12 p-0 n-field-box">
                                                                     <input type="file" class="" id="attach_emirates" name="attach_emirates">
                                                                </div>
                                                            </div>

                                                            <br />
                                                        </fieldset>
                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span5 -->

                                        </fieldset>

<!--                                        <div class="form-actions" style="padding-left: 211px;">

                                            <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload"/>
                                            <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                            <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            <input type="submit" class="btn btn-primary pull-right" value="Submit" name="maid_edit" onclick="return validate_maid();">

                                        </div>   -->


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> <!-- /widget-content -->
                    <?php
}
}
?>
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div> <!-- /row -->
<script>
$(function() {
	var current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('maids '); ?>') {
			$this.addClass('active');
		}
	})
})
var cropper;
function showCropper() {
	cropper = new Cropper(image, {
		aspectRatio: 300 / 300,
		viewMode: 1,
	});
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#crop-popup'),
	});
}
function closeCropper() {
	cropper.destroy();
	cropper = null;
	$.fancybox.close();
}
window.addEventListener('DOMContentLoaded', function() {
	var avatar_img = document.getElementById('avatar_img');
	var image = document.getElementById('image');
	var input = document.getElementById('input-image');
	input.addEventListener('change', function(e) {
		var files = e.target.files;
		var done = function(url) {
			input.value = '';
			image.src = url;
			showCropper();
		};
		var reader;
		var file;
		var url;
		if (files && files.length > 0) {
			file = files[0];
			if (URL) {
				done(URL.createObjectURL(file));
			} else if (FileReader) {
				reader = new FileReader();
				reader.onload = function(e) {
					done(reader.result);
				};
				reader.readAsDataURL(file);
			}
		}
	});
	document.getElementById('crop').addEventListener('click', function() {
		var initialpromo_imgURL;
		var canvas;
		if (cropper) {
			canvas = cropper.getCroppedCanvas({
				width: 300,
				height: 300,
			});
			initialpromo_imgURL = avatar_img.src;
			avatar_img.src = canvas.toDataURL();
			$('#avatar_base64').val(avatar_img.src);
		}
		closeCropper();
	});
});
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('maids'); ?>') {
			$this.addClass('active');
		}
	})
})

$('body').on('click', '#clickfirst', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #secondli').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#attachmentss').removeClass('active').css('display','none');
    $('#skills').removeClass('active').css('display','none');
   $('#addresstab').addClass('active').css('display','block');
  
});
$('body').on('click', '#firstli', function () {
    $('#mytabs #firstli').addClass('active');
    $('#mytabs #secondli').removeClass('active');
    
    $('#personal').addClass('active').css('display','block');
    $('#addresstab').removeClass('active').css('display','none');
    $('#attachmentss').removeClass('active').css('display','none');
    $('#attachments').removeClass('active').css('display','none');
    $('#skills').removeClass('active').css('display','none');
});
$('body').on('click', '#secondli', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #secondli').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#attachmentss').removeClass('active').css('display','none');
    $('#attachments').removeClass('active').css('display','none');
    $('#skills').removeClass('active').css('display','none');
    $('#addresstab').addClass('active').css('display','block');
});

$('body').on('click', '#four', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #secondli').removeClass('active');
    $('#mytabs #five').removeClass('active');
    $('#mytabs #four').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#addresstab').removeClass('active').css('display','none');
    $('#attachmentss').removeClass('active').css('display','none');
    $('#skills').removeClass('active').css('display','none');
    $('#attachments').addClass('active').css('display','block');
});
$(document).ready(function() {
    $('#personal').addClass('active').css('display','block');
    $('#addresstab').removeClass('active').css('display','none');
    $('#attachmentss').removeClass('active').css('display','none');
    // $('#attachments').removeClass('active').css('display','none');
    $('#four').removeClass('active').css('display','none');
    $('#skills').removeClass('active').css('display','none');

var selectedValue = $('#employee_type').val();

if (selectedValue === '1') {
  $('#four').show(); // Show the "Daily Preference" tab
} else {
  $('#four').hide();
  $('#clicksecond').hide(); 
 $('#submitBtn').css('display','block');  // Hide the "Daily Preference" tab
}
if (selectedValue === '2') {
                $('.payment_mode_customer1').show();
                $('.payment_mode_customer2').show();
                $('.payment_mode_customer3').show();
            } else {
                $('.payment_mode_customer1').hide();
                $('.payment_mode_customer2').hide();
                $('.payment_mode_customer3').hide();
            }
   

});
$('#employee_type').on('change', function() {
            var selectedValue = $(this).val();
            if (selectedValue === '1') {
                $('#four').show(); // Show the tab
                $('#submitBtn').css('display','none');
                $('#clicksecond').show(); 
            } else {
                $('#four').hide(); // Hide the tab
                $('#clicksecond').hide(); 
                $('#submitBtn').css('display','block'); 
                
            }
            if (selectedValue === '2') {
                $('.payment_mode_customer1').show();
                $('.payment_mode_customer2').show();
                $('.payment_mode_customer3').show();
            } else {
                $('.payment_mode_customer1').hide();
                $('.payment_mode_customer2').hide();
                $('.payment_mode_customer3').hide();
            }
        });

        $('body').on('click', '#clicksecond', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #secondli').removeClass('active');
    $('#mytabs #four').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#addresstab').removeClass('active').css('display','none');
   $('#attachments').addClass('active').css('display','block');
  
});
$('body').on('click', '#five', function () {
    $('#mytabs #firstli').removeClass('active');
    $('#mytabs #secondli').removeClass('active');
    $('#mytabs #four').removeClass('active');
    $('#mytabs #five').addClass('active');
    
    $('#personal').removeClass('active').css('display','none');
    $('#addresstab').removeClass('active').css('display','none');
    $('#attachments').removeClass('active').css('display','none');
    $('#skills').addClass('active').css('display','block');
});
       
      
</script>
<script>
$(document).ready(function() {
  $('#skills option').mousedown(function(e) {
    e.preventDefault();
    $(this).prop('selected', !$(this).prop('selected'));
  });
});
</script>