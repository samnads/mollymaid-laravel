<script>
  // Pass the PHP array of drivers to JavaScript
  var drivers = <?php echo json_encode($drivers); ?>;
</script>
<style>
.save-btn {
  background-color: #28a745;
  color: white;
  padding: 8px 16px;
  border-radius: 4px;
  cursor: pointer;
}
</style>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Driver Schedules</h3></li>
        <li class="tab2 tab3 tab1" >
          <input type="text" id="service_date" name="service_date" value="<?=DateTime::createFromFormat('Y-m-d', $service_date)->format('d/m/Y')?>" autocomplete="off" readonly>
        </li>
        
        <li class="tab4">
          <span>From Date:</span><input type="text" id="service_date1" name="service_date1" value="<?=DateTime::createFromFormat('Y-m-d', $service_date)->format('d/m/Y')?>" autocomplete="off" readonly>
        </li>
        <li class="tab4">
          <span>To Date:</span><input type="text" id="service_date2" name="service_date2" value="<?=DateTime::createFromFormat('Y-m-d', $service_date)->format('d/m/Y')?>" autocomplete="off" readonly>
        </li> 
        <li class="tab2 tab3 tab4" style="display:none">
          <select name="filter_driver_id" data-placeholder="-- Select driver --" class="sel2 tab2" style="width:180px">
            <option>-- All Drivers --</option>
            <?php foreach($drivers as $key => $driver): ?>
              <option value="<?= $driver->driver_id; ?>"><?= $driver->driver_name; ?></option>
            <?php endforeach; ?>
          </select>
        </li>
        <li class="tab2 tab3" style="display:none">
          <select name="filter_maid_id" data-placeholder="-- Select driver --" class="sel2 tab2" style="width:180px">
            <option>-- All Maids --</option>
            <?php foreach($maids as $key => $maid): ?>
              <option value="<?= $maid->maid_id; ?>"><?= $maid->maid_name; ?></option>
            <?php endforeach; ?>
          </select>
        </li>
        <li class="tab2 tab3 tab4" style="display:none">
          <select name="filter_zone_id" data-placeholder="-- Select Zone --" class="sel2 tab4" style="width:180px">
            <option>-- All Zones --</option>
            <?php foreach($zones as $key => $zone): ?>
              <option value="<?= $zone->zone_id; ?>"><?= $zone->zone_name; ?></option>
            <?php endforeach; ?>
          </select>
        </li>
        <!-- <li class="tab2" style="display:none">
          <button class="btn btn-info" data-action="confirm-driver-mapping-popup"><i class="fa fa-check" aria-hidden="true"></i> Confirm</button>
        </li> -->
        
        <li class="tab1 tab4" style="display:none">
          <button class="btn btn-info" data-action="open-new-driver-mapping"><i class="fa fa-plus" aria-hidden="true"></i> New</button>
        </li>
        <li class="mr-0 float-right tab3" style="display:none">
          <div class="topiconnew border-0 green-btn">
            <a id="printButnforschedule" title="Print"> <i class="fa fa-print"></i></a>
          </div>
        </li>
        </ul>
      </div>
      <!-- /widget-header -->
      <div id="exTab2" class="">
        <ul class="nav nav-tabs">
          <li>
            <a data-target="#tab4" data-toggle="tab" data-tab="tab4">Mapping (Active)</a>
          </li>
          <li>
            <a data-target="#tab1" data-toggle="tab" data-tab="tab1">Mapping (This Date)</a>
          </li>
          <li>
            <a data-target="#tab2" data-toggle="tab" data-tab="tab2">Unconfirmed</a>
          </li>
          <li>
            <a data-target="#tab3" data-toggle="tab" data-tab="tab3">Confirmed</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab4">
            <table class="table table-bordered">
              <thead>
              <tr>
                <th width="20%" style="background-color: #112e3a;">Driver</th>
                <th width="20%" style="background-color: #112e3a;">Zone</th>
                <th width="20%" style="background-color: #112e3a;">Area</th>
                <th width="20%" style="background-color: #112e3a;">Date From</th>
                <th width="20%" style="background-color: #112e3a;">Date To</th>
                <th width="20%" style="background-color: #112e3a;">Days Left</th>
                <th width="20%" style="background-color: #112e3a;">Action</th>
              </tr>

              <thead>
                  </thead>
            </table>
            <button type="button"  data-action="delete-now" class="n-btn red-btn mb-0" style="display:none">Delete</button>
            <div class="col-sm-3 n-field-box pl-0 all-mapped">
              <div class="n-end n-pick-maids-set">
                <input id="check_all2" type="checkbox">
                <label for="check_all2">
                  <span class="border-radius-3"></span>
                  <p>Check All</p>
                </label>
              </div>
            </div>
            <div id="all-mapped-drivers-holder">
            </div>
          </div>
          <div class="tab-pane" id="tab1">
            <table class="table table-bordered">
              <thead>
              <tr>
                <th width="20%" style="background-color: #112e3a;">Driver</th>
                <!-- <th width="20%" style="background-color: #112e3a;">Area & Zone</th> -->
                <th width="20%" style="background-color: #112e3a;">Zone</th>
                <!-- <th width="20%" style="background-color: #112e3a;">Area</th> -->
                <th width="20%" style="background-color: #112e3a;">Date From</th>
                <th width="20%" style="background-color: #112e3a;">Date To</th>
                <th width="20%" style="background-color: #112e3a;">Days Left</th>
                <th width="20%" style="background-color: #112e3a;">Action</th>
              </tr>
              <thead>
                  </thead>
            </table>
            <button type="button"  data-action="mapped-delete-now" class="n-btn red-btn mb-0" style="display:none">Delete</button>
            <div class="col-sm-3 n-field-box pl-0 mapped-drivers">
              <div class="n-end n-pick-maids-set">
                <input id="check_all1" type="checkbox">
                <label for="check_all1">
                  <span class="border-radius-3"></span>
                  <p>Check All</p>
                </label>
              </div>
            </div>
            <div id="mapped-drivers-holder">
              <table class="table table-bordered table-hover" width="100%" style="display:none">
              <!-- for layout test only -->
                <thead>
                <tr style="background-color: #758b93 !important;">
                  <th colspan="4">Loading...</th>
                </tr>
                </thead>
                <tr>
                  <td width="30%"></td>
                  <td width="24%">Loading...</td>
                  <td width="23%">Loading...</td>
                  <td width="23%">Loading...</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div id="schedules-with-unconfirmed-drivers">
              <table class="table table-bordered table-hover" width="100%">
              <!-- for layout test only -->
                <thead>
                  <tr>
                    <th style="background-color: #112e3a; width:10%"></th>
                    <th style="background-color: #112e3a;">Sl. No.</th>
                    <th style="background-color: #112e3a;">Service ID.</th>
                    <th style="background-color: #112e3a;">Customer</th>
                    <th style="background-color: #112e3a;">Maid</th>
                    <th style="background-color: #112e3a;">Pick-up Time</th>
                    <th style="background-color: #112e3a;">Pick-up Address</th>
                    <th style="background-color: #112e3a;">Drop Off Time</th>
                    <th style="background-color: #112e3a;">Drop Address</th>
                    <th style="background-color: #112e3a;">Zone</th>
                    <th style="background-color: #112e3a;">Area</th>
                    <th style="background-color: #112e3a;">Driver</th>
                    <th style="background-color: #112e3a;">Action</th>
                  </tr>
                  <div class="col-sm-4 n-field-box pl-0 all-mapped">
                    <div class="n-end n-pick-maids-set">
                      <input id="check_all4" type="checkbox">
                      <label for="check_all4">
                        <span class="border-radius-3"></span>
                        <p>Check All</p>
                      </label>
                    </div>
                    <button class="btn btn-info" data-action="confirm-mapping"><i class="fa fa-check" aria-hidden="true"></i> Confirm</button>
                    <button class="btn red-btn" data-action="delete-unconfirmed"><i class="fa fa-check" aria-hidden="true"></i> Delete</button>
                    <button class="btn btn-info" data-action="reset-unconfirmed"><i class="fa fa-check" aria-hidden="true"></i> Reset</button>

                  </div>
                </thead>
                <tbody>
                  <tr style="display:none">
                    <td width="30%"></td>
                    <td width="24%">Loading...</td>
                    <td width="23%">Loading...</td>
                    <td width="23%">Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane" id="tab3">
            <div id="schedules-with-confirmed-drivers">
              <table class="table table-bordered table-hover" width="100%">
              <!-- for layout test only -->
                <thead>
                  <tr>
                  <th style="background-color: #112e3a; width:10%"></th>
                    <th style="background-color: #112e3a;">Sl. No.</th>
                    <!-- <th style="background-color: #112e3a;">Service ID.</th> -->
                    <th style="background-color: #112e3a;">Driver Name</th>
                    <!-- <th style="background-color: #112e3a;">Customer</th> -->
                    <th style="background-color: #112e3a;">Maid Name</th>
                    <th style="background-color: #112e3a;">Pick</th>
                    <th style="background-color: #112e3a;">Pick-up Location</th>
                    <th style="background-color: #112e3a;">Drop</th>
                    <th style="background-color: #112e3a;">Drop Location</th>
                    <!-- <th style="background-color: #112e3a;">Zone</th> -->
                    <!-- <th style="background-color: #112e3a;">Area</th> -->
                   
                    <th style="background-color: #112e3a;">Action</th>
                  </tr>
                  <button type="button"  data-action="rollback_confirm_driver" class="n-btn blue-btn mb-0" style="display:none">Rollback</button>
                  <div class="col-sm-3 n-field-box pl-0 all-mapped">
                    <div class="n-end n-pick-maids-set">
                      <input id="check_all3" type="checkbox">
                      <label for="check_all3">
                        <span class="border-radius-3"></span>
                        <p>Check All</p>
                      </label>
                    </div>
                  </div>
                </thead>
                <tbody>
               
                <tr style="display:none">
                    <td width="30%"></td>
                    <td width="24%">Loading...</td>
                    <td width="23%">Loading...</td>
                    <td width="23%">Loading...</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="widget-content">

      </div>
    </div>
  </div>
</div>
<div id="divForPrintSchedule" style="display: none">
  <div class="widget-content" style="margin-bottom:30px">
    <table border="1" width="70%" cellspacing="0" cellpadding="0" align="center">
      <thead>
        <tr>
       
          <th style="background-color: #112e3a;">Sl. No.</th>
          <!-- <th style="background-color: #112e3a;">Service ID.</th> -->
          <th style="background-color: #112e3a;">Driver Name</th>
          <!-- <th style="background-color: #112e3a;">Customer</th> -->
          <th style="background-color: #112e3a;">Maid Name</th>
          <th style="background-color: #112e3a;">Pick</th>
          <th style="background-color: #112e3a;">Pick-up Location</th>
          <th style="background-color: #112e3a;">Drop</th>
          <th style="background-color: #112e3a;">Drop Location</th>
                   
        </tr>
      </thead>
      <tbody>
      
      <tr style="display:none">
          <td width="30%"></td>
          <td width="24%">Loading...</td>
          <td width="23%">Loading...</td>
          <td width="23%">Loading...</td>
        </tr>
           
      </tbody>
    </table>
  </div>
  <!-- /widget-content -->
</div>
<?php
$this->load->view('driver/driver-new-availability-popup');
$this->load->view('driver/driver-change-popup');
$this->load->view('driver/confirm-driver-mapping-popup');
$this->load->view('driver/driver-edit-availability-popup');

?>