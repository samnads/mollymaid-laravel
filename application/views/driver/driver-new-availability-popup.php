<div id="new-driver-mapping-popup" style="background:none;display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">Driver Availability</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="new_driver_mapping_form">
    <div class="modal-body">
      <div class="controls">
        <div id="">
          <div class="row m-0">
            <div class="col-sm-6 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Driver</p>
                <div class="n-field-box">
                  <select name="driver_id" data-placeholder="-- Select driver --" class="sel2" style="width:100%">
                  <option value=""></option>
                  <?php foreach($drivers as $key => $driver): ?>
                    <option value="<?= $driver->driver_id; ?>"><?= $driver->driver_name; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-6 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Zone</p>
                <div class="n-field-box">
                  <select name="zone_id" data-placeholder="-- Select zone --" class="sel2" style="width:100%">
                  <option value=""></option>
                  <?php foreach($zones as $key => $zone): ?>
                    <option value="<?= $zone->zone_id; ?>"><?= $zone->zone_name; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-6 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Area</p>
                <div class="n-field-box">
                  <select name="area_id[]" data-placeholder="-- Select area --" style="width:100%" multiple="">
                  <option value=""></option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 input-daterange">
            <div class="col-sm-5 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Date From</p>
                <div class="n-field-box">
                  <input name="date_from" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-2 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>&nbsp;</p>
                <div class="text-center">
                  <i class="fa fa-arrows-h" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-5 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Date To</p>
                <div class="n-field-box">
                  <input name="date_to" readonly>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
  </form>
</div>
</div>
