<div id="confirm-driver-mapping-popup" style="background:none;display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span class="title">Confirm Driver Dispatch</span>
         <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <form id="confirm_driver_mapping_popup_form">
         <div class="modal-body">
            <div class="row m-0 n-field-main" style="border-bottom: 1px solid #ddd;">
               <div class="col-sm-3 n-field-box pl-0">
                  <div class="n-end n-pick-maids-set">
                     <input id="check_all" type="checkbox">
                     <label for="check_all">
                        <span class="border-radius-3"></span>
                        <p>Check All</p>
                     </label>
                  </div>
               </div>
            </div>
            <div class="inner p-2" style="height: 260px;overflow-x:hidden;overflow-y:scroll;">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
            <button type="submit" class="n-btn mb-0">Confirm</button>
         </div>
      </form>
   </div>
</div>