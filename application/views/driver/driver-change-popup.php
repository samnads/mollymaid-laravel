<div id="schedule-driver-change-popup" style="background:none;display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">Change Driver</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="schedule_driver_change_form">
    <input type="hidden" name="day_service_id">
    <div class="modal-body">
      <div class="controls">
        <div id="">
          <div class="row m-0">
            <div class="col-sm-12 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Mapped Driver</p>
                <div class="n-field-box">
                  <select name="driver_availability_driver_id" data-placeholder="-- Not Available --" class="sel2" style="width:100%" disabled>
                  <option value=""></option>
                  <?php foreach($drivers as $key => $driver): ?>
                    <option value="<?= $driver->driver_id; ?>"><?= $driver->driver_name; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-12 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Selected Driver</p>
                <div class="n-field-box">
                  <select name="driver_id" data-placeholder="-- Select driver --" class="sel2" style="width:100%">
                  <option value=""></option>
                  <?php foreach($drivers as $key => $driver): ?>
                    <option value="<?= $driver->driver_id; ?>"><?= $driver->driver_name; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" class="n-btn mb-0">Update</button>
    </div>
  </form>
</div>
</div>
