<div id="edit-driver-mapping-popup" style="background:none;display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">Edit Driver Availability</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="edit_driver_mapping_form">
    <div class="modal-body">
      <div class="controls">
        <div id="">
          <div class="row m-0">
            <div class="col-sm-6 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Driver</p>
                <div class="n-field-box">
                    <input type="hidden" name="edit_driver_avalability_id" id="edit_driver_avalability_id">
                  <select name="edit_driver_id" id="edit_driver_id" data-placeholder="-- Select driver --" class="sel2" style="width:100%">
                  <option value=""></option>
                  <?php foreach($drivers as $key => $driver): ?>
                    <option value="<?= $driver->driver_id; ?>"><?= $driver->driver_name; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-6 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Zone</p>
                <div class="n-field-box">
                  <select name="edit_zone_id" id="edit_zone_id" data-placeholder="-- Select zone --" class="sel2" style="width:100%">
                  <option value=""></option>
                  <?php foreach($zones as $key => $zone): ?>
                    <option value="<?= $zone->zone_id; ?>"><?= $zone->zone_name; ?></option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-6 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Area</p>
                <div class="n-field-box">
                    <select name="edit_area_id[]" id="edit_area_id" class="sel2" style="width:100%" multiple data-searchContain='true'>
                        <option value="">-- Select Area --</option> <?php
                       foreach($areas as $key => $area): ?>
                            <option value="<?= $area->area_id; ?>"><?= $area->area_name; ?></option>
                        <?php endforeach; ?>
                            
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 input-daterange">
            <div class="col-sm-5 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Date From</p>
                <div class="n-field-box">
                  <input name="edit_date_from" id="edit_date_from" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-2 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>&nbsp;</p>
                <div class="text-center">
                  <i class="fa fa-arrows-h" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-5 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Date To</p>
                <div class="n-field-box">
                  <input name="edit_date_to" id="edit_date_to" readonly>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
  </form>
</div>
</div>




<div id="delete-driver-mapping-popup" style="background:none;display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">Delete Driver Availability</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="delete_driver_mapping_form">
    <div class="modal-body">
    <input type="hidden" name="driver_availability_id" value="">
    <div class="controls">
        <div class="row m-1 mt-4 ">
            <h6 class='text-center'style="font-family:'verdana';color:#4d4d00;font-size:20px;text-transform: capitalize;">
            <strong>Are you sure you want to delete this driver availability ? </strong>
          </h6>
        </div>
    </div>

    </div>
    <div class="modal-footer mt-5 col-4">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="button" onclick="delete_driver_availability()" class="n-btn mb-0">Delete</button>
    </div>
  </form>
</div>
</div>