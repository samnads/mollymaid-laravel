<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet" />
<style>
    select[multiple] {
        height: 100px;
        width: 500px;
    }
</style>
<div id="crop-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Image Cropper</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
        </div>
        <div id="" class="col-12 p-0">
            <div class="modal-body">
                <div class="img-container">
                    <img id="image" src="#">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
                <button type="button" class="n-btn mb-0" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>




<div class="row m-0">
    <div class="col-md-12">
        <div class="widget ">
            <div class="widget-header">

                <ul>
                    <li>
                        <i class="icon-user"></i>
                        <h3>New Staff</h3>

                    </li>


                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<? php // echo base_url();
                                                                                            ?>maids"><img src="<? php // echo base_url(); 
                                                                                                                                        ?>img/list_female_user.png" title="Maid List"/></a>-->




                    <li class="mr-0 float-right">
                        <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>maids" title="Maid List"> <i class="fa fa-users"></i></a> </div>
                    </li>
                </ul>


            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">

                    <ul class="nav nav-tabs">
                        <li class="active" id="firstli"><a href="#personal" data-toggle="tab">Personal Details</a></li>

                        <li id="secondli"><a href="#addresstab" data-toggle="tab">Address</a></li>
                        <li id="four"><a href="#attachments" data-toggle="tab">Daily Preference</a></li>
                        <li id="five"><a href="#skills" data-toggle="tab">Skills</a></li>


                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->

                                <?php
                                if ($message == "success") { ?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                        <div class="controls">
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                <strong>Success!</strong> Maid added Successfully.
                                            </div>
                                        </div> <!-- /controls -->
                                    </div>
                                <?php
                                }
                                ?>


                                <fieldset>
                                    <div class="col-sm-4">


                                        <fieldset>
                                            <div class="row m-0 n-field-main">
                                                <p>Employee Type</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="employee_type" id="employee_type" class="">
                                                        <option value="">Select Employee Type</option>
                                                        <?php
                                                        if (count($employee_type) > 0) {
                                                            foreach ($employee_type as $teamsVal) {
                                                        ?>
                                                                <option value="<?php echo $teamsVal->employee_type_id; ?>"><?php echo $teamsVal->employee_type ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="row m-0 n-field-main">
                                                <p>Name</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" class="" id="maid_name" name="maid_name">
                                                </div>
                                            </div>







                                            <div class="row m-0 n-field-main">
                                                <p>Gender</p>
                                                <div class="col-sm-12 p-0 n-field-box">


                                                    <div class="row m-0">
                                                        <div class="col-sm-6 n-radi-check-main p-0">
                                                            <input type="radio" value="M" id="gender" name="gender" class="" checked="">
                                                            <label for="gender"> <span class="border-radius-3"></span>
                                                                <p>Male</p>
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-6 n-radi-check-main p-0">
                                                            <input type="radio" value="F" id="gender-f" name="gender" class="">
                                                            <label for="gender-f"> <span class="border-radius-3"></span>
                                                                <p>Female</p>
                                                            </label>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>










                                            <div class="row m-0 n-field-main">
                                                <p>Nationality</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" class="" id="nationality" name="nationality">
                                                </div>
                                            </div>








                                            <!--<div class="row m-0 n-field-main">
                                                        <p>Upload Photo</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <div id="me" class="styleall" style=" cursor:pointer;">
                                                                <span style=" cursor:pointer;">
                                                                    <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>-->


                                            <div class="row m-0 n-field-main">
                                                <p>Upload Photo</p>
                                                <label>
                                                    <img class="rounded" id="avatar_img" src="<?= base_url('uploads/images/default/maid-avatar-upload.png?v=' . IMG_VERSION); ?>" style="width:80px;" alt="Promotion Image">
                                                    <input type="file" class="sr-only" id="input-image" accept="image/*">
                                                </label>
                                                <input type="hidden" name="avatar_base64" id="avatar_base64">
                                            </div>









                                            <div class="row m-0 n-field-main">
                                                <p>Present Address</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea class="" rows="5" id="present_address" name="present_address"></textarea>
                                                </div>
                                            </div>









                                            <div class="row m-0 n-field-main">
                                                <p>Permanent Address</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea class="" rows="5" id="permanent_address" name="permanent_address"></textarea>
                                                </div>
                                            </div>



                                            <div class="row m-0 n-field-main">
                                                <p>Date of Joining</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" class="" id="doj" name="doj">
                                                </div>
                                            </div>






                                            <div class="row m-0 n-field-main">
                                                <p>Mobile Number 1</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" class="" id="mobile1" name="mobile1">
                                                </div>
                                            </div>







                                            <div class="row m-0 n-field-main">
                                                <p>Mobile Number 2</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" class="" id="mobile2" name="mobile2">
                                                </div>
                                            </div>

                                            <div class="row m-0 n-field-main">
                                                <p>WhatsApp No.</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="number" autocomplete="off" class="" id="whatsapp_number_1" name="whatsapp_number_1">
                                                </div>
                                            </div>





                                            <div class="row m-0 n-field-main">
                                                <p>Flat</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="flat" id="flat" class="" required>
                                                        <?php
                                                        if (count($flats) > 0) {
                                                            foreach ($flats as $flatsVal) {
                                                        ?>
                                                                <option value="<?php echo $flatsVal['flat_id'] ?>"><?php echo $flatsVal['flat_name'] ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>








                                            <div class="row m-0 n-field-main">
                                                <p>Team</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="team" id="team" class="">
                                                        <option value="">Select Team</option>
                                                        <?php
                                                        if (count($teams) > 0) {
                                                            foreach ($teams as $teamsVal) {
                                                        ?>
                                                                <option value="<?php echo $teamsVal->team_id; ?>"><?php echo $teamsVal->team_name; ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>










                                        </fieldset>



                                    </div>


                                    <div class="col-sm-1"></div>




                                    <div class="col-sm-4">
                                        <div id="target-2" class="">




                                            <div class="row m-0 n-field-main" id="leaderbox" style="display:none">
                                                <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
                                                    <div class="col-sm-12 pr-0 pl-0 n-radi-check-main">
                                                        <input id="leader" type="checkbox" value="1" name="leader" class="services" checked>
                                                        <label for="leader"> <span class="border-radius-3"></span>
                                                            <p> Is Team Leader</p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row m-0 n-field-main" id="teamLeaderSelectBox" style="display:none">
                                                <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
                                                    <label for="teamLeaderSelect">Select Team Leader:</label>
                                                    <select name="teamLeaderSelect" id="teamLeaderSelect">
                                                        <option value="" selected>Select team leader</option>
                                                        <?php

                                                        if (count($team_leaders) > 0) {
                                                            foreach ($team_leaders as $Val) {
                                                        ?>
                                                                <option value="<?php echo $Val['maid_id']; ?>"><?php echo $Val['maid_name']; ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>









                                            <div class="row m-0 n-field-main">
                                                <p>Services</p>

                                                <?php
                                                if (count($services) > 0) {
                                                    foreach ($services as $key => $serviceVal) {
                                                ?>







                                                        <div class="col-sm-12 pr-0 pl-0 n-field-box pb-3">
                                                            <div class="col-sm-12 pr-0 pl-0 n-radi-check-main">
                                                                <input id="services<?= $key ?>" type="checkbox" value="<?php echo $serviceVal['service_type_id'] ?>" name="services[]" class="services">
                                                                <label for="services<?= $key ?>"> <span class="border-radius-3"></span>
                                                                    <p><?php echo $serviceVal['service_type_name'] ?></p>
                                                                </label>
                                                            </div>
                                                        </div>






                                                <?php
                                                    }
                                                }
                                                ?>

                                            </div>







                                            <div class="row m-0 n-field-main">
                                                <p>Notes</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea class="" rows="5" id="notes" name="notes"></textarea>
                                                </div>
                                            </div>






                                            <div class="row m-0 n-field-main">
                                                <p>Username</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" class="" id="username" name="username">
                                                </div>
                                            </div>








                                            <div class="row m-0 n-field-main">
                                                <p>Password</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="password" autocomplete="off" class="" id="password" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                                                </div>
                                            </div>
                                            <div class="row m-0 n-field-main payment_mode_customer" style="">
                                                <p>Priority Number</p>
                                                <div class="col-sm-12 pr-0 pl-0 n-field-box">
                                                    <input type="text" id="maid-priority" name="maid_priority">
                                                </div>
                                            </div>

                                            <div class="row m-0 n-field-main payment_mode_customer1" style="display:none;">
                                                <p>Driver location</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select id="d_location" name="d_location">
                                                        <option value="">select location</option>
                                                        <?php
                                                        if (count($locations) > 0) {
                                                            foreach ($locations as $teamsVal) {
                                                        ?>
                                                                <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 n-field-main payment_mode_customer2" style="display:none;">
                                                <p>From Date</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="date" id="from_date" name="from_date" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="row m-0 n-field-main payment_mode_customer3" style="display:none;">
                                                <p>To Date</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="date" id="to_date" name="to_date" autocomplete="off">
                                                </div>
                                            </div>








                                        </div>
                                    </div>

                                </fieldset>
                                <div class="col-sm-12">

                                    <!-- <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/> -->
                                    <!--  <input type="submit" class="n-btn" value="Save" name="maid_sub" onclick="return validate_maid();"> -->
                                    <div class="col-sm-12"><button type="button" class="n-btn" id="clickfirst">Next</button></div>




                                </div>
                            </div>


                            <div class="tab-pane" id="attachments">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <!-- <p>Day</p> -->
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="sunday-checkbox" type="checkbox" value="0" name="days[]">
                                                            <label for="sunday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Sunday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="monday-checkbox" type="checkbox" value="1" name="days[]">
                                                            <label for="monday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Monday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="tuesday-checkbox" type="checkbox" value="2" name="days[]">
                                                            <label for="tuesday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Tuesday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="Wednesday-checkbox" type="checkbox" value="3" name="days[]">
                                                            <label for="Wednesday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Wednesday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="thursday-checkbox" type="checkbox" value="4" name="days[]">
                                                            <label for="thursday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Thursday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="friday-checkbox" type="checkbox" value="5" name="days[]">
                                                            <label for="friday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Friday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input id="saturday-checkbox" type="checkbox" value="6" name="days[]">
                                                            <label for="saturday-checkbox">
                                                                <span class="border-radius-3"></span>
                                                                <p>Saturday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-sm-3">
                                            <fieldset>
                                                <div class="n-field-main">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">

                                                    <div class="n-field-box">
                                                        <select id="" name="locations[]">
                                                            <option value="">select location</option>
                                                            <?php
                                                            if (count($locations) > 0) {
                                                                foreach ($locations as $teamsVal) {
                                                            ?>
                                                                    <option value="<?php echo $teamsVal['location_id']; ?>"><?php echo $teamsVal['location_name'] ?></option>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>


                                        </div>
                                        <div class="col-sm-3">
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_from[]" placeholder="Time From">
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </div>
                                        <div class="col-sm-3">

                                            <fieldset>

                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="n-field-main" style="margin-top: 10px;">
                                                    <div class="n-field-box">
                                                        <input type="time" id="" name="time_to[]" placeholder="Time to">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="col-sm-12">

                                    <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload" />
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg" />
                                    <input type="hidden" name="img_name_resp" id="img_name_resp" />
                                    <div class="col-sm-12"><button type="button" class="n-btn" id="clickthird">Next</button>
                                        <input type="submit" class="n-btn" style="display: none;" value="Save" id="submitBtn" name="maid_sub" onclick="return validate_maid();">
                                    </div>



                                </div>
                            </div>





                            <div class="tab-pane" id="skills">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="tab-pane" id="skills">
                                                <fieldset>
                                                    <div class="row m-0 n-field-main">
                                                        <p style="color:red;">Select Skills <input type="submit" style="background-color: #3fb9f4" value="Add New" name="" id="addNewButton"> </p>


                                                        <div class="col-sm-12 p-0 n-field-box">
                                                            <div id="newSkillInput" style="display: none;">
                                                                <input type="text" name="newSkillInput" placeholder="Enter a new skill"></br></br>
                                                                <div class="col-sm-6 p-0 n-field-box">
                                                                    <input type="submit" class="n-btn" value="Add" name="skill_add">
                                                                </div></br>
                                                            </div>
                                                        </div> </br></br>

                                                        <div class="col-sm-12 p-0 n-field-box">
                                                            <select id="skills" name="skills[]" multiple>

                                                                <!-- <option value="" selected>Select Skill</option> -->
                                                                <?php
                                                                if (count($skills) > 0) {

                                                                    foreach ($skills as $teamsVal) {
                                                                ?>
                                                                        <option value="<?php echo $teamsVal['skill_id']; ?>"><?php echo $teamsVal['skill']; ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></br></br></br>
                                                        <div class="row m-0 n-field-main">
                                                            <p style="color:red;"> Select Level</p>
                                                            <div class="col-sm-12 p-0 n-field-box">
                                                                <select id="level" name="levels[]" multiple>

                                                                    <!-- <option value="" selected>Select Rating Level </option> -->
                                                                    <?php
                                                                    if (count($levels) > 0) {
                                                                        foreach ($levels as $teamsVal) {
                                                                    ?>
                                                                            <option value="<?php echo $teamsVal['rating_level_id']; ?>"><?php echo $teamsVal['rating_level']; ?></option>
                                                                    <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div></br>

                                                        <div class="row m-0 n-field-main">
                                                            <p>Note</p>
                                                            <div class="col-sm-12 p-0 n-field-box">
                                                                <textarea class="" id="" name="note[]"></textarea>
                                                            </div>
                                                        </div>




                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">

                                        <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload" />
                                        <input type="hidden" name="img_fold" id="img_fold" value="maidimg" />
                                        <input type="hidden" name="img_name_resp" id="img_name_resp" />
                                        <input type="submit" class="n-btn" value="Save" name="maid_sub" onclick="return validate_maid();">



                                    </div>
                                </div>
                            </div>














                            <div class="tab-pane" id="addresstab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="tab-pane" id="addresstab">
                                                <fieldset>
                                                    <p style="color:red;">Local Address</p>
                                                    <div class="row m-0 n-field-main">
                                                        <p>House Number</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="local_house" name="local_house">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Street</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="local_street" name="local_street">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Town</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="local_town" name="local_town">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>City</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="local_city" name="local_city">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Country</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <!-- <input type="text" class="" id="local_country" name="local_country"> -->
                                                            <select name="local_country" id="local_country" class="">
                                                                <option value="">Select Country</option>
                                                                <?php
                                                                if (count($countries) > 0) {
                                                                    foreach ($countries as $teamsVal) {
                                                                ?>
                                                                        <option value="<?php echo $teamsVal['id']; ?>"><?php echo $teamsVal['country'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div id="target-2">
                                                <fieldset>
                                                    <p style="color:red;">Permanent Address</p>
                                                    <!-- Address fields -->
                                                    <div class="row m-0 n-field-main">
                                                        <p>House Number</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="perm_house" name="perm_house">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Street</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="perm_street" name="perm_street">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Town</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="perm_town" name="perm_town">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>City</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="perm_city" name="perm_city">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Country</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <!-- <input type="text" class="" id="perm_country" name="perm_country"> -->
                                                            <select name="perm_country" id="perm_country" class="">
                                                                <option value="">Select Country</option>
                                                                <?php
                                                                if (count($countries) > 0) {
                                                                    foreach ($countries as $teamsVal) {
                                                                ?>
                                                                        <option value="<?php echo $teamsVal['id']; ?>"><?php echo $teamsVal['country'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div id="target-3">
                                                <fieldset>
                                                    <p style="color:red;">Emergency Contact-Local</p>
                                                    <!-- Address fields -->
                                                    <div class="row m-0 n-field-main">
                                                        <p>Name</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_local_name" name="emergency_local_name">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Relationship</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_local_relationship" name="emergency_local_relationship">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Contact No</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_local_contact" name="emergency_local_contact">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Whatsapp No</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_local_whatsapp" name="emergency_local_whatsapp">
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div id="target-4">
                                                <fieldset>
                                                    <p style="color:red;">Emergency Contact-Home Country</p>
                                                    <!-- Address fields -->
                                                    <div class="row m-0 n-field-main">
                                                        <p>Name</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_home_name" name="emergency_home_name">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Relationship</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_home_relationship" name="emergency_home_relationship">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Contact No</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_home_contact" name="emergency_home_contact">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Whatsapp No</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <input type="text" class="" id="emergency_home_whatsapp" name="emergency_home_whatsapp">
                                                        </div>
                                                    </div>
                                                    <div class="row m-0 n-field-main">
                                                        <p>Home Country</p>
                                                        <div class="col-sm-8 p-0 n-field-box">
                                                            <!-- <input type="text" class="" id="emergency_home_country" name="emergency_home_country"> -->
                                                            <select name="emergency_home_country" id="emergency_home_country" class="">
                                                                <option value="">Select Country</option>
                                                                <?php
                                                                if (count($countries) > 0) {
                                                                    foreach ($countries as $teamsVal) {
                                                                ?>
                                                                        <option value="<?php echo $teamsVal['id']; ?>"><?php echo $teamsVal['country'] ?></option>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">

                                        <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload" />
                                        <input type="hidden" name="img_fold" id="img_fold" value="maidimg" />
                                        <input type="hidden" name="img_name_resp" id="img_name_resp" />
                                        <!--  <input type="submit" class="n-btn" value="Save" name="maid_sub" onclick="return validate_maid();"> -->
                                        <!-- <div class="col-sm-12"><button type="button" class="n-btn" id="clicksecondd">Next</button> -->
                                        <!-- <input type="submit" class="n-btn" style="display: none;" value="Save" id="submitBtn" name="maid_sub" onclick="return validate_maid();"> -->
                                    </div>

                                </div>


                            </div>


















                            <!-- <div class="tab-pane" id="attachments">
                                <fieldset>

                                    <div class="col-sm-4">

                                                <fieldset>


                                                    <div class="row m-0 n-field-main">
                                                        <p>Passport Number</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="passport_number" name="passport_number">
                                                        </div>
                                                    </div>







                                                    <div class="row m-0 n-field-main">
                                                        <p>Passport Expiry</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="passport_expiry" name="passport_expiry">
                                                        </div>
                                                    </div>










                                                    <div class="row m-0 n-field-main">
                                                        <p>Attach Passport</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="file" class="" id="attach_passport" name="attach_passport">
                                                        </div>
                                                    </div>







                                                    <div class="row m-0 n-field-main">
                                                        <p>Visa Number</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="visa_number" name="visa_number">
                                                        </div>
                                                    </div>








                                                    <div class="row m-0 n-field-main">
                                                        <p>Visa Expiry</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="visa_expiry" name="visa_expiry">
                                                        </div>
                                                    </div>








                                                    <div class="row m-0 n-field-main">
                                                        <p>Attach Visa</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="file" class="" id="attach_visa" name="attach_visa">
                                                        </div>
                                                    </div>







                                                </fieldset>


                                    </div> 


                                    <div class="col-sm-4">
                                        <div id="target-2" class="">

                                                <fieldset>



                                                    <div class="row m-0 n-field-main">
                                                        <p>Labour Card Number</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="labour_number" name="labour_number">
                                                        </div>
                                                    </div>








                                                    <div class="row m-0 n-field-main">
                                                        <p>Labour Card Expiry</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="labour_expiry" name="labour_expiry">
                                                        </div>
                                                    </div>








                                                    <div class="row m-0 n-field-main">
                                                        <p>Attach Labour Card</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="file" class="" id="attach_labour" name="attach_labour">
                                                        </div>
                                                    </div>








                                                    <div class="row m-0 n-field-main">
                                                        <p>Emirates Id</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="emirates_id" name="emirates_id">
                                                        </div>
                                                    </div>









                                                    <div class="row m-0 n-field-main">
                                                        <p>Emirates Id Expiry</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="text" class="" id="emirates_expiry" name="emirates_expiry">
                                                        </div>
                                                    </div>









                                                    <div class="row m-0 n-field-main">
                                                        <p>Attach Emirates Card</p>
                                                        <div class="col-sm-12 p-0 n-field-box">
                                                             <input type="file" class="" id="attach_emirates" name="attach_emirates">
                                                        </div>
                                                    </div>







                                                </fieldset>

                                        </div>
                                    </div> 

                                </fieldset>  -->


                            <!--                                <div class="form-actions" style="padding-left: 211px;">

                                    <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="btn btn-primary pull-right" value="Submit" name="maid_sub" onclick="return validate_maid();">


                                </div>   -->






                        </div>
                </div>
                </form>
            </div>
        </div> <!-- /widget-content -->
    </div> <!-- /widget -->
</div> <!-- /span8 -->
</div> <!-- /row -->

<script>
    $(function() {
        var current = window.location.href;
        $('#primary_nav_wrap li a').each(function() {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href') === '<?php echo base_url('maids '); ?>') {
                $this.addClass('active');
            }
        })
    })
    var cropper;

    function showCropper() {
        cropper = new Cropper(image, {
            aspectRatio: 300 / 300,
            viewMode: 1,
        });
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#crop-popup'),
        });
    }

    function closeCropper() {
        cropper.destroy();
        cropper = null;
        $.fancybox.close();
    }
    window.addEventListener('DOMContentLoaded', function() {
        var avatar_img = document.getElementById('avatar_img');
        var image = document.getElementById('image');
        var input = document.getElementById('input-image');
        input.addEventListener('change', function(e) {
            var files = e.target.files;
            var done = function(url) {
                input.value = '';
                image.src = url;
                showCropper();
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
                file = files[0];
                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });
        document.getElementById('crop').addEventListener('click', function() {
            var initialpromo_imgURL;
            var canvas;
            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: 300,
                    height: 300,
                });
                initialpromo_imgURL = avatar_img.src;
                avatar_img.src = canvas.toDataURL();
                $('#avatar_base64').val(avatar_img.src);
            }
            closeCropper();
        });
    });
    $(function() {
        let current = window.location.href;
        $('#primary_nav_wrap li a').each(function() {
            var $this = $(this);
            // if the current path is like this link, make it active
            if ($this.attr('href') === '<?php echo base_url('maids'); ?>') {
                $this.addClass('active');
            }
        })
    })
    $('body').on('click', '#clickfirst', function() {
        $('#mytabs #firstli').removeClass('active');
        $('#mytabs #secondli').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        $('#addresstab').addClass('active').css('display', 'block');

    });
    $('body').on('click', '#firstli', function() {
        $('#mytabs #firstli').addClass('active');
        $('#mytabs #secondli').removeClass('active');
        $('#mytabs #five').removeClass('active');
        $('#mytabs #four').removeClass('active');
        // $('#mytabs #thirdli').removeClass('active');

        $('#personal').addClass('active').css('display', 'block');
        $('#addresstab').removeClass('active').css('display', 'none');
        // $('#daily').removeClass('active').css('display','none');
        $('#attachments').removeClass('active').css('display', 'none');
        $('#skills').removeClass('active').css('display', 'none');
    });
    $('body').on('click', '#secondli', function() {
        $('#mytabs #firstli').removeClass('active');
        // $('#mytabs #thirdli').removeClass('active');
        $('#mytabs #four').removeClass('active');
        $('#mytabs #five').removeClass('active');
        $('#mytabs #secondli').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        // $('#daily').removeClass('active').css('display','none');
        $('#attachments').removeClass('active').css('display', 'none');
        $('#skills').removeClass('active').css('display', 'none');
        $('#addresstab').addClass('active').css('display', 'block');
    });

    $('body').on('click', '#thirdli', function() {
        $('#mytabs #firstli').removeClass('active');
        $('#mytabs #secondli').removeClass('active');
        $('#mytabs #five').removeClass('active');
        $('#mytabs #thirdli').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        $('#addresstab').removeClass('active').css('display', 'none');
        $('#skills').removeClass('active').css('display', 'none');
        $('#daily').addClass('active').css('display', 'block');
    });
    $('body').on('click', '#four', function() {
        $('#mytabs #firstli').removeClass('active');
        $('#mytabs #secondli').removeClass('active');
        $('#mytabs #five').removeClass('active');
        $('#mytabs #four').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        $('#addresstab').removeClass('active').css('display', 'none');
        $('#skills').removeClass('active').css('display', 'none');
        $('#attachments').addClass('active').css('display', 'block');
    });

    $('body').on('click', '#clicksecond', function() {
        $('#mytabs #firstli').removeClass('active');
        $('#mytabs #secondli').removeClass('active');
        $('#mytabs #four').removeClass('active');
        $('#mytabs #five').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        $('#addresstab').removeClass('active').css('display', 'none');
        $('#attachments').addClass('active').css('display', 'block');

    });


    $('#employee_type').on('change', function() {
        var selectedValue = $(this).val();
        if (selectedValue === '1') {
            $('#four').show(); // Show the tab
            $('#submitBtn').css('display', 'none');
            $('#clicksecond').show();
            $('#leaderbox').show();
            $('#teamLeaderSelectBox').hide();
        } else {
            $('#four').hide(); // Hide the tab
            $('#clicksecond').hide();
            $('#submitBtn').css('display', 'block');
            $('#leaderbox').hide();
            $('#teamLeaderSelectBox').hide();
        }
        if (selectedValue === '2') {
            $('.payment_mode_customer1').show();
            $('.payment_mode_customer2').show();
            $('.payment_mode_customer3').show();
        } else {
            $('.payment_mode_customer1').hide();
            $('.payment_mode_customer2').hide();
            $('.payment_mode_customer3').hide();
        }
    });
    $(document).ready(function() {
        var selectedValue = $('#employee_type').val();

        if (selectedValue === '1') {
            $('#four').show(); // Show the "Daily Preference" tab
        } else {
            $('#four').hide();
            $('#clicksecond').hide();
            $('#submitBtn').css('display', 'block'); // Hide the "Daily Preference" tab
        }
    });


    $('body').on('click', '#five', function() {
        $('#mytabs #firstli').removeClass('active');
        $('#mytabs #secondli').removeClass('active');
        $('#mytabs #four').removeClass('active');
        $('#mytabs #five').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        $('#addresstab').removeClass('active').css('display', 'none');
        $('#attachments').removeClass('active').css('display', 'none');
        $('#skills').addClass('active').css('display', 'block');
    });


    $('body').on('click', '#clickthird', function() {
        $('#mytabs #firstli').removeClass('active');
        $('#mytabs #secondli').removeClass('active');
        $('#mytabs #four').removeClass('active');
        $('#mytabs #five').addClass('active');

        $('#personal').removeClass('active').css('display', 'none');
        $('#addresstab').removeClass('active').css('display', 'none');
        $('#attachments').removeClass('active').css('display', 'none');
        $('#skills').addClass('active').css('display', 'block');

    });

    document.getElementById('addNewButton').addEventListener('click', function() {
        event.preventDefault();
        showInputBox('newSkillInput');
    });


    function showInputBox(id) {
        document.getElementById(id).style.display = 'block';
    }





    const leaderCheckbox = document.getElementById('leader');
    const teamLeaderSelectBox = document.getElementById('teamLeaderSelectBox');

    // Function to handle the checkbox change event
    function toggleTeamLeaderSelectBox() {

        if (leaderCheckbox.checked) {
            teamLeaderSelectBox.style.display = 'none';
        } else {
            teamLeaderSelectBox.style.display = 'block';
        }
    }

    // Add an event listener to the checkbox for the change event
    leaderCheckbox.addEventListener('change', toggleTeamLeaderSelectBox);

    // Call the function initially to set the initial state
    toggleTeamLeaderSelectBox();
</script>

<script>
    $(document).ready(function() {
        $('#skills option').mousedown(function(e) {
            e.preventDefault();
            $(this).prop('selected', !$(this).prop('selected'));
        });
    });
</script>