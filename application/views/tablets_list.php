<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }
</style>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Activate ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to activate this tablet ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Activate</button>
    </div>
  </div>
</div>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Deactivate ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to deactivate this tablet ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Deactivate</button>
    </div>
  </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Tablet</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="col-12 p-0">
      <form method="post" id="add-new-zone-form">
        <div class="modal-body" style="padding: 15px 30px;">
          <div class="row m-0 n-field-main">
            <p>Zone Name</p>
            <div class="col-sm-12 p-0 n-field-box">
             <select name="zone_id" id="zone_id" class="sel2" style="width:100%" required>
                                                <option value="">-- Select Zone --</option>
                                                <?php
                                                if (count($zones) > 0) {
                                                    foreach ($zones as $zones_val) {
                                                        ?>
                                                        <option value="<?php echo $zones_val['zone_id']; ?>">
                                                            <?php echo $zones_val['zone_name']; ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" readonly="true" id="access_code" name="access_code" required="required" value="<?php echo 'EM' . rand(1000, 2000) ?>">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Driver Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="drivernameval" name="drivernameval">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Tablet IMEI</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="imei" name="imei">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Username</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="username" name="username" required="required">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Password</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="password" name="password" required="required">
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="tablet_sub">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Tablet</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="col-12 p-0">
      <form method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Zone Name</p>
            <div class="col-sm-12 p-0 n-field-box">
             <select name="edit_zone_id" id="edit_zone_id" class="sel" style="width:100%" required>
                                                <option value="">-- Select Zone --</option>
                                                <?php
                                                if (count($zones) > 0) {
                                                    foreach ($zones as $zones_val) {
                                                        ?>
                                                        <option value="<?php echo $zones_val['zone_id']; ?>">
                                                            <?php echo $zones_val['zone_name']; ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Access Code</p>
            <div class="col-sm-12 p-0 n-field-box">
                <input type="text" id="edit_access_code" readonly>
              <input type="hidden" id="edit_tabletid" name="edit_tabletid">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Driver Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="editdrivernameval" name="editdrivernameval">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Tablet IMEI</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_imei" name="edit_imei">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Username</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_username" name="edit_username">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Password</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_password" name="edit_password">
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" name="tablet_edit">Update</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
            <ul>
            <li>
            <i class="icon-th-list"></i>
                <h3>Tablets</h3>
            </li>
            <li>
                <select style="width: 160px; " name="tabletstatus"
                    id="tabletstatus" class="span3">
                    <option value="" <?php echo $tabstatus == 3 ? 'selected="selected"' : ''; ?>>All</option>
                    <option value="1" <?php echo $tabstatus == 1 ? 'selected="selected"' : ''; ?>>Active</option>
                    <option value="0" <?php echo $tabstatus == 0 ? 'selected="selected"' : ''; ?>>Inactive</option>
                </select>
                
             </li>
             <li class="mr-1 float-right">   
                  <div class="topiconnew border-0 green-btn">
              	       <a onclick="newPopup()" title="Add"> <i class="fa fa-plus"></i></a>
                  </div>      
             </li>
                  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <table id="tablets-list-table" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px; text-align:center;">Sl. No.</th>
                            <th style="line-height: 18px">Tablet IMEI</th>
                            <th style="line-height: 18px">Access Code</th>
                            <th style="line-height: 18px">Zone</th>
                            <th style="line-height: 18px">Driver</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($tablets) > 0) {
                            $i = 1;
                            foreach ($tablets as $tablets_val) {
                                ?>
                                <tr>
                                    <td style="line-height: 18px; text-align:center;"><?php echo $i; ?></td>
                                    <td style="line-height: 18px">
                                        <?php echo $tablets_val['imei'] ?>
                                    </td>
                                    <td style="line-height: 18px"><?php echo $tablets_val['access_code'] ?></td>
                                    <td style="line-height: 18px">
                                        <?php echo $tablets_val['zone_name'] ?>
                                    </td>
                                    <td style="line-height: 18px"><?php echo $tablets_val['tablet_driver_name'] ?></td>
                                    <td style="line-height: 18px" class="td-actions">
                                        <a href="javascript:;" class="n-btn-icon purple-btn"
                                            onclick="edit_get(<?php echo $tablets_val['tablet_id'] ?>);"><i
                                                class="btn-icon-only icon-pencil"> </i></a>

                                        <?php if ($tablets_val['tablet_status'] == 1) {
                                            ?>
                                            <a href="javascript:;" class="n-btn-icon green-btn"
                                                onclick="confirm_disable_enable_modal(<?php echo $tablets_val['tablet_id'] ?>,<?php echo $tablets_val['tablet_status'] ?>);"><i
                                                    class="btn-icon-only icon-ok"> </i></a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="javascript:;" class="n-btn-icon red-btn"
                                                onclick="confirm_disable_enable_modal(<?php echo $tablets_val['tablet_id'] ?>,<?php echo $tablets_val['tablet_status'] ?>);"><i
                                                    class="btn-icon-only icon-remove"> </i></a>
                                            <?php
                                        }
                                        ?>

                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>

                    </tbody>
                </table>
            </div> <!-- /widget-content -->
        </div><!-- /widget -->
    </div><!-- /span6 -->
</div>
<script>
    /*********************************************************************************** */
    (function (a) {
        a(document).ready(function (b) {
            if (a('#tablets-list-table').length > 0) {
                a("table#tablets-list-table").dataTable({
                    'sPaginationType': "full_numbers",
                    "bSort": true,
                    "scrollY": true,
                    "orderMulti": false,
                    'bFilter': true,
                    "lengthChange": true,
                    "pageLength": 100,
                    'columnDefs': [
                        {
                            'targets': [-1],
                            'orderable': false
                        },
                    ]
                });
            }
        });
    })(jQuery);
    var disable_tabletid = null;
    var web_status = null;
    function confirm_disable_enable_modal(id, status) {
        if (status == 0) {
            $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#enable-popup'),
  });
        }
        else {
            $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#disable-popup'),
  });
        }
        disable_tabletid = id;
        tablet_status = status;
    }
    function confirm_disable() {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/tablet_status",
            data: { tablet_id: disable_tabletid, status: tablet_status },
            dataType: "text",
            cache: false,
            success: function (result) {
                location.reload();
            },
            error: function (data) {
                alert(data.statusText);
                console.log(data);
            }
        });
    }
    function confirm_enable() {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/tablet_status",
            data: { tablet_id: disable_tabletid, status: tablet_status },
            dataType: "text",
            cache: false,
            success: function (result) {
                location.reload();
            },
            error: function (data) {
                alert(data.statusText);
                console.log(data);
            }
        });
    }
    function edit_get(id) {
      $('.mm-loader').show();
        $.ajax({
            type: "POST",
            url: _base_url + "settings/edit_tablet",
            data: { tablet_id: id },
            dataType: "text",
            cache: false,
            success: function (result) {
                //alert(result);
                var obj = jQuery.parseJSON(result);
                $.each($.parseJSON(result), function (edit, value) {
                    //alert(value.zone_id);
                    $('#edit_tabletid').val(value.tablet_id)
                    $('#edit_access_code').val(value.access_code)
                    $('#edit_imei').val(value.imei)
                    $('#editdrivernameval').val(value.tablet_driver_name)
                    $('#edit_username').val(value.tablet_username == '0' ? '' : value.tablet_username)
                    $('#edit_password').val(value.tablet_password == '0' ? '' : value.tablet_password)
                    $('#edit_zone_id option[value='+value.zone_id+']').attr('selected', 'selected');
                });
                $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#edit-popup'),
  });
  $('.mm-loader').hide();
            },
            error: function (data) {
              $('.mm-loader').hide();
                alert(data.statusText);
                console.log(data);
            }
        });
    }
    function closeFancy(){
  $.fancybox.close();
}
function newPopup(){
  $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#new-popup'),
  });
}
/*********************************************************************************** */
</script>