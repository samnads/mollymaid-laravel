<div id="cancel-one-day-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_one_day_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div>
        <div class="row m-0 n-field-main">
            <div class="col-sm-5 pt-2 n-form-set-left">
                <div class="n-radi-check-main">
                    <input type="hidden" value="5" name="pay_employee">
                    <input type="checkbox" id="pay_employee" value="Y" class="week_day" name="pay_employee">
                    <label for="pay_employee">
                    <span class="border-radius-3"></span>
                    <p>Pay employee</p>
                    </label>
                </div>
            </div>
            <div class="col-sm-5 pt-2 n-form-set-left">
                <div class="n-radi-check-main">
                    <input type="hidden" value="5" name="pay_customer">
                    <input type="checkbox" id="pay_customer" value="Y" class="week_day" name="pay_customer">
                    <label for="pay_customer">
                        <span class="border-radius-3"></span>
                        <p>charge customer</p>
                    </label>
                </div>
            </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-one-day-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-one-day-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_one_day_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <!-- <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div> -->
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
            <div class="col-sm-5 pt-2 n-form-set-left">
                <div class="n-radi-check-main">
                    <input type="hidden" value="5" name="pay_employee1">
                    <input type="checkbox" id="pay_employee1" value="Y" class="week_day" name="pay_employee1">
                    <label for="pay_employee1">
                    <span class="border-radius-3"></span>
                    <p>Pay employee</p>
                    </label>
                </div>
            </div>
            <div class="col-sm-5 pt-2 n-form-set-left">
                <div class="n-radi-check-main">
                    <input type="hidden" value="5" name="pay_customer1">
                    <input type="checkbox" id="pay_customer1" value="Y"  class="week_day" name="pay_customer1">
                    <label for="pay_customer1">
                        <span class="border-radius-3"></span>
                        <p>charge customer</p>
                    </label>
                </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-one-day-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-permeantly-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_permeantly_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel Permanently?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-permeantly-we-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="cancel-permeantly-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="cancel_permeantly_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel permanently?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <div class="row m-0 n-field-main">
          <p>Cancel Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($cancel_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="cancel-permeantly-od-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-one-day-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_one_day_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-one-day-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-one-day-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_one_day_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <!-- <div class="row m-0 n-field-main">
          <p>Date From</p>
          <div class="col-sm-6 p-0 n-field-box">
            <input name="suspend_date_from" id="suspend_date_from" readonly>
          </div>
        </div> -->
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-one-day-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-date-range-we-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_date_range_schedule_we" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-date-range-we-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<div id="suspend-date-range-od-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="suspend_date_range_schedule_od" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to suspend it?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Reasons</p>
          <div class="col-sm-6 p-0 n-field-box">
          <select name="cancel_reason_id" class="sel2" data-placeholder="-- Select Reasons --" style="width:100%">
              <option value="">-- Select Reasons --</option>
              <?php foreach ($suspend_reasons as  $reasons): ?>
                <option value="<?=$reasons->cancel_reason_id;?>"><?=$reasons->cancel_reason;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Remark</p>
          <div class="col-sm-12 p-0">
            <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="suspend-date-range-od-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>
<!-- Enquiry booking form -->
<div id="create-enquiry-schedule-popup" style="display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head bg-od">
    <span class="title">Create Schedule</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="create_enquiry_schedule_booking_form">
    <input name="maid_id" type="hidden">
    <!-- <input name="service_week_day" type="hidden"> -->
    <input name="booking_id" type="hidden">
    <input name="caller_enquiry_typ_ids" type="hidden">
    <input name="enquiry_customer_address_ids" type="hidden">
    <input name="service_start_date1" type="hidden">
    <input name="enquirydate" type="hidden">
    <input name="areaId" type="hidden">
    <input name="locationId" type="hidden">
    <input name="outside_new_area1" type="hidden">
    <input name="enquiry_customer_email1" type="hidden">
    <input name="outside_service_area1" type="hidden">
    <input name="outside_service_checked1" type="hidden">
    <input name="enquirynote" type="hidden">
    <input name="enquirysourceId" type="hidden">
    <input name="service_end_date1" type="hidden">
    
    <div class="modal-body">
      <div class="controls">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer<rf></p>
                <!-- <p>Maid Name<rf></p> -->
                <div class="n-field-box">
                    <input name="customer_enquiryname" id="customer_enquiryname" disabled>
                    <input name="customer_enquiryname1" type="hidden">
                    <input name="customer_enquiryids" type="hidden">
                    <input name="customer_enquiry_phone_no" type="hidden">

                </div>
                <!-- <div class="col-sm-12 p-0 n-field-box"> -->
                  <!-- <select name="customer_ids" id="customer_ids" data-placeholder="-- Select Customer --" style="width:100%"> -->
                  <!-- </select> -->
                  <!-- <input name="customer_name" id="customer_name" readonly> -->
                <!-- </div> -->
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
                <p>Booking Type</p>
                <div class="col-sm-12 p-0 n-field-box">
                    <select name="booking_type" id="booking_type" class="sel2" style="width:100%">
                        <option value="">Select type</option>
                        <?php foreach ($booking_types as $booking_type) : ?>
                            <option value="<?= $booking_type->booking_type ?>">
                                <?= $booking_type->booking_type_name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid Name<rf></p>
                <div class="n-field-box">
                  <input name="maid_name" id="maid_name" disabled>
                </div>
              </div>
            </div>
           
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type<rf></p>
                <div class="n-field-box">
                  <select name="service_type_id" class="sel2" data-placeholder="-- Select Service --" style="width:100%">
                    <option value="">-- Select Service Type --</option>
                    <?php foreach ($services as $key => $service): ?>
                      <option value="<?=$service['service_type_id'];?>"  <?= ($service['service_type_id'] == 6) ? "selected" : "" ?>><?=$service['service_type_name'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time<rf></p>
                <div class="n-field-box">
                  <select name="schedule_enquiry_time_from" id="schedule_enquiry_time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                    <?php
                        $from_time = DateTime::createFromFormat('H:i:s', "08:00:00");
                        $to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
                        $i = 0;
                        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
                            $time_clone = clone $time;
                            $i++;
                            echo '<option value="' . $time->format('H:i:s') . '">' . $time->format('h:i A') . '</option>';
                        }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time<rf></p>
                <div class="n-field-box">
                  <select name="schedule_enquiry_time_to" id="schedule_enquiry_time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
          </div>
          <div class="row m-0" id="booking_service_date_enquiry" style="display:none;">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Start Date<rf></p>
                <div class="n-field-box">
                  <input type="text" name="service_start_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service End Date&nbsp;<span tabindex="0" class="badge badge-primary p-0" role="button" data-toggle="popover" data-trigger="focus" data-title="Service End Date" data-content="Please keep the service end date empty for never ending services or minimum 3 service weeks required.">?</span>
                <div class="n-field-box">
                  <input type="text" name="service_end_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Week</p>
                <div class="n-field-box">
                  <input name="service_week_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="cleaning_material_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows" value="10" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Want Cleaning Material ?</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" value="0" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount (<span class="vat_percentage">-</span>%)</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" value="0"readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Total Amount<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="taxed_total" value="0" class="text-center no-arrows" readonly>
              </label>
            </div>
          </div>
      </div>
    </div>
    <div class="modal-footer new">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
    <!-- <div class="modal-footer edit">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Update</button>
    </div> -->
  </form>
</div>
</div>



<!-- suspension re-actvation model pop up -->
<div id="schedule_reactivate-popup" style="display:none;">
  <div class="popup-main-box">
    <!-- <form id="delete_day_schedule" class="form-horizontal" method="post"> -->
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Reactivate</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want re-activate this booking?</h3>
        <input type="hidden" id="booking_id" name="booking_id">
        <input  type="hidden" id="call_history_id" name="call_history_id">

        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="schedule_reactivate_btn" class="n-btn mb-0">OK</button>
      </div>
    <!-- </form> -->
  </div>
</div>
<!-- end -->

<div id="re-activate-popup" style="display:none;">
  <div class="popup-main-box">
    <!-- <form id="delete_day_schedule" class="form-horizontal" method="post"> -->
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Reactivate</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want re-activate this booking?</h3>
        <input type="hidden" id="booking_id" name="booking_id">
        <input type="hidden" id="service_date" name="service_date">
        <input  type="hidden" id="re_activate_customer_id" name="re_activate_customer_id">
        <input  type="hidden" id="re_activate_customer_phoneno" name="re_activate_customer_phoneno">
        <input  type="hidden" id="caller_re_activate_typ_id" name="caller_re_activate_typ_id">
        <input  type="hidden" id="booking_delete_id" name="booking_delete_id">

        <!-- <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="re-activate-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    <!-- </form> -->
  </div>
</div>

<div id="delete-regular-schedule-popup" style="display:none;">
<div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Suspend</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
  <div class="content">
    <div class="row m-0 n-delete-section-main pl-4 pt-5 pr-4">
      <div class="col-sm-6 pl-0">
        <input type="button" value="Suspend One Day" class="n-delete-set-left n-btn red-btn">
      </div>
      <div class="col-sm-6 pr-0">
        <input type="button" value="Suspend Permanently" class="n-delete-set-right n-btn red-btn de-select">
      </div>
      <div class="n-delete-set-left-cont">
        <div class="col-sm-12 p-0 pt-2" style="font-size: 18px;text-align: left;">Are you sure want to Suspend it for one day?</div>
        <span id="deleteremarks_book_day" style="color:red; display:none;"></span>
        <div class="row m-0 input-daterange">
          <div class="col-sm-5 pt-2 n-form-set-left">
            <div class="row m-0 n-field-main">
              <p>Date From</p>
              <div class="n-field-box">
                <input name="delete_date_from" id="delete_date_from" readonly>
              </div>
            </div>
          </div>
          <div class="col-sm-5 pt-2 n-form-set-left">
              <div class="row m-0 n-field-main">
              <p>Date To</p>
                <div class="n-field-box">
                  <input name="delete_date_to" id="delete_date_to" readonly>
                </div>
              </div>
          </div>
        </div>
        <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="delete_remark_day"></textarea>
          <input type="hidden" id="booking_id_day" name="booking_id_day">
        </div>
        <div class="col-sm-6 pl-0">
          <input type="button" value="Yes" class="delete_yes_book_day n-btn green-btn"/>
        </div>
      </div>
      <div class="n-delete-set-right-cont">
        <div class="col-sm-12 p-0 pt-3" style="font-size: 18px;text-align: left;">Are you sure want to Suspend it for permanently?</div>
        <span id="deleteremarks_book_day" style="color:red; display:none;"></span>
        <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="delete_remark_perm"></textarea>
          <input type="hidden" id="booking_id_perment" name="booking_id_perment">
        </div>
        <div class="col-sm-6 pl-0">
          <input type="button" value="Yes" class="delete_yes_book n-btn green-btn"/>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <form id="delete_day_schedule" class="form-horizontal" method="post">
      <div class="col-md-12 col-sm-12 green-popup-head">
        <span id="b-maid-name">Cancel</span>
        <span id="b-time-slot"></span>
        <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="modal-body">
        <h3>Are you sure want to cancel it for one day?</h3>
        <input type="hidden" id="day_service_id" name="day_service_id">
        <input type="hidden" id="booking_ids" name="booking_id">
        <input type="hidden" id="customer_service_date" name="service_date">
        <input  type="hidden" id="day_service_status" name="day_service_status">
        <input  type="hidden" id="booking_customer_id" name="booking_customer_id">
        <input  type="hidden" id="customer_phoneno" name="customer_phoneno">
        <input  type="hidden" id="caller_service_typ_id" name="caller_service_typ_id">

        <div class="col-sm-12 p-0">
          <textarea name="remark" placeholder="Please enter remarks." id="remark"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
        <button type="submit" id="delete-schedule-btn" class="n-btn mb-0">OK</button>
      </div>
    </form>
  </div>
</div>

<div id="create-schedule-popup" style="display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head bg-od">
    <span class="title">Create Schedule</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="create_schedule_booking_form">
    <input name="maid_id" type="hidden">
    <!-- <input name="service_week_day" type="hidden"> -->
    <input name="booking_id" type="hidden">
    <input name="caller_booking_typ_id" type="hidden">
    <input name="booking_customer_address_ids" type="hidden">
    <input name="service_start_date1" type="hidden">
    <input name="service_end_date1" type="hidden">
    <div class="modal-body">
      <div class="controls">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer<rf></p>
                <!-- <p>Maid Name<rf></p> -->
                <div class="n-field-box">
                  <input name="customer_name" id="customer_name" disabled>
                    <input name="customer_ids" type="hidden">
                    <input name="customer_booking_phone_no" type="hidden">

                </div>
                <!-- <div class="col-sm-12 p-0 n-field-box"> -->
                  <!-- <select name="customer_ids" id="customer_ids" data-placeholder="-- Select Customer --" style="width:100%"> -->
                  <!-- </select> -->
                  <!-- <input name="customer_name" id="customer_name" readonly> -->
                <!-- </div> -->
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
                <p>Booking Type</p>
                <div class="col-sm-12 p-0 n-field-box">
                    <select name="booking_type" id="booking_type" class="sel2" style="width:100%">
                        <option value="">Select type</option>
                        <?php foreach ($booking_types as $booking_type) : ?>
                            <option value="<?= $booking_type->booking_type ?>">
                                <?= $booking_type->booking_type_name ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid Name<rf></p>
                <div class="n-field-box">
                  <input name="maid_name" id="maid_name" disabled>
                </div>
              </div>
            </div>
           
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type<rf></p>
                <div class="n-field-box">
                  <select name="service_type_id" class="sel2" data-placeholder="-- Select Service --" style="width:100%">
                    <option value="">-- Select Service Type --</option>
                    <?php foreach ($services as $key => $service): ?>
                      <option value="<?=$service['service_type_id'];?>"  <?= ($service['service_type_id'] == 6) ? "selected" : "" ?>><?=$service['service_type_name'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time<rf></p>
                <div class="n-field-box">
                  <select name="schedule_time_from" id="schedule_time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                    <?php
                        $from_time = DateTime::createFromFormat('H:i:s', "08:00:00");
                        $to_time = DateTime::createFromFormat('H:i:s', "19:59:59");
                        $i = 0;
                        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
                            $time_clone = clone $time;
                            $i++;
                            echo '<option value="' . $time->format('H:i:s') . '">' . $time->format('h:i A') . '</option>';
                        }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time<rf></p>
                <div class="n-field-box">
                  <select name="schedule_time_to" id="schedule_time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
          </div>
          <div class="row m-0" id="booking_service_date" style="display:none;">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Start Date<rf></p>
                <div class="n-field-box">
                  <input type="text" name="service_start_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service End Date&nbsp;<span tabindex="0" class="badge badge-primary p-0" role="button" data-toggle="popover" data-trigger="focus" data-title="Service End Date" data-content="Please keep the service end date empty for never ending services or minimum 3 service weeks required.">?</span>
                <div class="n-field-box">
                  <input type="text" name="service_end_date" placeholder="dd/mm/yyyy" readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Week</p>
                <div class="n-field-box">
                  <input name="service_week_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material Rate<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="cleaning_material_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows" value="10" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Want Cleaning Material ?</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" value="0" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" value="0" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount (<span class="vat_percentage">-</span>%)</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" value="0"readonly>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Total Amount<rf></p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="taxed_total" value="0" class="text-center no-arrows" readonly>
              </label>
            </div>
          </div>
      </div>
    </div>
    <div class="modal-footer new">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
    <!-- <div class="modal-footer edit">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Close</button>
      <button type="submit" data-action="" class="n-btn mb-0">Update</button>
    </div> -->
  </form>
</div>
</div>

<div id="customer-add-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">New Customer</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div id="popup-booking" class="col-12 p-0">
            <form name="customer-popup" class="form-horizontal" method="post" id="customer-popup">
                <div class="modal-body">
                    <div class="row m-0">
                        <input type="hidden" value="1" id="is_company_1" name="customer_type_id">
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Customer <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" name="customer_name" id="customer_name" style="width:220px;">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Email <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" name="customer_email" id="customer_email" style="width:220px;">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Mobile Number <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" name="customer_mobilenumber" id="customer_mobilenumber" style="width:220px;">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Area</p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="customer_area_id" id="customer_area_id" class="sel2" data-placeholder="Select area" style="width:100%">
                                        <option value="">Select area</option>
                                        <?php foreach ($areas as $area) : ?>
                                            <option value="<?= $area->area_id ?>"><?= $area->area_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Location</p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="customer_location" id="customer_location" class="sel2" data-placeholder="Select location" style="width:100%">
                                        <option value="">Select location</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 n-form-set-left">

                            <div class="row m-0 n-field-main">
                                <p>Landmark</p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="customer_landmark" id="customer_landmark" class="sel2" data-placeholder="Select landmark" style="width:100%">
                                        <option value="">Select landmark</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Resdidence Type <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="customer_residence_type" id="customer_residence_type" class="sel2" data-placeholder="Select residence type" style="width:100%">
                                        <option value="">Select residence type</option>
                                        <?php foreach ($residence_types as $residence_type) : ?>
                                            <option value="<?= $residence_type->residence_type_id ?>"><?= $residence_type->residence_type ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Address <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <textarea type="text" name="customer_address" id="customer_address"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" id="form-error">&nbsp;</p>
                </div>
        </div>
        <div class="modal-footer">
            <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
            <button type="submit" class="n-btn m-0" value="Submit" id="customer-save-btn">Save</button>
        </div>
        </form>
    </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">
            <div class="widget-header">
                <div class="book-nav-top">
                    <ul>
                        <li>
                            <i class="icon-user"></i>
                            <h3>Call Management</h3>
                        </li>
                        <li class="mr-0 float-right">
                            <div class="topiconnew border-0 green-btn">
                                <a href="<?php echo base_url('call-management'); ?>" title="Call Management"> <i class="fa fa-list"></i></a>
                            </div>
                        </li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>



            <div class="widget-content">
                <div class="col-sm-6 call-customer-det-left-section">

                    <div class="row m-0 n-field-main call-customer-field-main">
                        <p>Search Customer</p>
                        <div class="col-sm-4 p-0 n-field-box call-customer-det-textfield">
                            <select name="customer_id" class="sel2 call-customer-det-select" id="customer_id" data-placeholder="Select customer" style="width:100%">
                            </select>
                            <div id="customer-picked-address-u"></div>
                        </div>

                        <div class="col-sm-4 m-0 n-field-box" id="view_customer" style="display:none;">
                            <div class="n-btn">
                            <a href="" name="customer_view" id="customer_view" title="View Customer">View Customer</a>
                            </div>

                        </div>
                    </div>

                    <div id="customer-address-panel-user">
                        <div class="head">Pick One Address <span class="close">Close</span></div>

                        <div class="inner">
                                Loading<span class="dots_loader"></span>
                        </div>			
                    </div>
                    <div class="row cl-customer-det-tmb-set   m-0">
                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">
                            <p>Customer Name</p>

                            <p><span id="customername"></span></p>

                        </div>
                        <div class="col-sm-8 cl-customer-det-tmb-main v-center">

                            <p>Address</p>

                            <p><span id="customer-address"></span></p>

                        </div>


                    </div>
                    <div class="row cl-customer-det-tmb-set no-top-border no-bottom-border m-0">
                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Mobile</p>

                            <p><span id="customer-mobile"></span></p>


                        </div>

                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Location</p>

                            <p><span id="customer-location"></span></p>


                        </div>
                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Landmark</p>

                            <p><span id="customer-sublocation"></span></p>


                        </div>

                    </div>
                    <div class="row cl-customer-det-tmb-set  no-top-border m-0 ">

                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Customer Status</p>

                            <p><span id="customer-status"></span></p>


                        </div>
                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Customer Type</p>

                            <p><span id="customer-type"></span></p>


                        </div>
                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Customer Email</p>

                            <p><span id="customer-email"></span></p>


                        </div>
                    </div>
                    <div class="row cl-customer-det-tmb-set  no-top-border m-0 ">
                        <div class="col-sm-4 cl-customer-det-tmb-main v-center">

                            <p>Area/Zone</p>

                            <p><span id="customer-area-zone"></span></p>


                        </div>
                        <div class="col-sm-4 cl-customer-det-tmb-main  v-center">

                            <p>Called For</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <select name="called_for" class="sel2" style="width:100%" id="called_for">
                                <option value="">Select type</option>
                                    <?php foreach ($enquiry_types as $enquiry_type) : ?>
                                        <option value="<?= $enquiry_type->enquiry_type_id ?>"><?= $enquiry_type->enquiry_type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6" id="call_details">
                    <h6>Call Details</h6>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Date</th>
                                <th>For</th>
                                <th>Attended By</th>
                                <!-- <th>Status</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($call_lists) > 0) {
                                $i = 1;
                                foreach ($call_lists as $row) {
                            ?>
                                    <tr>
                                        <td><?php echo $row->customer_name ?></td>
                                        <td><?php echo $row->mobile_number ?></td>
                                        <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $row->added_date_time)->format('d M Y') ?></td>
                                        <td><?php echo $row->enquiry_type ?: "-"; ?></td>
                                        <td><?php echo $row->user_fullname ?></td>
                                        <!-- <td><?php echo $row->status ?: "-"; ?></td> -->

                                    </tr>
                                <?php
                                    $i++;
                                }
                            } else {
                                ?>
                                <tr class="bg-warning">
                                    <td colspan="6" style="text-align:center;">No Data Found !</td>
                                <tr>
                                <?php
                            }
                                ?>
                        </tbody>
                    </table>
                </div>


                <div class="col-sm-12">
                    <div class="row m-0">
                        <div class="tabbable">
                            <ul class="nav nav-tabs pb-4" id="mytabs">
                                <li id="booking-li" data-tab="booking"><a href="#booking" data-toggle="tab">Booking</a></li>
                                <li id="enquiry-li" data-tab="enquiry"><a href="#enquiry" data-toggle="tab">Enquiry</a></li>
                                <li id="complaints-li" data-tab="complaints"><a href="#complaints" data-toggle="tab">Complaints</a></li>
                                <li id="re-activations-li" data-tab="re-activations"><a href="#re-activations" data-toggle="tab">Re-activation</a></li>
                                <!-- <li id="cancel-service-li" data-tab="cancel-service"><a href="#cancel-service" data-toggle="tab">Cancel a service</a></li> -->
                                <li id="suspend-service-li" data-tab="suspend-service"><a href="#suspend-service" data-toggle="tab">Suspend a service</a></li>
                                <!-- <li id="other-li" data-tab="other"><a href="#other" data-toggle="tab">Other</a></li> -->
                                <!-- <li id="preferences-li" data-tab="preferences"><a href="#preferences" data-toggle="tab">Preferences</a></li> -->
                                <li id="feedback-li" data-tab="feedback"><a href="#feedback" data-toggle="tab">Feedback</a></li>
                            </ul>
                            <!-- <form id="new_call_management_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="caller_typ_id" id="caller_typ_id"> -->
                            <div class="tab-content">
                                <div class="tab-pane tab-panel" data-panel="booking">
                                    <form id="call_management_booking_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_booking_typ_id" id="caller_booking_typ_id">
                                        <input type="hidden" name="booking_customer_phoneno" id="booking_customer_phoneno">
                                        <input type="hidden" name="booking_customer_name" id="booking_customer_name">
                                        <input type="hidden" name="booking_customer_ids" id="booking_customer_ids">
                                        <input type="hidden" name="booking_customer_address_id" id="booking_customer_address_id">
                                        <div class="col-sm-3">
                                            <div class="row m-0 n-field-main">
                                                <p>Booking Type</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="booking_type" id="booking_type" class="sel2" style="width:100%">
                                                        <option value="">Select type</option>
                                                        <?php foreach ($booking_types as $booking_type) : ?>
                                                            <option value="<?= $booking_type->booking_type ?>">
                                                                <?= $booking_type->booking_type_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 n-field-main">
                                                <p>Date</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" name="booking_date" readonly>
                                                    <!-- <input type="date" name="booking_date" id="booking_date"> -->
                                                </div>
                                            </div>
                                            <div class="row m-0 n-field-main">
                                                <p>Maids</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="booking_maid_id" id="booking_maid_id" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($pref_maids as $maid) : ?>
                                                            <option value="<?= $maid->maid_id ?>">
                                                                <?= $maid->maid_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 n-field-main">
                                                <p>Search Zone</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="booking_zone_id" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($zones as $zone) : ?>
                                                            <option value="<?= $zone->zone_id ?>">
                                                                <?= $zone->zone_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 weekly_booking_time">
                                            <div class="row m-0 booking-type-det-set">
                                                <!-- <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="1" name="availability_week_days[1]">
                                                            <input type="checkbox" id="week_day_1" class="week_day" class="week_day" value="Y" name="availability_checked[1]">
                                                            <label for="week_day_1">
                                                                <span class="border-radius-3"></span>
                                                                <p>Monday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <p>Start Time</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="availability_time_from" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="row m-0 booking-type-det-set">
                                                <p>End Time</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="availability_time_to" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="2" name="availability_week_days[2]">
                                                            <input type="checkbox" id="week_day_2" class="week_day"  value="Y" name="availability_checked[2]">
                                                            <label for="week_day_2">
                                                                <span class="border-radius-3"></span>
                                                                <p>Tuesday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_from[2]" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_to[2]" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="3" name="availability_week_days[3]">
                                                            <input type="checkbox" id="week_day_3" class="week_day" value="Y" name="availability_checked[3]">
                                                            <label for="week_day_3">
                                                                <span class="border-radius-3"></span>
                                                                <p>Wednessday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_from[3]" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_to[3]" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="4" name="availability_week_days[4]">
                                                            <input type="checkbox" id="week_day_4" value="Y" class="week_day" name="availability_checked[4]">
                                                            <label for="week_day_4">
                                                                <span class="border-radius-3"></span>
                                                                <p>Thursday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_from[4]" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_to[4]" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="5" name="availability_week_days[5]">
                                                            <input type="checkbox" id="week_day_5" value="Y" class="week_day" name="availability_checked[5]">
                                                            <label for="week_day_5">
                                                                <span class="border-radius-3"></span>
                                                                <p>Friday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_from[5]" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_to[5]" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="6" name="availability_week_days[6]">
                                                            <input type="checkbox" id="week_day_6" value="Y" class="week_day" name="availability_checked[6]">
                                                            <label for="week_day_6">
                                                                <span class="border-radius-3"></span>
                                                                <p>Saturday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_from[6]" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_to[6]" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main" >
                                                            <input type="hidden" value="0" name="availability_week_days[0]">
                                                            <input type="checkbox" id="week_day_0" value="Y" class="week_day" name="availability_checked[0]">
                                                            <label for="week_day_0">
                                                                <span class="border-radius-3"></span>
                                                                <p>Sunday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_from[0]" class="sel2 week_time" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="availability_time_to[0]" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 od_booking_time" style="display:none">
                                            <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="1" name="availability_week_days[1]">
                                                            <input type="checkbox" id="week_day_1" class="week_day" class="week_day" value="Y" name="availability_checked[1]">
                                                            <label for="week_day_1">
                                                                <span class="border-radius-3"></span>
                                                                <p>Monday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="booking_time_from" class="sel2" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 booking-type-det-time">
                                                    <select name="booking_time_to" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="2" name="availability_week_days[2]">
                                                            <input type="checkbox" id="week_day_2" class="week_day"  value="Y" name="availability_checked[2]">
                                                            <label for="week_day_2">
                                                                <span class="border-radius-3"></span>
                                                                <p>Tuesday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="3" name="availability_week_days[3]">
                                                            <input type="checkbox" id="week_day_3" class="week_day" value="Y" name="availability_checked[3]">
                                                            <label for="week_day_3">
                                                                <span class="border-radius-3"></span>
                                                                <p>Wednessday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="4" name="availability_week_days[4]">
                                                            <input type="checkbox" id="week_day_4" value="Y" class="week_day" name="availability_checked[4]">
                                                            <label for="week_day_4">
                                                                <span class="border-radius-3"></span>
                                                                <p>Thursday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="5" name="availability_week_days[5]">
                                                            <input type="checkbox" id="week_day_5" value="Y" class="week_day" name="availability_checked[5]">
                                                            <label for="week_day_5">
                                                                <span class="border-radius-3"></span>
                                                                <p>Friday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main">
                                                            <input type="hidden" value="6" name="availability_week_days[6]">
                                                            <input type="checkbox" id="week_day_6" value="Y" class="week_day" name="availability_checked[6]">
                                                            <label for="week_day_6">
                                                                <span class="border-radius-3"></span>
                                                                <p>Saturday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="row m-0 booking-type-det-set">
                                                <div class="col-sm-4 booking-type-det-day">
                                                    <div class="n-field-box">
                                                        <div class="n-radi-check-main" >
                                                            <input type="hidden" value="0" name="availability_week_days[0]">
                                                            <input type="checkbox" id="week_day_0" value="Y" class="week_day" name="availability_checked[0]">
                                                            <label for="week_day_0">
                                                                <span class="border-radius-3"></span>
                                                                <p>Sunday</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                        <div class="col-sm-6">
                                        <div id="maid_search" style="border: medium none !important;"></div>
                                        </div>
                                        <div class="col-sm-12" style="display:none">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="booking-save-btn">Search</button>
                                        </div>
                                    </form>
                                </div>




                                <div class="tab-pane tab-panel" data-panel="enquiry" style="display:none;">
                                    <form id="call_management_enquiry_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_enquiry_typ_id" id="caller_enquiry_typ_id">
                                        <input type="hidden" name="enquiry_customer_address_id" id="enquiry_customer_address_id">
                                        <div class="col-sm-3">
                                            <div class="row m-0 n-field-main">
                                                <p>Booking Type</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="booking_type_enquiry" id="booking_type_enquiry" class="sel2" style="width:100%">
                                                        <option value="">Select type</option>
                                                        <?php foreach ($booking_types as $booking_type) : ?>
                                                            <option value="<?= $booking_type->booking_type ?>">
                                                                <?= $booking_type->booking_type_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">&nbsp;</div>
                                      
                                        <div class="col-sm-10 p-0 pb-3">
                                            <div class="row m-0 input-daterange">
                                                <!-- <div style="display:none" id="date_range">
                                                    <div class="col-sm-3 n-field-main">
                                                        <p>Date From
                                                            <rf>
                                                        </p>
                                                        <div class="n-field-box">
                                                            <input name="date_from" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 n-field-main">
                                                        <p>Date To
                                                            <rf>
                                                        </p>
                                                        <div class="n-field-box">
                                                            <input name="date_to" readonly>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <!-- <div id="enquiry_date">
                                                    
                                                </div> -->
                                                <div id="enquiry_time">
                                                    <div class="col-sm-3 n-field-main">
                                                        <p>Time From
                                                            <rf>
                                                        </p>
                                                        <select name="enquiry_time_from" class="sel2" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 n-field-main">
                                                        <p>Time To
                                                            <rf>
                                                        </p>
                                                        <select name="enquiry_time_to" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 n-field-main">
                                                        <div class="row m-0 n-field-main">
                                                            <p>Total Hours</p>
                                                            <div class="n-field-box">
                                                                <input name="working_hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                        
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Date <rf></p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" name="enquiry_booking_date" id="enquiry_booking_date" readonly>
                                                </div>
                                            </div>
                                                 
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Maid</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="maidId" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($pref_maids as $maid) : ?>
                                                            <option value="<?= $maid->maid_id ?>">
                                                                <?= $maid->maid_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Service Type
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="service_id" class="sel2" style="width:100%">
                                                        <!-- <option value="">All</option> -->
                                                        <?php foreach ($services as $key => $service) : ?>
                                                            <option value="<?= $service['service_type_id'] ?>" <?= ($service['service_type_id'] == 6) ? "selected" : "" ?>>
                                                                <?= $service['service_type_name'] ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <div class="col-sm-12 n-radi-check-main p-0">
                                                    <!-- <input type="radio" value="1" id="outside_area" name="outside_service_area" class="">
                                                    <label for="outside_service_area">
                                                        <span class="border-radius-3"></span>
                                                        <p>Outside Service Area</p>
                                                    </label> -->
                                                
                                                    <input type="hidden" value="1" name="outside_service_area">
                                                    <input type="checkbox" id="outside_service_check" value="Y" name="outside_service_checked">
                                                    <label for="outside_service_check">
                                                        <span class="border-radius-3"></span>
                                                        <p>Outside Service Area</p>
                                                    </label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main area_div">
                                                <p>Area
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="area_id" class="sel2" style="width:100%" id="area_id">
                                                        <option value="">Select area</option>
                                                        <?php foreach ($areas as $area) : ?>
                                                            <option value="<?= $area->area_id ?>">
                                                                <?= $area->area_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main loc_div">
                                                <p>Location
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="location_id" class="sel2" style="width:100%" id="location_id">
                                                        <option value="">Select location</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main" id="outside_new" style="display:none;">
                                              <p>New Area & Address <rf></p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="outside_new_area" id="outside_new_area">
                                                </div>
                                             </div>
                                        </div>

                                        <div class="col-sm-1">&nbsp;</div>
                                        <div class="col-sm-3">
                                            <input type="hidden" name="enquiry_customer_id" id="enquiry_customer_id">
                                            <input type="hidden" name="enquiry_customer_name" id="enquiry_customer_name">
                                            <input type="hidden" name="enquiry_customer_phoneno" id="enquiry_customer_phoneno">
                                            <input type="hidden" name="enquiry_customer_email" id="enquiry_customer_email">
                                            <!-- <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Name</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="enquiry_customer_name" id="enquiry_customer_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Phone Number</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="enquiry_customer_phoneno" id="enquiry_customer_phoneno">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Email</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="enquiry_customer_email" id="enquiry_customer_email">
                                                </div>
                                            </div> -->
                                            
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main" id="enquiry_zone" style="display:none;">
                                                <p>Search Zone</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="enquiry_zone_id" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($zones as $zone) : ?>
                                                            <option value="<?= $zone->zone_id ?>">
                                                                <?= $zone->zone_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Source</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="enquiry_source_id" class="sel2" style="width:100%">
                                                        <option value=""></option>
                                                        <?php foreach ($refer_sources as $source) : ?>
                                                            <option value="<?= $source->refer_source_id ?>">
                                                                <?= $source->refer_source ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Notes</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea type="text" name="enquiry_notes" id="enquiry_notes"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5" id="week_available">
                                            <div class="col-sm-12 weekly_booking_time">
                                                <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main">
                                                                <input type="hidden" value="1" name="availability_week_days[1]">
                                                                <input type="checkbox" id="week_day_1" class="week_day" class="week_day" value="Y" name="availability_checked[1]">
                                                                <label for="week_day_1">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Monday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[1]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[1]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[1]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main">
                                                                <input type="hidden" value="2" name="availability_week_days[2]">
                                                                <input type="checkbox" id="week_day_2" class="week_day"  value="Y" name="availability_checked[2]">
                                                                <label for="week_day_2">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Tuesday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[2]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[2]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[2]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main">
                                                                <input type="hidden" value="3" name="availability_week_days[3]">
                                                                <input type="checkbox" id="week_day_3" class="week_day" value="Y" name="availability_checked[3]">
                                                                <label for="week_day_3">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Wednessday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[3]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[3]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[3]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main">
                                                                <input type="hidden" value="4" name="availability_week_days[4]">
                                                                <input type="checkbox" id="week_day_4" value="Y" class="week_day" name="availability_checked[4]">
                                                                <label for="week_day_4">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Thursday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[4]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[4]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[4]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main">
                                                                <input type="hidden" value="5" name="availability_week_days[5]">
                                                                <input type="checkbox" id="week_day_5" value="Y" class="week_day" name="availability_checked[5]">
                                                                <label for="week_day_5">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Friday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[5]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[5]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[5]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main">
                                                                <input type="hidden" value="6" name="availability_week_days[6]">
                                                                <input type="checkbox" id="week_day_6" value="Y" class="week_day" name="availability_checked[6]">
                                                                <label for="week_day_6">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Saturday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[6]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[6]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[6]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row m-0 booking-type-det-set">
                                                    <div class="col-sm-3 booking-type-det-day">
                                                        <div class="n-field-box">
                                                            <div class="n-radi-check-main" >
                                                                <input type="hidden" value="0" name="availability_week_days[0]">
                                                                <input type="checkbox" id="week_day_0" value="Y" class="week_day" name="availability_checked[0]">
                                                                <label for="week_day_0">
                                                                    <span class="border-radius-3"></span>
                                                                    <p>Sunday</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_from[0]" class="sel2 week_time" style="width:100%">
                                                            <option value="">Start time</option>
                                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                                <option value="<?= $time; ?>">
                                                                    <?= $label; ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <select name="availability_time_to[0]" class="sel2" style="width:100%">
                                                            <option value="">End time</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 booking-type-det-time">
                                                        <div class="row m-0 n-field-main">
                                                            <!-- <p>Total Hours</p> -->
                                                            <div class="n-field-box">
                                                                <input name="working_hour[0]" placeholder="Total Hours">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-12" id="enquiry_save" style="display:none;">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="enquiry-save-btn">Submit</button>
                                        </div>
                                        <div class="col-sm-12" id="enquiry_search" style="display:none;">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="enquiry-search-btn">Search</button>
                                        </div>
                                    </form>
                                </div>




                                <div class="tab-pane tab-panel" data-panel="complaints" style="display:none;">
                                    <form id="call_management_complaints_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_complaints_typ_id" id="caller_complaints_typ_id">
                                        <div class="col-sm-10 p-0">
                                            <div class="col-sm-3  n-field-main">
                                                <p>Date
                                                    <rf>
                                                </p>
                                                <div class="n-field-box">
                                                    <!-- <input type="text" autocomplete="off" name="complaint_date" readonly> -->
                                                    <input type="date" name="complaint_date">
                                                </div>
                                            </div>
                                            <div class="col-sm-3  n-field-main">
                                                <p>Time
                    
                                                </p>
                                                <div class="n-field-box">
                                                    <select name="complaint_time" class="sel2" style="width:100%">
                                                        <option value="">Time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-3  n-field-main">
                                                <p>Complaint No</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="complaint_no" autocomplete="off" value="MM-CC-00-<?php echo date('m-Y')?>" disabled>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-3  n-field-main">
                                                <p>Type
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                    <select name="complaint_type" class="sel2" style="width:100%">
                                                        <option value="">--select type--</option>
                                                        <?php foreach ($complaint_types as $complaint_type) : ?>
                                                            <option value="<?= $complaint_type->type_id ?>">
                                                                <?= $complaint_type->type ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Complainer</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="hidden" name="complainer_id" id="complainer_id">
                                                    <input type="text" name="complainer_name" id="complainer_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Phone No</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="complainer_phone" id="complainer_phone">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Email</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="complainer_email" id="complainer_email">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Existing Customer</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="existing_customer" class="sel2" style="width:100%">
                                                        <option value="Y">YES</option>
                                                        <option value="N">NO</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">&nbsp;</div>
                                        <div class="col-sm-3">
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Against</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="complaint_against" id="employee_type_id" class="sel2" style="width:100%">
                                                        <option value="C">Cleaner</option>
                                                        <option value="D">Driver</option>
                                                        <option value="U">User</option>
                                                        <!-- <option value="4">Mechanic</option> -->
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Employee Name</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="employee_name" id="employee_name" class="sel2" style="width:100%">
                                                        <option value="">--select--</option>
                                                        <?php foreach ($maids as $maid) : ?>
                                                            <option value="<?= $maid->maid_id ?>">
                                                                <?= $maid->maid_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                        <input type="hidden" name="complaint_against_name" id="complaint_against_name">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Complaint</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea type="text" name="complaint" id="complaint"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Action Taken</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea type="text" name="action_taken" id="action_taken"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Complaint Status
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="complaint_status" class="sel2" style="width:100%">
                                                        <option value="">--select status-</option>
                                                        <?php foreach ($complaint_statuses as $complaint_status) : ?>
                                                            <option value="<?= $complaint_status->complaint_status ?>">
                                                                <?= $complaint_status->complaint_status ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="complaint-save-btn">Submit</button>
                                        </div>
                                    </form>
                                </div>




                                <div class="tab-pane tab-panel" data-panel="re-activations" style="display:none;">
                                    <form id="call_management_re_activations_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_re_activations_typ_id" id="caller_re_activations_typ_id">
                                        <div class="col-sm-10 p-0 pb-3">
                                            <div class="row m-0">
                                                <div class="col-sm-3 n-field-main">
                                                    <p>Date From
                                                        <rf>
                                                    </p>
                                                    <div class="n-field-box">
                                                        <input name="re_activation_date_from" readonly>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-sm-3 n-field-main">
                                                    <p>Date To
                                                        <rf>
                                                    </p>
                                                    <div class="n-field-box">
                                                        <input name="re_activation_date_to" readonly>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="col-sm-1">&nbsp;</div>
                                        <div class="col-sm-3">
                                            <input type="hidden" name="re_actvation_customer_id" id="re_actvation_customer_id">
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Name</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="re_actvation_customer_name" id="re_actvation_customer_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Phone Number</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="re_actvation_customer_phoneno" id="re_actvation_customer_phoneno">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Email</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="re_actvation_customer_email" id="re_actvation_customer_email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="re-activations-save-btn">Search</button>
                                        </div>
                                    </form>
                                </div>



                                <!-- <div class="tab-pane tab-panel" data-panel="cancel-service" style="display:none;">
                                    <form id="call_management_cancel_serice_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_cancel_service_typ_id" id="caller_cancel_service_typ_id">
                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="cancel-service-save-btn">Submit</button>
                                        </div>
                                    </form>
                                </div> -->


                                <div class="tab-pane tab-panel" data-panel="suspend-service" style="display:none;">
                                    <form id="call_management_suspend_service_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_suspend_service_typ_id" id="caller_suspend_service_typ_id">
                                        <div class="col-sm-10 p-0 pb-3">
                                            <div class="row m-0 input-daterange">
                                                <div class="col-sm-3 n-field-main">
                                                    <p>Date From
                                                        <rf>
                                                    </p>
                                                    <div class="n-field-box">
                                                        <input name="suspend_date_from" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 n-field-main">
                                                    <p>Date To
                                                        <rf>
                                                    </p>
                                                    <div class="n-field-box">
                                                        <input name="suspend_date_to" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">&nbsp;</div>
                                        <div class="col-sm-3">
                                            <input type="hidden" name="suspend_customer_id" id="suspend_customer_id">
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Name</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="suspend_customer_name" id="suspend_customer_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Phone Number</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="suspend_customer_phoneno" id="suspend_customer_phoneno">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Email</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" name="suspend_customer_email" id="suspend_customer_email">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="suspend-service-save-btn">Search</button>
                                        </div>
                                    </form>
                                </div>


                                <!-- <div class="tab-pane tab-panel" data-panel="other" style="display:none;">
                                    <form id="call_management_other_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_other_typ_id" id="caller_other_typ_id">
                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="other-save-btn">Submit</button>
                                        </div>
                                    </form>
                                </div> -->


                                <div class="tab-pane tab-panel" data-panel="preferences" style="display:none;">
                                    <form id="call_management_preferences_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_preferences_typ_id" id="caller_preferences_typ_id">
                                        <input type="hidden" name="customer_preference_id" id="customer_preference_id">
                                        <input type="hidden" name="customer_preference_phone" id="customer_preference_phone">
                                        <div class="col-sm-10">
                                            <div class="row pb-3 input-daterange">
                                                <div class="col-sm-3 n-form-set-left">
                                                    <p>Date From
                                                        <rf>
                                                    </p>
                                                    <div class="n-field-box">
                                                        <input name="preference_date_from" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 n-form-set-left">
                                                    <p>Date To
                                                        <rf>
                                                    </p>
                                                    <div class="n-field-box">
                                                        <input name="preference_date_to" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 n-form-set-left">
                                                    <p>Time From
                                                        <rf>
                                                    </p>
                                                    <select name="preference_time_from" class="sel2" style="width:100%">
                                                        <option value="">Start time</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                            <option value="<?= $time; ?>">
                                                                <?= $label; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3 n-form-set-left">
                                                    <p>Time To
                                                        <rf>
                                                    </p>
                                                    <select name="preference_time_to" class="sel2" style="width:100%">
                                                        <option value="">End time</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 p-0 n-field-main">
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Booking Type
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="preference_booking_type" class="sel2" style="width:100%">
                                                        <option value="">Select type</option>
                                                        <?php foreach ($booking_types as $booking_type) : ?>
                                                            <option value="<?= $booking_type->booking_type ?>">
                                                                <?= $booking_type->booking_type_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Maid</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="preference_maidId" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($pref_maids as $maid) : ?>
                                                            <option value="<?= $maid->maid_id ?>">
                                                                <?= $maid->maid_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Service Type
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="preference_service_id" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($services as $key => $service) : ?>
                                                            <option value="<?= $service['service_type_id'] ?>" <?= ($service['service_type_id'] == 6) ? "selected" : "" ?>>
                                                                <?= $service['service_type_name'] ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Area
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="preference_area_id" id="preference_area_id" class="sel2" data-placeholder="Select area" style="width:100%">
                                                        <option value="">Select area</option>
                                                        <?php foreach ($areas as $area) : ?>
                                                            <option value="<?= $area->area_id ?>">
                                                                <?= $area->area_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                                <p>Location
                                                    <rf>
                                                </p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="preference_location" id="preference_location" class="sel2" data-placeholder="Select location" style="width:100%">
                                                        <option value=""></option>Select location</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="preferences-save-btn">Search</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-pane tab-panel" data-panel="feedback" style="display:none;">
                                    <form id="call_management_feedback_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="caller_feedback_typ_id" id="caller_feedback_typ_id">
                                        <input type="hidden" name="feedback_customer_id" id="feedback_customer_id">
                                        <input type="hidden" name="feedback_customer_phone" id="feedback_customer_phone">
                                       
                                        <div class="col-sm-3">
                                            <!-- <input type="hidden" name="re_actvation_customer_id" id="re_actvation_customer_id"> -->
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                            <p>Date<rf></p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <input type="text" autocomplete="off" name="feedback_date" readonly>
                                                 
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                            <p>Maids<rf></p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <select name="feedback_maid_id" id="feedback_maid_id" class="sel2" style="width:100%">
                                                        <option value="">All</option>
                                                        <?php foreach ($pref_maids as $maid) : ?>
                                                            <option value="<?= $maid->maid_id ?>">
                                                                <?= $maid->maid_name ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                              <p>Feedback Message</p>
                                                <div class="col-sm-12 p-0 n-field-box">
                                                    <textarea type="text" name="message" id="message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                                            <button type="button" class="n-btn" id="feedback-save-btn">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            

                            <!-- </form> -->
                        </div>
                    </div>
                </div>

               
                <div id="maid_enquiry_search" style="border: medium none !important;"></div>


                <div id="booking_customer_details" style="border: medium none !important;">
                  
                </div>
           
                <div id="booking_customer_re_active_details" style="display:none;">
                    <div class="col-sm-12 n-mobile-scroll">
                        <table width="100%" border="1" cellpadding="0" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th align="center" valign="middle" style="width:50px;">
                                        <p>Sl:No</p>
                                    </th>
                                    <th align="left" valign="middle">
                                        <p class="text-left">Customer Name</p>
                                    </th>
                                    <th align="left" valign="middle">
                                        <p class="text-left">Booking Id</p>
                                    </th>
                                    <th align="left" valign="middle">
                                    <p class="text-left">Maid Name</p>
                                    </th>
                                    <th align="left" valign="middle">
                                    <p class="text-left">Booking Type</p>
                                    </th>
                                    <th align="left" valign="middle">
                                    <p class="text-left">Booking Delete from date</p>
                                    </th>
                                    <th align="center" valign="middle">
                                        <p class="text-left">Booking Delete to date</p>
                                    </th>
                                    
                                    <!-- <th align="center" valign="middle">
                                        <p>Total Hours</p>
                                    </th> -->
                                    <th align="center" valign="middle">
                                        <p>Action</p>
                                    </th>
                                    
                                </tr>
                            </thead>
                            <tbody id="booking_re_activation_customer">
                                

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- /widget -->
    </div>
    <!-- /span8 -->
</div>
<!-- /row -->
<script>

</script>