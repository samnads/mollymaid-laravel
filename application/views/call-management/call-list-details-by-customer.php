  <h6>Call Details</h6>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Number</th>
            <th>Date</th>
            <th>For</th>
            <th>Attended By</th>
            <!-- <th>Status</th> -->
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($call_lists) > 0) {
            $i = 1;
            foreach ($call_lists as $row) {
        ?>
                <tr>
                    <td><?php echo $row->customer_name ?></td>
                    <td><?php echo $row->mobile_number ?></td>
                    <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $row->added_date_time)->format('d M Y') ?></td>
                    <td><?php echo $row->enquiry_type ?: "-"; ?></td>
                    <td><?php echo $row->user_fullname ?></td>
                    <!-- <td><?php echo $row->status ?: "-"; ?></td> -->

                </tr>
            <?php
                $i++;
            }
        } else {
            ?>
            <tr class="bg-warning">
                <td colspan="6" style="text-align:center;">No Data Found !</td>
            <tr>
            <?php
        }
        ?>
    </tbody>
</table>