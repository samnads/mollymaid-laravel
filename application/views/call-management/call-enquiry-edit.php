<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">
            <div class="widget-header">
                <div class="book-nav-top">
                    <ul>
                        <li>
                            <i class="icon-user"></i>
                            <h3>Call Enquiry Edit</h3>
                        </li>
                        <li class="mr-0 float-right">
                            <div class="topiconnew border-0 green-btn">
                                <a href="<?php echo base_url('call-management'); ?>" title="Call Management"> <i class="fa fa-list"></i></a>
                            </div>
                        </li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
            <div class="widget-content">
                <form id="call_management_enquiry_edit_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="caller_enquiry_typ_id" id="caller_enquiry_typ_id" value="<?= $enquiry->enquiry_type_id ?>">
                    <input type="hidden" value="<?= $enquiry->call_history_id ?>" name="call_history_id">
                    <div class="col-sm-3">
                        <div class="row m-0 n-field-main">
                            <p>Booking Type</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <select name="booking_type_enquiry" id="booking_type_enquiry" class="sel2" style="width:100%">
                                    <option value="">Select type</option>
                                    <?php foreach ($booking_types as $booking_type) : ?>
                                        <option value="<?=  $booking_type->booking_type ?>" <?= ($enquiry->booking_type == $booking_type->booking_type) ? "selected" : "" ?>>
                                        <?= $booking_type->booking_type_name  ?></option>
                                        <!-- <option value="<?= $booking_type->booking_type ?>">
                                            <?= $booking_type->booking_type_name ?>
                                        </option> -->
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">&nbsp;</div>
                                        
                        <div class="col-sm-10 p-0 pb-3">
                            <div class="row m-0 input-daterange">
                                <!-- <div style="display:none" id="date_range">
                                    <div class="col-sm-3 n-field-main">
                                        <p>Date From
                                            <rf>
                                        </p>
                                        <div class="n-field-box">
                                            <input name="date_from" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 n-field-main">
                                        <p>Date To
                                            <rf>
                                        </p>
                                        <div class="n-field-box">
                                            <input name="date_to" readonly>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div id="enquiry_date">
                                    
                                </div> -->
                                <div id="enquiry_time" style="<?= $enquiry->booking_type == 'OD' ? "display:block;" : "display:none" ?>">
                                    <div class="col-sm-3 n-field-main">
                                        <p>Time From
                                            <rf>
                                        </p>
                                        <select name="enquiry_time_from" class="sel2" style="width:100%">
                                            <option value="">Start time</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>" <?= $enquiry->time_from ==  $time ? "selected" : "" ?>>
                                                    <?= $label; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 n-field-main">
                                        <p>Time To
                                            <rf>
                                        </p>
                                        <select name="enquiry_time_to" class="sel2" style="width:100%">
                                            <!-- <option value="">End time</option> -->
                                            <option value="<?= $enquiry->time_to; ?>" selected><?= $enquiry->time_to; ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 n-field-main">
                                        <div class="row m-0 n-field-main">
                                            <p>Total Hours</p>
                                            <div class="n-field-box">
                                                <input name="working_hours" value="<?= $enquiry->total_hours ; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                        
                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                <p>Date <rf></p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <input type="text" autocomplete="off" name="enquiry_booking_date" id="enquiry_booking_date" readonly value="<?= date('d/m/Y', strtotime($enquiry->date_from)) ?>">
                                </div>
                            </div>
                                                    
                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                <p>Maid</p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="maidId" class="sel2" style="width:100%">
                                        <option value="">All</option>
                                        <?php foreach ($pref_maids as $maid) : ?>
                                            <option value="<?= $maid->maid_id ?>" <?= ($enquiry->maid_id == $maid->maid_id) ? "selected" : "" ?>>
                                                <?= $maid->maid_name ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                <p>Service Type
                                    <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="service_id" class="sel2" style="width:100%">
                                        <!-- <option value="">All</option> -->
                                        <?php foreach ($services as $key => $service) : ?>
                                            <option value="<?= $service['service_type_id'] ?>" <?= ($enquiry->service_type_id == $service['service_type_id']) ? "selected" : "" ?>>
                                                <?= $service['service_type_name'] ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 pl-0 pr-0 n-field-main">
                                <div class="col-sm-12 n-radi-check-main p-0">
                                    <!-- <input type="radio" value="1" id="outside_area" name="outside_service_area" class="">
                                    <label for="outside_service_area">
                                        <span class="border-radius-3"></span>
                                        <p>Outside Service Area</p>
                                    </label> -->
                                
                                    <input type="hidden" value="1" name="outside_service_area">
                                    <input type="checkbox" id="outside_service_check" value="Y" name="outside_service_checked" <?= $enquiry->customer_service_area  ? "checked" : "" ?>>
                                    <label for="outside_service_check">
                                        <span class="border-radius-3"></span>
                                        <p>Outside Service Area</p>
                                    </label>
                                        
                                </div>
                            </div>
                            <div class="col-sm-12 pl-0 pr-0 n-field-main area_div" style="<?= $enquiry->customer_service_area ? "display:none;" : "display:block" ?>">
                                <p>Area
                                    <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="area_id" class="sel2" style="width:100%" id="area_id">
                                        <option value="">Select area</option>
                                        <?php foreach ($areas as $area) : ?>
                                            <option value="<?= $area->area_id ?>" <?= ($enquiry->area_id == $area->area_id) ? "selected" : "" ?>>
                                                <?= $area->area_name ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 pl-0 pr-0 n-field-main loc_div"  style="<?= $enquiry->customer_service_area ? "display:none;" : "display:block" ?>">
                                <p>Location
                                    <rf>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <select name="location_id" class="sel2" style="width:100%" id="location_id">
                                        <option value="">Select location</option>
                                        <?php foreach ($locations as $location) : ?>
                                            <option value="<?= $location->location_id ?>" <?= ($enquiry->location_id == $location->location_id) ? "selected" : "" ?>>
                                                <?= $location->location_name ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                                                
                            <div class="col-sm-12 pl-0 pr-0 n-field-main" id="outside_new" style="<?= $enquiry->customer_service_area  ? "display:block;" : "display:none" ?>">
                                <p>New Area & Address <rf></p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="text" name="outside_new_area" id="outside_new_area" value="<?= $enquiry->new_service_area ; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-1">&nbsp;</div>
                    <div class="col-sm-3">
                        <input type="hidden" name="enquiry_customer_id" id="enquiry_customer_id" value="<?= $enquiry->customer_id; ?>">
                        <div class="col-sm-12 pl-0 pr-0 n-field-main">
                            <p>Name</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="text" name="enquiry_customer_name" id="enquiry_customer_name" value="<?= $enquiry->enquiry_customer_name; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 pl-0 pr-0 n-field-main">
                            <p>Phone Number</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="text" name="enquiry_customer_phoneno" id="enquiry_customer_phoneno" value="<?= $enquiry->enquiry_customer_phoneno; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 pl-0 pr-0 n-field-main">
                            <p>Email</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="text" name="enquiry_customer_email" id="enquiry_customer_email"  value="<?= $enquiry->enquiry_customer_email; ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 pl-0 pr-0 n-field-main" id="enquiry_zone" style="<?= $enquiry->booking_type == 'OD' ? "display:block;" : "display:none" ?>">
                            <p>Search Zone</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <select name="enquiry_zone_id" class="sel2" style="width:100%">
                                    <option value=""></option>
                                    <?php foreach ($zones as $zone) : ?>
                                        <option value="<?= $zone->zone_id ?>">
                                            <?= $zone->zone_name ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 pl-0 pr-0 n-field-main">
                            <p>Notes</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <textarea type="text" name="enquiry_notes" id="enquiry_notes"><?= $enquiry->enquiry_notes; ?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 pl-0 pr-0 n-field-main">
                            <p>Service Type
                                <rf>
                            </p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <select name="enquiry_source_id" class="sel2" style="width:100%">
                                    <!-- <option value="">All</option> -->
                                    <?php foreach ($refer_sources as $source) : ?>
                                        <option value="<?= $source->refer_source_id ?>" <?= ($enquiry->refer_source_id == $source->refer_source_id) ? "selected" : "" ?>>
                                            <?= $source->refer_source ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5" id="week_available" style="<?= $enquiry->booking_type == 'WE' ? "display:block;" : "display:none" ?>">
                        <div class="col-sm-12 weekly_booking_time">
                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main">
                                            <input type="hidden" value="1" name="availability_week_days[1]">
                                            <input type="checkbox" id="week_day_1" class="week_day" class="week_day" value="Y" name="availability_checked[1]" <?= $enquiry_data1[1] ? "checked" : "" ?>>
                                            <label for="week_day_1">
                                                <span class="border-radius-3"></span>
                                                <p>Monday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[1]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[1]) && $enquiry_data1[1]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[1]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[1]->time_to; ?>" selected><?= $enquiry_data1[1]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[1]" value="<?= $enquiry_data1[1]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main">
                                            <input type="hidden" value="2" name="availability_week_days[2]">
                                            <input type="checkbox" id="week_day_2" class="week_day"  value="Y" name="availability_checked[2]" <?= $enquiry_data1[2] ? "checked" : "" ?>>
                                            <label for="week_day_2">
                                                <span class="border-radius-3"></span>
                                                <p>Tuesday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[2]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[2]) && $enquiry_data1[2]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[2]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[2]->time_to; ?>" selected><?= $enquiry_data1[2]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[2]" value="<?= $enquiry_data1[2]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main">
                                            <input type="hidden" value="3" name="availability_week_days[3]">
                                            <input type="checkbox" id="week_day_3" class="week_day" value="Y" name="availability_checked[3]" <?= $enquiry_data1[3] ? "checked" : "" ?>>
                                            <label for="week_day_3">
                                                <span class="border-radius-3"></span>
                                                <p>Wednesday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[3]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[3]) && $enquiry_data1[3]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[3]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[3]->time_to; ?>" selected><?= $enquiry_data1[3]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[3]" value="<?= $enquiry_data1[3]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main">
                                            <input type="hidden" value="4" name="availability_week_days[4]">
                                            <input type="checkbox" id="week_day_4" value="Y" class="week_day" name="availability_checked[4]" <?= $enquiry_data1[4] ? "checked" : "" ?>>
                                            <label for="week_day_4">
                                                <span class="border-radius-3"></span>
                                                <p>Thursday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[4]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[4]) && $enquiry_data1[4]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[4]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[4]->time_to; ?>" selected><?= $enquiry_data1[4]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[4]" value="<?= $enquiry_data1[4]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                
                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main">
                                            <input type="hidden" value="5" name="availability_week_days[5]">
                                            <input type="checkbox" id="week_day_5" value="Y" class="week_day" name="availability_checked[5]" <?= $enquiry_data1[5] ? "checked" : "" ?>>
                                            <label for="week_day_5">
                                                <span class="border-radius-3"></span>
                                                <p>Friday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[5]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[5]) && $enquiry_data1[5]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[5]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[5]->time_to; ?>" selected><?= $enquiry_data1[5]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[5]" value="<?= $enquiry_data1[5]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main">
                                            <input type="hidden" value="6" name="availability_week_days[6]">
                                            <input type="checkbox" id="week_day_6" value="Y" class="week_day" name="availability_checked[6]" <?= $enquiry_data1[6] ? "checked" : "" ?>>
                                            <label for="week_day_6">
                                                <span class="border-radius-3"></span>
                                                <p>Saturday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[6]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[6]) && $enquiry_data1[6]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[6]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[6]->time_to; ?>" selected><?= $enquiry_data1[6]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[6]" value="<?= $enquiry_data1[6]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-0 booking-type-det-set">
                                <div class="col-sm-3 booking-type-det-day">
                                    <div class="n-field-box">
                                        <div class="n-radi-check-main" >
                                            <input type="hidden" value="0" name="availability_week_days[0]">
                                            <input type="checkbox" id="week_day_0" value="Y" class="week_day" name="availability_checked[0]" <?= $enquiry_data1[0] ? "checked" : "" ?>>
                                            <label for="week_day_0">
                                                <span class="border-radius-3"></span>
                                                <p>Sunday</p>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_from[0]" class="sel2 week_time" style="width:100%">
                                        <option value="">Start time</option>
                                        <?php foreach ($time_slots as $time => $label) : ?>
                                            <option value="<?= $time; ?>" <?= isset($enquiry_data1[0]) && $enquiry_data1[0]->time_from == $time ? "selected" : "" ?>>
                                                <?= $label; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <select name="availability_time_to[0]" class="sel2" style="width:100%">
                                        <!-- <option value="">End time</option> -->
                                        <option value="<?= $enquiry_data1[0]->time_to; ?>" selected><?= $enquiry_data1[0]->time_to; ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-3 booking-type-det-time">
                                    <div class="row m-0 n-field-main">
                                        <!-- <p>Total Hours</p> -->
                                        <div class="n-field-box">
                                            <input name="working_hour[0]" value="<?= $enquiry_data1[0]->total_hours; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-12" id="enquiry_save" style="<?= $enquiry->booking_type == 'WE' ? "display:block;" : "display:none" ?>">
                        <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                        <button type="button" class="n-btn" id="enquiry-update-btn">Update</button>
                    </div>
                    <div class="col-sm-12" id="enquiry_search" style="<?= $enquiry->booking_type == 'OD' ? "display:block;" : "display:none" ?>">
                        <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                        <button type="button" class="n-btn" id="enquiry-search-btn">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>