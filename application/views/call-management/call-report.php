<style>
  .tabbable .nav-tabs>li>a {
    padding: 7px 12px;
  }

  .width-auto {
    width: auto;
  }

  .m-auto {
    margin: 0 auto;
  }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>


<div class="row m-0">
  <div class="col-sm-12">
    <div class="widget">
      <div class="widget-header">
        <ul>
          <li>
            <i class="icon-user"></i>
            <h3>Call Management Report</h3>

          </li>
          <li>
            <!-- <php  print_r($activity_date_to);die(); ?> -->
              <form method='post'>
                <input type="text" id="from_date_call" name="from_date" value="<?= $activity_date; ?>">
          </li>
            <li>
              <input type="text" id="to_date_call" name="to_date" value="<?= $activity_date_to; ?>">
            </li>
            <li>
              <input type="submit" class="n-btn" value="GO" id="call_report" name="call_report">
            </li>
          </form>
          <li class="mr-0 float-right">
            <div class="topiconnew border-0 green-btn">
              <a href="<?php echo base_url('call-management'); ?>" title="Call Management"> <i class="fa fa-phone"></i></a>
            </div>
          </li>
        </ul>
      </div>
      <!-- /widget-header -->
      <div class="widget-content no-top-padding">
        <div class="tabbable">

          <style>
            .table-bordered {
              border-radius: 0px !important;
            }


            .table-bordered thead:first-child tr:first-child th:first-child,
            .table-bordered tbody:first-child tr:first-child td:first-child {
              border-radius: 0px !important;
            }

            table td {
              font-family: Arial, Helvetica, sans-serif !important;
              text-align: left !important;
            }
          </style>
          <script>
            jQuery(function($) {
              //$(".accordion-content").css("display", "none");
              $(".accordion-title").click(function() {
                //$(".accordion-title").not(this).removeClass("open");
                //$(".accordion-title").not(this).next().slideUp(300);
                $(this).toggleClass("open");
                $(this).next().slideToggle(300);
              });
            });
          </script>

<div class="tab-main">
	<div>
		<div class="tab-btn active" data-id="cont1">Call History Report</div>
		<div class="tab-btn" data-id="cont2">Booking Call Report</div>
		<div class="tab-btn" data-id="cont3">Reactivate Call Report</div>
		<div class="tab-btn" data-id="cont4">Suspend Call Report</div>
		<div class="tab-btn" data-id="cont5">Call Complaint Report</div>
		<div class="tab-btn" data-id="cont6">Call Enquiry Report</div>
		<!-- <div class="tab-btn" data-id="cont7">Call Preference Report</div> -->
		<div class="tab-btn" data-id="cont8">Call Feedback Report</div>
		<div class="clear"></div>
	</div>
	
	<div style="background: #F4F4F4;">
		<div class="tab-cont" id="cont1">
		  <fieldset>
        <div class="col-sm-12 p-0" id="call_history">
          <table id="cal_his" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Sl.No.
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Number
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Call Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Called For
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Call Attended By
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($call_report_all)) {
                $i = 0;
                foreach ($call_report_all as $c_history) {
                  echo '<tr>
                    <td style="line-height: 18px; width: 20px"><center>' . '<b>'. ++$i . '</b>'.'</center></td>

                    <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->call_history_id) . '</td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->customer_name) . '</td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->customer_code) . '</td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape($c_history->mobile_number) . '</td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;">' . html_escape(DateTime::createFromFormat('Y-m-d H:i:s', $c_history->added_date_time)->format('d M Y')) . '</td>
                    <td style="line-height: 18px; cursor: pointer; text-align : right;">' .   html_escape($c_history->enquiry_type ?: "-") . '</td>
                    <td style="line-height: 18px; cursor: pointer; text-align : right;">' . html_escape($c_history->user_fullname) . '</td>
                  </tr>';
                }
              } else {
                echo '<tr><td style="line-height: 18px;" colspan="6"><center>No Records!</center></td>';
              }
              ?>
            </tbody>
          </table>
        </div>
      </fieldset>
		</div>
		
		<div class="tab-cont" id="cont2"> 
		  <fieldset>
        <div class="col-sm-12 p-0" id="personal">
          <table id='book_rprt' class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Sl.No.
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Maid Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    BookIng Type
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Type
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Zone & Area
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Day
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Timing
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Called Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Start Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service End Date
                  </center>
                </th>

            </thead>
            <tbody>

              <?php
              if (count($call_book_report) > 0) {
                $j = 1;
                foreach ($call_book_report as $cbr) {
                  $week = [0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday'];
                  $weekdays = $cbr->service_week_day;
                  $weekdayname = $week[$weekdays];
                  $time_from = date("h:i A", strtotime($cbr->time_from));
                  $time_to = date("h:i A", strtotime($cbr->time_to));
                  // print_r($weekdayname);die();
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 50px"><b><?= $j++; ?></b></td>
                    <td style="line-height: 18px; width: 50px"><?= $cbr->call_history_id ?></td>
                    <td style="line-height: 18px; width: 50px"><?= $cbr->customer_code ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $cbr->customer_name ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $cbr->maid_name ?></td>
                    <td style="line-height: 18px; width: 300px"><?= ($cbr->booking_type == 'WE') ? 'Weekly' : (($cbr->booking_type == 'OD') ? 'One Day' : 'Bi Weekly'); ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $cbr->service_type_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= 'Zone : ' . $cbr->zone_name . '<br>' . 'Area : ' . $cbr->area_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $weekdayname; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $time_from . '-' . $time_to ?></td>
                    <td style="line-height: 18px; width: 300px"><?= DateTime::createFromFormat('Y-m-d H:i:s', $cbr->booked_datetime)->format('d-m-Y H:i:s') ?></td>
                    <td style="line-height: 18px; width: 300px"><?= date('d-m-Y',strtotime($cbr->service_start_date));?></td>
                    <td style="line-height: 18px; width: 300px"><?= ($cbr->service_end==0) ? 'Never Ends ': date('d-m-Y',strtotime($cbr->service_actual_end_date));?></td>
                  </tr>
                  <?php
                }
              } else { ?>
                <tr>
                  <td style="line-height: 18px;" colspan="13">
                    <center>No Records!</center>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </fieldset>
		</div>
		
		<div class="tab-cont" id="cont3"> 
		  <fieldset>
        <div class="col-sm-12 p-0" id="personal">
          <table id='reactivate_rprt' class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                <center>
                  Sl.No.
                </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Type
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Reactivated Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Reactivated Service Start Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Reactivated Service End Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Reactivated By
                  </center>
                </th>

            </thead>
            <tbody>

              <?php
              if (!empty($call_reactivate_report) && count($call_reactivate_report) > 0) : 
                $j = 1;
                foreach ($call_reactivate_report as $car) : 
                  // print_r($weekdayname);die();
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 50px"><b><?= $j++; ?></b></td>
                    <td style="line-height: 18px; width: 50px"><?= $car->call_history_id ?></td>
                    <td style="line-height: 18px; width: 50px"><?= $car->customer_code ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $car->customer_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $car->service_type_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= date("d-m-Y H;i:s", strtotime($car->added_date_time)) ?></td>
                    <td style="line-height: 18px; width: 300px"><?= date("d-m-Y", strtotime($car->service_date)) ?></td>
                    <td style="line-height: 18px; width: 300px"><?= date("d-m-Y", strtotime($car->re_activate_date_from)) ?></td>
                    <td style="line-height: 18px; width: 300px"><?= date("d-m-Y", strtotime($car->re_activate_date_to)) ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $car->user_fullname ?></td>

                  </tr>
                  <?php
                endforeach;?>
              <?php else :  ?>
                <tr>
                  <td style="line-height: 18px;" colspan="13">
                    <center>No Records!</center>
                  </td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </fieldset>
		</div>
		
		<div class="tab-cont" id="cont4"> 
		  <fieldset>
        <div class="col-sm-12 p-0" id="personal">
          <table id='suspend_rprt' class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Sl.No.
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Booking ID
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Maid Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Start Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Type
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Service Day
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Booking Type
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Time Slots
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Total Hours
                  </center>
                </th>

            </thead>
            <tbody>

              <?php
              if (count($call_suspend_report) > 0) {
                $j = 1;
                foreach ($call_suspend_report as $csr) {
                  $week = [0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday'];
                  $weekdays = $csr->service_week_day;
                  $weekdayname = $week[$weekdays];
                  $time_from = date("h:i A", strtotime($csr->time_from));
                  $time_to = date("h:i A", strtotime($csr->time_to));
                  // print_r($weekdayname);die();
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 50px"><b><?= $j++; ?></b></td>
                    <td style="line-height: 18px; width: 50px"><?= $csr->call_history_id ?></td>
                    <td style="line-height: 18px; width: 50px"><?= $csr->customer_code ?></td>
                    <td style="line-height: 18px; width: 50px"><?= $csr->booking_id ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $csr->customer_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $csr->maid_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= date("d-m-Y", strtotime($csr->service_start_date)) ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $csr->service_type_name; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $weekdayname; ?></td>
                    <td style="line-height: 18px; width: 300px"><?= ($cbr->booking_type == 'WE') ? 'Weekly' : (($cbr->booking_type == 'OD') ? 'One Day' : 'Bi Weekly'); ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $time_from . '-' . $time_to ?></td>
                    <td style="line-height: 18px; width: 300px"><?= $csr->total_hrs; ?></td>

                  </tr>
                  <?php
                }
              } else { ?>
                <tr>
                  <td style="line-height: 18px;" colspan="13">
                    <center>No Records!</center>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </fieldset>
		</div>
		
		<div class="tab-cont" id="cont5"> 
		  <fieldset>
        <div class="col-sm-12 p-0" id="call_history">
          <table id="cal_comp" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Sl.No.
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Complaint ID
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint Added Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint Service Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint Time
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint Type
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Action Taken
                  </center>
                </th>
                <th style="line-height: 18px; width: 30px">
                  <center>
                    Status
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint Against
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Complaint Against Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Call Attended By
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($call_complaint_report)) {
                $i = 0;
                foreach ($call_complaint_report as $ccr) {

                  if ($ccr->complaint_against == 'D') {
                    $comp_against = 'Driver';
                  } else if ($ccr->complaint_against == 'U') {
                    $comp_against = 'User';
                  } else if ($ccr->complaint_against == 'C') {
                    $comp_against = 'Staff';
                  }

                  $status_style = ''; 


                  switch ($ccr->status) {
                    case 'In Progress':
                      $status_style = 'border-radius: 20px; font-weight: bold; padding: 5px 8px;';
                      break;
                    case 'Completed':
                      $status_style = 'border-radius: 20px; font-weight: bold; padding: 5px 8px;';
                      break;
                    case 'Assigned':
                      $status_style = 'border-radius: 20px; font-weight: bold; padding: 5px 8px;';
                      break;
                    case 'On Hold':
                      $status_style = 'border-radius: 20px; font-weight: bold; padding: 5px 8px;';
                      break;
                    case 'Rejected':
                      $status_style = 'border-radius: 20px; font-weight: bold; padding: 5px 8px;';
                      break;
                    default:
                      $status_style = ''; 
                  }
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 20px">
                      <center> <b> <?= ++$i ?> </b> </center>
                    </td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->call_history_id ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->customer_code ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint_no ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->customer_name ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= date('d-m-Y', strtotime($ccr->added_date_time)) ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= date('d-m-Y', strtotime($ccr->service_date)) ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= isset($ccr->complaint_time) ? date('h:i A', strtotime($ccr->complaint_time)) : "" ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->type ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->action_taken ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;">
                      <div style="display: flex; justify-content: space-between; align-items: center;">
                      <span style="<?= $status_style ?>"><?= $ccr->status ?></span>
                        <button class="ml-2 bg-primary edit-status-btn" data-call-history-id="<?= $ccr->call_history_id ?>">Edit</button>
                      </div>
                    </td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $comp_against ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->complaint_against_name ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align : center;"><?= $ccr->user_fullname ?></td>
                  </tr>
                <?php }
              } else { ?>
                <tr>
                  <td style="line-height: 18px;" colspan="13">
                    <center>No Records!</center>
                  </td>
                </tr>
              <?php } ?>
            </tbody>

          </table>
        </div>
      </fieldset>
		</div>
		
		<div class="tab-cont" id="cont6"> 
		  <fieldset>
        <div class="col-sm-12 p-0" id="call_history">
          <table id="cal_enq" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Sl.No.
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Enquired Date
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Enquired Time
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Enquered Service Area
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Mobile
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Customer Email
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Staff Name
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Staff Service
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Source
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Enquiry Status
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Called Date
                  </center>
                </th>
                <th style="line-height: 18px">
                  <center>
                    Action
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($call_enquiry_report)) {
                $i = 0;
                foreach ($call_enquiry_report as $cer) {
                  $from_date = date('d-m-Y', strtotime($cer->date_from));
                  // $to_date = date('d-m-Y', strtotime($cer->date_to));
                  $time_from = date('h:i A', strtotime($cer->time_from));
                  $time_to = date('h:i A', strtotime($cer->time_to));
                  $diff_seconds = strtotime($cer->time_to) - strtotime($cer->time_from);
                  $hours = floor($diff_seconds / 3600);
                  $minutes = floor(($diff_seconds % 3600) / 60);
                  $sum_enq_time = $hours . '.' . str_pad($minutes, 2, '0', STR_PAD_LEFT) . ' hrs';
                  $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
                  if ($cer->enquiry_week_day != "") {
                    $week_day = $days[$cer->enquiry_week_day];
                    $week_day = '<strong>Weekday:</strong><br>' .$week_day;
                  }
                  if ($cer->customer_service_area == 0) {
                    $service_area = $cer->zone_name . '<br>' . $cer->area_name . '<br>' . $cer->location_name;
                  } else if ($cer->customer_service_area == 1) {
                    $service_area = '<strong>Outside Area:</strong><br>' . $cer->new_service_area;
                  }
                  if($cer->enquiry_status == "Saved") { 
                    // $url = base_url('employee/list');
                    $edit_btn = '<a class="n-btn-icon purple-btn" href='. base_url('call-management/Enquiry_Edit/'. $cer->call_history_id).' title="Edit"><i class="btn-icon-only icon-pencil"> </i></a>';
                  } else {
                    $edit_btn = '-';
                  }
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 20px;">
                      <center><b><?= ++$i; ?></b></center>
                    </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->call_history_id ?> </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->customer_name ?> </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->customer_code ?> </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $from_date. '<br>'. $week_day?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $time_from . ' - ' . $time_to .'<br><b>'.'Total Hours : </b> '.$sum_enq_time ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $service_area ?: '-' ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->enquiry_customer_name ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->enquiry_customer_phoneno ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->enquiry_customer_email ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->maid_name ?: 'ALL MAIDS' ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->service_type_name ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cer->refer_source ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;">
                      <div style="display: flex; justify-content: space-between; align-items: center;">
                        <span><?= $cer->enquiry_status ?></span>
                        <button class="ml-2 bg-primary edit-enquiry-status-btn" data-call-history-id="<?= $cer->call_history_id ?>">Edit</button>
                      </div>
                          
                    </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= date('d-m-Y', strtotime($cer->added_date_time)) ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $edit_btn?></td>
                  </tr>
                  <?php
                }
              } else { ?>
                <tr>
                  <td style="line-height: 18px;" colspan="11">
                    <center>No Records!</center>
                  </td>
                </tr>
              <?php }
              ?>
            </tbody>
          </table>
        </div>
      </fieldset>
		</div>
		
		<!-- <div class="tab-cont" id="cont7"> 
		     <fieldset>
                <div class="col-sm-12 p-0" id="call_history">
                  <table id="cal_pref" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Sl.No.
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Call ID
                          </center>
                        </th>
                        <th style="line-height: 18px;">
                          <center>
                            Customer Name
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Customer ID
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Prefered Booked Date
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Prefered Booked Time
                          </center>
                        </th>
                        <th style="line-height: 18px; width: 20px">
                          <center>
                            Prefered Booking Type
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Preference Location & area
                          </center>
                        </th>
                       
                        <th style="line-height: 18px">
                          <center>
                            Staff Name
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Staff Service
                          </center>
                        </th>
                        <th style="line-height: 18px">
                          <center>
                            Called Date
                          </center>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($call_preference_report)) {
                        $i = 0;
                        foreach ($call_preference_report as $cpr) {
                          $from_date = DateTime::createFromFormat('Y-m-d', $cpr->date_from)->format('d M Y');
                          $to_date = DateTime::createFromFormat('Y-m-d', $cpr->date_to)->format('d M Y');
                          $time_from = date('h:i A', strtotime($cpr->time_from));
                          $time_to = date('h:i A', strtotime($cpr->time_to));

                          if ($cpr->booking_type == 'OD') {
                            $booking_type = 'ONE DAY';
                          } else if ($cpr->booking_type == 'WE') {
                            $booking_type = 'WEEKLY';
                          } else if ($cpr->booking_type == 'BI') {
                            $booking_type = 'BI WEEKLY';
                          }
                      ?>
                          <tr>
                            <td style="line-height: 18px; width: 20px;">
                              <center><b><?= ++$i; ?></b></center>
                            </td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cpr->call_history_id ?> </td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cpr->customer_name ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cpr->customer_code ?> </td>

                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $from_date . ' - ' . $to_date ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $time_from . ' - ' . $time_to ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $booking_type ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= 'Location :' . $cpr->location_name . '<br>' . 'Area :' . $cpr->area_name; ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cpr->maid_name ?: 'ALL MAIDS' ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cpr->service_type_name ?></td>
                            <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= date('d-m-Y', strtotime($cpr->added_date_time)) ?></td>
                          </tr>
                        <?php
                        }
                      } else { ?>
                        <tr>
                          <td style="line-height: 18px;" colspan="11">
                            <center>No Records!</center>
                          </td>
                        </tr>
                      <?php }
                      ?>
                    </tbody>


                  </table>
                </div>
              </fieldset>
		</div> -->
		
		<div class="tab-cont" id="cont8"> 
		  <fieldset>
        <div class="col-sm-12 p-0" id="call_history">
          <table id="cal_feedback" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Sl.No.
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Call ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer Name
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Customer ID
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Feedback Date
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Feddback Maid Name
                  </center>
                </th>
                <th style="line-height: 18px; width: 20px">
                  <center>
                    Feedback Message
                  </center>
                </th>

                <th style="line-height: 18px; width: 20px">
                  <center>
                    Called Date
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($call_feedback_report)) {
                $i = 0;
                foreach ($call_feedback_report as $cfr) {

                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 20px;">
                      <center> <b> <?= ++$i; ?> </b> </center>
                    </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cfr->call_history_id ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cfr->customer_name ?> </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cfr->customer_code ?> </td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= date('d-m-Y', strtotime($cfr->date)) ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cfr->maid_name ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= $cfr->message ?></td>
                    <td style="line-height: 18px; cursor: pointer; text-align: center;"><?= date('d-m-Y', strtotime($cfr->added_date_time)) ?></td>
                  </tr>
                  <?php
                }
              } else { ?>
                <tr>
                  <td style="line-height: 18px;" colspan="11">
                    <center>No Records!</center>
                  </td>
                </tr>
              <?php }
              ?>
            </tbody>


          </table>
        </div>
      </fieldset>
		</div>
	</div>
	
	
	<script type="text/javascript">
    $(document).ready(function() {
    $(".tab-cont").each(function() {
      $(this).hide(0);
      if ($(this).attr('id') == 'cont1') {
      $(this).show(0);
      }
    });

    $('.tab-btn').on("click", function(e) {
      e.preventDefault();
      $('.tab-btn').removeClass('active');
      $(this).addClass('active');
      var id = $(this).attr('data-id');

      if (!$('.tab-cont[id="' + id + '"]').is(':visible')) {
      $(".tab-cont").hide(500);
      $('.tab-cont[id="' + id + '"]').show(500);
      }
    });
    });
  </script> 
</div>

        </div>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span8 -->
</div>
<!-- /row -->


<div id="edit-status-popup" style="display: none;height:150px;width:350px">
  <div class="modal-body">
    <form method="post">
      <input type="hidden" id='call_history_id_pop_up' name="call_history_id" value="">
      <label for="status">Select Status:</label>
      <select name="status" id="status">
        <option value="">---select Status---</option>
        <option value="In Progress">In Progress</option>
        <option value="Completed">Completed</option>
        <option value="Assigned">Assigned</option>
        <option value="On Hold">On Hold</option>
        <option value="Rejected">Rejected</option>
      </select>
  </div>
  <div class="modal-footer">
    <button class="btn btn-success update-btn-status" type="submit">Update Status</button>
    <button type="button" class="btn btn-danger cancel-btn">Cancel</button>
  </div>
  </form>
</div>


<div id="edit-enquiry-status-popup" style="display: none;height:150px;width:350px">
  <div class="modal-body">
    <form method="post">
      <input type="hidden" id='call_history_id_pop_up_enquiry' name="call_history_id" value="">
      <label for="status">Select Status:</label>
      <select name="enquiry_status" id="enquiry_status">
        <option value="">---select Status---</option>
        <option value="Pending">Pending</option>
        <option value="Booking Confirmed ">Booking Confirmed </option>
        <option value="Follow Up">Follow Up</option>
      </select>
  </div>
  <div class="modal-footer">
    <button class="btn btn-success update-status" type="submit">Update Status</button>
    <button type="button" class="btn btn-danger cancel-btn">Cancel</button>
  </div>
  </form>
</div>



<script>

  $('.edit-status-btn').click(function() {
    var call_history_id = $(this).data('call-history-id');
    $('#call_history_id_pop_up').val(call_history_id);

    $.fancybox.open({
      autoCenter: true,
      fitToView: true,
      scrolling: false,
      openEffect: 'fade',
      openSpeed: 100,
      helpers: {
        overlay: {
          css: {
            'background': 'rgba(0, 0, 0, 0.3)'
          },
          closeClick: false
        }
      },
      width: '800px',
      height: '800px',
      padding: 0,
      content: $('#edit-status-popup'),
    });
  });

  $('.edit-enquiry-status-btn').click(function() {
    var call_history_id = $(this).data('call-history-id');
    $('#call_history_id_pop_up_enquiry').val(call_history_id);

    $.fancybox.open({
      autoCenter: true,
      fitToView: true,
      scrolling: false,
      openEffect: 'fade',
      openSpeed: 100,
      helpers: {
        overlay: {
          css: {
            'background': 'rgba(0, 0, 0, 0.3)'
          },
          closeClick: false
        }
      },
      width: '800px',
      height: '800px',
      padding: 0,
      content: $('#edit-enquiry-status-popup'),
    });
  });


  $(document).on('click', '.cancel-btn', function() {
    $.fancybox.close();
  });

  $('#edit-status-popup form').on('submit', function(e) {
    $('.update-btn-status').show();
    e.preventDefault();
    var status=$('#status').val();
    if(status==''){
      alert('please select a status');
      return false;
    }
    if (status !='') {
      $.ajax({
        type: 'POST',
        url: _base_url + 'call_management/update_status_complaint',
        data: $('#edit-status-popup form').serialize(),
        dataType: 'json',
        success: function(response) {
          console.log(response);
          if (response.status == 'success') {
            $.fancybox.close();
            toast('success', response.message);
            setTimeout(function() {
              location.reload();
            }, 2500);

          } else {
            toast('error', response.message);
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
          toast('error', errorThrown);
        }
      });
    }
  });

  $('#edit-enquiry-status-popup form').on('submit', function(e) {
    $('.update-status').show();
    e.preventDefault();
    var status=$('#edit-enquiry-status-popup form #enquiry_status').val();
    if(status==''){
      alert('please select a status');
      return false;
    }
    if (status !='') {
      $.ajax({
        type: 'POST',
        url: _base_url + 'call_management/update_status_enquiry',
        data: $('#edit-enquiry-status-popup form').serialize(),
        dataType: 'json',
        success: function(response) {
          console.log(response);
          if (response.status == 'success') {
            $.fancybox.close();
            toast('success', response.message);
            setTimeout(function() {
              location.reload();
            }, 2500);

          } else {
            toast('error', response.message);
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
          toast('error', errorThrown);
        }
      });
    }
  });

  $(document).ready(function() {

    $('#from_date_call,#to_date_call').datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true,
    });
    $('#book_rprt,#cal_his,#reactivate_rprt,#cal_comp,#cal_enq,#cal_pref,#cal_feedback').DataTable();

  });
</script>