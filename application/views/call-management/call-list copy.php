<div id="customer-add-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Customer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form name="customer-popup" class="form-horizontal" method="post" id="customer-popup">
        <div class="modal-body">
            <div class="row m-0">
                <input type="hidden" value="1" id="is_company_1" name="customer_type_id">
                <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Customer  <rf></p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="customer_name" id="customer_name" style="width:220px;">
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Email <rf></p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="customer_email" id="customer_email" style="width:220px;">
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Mobile Number <rf></p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="customer_mobilenumber" id="customer_mobilenumber" style="width:220px;">
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Area</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <select name="customer_area_id" id="customer_area_id" class="sel2"  data-placeholder="Select area" style="width:100%">
                                <option value="">-- Select area --</option>
                                    <?php foreach ($areas as $area): ?>
                                    <option value="<?=$area->area_id?>"><?=$area->area_name?></option>
                                    <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-0">
               <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Location</p>
                        <div class="col-sm-12 p-0 n-field-box">
                        <select name="customer_location" id="customer_location" class="sel2" data-placeholder="Select location" style="width:100%">
                        <option value="">-- Select location --</option>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 n-form-set-left">

                    <div class="row m-0 n-field-main">
                        <p>Landmark</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <select name="customer_landmark" id="customer_landmark" class="sel2" data-placeholder="Select landmark" style="width:100%">
                            <option value="">-- Select landmark --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-0">
                <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Resdidence Type <rf></p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <select name="customer_residence_type" id="customer_residence_type" class="sel2" data-placeholder="Select residence type" style="width:100%">
                            <option value="">-- Select residence type --</option>
                            <?php foreach ($residence_types as $residence_type) : ?>
                                <option value="<?= $residence_type->residence_type_id ?>"><?= $residence_type->residence_type ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 n-form-set-left">
                    <div class="row m-0 n-field-main">
                        <p>Address <rf></p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <textarea type="text" name="customer_address" id="customer_address"></textarea>
                        </div>
                    </div>
                </div>
            </div>
          <p class="text-danger" id="form-error">&nbsp;</p>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" id="customer-save-btn">Save</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
   <div class="col-sm-12">
        <div class="widget">
            <div class="widget-header">
                <div class="book-nav-top">
                    <ul>
                        <li>
                            <i class="icon-user"></i>
                            <h3>Call Management</h3>
                        </li>
                        <li class="mr-0 float-right">
                            <div class="topiconnew border-0 green-btn">
                                <a href="<?php echo base_url('call-management'); ?>" title="Call Management"> <i class="fa fa-list"></i></a>
                            </div>
                        </li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
            <div class="widget-content">
                <div class="col-sm-6">
                    <div class="row m-0 n-field-main">
                        <p>Search Customer</p>
                            <div class="col-sm-4 p-0 n-field-box">
                            <select name="customer_id" class="sel2" id="customer_id" data-placeholder="Select customer" style="width:100%">
                            </select>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Customer Name</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customername"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Address</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customer-address"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Mobile</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customer-mobile"></span></p>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="row m-0">
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Location</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customer-location"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Sublocation</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customer-sublocation"></span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Customer Status</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customer-status"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-sm-3 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Area/Zone</p>
                                <div class="col-sm-2 p-0 n-field-box">
                                    <p><span id="customer-area-zone"></span></p>
                                </div>
                            </div>
                        </div>  
                        <div class="col-sm-6 n-form-set-left">
                            <div class="row m-0 n-field-main">
                                <p>Called For</p>
                                <div class="col-sm-7 p-0 n-field-box">
                                    <select name="called_for" class="sel2" style="width:100%" id="called_for">
                                        <?php foreach ($enquiry_types as $enquiry_type) : ?>
                                        <option value="<?= $enquiry_type->enquiry_type_id ?>"><?= $enquiry_type->enquiry_type ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                <table class="table table-hover">
                <thead>
                    <tr>
                    <th>Number</th>
                    <th>Date</th>
                    <th>For</th>
                    <th>Attended By</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if (count($call_lists) > 0) {
                    $i = 1;
                    foreach ($call_lists as $row) {
                    ?>
                    <tr>
                        <td><?php echo $row->mobile_number ?></td>
                        <td><?php echo DateTime::createFromFormat('Y-m-d H:i:s',$row->added_date_time)->format('d M Y') ?></td>
                        <td><?php echo $row->enquiry_type ?: "-"; ?></td>
                        <td><?php echo $row->user_fullname ?></td>
                        <td><?php echo $row->status ?: "-"; ?></td>
                        
                    </tr>
                    <?php
                        $i++;
                    }
                    } else {
                    ?>
                    <tr class="bg-warning">
                        <td colspan="6" style="text-align:center;">No Data Found !</td>
                    <tr>
                    <?php
                    }
                    ?>
                </tbody>
                </table>
                </div>
                <div class="col-sm-12">
                <div class="row m-0">
                <div class="tabbable">
                    <ul class="nav nav-tabs pb-4"  id="mytabs">
            
                        <li id="booking-li" data-tab="booking"><a href="#booking" data-toggle="tab">Booking</a></li>
                        <li id="enquiry-li" data-tab="enquiry"><a href="#enquiry" data-toggle="tab">Enquiry</a></li>
                        <li id="complaints-li" data-tab="complaints"><a href="#complaints" data-toggle="tab">Complaints</a></li>
                        <li id="re-activations-li" data-tab="re-activations"><a href="#re-activations" data-toggle="tab">Re-activation</a></li>
                        <li id="cancel-service-li" data-tab="cancel-service"><a href="#cancel-service" data-toggle="tab">Cancel a service</a></li>
                        <li id="suspend-service-li" data-tab="suspend-service"><a href="#suspend-service" data-toggle="tab">Suspend a service</a></li>
                        <li id="other-li" data-tab="other"><a href="#other" data-toggle="tab">Other</a></li>
                        <li id="preferences-li" data-tab="preferences"><a href="#preferences" data-toggle="tab">Preferences</a></li>
                    </ul>
                    <form id="new_call_management_form" class="form-horizontal" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="caller_typ_id" id="caller_typ_id">
                        <div class="tab-content">
                            <div class="tab-pane tab-panel" data-panel="booking">
                                <div class="col-sm-5">
                                    <div class="row m-0 n-field-main">
                                        <p>Booking Type</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="booking_type" class="sel2" style="width:100%">
                                                <option value="">-- Select type --</option>
                                                <?php foreach ($booking_types as $booking_type) : ?>
                                                <option value="<?= $booking_type->booking_type ?>"><?= $booking_type->booking_type_name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Date</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="date" name="date">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Maids</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="maid_id" class="sel2" style="width:100%">
                                                <option value="">All</option>
                                                <?php foreach ($pref_maids as $maid): ?>
                                                <option value="<?=$maid->maid_id?>"><?=$maid->maid_name?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Search Zone</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="zone_id" class="sel2" style="width:100%">
                                                <option value=""></option>
                                                <?php foreach ($zones as $zone) : ?>
                                                    <option value="<?= $zone->zone_id ?>"><?= $zone->zone_name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="1" name="availability_week_days[1]">
                                                <input type="checkbox" id="week_day_1" value="Y" name="availability_checked[1]">
                                                <label for="week_day_1">
                                                    <span class="border-radius-3"></span>
                                                    <p>Monday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[1]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[1]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div>     
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="2" name="availability_week_days[2]">
                                                <input type="checkbox" id="week_day_2" value="Y" name="availability_checked[2]">
                                                <label for="week_day_2">
                                                <span class="border-radius-3"></span>
                                                <p>Tuesday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[2]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[2]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div> 
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="3" name="availability_week_days[3]">
                                                <input type="checkbox" id="week_day_3" value="Y" name="availability_checked[3]">
                                                <label for="week_day_3">
                                                <span class="border-radius-3"></span>
                                                <p>Wednessday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[3]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[3]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="4" name="availability_week_days[4]">
                                                <input type="checkbox" id="week_day_4" value="Y" name="availability_checked[4]">
                                                <label for="week_day_4">
                                                <span class="border-radius-3"></span>
                                                <p>Thursday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[4]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[4]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="5" name="availability_week_days[5]">
                                                <input type="checkbox" id="week_day_5" value="Y" name="availability_checked[5]">
                                                <label for="week_day_5">
                                                <span class="border-radius-3"></span>
                                                <p>Friday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[5]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[5]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="6" name="availability_week_days[6]">
                                                <input type="checkbox" id="week_day_6" value="Y" name="availability_checked[6]">
                                                <label for="week_day_6">
                                                <span class="border-radius-3"></span>
                                                <p>Saturday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[6]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[6]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-sm-2">
                                        <div class="n-field-box">
                                            <div class="n-radi-check-main">
                                                <input type="hidden" value="0" name="availability_week_days[0]">
                                                <input type="checkbox" id="week_day_0" value="Y" name="availability_checked[0]">
                                                <label for="week_day_0">
                                                <span class="border-radius-3"></span>
                                                <p>Sunday</p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_from[0]" class="sel2" style="width:100%">
                                            <option value="">-- Select start time --</option>
                                            <?php foreach ($time_slots as $time => $label) : ?>
                                                <option value="<?= $time; ?>"><?= $label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="availability_time_to[0]" class="sel2" style="width:100%">
                                            <option value="">-- Select end time --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane tab-panel" data-panel="enquiry" style="display:none;">
                                <div class="col-sm-4">        
                                    <div class="row m-0 input-daterange">
                                        <div class="col-sm-5 n-form-set-left">
                                            <div class="row m-0 n-field-main">
                                                <p>Date From <rf></p>
                                                <div class="n-field-box">
                                                    <input name="date_from" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 n-form-set-left">
                                            <div class="row m-0 n-field-main">
                                            <p>Date To <rf></p>
                                                <div class="n-field-box">
                                                    <input name="date_to" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-0">
                                        <div class="col-sm-5 n-form-set-left">
                                            <div class="row m-0 n-field-main">
                                                <p>Time From <rf></p>
                                                <select name="enquiry_time_from" class="sel2" style="width:100%">
                                                    <option value="">-- Select start time --</option>
                                                    <?php foreach ($time_slots as $time => $label) : ?>
                                                        <option value="<?= $time; ?>"><?= $label; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 n-form-set-left">
                                            <div class="row m-0 n-field-main">
                                            <p>Time To <rf></p>
                                            <select name="enquiry_time_to" class="sel2" style="width:100%">
                                                <option value="">-- Select end time --</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Maid</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="maidId" class="sel2" style="width:100%">
                                                <option value="">All</option>
                                                <?php foreach ($pref_maids as $maid): ?>
                                                <option value="<?=$maid->maid_id?>"><?=$maid->maid_name?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Service Type <rf></p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="service_id" class="sel2" style="width:100%">
                                                <option value="">All</option>
                                                <?php foreach ($services as $key => $service): ?>
                                                <option value="<?=$service['service_type_id']?>"><?=$service['service_type_name']?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <div class="col-sm-12 n-radi-check-main p-0">
                                            <input type="radio" value="1" id="outside_area" name="outside_service_area" class="" checked>
                                            <label for="outside_service_area">
                                            <span class="border-radius-3"></span>
                                            <p>Outside Service Area</p>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Area <rf></p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="area_id" class="sel2" style="width:100%" id="area_id">
                                            <option value="">-- Select area --</option>
                                            <?php foreach ($areas as $area): ?>
                                            <option value="<?=$area->area_id?>"><?=$area->area_name?></option>
                                            <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Location <rf></p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="location_id" class="sel2" style="width:100%" id="location_id">
                                            <option value="">-- Select location --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">  
                                    <input type="hidden" name="enquiry_customer_id" id="enquiry_customer_id">
                                    <div class="row m-0 n-field-main">
                                        <p>Name
                                        </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="text" name="enquiry_customer_name" id="enquiry_customer_name">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Phone Number</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                         <input type="text" name="enquiry_customer_phoneno" id="enquiry_customer_phoneno">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Email</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                         <input type="text" name="enquiry_customer_email" id="enquiry_customer_email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane tab-panel" data-panel="complaints" style="display:none;">
                                <div class="col-sm-4">        
                                    <div class="row m-0">
                                        <div class="col-sm-5 n-form-set-left">
                                            <div class="row m-0 n-field-main">
                                                <p>Date</p>
                                                <div class="n-field-box">
                                                <input type="date" name="complaint_date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 n-form-set-left">
                                            <div class="row m-0 n-field-main">
                                                <p>Time</p>
                                                <div class="n-field-box">
                                                    <select name="complaint_time" class="sel2" style="width:100%">
                                                        <option value="">-- Select start time --</option>
                                                        <?php foreach ($time_slots as $time => $label) : ?>
                                                        <option value="<?= $time; ?>"><?= $label; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Complaint No</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="text" name="complaint_no"  autocomplete="off" placeholder="MM-0048-11-2023">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Type</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="complaint_type" class="sel2" style="width:100%">
                                                <option value="">--select type--</option>
                                                <?php foreach ($complaint_types as $complaint_type) : ?>
                                                    <option value="<?=$complaint_type->type_id?>"><?=$complaint_type->type?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Complainer</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="hidden" name="complainer_id" id="complainer_id">
                                            <input type="text" name="complainer_name"  id="complainer_name">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Phone No</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="text" name="complainer_phone"  id="complainer_phone">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Email</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="text" name="complainer_email"  id="complainer_email">
                                        </div>
                                    </div>
                                    <div class="row m-0 n-field-main">
                                        <p>Existing Customer</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <select name="existing_customer" class="sel2" style="width:100%">
                                                <option value="Y">YES</option>
                                                <option value="N">NO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">  
                                    <div class="row m-0">
                                        <div class="row m-0 n-field-main">
                                            <p>Against</p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <select name="complaint_against" id="employee_type_id" class="sel2" style="width:100%">
                                                    <option value="C">Cleaner</option>
                                                    <option value="D">Driver</option>
                                                    <option value="U">User</option>
                                                    <!-- <option value="4">Mechanic</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row m-0 n-field-main">
                                            <p>Employee Name</p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <select name="employee_name" id="employee_name" class="sel2" style="width:100%">
                                                    <option value="">--select--</option>
                                                    <?php foreach ($maids as $maid): ?>
                                                    <option value="<?=$maid->maid_id?>"><?=$maid->maid_name?></option>
                                                    <?php endforeach;?>
                                                    <input type="hidden" name="complaint_against_name" id="complaint_against_name">

                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="row m-0 n-field-main">
                                            <p>Code</p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <input type="text" name="complainer_code"  autocomplete="off">
                                            </div>
                                        </div> -->
                                        <div class="row m-0 n-field-main">
                                            <p>Complain</p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                 <textarea type="text" name="complaint" id="complaint"></textarea>
                                            </div>
                                        </div>
                                        <div class="row m-0 n-field-main">
                                            <p>Action Taken</p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                 <textarea type="text" name="action_taken" id="action_taken"></textarea>
                                            </div>
                                        </div>
                                        <div class="row m-0 n-field-main">
                                            <p>Complaint Status</p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <select name="complaint_status" class="sel2" style="width:100%">
                                                    <option value="">--select status-</option>
                                                    <?php foreach ($complaint_statuses as $complaint_status) : ?>
                                                        <option value="<?=$complaint_status->complaint_status ?>"><?=$complaint_status->complaint_status?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane tab-panel" data-panel="re-activations" style="display:none;">
                            </div>
                            <div class="tab-pane tab-panel" data-panel="cancel-service" style="display:none;">
                            </div>
                            <div class="tab-pane tab-panel" data-panel="suspend-service" style="display:none;">
                            </div>
                            <div class="tab-pane tab-panel" data-panel="other" style="display:none;">
                            </div>
                            <div class="tab-pane tab-panel" data-panel="preferences" style="display:none;">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-danger" id="new-customer-form-error">&nbsp;</p>
                            <button type="button" class="n-btn" id="call-management-save-btn">Submit</button>
                        </div>
                    </form>
                </div>
               </div>
              </div>
            </div>
        </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<script>
 
</script>