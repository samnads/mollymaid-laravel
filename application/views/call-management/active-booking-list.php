<div class="col-sm-12 n-mobile-scroll">
    <table width="100%" border="1" cellpadding="0" cellspacing="0" >
        <thead>
            <tr>
                <th align="center" valign="middle" style="width:50px;">
                    <p>Sl:No</p>
                </th>
                <th align="left" valign="middle">
                    <p class="text-left">Customer Name</p>
                </th>
                <th align="left" valign="middle">
                    <p class="text-left">Service Date</p>
                </th>
                <th align="left" valign="middle">
                    <p class="text-left">Day</p>
                </th>
                <th align="left" valign="middle">
                    <p class="text-left">Booking Id</p>
                </th>
                <th align="left" valign="middle">
                <p class="text-left">Maid Name</p>
                </th>
                <th align="left" valign="middle">
                <p class="text-left">Booking Type</p>
                </th>
                <th align="center" valign="middle">
                    <p>Time Slot</p>
                </th>
                
                <th align="center" valign="middle">
                    <p>Total Hours</p>
                </th>
                <th align="center" valign="middle">
                    <p>Action</p>
                </th>
                
            </tr>
        </thead>
        <tbody id="booking-customer">
        <?php
            if (!empty($schedules)) {
                $i = 0;
                foreach ($schedules as $subcat){
                    foreach ($subcat as $booking)
                    {
                        $newDate = date("d/m/Y", strtotime($booking->scheduledates));
                        $ndate = date("Y/m/d", strtotime($booking->scheduledates));
                        $day = date('l', strtotime($ndate));
                        if ($booking->booking_type == "OD") {
                            $booktype = 'One-off';
                            $service_date = $newDate;
                        } else {
                            $booktype = 'Regular'; 
                            $service_date = "-";
                        }
                        ?>
                            <tr align="center" id="suspend_booking_id_<?php echo $booking->booking_id ?>"><td align="center" valign="middle" style="width:50px;">
                        <p><?php echo ++$i ?></p>
                        </td>
                        <td valign="middle">
                            <p><?php echo $booking->customer_name ?></p>
                        </td>
                        <td valign="middle">
                            <p><?php echo html_escape($service_date) ?></p>
                        </td>
                        <td valign="middle">
                            <p><?php echo $day ?></p>
                        </td>
                        <td valign="middle">
                            <p><?php echo $booking->booking_id ?><p>
                        </td>
                        <td valign="middle">
                            <p><?php echo $booking->maid_name ?><p>
                        </td>
                        <td valign="middle">
                            <p><?php echo $booktype ?><p>
                        </td>
                        <td valign="middle"><p><span class="slot2"><?php echo  date("h:i A", strtotime($booking->time_from)) . ' - ' . date("h:i A", strtotime($booking->time_to)) ?></span></p></td>
                        <td valign="middle">
                            <p><?php echo $booking->working_minutes / 60 ?><p>
                        </td>
                        <?php if ($booking->booking_type == "OD") {?>
                            <td align="center" valign="middle">
                                <p>
                                <!-- <a href="javascript:void(0);" class="slot-sml"  id="suspend_one_day_booking_id_<?php echo $booking->booking_id ?>" onclick="schedule_suspend_one_day('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Suspend One Day</strong></a><br> -->
                                <a href="javascript:void(0);" class="slot-sml"  id="suspend_date_booking_id_<?php echo $booking->booking_id ?>" onclick="schedule_suspend_date_range('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Suspend Date</strong></a><br>
                                <a href="javascript:void(0);" class="slot-sml"  id="cancel_one_day_booking_id_<?php echo $booking->booking_id ?>" onclick="schedule_cancel_one_day('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Cancel One Day</strong></a><br>
                                <a href="javascript:void(0);" class="slot-sml" id="cancel_permanently_booking_id_<?php echo $booking->booking_id  ?>" onclick="cancel_permanently('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Cancel Permanently</strong></a><br>
                                <!-- <a href="javascript:void(0);" class="slot-sml" id="delete_booking_id_<?php echo $booking->booking_id ?>" onclick="delete_schedule('<?php echo $booking->booking_type ?>', '<?php echo $booking->booking_id ?>', '<?php echo $booking->scheduledates ?>', '<?php echo$booking->day_service_status ?>', '<?php echo $booking->day_service_id ?>')"><strong>Cancel</strong></a> -->
                                <a href="javascript:void(0);" class="slot-sml" id="reactivate_booking_id_<?php echo $booking->booking_id ?>" style="display:none;"><strong>Reactivate</strong></a></p>
                            </td>
                            
                        <?php } if ($booking->booking_type == "WE") {?>
                            <td align="center" valign="middle">
                            <p>  
                                <!-- <a href="javascript:void(0);" class="slot-sml"  id="suspend_one_day_booking_id_<?php echo $booking->booking_id ?>" onclick="schedule_suspend_one_day('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Suspend One Day</strong></a><br> -->
                                <a href="javascript:void(0);" class="slot-sml"  id="suspend_date_booking_id_<?php echo $booking->booking_id ?>" onclick="schedule_suspend_date_range('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Suspend Date</strong></a><br>
                                <a href="javascript:void(0);" class="slot-sml"  id="cancel_one_day_booking_id_<?php echo $booking->booking_id ?>" onclick="schedule_cancel_one_day('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Cancel One Day</strong></a><br>
                                <a href="javascript:void(0);" class="slot-sml" id="cancel_permanently_booking_id_<?php echo $booking->booking_id  ?>" onclick="cancel_permanently('<?php echo $booking->booking_type?>', '<?php echo $booking->booking_id?>', '<?php echo $booking->day_service_id ?>','<?php echo $booking->ds_service_date ?>','<?php echo $booking->service_date ?>','<?php echo $booking->day_service_status ?>')"><strong>Cancel Permanently</strong></a><br>
                                <!-- <a href="javascript:void(0);" class="slot-sml" id="delete_booking_id_<?php echo $booking->booking_id ?>" onclick="delete_schedule('<?php echo $booking->booking_type ?>', '<?php echo $booking->booking_id ?>', '<?php echo $booking->scheduledates ?>', '<?php echo$booking->day_service_status ?>', '<?php echo $booking->day_service_id ?>')"><strong>Suspend/Cancel</strong></a> -->
                                <a href="javascript:void(0);" class="slot-sml" id="reactivate_booking_id_<?php echo $booking->booking_id ?>" style="display:none;"><strong>Reactivate</strong></a>
                            </p>
                            </td>
                            <?php } ?>
                    </tr>
                        <?php
                        
                    }
                }
            }                                                                                                                                                                                                                                                                             
            else { ?>
                <tr><td style="line-height: 18px;" colspan="7"><center>No schedules found on</center></td></tr>
            <?php }
            ?>
        </tbody>
    </table>
</div>