<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $settings->site_name; ?> - Statement of <?= $customername; ?></title>
</head>
<style>
@page {
  margin: 0px 0px 0px 0px !important;
  padding: 0px 0px 0px 0px !important;
}
/*.header { position: fixed; top: 0px; left: 0px; right: 0px; }
.footer { position: fixed; bottom: 0px; left: 0px; right: 0px;}
.main-content { page-break-after: always; }*/
</style>
<body style="padding: 0px; margin: 0px;">


<div class="main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
     
          <header style="height: 150px; overflow: hidden; position: fixed; left:0; top:0; z-index: 999;">
          
           
                 <div class="header" style="width:100%; height:auto; padding: 0px 0px 10px 20px; margin: 0px; background: #eee;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15%" rowspan="2" align="right">
                    <img src="<?= base_url('images/invoice-logo.png'); ?>" width="150" height="62" />
                  </td>
                  <td style=" padding: 5px 0px 0px 0px;">
                    <!--<img src="invoice-logo-name.png" width="130" height="14" />-->
                </td>
                  <td width="35%" rowspan="2" valign="bottom">
                  
                  
                  
                         <table width="90%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px; padding-right: 30px;">
                                      <tr>
                                        <td width="50%" style="padding: 0px 0px 0px 0px">Phone</td>
                                        <td width="5%">:</td>
                                        <td width="45%" valign="middle" style="padding: 0px 25px 0px 0px"><strong><?= $settings->company_tel_number; ?></strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">Email </td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px"><?= $settings->company_email; ?></td>
                                      </tr>
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px">TRN</td>
                                        <td>:</td>
                                        <td valign="middle" style="padding: 0px 25px 0px 0px"><?= $settings->company_trn; ?></td>
                                      </tr>
                                      
                             </table>
                  
                  
                  
                  </td>
                </tr>
                <tr>
                  <td width="50%"><p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: -5px 25px 0px 10px; margin: 0px;">
                            <b><?= $settings->company_name; ?></b><br />
                            <?= $settings->company_address_lines_html; ?></p>
                  </td>
                </tr>
              </table>

     </div>
          
          
          </header>
          
          
          
          
          
          <section style=" height: 1000px; padding-top: 100px;">
          
                 <div class="main-content" style="width: 90%; height:auto; padding: 0px 0px 0px 0px; margin: 0px auto;">
     
          <div class="tax-head" style="width:100%; height:auto; padding: 50px 0px 20px 0px; margin: 0px;">
                 <p style="font-family: Roboto, sans-serif; font-size: 30px; color: #555; line-height: 30px; padding: 0px; margin: 0px; text-align: center;">Customer Statement</p>
          </div>
             
       <div class="to-job" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px 0px 20px 0px;">
                  
                  <div class="to-address" style="width:100%; height:auto; background: #e5f6fc; padding: 20px 0px 25px; margin: 0px;">
                  
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td valign="middle">
                                <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px; margin: 0px;"><strong>To</strong></p>
                            
                                <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 0px 25px 0px 45px; margin: 0px;">
                            <strong> <?php echo $customername; ?></strong><br />
                                          <?php echo $address; ?>
                       </p>
                       
                       
                        <!--<p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 10px 25px 0px 45px; margin: 0px; font-weight: bold;">Customer TRN &nbsp;&nbsp;&nbsp; :&nbsp; 0087267213283283</p>-->
                        
                            </td>
                            
                            <td align="right" valign="middle"> 
                              
                                
                                
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; margin: 0px;">
                                      <tr>
                                        <td style="padding: 0px 0px 0px 0px"><strong>Statement</strong></td>
                                        <td>:</td>
                                        <td align="right" valign="middle" style="padding: 0px 25px 0px 0px"><strong><?php echo date('M d, Y') ?> </strong></td>
                                      </tr>
                                      
                                      
                                    </table>

                                
                                
                                
                                
                            </td>
                          </tr>
                        </table>

                  
                  
                       
                       
                       
         </div>
                  
                  
                   
       </div>
             
          <div class="table-main" style="width:100%; height:auto; padding: 0px 0px 0px 0px; margin: 0px;">
          
          
            <table width="100%" border="0" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 14px; padding: 0px; margin: 0px;">
                    <tr class="table-head" bgcolor="#fe6565" style=" font-size: 12px; line-height: 20px; font-weight: bold; color: #FFF;">
                          <td width="8%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF; font-family:Arial, Helvetica, sans-serif; font-weight: bold;">Date</td>
                          <td width="20%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Doc No.</td>
                          <td width="20%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Description</td>
                          <td width="20%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Invoice Remarks</td>
                          <td width="20%" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Debit</td>
                          <td width="7%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Credit</td>
                          <td width="10%" align="center" valign="middle" style="padding: 10px 10px; border-right:0.5px solid #FFF;">Balance</td>
                    </tr>


                    <?php
                    if($initial_bal_sign == 'Dr')
                    {
                      $credit = "0.00";
                      // $debit = $initial_balance;
                      $debit = "0.00";
                      $balance = number_format(($credit + $debit),2);
                      $newbal = ($credit + $debit);
                      //$newamount = ($credit + $debit);
                    } else {
                      // $credit = $initial_balance;
                      $credit = "0.00";
                      $debit = "0.00";
                      $balance = number_format(($credit + $debit),2);
                      // $newbal = '-'.$balance;
                      $newbal = 0;
                      //$newamount = 0;
                    }
                    ?>


                    <?php
                    //$i = 2;
                    $i = 1;
                    // print_r($invoice_statement);die();
                    foreach ($invoice_statement->transactions as $statval)
                    {
                      ?>
                      
                      
                      
                      <tr bgcolor="<?php echo ($i%2 ? "#FFF" : "#e5f6fc"); ?>">
                        <td align="center" style="padding: 10px 10px; border-left:0.5px solid #CCC;  border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC;"><?php echo date('d/m/Y',strtotime($statval->date)); ?></td>
                        <td style="padding: 10px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; font-size: 11px;"><?php echo $statval->doc_no ?></td> 
                        <td  align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC;  font-size: 11px; line-height: 14px;"><?php echo $statval->description ?></td>
                        <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px; line-height: 14px;"><?php echo $statval->remarks ?></td>
                        <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $statval->debit ?></td>
                        <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $statval->credit ?></td>
                        <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php echo $statval->balance ?></td>
                              <!--<td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php// echo $jobs->line_vat_amount; ?></td>
                              <td align="center" style="padding: 10px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 12px;"><?php// echo $jobs->line_net_amount; ?></td>-->
                      </tr>
                      <?php
                       $calcdebitnew += ($statval->debit);
                       $calccreditnew += ($statval->credit);

                    $i++; } ?> 
                           
          <tr bgcolor="#FFF" style="">
              <!-- <td align="center" style="padding: 20px 10px; border-right:0.5px solid #CCC; font-size: 16px; font-weight: bold;">&nbsp;</td> -->
              <td colspan="4" style="padding: 20px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; text-align:left;font-size: 14px; font-weight: bold;">TOTAL</td> 
              <td align="center" style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC;  font-size: 14px; font-weight: bold; line-height: 14px;"><?php echo number_format(($calcdebitnew + $debit),2); ?> <span style=" font-size: 11px;">AED</span></td>
              <td align="center" style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 14px; font-weight: bold;"><?php if($calccreditnew != ""){ echo number_format($calccreditnew,2); } else { echo '0.00'; } ?> <span style=" font-size: 11px;">AED</span></td>
              <td align="center" style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 14px; font-weight: bold;"><?php echo number_format($invoice_statement->closing_balance,2); ?> <span style=" font-size: 11px;">AED</span></td>
          </tr>
          <tr bgcolor="#FFF" style="">
              <!-- <td align="center" style="padding: 20px 10px; border-right:0.5px solid #CCC; font-size: 16px; font-weight: bold;">&nbsp;</td> -->
              <td colspan="6" style="padding: 20px 10px; border-bottom:0.5px solid #CCC;  border-right:0.5px solid #CCC; text-align:left;font-size: 14px; font-weight: bold;">Closing Balance</td> 
              <td align="center" style="padding: 20px 10px; border-bottom:0.5px solid #CCC; border-right:0.5px solid #CCC; font-size: 14px; font-weight: bold;"><?php echo number_format($invoice_statement->closing_balance,2); ?> <span style=" font-size: 11px;">AED</span></td>
          </tr>    
                    
       </table>
       
      

       </div>
       
           
          
  
  
  </div>
          
          </section>
          
          
          
          
          
          
          <footer style="height: 98px; overflow: hidden; position: fixed; left:0; bottom:0; z-index: 999;">
          
                  <div class="bot-text" style="width:90%; height:auto; padding: 0px 0px 50px 0px; margin: 0px auto;">
               
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 5px 0px 0px 0px; margin: 0px;">Accountant Signature</p>
                    </td>
                    <td align="right" valign="top">
                        <p style="font-family: Roboto, sans-serif; font-size:11px; color: #555; line-height: 16px; padding: 5px 0px 0px 0px; margin: 0px;">
                            Customer Signature
                        </p>
                    </td>
                  </tr>
                </table>

          </div>
          
          
          
          
                  <div class="footer" style="width:100%; height:auto; padding: 5px 0px; background: #ffe0e0; margin: 0px; text-align:center;">
               <a href="<?= $settings->company_website_url; ?>" target="_blank" style="font-family: Roboto, sans-serif; font-size:12px; color: #555; line-height: 16px; text-decoration: none; padding: 0px 0px 0px 0px; margin: 0px;"><?= $settings->company_website_label; ?></a>
          </div>
          
          
          </footer>
  
</div>

</body>
</html>
<?php
function numberTowords($num)
{
  $ones = array(
    0 =>"Zero",
    1 => "One",
    2 => "Two",
    3 => "Three",
    4 => "Four",
    5 => "Five",
    6 => "Six",
    7 => "Seven",
    8 => "Eight",
    9 => "Nine",
    10 => "Ten",
    11 => "Eleven",
    12 => "Twelve",
    13 => "Thirteen",
    14 => "Fourteen",
    15 => "Fifteen",
    16 => "Sixteen",
    17 => "Seventeen",
    18 => "Eighteen",
    19 => "Nineteen",
    "014" => "Fourteen"
  );
  $tens = array( 
    0 => "Zero",
    1 => "Ten",
    2 => "Twenty",
    3 => "Thirty", 
    4 => "Forty", 
    5 => "Fifty", 
    6 => "Sixty", 
    7 => "Seventy", 
    8 => "Eighty", 
    9 => "Ninety" 
  ); 
  $hundreds = array( 
    "Hundred", 
    "Thousand", 
    "Million", 
    "Billion", 
    "Trillion", 
    "Quardrillion" 
  ); /*limit t quadrillion */
  $num = number_format($num,2,".",","); 
  $num_arr = explode(".",$num); 
  $wholenum = $num_arr[0]; 
  $decnum = $num_arr[1]; 
  $whole_arr = array_reverse(explode(",",$wholenum)); 
  krsort($whole_arr,1); 
  $rettxt = ""; 
  foreach($whole_arr as $key => $i)
  {
    while(substr($i,0,1)=="0")
      $i=substr($i,1,5);
    if($i < 20){ 
    /* echo "getting:".$i; */
    $rettxt .= $ones[$i]; 
    }elseif($i < 100){ 
    if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
    if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
    }else{ 
    if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
    if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
    if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
    } 
    if($key > 0){ 
    $rettxt .= " ".$hundreds[$key]." "; 
    }
  } 
if($decnum > 0){
$rettxt .= " and ";
if($decnum < 20){
$rettxt .= $ones[$decnum];
$rettxt .= " Phills";
}elseif($decnum < 100){
$rettxt .= $tens[substr($decnum,0,1)];
if(substr($decnum,1,1) > 0)
{
$rettxt .= " ".$ones[substr($decnum,1,1)];
}
$rettxt .= " Phills";
}
}
return $rettxt;
}
?>