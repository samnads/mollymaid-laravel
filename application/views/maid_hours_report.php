<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                   <ul>
                   <li>
                    <i class="icon-th-list"></i>
                    <h3>Maid Hours Report</h3> 
                    
                    </li>
                    
                    <li>                  
                   
                    <input type="text" readonly="readonly" style="width: 160px;" id="vehicle_date" name="from_date" value="<?php echo $activity_date ?>">  
                    
                    </li>
                    
                    <li class="mr-2"> 
                                         
                    <input type="text" readonly="readonly" style=" width: 160px;" id="schedule_date" name="to_date" value="<?php echo $activity_date_to ?>">   
                    </li>
                    
                    <li>                     
                    <input type="submit" class="btn" value="Go" name="vehicle_report">
                    </li>
                    
                    <li class="mr-0 float-right">
                    
                
                
                    
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div> 
                  
                  
                    </li>
                    </ul>              
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Hours</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($maid_hours_report))
                        {
                            $i = 0;
                            $total_hrs=0;
                            foreach ($maid_hours_report as $report)
                            {
                                $maid_time=explode(":",$report->hours);
                                $maid_time=$maid_time[0]+($maid_time[1]/60);
                                $total_hrs+=$maid_time;
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_time . '</td>'
                                    .'</tr>';
                            }
                            //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Hours</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($maid_hours_report))
                        {
                            $i = 0;
                            $total_hrs=0;
                            foreach ($maid_hours_report as $report)
                            {
                                $maid_time=explode(":",$report->hours);
                                $maid_time=$maid_time[0]+($maid_time[1]/60);
                                $total_hrs+=$maid_time;
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $report->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $maid_time . '</td>'
                                    .'</tr>';
                            }
                            //echo '<tr>'. '<td style="line-height: 18px;"></td>'. '<td style="line-height: 18px;font-weight:bold;">Total</td>'. '<td style="line-height: 18px;">' . $total_hrs . '</td>'.'</tr>';
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>            
            </table>
    </div>
</div>

<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "MaidHoursReport.xls"); // Choose the file name
      return false;
  }  
</script>
