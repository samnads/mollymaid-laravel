<div class="row m-0"> 
  <!-- /span6 -->
  <div class="col-sm-12" id="add_user" >
    <div class="widget ">
    
    
    
      
      
        
        
        <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Add User</h3></li>
              




      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url(); ?>users" title="Users"> <i class="fa fa-users"></i></a>
                    </div>
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
</div>




      
      
      <div class="widget-content">
            <form id="useraadd" action="<?php echo base_url(); ?>users/add/<?php if(!empty($e_user)) { echo $e_user->user_id; } else { echo ''; } ?>" class="form-horizontal col-sm-4" method="post">
            
              <fieldset>
              
              
                <div class="col-sm-12 p-0 n-field-box">
                  <p style="color: #F00;"><?php echo isset($error) && strlen(trim($error)) > 0 ? $error : '&nbsp;'; ?></p>
                </div>
                
                
                
                <div class="row m-0 n-field-main">
                    <p class="p-0 m-0">Name</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <input type="text" autocomplete="off" class="" style="width: 300px;" id="usersname" name="name" value="<?php if(!empty($e_user)) { echo $e_user->user_fullname; } else { echo ''; } ?>">
                    </div>
                </div>
                
                
                
                <div class="row m-0 n-field-main">
                    <p>Username</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <input type="text" autocomplete="off" class="" style="width: 300px;" id="user_name" name="username" value="<?php if(!empty($e_user)) { echo $e_user->username; } else { echo ''; } ?>">
                    </div>
                </div>



                <?php if(empty($e_user)) { ?>
                <div class="row m-0 n-field-main">
                    <p>Password</p>
                    <div class="col-sm-12 p-0 n-field-box">
                         <input type="password" class="" style="width: 300px;" id="password" name="password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                    </div>
                </div>






                <?php } ?>
                
                
                
                
                <div class="row m-0 n-field-main">
                    
                    <div class="col-12 p-0"><p>Is Admin?</p></div>
                    <div class="col-sm-6 n-radi-check-main p-0">
                         <input type="radio" value="Y" id="is_admin" name="is_admin" class="" <?php if(!empty($e_user)) { if($e_user->is_admin == 'Y') { echo "checked"; } else { echo ""; } } ?> />
                         <label for="is_admin"> <span class="border-radius-3"></span> <p>Yes</p></label>
                    </div>
                    <div class="col-sm-6 n-radi-check-main p-0">
                         <input type="radio" value="N" id="is_admin1" name="is_admin" class="" <?php if(!empty($e_user)) { if($e_user->is_admin == 'N') { echo "checked"; } else { echo ""; } } else { echo "checked"; } ?> />
                         <label for="is_admin1"> <span class="border-radius-3"></span> <p>No</p></label>
                    </div>
                </div>
				
				<div class="row m-0 n-field-main">
                    
                    <div class="col-12 p-0"><p>Delete Permission?</p></div>
                    <div class="col-sm-6 n-radi-check-main p-0">
                         <input type="radio" value="Y" id="delete_permission" name="delete_permission" class="" <?php if(!empty($e_user)) { if($e_user->delete_permission == 'Y') { echo "checked"; } else { echo ""; } } ?> />
                         <label for="delete_permission"> <span class="border-radius-3"></span> <p>Yes</p></label>
                    </div>
                    <div class="col-sm-6 n-radi-check-main p-0">
                         <input type="radio" value="N" id="delete_permission1" name="delete_permission" class="" <?php if(!empty($e_user)) { if($e_user->delete_permission == 'N') { echo "checked"; } else { echo ""; } } else { echo "checked"; } ?> />
                         <label for="delete_permission1"> <span class="border-radius-3"></span> <p>No</p></label>
                    </div>
                </div>

                  <div class="row m-0 n-field-main">
                    <p>User Role</p>
                    <div class="col-sm-12 p-0 n-field-box">
                      <select name="user_role" class="sel2" style="width:100%">
                      <option value="">--select type--</option>
                        <option value="hr" <?php echo (isset($e_user) && !empty($e_user) && $e_user->user_role === 'hr') ? 'selected' : ''; ?>>HR</option>
                      </select>
                    </div>
                  </div>


                
                
                
                <div class="row m-0 n-field-main pt-3">
                    <div class="col-sm-12 p-0">
                         <input type="submit" class="n-btn" value="Submit" name="user_sub">
                    </div>
                </div>


              </fieldset>
            </form>
      </div>

    </div>
    <!-- /widget --> 
  </div>
  <!-- /span6 -->
  
  <div class="span6" id="edit_zone" style="display: none;">
    <div class="widget ">
      <div class="widget-header"> <i class="icon-globe"></i>
        <h3>Edit Zone</h3>
        <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_zone();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a> </div>
      <!-- /widget-header -->
      
      <div class="widget-content">
        <div class="tabbable">
          <div class="tab-content">
            <form id="zone" class="form-horizontal" method="post">
              <fieldset>
                <div class="control-group">
                  <label class="control-label" for="zonename">Zone Name</label>
                  <div class="controls">
                    <input type="text" class="span6" style="width: 300px;" id="edit_zonename" name="edit_zonename" required="required">
                    <input type="hidden" class="span6" style="width: 300px;" id="edit_zoneid" name="edit_zoneid">
                  </div>
                  <!-- /controls --> 
                </div>
                <!-- /control-group -->
                
                <div class="control-group">
                  <label class="control-label" for="drivername">Driver Name</label>
                  <div class="controls">
                    <input type="text" class="span6" style="width: 300px;" id="edit_drivername" name="edit_drivername" required="required">
                  </div>
                  <!-- /controls --> 
                </div>
                <!-- /control-group -->
                
                <div class="control-group">
                  <label class="control-label">Is Spare Zone</label>
                  <div class="controls">
                    <label class="radio inline">
                      <input type="radio"  name="edit_spare" value="Y">
                      Yes</label>
                    <label class="radio inline">
                      <input type="radio" name="edit_spare" value="N">
                      No</label>
                  </div>
                  <!-- /controls --> 
                </div>
                <!-- /control-group --> 
                
                <br />
                <div class="form-actions">
                  <input type="submit" class="btn btn-primary pull-right" value="Submit" name="zone_edit">
                  <!--					<button class="btn">Cancel</button>--> 
                </div>
                <!-- /form-actions -->
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <!-- /widget-content --> 
    </div>
    <!-- /widget --> 
  </div>
  <!-- /span6 --> 
  
</div>
