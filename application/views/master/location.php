<style>
.list-group-item{
   cursor: pointer;
}
.list-group-item:hover{
   cursor: pointer;
   background-color: #7dc35a36;
}
.list-group-item.active{
   border-color: #dddddd;
}
.list-group-item.active:hover{
   border-color: #ddd;
}
#locations .head{
   cursor: initial;
    background-color: #2c4c59;
    color: #fff;
    font-size: 15px;
    border-bottom: 0 !important;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
}
.list-group.items li:first-child{
   border-top: 0;
   border-top-left-radius: 0;
   border-top-right-radius: 0;
}
.list-group.items .active{
   background-color: #7AC255;
}
#locations .head:after,
#locations .head:before {
  left: 100%;
  top: 50%;
  border: solid transparent;
  content: " ";
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
}
#locations .col-sm-3:not(:last-child) .head:after {
  border-color: rgba(136, 183, 213, 0);
  border-left-color: #2c4c59;
  border-width: 10px;
  margin-top: -10px;
}
</style>
<div id="new-zone-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="title">New Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="col-12 p-0">
      <form class="form-horizontal" method="post" id="new_zone_form">
        <input type="hidden" name="zone_id" value=""/>
        <div class="modal-body">
          <div class="row m-0 n-field-main">
          <div class="col-12">
      </div>
            <p>Zone Name<rf></p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="name" autocomplete="off" >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
            <textarea  name="description" autocomplete="off" placeholder="Enter some description here..."></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="new-area-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span class="title">New Area</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="col-12 p-0">
         <form class="form-horizontal" method="post" id="new_area_form">
            <input type="hidden" name="area_id" value=""/>
            <div class="modal-body">
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Zone
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="zone_id" autocomplete="off" class="sel2" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Area Name
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <input type="text" name="name" autocomplete="off" >
                  </div>
               </div>

               <!-- <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Minimum Hour
                     
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <input class="no-arrows" type="number" name="min_booking_hour" autocomplete="off" >
                  </div>
               </div> -->

               <div class="row m-0 n-field-main">
                  <p>Description</p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <textarea  name="description" autocomplete="off" placeholder="Enter some description here..."></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
               <button type="submit" class="n-btn mb-0" value="Submit">Save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div id="new-location-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span class="title">New Location</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="col-12 p-0">
         <form class="form-horizontal" method="post" id="new_location_form">
            <input type="hidden" name="location_id" value=""/>
            <div class="modal-body">
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Zone
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="zone_id" data-placeholder="-- Select Zone --" autocomplete="off" class="sel2" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Area
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="area_id" data-placeholder="-- Select Area --" autocomplete="off" class="sel2" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Location Name
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <input type="text" name="name" autocomplete="off" >
                  </div>
               </div>

                <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Minimum Hour
                     
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <input class="no-arrows" type="number" name="min_booking_hour" autocomplete="off" >
                  </div>
               </div>

               <div class="row m-0 n-field-main">
                  <p>Description</p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <textarea  name="description" autocomplete="off" placeholder="Enter some description here..."></textarea>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-6">
                     <p>
                     Latitude
                     </p>
                     <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="latitude" autocomplete="off" >
                     </div>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-6">
                     <p>
                     Longitude
                     </p>
                     <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" name="longitude" autocomplete="off" >
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
               <button type="submit" class="n-btn mb-0" value="Submit">Save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div id="new-landmark-popup" style="display:none;">
   <div class="popup-main-box">
      <div class="col-md-12 col-sm-12 green-popup-head">
         <span class="title">New Landmark</span>
         <span id="b-time-slot"></span>
         <span class="pop_close n-close-btn">&nbsp;</span>
      </div>
      <div class="col-12 p-0">
         <form class="form-horizontal" method="post" id="new_landmark_form">
            <input type="hidden" name="landmark_id" value=""/>
            <div class="modal-body">
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Zone
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="zone_id" data-placeholder="-- Select Zone --" autocomplete="off" class="sel2" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Area
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="area_id" data-placeholder="-- Select Area --" autocomplete="off" class="sel2" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Location
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <select name="location_id" data-placeholder="-- Select Location --" autocomplete="off" class="sel2" style="width:100%">
                     </select>
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <div class="col-12">
                  </div>
                  <p>
                     Landmark
                     <rf>
                  </p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <input type="text" name="name" autocomplete="off" >
                  </div>
               </div>
               <div class="row m-0 n-field-main">
                  <p>Description</p>
                  <div class="col-sm-12 p-0 n-field-box">
                     <textarea  name="description" autocomplete="off" placeholder="Enter some description here..."></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="reset" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
               <button type="submit" class="n-btn mb-0" value="Submit">Save</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="row m-0">
   <div class="col-md-12">
      <div class="widget widget-table action-table">
         <div class="widget-header">
            <ul>
               <li>
                  <i class="icon-th-list"></i>
                  <h3>Locations</h3>
               </li>
               <li>
                  <select style="width: 160px;visibility:hidden" name="access_type_filter" id="access_type_filter" class="span3">
                     <option value="" <?php echo $accesstype == 0 ? 'selected="selected"' : ''; ?>>Active</option>
                  </select>
               </li>
               <!--<li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                     <a href="#" title="Assign Access" onclick="assignAccessPopUp()"> <i class="fa fa-plus"></i></a>
                  </div>
               </li>-->
            </ul>
         </div>
         <div class="widget-content" id="locations">
            <div class="row m-0 mt-3 n-field-main">
               <div class="col-sm-3 animate__animated animate__fadeIn">
                  <ul class="list-group mb-0">
                     <li class="list-group-item head">
                        <div class="row ml-1">
                           <div class="col-md-6 mt-1 p-0">Zones</div>
                           <div class="col-md-6 text-right"><span class="pull-right"><button type="button" class="btn btn-dafault" data-action="new-zone-popup"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button></span></div>
                        </div>
                     </li>
                  </ul>
                  <ul class="list-group items" id="zones-holder">
                     <li class="list-group-item active">Loading...</li>
                  </ul>
               </div>
               <div class="col-sm-3 animate__animated animate__fadeIn">
                  <ul class="list-group mb-0">
                     <li class="list-group-item head">
                        <div class="row ml-1">
                           <div class="col-md-6 mt-1 p-0">Areas</div>
                           <div class="col-md-6 text-right"><span class="pull-right"><button type="button" class="btn btn-dafault" data-action="new-area-popup"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button></span></div>
                        </div>
                     </li>
                  </ul>
                  <ul class="list-group items" id="areas-holder">
                     <li class="list-group-item active">Loading...</li>
                  </ul>
               </div>
               <div class="col-sm-3 animate__animated animate__fadeIn">
                  <ul class="list-group mb-0">
                     <li class="list-group-item head">
                        <div class="row ml-1">
                           <div class="col-md-6 mt-1 p-0">Locations</div>
                           <div class="col-md-6 text-right"><span class="pull-right"><button type="button" class="btn btn-dafault" data-action="new-location-popup"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button></span></div>
                        </div>
                     </li>
                  </ul>
                  <ul class="list-group items" id="locations-holder">
                     <li class="list-group-item active">Loading...</li>
                  </ul>
               </div>
               <div class="col-sm-3 animate__animated animate__fadeIn">
                  <ul class="list-group mb-0">
                     <li class="list-group-item head">
                        <div class="row ml-1">
                           <div class="col-md-6 mt-1 p-0">Landmarks</div>
                           <div class="col-md-6 text-right"><span class="pull-right"><button type="button" class="btn btn-dafault" data-action="new-landmark-popup"><span><i class="fa fa-plus" aria-hidden="true"></i></span></button></span></div>
                        </div>
                     </li>
                  </ul>
                  <ul class="list-group items" id="landmarks-holder">
                     <li class="list-group-item active">Loading...</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>