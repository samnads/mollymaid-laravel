<style type="text/css">
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Rate Reports</h3>  
                    </li>
                    <li>                 
                    <?php
                    if ($search['search_date_from'] == "") {
                        ?>
                        From : <input type="text" id="search_date_from" name="search_date_from" value="<?php echo date('d/m/Y', strtotime('-7 days')) ?>" required>

                        <?Php
                    } else {
                        ?>
                        <span style="color: #fff;">From :</span> <input type="text" id="search_date_from" name="search_date_from" value="<?php echo $search['search_date_from'] ?>" required> 
                        <?Php
                    }
                    ?>

</li>
                    <li>
                    
                    <?php
                    if ($search['search_date_to'] == "") {
                        ?>
                        <span style="color: #fff;">To :</span> <input type="text" id="search_date_to" name="search_date_to" value="<?php echo date('d/m/Y') ?>" required>

                        <?Php
                    } else {
                        ?>
                        <span style="color: #fff;">To :</span>  <input type="text" id="search_date_to" name="search_date_to" value="<?php echo $search['search_date_to'] ?>" required>
                        <?Php
                    }
                    ?>
                   </li>
                    <li class="mr-2">

                    <span style="margin-left:15px;"></span>
                    <input type="hidden" name="day" id="day_from" value="<?php echo $search['search_day_from'] ?>">
                    <input type="hidden" name="day" id="day_to" value="<?php echo $search['search_day_to'] ?>">
                    </li>
                    <li>
                    <input type="submit" class="n-btn" value="Go" name="rate_report">
                    
                    </li>
       
       
                    <li class="mr-0 float-right">
                    
                    
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div> 
                    
                    </li>
                    </ul>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom: 0px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 40px;"> Sl.No</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Service date</th>
                            <th style="line-height: 18px;width:90px"> Rating</th>
                            <th style="line-height: 18px;"> Review</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       
                        if ($rate_report != NULL) {
                            $i = 1;
                            foreach ($rate_report as $veh){
                                
								
								
                           ?>
                                <tr>
                                        <td style="line-height: 18px;"><?php echo $i; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->customer_name; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->maid_name; ?> </td>
										<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($veh->service_date)); ?> </td>
										<td>
                                            <p data-toggle="tooltip" data-placement="right" title="<?= $veh->rating; ?> Stars">
                                            <?php
                                            $stars = (int)$veh->rating;
                                            for($j=1; $j<=5; $j++){
                                                if($j <= $stars){
                                                    echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp;';
                                                }
                                                else{
                                                    echo '<i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;';
                                                }
                                            }
                                            ?>
                                            </p>
                                        </td>
										
										
										<td style="line-height: 18px;">
											<?php if($veh->comments == "") { echo ""; } else { echo $veh->comments; } ?> 
										</td>
                                    </tr>
                            <?php
							$i++;
                            }
                                } ?>



                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
            <thead>
                <tr>
                    <th style="line-height: 18px;"> Sl.No</th>
                    <th style="line-height: 18px;"> Customer</th>
                    <th style="line-height: 18px;"> Maid</th>
                    <th style="line-height: 18px;"> Service date</th>
                    <th style="line-height: 18px;"> Rating</th>
                    <th style="line-height: 18px;"> Review</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
               
                if ($rate_report != NULL) {
                    $i = 1;
                    foreach ($rate_report as $veh){
                        
                        
                        
                   ?>
                        <tr>
                                <td style="line-height: 18px;"><?php echo $i; ?> </td>
                                <td style="line-height: 18px;"><?php echo $veh->customer_name; ?> </td>
                                <td style="line-height: 18px;"><?php echo $veh->maid_name; ?> </td>
                                <td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($veh->service_date)); ?> </td>
                                <td>
                                            <p data-toggle="tooltip" data-placement="right" title="<?= $veh->rating; ?> Stars">
                                            <?php
                                            $stars = (int)$veh->rating;
                                            for($j=1; $j<=5; $j++){
                                                if($j <= $stars){
                                                    echo '<i class="fa fa-star" aria-hidden="true"></i>&nbsp;';
                                                }
                                                else{
                                                    echo '<i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;';
                                                }
                                            }
                                            ?>
                                            </p>
                                        </td>
                                
                                
                                <td style="line-height: 18px;">
                                    <?php if($veh->comments == "") { echo ""; } else { echo $veh->comments; } ?> 
                                </td>
                            </tr>
                    <?php
                    $i++;
                    }
                        } ?>



            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "RatingsAndReviews.xls"); // Choose the file name
      return false;
  }  
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>