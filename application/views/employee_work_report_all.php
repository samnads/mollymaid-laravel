<style>
    #da-ex-datatable-numberpaging th, #da-ex-datatable-numberpaging td{
        line-height: 20px !important;
    }
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>Employee Work Report</h3> 
                    
                    </li>
                    <li>                 


                    <span style="margin-left:23px;">Select Month :</span>
                    <select style="width:160px;"  name="month" id="month">
                        <option >-- Select Month --</option>
                        <option value="01" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '01') ? 'selected="selected"' : '';
                        }
                        ?>>January</option>
                        <option value="02" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '02') ? 'selected="selected"' : '';
                        }
                        ?>>February</option>
                        <option value="03" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '03') ? 'selected="selected"' : '';
                        }
                        ?>>March</option>
                        <option value="04" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '04') ? 'selected="selected"' : '';
                        }
                        ?>>April</option>
                        <option value="05" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '05') ? 'selected="selected"' : '';
                        }
                        ?>>May</option>
                        <option value="06" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '06') ? 'selected="selected"' : '';
                        }
                        ?>>June</option>
                        <option value="07" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '07') ? 'selected="selected"' : '';
                        }
                        ?>>July</option>
                        <option value="08" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '08') ? 'selected="selected"' : '';
                        }
                        ?>>August</option>
                        <option value="09" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '09') ? 'selected="selected"' : '';
                        }
                        ?>>September</option>
                        <option value="10" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '10') ? 'selected="selected"' : '';
                        }
                        ?>>October</option>
                        <option value="11" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '11') ? 'selected="selected"' : '';
                        }
                        ?>>November</option>
                        <option value="12" <?php
                        if (!empty($search_condition)) {
                            echo ($search_condition['month'] == '12') ? 'selected="selected"' : '';
                        }
                        ?>>December</option>

                    </select>
                    
                    </li>
                    <li>
                    <span style="margin-left:23px;">Select Year :</span>
                    <select style="width:160px;" id="year" name="year">
                        <option value="">-- Select Year --</option>
                        <?php
                        for ($i = 2014; $i <= 2025; $i++) {
                            ?>
                            <option value="<?php echo $i; ?>" <?php
                            if (!empty($search_condition)) {
                                echo ($search_condition['year'] == $i) ? 'selected="selected"' : '';
                            }
                            ?>><?php echo $i; ?></option>

                            <?php
                        }
                        ?>
                    </select>
                    </li>
                    <li>
                    <span style="margin-left:15px;"></span>
                    <input type="submit" class="n-btn" value="Go" name="activity_report">
                    
                    </li>
                   
                   <li class="mr-0 float-right">


                    <div class="topiconnew border-0 green-btn">
                    	<a href="#" id="EmpWorkPrint" title="Print"> <i class="fa fa-print"></i></a>
                    </div>


                    <div class="topiconnew border-0 green-btn">
                    	<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>
                    
                    </li>
                    </ul>
                    
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <div style="width: 100%; /*width:1180px;*/">
                    <table id="employee-work-report-table" class="table da-table" cellspacing="0" width="100%" style="line-height:20px !important">
                        <thead>
                            <tr>
                                <th style="width: 30px;" ><center>Sl No.</center></th>
                                <th > Maid</th> 
                                <th > After 6 [Hrs]</th>
                                <th > Holiday [Hrs]</th> 
                                <th > Normal [Hrs]</th>
                                <th > Total [Hrs]</th>
                                <th > Customer Reference</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($reports)) {
                                $i = 0;
                                $extra_work_hrs=0;
                                foreach ($reports as $wrk) {
                                    $work_total_hrs=0;
                                    $extra_work_hrs=0;
                                    $extra_work_hrs_holiday=0;
                                    
                                    $extra_work_hrs=$this->reports_model->get_employee_extra_work($month, $year,$wrk->maid_id);
                                    $extra_work_hrs_holiday=$this->reports_model->get_employee_extra_work_holiday($month, $year,$wrk->maid_id);
                                    $normal_work_hrs=$this->reports_model->get_employee_work_normal($month, $year,$wrk->maid_id);
                                    $work_total_hrs= $normal_work_hrs + $extra_work_hrs_holiday + $extra_work_hrs;
                                    
                                    echo '<tr>'
                                    . '<td><center>' . ++$i . '</center></td>'
                                    . '<td>' . $wrk->maid_name . '</td>'
                                    . '<td>'.$extra_work_hrs.'</td>' 
                                    . '<td>'.$extra_work_hrs_holiday.'</td>'    
                                    . '<td>' . $normal_work_hrs . '</td>'
                                    . '<td>' . $work_total_hrs . '</td>'        
                                    . '<td>' . ((!empty($maid_references) && array_key_exists($wrk->maid_id, $maid_references)) ? $maid_references[$wrk->maid_id]: 0 ). '</td>'
                                    . '</tr>';
                                }
                            } else {
                                //echo '<tr><td colspan="4">No Results!</td></tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="employeeWorkPrint">
    <table border="1" width="100%" cellspacing="0" cellpadding="10">
        <thead>
            <tr>
                <th style="width: 10%" >Sl No.</th>
                <th > Maid</th> 
                 <th > After 6 [Hrs]</th>
                 <th > Holiday [Hrs]</th> 
                 <th > Normal [Hrs]</th>
                 <th > Total [Hrs]</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if (!empty($reports)) {
            $i = 0;
            foreach ($reports as $wrk) {
                
                $work_total_hrs=0;
                $extra_work_hrs=0;
                $extra_work_hrs_holiday=0;
                                    
                $extra_work_hrs=$this->reports_model->get_employee_extra_work($month, $year,$wrk->maid_id);
                $extra_work_hrs_holiday=$this->reports_model->get_employee_extra_work_holiday($month, $year,$wrk->maid_id);
                $normal_work_hrs=$this->reports_model->get_employee_work_normal($month, $year,$wrk->maid_id);
                $work_total_hrs= $normal_work_hrs + $extra_work_hrs_holiday + $extra_work_hrs;
                echo '<tr>'
                . '<td>' . ++$i . '</td>'
                . '<td>' . $wrk->maid_name . '</td>'
                . '<td>'.$extra_work_hrs.'</td>' 
                . '<td>'.$extra_work_hrs_holiday.'</td>'    
                . '<td>' . $normal_work_hrs . '</td>'
                . '<td>' . $work_total_hrs . '</td>'      
                . '</tr>';
            }
        } else {
            echo '<tr><td colspan="3">No Results!</td></tr>';
        }
        ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    (function (a) {
    a(document).ready(function (b) {
      if (a('#employee-work-report-table').length > 0) {
        a("table#employee-work-report-table").dataTable({
          'sPaginationType': "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true,"orderMulti": false,
          "scrollX": true,
          'columnDefs': [
            {
              'targets': [],
              'orderable': false
            },
          ]
        });
      }
    });
  })(jQuery);
  function exportF(elem) 
  {
      var table = document.getElementById("employeeWorkPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "EmployeeWorkReport.xls"); // Choose the file name
      return false;
  }  
</script>