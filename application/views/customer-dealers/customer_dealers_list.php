<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Customer Dealer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal new-customer-form" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Dealer Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="dealer_name" autocomplete="off" >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>TRN</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="trn" autocomplete="off" >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Commision</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="commision" autocomplete="off" >
            </div>
          </div>

           <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="description" autocomplete="off" >
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="dealer_sub">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Customer Dealer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal edit-customer-form" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Customer Dealer Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_dealername" id="edit_dealername" autocomplete="off" >
              <input type="hidden" name="edit_dealerid" id="edit_dealerid">
            </div>
          </div>

          <div class="row m-0 n-field-main">
            <p>TRN</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_trn" id="edit_trn" autocomplete="off" >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Commision</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_commision" id="edit_commision" autocomplete="off" >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_description" id="edit_description" autocomplete="off" >
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="dealer_edit">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Customer Dealer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this customer dealer?</h3>
      <input type="hidden" id="delete_zoneid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="view-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">View Customer Dealer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Customer Dealer Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="view_dealername" id="view_dealername" readonly >
              <input type="hidden" name="view_dealerid" id="view_dealerid">
            </div>
          </div>

          <div class="row m-0 n-field-main">
            <p>TRN</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="view_trn" id="view_trn" readonly  >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Commision</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="view_commision" id="view_commision" readonly  >
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="view_description" id="view_description" readonly >
            </div>
          </div>
        </div>
        <div class="modal-footer">
          
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-success-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Deleted</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
    <h3>Customer Dealer Deleted Successfully!</h3>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Customer Dealers List</h3>

              <div class="topiconnew border-0 green-btn">
              	   <a onclick="newModal()" title="Add"> <i class="fa fa-plus"></i></a>
              </div>

            </div>
            <!-- /widget-header -->
             <div class="widget-content">

                <table id="zone-list-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px">Customer Dealer Name </th>
                            <th style="line-height: 18px"> TRN</th>
                            <th style="line-height: 18px"> Commision</th>
                            <th style="line-height: 18px">Description</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
if (count($customer_dealers) > 0) {
    $i = 1;
    foreach ($customer_dealers as $val) {
        ?>
                        <tr>
                            <td style="line-height: 18px; width: 20px;  text-align:center;"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $val['dealer_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $val['trn'] ?></td>
                            <td style="line-height: 18px"><?php echo $val['commision'] ?></td>
                            <td style="line-height: 18px"><?php echo $val['description'] ?></td>
                            <td style="line-height: 18px;width: 100px;" class="td-actions">
                            <a href="javascript:;" class="n-btn-icon blue-btn" onclick="view_dealer_get(<?php echo $val['customer_dealer_id'] ?>);"><i class="btn-icon-only fa fa-eye"> </i></a>
								<a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_dealer_get(<?php echo $val['customer_dealer_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if (user_authenticate() == 1) {?>
								<a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $val['customer_dealer_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php }?>
							</td>
                        </tr>
                        <?php
$i++;
    }
}
?>

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div>
    <!-- /span6 -->
    <div class="span6" id="edit_zone" style="display: none;">

    </div> <!-- /span6 -->

</div>
<script>
/*********************************************************************************** */
(function (a) {
    a(document).ready(function (b) {
        if (a('#zone-list-table').length > 0) {
            a("table#zone-list-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,  "scrollY": true,
                "orderMulti": false,
                'bFilter':true,
                "lengthChange": false,
                'columnDefs': [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function edit_dealer_get(customer_dealer_id){
  $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "customer_dealers/edit_dealer",
        data: { customer_dealer_id: customer_dealer_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_dealerid').val(value.customer_dealer_id)
                $('#edit_dealername').val(value.dealer_name)
                $('#edit_trn').val(value.trn)
                $('#edit_commision').val(value.commision)
                $('#edit_description').val(value.description)
            });
              fancybox_show('edit-popup', { width: 450 });
              $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
}
function closeFancy(){
  $.fancybox.close();
}
function confirm_delete_modal(customer_dealer_id){
    $('#delete_zoneid').val(customer_dealer_id);
  fancybox_show('delete-popup', { width: 450 });
}
function confirm_delete(){
     $.ajax({
            type: "POST",
            url: _base_url + "customer_dealers/remove_dealer",
            data: { customer_dealer_id: $('#delete_zoneid').val() },
            dataType: "text",
            cache: false,
            success: function (result) {
                if (result == 1) {
                    alert("Cannot Delete! ");
                }
                else {
                    //alert("Customer Dealer Deleted Successfully");
                    fancybox_show('delete-success-popup', { width: 450 });
                    setTimeout(location.reload.bind(location), 1);
                }
            },
            error:function(data){
                alert(data.statusText);
                console.log(data);
            }
        });
}
function newModal(){

  fancybox_show('new-popup', { width: 450 });
}

function view_dealer_get(customer_dealer_id){
  $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "customer_dealers/edit_dealer",
        data: { customer_dealer_id: customer_dealer_id},
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#view_dealerid').val(value.customer_dealer_id)
                $('#view_dealername').val(value.dealer_name)
                $('#view_trn').val(value.trn)
                $('#view_commision').val(value.commision)
                $('#view_description').val(value.description)
            });
              fancybox_show('view-popup', { width: 450 });
              $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
}

$(document).ready(function() {
  $('.new-customer-form').validate({
    rules: {
      dealer_name: {
        required: true
      },
      trn: {
        required: true
      },
      commision: {
        required: true
      },
      description: {
        required: true
      }
    },
    messages: {
      dealer_name: {
        required: 'Please enter the dealer name'
      },
      trn: {
        required: 'Please enter the TRN'
      },
      commision: {
        required: 'Please enter the commission'
      },
      description: {
        required: 'Please enter the description'
      }
    },
    submitHandler: function(form) {
      // Code to execute when the form is valid and submitted
      form.submit();
    }
  });
});

$(document).ready(function() {
  $('.edit-customer-form').validate({
    rules: {
      edit_dealername: {
        required: true
      },
      edit_trn: {
        required: true
      },
      edit_commision: {
        required: true
      },
      edit_description: {
        required: true
      }
    },
    messages: {
      edit_dealername: {
        required: 'Please enter the customer dealer name'
      },
      edit_trn: {
        required: 'Please enter the TRN'
      },
      edit_commision: {
        required: 'Please enter the commission'
      },
      edit_description: {
        required: 'Please enter the description'
      }
    },
    submitHandler: function(form) {
      // Code to execute when the form is valid and submitted
      form.submit();
    }
  });
});




/*********************************************************************************** */
</script>
