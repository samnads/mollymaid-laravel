<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row m-0">   
    <div class="col-md-12">
    
        <div class="widget widget-table action-table">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/schedule-report' ?>">
                   <ul>
                   <li>
                    <i class="icon-th-list"></i>
                    <h3>Schedule Report</h3> 
                    </li>
                    
                    <li>
					<input type="text" readonly="readonly" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo $payment_date ?>">
                    </li>
                    
                    <li>
					<input type="text" readonly="readonly" style="width: 160px;" id="search_date_to" name="search_date_to" value="<?php echo $payment_date_to ?>">
                    </li>
                    
                    <li class="mr-2">
					<span style="margin-left:23px;">Zone :</span>
                    <select style="width:160px;" id="zones" name="zones">
						<option value="">-- Select Zone --</option>
						<?php
						if (count($zones) > 0) {
							foreach ($zones as $zones_val) {
								?>
							<option value="<?php echo $zones_val['zone_id']; ?>" <?php if($zone_id == $zones_val['zone_id']){ echo 'selected="selected"'; } else { echo ''; } ?> ><?php echo $zones_val['zone_name']; ?></option>
								<?php
							}
						}
						?>
					</select>
                    </li>
                    <li>



					<input type="submit" class="n-btn" value="Go" name="add_payment">
                    </li>
                    
                    <li class="mr-0 float-right">
                    <div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url(); ?>reports/schedule_report_excel_new/<?php echo $servicedate; ?>/<?php echo $servicedateto; ?>/<?php echo $zone_id; ?>" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>
                    
                    </li>
                    
                    
                </form>   
            </div>

            <div class="widget-content">
                <table id="schedule-report" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Name</th>
                            <th style="line-height: 18px">Phone</th>
                            <th style="line-height: 18px">Email</th>
                            <th style="line-height: 18px">Date of Service</th>
                            <th style="line-height: 18px">Booking Type</th>
                            <th style="line-height: 18px">Booking Source</th>
                            <th style="line-height: 18px">Cleaner</th>
                            <th style="line-height: 18px">Hrs</th>
                            <th style="line-height: 18px">Amount</th>
                            <th style="line-height: 18px">Status</th>
			    <th style="line-height: 18px">Booking<br/>Time</th>
                            <th style="line-height: 18px">Service<br/>Time</th>
                            <th style="line-height: 18px">Started By</th>
                            <th style="line-height: 18px">Service Name</th>
                            <th style="line-height: 18px">Customer Added Date</th>
                            <th style="line-height: 18px">Star Rating</th>
                            <th style="line-height: 18px">Comments</th>
                            <th style="line-height: 18px">Customer Area-Zone</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($reports))
                        {
                            $i = 0;
							$totamount = 0;$tothrs = 0;
                            foreach ($reports as $rep)
                            {
                            	foreach ($rep as $report)
                                {
								$totamount += $report->total_amount;
						?>
							<tr>
								<td style="line-height: 18px;"><?php echo ++$i; ?></td>
								<td style="line-height: 18px;"><?php echo $report->customer_name; ?></td>
								<td style="line-height: 18px;"><?php echo $report->mobile_number_1; ?></td>
								<td style="line-height: 18px;"><?php echo $report->email_address; ?></td>
								<td style="line-height: 18px;"><?php echo $report->sch_date; ?></td>
								<td style="line-height: 18px;">
									<?php
									if($report->booking_type == "OD")
									{
										echo "One Day";
									} else if($report->booking_type == "WE"){
										echo "Weekly";
									} else if($report->booking_type == "BW"){
										echo "Bi-Weekly";
									}
									$hours=(strtotime($report->time_to) - strtotime($report->time_from)) / 3600;
									$tothrs += $hours;
									$hrs=strtotime($report->time_to)-strtotime($report->time_from);
									?>
								</td>
								<td style="line-height: 18px;"><?php echo $report->customer_source; ?></td>
								<td style="line-height: 18px;"><?php echo $report->maid_name; ?></td>
								<td style="line-height: 18px;"><?php echo gmdate("H:i", $hrs); ?></td>
								<td style="line-height: 18px;"><?php echo $report->total_amount; ?></td>
								<td style="line-height: 18px;">
								<?php
								if($report->service_status == 1)
								{
									echo "In Progress";
								} else if($report->service_status == 2){
									echo "Finished";
								} else if($report->service_status == 3){
									echo "Cancelled";
								} else {
									echo "Not Started";
								}
								?>
								</td>
								<td style="line-height: 18px;"><?php echo $report->booking_time_from." - ".$report->booking_time_to; ?></td>
								<td style="line-height: 18px;"><?php echo $report->service_time_from." - ".$report->service_time_to; ?></td>
								<td style="line-height: 18px;"><?php echo $report->drivername; ?></td>
								<td style="line-height: 18px;"><?php echo $report->service_type_name; ?></td>
								<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($report->customer_added_datetime)); ?></td>
								<td style="line-height: 18px; text-align: center;">
									<?php
									if($report->rating > 0)
									{
										echo $report->rating;
									}
									?>
								</td>
								<td style="line-height: 18px;"><?php echo $report->comments; ?></td>
                                <td style="line-height: 18px;"><?php echo $report->area_name;?>-<?php echo $report->zone_name; ?></td>
                            </tr>
						<?php
								}
                            }
                        }
                        
                        ?>
                    </tbody>
					<tfoot>
						<tr>
							<td style="line-height: 18px;" colspan="6"></td>
							<td style="line-height: 18px;">Hours Total</td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;">Amount Total</td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;" colspan="8"></td>
						</tr>
					</tfoot>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<script>
	(function (a) {
    a(document).ready(function (b) {
        if (a('#schedule-report').length > 0) {
            a("table#schedule-report").dataTable({
                sPaginationType: "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true,
                footerCallback: function (row, data, start, end, display) {
                    var api = this.api();
                    var intVal = function (i) {
                        return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                    };
                    var hourVal = function (i) {
                        return typeof i === 'string' ? i.replace(':', '.') * 1 : typeof i === 'number' ? i : 0;
                    };
                    totalAmount = api
                        .column(9)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                    pageTotalAmount = api
                        .column(9, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                    totalHours = api
                        .column(8)
                        .data()
                        .reduce(function (a, b) {
                            return hourVal(a) + hourVal(b);
                        }, 0);
                    pageTotalHours = api
                        .column(8, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return hourVal(a) + hourVal(b);
                        }, 0);
                    $(api.column(7).footer()).html('<strong>' + pageTotalHours.toFixed(2) + ' <hr>' + totalHours.toFixed(2) + '</strong>');
                    $(api.column(9).footer()).html('<strong>' + pageTotalAmount.toFixed(2) + ' <hr>' + totalAmount.toFixed(2) + '</strong>');
                },
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });

})(jQuery);
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('schedule'); ?>') {
			$this.addClass('active');
		}
	})
})
</script>