<div id="new-card-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Card</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="new-card-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Card Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="card" id="card" autocomplete="off" placeholder="MM-C-001">
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="n-btn blue-btn mb-0" id="generateBtn">Generate Card</button>
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="card_sub">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="edit-card-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Card</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="edit-card-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Card Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_card" id="edit_card" autocomplete="off">
              <input type="hidden" name="edit_cardid" id="edit_cardid">
            </div>
          </div>


        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="card_edit">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Card</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this card?</h3>
      <input type="hidden" id="delete_cardid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>

<div id="view-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">View Card</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Card Code</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="view_card" id="view_card" readonly>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-success-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Deleted</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Card Deleted Successfully!</h3>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Cards List</h3>

        <div class="topiconnew border-0 green-btn">
          <a onclick="newModal()" title="Add"> <i class="fa fa-plus"></i></a>
        </div>

      </div>
      <!-- /widget-header -->
      <div class="widget-content">

        <table id="zone-list-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
              <th style="line-height: 18px"> card</th>
              <th style="line-height: 18px"> Status</th>
              <th style="line-height: 18px" class="td-actions">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($cards) > 0) {
              $i = 1;
              foreach ($cards as $cards_val) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;"><?php echo $i; ?></td>
                  <td style="line-height: 18px"><?php echo $cards_val['code'] ?></td>

                  <td style="line-height: 18px; width: 75px">
                    <?php
                    if ($cards_val['is_assigned'] == 1 && empty($cards_val['returned_at'])) {
                      $css_class = 'warning'; // Assigned
                      $status = 'Assigned';
                    } else if ($cards_val['is_assigned'] == 1 && !empty($cards_val['returned_at'])) {
                      $css_class = 'success'; // Not Assigned
                      $status = 'Not Assigned';
                    } else {
                      $css_class = 'success';
                      $status = 'Not Assigned';
                    }
                    ?>
                    <span class="btn badge-<?php echo $css_class; ?>"><?php echo $status; ?></span>
                  </td>

                  <td style="line-height: 18px;width:100px;" class="td-actions">
                    <a href="javascript:;" class="n-btn-icon blue-btn" onclick="view_card_get(<?php echo $cards_val['id'] ?>);"><i class="btn-icon-only fa fa-eye"> </i></a>
                    <a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_card_get(<?php echo $cards_val['id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                    <?php if (user_authenticate() == 1) { ?>
                      <a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $cards_val['id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                    <?php } ?>
                  </td>
                </tr>
            <?php
                $i++;
              }
            }
            ?>

          </tbody>
        </table>

      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
  <div class="span6" id="edit_zone" style="display: none;">

  </div> <!-- /span6 -->

</div>
<script>
  /*********************************************************************************** */
  (function(a) {
    a(document).ready(function(b) {
      if (a('#zone-list-table').length > 0) {
        a("table#zone-list-table").dataTable({
          'sPaginationType': "full_numbers",
          "bSort": true,
          "scrollY": true,
          "orderMulti": false,
          'bFilter': true,
          "lengthChange": false,
          'columnDefs': [{
            'targets': [-1],
            'orderable': false
          }, ]
        });
      }
    });
  })(jQuery);

  function edit_card_get(id) {
    $("#edit-card-form").trigger("reset");
    edit_card_validator.resetForm();
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "key/edit_key",
      data: {
        id: id
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        //alert(result);
        var obj = jQuery.parseJSON(result);
        $.each($.parseJSON(result), function(edit, value) {
          $('#edit_cardid').val(value.id)
          $('#edit_card').val(value.code)

        });
        fancybox_show('edit-card-popup');
        $('.error-message').text('');
        $('.mm-loader').hide();
      },
      error: function(data) {
        $('.mm-loader').hide();
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  function closeFancy() {
    $.fancybox.close();
  }

  function confirm_delete_modal(id) {
    $('#delete_cardid').val(id);
    fancybox_show('delete-popup', {
      width: 450
    });

  }

  function confirm_deletee() {
    $.ajax({
      type: "POST",
      url: _base_url + "key/remove_key",
      data: {
        key_id: $('#delete_cardid').val()
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        fancybox_show('delete-success-popup', {
          width: 450
        });
        location.reload();
      },
      error: function(data) {
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  function newModal() {
    $("#new-card-form").trigger("reset");
    new_card_validator.resetForm();
    fancybox_show('new-card-popup');
    $('.error-message').text('');
  }
  /*********************************************************************************** */


  $('#generateBtn').click(function() {
    $.ajax({
      url: '<?php echo base_url("key/get_card_last_code"); ?>',
      type: 'GET',
      dataType: 'json',
      success: function(response) {
        if (response.success) {
          // Increment the code value and fill the input box
          var lastCode = response.code;
          // var numericPart = parseInt(lastCode.split('-')[2]);
          // var newNumericPart = numericPart + 1;
          // var paddedNumericPart = newNumericPart.toString().padStart(3, '0');
          // var newCode = 'MM-C-' + paddedNumericPart;
          //$('#card').val(newCode);
          $('#card').val(lastCode);
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError);
      }
    });
  });


  /*********************************************************************************** */

  function view_card_get(id) {
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "key/edit_key",
      data: {
        id: id
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        //alert(result);
        var obj = jQuery.parseJSON(result);
        $.each($.parseJSON(result), function(edit, value) {
          $('#view_card').val(value.code)

        });
        fancybox_show('view-popup', {
          width: 450
        });
        $('.mm-loader').hide();
      },
      error: function(data) {
        $('.mm-loader').hide();
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  $(document).ready(function() {
    new_card_validator = $('#new-card-form').validate({
      rules: {
        card: {
          required: true
        },

      },
      messages: {
        card: {
          required: 'Please enter the Card code'
        },
      },
      submitHandler: function(form) {
        // Code to execute when the form is valid and submitted
        //form.submit();
        var card = $('#card').val();


        $.ajax({
          url: '<?php echo base_url("key/check_key_code"); ?>',
          type: "POST",
          dataType: "json",
          data: {
            key: card
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Card already exists!');

            } else {
              $.ajax({
                url: '<?php echo base_url("key/save_card"); ?>',
                type: "POST",
                dataType: "json",
                data: {
                  card: card
                },
                success: function(saveResponse) {

                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving card.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking card name.');
          }
        });






      }
    });
  });

  $(document).ready(function() {
    edit_card_validator = $('#edit-card-form').validate({
      rules: {
        edit_card: {
          required: true
        },

      },
      messages: {
        edit_card: {
          required: 'Please enter the Card code'
        },
      },
      submitHandler: function(form) {
        // Code to execute when the form is valid and submitted
        //form.submit();

        var edit_card = $('#edit_card').val();
        var edit_cardid = $('#edit_cardid').val();


        $.ajax({
          url: '<?php echo base_url("key/check_key_code_edit"); ?>',
          type: "POST",
          dataType: "json",
          data: {
            key: edit_card,
            edit_keyid: edit_cardid
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Card already exists!');

            } else {
              $.ajax({
                url: '<?php echo base_url("key/edit_card_new"); ?>',
                type: "POST",
                dataType: "json",
                data: {
                  edit_card: edit_card,
                  edit_cardid: edit_cardid
                },
                success: function(saveResponse) {

                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving card.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking card name.');
          }
        });




      }
    });
  });

  toastr.options = {
    positionClass: "toast-bottom-full-width"
  };

  function confirm_delete() {
    var keyId = $('#delete_cardid').val();
    $.ajax({
      type: "POST",
      url: _base_url + "key/check_returned",
      data: {
        key_id: keyId
      },
      dataType: "json",
      cache: false,
      success: function(response) {
        if (response.status == 'null') {
          //toastr.error("Can't delete this card.The card is already in use");
          toast("error", "Can't delete this card.The card is already in use");
        } else {
          $.ajax({
            type: "POST",
            url: _base_url + "key/remove_key",
            data: {
              key_id: keyId
            },
            dataType: "text",
            cache: false,
            success: function(result) {
              fancybox_show('delete-success-popup', {
                width: 450
              });
              location.reload();
            },
            error: function(data) {
              alert(data.statusText);
              console.log(data);
            }
          });
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
  }
</script>