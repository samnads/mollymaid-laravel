<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal new-zone-form" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Zone Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="zonename" id="zonename" autocomplete="off">
            </div>
          </div>
          <!-- <div class="row m-0 n-field-main">
            <p>Driver Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="drivername" id="drivername" autocomplete="off" >
            </div>
          </div> -->
          <!-- <div class="row m-0">
            <div class="col-12">
              <p>Is Spare Zone</p>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="Y" id="spare-yes" name="spare" class="" >
              <label for="spare-yes"><span class="border-radius-3"></span> <p>Yes</p> </label>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="N" id="spare-no" name="spare" class="" checked>
              <label for="spare-no"><span class="border-radius-3"></span> <p>No</p> </label>
            </div>
          </div> -->
          </br>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea name="description" id="description" autocomplete="off"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="zone_sub">Save</button>

        </div>
      </form>
    </div>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="spanzone" id="b-maid-name">Edit Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal edit-zone-form" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <div class="col-12">
              <div class="error-message" style="color:red;"></div>
            </div>
            <p>Zone Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_zonename" id="edit_zonename" autocomplete="off">
              <input type="hidden" name="edit_zoneid" id="edit_zoneid">
            </div>
          </div>

          <!-- <div class="row m-0 n-field-main">
            <p>Driver Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_drivername" id="edit_drivername" autocomplete="off" >
            </div>
          </div> -->
          <!-- <div class="row m-0">
            <div class="col-12">
              <p>Is Spare Zone</p>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="Y" id="R_NAME_yes" name="edit_spare" class="" >
              <label for="R_NAME_yes">
                <span class="border-radius-3"></span> <p>Yes</p> </label>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="N" id="R_NAME_no" name="edit_spare" class="">
              <label for="R_NAME_no">
                <span class="border-radius-3"></span> <p>No</p> </label>
            </div>
          </div> -->
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea name="edit_description" id="edit_description" autocomplete="off"> </textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer view_footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="zone_edit">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this zone ?</h3>
      <input type="hidden" id="delete_zoneid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>

<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this zone ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this zone ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <ul>
          <li><i class="icon-th-list"></i>
            <h3>Zones List</h3>
          </li>

          <li><select style="width:160px;" id="all_location" onchange="activefilter(this.value)">
              <option value="" <?php echo empty($status) ? 'selected' : ''; ?>>Select</option>
              <option value="0" <?php echo $status == '0' ? 'selected' : ''; ?>>Active</option>
              <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Inactive</option>
            </select></li>



          <div class="topiconnew border-0 green-btn">
            <a onclick="newModal()" title="Add"> <i class="fa fa-plus"></i></a>
          </div>

      </div>
      </ul>
      <!-- /widget-header -->
      <div class="widget-content">

        <table id="zone-list-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
              <th style="line-height: 18px;width: 350px;"> Zone Name </th>
              <th style="line-height: 18px;width: 550px;"> Description</th>
              <th style="line-height: 18px;width: 150px;" class="td-actions">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($zones) > 0) {
              $i = 1;
              foreach ($zones as $zones_val) {
                // print_r($zones_val);die();
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px;  text-align:center;"><?php echo $i; ?></td>
                  <td style="line-height: 18px"><?php echo $zones_val['zone_name'] ?></td>
                  <td style="line-height: 18px"><?php echo $zones_val['description'] ?></td>
                  <td class="td-actions">
                    <a href="javascript:;" class="n-btn-icon green-btn" data-action="view-zone-popup" data-id="<?= $zones_val['zone_id'] ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_zone_get(<?php echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                    <!-- <php if (user_authenticate() == 1) { ?>
                      <a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                    <php } ?> -->
                    <a href="javascript:void(0)" class="n-btn-icon green-btn" style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);" title="" onclick="confirm_disable_enable_modal(<?php echo $zones_val['zone_id'] ?>,<?php echo $zones_val['zone_status'] ?>);">
                      <?php echo $zones_val['zone_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?>
                    </a>

                  </td>
                </tr>
            <?php
                $i++;
              }
            }
            ?>

          </tbody>
        </table>

      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
  <div class="span6" id="edit_zone" style="display: none;">

  </div> <!-- /span6 -->

</div>
<script>
  /*********************************************************************************** */
  var disable_areaid = null;
  var web_status = null;
  // **********************************
  function confirm_disable_enable_modal(area_id, status) {
    if (status == 0) {
      fancybox_show('enable-popup', {
        width: 450
      });
    } else {

      fancybox_show('disable-popup', {
        width: 450
      });
    }
    disable_areaid = area_id;
    web_status = status;
  }
  // *****************************************************************************************

  function confirm_disable() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/zone_status",
      data: {
        zone_id: disable_areaid,
        zone_status: web_status
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        window.location.assign(_base_url + 'zones');
      }
    });
  }
  /*********************************************************************************** */
  function confirm_enable() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/zone_status",
      data: {
        zone_id: disable_areaid,
        zone_status: web_status
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        window.location.assign(_base_url + 'zones');
      }
    });
  }

  // ***************************************************************************************
  // (function(a) {
  //   a(document).ready(function(b) {
  //     if (a('#zone-list-table').length > 0) {
  //       a("table#zone-list-table").dataTable({
  //         'sPaginationType': "full_numbers",
  //         "bSort": true,
  //         "scrollY": true,
  //         "orderMulti": false,
  //         'bFilter': true,
  //         "lengthChange": false,
  //         'columnDefs': [{
  //           'targets': [-1],
  //           'orderable': false
  //         }, ]
  //       });
  //     }
  //   });
  // })(jQuery);

  var dataTable;

  (function($) {
    $(document).ready(function() {
      if ($('#zone-list-table').length > 0) {
        dataTable = $("table#zone-list-table").DataTable({
          'sPaginationType': "full_numbers",
          "bSort": true,
          "scrollY": true,
          "orderMulti": false,
          'bFilter': true,
          "lengthChange": false,
          'columnDefs': [{
            'targets': [-1],
            'orderable': false
          }],
          'language': {
            'zeroRecords': 'No Zone data found'
          }
        });

        activefilter($('#all_location').val());

        $('#all_location').on('change', function() {
          var status = $(this).val();
          activefilter(status);
        });
      }
    });
  })(jQuery);

  function activefilter(value) {
    if (value === "") {
      $("tbody tr").show();
    } else {
      $("tbody tr").hide();

      $("tbody tr").each(function() {
        var status = $(this).find("i.fa-toggle-on").length > 0 ? "0" : "1";
        if (status === value) {
          $(this).show();
        }
      });
    }
  }


  // *************************************************************************************
  // view zones

  $('[data-action="view-zone-popup"]').click(function(event) {
    $('.spanzone').html("View Zones");
    $("#edit-popup :input").prop("disabled", true);
    $('.view_footer').hide();
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "settings/view_zone",
      data: {
        zone_id: $(this).attr("data-id")
      },
      dataType: "json",
      cache: false,
      success: function(result) {
        $('#edit_zonename').val(result.zone_name)
        $('#edit_description').val(result.description)
        fancybox_show('edit-popup', {
          closeClick: false
        });
      },
      error: function(data) {
        toast('error', data.statusText);
      }
    });
  });

  // ****************************************************************************

  function edit_zone_get(zone_id) {
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "settings/edit_zone",
      data: {
        zone_id: zone_id
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        //alert(result);
        var obj = jQuery.parseJSON(result);
        $.each($.parseJSON(result), function(edit, value) {
          $('#edit_zoneid').val(value.zone_id)
          $('#edit_zonename').val(value.zone_name)
          $('#edit_drivername').val(value.driver_name)
          $('input[value="' + value.spare_zone + '"]').prop('checked', true);
          $('#edit_description').val(value.description)
        });

        fancybox_show('edit-popup', {
          width: 450
        });
        $('.error-message').text('');
        $('.mm-loader').hide();
      },
      error: function(data) {
        $('.mm-loader').hide();
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  function closeFancy() {
    $.fancybox.close();
  }

  function confirm_delete_modal(zone_id) {
    $('#delete_zoneid').val(zone_id);
    fancybox_show('delete-popup', {
      width: 450
    });
  }

  function confirm_delete() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/remove_zone",
      data: {
        zone_id: $('#delete_zoneid').val()
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        if (result == 1) {
          alert("Cannot Delete! This Zones has Areas");
        } else {
          alert("Zone Deleted Successfully");
          setTimeout(location.reload.bind(location), 1);
        }
      },
      error: function(data) {
        alert(data.statusText);
        console.log(data);
      }
    });
  }

  function newModal() {
    fancybox_show('new-popup', {
      width: 450
    });
    $('.new-zone-form')[0].reset();
    $('.error-message').text('');
  }
  /*********************************************************************************** */


  $(document).ready(function() {
    $('.edit-zone-form').validate({
      rules: {
        edit_zonename: {
          required: true
        },

        edit_description: {
          required: true
        }

      },
      messages: {
        edit_zonename: {
          required: 'Please enter zone name'
        },

        edit_description: {
          required: 'Please enter the description'
        }

      },
      submitHandler: function(form) {
        // Code to execute when the form is valid and submitted
        // form.submit();

        var edit_zonename = $('#edit_zonename').val();
        var edit_description = $('#edit_description').val();
        var edit_zoneid = $('#edit_zoneid').val();

        // Check if the zone name already exists
        $.ajax({
          url: "<?php echo base_url('settings/check_zone_name_edit'); ?>",
          type: "POST",
          dataType: "json",
          data: {
            zone_name: edit_zonename,
            zone_id: edit_zoneid
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Zone name already exists!');
            } else {
              // If the zone name is new, save the zone using AJAX
              $.ajax({
                url: "<?php echo base_url('settings/edit_zone_new'); ?>",
                type: "POST",
                dataType: "json",
                data: {
                  edit_zonename: edit_zonename,
                  edit_description: edit_description,
                  edit_zoneid: edit_zoneid
                },
                success: function(saveResponse) {
                  //alert('Zone saved successfully!');
                  // Handle any UI updates or redirection if needed
                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving zone.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking zone name.');
          }
        });









      }
    });

  });
  /*********************************************************************************** */
  $(document).ready(function() {
    $('.new-zone-form').validate({
      rules: {
        zonename: {
          required: true
        },

        description: {
          required: true
        }

      },
      messages: {
        zonename: {
          required: 'Please enter zone name'
        },
        description: {
          required: 'Please enter the description'
        }
      },

      submitHandler: function(form) {
        // Code to execute when the form is valid and submitted
        //form.submit();
        var zoneName = $('#zonename').val();
        var description = $('#description').val();

        // Check if the zone name already exists
        $.ajax({
          url: "<?php echo base_url('settings/check_zone_name'); ?>",
          type: "POST",
          dataType: "json",
          data: {
            zone_name: zoneName
          },
          success: function(response) {
            if (response.exists) {
              $('.error-message').text('Zone name already exists!');

            } else {
              // If the zone name is new, save the zone using AJAX
              $.ajax({
                url: "<?php echo base_url('settings/save_zone'); ?>",
                type: "POST",
                dataType: "json",
                data: {
                  zone_name: zoneName,
                  description: description
                },
                success: function(saveResponse) {
                  //alert('Zone saved successfully!');
                  // Handle any UI updates or redirection if needed
                  $.fancybox.close();
                  location.reload();
                },
                error: function() {
                  alert('Error saving zone.');
                }
              });
            }
          },
          error: function() {
            alert('Error checking zone name.');
          }
        });








      }
    });
  });


  /*********************************************************************************** */
  function checkZoneNameAvailability(form) {
    var zoneName = $('#zonename').val();

    // AJAX request to check if the zone name exists in the database
    $.ajax({
      url: '<?= base_url() ?>settings/check_zone_name',
      type: 'POST',
      data: {
        zone_name: zoneName
      },
      dataType: 'json',
      success: function(response) {
        //alert(response.exists);
        //console.log(response);
        if (response.exists) {
          showErrorMessage('Zone name already exists. Please choose a different name.');

        } else {
          alert('Hai');
          form.submit();


        }

      },
      error: function() {
        showErrorMessage('An error occurred. Please try again later.');
      }
    });
  }

  /*********************************************************************************** */
  function showErrorMessage(message) {
    $('.error-message').remove(); // Remove any existing error message
    $('<div class="error-message">' + message + '</div>').insertBefore('#zonename');
  }





  /*********************************************************************************** */



  /*********************************************************************************** */
</script>