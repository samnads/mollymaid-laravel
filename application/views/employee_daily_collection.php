<style type="text/css">
    .widget .widget-header {
        margin-bottom: 0px;
    }

    table.da-table tr td {
        padding: 0px 6px;
    }
    .widget-header ul {
    display: table;
    height: 45px;
}
</style>


<div class="row m-0">
    <div class="col-md-12 ">
        <div class="widget widget-table action-table">
            <div class="widget-header pt-3">
                <ul>
                    <form id="daily_collection" class="form-horizontal" action="<?= base_url('employee/daily_collection'); ?>" method="post" enctype="multipart/form-data">

                        <li>
                            <i class="icon-th-list"></i>
                            <h3>Daily Collection</h3>

                        <li>
                            <input type="date" id="filter_date" name="filter_date" value="<?= isset($post['filter_date']) ? $post['filter_date'] : ''; ?>" autocomplete="off">
                        </li>

                        <?php

                        $groupedEmployees = [];
                        foreach ($employees as $employee) {
                            $leaderId = $employee['maid_leader_id'];

                            $leaderName = ($leaderId !== null && $leaderId !== '') ? $employee['leader_name'] : 'No Leader';
                            $leaderValue = ($leaderId !== null && $leaderId !== '') ? $leaderId : 'no_leader';

                            if (!isset($groupedEmployees[$leaderName])) {
                                $groupedEmployees[$leaderName] = [
                                    'leader_id' => $leaderValue,
                                    'maids'     => [],
                                ];
                            }

                            $groupedEmployees[$leaderName]['maids'][] = $employee;
                        }

                        ?>


                        <li>
                            <select name="filter_leader" id="filter_leader" class="sel2" style="width:180px">
                                <option value="">-- Select Leader --</option>
                                <?php foreach ($groupedEmployees as $leaderName => $leaderData) { ?>
                                    <option value="<?= $leaderData['leader_id']; ?>" <?= isset($post['filter_leader']) && $post['filter_leader'] == $leaderData['leader_id'] ? 'selected' : ''; ?>>
                                        <?= $leaderName; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </li>

                        <li>
                            <select name="filter_maid" id="filter_maid" class="sel2" style="width:180px">
                                <option value="">-- Select Employee --</option>
                                <?php foreach ($groupedEmployees as $leaderName => $leaderData) { ?>
                                    <?php foreach ($leaderData['maids'] as $maid) { ?>
                                        <option value="<?= $maid['maid_id']; ?>" <?= (isset($post['filter_maid']) && $post['filter_maid'] == $maid['maid_id']) ? 'selected' : ''; ?>>
                                            <?= $maid['maid_name'] . ' (' . $maid['maid_id'] . ')' ?>
                                        </option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </li>

                        <!-- <li>
                            <select name="filter_maid" id="filter_maid" data-placeholder="-- Select employee --" class="sel2" style="width:180px">
                                <option value="" <?= isset($post['filter_maid']) && $post['filter_maid'] == $post['maid_id'] ? 'selected' : ''; ?>>-- Select Employee --</option>
                            </select>
                        </li> -->

                        <!-- <li>
                            <select name="filter_maid" id="filter_maid" data-placeholder="-- Select maid --" class="sel2" style="width:180px">
                                <option value="">-- Select Employee --</option>
                                <?php foreach ($employees as $maid) { ?>
                                    <option value="<?= $maid['maid_id']; ?>" data-leader-id="<?= $maid['maid_leader_id']; ?>" data-maid-id="<?= $maid['maid_id']; ?>" data-maid-name="<?= $maid['maid_name']; ?>" <?= isset($post['filter_maid']) && $post['filter_maid'] == $maid['maid_id'] ? 'selected' : ''; ?>>
                                        <?= $maid['maid_name'] . ' (' . $maid['maid_id'] . ')' ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </li> -->

                        <!-- <li>
                            <select name="filter_leader" id="filter_leader" class="sel2" style="width:180px">
                                <option value="">-- No Leader --</option>
                            </select>
                        </li> -->


                        <li>
                            <span style="margin-left:5px;"></span>
                            <div class="mm-drop">
                                <select style="width: 100px;" id="collection_status" name="collection_status">
                                    <option value="2" <?= isset($post['collection_status']) && $post['collection_status'] == '2' ? 'selected' : ''; ?>>All</option>
                                    <option value="0" <?= isset($post['collection_status']) && $post['collection_status'] == '0' ? 'selected' : ''; ?>>Not Paid</option>
                                    <option value="1" <?= isset($post['collection_status']) && $post['collection_status'] == '1' ? 'selected' : ''; ?>>Paid</option>
                                </select>
                            </div>

                        </li>
                        <li>
                            <input type="submit" class="btn" value="Go" id="daily_collection_sbmt" name="daily_collection_sbmt" style="margin-bottom: 4px;">
                        </li>

                        </li>
                    </form>

                    <li class="mr-0 float-right">
                        <div class="topiconnew border-0 green-btn" title="Download as Excel" onclick="downloadExcel()">
                            <i class="fa fa-file-excel-o"></i>
                        </div>
                        <div class="topiconnew border-0 green-btn">
                            <a id="printButnfordailycollection" title="Print"> <i class="fa fa-print"></i></a>
                        </div>
                    </li>
                </ul>

            </div>


            <div class="widget-content">
                <?php if (!empty($collection_datas)) { ?>
                    <table id="employee-collection-table" class="table table-hover da-table" width="100%">
                        <thead>
                            <th>Service Id</th>
                            <th>Customer Code</th>
                            <th>Service Date</th>
                            <th>Customer Name</th>
                            <th>Employee Name</th>
                            <!-- <th>Customer Address</th> -->
                            <th>Hours</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            <?php
                            $total = 0;
                            $total_hours = 0;
                            ?>
                            <tr colspan="6">
                                <td colspan="4" style="padding: 20px 10px; color:#CB3636;"><b><?= $leader_name->maid_name.' (Leader)'?></b></td>
                            </tr>
                            <?php
                            foreach ($collection_datas as $collection) {
                                $total += $collection['_total_amount'];
                                $total_hours += $collection['_service_hours'];
                            ?>
                                <tr>
                                    <td><?= $collection['day_service_reference_id']; ?></td>
                                    <td><?= $collection['customer_id']; ?></td>
                                    <td><?= date('d-m-y', strtotime($collection['service_date'])); ?></td>
                                    <td><?= $collection['customer_name']; ?></td>
                                    <td><?= $collection['maid_name'] . ' (' . $collection['maid_id'] . ')'; ?></td>
                                    <!-- <td><?= $collection['customer_address']; ?></td> -->
                                    <td><?= $collection['_service_hours']; ?></td>
                                    <td><?= $collection['_total_amount']; ?></td>
                                    <td><?= $collection['payment_status'] == 0 ? 'Not Paid' : 'Paid'; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-right" colspan="5">Total hours</th>
                                <th><?= $total_hours; ?></th>
                                <th colspan="2"></th>
                            </tr>
                            <tr>
                                <th class="text-right" colspan="6">Total Amount</th>
                                <th><?= $total; ?></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                <?php } else { ?>
                    <table id="employee-collection-table" class="table table-hover da-table" width="100%">
                        <tbody>
                            <tr>
                                <td colspan="8" style="text-align: center;">No records found.</td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>



            <!-- /widget-content -->

            <div id="divToPrint" style="display: none">
                <div class="widget-content" style="margin-bottom:30px">
                    <?php if (!empty($collection_datas)) { ?>
                        <table id="employee-collection-table-excel" class="table table-hover da-table" width="100%">
                            <thead>
                                <th>Service Id</th>
                                <th>Customer Code</th>
                                <th>Service Date</th>
                                <th>Customer Name</th>
                                <th>Employee Name</th>
                                <!-- <th>Customer Address</th> -->
                                <th>Hours</th>
                                <th>Total Amount</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                $total_hours = 0; ?>
                                <tr colspan="6">
                                <td colspan="4" style="padding: 20px 10px; color:#CB3636;"><b><?= $leader_name->maid_name.' (Leader)'?></b></td>
                                </tr> <?php
                                foreach ($collection_datas as $collection) {
                                    $total += $collection['_total_amount'];
                                    $total_hours += $collection['_service_hours'];
                                ?>
                                
                                    <tr>
                                        <td><?= $collection['day_service_reference_id']; ?></td>
                                        <td><?= $collection['customer_id']; ?></td>
                                        <td><?= date('d-m-y', strtotime($collection['service_date'])); ?></td>
                                        <td><?= $collection['customer_name']; ?></td>
                                        <td><?= $collection['maid_name'] . ' (' . $collection['maid_id'] . ')'; ?></td>
                                        <!-- <td><?= $collection['customer_address']; ?></td> -->
                                        <td><?= $collection['_service_hours']; ?></td>
                                        <td><?= $collection['_total_amount']; ?></td>
                                        <td><?= $collection['payment_status'] == 0 ? 'Not Paid' : 'Paid'; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="6"></th>
                                    <th class="text-right">Total hours: <span style="float: right;"><?= $total_hours; ?></span></th>
                                </tr>
                                <tr>
                                    <th colspan="7"></th>
                                    <th class="text-right">Total Amount: <span style="float: right;"><?= $total; ?></span></th>
                                </tr>
                            </tfoot>

                        </table>
                    <?php } else { ?>
                        <table id="employee-collection-table" class="table table-hover da-table" width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="8" style="text-align: center;">No records found.</td>
                                </tr>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div>

            <div id="divForPrintCollection" style="display: none">
                <div class="widget-content" style="margin-bottom:30px">
                    <?php if (!empty($collection_datas)) { ?>
                      
                        <b><span>Employee Name: </b><?= "MM-" . sprintf('%02d', $maid_name->maid_id)?>  <span style="padding: 20px 10px;"><?=$maid_name->maid_name?></span></span><br />     
                        <!-- <table id="employee-collection-table" class="table table-hover da-table" width="100%"> -->
                        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
                            <thead>
                                <th>Service Id</th>
                                <th>Customer Code</th>
                                <!-- <th>Service Date</th> -->
                                <th>Customer Name</th>
                                <!-- <th>Employee Name</th> -->
                                <!-- <th>Customer Address</th> -->
                                <th>Hours</th>
                                <th>Total Amount</th>
                                <!-- <th>Status</th> -->
                                <th>Paid Amount</th>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                $total_paid = 0;
                                $total_hours = 0; ?>
                                <?php
                                foreach ($collection_datas as $collection) {
                                    $total += $collection['_total_amount'];
                                    $total_paid += $collection['paid_amount'];
                                    $total_hours += $collection['_service_hours'];
                                ?>
                                
                                    <tr>
                                        <td><?= $collection['day_service_reference_id']; ?></td>
                                        <td><?= $collection['customer_id']; ?></td>
                                        <!-- <td><?= date('d-m-y', strtotime($collection['service_date'])); ?></td> -->
                                        <td><?= $collection['customer_name']; ?></td>
                                        <!-- <td><?= $collection['maid_name'] . ' (' . $collection['maid_id'] . ')'; ?></td> -->
                                        <!-- <td><?= $collection['customer_address']; ?></td> -->
                                        <td style="text-align:right;"><?= $collection['_service_hours']; ?></td>
                                        <td style="text-align:right;"><?= $collection['_total_amount']; ?></td>
                                        <!-- <td><?= $collection['payment_status'] == 0 ? 'Not Paid' : 'Paid'; ?></td> -->
                                        <td style="text-align:right;"><?= $collection['paid_amount'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3" class="text-right" style="text-align:right;">Total</th>
                                    <th class="text-right"><span style="float: right;"><?= $total_hours; ?></span></th>
                                    <th class="text-right"><span style="float: right;"><?= $total; ?></span></th>
                                    <th class="text-right"><span style="float: right;"><?= $total_paid ? :'' ; ?></span></th>

                                </tr>
                                <tr>
                                    <td colspan="2"  style="padding: 40px 40px;" class="text-right"><span style="padding: 40px 40px;text-align:center;"><b><?= $maid_name->maid_name ?></span></b></td>
                                    <td colspan="2"  style="padding: 40px 40px;" class="text-right"><span style="padding: 40px 40px;text-align:center;"><b>Collected By</span></b></td>
                                    <td colspan="2"  style="padding: 40px 40px;" class="text-right"><span style="padding: 40px 40px;text-align:center;"><b>Accountant</span></b></td>

                                </tr>
                                <!-- <tr>
                                    <th colspan="7"></th>
                                    <th class="text-right">Total Amount: <span style="float: right;"><?= $total; ?></span></th>
                                </tr> -->
                            </tfoot>

                        </table>
                    <?php } else { ?>
                        <table id="employee-collection-table" class="table table-hover da-table" width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="8" style="text-align: center;">No records found.</td>
                                </tr>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div>
            <!-- print table -->
        </div>
        <!-- /widget -->
    </div>
    <!-- /span6 -->
</div>


<script>
    // $(document).ready(function() {

    //     var employees = <?php echo json_encode($employees); ?>;

    //     $('#filter_maid').change(function() {
    //         var leaderid = $(this).find(':selected').data('leader-id');
    //         $('#filter_leader').empty();
    //         if (leaderid !== undefined && leaderid !== null && leaderid !== "") {
    //             var leadername = getleadername(leaderid, employees);
    //             $('#filter_leader').append('<option value="' + leaderid + '">' + leadername + '</option>');
    //         } else if (leaderid === null || leaderid === "") {
    //             var leaderid = $(this).find(':selected').data('maid-id');
    //             var leadername = $(this).find(':selected').data('maid-name');
    //             $('#filter_leader').append('<option value="' + leaderid + '">' + leadername + '</option>');
    //         }
    //     });

    //     function getleadername(leaderid, employees) {
    //         var leadername = "No Leader";
    //         for (var i = 0; i < employees.length; i++) {
    //             if (employees[i].maid_id == leaderid) {
    //                 leadername = employees[i].leader_name;
    //                 break;
    //             }
    //         }
    //         return leadername;
    //     }
    // });
</script>