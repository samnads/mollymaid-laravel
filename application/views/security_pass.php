<style>
    #edit-profile label,
    input,
    button,
    select,
    textarea {
        font-size: 12px !important;
    }

    .flexdatalist-multiple.flex0 {
        margin-bottom: 0px;
    }

    .flexdatalist-results li {
        margin: 0px !important;
        height: auto !important;
        overflow-y: hidden;
    }
</style>

<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">
            <div class="widget-header">
                <div class="book-nav-top">
                    <ul>
                        <li>
                            <i class="icon-th-list"></i>
                            <h3><?= $passdetails ? "Edit Security Pass - " . $passdetails->pass_id : "New Security Pass"; ?></h3>
                        </li>
                        <li class="mr-0 float-right">
                            <div class="topiconnew border-0 green-btn">
                                <a href="<?php echo base_url('security_pass/pass_list'); ?>" title="Gate Pass List"> <i class="fa fa-list"></i></a>
                            </div>
                        </li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
            <div class="widget-content">
                <form id="<?= $passdetails ? "edit_security_pass_form" : "security_pass_form"; ?>" class="form-horizontal" enctype="multipart/form-data" method="post">
                    <input type="hidden" value="<?= $passdetails->pass_id; ?>" name="pass_id">
                    <fieldset>
                        <div class="col-sm-6">
                            <fieldset>
                                <div class="row m-0 n-field-main">
                                    <!-- <div class="col-sm-12"> -->
                                    <label for="pass_customer_name">Customer</label>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <select name='pass_customer_name' id="pass_customer_name" class="form-select">
                                            <option value="">---Select Customer---</option>
                                            <?php foreach ($customers as $customer) { ?>
                                                <option value="<?= $customer->customer_id; ?>" data-customer-code="<?= $customer->customer_code; ?>" data-customer-name="<?= $customer->customer_name; ?>" <?= isset($passdetails) && $passdetails->customer_id == $customer->customer_id ? 'selected' : ''; ?>>
                                                    <?= $customer->customer_name; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <!-- </div> -->
                                    <!-- <div class="col-sm-6">
                                        <label for="customer_code">Customer Code</label>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="text" class="form-control" id="customer_code" name="customer_code" disabled>
                                        </div>
                                    </div> -->
                                </div>


                                <div class="row m-0 n-field-main">
                                    <!-- <div class="col-sm-12"> -->
                                    <label for="pass_maid_name">Employee </label>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <select name='pass_maid_name' id="pass_maid_name" class="form-select">
                                            <option value="">---Select Employee---</option>
                                            <?php foreach ($maids as $maid) { ?>
                                                <option value="<?= $maid->maid_id; ?>" data-emp-type="<?= $maid->employee_type; ?>" data-employee-name="<?= $maid->maid_name; ?>" <?= isset($passdetails) && $passdetails->maid_id == $maid->maid_id ? 'selected' : ''; ?>>
                                                    <?= $maid->maid_name; ?>
                                                </option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                    <!-- </div> -->
                                    <!-- <div class="col-sm-6">
                                        <label for="emp_type">Employee Type</label>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <input type="text" class="form-control" id="emp_type" name="emp_type" disabled>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row m-0 n-field-main">
                                    <label for="pass_type">Pass Type </label>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <select name='pass_type' id="pass_type" class="form-select">
                                            <option value="">---Select Pass Type---</option>
                                            <option value="Daily" <?= isset($passdetails) && $passdetails->pass_type == 'Daily' ? 'selected' : ''; ?>>
                                                Daily
                                            </option>
                                            <option value="Yearly" <?= isset($passdetails) && $passdetails->pass_type == 'Yearly' ? 'selected' : ''; ?>>
                                                Yearly
                                            </option>
                                        </select>
                                    </div>
                                </div>



                                <div class="row m-0 n-field-main">
                                    <p>Upload Photo</p>
                                    <small id="allowed_types">Allowed types:jpg|jpeg|gif|png|doc|docx|pdf</small>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <label>
                                            <img class="rounded" id="avatar_img" src="<?= base_url('uploads/images/default/maid-avatar-upload.png?v=' . IMG_VERSION); ?>" style="width:80px;" alt="Promotion Image">
                                            <input type="file" class="form-control" name="security_pass_image" id="security_pass_image">
                                            <span id="image_name"><?= isset($passdetails) ? $passdetails->file_name : ''; ?></span>
                                        </label>
                                        <div id="security_pass_image_error"></div>
                                    </div>
                                </div>

                                <div class="row m-0 n-field-main">
                                    <p>Service Date</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <input type="text" id="service_date" name="service_date" value="<?= isset($passdetails) ? date('d-m-y', strtotime($passdetails->service_date)) : ''; ?>" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row m-0 n-field-main">
                                    <p>Service Start Date</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <input type="text" id="service_start_date" name="service_start_date" value="<?= isset($passdetails) ? date('d-m-y', strtotime($passdetails->service_start_date)) : ''; ?>" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row m-0 n-field-main">
                                    <p>Service End Date</p>
                                    <div class="col-sm-12 p-0 n-field-box">
                                        <input type="text" id="service_end_date" name="service_end_date" value="<?= isset($passdetails) ? date('d-m-y', strtotime($passdetails->service_end_date)) : ''; ?>" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                            </fieldset>
                        </div>

                    </fieldset>
                    <div class="col-sm-12">
                        <p class="text-danger" id="security-pass-form-error">&nbsp;</p>
                        <button type="button" class="n-btn" id="pass-save-btn"><?= isset($passdetails) ? 'Update' : 'Save'; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>