<html>
    <head></head>
    <body>
        <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:800px!important">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;">
                    <tr>
                        <td colspan="2"><img align="left" alt="" src="<?php echo base_url(); ?>images/elitemaidemaidbanner.jpg" width="794" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0"></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; text-align: center;">
                            <br />
                            <strong>Dear Admin,</strong><br /><br />
                            New Maintenance booking received...<br />
                            <span style=" font-size: 18px; color:#FFF; background: #78c056; padding: 7px 30px; border-radius: 25px; margin: 20px 0px 40px 0px; display: inline-block;">Reference Id - <?php echo $bookingdetails->reference_id ?></span> 
                        </td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 20px;">
                        <td valign="top" style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Address Summary</b><br></span>
                            Name - <i> <?php echo $name; ?></i><br>
                            Address - <i>Building : <?php echo $bookingdetails->building; ?></i><br>
                            <i>Unit : <?php echo $bookingdetails->unit_no; ?></i><br>
                            <i>Street : <?php echo $bookingdetails->street; ?></i><br>
                            Area - <i> <?php echo $bookingdetails->area_name; ?> </i><br />
                            Email ID - <i> <?php echo $bookingdetails->email_address; ?> </i><br />
                            Mobile - <i> <?php echo $bookingdetails->mobile_number_1; ?> </i><br />
                        </td>
                        <td style="padding: 0px 0px 0px 20px;">
                            <span style="line-height:30px;"><b>Booking Summary</b><br></span>
                            Service Date - <i><?php echo $bookingdetails->service_start_date; ?> </i>&nbsp;<br>
                            Service Type - <i><?php echo $bookingdetails->service_type_name; ?> </i>&nbsp;<br>
                            Priority Type - <i><?php echo $bookingdetails->priority_type; ?> </i>&nbsp;<br>
                            Select Shift - <i><?php echo $bookingdetails->time_type; ?></i>&nbsp;&nbsp;<i>  <?php echo $bookingdetails->start_time; ?> </i>&nbsp;<br>
                            Instruction -<i><?php echo $bookingdetails->booking_note ?></i><br /><br />
                            <?php
                            if($bookingdetails->coupon_id > 0)
                            {
                            ?>
                            Coupon Discount -<i><?php echo $percentage->percentage; ?> %</i><br /><br />    
                            <?php }
                            ?>
                        </td>
                    </tr>
                    
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #555; line-height: 18px;">
                        <td style="padding: 20px 0px 20px 20px;">
                            <span style="line-height:30px;"><b>Spectrum Services</b><br></span>
                            Al Barsha Heights, Tecom,<br>
                            Al Thuraya Telecom Tower, Office 906,<br>
                            Dubai, U.A.E.<br>
                            For Bookings : 8007274 / 04- 4310113 <br>
                            Email : booking@spectrumservices.ae<br>                                        
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #555; line-height: 18px; text-align:center; ">
                        <td colspan="2" style="padding: 20px 0px 20px 0px">&copy; 2018 Spectrum All Rights Reserved.</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </center>
        </div>
    </body>
</html>