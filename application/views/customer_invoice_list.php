<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row">   
    <div class="span12" style="width: 97% !important;">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <i class="icon-th-list"></i>
                    <h3>Customer Invoices </h3>                   
                    <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="vehicle_date" placeholder="From Date" value="<?php if($search_date_from != NULL){ echo $search_date_from; } ?>">
					<input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date_to" name="vehicle_date_to" placeholder="To Date" value="<?php if($search_date_to != NULL){ echo $search_date_to; } ?>">
					<?php 
						 if($search_date_from != NULL)
							{ 
								$url_sd_from = str_replace('/', '-', $search_date_from);
								$url_sd_from = date('Y-m-d', strtotime($url_sd_from));
							}
						 else
						   {
						   		$url_sd_from='nil';
						   } 

						 if($search_date_to != NULL)
							{ 
								$url_sd_to = str_replace('/', '-', $search_date_to);
								$url_sd_to = date('Y-m-d', strtotime($url_sd_to));
							}
						 else
						   {
						   		$url_sd_to='nil';
						   }
					?>
                    <select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="0">-- Select Customer --</option>
						<?php
						$url_custid='nil';
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
								$url_custid=$customer_id;
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
                    </select>
					<select style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="inv_status" name="inv_status">
						<option value="">-- Select Type --</option>
						<option value="0" <?php if($inv_status == '0'){ echo 'selected'; }; ?>>Draft</option>
						<option value="1" <?php if($inv_status == '1'){ echo 'selected'; }; ?>>Open</option>
						<option value="2" <?php if($inv_status == '2'){ echo 'selected'; }; ?>>Cancelled</option>
						<option value="3" <?php if($inv_status == '3'){ echo 'selected'; }; ?>>Paid</option>
						<option value="4" <?php if($inv_status == '4'){ echo 'selected'; }; ?>>UnPaid</option>
                    </select>
                    <?php $url_inv_status=(is_numeric($inv_status))?$inv_status:'nil';?>
                    <input type="submit" class="btn" value="Go" name="vehicle_report" style="margin-bottom: 4px;">

                    <div class="topiconnew"><a href="<?php echo base_url();?>reports/invoicereporttoExcel/<?php echo $url_sd_from;?>/<?php echo $url_sd_to;?>/<?php echo $url_custid;?>/<?php echo $url_inv_status;?>"><img src="<?php echo base_url();?>images/excel-icon.png" title="Download to Excel"></a></div>

                    <!--<a style="background: transparent;border:0px;float: right;margin-right: 15px;" href='<?php// echo base_url();?>quickbook/quickbook_sync_invoice'>
                    <input type="button" class="btn" value="Sync To Quickbook" name="" >
                    </a>-->
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl. No</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Number</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Invoice Date</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Due Date</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Total</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Paid Amount</th>
							<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Due Amount</th>
                            <!--<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount Due</th>-->
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Status</th>
                            <th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$sub_t = 0;
					$grand_paid = 0;
					$grand_due = 0;
					$i = 1;
                    foreach ($invoice_report as $inv){
						$sub_t += round($inv->invoice_net_amount,2);
						$grand_paid += round($inv->received_amount,2);
						$grand_due += round($inv->balance_amount,2);
					?>
						<tr>
							<td style="line-height: 18px;"><?php echo $i; ?> </td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_num; ?> </td>
							<td style="line-height: 18px;"><?php echo $inv->userName; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_date; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_due_date; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->invoice_net_amount; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->received_amount ?: '-'; ?></td>
							<td style="line-height: 18px;"><?php echo $inv->balance_amount; ?></td>
							<!--<td style="line-height: 18px;"></td>-->
							<td style="line-height: 18px">
								<?php
								if($inv->invoice_paid_status == 2)
								{
									$paystat = " (Partially Paid)";
								} else {
									$paystat = "";
								}
								if($inv->invoice_status == '0')
								{
									$invstatus = "Draft";
								} else if($inv->invoice_status == '1')
								{
									$invstatus = "Open";
								} else  if($inv->invoice_status == '2'){
									$invstatus = "Cancelled";
								} else  if($inv->invoice_status == '3'){
									$invstatus = "Paid";
								}
								echo $invstatus.$paystat;
								?>
							</td>
							<td style="line-height: 18px">
								<a class="btn btn-small btn-info" href="<?php echo base_url(); ?>invoice/view_invoice/<?php echo $inv->invoice_id; ?>" title="View Invoice">
									<i class="btn-icon-only fa fa-eye "> </i>
								</a>
								<a class="btn btn-small btn-info" href="<?php echo base_url(); ?>invoice/generateinvoice/<?php echo $inv->invoice_id; ?>" target="_blank" title="Download Invoice">
									<i class="btn-icon-only fa fa-download "> </i>
								</a>
								<!--<div class="btn btn-small btn-info send_inv_mail" title="Download Invoice" data-invoiceid="<?php echo $inv->invoice_id; ?>" data-invoiceemail="<?php echo $inv->email_address; ?>">
									<i class="btn-icon-only fa fa-envelope" ></i>
									<i class="fa fa-spinner fa-spin" style="display:none;"></i>
								</div>-->
							</td>
						</tr>
					<?php
					$i++;	
					}
					?>
                    </tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td><?php echo $sub_t; ?></td>
							<td><?php echo $grand_paid; ?></td>
							<td><?php echo $grand_due; ?></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<button type="button" class="btn btn-info btn-lg openmodal hidden" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body popmsg">
        <div class="alert alert-success">
		  <strong>Success!</strong> Indicates a successful or positive action.
		</div>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>



<button type="button" class="btn btn-info btn-lg openemailmodal hidden" data-toggle="modal" data-target="#emailModal">Open Modal</button>

<!-- Modal -->
<div id="emailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -6px;font-size:32px;">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
      	  <div id="emailpopmsg"></div>
          <div class="form-group">
		    <label for="email_content">Email Content:</label>
		    <textarea class="form-control" id="email_content"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="email">Email:</label>
		    <input type="email" class="form-control" id="email">
		    <input type="hidden" class="form-control" id="hidinvid">
		  </div>
		  
		  <button type="submit" class="btn btn-default emailsubbtn">Submit</button>
		  <button type="submit" class="btn btn-default emailloadbtn" style="display:none;"><i class="fa fa-spinner fa-spin" ></i></button>
      </div>
      <!-- <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>



<script type="text/javascript">
$( document ).ready(function() {


	$(".send_inv_mail").click(function() {
		var invoiceid=$(this).data('invoiceid');
		var emailid=$(this).data('invoiceemail');
		$("#email").val(emailid);$("#hidinvid").val(invoiceid);
		$("#emailpopmsg").html('');
		$("#emailpopmsg").html('');
		$(".openemailmodal").click();
	});	


    $(".emailsubbtn").click(function() {
    	$("#emailpopmsg").html('');
	    var invoiceid=$("#hidinvid").val();
	    var invoicecontent=$("#email_content").val();
	    var invoiceemail=$("#email").val();
	    //console.log(invoiceid);
	    if(invoicecontent=='')
	    {
	    	$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please enter email content!</strong></div>');
	    	return false;
	    }validateEmail

	    if(!validateEmail(invoiceemail))
	    {
	    	$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check the email entered!</strong></div>');
	    	return false;
	    }



	    $(this).hide();
	    $(".emailloadbtn").show();
	    $.ajax({
                   url: "<?php echo base_url();?>invoice/send_invoice_email",
                   type: "post",
                   data: {invoiceid:invoiceid,invoicecontent:invoicecontent,invoiceemail:invoiceemail} ,
                   success: function (response) {
                        $(".emailsubbtn").show();
	    				$(".emailloadbtn").hide();
                        if(response=='success')
                        {
                        	$("#emailpopmsg").html('<div class="alert alert-success text-center"><strong>Invoice email sent successfully!</strong></div>');
                        	$("#email_content,#email").val('')
                        	//$(".openmodal").click(); 
                        }
                    	else if(response=='email_error')
                        {
                        	$("#emailpopmsg").html('<div class="alert alert-warning text-center"><strong>Please check customer email!</strong></div>');
                        	//$(".openmodal").click();
                        }
                    	else
                    	{
                    		$("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                    		//$(".openmodal").click();
                    	}

                   },
                   error: function(jqXHR, textStatus, errorThrown) {
                        $(".send_inv_mail[data-invoiceid='"+invoiceid+"'] .fa-envelope").show();
                        $(".send_inv_mail[data-invoiceid='"+invoiceid+"'] .fa-spin").hide();
                        $("#emailpopmsg").html('<div class="alert alert-danger text-center"><strong>Error!Try again later</strong></div>');
                    	//$(".openmodal").click();

                   }
               });
	});
});	

function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
</script>
