<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />

<div class="row m-0">
    <div class="col-md-12">


        <div class="widget widget-table action-table">
            <div class="widget-header">

                <form class="form-horizontal" method="post">
                    
                    <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Booking Approval List</h3></li>
              <li>               
                <input type="text" style="width:160px;" id="schedule_date" name="schedule_date" value="<?= $this->input->post('schedule_date') ?: date('d/m/Y'); ?>">
            </li>
              <li class="mr-2 mm-drop">
                  <select name="filter" id="filter" class="span3" required style="width: 160px;">
                        <option value="All" <?php echo $filter == 'All' ? 'selected="selected"' : '' ?>>All</option>
                        <option value="Pending" <?php echo $filter == 'Pending' ? 'selected="selected"' : '' ?>>Pending</option>
                        <option value="Expired" <?php echo $filter == 'Expired' ? 'selected="selected"' : '' ?>>Missed</option>
                        <option value="Assigned" <?php echo $filter == 'Assigned' ? 'selected="selected"' : '' ?>>Assigned</option>
                        <option value="Deleted" <?php echo $filter == 'Deleted' ? 'selected="selected"' : '' ?>>Deleted</option>
                    </select>
              </li>
      
      
      
              <li>
                  <input type="submit" class="n-btn" id="deleteassignmaidfilter" value="Go" name="vehicle_report" >
              </li>
      
      
      
             
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
                    
                </form>
            </div>
            <!-- /widget-header -->
            
            
            
        
        
            
            
            
            <div class="widget-content">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 10px; width: 5px !important;"><center> Sl.No.</center> </th>
                            <th style="line-height: 10px; width: 75px !important;">Customer </th>
                            <th style="line-height: 10px; width: 130px !important;">Address</th>
                            <th style="line-height: 10px; width: 107px !important;">Day</th>
                            <th style="line-height: 20px; width: 150px !important;">Service</th>
                            <th style="line-height: 20px; width: 150px !important;">Requested Maid</th>
                             <th style="line-height: 20px; width: 75px !important;">MOP</th>
                            <th style="line-height: 10px; width: 126px !important;">Shift</th>
                            <th style="line-height: 10px; width: 140px !important;">Notes</th>
                            <th style="line-height: 20px; width: 97px !important;">From</th>
<th style="line-height: 10px; width: 200px !important;" class="td-actions"><center><?php if ($filter == 'Deleted') {echo 'Remarks';} else {echo 'Actions';}?></center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
if (empty($approval_list)) {
    ?>
                        <tr><td colspan="9"  style="line-height: 20px; text-align:justify;">No records found</td></tr>
                                <?php
}
$i = 0;
$attribute = $filter == 'Assigned' || $filter == 'Expired' ? 'disabled' : '';
foreach ($approval_list as $booking) {
    //Payment Type
    if ($booking->payment_type == "D") {
        $paytype = "(D)";
    } else if ($booking->payment_type == "W") {
        $paytype = "(W)";
    } else if ($booking->payment_type == "M") {
        $paytype = "(M)";
    } else {
        $paytype = "";
    }
    if ($booking->cleaning_material == "Y") {
        $request_clean_material = " + <span style='color:green';><br>cleaning material</span>";
    } else {
        $request_clean_material = "";
    }

    if ($booking->customer_address == "") {
        $a_address = 'Building - ' . $booking->building . ', ' . $booking->unit_no . '' . $booking->street;
    } else {
        $a_address = $booking->customer_address;
    }

    echo '<tr>'
    . '<td style="line-height: 20px; width: 5px; text-align:center;">' . ++$i . '</td>'
    . '<td style="line-height: 20px; width: 75px; text-align:justify;">' . $booking->customer_name . $paytype . '<br /> ' . ($booking->phone_number ? $booking->phone_number : ($booking->mobile_number_1 ? $booking->mobile_number_1 : ($booking->mobile_number_2 ? $booking->mobile_number_2 : $booking->mobile_number_3))) . '</td>'
    . '<td style="line-height: 20px; width: 130px; text-align:justify;">' . $a_address . '<br />' . $booking->area_name . '<br />' . $booking->zone_name . '</td>'
    . '<td style="line-height: 20px; width: 107px; text-align:justify;">' . $booking->service_date . ' <br /> ' . $booking->weekday . ' ( ' . $booking->booking_type . ')</td>'
    . '<td style="line-height: 20px; width: 97px; text-align:justify;">' . $booking->service_type_name . ' ' . $booking->exserv . '</td>'
    . '<td style="line-height: 20px; width: 97px; text-align:justify;">' . $booking->maid_name . '(' . $booking->no_of_maids . ')' . $request_clean_material . '</td>'
    . '<td style="line-height: 20px; width: 75px; text-align:justify;">' . $booking->pay_by . '</td>'
    . '<td style="line-height: 20px; width: 126px; text-align:justify;">' . $booking->shift . '</td>'
    //. '<td style="line-height: 20px; width: 97px; text-align:justify;">' . $booking->booked_datetime . '</td>'
     . '<td style="line-height: 20px; width: 140px; text-align:justify;">' . $booking->booking_note . ' (' . $booking->crew_in . ') </td>'
        . '<td  style="line-height: 20px; width: 97px; text-align:justify;"><span class="book_source_' . $source[$booking->booked_from] . '">' . $source[$booking->booked_from] . '</span></td>'
        . '<td vstyle="line-height: 20px; width: 275px;" class="td-actions" style="padding: 7px 10px;">';
//                                           . '<input type="button" ' . $attribute . ' onclick="assignMaid(' . $booking->booking_id . ', ' . $booking->no_of_maids . ')" class="save-but" value="Assign Maid" style="font-size:9px; line-height:12px;" />'
    //                                           . '<input type="button" ' . $attribute . ' onclick="deleteBooking(' . $booking->booking_id . ')" class="delete-but" value="Delete"  style="font-size:9px; line-height:12px;"/>'
    if ($booking->booking_status == '0') {

        //echo '<input type="button" ' . $attribute . ' onclick="Maid_booking(' . $booking->booking_id . ', ' . $booking->no_of_maids . ', ' . $booking->customer_id. ')" class="save-but" value="Assign" style="font-size:9px; line-height:12px; margin-right: 0px!important;" />';
        if ($filter == 'Assigned') {
            echo '<input type="button" onclick="Maid_booking_view(' . $booking->booking_id . ', ' . $booking->no_of_maids . ', ' . $booking->customer_id . ')" class="btn" value="Assigned" disabled>';
        }
        else if ($filter == 'Expired') {
            echo '<input type="button" onclick="Maid_booking_view(' . $booking->booking_id . ', ' . $booking->no_of_maids . ', ' . $booking->customer_id . ')" class="btn" value="Missed" disabled>';

        } else {
            echo '<input type="button" onclick="Maid_booking_view(' . $booking->booking_id . ', ' . $booking->no_of_maids . ', ' . $booking->customer_id . ')" class="n-btn mb-0 mr-1" value="Assign">';
        }
        // if(user_authenticate() == 1)
        // {
        echo '<span class="n-btn-icon red-btn" onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id . ')"><i class="btn-icon-only icon-remove"> </i></span>';
        //}
        //echo '<input type="button" ' . $attribute . ' onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id. ')" class="btn btn-danger" value="Delete" style="font-size:9px; line-height:12px; margin-right: 0px!important;" />';
    } else if ($booking->booking_status == '2') {
        echo $booking->delete_remarks;
    } else {

        echo '<input type="button" class="btn" value="Assigned" disabled>';
//                                   echo '<span class="not-started">Assigned</span>';
        // if(user_authenticate() == 1)
        // {
        echo '<span class="n-btn-icon red-btn" onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id . ')"><i class="btn-icon-only icon-remove"> </i></span>';
        //}
        //echo '<input type="button" ' . $attribute . ' onclick="delete_booking(' . $booking->booking_id . ',' . $booking->customer_id. ')" class="btn btn-danger" value="Delete" style="font-size:9px; line-height:12px; margin-right: 0px!important;" />';
    }

    echo '</td>'
        . '</tr>';

}

?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<div id="free-maid-list-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 style="color:white; padding-bottom: 0px !important;">Free Maid List</h3>
    </div>
    <div class="modal-body">


            <div class="controls">

                <label class="radio inline">
                    <input type="radio" name="same_zone" value="0" checked="checked"> All
                </label>
                <label class="radio inline">
                    <input type="radio" name="same_zone" value="1"> Same Zone
                </label>


            </div>
        <p id="frm-transfer">
                <select id="free-maid-id" data-placeholder="Select service type" class="sel2">
                    <option value="">Select Maid</option>

                </select>
        </p>
        <p id="no-maids-selected"></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a class="save-but" style="text-decoration: none;" href="javascript:void" id="assign-maid">Submit</a>
    </div>
</div>
<script>
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('schedule'); ?>') {
			$this.addClass('active');
		}
	})
})
</script>