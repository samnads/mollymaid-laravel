<style>
    .book-nav-top li{
        margin: 0 10px 0 0 !important;
    }
    .select2-arrow{visibility : hidden;}
    .select2-container .select2-choice{
	-moz-appearance: none;
        background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #ccc;
        border-radius: 3px;
        cursor: pointer;
        font-size: 12px;
        height: 30px;
        line-height: 24px;
        padding: 3px 0 3px 10px;
        text-indent: 0.01px;
    }
    .select2-results li{margin-left: 0px !important;}
/*    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }*/
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">
        <div class="col-md-12 col-sm-12 no-left-right-padding">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header"> 
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>customer/customer_statement">
                    <div class="book-nav-top">
                        <ul>
                            <li>
                                <i class="icon-th-list"></i>
                                <h3>Customer Statement</h3>
                            </li>
                            
                            <li>
                                <input type="text" id="cstatemnet-date-from" name="cstatemnet-date-from" style="width: 160px;" data-date="<?php echo $search_date_from_statement; ?>" readonly value="<?php echo $search_date_from_statement; ?>" data-date-format="dd/mm/yyyy"/> 
                            </li>
                            <li>
                                <input type="text" id="cstatemnet-date-to" name="cstatemnet-date-to" style="width: 160px;" data-date='<?php echo $search_date_to_statement; ?>' readonly value='<?php echo $search_date_to_statement ?>' data-date-format="dd/mm/yyyy"/>
                            </li>
                            
                            <li>
                                <select name="statement_customer_id" id="statement_customer_id" data-placeholder="Select customer" class="statement-customer-sec" style="width: 160px;">
                                    <option></option> 
                                    <?php
                                    foreach($customers as $customer)
                                    {  
                                        if($customer->payment_type == "D")
                                        {
                                            $paytype = "(Daily)";
                                        } else if($customer->payment_type == "W")
                                        {
                                            $paytype = "(Weekly)";
                                        } else if($customer->payment_type == "M")
                                        {
                                            $paytype = "(Monthly)";
                                        } else
                                        {
                                            $paytype = "";
                                        } 
                                        $p_number = array();
                                        if($customer->phone_number != NULL)
                                            array_push ($p_number, $customer->phone_number);
                                        if($customer->mobile_number_1 != NULL)
                                            array_push ($p_number, $customer->mobile_number_1);
                                        if($customer->mobile_number_2 != NULL)
                                            array_push ($p_number, $customer->mobile_number_2);
                                        if(@$customer->mobile_numebr_3 != NULL)
                                            array_push ($p_number, @$customer->mobile_number_3);

                                        $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';
                                        if($search_cust_id == $customer->customer_id)
                                        {
                                            $sel = "selected";
                                        } else {
                                            $sel = "";
                                        }
                                        echo '<option value="' . $customer->customer_id . '" '.$sel.'>' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
                                    }			
                                    ?>
                                </select>
                            </li>
                            
                            <li>
                                <input type="submit" id="customer-statement-search" value="Search" class="n-btn" />
                            </li>
<!--                                <li class="pull-right no-right-margin">
                                    <a class="newjobbutton" id="newjobbutton" href="<?php// echo base_url(); ?>activity/addjob"><i class="fa fa-plus"></i> New Job</a>
                                </li>-->
                            <div class="clear"></div>
                            <!-- Statement ends -->
                        </ul>
                    </div>
                    <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->            
                </form>
            </div>
            
            
            
            
            
            
            
            
            
            <div id="statement_content" class="">	
                <table id="statement-content-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <!--<th class="no-left-border"></th>-->
                            <th>Sl</th>
                            <th>Customer</th>
                            <th>Maid</th>
                            <th>Time</th>
                            <th>Service date</th>
                            <th>Amount</th>
                            <th>Paid Amount</th>
                            <th>Paid Date</th>
                            <th class="no-right-border">Balance</th>
                        </tr>
                    </thead>
                    <tbody id="invoice-tabtbody1">
                        <?php
                        if(!empty($customer_statement_array))
                        {
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Balance Due : </td>
                            <td><?php echo $balance_amount;  ?></td>
                        </tr>
                        <?php
                        $sln = 1;
                        $amt = 0;
                        $p_amt = 0;
                        $bal_amt = $balance_amount;
                        
                        foreach($customer_statement_array as $key => $val){
                        foreach($val as $sales)
                        {
                            $paid = ($sales->paid_amount + $sales->outstanding_amt);
                            $bal = ($sales->total_fee - $paid);
                            $amt += $sales->total_fee;
                            $p_amt += $paid;
                            $bal_amt += $bal;
                        ?>
                        <tr>
                            <td><?php echo $sln++; ?></td>
                            <td><?php echo $sales->customer_name; ?></td>
                            <td><?php echo $sales->maid_name; ?></td>
                            <td><?php echo $sales->time_from; ?> - <?php echo $sales->time_to; ?></td>
                            <td><?php echo $sales->service_date; ?></td>
                            <td><?php echo $sales->total_fee; ?></td>
                            <td><?php echo $paid; ?></td>
                            <td><?php echo $sales->paid_time; ?></td>
                            <td><?php echo $bal; ?></td>
                            
                        </tr>
                        <?php
                        } }
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <td><?php echo $amt; ?></td>
                            <td><?php echo $p_amt; ?></td>
                            <td></td>
                            <td><?php echo $bal_amt; ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                 </table> 
            </div>
        </div><!--welcome-text-main end-->
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->