<style>
.bootstrap-tagsinput {

    background-color: #fff;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    display: inline-block;
    padding: 4px 6px;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    max-width: 100%;
    line-height: 22px;
    cursor: text;
    overflow-y: scroll;
    max-height: 300px;
    width:99.8%;

}
</style>
<div class="row m-0">
    <!-- /span6 --> 
    <div class="col-md-12" id="add_user" style="float: none; margin: 0 auto;">      		
	<div class="widget ">
	    <div class="widget-header" style="margin-bottom: 0;">
	      	<i class="icon-globe"></i>
                    <h3>Send Bulk Email</h3>
                    
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="sms" action="<?php echo base_url(); ?>customer/customer_bulk_email" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">
                                        <div class="error" style="text-align: center;">
										 <?php if($this->session->flashdata('email_success')){ ?>
										 <p style="color:green;text-align:center;"><?php echo $this->session->flashdata('email_success') ?></p>
										 
										 <?php } ?>
										 <?php if($this->session->flashdata('email_error')){ ?>
										 <p style="color:red;text-align:center;"><?php echo $this->session->flashdata('email_error') ?></p>
										 
										 <?php } ?>
										</div>
                                    </div>
                  
									<div class="control-group">											
											<label class="control-label" for="coupontype">Customer Type</label>
                                            <div class="controls">
                                                <select class="form-control select2" name="customer_type" id="customer_type_bulk_email" style="width: 350px;">
													  
													  <option value="1">Regular</option>
													  <option value="0">One-time customer</option>
													  <?php foreach ($email_groups as $email_group) {?>
														<option value="<?php echo $email_group->email_group_id;?>"><?php echo $email_group->email_group_name;?></option>   
														<?php }?>
												</select>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->


                                    <div class="control-group">											
                                            <label class="control-label" for="coupontype">Email address</label>
                                            <div class="controls">
                                            <input type="text" name="email_list" id="email_list" value="<?php echo $customer_email_address ?>" data-role="tagsinput" /> 
					    </div>
                                       </div> <!-- /controls -->
                                     <div class="control-group">											
                                            <label class="control-label" for="coupontype">Banner Image</label>
                                            <div class="controls">
                                                    <div id="me" class="styleall" style=" cursor:pointer;"> 
                                                          <span style=" cursor:pointer;">                             
                                                               <img src="<?php echo base_url();?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> 
                                                          </span>
                                                       
                                                    </div> 
                                                <div id="bnr_dsply">
                                                 <?php 
                                                        $files = glob('blk_mail_img/*');
                                                        $bnr_url=base_url().$files[0];
                                                        if (file_get_contents($bnr_url) !== false)
                                                            { ?>
                                                        
                                                            <img src="<?php echo $bnr_url;?>" style="width:300px"/><br>
                                                            
                                                        
                                                         <?php   }
                                                        
                                                        ?>
                                                 </div>
                                                <input type="hidden" name="call_method" id="call_method" value="customer/bulkmailbannerimgupload"/>
                                                <input type="hidden" name="img_fold" id="img_fold" value="blk_mail_img"/>
                                                <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            </div>
                                    </div> <!-- /control-group -->
				    <div class="control-group">											
                                            <label class="control-label" for="coupontype">Subject</label>
                                            <div class="controls">
                                            <input type="text" name="email_subject" id="email_subject" value="" /> 

                                       </div> <!-- /controls --><br>					
		  		    <div class="control-group">											
											<label class="control-label" for="customertype">Message</label>
                                            <div class="controls">
                                             <textarea style="width: 99.8%;" name="email_message" id="editor"><?php echo $this->load->view('offer_view') ?></textarea>
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                   
										
                                    <div class="form-actions">
                                        <input type="submit" class="n-btn" value="Send" name="send_email"><!--btn btn-info send_bulk e-new-but-->

                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>