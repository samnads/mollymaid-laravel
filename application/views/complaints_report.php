<style type="text/css">
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Complaints Reports</h3></li>
              
              <li>
                  <?php
                    if ($search['search_date_from'] == "") {
                        ?>
                        <span style="color: #fff;">From :</span> <input type="text" id="search_date_from" name="search_date_from" value="<?php echo date('d/m/Y', strtotime('-7 days')) ?>" required>

                        <?Php
                    } else {
                        ?>
                        <span style="color: #fff;">From :</span> <input type="text" id="search_date_from" name="search_date_from" value="<?php echo $search['search_date_from'] ?>" required> 
                        <?Php
                    }
                    ?>
              </li>
      
      
      
              <li>
                    <?php
                    if ($search['search_date_to'] == "") {
                        ?>
                        <span style="color: #fff;">To :</span> <input type="text" id="search_date_to" name="search_date_to" value="<?php echo date('d/m/Y') ?>" required>

                        <?Php
                    } else {
                        ?>
                        <span style="color: #fff;">To :</span> <input type="text" id="search_date_to" name="search_date_to" value="<?php echo $search['search_date_to'] ?>" required>
                        <?Php
                    }
                    ?>
              </li>
      
      
      
              <li class="mr-2">
                  <input type="hidden" name="day" id="day_from" value="<?php echo $search['search_day_from'] ?>">
                    <input type="hidden" name="day" id="day_to" value="<?php echo $search['search_day_to'] ?>">
                    
                    
              </li>
      
      
      
              <li>
                  <input type="submit" class="n-btn" value="Go" name="rate_report" >
              </li>
      
      
      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                      <a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                 </div>
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
                    
                    
                </form>   
            </div>
            
            
            
            
            
        
        





            
            
            
            
            

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width:40px;"> Sl.No</th>
                            <th style="line-height: 18px;"> Customer Name</th>
                            <th style="line-height: 18px;"> Location</th>
                            <th style="line-height: 18px;"> Booking date</th>
                            <th style="line-height: 18px;"> Booking time</th>
                            <th style="line-height: 18px;"> Complaint type</th>
                            <th style="line-height: 18px;"> Customer feedback</th>
                            <th style="line-height: 18px;"> Staff</th>
                            <th style="line-height: 18px;"> Status</th>
                            <th style="line-height: 18px;"> Staff/Driver comments</th>
                            <th style="line-height: 18px;"> Action take</th>
                            <th style="line-height: 18px;"> Service date</th>
                            <th style="line-height: 18px;"> Driver name</th>
                            <th style="line-height: 18px;">Deduction Amount</th>
                            <th style="line-height: 18px;">Complaint Added</th>
                            <th style="line-height: 18px;">Image</th>
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       
                        if ($rate_report != NULL) {
                            $i = 1;
                            foreach ($rate_report as $veh){
                                
								
								
                           ?>
                                <tr>
                                        <td style="line-height: 18px;"><?php echo $i; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->customer_name; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->customer_address; ?> </td>
										<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($veh->booked_datetime)); ?> </td>
										<td style="line-height: 18px;"><?php echo  date('h:i:a',strtotime($veh->booked_datetime)); ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->complaint_type; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->complaint; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->maid_name; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->status; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->cleaner_comments; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->action_taken; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->service_date; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->driver_name; ?> </td>
										<td style="line-height: 18px;text-align:center;"><?php echo $veh->deduction_amount; ?> </td>
										<td style="line-height: 18px;"><?php echo date('d-m-Y h:i:s A',strtotime($veh->added_time)) ; ?> </td>
                                        <?php if($veh->image !=''){?>
                                        <td style="line-height: 18px;"><a href="<?php echo base_url(); ?>complaints/<?php echo $veh->image; ?>" target="_blank">View Image</a> </td>
										<?php }else{ ?>
                                            <td style="line-height: 18px;"> </td> 
                                      <?php  } ?>
									
                                    </tr>
                            <?php
							$i++;
                            }
                                } ?>



                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
            <thead>
                <tr>
                
                            <th style="line-height: 18px;"> Sl.No</th>
                            <th style="line-height: 18px;"> Customer Name</th>
                            <th style="line-height: 18px;"> Location</th>
                            <th style="line-height: 18px;"> Booking date</th>
                            <th style="line-height: 18px;"> Booking time</th>
                            <th style="line-height: 18px;"> Complaint type</th>
                            <th style="line-height: 18px;"> Customer feedback</th>
                            <th style="line-height: 18px;"> Staff</th>
                            <th style="line-height: 18px;"> Status</th>
                            <th style="line-height: 18px;"> Staff/Driver comments</th>
                            <th style="line-height: 18px;"> Action take</th>
                            <th style="line-height: 18px;"> Service date</th>
                            <th style="line-height: 18px;"> Driver name</th>
                            <th style="line-height: 18px;">Deduction Amount</th>
                            <th style="line-height: 18px;">Complaint Added</th>
                            
                            
                    
                </tr>
            </thead>
            <tbody>
                <?php
               
                if ($rate_report != NULL) {
                    $i = 1;
                    foreach ($rate_report as $veh){
                        
                        
                        
                   ?>
                        <tr>
                        <td style="line-height: 18px;"><?php echo $i; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->customer_name; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->customer_address; ?> </td>
										<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($veh->booked_datetime)); ?> </td>
										<td style="line-height: 18px;"><?php echo  date('h:i:a',strtotime($veh->booked_datetime)); ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->complaint_type; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->complaint; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->maid_name; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->status; ?> </td>
                                        <td style="line-height: 18px;"><?php echo $veh->cleaner_comments; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->action_taken; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->service_date; ?> </td>
										<td style="line-height: 18px;"><?php echo $veh->driver_name; ?> </td>
                                        <td style="line-height: 18px;text-align:center;"><?php echo $veh->deduction_amount; ?> </td>
										<td style="line-height: 18px;"><?php echo date('d-m-Y h:i:s A',strtotime($veh->added_time)) ; ?> </td>
                                     
                            </tr>
                    <?php
                    $i++;
                    }
                        } ?>



            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "ComplaintsReport.xls"); // Choose the file name
      return false;
  }  
</script>