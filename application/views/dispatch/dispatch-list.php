<div id="alert-popup" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="alert-title"></span>
    </div>
    <div class="modal-body">
      <h3 id="alert-message"></h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0 pop_close">OK</button>
    </div>
  </div>
</div>
<div id="bookings-for-confirm-dispatch" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="title">Bookings to Confirm Dispatch</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <form id="bookings-confirm-dispatch-form">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-confirm-dispatch-list">
        <div class="row m-0 n-field-main" style="border-bottom: 1px solid #ddd;">
          <div class="col-sm-3 n-field-box pl-0">
            <div class="n-end n-pick-maids-set">
              <input id="check_all" type="checkbox">
              <label for="check_all">
                <span class="border-radius-3"></span>
                <p>Check All</p>
              </label>
            </div>
          </div>
        </div>
        <div class="inner p-2" style="height: 260px;overflow-x:hidden;overflow-y:scroll;">
        </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0 pop_close">Cancel</button>
      <button type="button"  data-action="confirm-dispatch-now" class="n-btn mb-0" style="display:none">Confirm</button>
    </div>
    </form>
  </div>
</div>
<div id="bookings-for-undo-confirm-dispatch" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="title">Rollback Confirmed Dispatches</span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <form id="bookings-undo-confirm-dispatch-form">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-undo-confirm-dispatch-list">
        <div class="row m-0 n-field-main" style="border-bottom: 1px solid #ddd;">
          <div class="col-sm-3 n-field-box pl-0">
            <div class="n-end n-pick-maids-set">
              <input id="check_all_for_undo" type="checkbox">
              <label for="check_all_for_undo">
                <span class="border-radius-3"></span>
                <p>Check All</p>
              </label>
            </div>
          </div>
        </div>
        <div class="inner p-2" style="height: 260px;overflow-x:hidden;overflow-y:scroll;">
        </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0 pop_close">Cancel</button>
      <button type="button" data-action="confirm-undo-dispatch-now" class="n-btn mb-0" style="display:none;">Rollback</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Booking Dispatched List</h3></li>
        <li><input type="text" id="dispatch_service_date" name="dispatch_service_date" value="<?=DateTime::createFromFormat('Y-m-d', $service_date)->format('d/m/Y')?>" readonly></li>
        <li class="mr-0 float-right">
        <li id="dispatch-confirm-li"><button class="n-btn" data-action="dispatch-confirm"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Confirm</button></li>
        <li id="dispatch-confirm-undo-li" style="display:none"><button class="n-btn red-btn" data-action="dispatch-confirm-undo"><i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp;Rollback</button></li>
        </li>
        </ul>
      </div>
      <!-- /widget-header -->
      <div id="exTab2" class="">
        <ul class="nav nav-tabs">
          <li class="active">
            <a data-target="#dispatched" data-toggle="tab" id="tab1" data-tab="dispatched"><i class="fa fa-clock-o" aria-hidden="true"></i> Unconfirmed</a>
          </li>
          <li>
            <a data-target="#confirmed" data-toggle="tab" id="tab2" data-tab="confirmed"><i class="fa fa-check" aria-hidden="true"></i> Confirmed</a>
          </li>
          <li>
            <a data-target="#keydispatch" data-toggle="tab" id="tab3" data-tab="keydispatch"><i class="fa fa-key" aria-hidden="true"></i> Key Dispatch</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="dispatched">
            <table id="dispatches-table" class="table table-hover da-table" width="100%">
              <thead>
              <tr>
                <th style="width:45px">
                  Sl. No.
                </th>
                <th>
                  Booking Ref.
                </th>
                <th>
                  Schedule Ref.
                </th>
                <th>
                  Schedule Type
                </th>
                <th>
                  Maid
                </th>
                <th>
                  Customer
                </th>
                <th>
                  Date
                </th>
                <th>
                  Time
                </th>
                <th>
                  Status
                </th>
                <th class="text-center">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              <?php if (is_array($unconfirmed_dispatches) && count($unconfirmed_dispatches) > 0): ?>
                <?php foreach ($unconfirmed_dispatches as $key => $row) :
                    if ($row->booking_type == "OD") {
                      $booking_type_name = "One Time";
                    }
                    if ($row->booking_type == "WE") {
                      $booking_type_name = "Regular";
                    }
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 20px;  text-align:center;">
                      <center><?php echo $key + 1; ?></center>
                    </td>
                    <td><?php echo $row->booking_reference_id ?></td>
                    <td><?php echo $row->day_service_reference_id ?></td>
                    <td><?php echo $booking_type_name ?></td>
                    <td><?php echo $row->maid_name ?></td>
                    <td><?php echo $row->customer_name ?></td>
                    <td><?php echo DateTime::createFromFormat('Y-m-d', $row->service_date)->format('d M Y') ?></td>
                    <td><?=DateTime::createFromFormat('H:i:s', $row->time_from)->format('h:i A')?> to <?=DateTime::createFromFormat('H:i:s', $row->time_to)->format('h:i A')?></td>
                    <td>
                      <span class="btn-block badge badge-success">Dispatched</span>
                    </td>
                    <td style=" width: 100px" class="td-actions">
                      <a class="n-btn-icon green-btn" data-action="edit-dispatched-schedule" data-id="<?= $row->day_service_id ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      <a class="n-btn-icon purple-btn" data-action="view-dispatched-schedule" data-id="<?= $row->day_service_id ?>" data-id="<?= $row->day_service_id ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <!-- <a class="n-btn-icon red-btn" data-action="delete-dispatched-schedule" data-id="<?= $row->day_service_id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a> -->
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane" id="confirmed">
          <table id="confirmed-dispatches-table" class="table table-hover da-table" width="100%">
            <thead>
              <tr>
                <th style="width:45px">
                  <center>Sl. No.</center>
                </th>
                <th>
                  Booking Ref.
                </th>
                <th>
                  Schedule Ref.
                </th>
                <th>
                  Schedule Type
                </th>
                <th>
                  Customer
                </th>
                <th>
                  Maid
                </th>
                <th>
                  Date
                </th>
                <th>
                  Time
                </th>
                <th>
                  Status
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (is_array($confirmed_dispatches) && count($confirmed_dispatches) > 0) {
                foreach ($confirmed_dispatches as $key => $row) {
                  if ($row->booking_type == "OD") {
                    $booking_type_name = "One Time";
                  }
                  if ($row->booking_type == "WE") {
                    $booking_type_name = "Regular";
                  }
                  ?>
                  <tr>
                    <td style="line-height: 18px; width: 20px;  text-align:center;">
                      <center><?php echo $key + 1; ?></center>
                    </td>
                    <td><?php echo $row->booking_reference_id ?></td>
                    <td><?php echo $row->day_service_reference_id ?></td>
                    <td><?php echo $booking_type_name ?></td>
                    <td><?php echo $row->customer_name ?></td>
                    <td><?php echo $row->maid_name ?></td>
                    <td><?php echo DateTime::createFromFormat('Y-m-d', $row->service_date)->format('d M Y') ?></td>
                    <td><?=DateTime::createFromFormat('H:i:s', $row->time_from)->format('h:i A')?> to <?=DateTime::createFromFormat('H:i:s', $row->time_to)->format('h:i A')?></td>
                    <td>
                      <span class="btn-block badge badge-success">Confirmed</span>
                    </td>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane" id="keydispatch">
          <?php foreach ($dispatch_keys as $maid_leader_id => $key_data): ?>
            <?php if (sizeof($key_data['maids'])): ?>
              <?php $count++; ?>
              <div id="leader-<?=$maid_leader_id?>">
                <table class="table table-bordered table-hover" width="100%">
                  <thead>
                    <tr style="background-color: #758b93 !important;">
                      <th colspan="3">Charge ~ <?=$key_data['leader']['name'];?> <button href="#" data-action="print-this" data-id="leader-<?=$maid_leader_id?>" class="btn btn-info pull-right"><i class="fa fa-print" aria-hidden="true"></i></button></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th style="width:30%;background-color: #b4b9bb;">Maid</th>
                      <th style="width:35%;background-color: #b4b9bb;">Keys</th>
                      <th style="width:35%;background-color: #b4b9bb;">Cards</th>
                    </tr>
                    <?php foreach ($key_data['maids'] as $key => $maid): ?>
                      <tr>
                        <td><?=$maid['name'];?></td>
                        <td>
                          <?php foreach ($maid['accesses'] as $key2 => $access): ?>
                          <span class="label label-info"><?=$access['customer_access_type'] == "Key" ? $access['customer_access_code'] : "";?></span>
                          <?php endforeach;?>
                        </td>
                        <td>
                          <?php foreach ($maid['accesses'] as $key2 => $access): ?>
                          <span class="label label-info"><?=$access['customer_access_type'] == "Card" ? $access['customer_access_code'] : "";?></span>
                          <?php endforeach;?>
                        </td>
                      </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
              </div>
            <?php endif;?>
          <?php endforeach;?>
          <?php if ($count == 0): ?>
            <div class="text-center">No bookings found !</div>
          <?php endif;?>
        </div>
      </div>
    </div>
    <div class="widget-content">
    </div>
    <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<?php $this->load->view('dispatch/partial-edit-dispatched-schedule-popup'); ?>