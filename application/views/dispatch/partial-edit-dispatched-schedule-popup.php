<div id="edit-dispatched-schedule-popup" style="background:none;display:none;">
<div class="popup-main-box">
  <div class="col-md-12 col-sm-12 green-popup-head">
    <span class="title">View / Edit Dispatched Schedule</span>
    <span class="pop_close n-close-btn">&nbsp;</span>
  </div>
  <form id="dispatch_edit_form">
    <input name="day_service_id" type="hidden">
    <div class="modal-body">
      <div class="controls">
        <div id="bookings-confirm-dispatch-list">
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Customer</p>
                <div class="n-field-box">
                  <input name="customer_name" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Maid</p>
                <div class="n-field-box">
                  <input name="maid_name" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Type</p>
                <div class="n-field-box">
                  <input name="service_type_name" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-3 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>From Time</p>
                <div class="n-field-box">
                  <select name="time_from" data-placeholder="-- Select start time --" class="sel2" style="width:100%">
                  <?php
                  $from_time = DateTime::createFromFormat('H:i:s',"00:00:00");
                  $to_time = DateTime::createFromFormat('H:i:s',"23:59:59");
                  $i= 0;
                  for($time= $from_time;$time<=$to_time;$time=$from_time->modify('+30 minutes')){
                    $time_clone = clone $time;
                    $i++;
                    echo '<option value="'.$time->format('H:i:s').'">'.$time->format('h:i A').'</option>';
                  }
                  ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>To Time</p>
                <div class="n-field-box">
                  <select name="time_to" data-placeholder="-- Select end time --" class="sel2" style="width:100%">
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Hours</p>
                <div class="n-field-box">
                  <input name="working_hours" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-3 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Date</p>
                <div class="n-field-box">
                  <input name="service_date" disabled>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0 mb-3">
            <div class="col-sm-4 n-field-box">
              <p>Service Rate</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Discount Rate</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <div class="n-right-position">/hr</div>
                <input name="service_discount_rate_per_hour" type="number" step="any" autocomplete="off" class="text-center no-arrows"/>
              </label>
            </div>
            <div class="col-sm-4 n-field-box">
              <p>Service Discount</p>
              <label class="position-relative">
                <div class="n-left-position">AED</div>
                <input name="service_discount" type="number" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-field-box">
              <p>Cleaning Material / Cost</p>
              <label class="position-relative">
                <div class="n-left-position">
                  <div class="switch-main">         
                    <label class="switch">
                      <input type="checkbox" name="cleaning_materials" value="Y">
                      <span class="slider round"></span>
                    </label>
                  </div>
                </div>
                <input name="material_fee" type="number" step="any" autocomplete="off" class="text-center no-arrows" readonly/>
              </label>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Service Amount</p>
                <div class="n-field-box">
                  <input name="service_amount" disabled>
                </div>
              </div>
            </div>
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Vat Amount <?= $settings->service_vat_percentage?> %</p>
                <div class="n-field-box">
                  <input name="service_vat_amount" readonly>
                </div>
              </div>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-sm-4 n-form-set-left">
              <div class="row m-0 n-field-main">
                <p>Total Amount</p>
                <div class="n-field-box">
                  <input name="taxed_total" readonly>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-action="close-fancybox">Cancel</button>
      <button type="submit" data-action="" class="n-btn mb-0">Save</button>
    </div>
  </form>
</div>
</div>
