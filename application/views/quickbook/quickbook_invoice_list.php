<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
</style>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Not Synched Invoices</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <div class="col-md-12" id="sync-error-div">
	  
	  </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Close</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Sync ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to sync invoices to quickbooks?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="sync_invoices()">Yes</button>
    </div>
  </div>
</div>
<div id="avatar-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name" class="avatar-popup-name"></span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="avatar-popup-image" src="#">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">

            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>Invoices</h3>
            </li>
			<li>
				<span style="color: #fff;">Customer</span>
				<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="">-- Select Customer --</option>
						<?php
						$url_custid='nil';
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
								$url_custid=$customer_id;
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
				</select>
			</li>
            <!--<li>
				<input type="text"  id="keyword-search" Placeholder="Search customer" style="width: 160px;">


			</li>-->
			<li class="mr-0 float-right">
				<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" onclick="confirm_disable_enable_modal();"> <i class="fa fa-refresh"></i></a> </div>
			</li>

            
			</ul>
			</div>
			<!--<div id="LoadingImage" style="text-align:center;display:none;position:absolute;top:133px;right:0px;width:100%;height:100%;background-position:center;"><img src="<?php echo base_url() ?>img/loader.gif"></div>-->
			<div class="widget-content" style="margin-bottom:30px">
				<table id="quickbookInvoiceslisttable" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl. No.</center></th>
							<th style="">Number</th>
							<th style="">Customer</th>
							<th style="">Mobile</th>
							<th style="">Invoice Date</th>
							<th style="">Amount</th>
							<th style="">Paid Amount</th>
							<th style="">Due Amount</th>
							<th style="">Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>

		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>