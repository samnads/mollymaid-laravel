<?php
//print_r($maids);;
?>
<style>
    .new-radio-sub-box {width: 20% !important;}
    .new-radio-sub-box .radio { margin: 0px 16px 0px 8px; }
    .color-box { padding:  5px 10px; border-radius: 3px; line-height: 20px; display: inline-block;margin: 5px 5px 0px 5px;}
    .table th, .table td { line-height:  normal !important ;}
    .widget-header th h3{color: #fff; display: inline-block; font-size: 14px; font-weight: 800; line-height: 25px; text-shadow: 1px 1px 2px rgba(255, 255, 255, 0.5); left: 0px; position: static !important; margin-right: 0;}
</style>

<table class="table table-striped table-bordered">
    <thead>
        <tr class="widget-header">
            <th><center> <h3 style="top: 10px !important;">Sl.No.</h3></center> </th>
            <th> <center><h3 style="top: 10px !important;">Maid</h3></center></th>
            <th> <center><h3 style="top: 10px !important;">Booking shift</h3></center></th>
            <th> <center><h3 style="top: 10px !important;">Action</h3></center></th>
        </tr>
    </thead>                                               
    <tbody>
        <tr>                                    
            <td></td>
            <td colspan="2">
                <div class="new-radio-sub-box">
                    <input type="radio" class="radio location" name="location_filter" value="area"  checked="checked"> Area
                </div><!--new-form-sub-main end--> 
                <div class="new-radio-sub-box">
                    <input type="radio" class="radio location" name="location_filter" value="zone"> Zone
                </div><!--new-form-sub-main end-->
                <div class="new-radio-sub-box">
                    <input type="radio" class="radio location" name="location_filter"  value="province">Province
                </div><!--new-form-sub-main end-->
                <div class="new-radio-sub-box">
                    <input type="radio" class="radio location" name="location_filter"  value="free">Free
                </div><!--new-form-sub-main end-->
                <div class="new-radio-sub-box">
                    <input type="radio" class="radio location" name="location_filter"  value="all">All
                </div><!--new-form-sub-main end-->
            </td>
            <td></td>
        </tr>                                            
        <?php                                               
        $i=1;
//                                      $flagMaidCount = 0;                           
        foreach($maids as $list)
        {
          if(!in_array($list->maid_id, $leave_maid_ids)) // Maid Leave
           {
            if($new_book_type == 'OD')
            {
                //$shifts=$this->bookings_model->get_free_maid($service_date,$from_time,$to_time,$list->maid_id);
                //latesthide - $shifts=$this->bookings_model->get_free($service_date,$from_time,$to_time,$list->maid_id);
				if(!empty($n_maids))
                {
                    foreach ($n_maids as $n_maid)
                    {
                        if($n_maid == $list->maid_id)
                        {
                            $shifts = 1;
                            break;
                        } else {
                            //$shifts=$this->bookings_model->get_free($service_date,$from_time,$to_time,$list->maid_id);
							$shifts = 0;
                        }
                    }
                } else {
                    //$shifts=$this->bookings_model->get_free($service_date,$from_time,$to_time,$list->maid_id);
                    $shifts = 0;
                }
            } else if($new_book_type == 'WE')
            {
                $shifts=$this->bookings_model->get_free_maid($service_date,$from_time,$to_time,$list->maid_id);
            } else {
                if(!empty($n_maids))
                {
                    foreach ($n_maids as $n_maid)
                    {
                        if($n_maid == $list->maid_id)
                        {
                            $shifts = 1;
                            break;
                        } else {
                            $shifts=$this->bookings_model->get_free_maid_bw($service_date,$from_time,$to_time,$list->maid_id);
                        }
                    }
                } else {
                    $shifts=$this->bookings_model->get_free_maid_bw($service_date,$from_time,$to_time,$list->maid_id);
                    //$shifts = 0;
                }
                
            }                                                 
            if($shifts==0)
            {
                //$flagMaidCount++;
                if($new_book_type == 'OD')
                {
                    //$time=$this->bookings_model->get_shifts_maid($service_date,$list->maid_id);
                    $time=$this->bookings_model->get_shifts_maid_new($service_date,$list->maid_id);
                } else if($new_book_type == 'WE')
                {
                    $time=$this->bookings_model->get_shifts_maid($service_date,$list->maid_id);	
                } else {
					$time=$this->bookings_model->get_shifts_maid($service_date,$list->maid_id);
				}
//                                                                echo "<pre>";
//                                                                print_r($time);
//                                                               echo "</pre>";
                if(count($time) > 0)
				{
					$book_type=explode(",",$time[0]->booking_type);
				} else {
					$book_type="";
				}
                $type_req = $_REQUEST['location_type'];    
                $flagCheckInSameLocation = "0";
				if(count($time) > 0)
				{
					$newtypelistarea = $time[0]->area_id;
					$newtypelistzone = $time[0]->zone_id;
					$newtypelistprovince = $time[0]->province_id;
				} else {
					$newtypelistarea = "";
					$newtypelistzone = "";
					$newtypelistprovince = "";
				}
                switch($type_req):
                    case 'area':
                        $type_list  = $newtypelistarea;
                        $type_id    = $area_id;                                  
                    break;
                    case 'zone':
                        $type_list  = $newtypelistzone;
                        $type_id    = $zone_id;                               
                    break;
                    case 'province':
                        $type_list  = $newtypelistprovince;
                        $type_id    = $province_id;             
                    break;

                endswitch;

                if($type_req == "free") 
                {   // displaying full free maids
                    if(empty($newtypelistarea)) 
                    {
                    ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td>
                            <?php 
                            $path = './maidimg/thumb/'.$list->maid_photo_file;
                            $path2 = './maidimg/'.$list->maid_photo_file;
                            if (file_exists($path))
                            {
                                $imgpath1 = base_url() . 'maidimg/thumb/' . html_escape($list->maid_photo_file);
                            } elseif (file_exists($path2))
                            {
                                $imgpath1 = base_url() . 'maidimg/' . html_escape($list->maid_photo_file);
                            } else {
                                $imgpath1 = base_url() . 'img/no_image.jpg';
                            }
                            ?>
                            <?php if(!empty($list->maid_photo_file))
                            { ?>
                                <!--<img src="<?php// echo base_url() ?>maidimg/<?php// echo $list->maid_photo_file ?>" width="50" height="50">-->
                                <img src="<?php echo $imgpath1; ?>" style="width:150px;height:150px;">
                            <?php } else { ?>
                                <img src="<?php echo base_url() ?>img/no_image.jpg" style="width:150px;height:150px;">
                            <?php } ?>
                            &nbsp;&nbsp;
                            <span id="name_<?php echo $list->maid_id  ?>"><?php echo $list->maid_name ?></span>
                        </td>
                        <td>
                            <span class="color-box" style='background:#00A642; padding:5px 27px 3px;color:#fff;' data-area="<?php echo $list->area_id ?>" >Free</span>
                        </td>
                        <td>
                            <input type='button' class='save-but book_maid_list' id='btn-book-maid_<?php echo $list->maid_id ?>' value='Book'/>
                        </td>
                    </tr> 
                    <?php
                    $i++;
                    }
                } else {
                    $type = explode(",", trim($type_list));
                    if (in_array($type_id, $type))
                    {
                        $flagCheckInSameLocation = "1";
                    }
                    else {
                        $flagCheckInSameLocation = "0";
                    }
                    if($flagCheckInSameLocation == "1") 
                    {
                    ?>

        <tr>
            <td><?php echo $i ?></td>
            <td>
            <?php 
            $path = './maidimg/thumb/'.$list->maid_photo_file;
            $path2 = './maidimg/'.$list->maid_photo_file;
            if (file_exists($path)) {
            $imgpath1 = base_url() . 'maidimg/thumb/' . html_escape($list->maid_photo_file);
            } elseif (file_exists($path2)){
            $imgpath1 = base_url() . 'maidimg/' . html_escape($list->maid_photo_file);
            } else {
            $imgpath1 = base_url() . 'img/no_image.jpg';
            }
            ?>
            <?php if(!empty($list->maid_photo_file)) : ?>
            <!--<img src="<?php echo base_url() ?>maidimg/<?php echo $list->maid_photo_file ?>" width="50" height="50">-->
            <img src="<?php echo $imgpath1; ?>" style="width:150px;height:150px;">
            <?php else : ?>

            <img src="<?php echo base_url() ?>img/no_image.jpg" style="width:150px;height:150px;">
            <?php endif; ?>
            &nbsp;&nbsp;
            <span id="name_<?php echo $list->maid_id  ?>"><?php echo $list->maid_name ?></span>
            </td>
            <td>

<?php 
$k=0;

foreach($book_type as $val) {
	if(count($time) > 0)
	{
		$shift_time_shifts = $time[0]->shifts;
		$area_area_id = $time[0]->area_id;
		$area_name_area_name = $time[0]->area_name;
	} else {
		$shift_time_shifts = "";
		$area_area_id = "";
		$area_name_area_name = "";
	}
$btype=trim($val);
$shift_time=explode(",",$shift_time_shifts);
$area=explode(",",$area_area_id);
$area_name=explode(",",$area_name_area_name);
if($btype=="WE")
{
?>    
<span class="color-box bg-we" data-area="<?php echo $area[$k] ?>" >
<?php echo $shift_time[$k];?><br>
<?php echo $area_name[$k];?>

</span> 

<?php
}
else if($btype=="OD")
{

?>
<span class="color-box bg-od" data-area="<?php echo $area[$k] ?>" >
<?php echo $shift_time[$k] ;?><br>
<?php echo $area_name[$k];?>
</span>

<?php
}
else{
?>   
<span class="color-box" style='background:#00A642; padding:5px 27px 3px;color:#fff;' data-area="<?php echo $list->area_id ?>" >
Free
</span>
<?php
}

$k++;
} 
?>     

</td>
<td><input type='button' class='save-but book_maid_list' id='btn-book-maid_<?php echo $list->maid_id ?>' value='Book'/></td>
</tr>
<?php 

$i++;           

}


} // location filter ends                          



}              

           }

}

if($i == 1){ 
?>  <tr><td colspan="4" style="text-align: center;color: #f00; padding: 20px 0px;font-weight: bold;">No Maids available in your search criteria ! </td></tr> <?php         
}                                                                                            
?>


</tbody>
</table>



