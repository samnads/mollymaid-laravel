<div class="row m-0">
    <div class="col-md-12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Edit Maid</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url(); ?>maid/maid_list"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url(); ?>maid/maid_list"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Maid List"/></a></div>
            </div> <!-- /widget-header -->
            <?php
            if (count($maid_details) > 0) {
                foreach ($maid_details as $maid_val) {
                    ?>  
                    <div class="widget-content">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                                <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->

                            </ul>
                            <br>
                            <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">      
                                <div class="tab-content">
                                    <div class="tab-pane active" id="personal">   
                                        
                                        <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Maid Details Updated Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                        
                                        
                                        
                                        <fieldset>            
                                            <div class="span5">
                                                <div class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Maid Name &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="maid_name" name="maid_name" value="<?php echo $maid_val['maid_name'] ?>">
                                                                    <input type="hidden" class="span3" id="maid_id" name="maid_id" value="<?php echo $maid_val['maid_id'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->				

                                                            <div class="control-group">											
                                                                <label class="control-label">Gender &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <label class="radio inline">
                                                                        <input type="radio" name="gender" id="gender" value="M" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "M" ? 'checked="checked"' : '') : '' ?>>
                                                                        Male
                                                                    </label>
                                                                    <label class="radio inline">
                                                                        <input type="radio" name="gender" id="gender" value="F" <?php echo isset($maid_val['maid_gender']) ? ($maid_val['maid_gender'] == "F" ? 'checked="checked"' : '') : '' ?>>
                                                                        Female
                                                                    </label>
                                                                </div>	<!-- /controls -->			
                                                            </div> <!-- /control-group -->


                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Nationality &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="nationality" name="nationality" value="<?php echo $maid_val['maid_nationality'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->		

                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Current Photo</label>
                                                                <div class="controls">
                                                                    <!--                                                                    <div id="me" class="styleall" style=" cursor:pointer;"> -->
                                                                    <span style=" cursor:pointer;"> 
                                                                        <?php
                                                                        if ($maid_val['maid_photo_file'] == "") {
                                                                            $image = base_url() . "img/no_image.jpg";
                                                                        } else {
                                                                            $image = base_url() . "maidimg/" . $maid_val['maid_photo_file'];
                                                                        }
                                                                        ?>

                                                                        <img src="<?php echo $image; ?>" style="height: 100px; width: 100px"/> 
                                                                        <input type="hidden" name="old_image" id="old_image" value="<?php echo $maid_val['maid_photo_file'] ?>"/>
                                                                    </span> 
                                                                    <!--                                                                    </div> -->
                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Change Photo</label>
                                                                <div class="controls">
                                                                    <div id="me" class="styleall" style=" cursor:pointer;"> 
                                                                        <span style=" cursor:pointer;">                             
                                                                            <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> 
                                                                        </span> 
                                                                    </div> 
                                                                </div>
                                                            </div>    

                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Present Address &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <textarea class="span3" rows="5" id="present_address" name="present_address"><?php echo $maid_val['maid_present_address'] ?></textarea>	
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Permanent Address &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <textarea class="span3" rows="5" id="permanent_address" name="permanent_address"><?php echo $maid_val['maid_permanent_address'] ?></textarea>	
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->



                                                        </fieldset>

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->




                                            <div class="span5">
                                                <div id="target-2" class="widget">
                                                    <div class="widget-content" style="border: 0px">


                                                        <div class="control-group">											
                                                            <label class="control-label" for="firstname">Mobile Number 1 &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="mobile1" name="mobile1" value="<?php echo $maid_val['maid_mobile_1'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->


                                                        <div class="control-group">											
                                                            <label class="control-label" for="firstname">Mobile Number 2</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="mobile2" name="mobile2" value="<?php echo $maid_val['maid_mobile_2'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->


                                                        <div class="control-group">
                                                            <label class="control-label" for="basicinput">Flat &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <select name="flat" id="flat" class="span3" required >
                                                                    <?php
                                                                    if (count($flats) > 0) {
                                                                        foreach ($flats as $flatsVal) {
                                                                            if ($flatsVal['flat_id'] == $maid_val['flat_id'])
                                                                                $selected = "selected";
                                                                            else
                                                                                $selected = "";
                                                                            ?>
                                                                            <option value="<?php echo $flatsVal['flat_id'] ?>" <?php echo $selected; ?>><?php echo $flatsVal['flat_name'] ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>




                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <!--<div class="control-group">
                                                            <label class="control-label" for="basicinput">Team &nbsp;</label>
                                                            <div class="controls">
                                                                <select name="team" id="team" class="span3">
                                                                    <option value="">Select Team</option>
                                                                    <?php
                                                                    // if (count($teams) > 0) {
                                                                        // foreach ($teams as $teamsVal) {
                                                                            // if ($teamsVal->team_id == $maid_val['team_id'])
                                                                                // $selected = "selected";
                                                                            // else
                                                                                // $selected = "";
                                                                            // ?>
                                                                            // <option value="<?php// echo $teamsVal->team_id ?>" <?php// echo $selected; ?>><?php// echo $teamsVal->team_name; ?></option>
                                                                            // <?php
                                                                        // }
                                                                    // }
                                                                    ?>




                                                                </select>
                                                            </div>
                                                        </div>-->

                                                        <div class="control-group">											
                                                            <label class="control-label">Services &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <?php
                                                                if (count($services) > 0) {
                                                                    foreach ($services as $serviceVal) {
                                                                        ?>
                                                                        <label class="checkbox">


                                                                            <input type="checkbox" class="services" name="services[]" value="<?php echo $serviceVal['service_type_id'] ?>" <?php echo (in_array($serviceVal['service_type_id'], $service_maid)) ? "checked='checked'" : ''; ?>> <?php echo $serviceVal['service_type_name'] ?>
                                                                        </label>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </div><!-- /controls -->		
                                                        </div> <!-- /control-group -->


                                                        <div class="control-group">											
                                                            <label class="control-label" for="firstname">Notes</label>
                                                            <div class="controls">
                                                                <textarea class="span3" rows="5" id="notes" name="notes"><?php echo $maid_val['maid_notes'] ?></textarea>	
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
														<div class="control-group">											
                                                            <label class="control-label" for="username">Username</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="username" name="username" value="<?php echo $maid_val['username'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
														<div class="control-group">											
                                                            <label class="control-label" for="password">Password</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="password" name="password" value="<?php echo $maid_val['password'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
														
														<div class="control-group">
                                                            <label class="control-label" for="basicinput">App Login Status &nbsp;<!--<font style="color: #C00">*</font>--></label>
                                                            <div class="controls">
                                                                <select name="appstatus" id="appstatus" class="span3">
                                                                    <option value="0" <?php echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "0" ? 'selected="selected"' : '') : '' ?>>Inactive</option>
                                                                    <option value="1" <?php echo isset($maid_val['maid_login_status']) ? ($maid_val['maid_login_status'] == "1" ? 'selected="selected"' : '') : '' ?>>Active</option>
																</select>
                                                            </div>
                                                        </div>

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span5 -->

                                        </fieldset>   
                                    <div class="form-actions" style="padding-left: 211px;">

                                            <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload"/>
                                            <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                            <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            <input type="submit" class="btn mm-btn pull-right" value="Submit" name="maid_edit" onclick="return validate_maid();">

                                        </div>   
                                    </div>

                                    <div class="tab-pane" id="attachments">
                                        <fieldset>            

                                            <div class="span5">
                                                <div class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Passport Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="passport_number" name="passport_number" value="<?php echo $maid_val['maid_passport_number'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->
                                                            <?php
                                                            if ($maid_val['maid_passport_expiry_date'] != '' && $maid_val['maid_passport_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_passport_expiry_date']);
                                                                $passport_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_passport_expiry_date'] = '0000-00-00') {
                                                                $passport_expiry = "";
                                                            }
                                                            ?>
                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Passport Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="passport_expiry" name="passport_expiry" value="<?php echo $passport_expiry ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            
                                                            
                                                            
                                                            <?php
                                                            if ($maid_val['maid_passport_file'] != '') {
                                                                
                                                                 $passport_image = base_url() . "maid_passport/" . $maid_val['maid_passport_file'];
                                                                
                                                                ?>

                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $passport_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_passport" name="old_attach_passport" value="<?php echo $maid_val['maid_passport_file'] ?>">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->


                                                                <?php
//                                                                
                                                            } else {
                                                                $passport_image = base_url() . "img/no_image.jpg";
                                                                
                                                                ?>
                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $passport_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_passport" name="old_attach_passport" value="">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->

                                                                <?php
                                                            }
                                                            ?>    


                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Attach Passport</label>
                                                                <div class="controls">
                                                                    <input type="file" class="span3" id="attach_passport" name="attach_passport">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <div class="control-group">											
                                                                <label class="control-label" for="email">Visa Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="visa_number" name="visa_number" value="<?php echo $maid_val['maid_visa_number'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <?php
                                                            if ($maid_val['maid_visa_expiry_date'] != '' && $maid_val['maid_visa_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_visa_expiry_date']);
                                                                $visa_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_visa_expiry_date'] = '0000-00-00') {
                                                                $visa_expiry = "";
                                                            }
                                                            ?>

                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Visa Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="visa_expiry" name="visa_expiry" value="<?php echo $visa_expiry ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <?php
                                                            if ($maid_val['maid_visa_file'] != '') {
                                                                
                                                                $visa_image = base_url() . "maid_visa/" . $maid_val['maid_visa_file'];
                                                                
                                                                ?>

                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_visa" name="old_attach_visa" value="<?php echo $maid_val['maid_visa_file'] ?>">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->


                                                                <?php
//                                                                $maid_visa_file = $maid_val['maid_visa_file'];
                                                            } else {
                                                                
                                                                $visa_image = base_url() . "img/no_image.jpg";
                                                                
                                                                ?>
                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_visa" name="old_attach_visa" value="">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->

                                                                <?php
                                                            }
                                                            ?>                                                           

                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Attach Visa</label>
                                                                <div class="controls">
                                                                    <input type="file" class="span3" id="attach_visa" name="attach_visa">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->


                                                        </fieldset>

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->


                                            <div class="span5">
                                                <div id="target-2" class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <div class="control-group">											
                                                                <label class="control-label" for="email">Labour Card Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="labour_number" name="labour_number" value="<?php echo $maid_val['maid_labour_card_number'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->


                                                            <?php
                                                            if ($maid_val['maid_labour_card_expiry_date'] != '' && $maid_val['maid_labour_card_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_labour_card_expiry_date']);
                                                                $labour_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_labour_card_expiry_date'] = '0000-00-00') {
                                                                $labour_expiry = "";
                                                            }
                                                            ?>                                                           

                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Labour Card Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="labour_expiry" name="labour_expiry" value="<?php echo $labour_expiry ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->


                                                            <?php
                                                            if ($maid_val['maid_labour_card_file'] != '') {
                                                                
                                                                
                                                                $labour_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
                                                                
                                                                
                                                                ?>

                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_labour" name="old_attach_labour" value="<?php echo $maid_val['maid_labour_card_file'] ?>">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->


                                                                <?php
//                                                                
                                                            } else {
                                                                
                                                                
                                                                $labour_image = base_url() . "img/no_image.jpg";
                                                                
                                                                
                                                                ?>
                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $visa_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_labour" name="old_attach_labour" value="">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->

                                                                <?php
                                                            }
                                                            ?>  





                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Attach Labour Card</label>
                                                                <div class="controls">
                                                                    <input type="file" class="span3" id="attach_labour" name="attach_labour" >
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <div class="control-group">											
                                                                <label class="control-label" for="email">Emirates Id</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="emirates_id" name="emirates_id" value="<?php echo $maid_val['maid_emirates_id'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <?php
                                                            if ($maid_val['maid_emirates_expiry_date'] != '' && $maid_val['maid_emirates_expiry_date'] != '0000-00-00') {
                                                                list($year, $month, $day) = explode("-", $maid_val['maid_emirates_expiry_date']);
                                                                $emirates_expiry = "$day/$month/$year";
                                                            } else if ($maid_val['maid_emirates_expiry_date'] = '0000-00-00') {
                                                                $emirates_expiry = "";
                                                            }
                                                            ?>                                                             


                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Emirates Id Expiry</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="emirates_expiry" name="emirates_expiry" value="<?php echo $emirates_expiry ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->



                                                            <?php
                                                            if ($maid_val['maid_emirates_file'] != '') {
                                                                
                                                                $emirates_image = base_url() . "maid_labour/" . $maid_val['maid_labour_card_file'];
                                                                
                                                                ?>

                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_emirates" name="old_attach_emirates" value="<?php echo $maid_val['maid_emirates_file'] ?>">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->


                                                                <?php
//                                                                
                                                            } else {
                                                                
                                                                $emirates_image = base_url() . "img/no_image.jpg";
                                                                
                                                                ?>
                                                                <div class="control-group">											
                                                                    <label class="control-label" for="firstname">Current Attachment</label>
                                                                    <div class="controls">
                                                                        <span><img src="<?php echo $emirates_image; ?>" style="height: 100px; width: 100px"/></span>
                                                                        <input type="hidden" class="span3" id="old_attach_emirates" name="old_attach_emirates" value="">
                                                                    </div> <!-- /controls -->				
                                                                </div> <!-- /control-group -->

                                                                <?php
                                                            }
                                                            ?>  







                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Attach Emirates Card</label>
                                                                <div class="controls">
                                                                    <input type="file" class="span3" id="attach_emirates" name="attach_emirates">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                            <br />
                                                        </fieldset>
                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span5 -->

                                        </fieldset>            

<!--                                        <div class="form-actions" style="padding-left: 211px;">

                                            <input type="hidden" name="call_method" id="call_method" value="maid/editmaidimgupload"/>
                                            <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                            <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            <input type="submit" class="btn btn-primary pull-right" value="Submit" name="maid_edit" onclick="return validate_maid();">

                                        </div>   -->


                                    </div>
                                </div>	
                            </form>
                        </div>
                    </div> <!-- /widget-content -->	
                    <?php
                }
            }
            ?> 
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->