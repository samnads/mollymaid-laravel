<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style>
    table thead tr th table th{
        text-transform: none;
        padding: 5px;
    }
    table td{
        border-right: 1px solid #000 !important;
        border-bottom: 1px solid #000 !important;
        padding: 5px;
    }
    .main-inner .container .dash-top-wrapper table colgroup + thead tr:first-child th, table colgroup + thead tr:first-child td, table thead:first-child tr:first-child th, table thead:first-child tr:first-child td{
        border-top: 1px solid #000 !important;
    }
    
    table tbody tr:first-child{
        border-left: 1px solid #000 !important;
    }
    
    table{font-size: 9px;}
    
    /*source color*/
.colorod{ background: #1ca99e;}
.colorwe{ background: #42dbe0;}
.colorbw{ background: #b39bd4;}
.helpling {
	background:rgb(255, 192, 0);
}
.justmop {
	background:rgb(177, 160, 199);
}
.justmopod {
	background:rgb(230, 184, 183);
}
.helpersquad {
	background:rgb(255, 51, 153);
}
.bookservice {
	background:rgb(196, 189, 151);
}
.helpbit {
	background:rgb(0, 176, 240);
}
.mrusta {
	background:rgb(218, 150, 148);
}
.spectrum {
	background:rgb(146, 208, 80);
}
.rose {
	background:rgb(237, 127, 34);
}

.jane {
	background:rgb(103, 64, 140);
}
.other {
    background:white;
}
/*    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }*/
</style>
<table id="" class="" cellspacing="0" width="100%">
    <thead>
        <tr>
            <td> <center>Sl.No</center></td>
            <td> <center><b>Cleaner Name</b></center></td>
            <?php 
            $i = 0;
            for ($i=0; $i < 3; $i++)
            {
            ?>


                    <td><b>Total Timings</b></td>
                    <td><b>Client</b></td>
					<td><b>Reference</b></td>
                    <td><b>Timing</b></td>
                    <td><b>Location</b></td>
                    <td><b>Mat</b></td>
                    <td><b>Payment</b></td>
                    <td><b>Payment mode</b></td>
                    <td><b>Driver</b></td>


            <?php
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $j = 1;
        foreach ($maids as $maids_val) {
        ?>
        <tr>
            <td><?php echo $j; ?></td>
            <td><b><?php echo $maids_val->maid_name; ?></b></td>
            <?php
            //$getbookingsbymaid_id = $this->reports_model->getbookingsbymaid_id(date('Y-m-d'),$maids_val['maid_id']);
            $veh = $this->reports_model->getbookingsbymaid_id($date_from_job,$maids_val->maid_id,$zone_id);

            if(count($veh) == 1)
								{
									if($veh[0]['booking_type'] == "OD")
									{
										$zonecolor = "colorod";
									} else if($veh[0]['booking_type'] == "WE"){
										$zonecolor = "colorwe";
									} else {
										$zonecolor = "colorbw";
									}
									//Payment Type
									if($veh[0]['customer_paytype'] == "D")
									{
										$paytype = "(D)";
									} else if($veh[0]['customer_paytype'] == "W")
									{
										$paytype = "(W)";
									} else if($veh[0]['customer_paytype'] == "M")
									{
										$paytype = "(M)";
									} else
									{
										$paytype = "";
									}
									$t_shrt = $veh[0]['time_to']; 
									$cur_shrt = $veh[0]['time_from'];
									$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
									
									if($veh[0]['cleaning_material'] == 'Y')
									{
										$mat = "Yes";
									} else {
										$mat = "No";
									}
									
									$check_buk_exist = $this->bookings_model->get_booking_exist($veh[0]['booking_id'],$date_from_job);
									if(!empty($check_buk_exist))
									{
										$mop = $check_buk_exist->mop;
										$ref = $check_buk_exist->just_mop_ref;
									} else {
										if($veh[0]['pay_by'] != "")
										{
											$mop = $veh[0]['pay_by'];
											$ref = $veh[0]['justmop_reference'];
										} else {
											$mop = $veh[0]['payment_mode'];
											$ref = $veh[0]['justmop_reference'];
										}
									}
									//if(($veh[0]['time_from'] >= '05:00:00') && ($veh[0]['time_from'] <= '12:00:00'))
									if($veh[0]['time_from'] >= '12:00:00')
									{
										?>
										<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[0]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[0]['time_to']));?> </td>
<!--                                    <td class="<?php// echo $zonecolor; ?>"><?php// echo date("g:i A", strtotime($veh[$i]['time_from']));?> &nbsp; - &nbsp;
										<?php// echo date("g:i A", strtotime($veh[$i]['time_to']));?> </td>-->
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['area_name']; ?> / <?php echo $veh[0]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[0]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
										
								
										
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
										<?php
									} else {
										$i = 0;
										for ($i=0; $i < 3; $i++)
										{
											if(!empty($veh[$i]))
											{
												if($veh[$i]['booking_type'] == "OD")
												{
													$zonecolor = "colorod";
												} else if($veh[$i]['booking_type'] == "WE"){
													$zonecolor = "colorwe";
												} else {
													$zonecolor = "colorbw";
												}
												//Payment Type
												if($veh[$i]['customer_paytype'] == "D")
												{
													$paytype = "(D)";
												} else if($veh[$i]['customer_paytype'] == "W")
												{
													$paytype = "(W)";
												} else if($veh[$i]['customer_paytype'] == "M")
												{
													$paytype = "(M)";
												} else
												{
													$paytype = "";
												}
												
												$t_shrt = $veh[$i]['time_to']; 
												$cur_shrt = $veh[$i]['time_from'];
												$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
												
												if($veh[$i]['cleaning_material'] == 'Y')
												{
													$mat = "Yes";
												} else {
													$mat = "No";
												}
												
												$check_buk_exist = $this->bookings_model->get_booking_exist($veh[$i]['booking_id'],$date_from_job);
												if(!empty($check_buk_exist))
												{
													$mop = $check_buk_exist->mop;
													$ref = $check_buk_exist->just_mop_ref;
												} else {
													if($veh[$i]['pay_by'] != "")
													{
														$mop = $veh[$i]['pay_by'];
														$ref = $veh[$i]['justmop_reference'];
													} else {
														$mop = $veh[$i]['payment_mode'];
														$ref = $veh[$i]['justmop_reference'];
													}
												}
												?>
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[$i]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[$i]['time_to']));?> </td>
<!--                                    <td class="<?php// echo $zonecolor; ?>"><?php// echo date("g:i A", strtotime($veh[$i]['time_from']));?> &nbsp; - &nbsp;
										<?php// echo date("g:i A", strtotime($veh[$i]['time_to']));?> </td>-->
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['area_name']; ?> / <?php echo $veh[$i]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
												<?php
											} else { ?>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td> 
								<td class="other"></td>
										<?php }
										}
									}
								} else {
									$i = 0;
									for ($i=0; $i < 3; $i++)
									{
										if(!empty($veh[$i]))
										{
											if($veh[$i]['booking_type'] == "OD")
											{
												$zonecolor = "colorod";
											} else if($veh[$i]['booking_type'] == "WE"){
												$zonecolor = "colorwe";
											} else {
												$zonecolor = "colorbw";
											}
											//Payment Type
											if($veh[$i]['customer_paytype'] == "D")
											{
												$paytype = "(D)";
											} else if($veh[$i]['customer_paytype'] == "W")
											{
												$paytype = "(W)";
											} else if($veh[$i]['customer_paytype'] == "M")
											{
												$paytype = "(M)";
											} else
											{
												$paytype = "";
											}
												
											$t_shrt = $veh[$i]['time_to']; 
											$cur_shrt = $veh[$i]['time_from'];
											$hours = ((strtotime($t_shrt) - strtotime($cur_shrt))/3600);
											
											if($veh[$i]['cleaning_material'] == 'Y')
											{
												$mat = "Yes";
											} else {
												$mat = "No";
											}
											$check_buk_exist = $this->bookings_model->get_booking_exist($veh[$i]['booking_id'],$date_from_job);
											if(!empty($check_buk_exist))
											{
												$mop = $check_buk_exist->mop;
												$ref = $check_buk_exist->just_mop_ref;
											} else {
												if($veh[$i]['pay_by'] != "")
												{
													$mop = $veh[$i]['pay_by'];
													$ref = $veh[$i]['justmop_reference'];
												} else {
													$mop = $veh[$i]['payment_mode'];
													$ref = $veh[$i]['justmop_reference'];
												}
											}
										?>
								<td class="<?php echo $zonecolor; ?>"><?php echo $hours; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['customer_name']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $ref; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo date("g:i", strtotime($veh[$i]['time_from']));?>-
										<?php echo date("g:i", strtotime($veh[$i]['time_to']));?> </td>
<!--                                    <td class="<?php// echo $zonecolor; ?>"><?php// echo date("g:i A", strtotime($veh[$i]['time_from']));?> &nbsp; - &nbsp;
										<?php// echo date("g:i A", strtotime($veh[$i]['time_to']));?> </td>-->
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['area_name']; ?> / <?php echo $veh[$i]['customer_address'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mat; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['total_amount']; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $mop; ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['driver_name'] ?></td>
								<td class="<?php echo $zonecolor; ?>"><?php echo $veh[$i]['booking_note'] ?></td>
												<?php
										} else { ?>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td>
								<td class="other"></td> 
								<td class="other"></td>
										<?php
										}
									}
								}
							$j++;		
							}
							?>
        </tr>
        

    </tbody>
</table>