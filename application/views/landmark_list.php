<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }
</style>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this landmark ?</h3>
      <input type="hidden" id="delete_areaid">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this area ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="disable-all-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable All ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable all locations ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable_all()">Disable All</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this location ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div id="enable-all-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable All ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable all locations ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable_all()">Enable All</button>
    </div>
  </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Landmark</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form" onsubmit="return validateForm()">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Landmark Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="landmarkname" autocomplete="off" id="landmarkname">
              <span id="username-error" style="color: red;"></span>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Area Name</p>
            <span id="area-error" style="color: red;"></span>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="area_id" id="area_id" class="sel2" style="width:100%" onchange="loadLocations()">
                <option value="">-- Select Area--</option> <?php
                                                            if (count($areas) > 0) {
                                                              foreach ($areas as $areas_val) {
                                                            ?> <option value="<?php echo $areas_val['area_id']; ?>"> <?php echo $areas_val['area_name']; ?> </option> <?php
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                      ?>
              </select>
            </div>
          </div>

          <div class="row m-0 n-field-main">
            <p>Location Name</p>
            <span id="location-error" style="color: red;"></span>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="location_id" id="location_id" class="sel2" style="width:100%">
                <option value="">-- Select Location--</option>
                <!-- <?php
                      if (count($locations) > 0) {
                        foreach ($locations as $areas_val) {
                      ?> <option value="<?php echo $areas_val['location_id']; ?>"> <?php echo $areas_val['location_name']; ?> </option> <?php
                                                                                                                                      }
                                                                                                                                    }
                                                                                                                                        ?> -->
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="description" autocomplete="off">
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn m-0" value="Submit" name="landmark_sub">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span class="spanheading" id="b-maid-name">Edit Landmark</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="" onsubmit="return validateFormEdit()">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Landmark Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_landmarkname" id="edit_landmarkname" autocomplete="off">
              <input type="hidden" class="span3" id="edit_landmarkid" name="edit_landmarkid">
              <span id="username-errorr" style="color: red;"></span>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Area Name</p>
            <span id="area-errorr" style="color: red;"></span>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="edit_area_id" id="edit_area_id" class="sel2" style="width:100%" onchange="loadLocationsedit()">
                <option value="">-- Select Area --</option> <?php
                                                            if (count($areas) > 0) {
                                                              foreach ($areas as $areas_val) {
                                                            ?> <option value="<?php echo $areas_val['area_id']; ?>"> <?php echo $areas_val['area_name']; ?> </option> <?php
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                      ?>
              </select>
            </div>
          </div>

          <div class="row m-0 n-field-main">
            <p>Location Name</p>
            <span id="location-errorr" style="color: red;"></span>
            <div class="col-sm-12 p-0 n-field-box">
              <select name="edit_location_id" id="edit_location_id" class="sel2" style="width:100%">
                <option value="">-- Select Location --</option> <?php
                                                                // if (count($locations) > 0) {
                                                                //     foreach ($locations as $areas_val) {
                                                                //         
                                                                ?> <option value="<?php echo $areas_val['location_id']; ?>"> <?php echo $areas_val['location_name']; ?> </option> <?php
                                                                                                                                                                                  // }}
                                                                                                                                                                                  // 
                                                                                                                                                                                  ?>
              </select>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Description</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_description" id="edit_description" autocomplete="off">
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer" id="landamark_footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="landmark_edit">Save</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <ul>
          <li><i class="icon-th-list"></i>
            <h3>Landmark List</h3>
          </li>

          <li><select style="width:160px;" id="all_location" onchange="activefilter(this.value)">
              <option value="" <?php echo empty($status) ? 'selected' : ''; ?>>Select</option>
              <option value="0" <?php echo $status == '0' ? 'selected' : ''; ?>>Active</option>
              <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Inactive</option>
            </select></li>

          <li>

          </li>

          <li>

          </li>
          <li class="mr-0 float-right">



            <div class="topiconnew border-0 green-btn">
              <a onclick="newPopup()" title="Add"> <i class="fa fa-plus"></i></a>
            </div>

          </li>

      </div>
      <style>
        table.da-table tr td {
          padding: 0px 6px;
        }
      </style>
      <!-- /widget-header -->
      <div class="widget-content">
        <table id="area-list-table" class="table table-hover da-table" cellspacing="0" width="100%">
          <!--                <table class="table table-striped table-bordered">-->
          <thead>
            <tr>
              <th style="width: 45px;  text-align:center;">Sl. No.</th>
              <th style="">Landmark </th>
              <th style="">Area</th>
              <th style="">Location</th>
              <th style="">Zone</th>
              <th style="">Description</th>

              <th style="width: 120px" class="td-actions">Actions</th>

            </tr>
          </thead>
          <tbody>
            <?php
            if (count($landmarks) > 0) {
              $i = 1;
              foreach ($landmarks as $areas_val) {
                //print_r($areas_val);die;
            ?>
                <tr>
                  <td class="text-center" style=""><?php echo $i; ?></td>
                  <td style="">
                    <?php echo $areas_val['landmark_name'] ?>
                  </td>
                  <td style=""><?php echo $areas_val['area_name'] ?></td>
                  <td style="">
                    <?php echo $areas_val['location_name'] ?>
                  </td>
                  <td style="">
                    <?php echo $areas_val['zone_name'] ?>
                  </td>
                  <td style="">
                    <?php echo $areas_val['description'] ?>
                  </td>

                  <td style="" class="td-actions">
                    <a href="javascript:;" class="n-btn-icon green-btn" data-action="view-landmark-popup" data-id="<?= $areas_val['landmark_id'] ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_landmark_get(<?php echo $areas_val['landmark_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                    <!-- <?php if (user_authenticate() == 1) { ?>
                      <a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $areas_val['landmark_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                    <?php } ?> -->

                    <a href="javascript:void(0)" class="n-btn-icon green-btn" style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);" title="" onclick="confirm_disable_enable_modal(<?php echo $areas_val['landmark_id'] ?>,'<?php echo $areas_val['deleted_at'] ?>');">
                      <?php echo $areas_val['deleted_at'] == null ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?>
                    </a>
                    <!-- <a href="javascript:void(0)"  style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);border-radius: 15px;" title="Disable Web Status" onclick="disable_webstatus(this, <?php echo $areas_val['area_id'] ?>, <?php echo $areas_val['web_status'] ?>);"><?php echo $areas_val['web_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?></a> -->
                    <!-- <a href="javascript:void(0)" class="n-btn-icon green-btn"
                                            style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);"
                                            title=""
                                            onclick="confirm_disable_enable_modal(<php echo $areas_val['landmark_id'] ?>,<php echo $areas_val['web_status'] ?>);">
                                            <php echo $areas_val['web_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?>
                                        </a> -->
                    <!-- <php if ($areas_val['web_status'] == 1) {
                          ?>
                                                                                                <a href="javascript:;" class="btn btn-success btn-small" onclick="areaweb_status(<php echo $areas_val['area_id'] ?>,<php echo $areas_val['web_status'] ?>);"><i class="btn-icon-only icon-ok"> </i></a>
                                                                                        <php
                                                                                      } else {
                                                                                        ?>
                                                                                                <a href="javascript:;" class="btn btn-danger btn-small" onclick="areaweb_status(<php echo $areas_val['area_id'] ?>,<php echo $areas_val['web_status'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
                                                                                            <php
                                                                                          }
                                                                                            ?> -->

                  </td>

                </tr>
            <?php
                $i++;
              }
            }
            ?>

          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div><!-- /span5 -->
</div>
<script>
  /*********************************************************************************** */
  function reloadPage() {
    //$.fancybox.close();
    window.location.reload();
  }
  /*********************************************************************************** */
  function disable_webstatus($this, area_id, web_status) {
    // alert(area_id);
    // alert(web_status); die;
    var _lblstatus = web_status == 1 ? 'disable' : 'enable';
    if (confirm('Are you sure you want to ' + _lblstatus + ' the Web status')) {
      $.ajax({
        type: "POST",
        url: _base_url + "settings/remove_area_click",
        data: {
          area_id: area_id,
          web_status: web_status
        },
        dataType: "text",
        cache: false,
        success: function(result) {
          //window.location.assign(_base_url + 'customers');
          window.location.reload();
          if (result == 1) {
            $($this).attr('class', 'btn btn-success btn-small');
            $($this).html('<i class="btn-icon-only icon-ok"></i>');
          } else {
            if (result == 'exist_bookings') {
              alert('Warning! Can\'t deactivate this customer, have some active bookings.');
              result = 0;
            } else {
              $($this).attr('class', 'btn btn-danger btn-small');
              $($this).html('<i class="btn-icon-only icon-remove"> </i>');
            }
          }
          $($this).attr('onclick', 'delete_customer(this, ' + area_id + ', ' + result + ')');
        }
      });
    }
  }
  var disable_areaid = null;
  var web_status = null;
  /*********************************************************************************** */
  var dataTable;

  $(document).ready(function() {
    dataTable = $('#area-list-table').DataTable({
      'sPaginationType': "full_numbers",
      'bSort': true,
      'scrollY': true,
      'orderMulti': false,
      'bFilter': true,
      'lengthChange': true,
      'pageLength': 100,
      'columnDefs': [{
        'targets': [5],
        'orderable': false
      }],
      'language': {
        'zeroRecords': 'No location data found'
      }
    });

    $('#all_location').on('change', function() {
      var status = $(this).val();
      activefilter(status);
    });
  });

  function activefilter(value) {
    if (value === "") {
        $("tbody tr").show();
    } else {
        $("tbody tr").hide();

        $("tbody tr").each(function() {
            var status = $(this).find("i.fa-toggle-on").length > 0 ? "0" : "1";
            if (status === value) {
                $(this).show();
            }
        });
    }
}


  /*********************************************************************************** */
  function confirm_delete_modal(landmark_id) {
    $('#delete_areaid').val(landmark_id);
    fancybox_show('delete-popup', {
      width: 450
    });
  }
  /*********************************************************************************** */
  function confirm_disable_enable_modal(location_id, status) {
    console.log(status);
    if (status !== "" && status !== null) {
      fancybox_show('enable-popup', {
        width: 450
      });
      status = 0;
    } else {
      fancybox_show('disable-popup', {
        width: 450
      });
      status = 1;
    }
    disable_areaid = location_id;
    web_status = status;
  }


  function confirm_disable_enable_all(action) {
    if (action == 'enable') {
      fancybox_show('enable-all-popup', {
        width: 450
      });
    } else {
      fancybox_show('disable-all-popup', {
        width: 450
      });
    }
  }

  function confirm_disable_all() {
    $.ajax({
      url: _base_url + 'settings/change_status_location/',
      type: 'POST',
      dataType: 'JSON',
      success: function(data) {
        reloadPage();
      }
    });
  }

  function confirm_enable_all() {
    $.ajax({
      url: _base_url + 'settings/change_status_enable_location/',
      type: 'POST',
      dataType: 'JSON',
      success: function(data) {
        reloadPage();
      }
    });
  }
  /*********************************************************************************** */
  function confirm_disable() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/landmark_status",
      data: {
        landmark_id: disable_areaid,
        landmark_status: web_status
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        window.location.assign(_base_url + 'landmark');
      }
    });
  }
  /*********************************************************************************** */
  function confirm_enable() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/landmark_status",
      data: {
        landmark_id: disable_areaid,
        landmark_status: web_status
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        window.location.assign(_base_url + 'landmark');
      }
    });
  }
  /*********************************************************************************** */
  function confirm_delete() {
    $.ajax({
      type: "POST",
      url: _base_url + "settings/remove_landmark",
      data: {
        landmark_id: $('#delete_areaid').val()
      },

      dataType: "text",
      cache: false,
      success: function(data) {
        location.reload();
      },
      error: function(data) {
        alert(data.statusText);
        console.log(data);
      }
    });
  }
  // *************************************************************************************
  // view landmark

  $('[data-action="view-landmark-popup"]').click(function (event) {
    $('.spanheading').html("View Landmark");
    $("#edit-popup :input").prop("disabled", true);
    $('#landamark_footer').hide();
    $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "settings/view_landmark",
        data: {
            landmark_id: $(this).attr("data-id")
        },
        dataType: "json",
        cache: false,
        success: function (result) {
            $('#edit_landmarkname').val(result.landmark_name)
            // Corrected: Use value attribute for setting the selected option
            $('#edit_area_id').val(result.area_id).trigger('change');
            $('#edit_location_id').val(result.location_id)
            $('#edit_description').val(result.description)
            fancybox_show('edit-popup', {
                closeClick: false
            });
        },
        error: function (data) {
            toast('error', data.statusText);
        }
    });
});



  /*********************************************************************************** */
  function edit_landmark_get(landmark_id) {
    $('.mm-loader').show();
    $.ajax({
      type: "POST",
      url: _base_url + "settings/edit_landmark",
      data: {
        landmark_id: landmark_id
      },
      dataType: "text",
      cache: false,
      success: function(result) {
        //alert(result);
        var obj = jQuery.parseJSON(result);
        $.each($.parseJSON(result), function(edit, value) {
          //alert(value.zone_id);
          $('#edit_landmarkid').val(value.landmark_id)
          $('#edit_landmarkname').val(value.landmark_name)
          $('#edit_area_id option[value="' + value.area_id + '"]').prop('selected', true);
          $('#edit_area_id').trigger('change');
          $('#edit_location_id option[value="' + value.location_id + '"]').prop('selected', true);
          $('#edit_location_id').trigger('change');
          $('#edit_description').val(value.description)
        });
        fancybox_show('edit-popup', {
          width: 450
        });
        $('.mm-loader').hide();
      },
      error: function(data) {
        $('.mm-loader').hide();
        alert(data.statusText);
        console.log(data);
      }
    });
  }
  /*********************************************************************************** */
  function closeFancy() {
    $.fancybox.close();
  }
  /*********************************************************************************** */
  function newPopup() {
    fancybox_show('new-popup', {
      width: 450
    });
  }
  /*********************************************************************************** */

  function loadLocations() {
    // Get the selected area ID
    var areaId = document.getElementById('area_id').value;

    // Make an AJAX request to the server
    $.ajax({
      url: '<?php echo base_url("settings/getlocations"); ?>',
      type: 'POST',
      data: {
        area_id: areaId
      },
      dataType: 'json',
      success: function(response) {
        // Clear the previous options
        $('#location_id').empty();

        // Add the new options based on the response
        $.each(response, function(index, location) {
          $('#location_id').append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
        });
      }
    });
  }
  /*********************************************************************************** */
  function loadLocationsedit() {
    // Get the selected area ID
    var areaId = document.getElementById('edit_area_id').value;

    // Make an AJAX request to the server
    $.ajax({
      url: '<?php echo base_url("settings/getlocations"); ?>',
      type: 'POST',
      data: {
        area_id: areaId
      },
      dataType: 'json',
      success: function(response) {
        // Clear the previous options
        $('#edit_location_id').empty();

        // Add the new options based on the response
        $.each(response, function(index, location) {
          $('#edit_location_id').append('<option value="' + location.location_id + '">' + location.location_name + '</option>');
        });
      }
    });
  }

  /*********************************************************************************** */
  function validateForm() {
    const name = document.getElementById("landmarkname").value;
    const area_id = document.getElementById("area_id").value;
    const location_id = document.getElementById("location_id").value;

    if (name === "") {
      document.getElementById("username-error").textContent = "Please enter a landmark name.";
      return false;
    }
    if (area_id === "") {
      document.getElementById("area-error").textContent = "Please select area.";
      return false;
    }
    if (location_id === "") {
      document.getElementById("location-error").textContent = "Please select location.";
      return false;
    }



    // Additional validation logic goes here...

    return true; // Form submission will proceed if true.
  }



  function validateFormEdit() {
    const name = document.getElementById("edit_landmarkname").value;
    const area_id = document.getElementById("edit_area_id").value;
    const location_id = document.getElementById("edit_location_id").value;

    if (name === "") {
      document.getElementById("username-errorr").textContent = "Please enter a landmark name.";
      return false;
    }
    if (area_id === "") {
      document.getElementById("area-errorr").textContent = "Please select area.";
      return false;
    }
    if (location_id === "") {
      document.getElementById("location-errorr").textContent = "Please select location.";
      return false;
    }



    // Additional validation logic goes here...

    return true; // Form submission will proceed if true.
  }

  /*********************************************************************************** */
</script>