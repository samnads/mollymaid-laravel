<!--<script src="<?php// echo base_url(); ?>js/jquery-1.7.2.min.js"></script>--> 
<script src="//maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&libraries=places&callback=Function.prototype" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/geocodezip/v3-utility-library@master/archive/maplabel/src/maplabel.js"></script>


<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />

<section>
    <div class="row no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                    
                    
                    
                    
                    
                    <div class="widget-header"><form class="form-horizontal" method="POST" action="<?php echo base_url('location/booking'); ?>">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Booking Locations</h3></li>
              
              <li class="mr-2">
                  <input class="n-calendar-icon" type="text" style="width: 160px;" id="service_date" name="service_date" value="<?php echo $service_date ?>">           
              </li>
      
      
      
              <li>
                  <input type="hidden" id="formatted-date-job" value="<?php echo $formatted_date ?>"/>
                            <input type="submit" class="n-btn" value="Go" name="map_button" >
              </li>
      
      
      
      
      
      
              <div class="clear"></div>
            </ul>
     </div></form> 
</div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
                    <div id="job_map" style="height: 800px; width:100%;"></div>
            </div><!--welcome-text-main end--> 
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->
<script type="text/javascript">
var map;
var mapOptions = {
    zoom: 12,
    center: {lat:25.2048, lng:55.2708}
}; // map options
var markers = [];
function initialize() {
    map = new google.maps.Map(document.getElementById('job_map'),
            mapOptions);
//            var marker = new google.maps.Marker({
//          map: map,
//          position: latlng,
//          draggable: false,
//          anchorPoint: new google.maps.Point(0, -29)
//       });
//alert(mapOptions.center);
//var marker = new google.maps.Marker({
//          position: mapOptions.center,
//          map: map,
//          title: 'Hello World!'
//        });

    jQuery.ajax({
        type: 'POST',
        url: '<?= base_url() ?>booking/closest_locations_job',
        dataType: "json",
        data:"data="+ '{ "latitude":"25.2048", "longitude": "55.2708", "servicedate" : "'+$('#formatted-date-job').val()+'" }',
        //data: {},
        success:function(data) {
            //alert(data);
            // when request is successed add markers with results
            //add_markers(data);
            var marker, i;
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();
        // display how many closest providers avialable
        //document.getElementById('info').innerHTML = ' Available:' + data.length + ' Providers<br>';

        for (i = 0; i < data.length; i++) {
            var htmlval = '<table style="margin-bottom:0px; color: #0756A5;"><tr><td><i class="fa fa-user"></i></td><td style="padding-left:5px; font-weight:bold;">'+data[i][3]+'</td></tr><tr><td><i class="fa fa-female"></i></td><td style="padding-left:5px; font-weight:bold;">'+data[i][6]+'</td></tr><tr><td><i class="fa fa-clock-o"></i></td><td style="padding-left:5px; font-weight:bold;">'+data[i][0]+' - '+data[i][1]+'</td></tr></table>';
            //var infowindow = new google.maps.InfoWindow({content: htmlval});
//            var coordStr = data[i][5];
//            var coords = coordStr.split(',');
            //alert(data[i][4]);
            var pt = new google.maps.LatLng(data[i][5], data[i][4]);
            bounds.extend(pt);


            var mapLabel = new MapLabel({
			text: data[i][3],
			position: new google.maps.LatLng(data[i][5],  data[i][4]),
			map: map,
			fontSize: 15,
            fontColor: 'black',
			align: 'left'
		});
        
            marker = new google.maps.Marker({
                position: pt,
                map: map,
                animation: google.maps.Animation.DROP,
                //icon: data[i][6],
                //icon: _base_url+'assets/images/38.png',
                //address: data[i][1],
                title: data[i][3],
                html: htmlval
            });
            markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(marker.html);
                    //prov.value = data[i][5];
                    infowindow.open(map, marker);
                }
            })
            (marker, i));

        }
        }
    });
}
window.addEventListener('load', initialize)
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('location'); ?>') {
			$this.addClass('active');
		}
	})
    $('#service_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
})
</script>
