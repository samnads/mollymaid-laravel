<style>
    #invoice-exTab2 ul li { margin-left: 0px !important; }
    #invoice-exTab2 ul li a { color: black; }
    #exTab2 .dataTables_filter {
display: none;
} 
.book-nav-top li{
    margin-right: 10px;
}

.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child {
    border-radius: 0;
}
.table-bordered thead:first-child tr:first-child th:last-child, .table-bordered tbody:first-child tr:first-child td:last-child {
    border-radius: 0;
}
.no-left-border { border-left: 0px  !important;}

.no-right-border { border-right: 0px  !important;}

.table-bordered { border-radius: 0; }
    /*.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{border: auto;}*/
</style>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                    <div class="widget-header"> 
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                              <div class="book-nav-top">

                                <ul>
                                    <li>
                            <i class="icon-th-list"></i>
                            <h3>Invoice</h3>
                                    </li>
                                    
                                    <li>
                                        <?php if($post_invoice_date!=""){ ?>
                                        <input type="text" style="width: 160px;" id="invoice_date" name="invoice_date" value="<?php echo $post_invoice_date ?>">
                                        <?php } else {?> 
                                        <input type="text" style="width: 160px;" id="invoice_date" name="invoice_date" value="<?php echo date('d/m/Y') ?>">
                                        <?php } ?>
                                        <input type="hidden" name="day" id="day" value="">
                                        <input type="hidden" name="invoicedateformat" id="invoicedateformat" value="<?php echo $invoice_date; ?>" />
                                        <input type="submit" class="n-btn" value="Go" id="invoicego" name="invoice_report">
                                        <div class="clear"></div>
                                    </li>
                                
                                <!-- Job start date -->
                                <li>
                                <input type="text" id="invoice-b-date-from-job" style="width: 160px; display: none;" data-date="<?php echo $search_date_from_job; ?>" readonly value="<?php echo $search_date_from_job; ?>" data-date-format="dd/mm/yyyy"/> 
                                </li>
                                <li>
                                <input type="text" id="invoice-b-date-to-job" style="width: 160px;  display: none;" data-date='<?php echo $search_date_to_job ?>' readonly value='<?php echo $search_date_to_job ?>' data-date-format="dd/mm/yyyy"/>
                                </li>
                                <li>
                                    <input type="button" id="invoice-search" value="Search" style="width: 65px; display: none;" />
                                </li>
                                <div class="clear"></div>
                                <!-- Job ends -->
                                </ul>
                              </div>
                                
                            
                            <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                            <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                            
                        </form>
                        
                    </div>
                    <!--<div class="widget-content" style="margin-bottom:30px">-->
                <div id="invoice-exTab2" class="">	
                    <ul class="nav nav-tabs">
			<li class="active">
                            <a data-target="#invoice1" data-toggle="tab" id="invoice-tab1">New</a>
			</li>
                        <li>
                            <a data-target="#invoice2" data-toggle="tab" id="invoice-tab2">Due</a>
			</li>
			<li>
                            <a data-target="#invoice3" data-toggle="tab" id="invoice-tab3">Paid</a>
			</li>
                        <li>
                            <a data-target="#invoice4" data-toggle="tab" id="invoice-tab4">All</a>
			</li>
                    </ul>
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="invoice1">
                            <table id="invoice-job-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <!--<th class="no-left-border"></th>-->
                                        <th>Sl</th>
                                        <th>Maid</th>
                                        <th>Time</th>
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <th>Service date</th>
                                        <th>Amount</th>
                                        <th>Paid Amount</th>
                                        <th class="no-right-border">Balance</th>
                                    </tr>
				</thead>
                                <tbody id="invoice-tabtbody1">
                                    <?php
                                    $i=1;
                                    $total_amount_paid = 0;
                                    foreach($invoice_report as $report)
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $report->maid_name; ?></td>
                                        <td><?php echo $report->time_from." - ".$report->time_to; ?></td>
                                        <td><?php echo $report->customer_name; ?></td>
                                        <td><?php echo $report->customer_address; ?></td>
                                        <td><?php echo $report->service_date; ?></td>
                                        <td><?php echo $report->billed_amount; ?></td>
                                        <td><?php echo $report->received_amount; ?></td>
                                        <td><?php echo $report->balance_amount; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                    }
                                    ?>
				</tbody>
                             </table>
<!--                                <p class="form-group">
                                    <button type="button" class="btn btn-primary" id="tbl-btn">Submit</button>
</p>-->
                        </div>
                        <div class="tab-pane" id="invoice2">
                            <table id="invoice-job-table2" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Maid</th>
                                        <th>Time</th>
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <th>Service date</th>
                                        <th>Amount</th>
                                        <th>Paid Amount</th>
                                        <th class="no-right-border">Balance</th>
                                    </tr>
				</thead>
                                <tbody id="invoice-tabtbody2">
                                    <?php
                                    //$i=1;
                                    //foreach($booking_list as $b_list)
                                    //{
                                    //    if($b_list->booking_type == "WE")
                                    //    {
                                    //        $booking_type = "Weekly";
                                    //    } else if($b_list->booking_type == "OD"){
                                           // $booking_type = "One Day";
                                    //    }
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $b_list->customer_name; ?></td>
                                        <td><?php// echo $b_list->customer_address; ?></td>
                                        <td><?php// echo $b_list->shift; ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $b_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $b_list->email_address; ?>
                                        </td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                                <p class="form-group">
                                    <button type="button" class="btn btn-primary" id="tbl-btn2">Submit</button>
</p>-->
                        </div>
                        <div class="tab-pane" id="invoice3">
                            <table id="invoice-job-table3" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Maid</th>
                                        <th>Time</th>
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <th>Service date</th>
                                        <th>Amount</th>
                                        <th>Paid Amount</th>
                                        <th class="no-right-border">Balance</th>
                                    </tr>
				</thead>
                                <tbody id="invoice-tabtbody3">
                                    <?php
                                    //$i=1;
                                    //foreach($progress_list as $p_list)
                                    //{
                                    //    if($p_list->booking_type == "WE")
                                    //    {
                                    //        $booking_type = "Weekly";
                                    //    } else if($p_list->booking_type == "OD"){
                                    //        $booking_type = "One Day";
                                    //    }
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $p_list->customer_name; ?></td>
                                        <td><?php// echo $p_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($p_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($p_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $p_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $p_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($p_list->start_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                                <p class="form-group">
                                    <button type="button" class="btn btn-primary" id="tbl-btn3">Submit</button>
</p>-->
                        </div>
                        <div class="tab-pane" id="invoice4">
                            <table id="invoice-job-table4" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Maid</th>
                                        <th>Time</th>
                                        <th>Customer</th>
                                        <th>Address</th>
                                        <th>Service date</th>
                                        <th>Amount</th>
                                        <th>Paid Amount</th>
                                        <th class="no-right-border">Balance</th>
                                    </tr>
				</thead>
				<tbody id="invoice-tabtbody4">
                                    <?php
                                    //$i=1;
                                    //foreach($finish_list as $f_list)
                                    //{
                                      //  if($f_list->booking_type == "WE")
                                        //{
                                         //   $booking_type = "Weekly";
                                        //} else if($f_list->booking_type == "OD"){
                                          //  $booking_type = "One Day";
                                        //}
                                    ?>
<!--                                    <tr>
                                        <td></td>
                                        <td><?php// echo $i; ?></td>
                                        <td><?php// echo $f_list->customer_name; ?></td>
                                        <td><?php// echo $f_list->customer_address; ?></td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->time_from)); ?> - <?php// echo date('h:i A', strtotime($f_list->time_to)); ?></td>
                                        <td><?php// echo $booking_type; ?></td>
                                        <td>
                                            <i class="fa fa-phone"></i> <?php// echo $f_list->mobile_number_1; ?>
                                            <br>
                                            <i class="fa fa-envelope"></i> <?php// echo $f_list->email_address; ?>
                                        </td>
                                        <td><?php// echo date('h:i A', strtotime($f_list->start_time)); ?> - <?php// echo date('h:i A', strtotime($f_list->end_time)); ?></td>
                                    </tr>-->
                                    <?php
                                    //$i++;
                                    //}
                                    ?>
				</tbody>
                             </table>
<!--                            <p class="form-group">
                                <button type="button" class="btn btn-primary" id="tbl-btn4">Submit</button>
                            </p>-->
                        </div>
                    </div>
                <!--</div>-->
                    <!--</div>-->
                </div>
            </div><!--welcome-text-main end--> 
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->