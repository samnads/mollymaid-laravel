<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/********************************************** */
/*  functions to safely delete and update       */
/*  database without lossing db integrity       */
/*  Always use this function before delete      */
/*  or update                                   */ 
/********************************************** */
function is_safe_to_delete_customer_payment($payment_id)
{
    // check all possibilies for any error here !
    // for extra conditions edit from here
    // return true if SAFE
    // returns error message if not safe to delete
    $CI = get_instance();
    $CI->load->model('customers_model');
    $payment = $CI->customers_model->get_customer_payment_byid($payment_id);
    if(!$payment){
        return 'Payment not found or deleted !';
    }
    else if($payment->payment_status == '1'){
        return 'Posted payment can\'t be deleted !';
    }
    else if($payment->payment_status == '2'){
        return 'Posted payment can\'t be deleted !';
    }
    else if($payment->paid_at != 'P' && $payment->verified_status == '1'){
        return 'Verified payment can\'t be deleted !';
    }
    // some more conditions will add soon...........
    return true;
}