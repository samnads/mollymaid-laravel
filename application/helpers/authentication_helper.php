<?php
if(! defined('BASEPATH')) exit('No direct script access allowed');

function user_login($username, $password)
{
	$CI =& get_instance();
	
	$CI->load->model('users_model');
	
	$user = $CI->users_model->get_user_by_username_and_password($username, md5($password));

	$returns = array();
	
	if(isset($user->user_id))
	{
		if($user->user_status == 1)
		{
			$sess_array = array();
			$sess_array['user_id'] = $user->user_id;
			$sess_array['user_name'] = $user->username;
			$sess_array['user_fullname'] = $user->user_fullname;
			$sess_array['user_permissions'] = $user->permissions;
			$sess_array['user_admin'] = $user->is_admin;
			$sess_array['login_type'] = $user->login_type;
			$sess_array['user_role'] = $user->user_role;

			$CI->session->set_userdata('user_logged_in', $sess_array);
			
			$update_fields = array();
			$update_fields['last_loggedin_datetime'] = date('Y-m-d H:i:s');
			$update_fields['last_loggedin_ip'] = $_SERVER['REMOTE_ADDR'];
			
			$CI->users_model->update_user($user->user_id, $update_fields);
                        
                        $update_login_fields = array();
                        $update_login_fields['user_id'] = $user->user_id;
                        $update_login_fields['login_ip'] = $_SERVER['REMOTE_ADDR'];
                        $update_login_fields['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$update_login_fields['login_date_time'] = date('Y-m-d H:i:s');
			
			$CI->users_model->add_user_login_detail($update_login_fields);
			
			$returns['user_id'] = $user->user_id;
		}
		else
		{
			$returns['error'] = 'Inactive';
		}
	}
	else
	{
		$returns['error'] = 'Invalid login';
	}
	
	return $returns;
}

function is_user_loggedin()
{
	$CI =& get_instance();
	
	if($CI->session->userdata('user_logged_in'))
	{
		$user_logged_in_session = $CI->session->userdata('user_logged_in');
		
		if(isset($user_logged_in_session['user_id']) && is_numeric($user_logged_in_session['user_id']) && $user_logged_in_session['user_id'] > 0)
		{
			return TRUE;
		}
	}
	
	return FALSE;
}
function user_authenticate_name()
{
	$CI =& get_instance();
	
	if($CI->session->userdata('user_logged_in'))
	{
		$user_logged_in_session = $CI->session->userdata('user_logged_in');
		
		if(isset($user_logged_in_session['user_id']) && is_numeric($user_logged_in_session['user_id']) && $user_logged_in_session['user_id'] > 0)
		{
			return $user_logged_in_session['user_fullname'];
		}
	}
	
	redirect('logout');
}
function user_authenticate()
{
	$CI =& get_instance();
	
	if($CI->session->userdata('user_logged_in'))
	{
		$user_logged_in_session = $CI->session->userdata('user_logged_in');
		
		if(isset($user_logged_in_session['user_id']) && is_numeric($user_logged_in_session['user_id']) && $user_logged_in_session['user_id'] > 0)
		{
			return $user_logged_in_session['user_id'];
		}
	}
	
	redirect('logout');
}

function user_logout()
{
	$CI =& get_instance();

	if($CI->session->userdata('user_logged_in'))
	{
		$CI->session->unset_userdata('user_logged_in');
	}	
}
function user_permission($user_id, $module_id)
{
	$CI =& get_instance();
	
	$CI->load->model('users_model');
	
	$user = $CI->users_model->check_user_permission($user_id, $module_id);
        
        if($user && $user->user_permission_id)
        {
            return TRUE;
        }
        
        return FALSE;
}

function activity_icon($activity)
{
    
   $icon= array(
                'Add_Customer'  => '<i class="fa fa-user" aria-hidden="true"></i>',
                'Edit_Customer' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'Customer_enable' => '<i class="fa fa-user" aria-hidden="true"  style="color:#428bca"></i>',
                'Customer_disable'=> '<i class="fa fa-trash" aria-hidden="true"  style="color:#428bca"></i>',
                'Maid_added'    => '<i class="fa fa-user" aria-hidden="true"></i>',
                'Maid_edited'   => '<i class="fa fa-pencil" aria-hidden="true"></i>',
                'Maid_enabled'  => '<i class="fa fa-user" aria-hidden="true"></i>',
                'Maid_disabled' => '<i class="fa fa-trash" aria-hidden="true"  style="color:#428bca"></i>',
                'Maid_deleted'  => '<i class="fa fa-trash" aria-hidden="true" style="color:#428bca"></i>',
                'Booking_add'   => '<i class="fa fa-calendar"></i>',
                'Booking_update'=> '<i class="fa fa-calendar"></i>',
                'Weekly_Cancel' => '<i class="fa fa-calendar" style="color:#428bca"></i>',
                'One_day_Cancel'=> '<i class="fa fa-calendar" style="color:#428bca"></i>',
                'Maid_marked_leave'=> '<i class="fa fa-calendar" aria-hidden="true"></i>',
                'email_notification'=> '<i class="fa fa-envelope" aria-hidden="true"></i>',
                'sms_notification'=> '<i class="fa fa-mobile" aria-hidden="true"></i>',
                'Customer_IN'=> '<i class="fa fa-briefcase"></i>',
                'Customer_OUT'=> '<i class="fa fa-briefcase"></i>',
                'Service_Cancel'=> '<i class="fa fa-briefcase" style="color:#428bca"></i>',
                'Payment'=> '<i class="fa fa-money" aria-hidden="true"></i>',
                'Maid_OUT'=> '<i class="fa fa-briefcase"></i>',
                'Transfer'=> '<i class="fa fa-briefcase"></i>',
				'driver_change'=> '<i class="icon-truck"></i>',
				'package_added'=> '<i class="icon-plus"></i>',
				'package_updated'=> '<i class="icon-pencil"></i>',
				'package_enabled'=> '<i class="icon-check"></i>',
				'package_disabled'=> '<i class="fa fa-minus-square"></i>',
                ) ;
   if(array_key_exists($activity, $icon)){
   return $icon[$activity];
   }
}
   

	function user_authenticate_isadmin()
	{
		$CI =& get_instance();
		
		if($CI->session->userdata('user_logged_in'))
		{
			$user_logged_in_session = $CI->session->userdata('user_logged_in');
			
			if(isset($user_logged_in_session['user_id']) && is_numeric($user_logged_in_session['user_id']) && $user_logged_in_session['user_id'] > 0)
			{
				return $user_logged_in_session['user_admin'];
			}
		}
		
		redirect('logout');
	}
    
	function user_authenticate_role()
	{
		$CI =& get_instance();
		
		if($CI->session->userdata('user_logged_in'))
		{
			$user_logged_in_session = $CI->session->userdata('user_logged_in');
			
			if(isset($user_logged_in_session['user_id']) && is_numeric($user_logged_in_session['user_id']) && $user_logged_in_session['user_id'] > 0)
			{
				return $user_logged_in_session['user_role'];
			}
		}
		
		redirect('logout');
	}
