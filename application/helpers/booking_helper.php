<?php
/*********************************** */
/*                                   */
/*      File Author : Samnad. S      */
/*                                   */
/*********************************** */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
function showPrepareTimeError($schedule_date, $hour_from, $book_type, $repeat_days = [], $response_type = 'json')
{
    // Code by : Samnad. S @ 31-03-2023
    /*********************************** */
    // This function is used to ensure atleast 1 hour (60 minutes) free time available for new booking.
    // Function can run while creating new booking
    // For eg. If new booking time is 04:00 PM booking must be made before 03.00 PM (means 60 minute prepare time needed)
    /*********************************** */
    $CI = get_instance();
    $CI->load->model('settings_model');
    $settings = $CI->settings_model->get_settings();
    $waitTime = $settings->booking_preparation_time;
    $schedule_date = str_replace('/', '-', $schedule_date);
    if (strtotime($schedule_date) == strtotime(date('d-M-Y'))) {
        // checking not required for future dates
        $response = array();
        $current_week_day = date('w');
        $hour_from = date('H:i', trim($hour_from));
        $hour_now = date('H:i');
        $diffMinutes = (strtotime($hour_from) - strtotime($hour_now)) / 60;
        if ($book_type == 'OD' && $diffMinutes < $waitTime) {
            if ($response_type == 'json') {
                $response['status'] = 'error';
                $response['message'] = 'Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.';
                die(json_encode($response));
            } else {
                die('<span class="field-error text-danger">Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.</p>');
            }
        } else if ($book_type == 'WE') {
            if (empty($repeat_days)) {
                $repeat_days[] = $current_week_day; // in some pages multiple days selection not shown, so we take today as weekly day
            }
            foreach ($repeat_days as $key => $week) {
                if ($week == $current_week_day && $diffMinutes < $waitTime) {
                    if ($response_type == 'json') {
                        $response['status'] = 'error';
                        $response['message'] = 'Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.';
                        die(json_encode($response));
                    } else {
                        die('<span class="field-error text-danger">Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.</p>');
                    }

                }
            }
        } else if ($book_type == 'BW') {
            if (empty($repeat_days)) {
                $repeat_days[] = $current_week_day; // in some pages multiple days selection not shown, so we take today as bi-weekly day
            }
            foreach ($repeat_days as $key => $week) {
                if ($week == $current_week_day && $diffMinutes < $waitTime) {
                    if ($response_type == 'json') {
                        $response['status'] = 'error';
                        $response['message'] = 'Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.';
                        die(json_encode($response));
                    } else {
                        die('<span class="field-error text-danger">Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.</p>');
                    }
                }
            }
        }
        //die(json_encode(array('status' => 'error', 'message' => 'Booking Allowed !'))); // test
    }
}
function getFutureDayServiceDates($weekday, $dateFromString, $dateToString, $booking_type)
{
    $booking_type = strtoupper($booking_type);
    if ($booking_type == 'OD') {
        $dates[] = $dateFromString;
        return $dates;
    }
    $weekMapping = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday');
    $format = 'Y-m-d';
    $weekday = $weekMapping[$weekday];
    $dateFrom = new \DateTime($dateFromString);
    $dateTo = new \DateTime($dateToString);
    $dates = [];
    if ($dateFrom > $dateTo) {
        return $dates;
    }
    if (date('N', strtotime($weekday)) != $dateFrom->format('N')) {
        $dateFrom->modify("next $weekday");
    }
    while ($dateFrom <= $dateTo) {
        $dates[] = $dateFrom->format($format);
        if ($booking_type == 'WE') {
            $dateFrom->modify('+1 week');
        } elseif ($booking_type == 'BW') {
            $dateFrom->modify('+2 week');
        }
    }
    return $dates;
}
function is_any_ongoing_day_service_for_booking_by_service_date($booking_id, $service_date)
{
    /*********************************** */
    // Author : Samnad. S @ 10-05-2023
    // check for any none finished day services by booking id and service date
    // returns TRUE if service(s) found else returns FALSE
    /*********************************** */
    // $service_date = DateTime::createFromFormat('d-m-Y', $service_date)->format('Y-m-d');
    $CI = get_instance();
    $CI->load->model('day_services_model');
    $day_services = $CI->day_services_model->get_day_services_where(array('ds.booking_id' => $booking_id, 'ds.service_status' => 1, 'ds.service_date' => $service_date)); // service_status 1 means ongoing
    if (count($day_services)) {
        return true;
    }
    return false;
}
// function get_booking_deletes_by_date($service_date)
// 	{
// 		$this->db->select('booking_id')
// 				->from('booking_deletes')
// 				->where('service_date', $service_date);
		
// 		$get_booking_deletes_by_date_qry = $this->db->get();
		
// 		return $get_booking_deletes_by_date_qry->result();
// 	}
function get_same_timed_bookings($booking, $except_booking_ids)
{
    $CI = get_instance();
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $time_from = $booking->time_from;
    $time_to = $booking->time_to;
    $deleted_bookings = array();
    $CI->load->model('day_services_model');
    $deletes = $CI->day_services_model->get_booking_deletes_by_date_where(array('bd.service_date' => $service_start_date));
    if (!empty($deletes)) {

        foreach ($deletes as $delete) {

        $deleted_bookings[] = $delete->booking_id;
        }
    }
    // print_r($deleted_bookings);die();
    // Base query to select bookings
    $CI->db->select('b.booking_id,
        b.booking_type,
        b.time_from,
        b.time_to,
        m.maid_id,
        m.maid_name,
        m.maid_gender,
        m.maid_nationality,
        m.maid_priority,
        bd.booking_delete_id,
        ROUND(DATEDIFF(b.service_start_date, ' . $CI->db->escape($service_start_date) . ')/7) as service_start_date_week_difference', false)
        ->from('bookings as b')
        ->join('maids as m', 'm.maid_id = b.maid_id')
        ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id', 'left')
        ->where(array('b.deleted_at' => null, 'b.booking_status' => 1, 'b.service_week_day' => $service_week_day))
        ->where_not_in('b.booking_id', $except_booking_ids ?: '[]');
		// ->where_not_in('b.booking_id', $deleted_bookings);
        
    // Handle overlap based on booking type
    if ($booking->service_end == '0') {
        $CI->db->where("(
            b.service_end = 0 OR
            (b.service_end = 1 AND b.service_start_date > '" . $service_start_date . "') OR
            (b.service_end = 1 AND b.service_start_date = '" . $service_start_date . "' AND b.service_actual_end_date = '" . $service_actual_end_date . "')
        )", null, false);
    } else {
        $CI->db->where("(
            b.service_end = 0 OR
            (b.service_end = 1 AND b.service_start_date >= '" . $service_start_date . "' AND b.service_start_date <= '" . $service_actual_end_date . "') OR
            (b.service_end = 1 AND b.service_start_date BETWEEN '" . $service_start_date . "' AND '" . $service_actual_end_date . "') OR
            (b.service_end = 1 AND b.service_actual_end_date BETWEEN '" . $service_start_date . "' AND '" . $service_actual_end_date . "')
        )", null, false);
    }

    // Modify the query based on booking type (weekly vs one-time)
    if ($booking->booking_type == 'WE') {
        // Weekly booking: Check for overlap with both OD and WE bookings
        $CI->db->where("(
            (b.time_from = " . $CI->db->escape($time_from) . ") OR
            (b.time_from = " . $CI->db->escape($time_from) . " AND b.time_to = " . $CI->db->escape($time_to) . ") OR
            (b.time_from > " . $CI->db->escape($time_from) . " AND b.time_to <= " . $CI->db->escape($time_to) . ") OR
            (b.time_from < " . $CI->db->escape($time_from) . " AND b.time_to = " . $CI->db->escape($time_to) . ") OR
            (b.time_from < " . $CI->db->escape($time_to) . " AND b.time_to > " . $CI->db->escape($time_to) . ") OR
            (b.time_from < " . $CI->db->escape($time_from) . " AND b.time_to > " . $CI->db->escape($time_from) . ") OR
            (b.time_from < " . $CI->db->escape($time_from) . " AND b.time_to > " . $CI->db->escape($time_to) . "))
        ");
    } else {
        if (count($deletes) > 0) 
        {
            $CI->db->where_not_in('b.booking_id', $deleted_bookings);
        }
        // One-time booking: Check for overlap only with other OD bookings
        //     $CI->db->where("b.booking_type = 'OD' AND (
        $CI->db->where("(
            (b.time_from = " . $CI->db->escape($time_from) . ") OR
            (b.time_from = " . $CI->db->escape($time_from) . " AND b.time_to = " . $CI->db->escape($time_to) . ") OR
            (b.time_from > " . $CI->db->escape($time_from) . " AND b.time_to <= " . $CI->db->escape($time_to) . ") OR
            (b.time_from < " . $CI->db->escape($time_from) . " AND b.time_to = " . $CI->db->escape($time_to) . ") OR
            (b.time_from < " . $CI->db->escape($time_to) . " AND b.time_to > " . $CI->db->escape($time_to) . ") OR
            (b.time_from < " . $CI->db->escape($time_from) . " AND b.time_to > " . $CI->db->escape($time_from) . ") OR
            (b.time_from < " . $CI->db->escape($time_from) . " AND b.time_to > " . $CI->db->escape($time_to) . "))
        ");
    }

    $query = $CI->db->get();
    return $query->result();
}

function createReference($prefix,$unique_id){
    return $prefix.$unique_id;
}
function get_same_timed_day_service($day_service, $except_day_service_ids)
{
    $CI = get_instance();
    /*************************************** */
    $time_from = $day_service->time_from;
    $time_to = $day_service->time_to;

    /******************************************************************** */
    $CI->db->select('ds.day_service_id,
    m.maid_id,
    ds.time_from,
    bd.booking_delete_id,
    ds.booking_id as day_service_booking_id,
    bd.day_service_id as delete_day_service_id,
    ds.time_to')
        ->from('day_services as ds')
        ->join('maids as m', 'm.maid_id = ds.maid_id')
        ->join('booking_deletes as bd', 'ds.booking_id = bd.booking_id', 'left')
    //    ->join('booking as b', 'b.booking_id = ds.booking_id', 'left')
        ->where_not_in('ds.day_service_id', $except_day_service_ids ?: '[]')
        ->where('ds.service_date', $day_service->service_date);;
    /******************************************************************** */
    /******************************************************************** */
    $CI->db->where("(
        (ds.time_from = " . $CI->db->escape($time_from) . ") OR
        (ds.time_from = " . $CI->db->escape($time_from) . " AND ds.time_to = " . $CI->db->escape($time_to) . ") OR
        (ds.time_from > " . $CI->db->escape($time_from) . " AND ds.time_to <= " . $CI->db->escape($time_to) . ") OR
        (ds.time_from < " . $CI->db->escape($time_from) . " AND ds.time_to = " . $CI->db->escape($time_to) . ") OR
        (ds.time_from < " . $CI->db->escape($time_to) . " AND ds.time_to > " . $CI->db->escape($time_to) . ") OR
        (ds.time_from < " . $CI->db->escape($time_from) . " AND ds.time_to > " . $CI->db->escape($time_from) . ") OR
        (ds.time_from < " . $CI->db->escape($time_from) . " AND ds.time_to > " . $CI->db->escape($time_to) . "))");
    $query = $CI->db->get();
    
    return $query->result();
}
function get_deleted_booking($booking_data)
{
    $CI = get_instance();
    $time_from = $booking_data['time_from'];
    // print_r($time_from);die();
    $time_to = $booking_data['time_to'];
    $booking_date = $booking_data['service_start_date'];
    // print_r($booking_date);die();
    $CI->db->select('
    ds.day_service_id,
    m.maid_id,
    ds.time_from,
    bd.booking_delete_id,
    ds.booking_id as day_service_booking_id,
    bd.day_service_id as delete_day_service_id,
    ds.time_to')
        ->from('day_services as ds')
        ->join('maids as m', 'm.maid_id = ds.maid_id')
        ->join('booking_deletes as bd', 'bd.day_service_id = ds.day_service_id', 'left')
        ->where('ds.service_date', $booking_date);

        $CI->db->where("(
            (ds.time_from = " . $CI->db->escape($time_from) . ") OR
            (ds.time_from = " . $CI->db->escape($time_from) . " AND ds.time_to = " . $CI->db->escape($time_to) . ") OR
            (ds.time_from > " . $CI->db->escape($time_from) . " AND ds.time_to <= " . $CI->db->escape($time_to) . ") OR
            (ds.time_from < " . $CI->db->escape($time_from) . " AND ds.time_to = " . $CI->db->escape($time_to) . ") OR
            (ds.time_from < " . $CI->db->escape($time_to) . " AND ds.time_to > " . $CI->db->escape($time_to) . ") OR
            (ds.time_from < " . $CI->db->escape($time_from) . " AND ds.time_to > " . $CI->db->escape($time_from) . ") OR
            (ds.time_from < " . $CI->db->escape($time_from) . " AND ds.time_to > " . $CI->db->escape($time_to) . "))");
        $query = $CI->db->get();
    
        return $query->result();
}