<?php
/*********************************** */
/*                                   */
/*      Author : Samnad. S           */
/*                                   */
/*********************************** */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
function maid_listing_priority_types()
{
    $maid_listing_priority_types = array(
    'lifo' => 'Last Added First (LIFO)',
    'fifo' => 'First Added First (FIFO)',
    'priority_number_asc' => 'Priority Number (Smallest First)',
    'priority_number_desc' => 'Priority Number (Largest First)',
    'maid_name_asc' => 'Staff Name (A-Z)',
    'maid_name_desc' => 'Staff Name (Z-A)');
    return $maid_listing_priority_types;
}
