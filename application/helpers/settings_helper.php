<?php

/*********************************** */
/*                                   */
/*      Author : Samnad. S           */
/*                                   */
/*********************************** */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
function upload_base64_image($base64, $prefix, $uploaddir)
{
    $uploaddir = "./" . $uploaddir;
    if ($base64 != null) {
        list($type, $base64) = explode(';', $base64);
        list(, $base64) = explode(',', $base64);
        $base64 = base64_decode($base64);
        $file_name = $prefix . '-' . date('Y-m-d') . '-' . md5($base64) . ".png";
        if (file_put_contents($uploaddir . $file_name, $base64)) {
            return $file_name;
        }
        return false;
    }
    return false;
}
// ****************************************************************************


// *********************************************************************************
function check_and_get_img_url($image_path, $default_file_name = 'error.webp')
{
    return base_url((is_file($image_path) ? ltrim($image_path, './') : 'uploads/images/default/' . $default_file_name) . '?v=' . IMG_VERSION);
}

function get_quick_book_values()
{
    $CI = get_instance();
    $CI->load->model('settings_model');
    $settings = $CI->settings_model->get_settings();
    return $settings;
}
