<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 1)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'reports">Go to Reports</a>', 401, 'Access denied');
        }
        $this->load->model('settings_model');
    }
    public function index()
    {
        $data = array();
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('dashboard', $data, true);
        $layout_data['page_title'] = 'Dashboard';
        $layout_data['meta_description'] = 'Dashboard';
        $layout_data['css_files'] = array();
        $layout_data['js_files'] = array('jqBarGraph.1.1.js','dashboard.js');
        $layout_data['external_js_files'] = array();
        $this->load->view('layouts/default_dashboard', $layout_data);
    }

    public function changepassword()
    {
        $this->load->helper('form');
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('users_model');
        $data = array();
        $userid = $this->session->userdata('user_logged_in')['user_id'];
        if ($this->input->post('change_password')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

            $this->form_validation->set_rules('old_password', 'Old Password', 'required|callback_numeric_a');
            $this->form_validation->set_message('required', 'Please enter password', 'old_password');

            $this->form_validation->set_rules('new_password', 'New Password', 'required|callback_numeric_b');
            $this->form_validation->set_message('required', 'Please enter password', 'new_password');

            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|callback_numeric_c');
            $this->form_validation->set_message('required', 'Please enter password', 'confirm_password');

            if ($this->form_validation->run()) {
                $update_fields = array();
                $old_password = trim($this->input->post('old_password'));
                $new_password = trim($this->input->post('new_password'));
                $confirm_password = trim($this->input->post('confirm_password'));
                $update_fields['password'] = md5($new_password);
                $chk_password = $this->users_model->check_passwords($userid, md5($old_password));
                if ($chk_password) {
                    if ($confirm_password == $new_password) {
                        $update_settings = $this->users_model->update_users($userid, $update_fields);

                        $this->session->set_flashdata('success_msg', 'Password changed successfully.');
                        redirect(base_url() . 'dashboard/changepassword');
                        exit();
                    } else {
                        $this->session->set_flashdata('failure_msg', 'Password mismatch.');
                        redirect(base_url() . 'dashboard/changepassword');
                        exit();
                    }
                } else {
                    $this->session->set_flashdata('failure_msg', 'Invalid old password.');
                    redirect(base_url() . 'dashboard/changepassword');
                    exit();
                }
            }
        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('change_password', $data, true);
        $layout_data['page_title'] = 'Change Password';
        $layout_data['meta_description'] = 'Change Password';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array();

        $this->load->view('layouts/default', $layout_data);
    }
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
