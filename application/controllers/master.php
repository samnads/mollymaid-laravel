<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
    }
    public function location()
    {
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('master/location', $data, true);
        $layout_data['page_title'] = 'Locations';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('animate.min.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('master.location.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function get_zones()
    {
        header('Content-Type: application/json; charset=utf-8');
        $response['zones'] = $this->db->select('z.zone_id,z.zone_name,z.zone_status')
            ->from('zones as z')->where('z.zone_status', 1)->order_by('z.zone_id', "DESC")->get()->result();
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function get_places()
    {
        header('Content-Type: application/json; charset=utf-8');
        /********************************************* */
        $response['zones'] = $this->db->select('
            z.zone_id,
            z.zone_name,
            z.description as zone_description,
            ')
            ->from('zones as z')
            ->where('z.zone_status', 1)
            ->order_by('z.zone_name', "ASC")
            ->get()->result();
        /********************************************* */
        $response['areas'] = $this->db->select('
            a.area_id,
            a.area_name,
            a.area_status,
            a.description as area_description,
            z.zone_id,
            ')
            ->from('areas as a')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            ->where('a.deleted_at', null)
            ->where('a.area_status',1)
            ->where('z.zone_status', 1)
            ->order_by('a.area_name', "ASC")
            ->get()->result();
        /********************************************* */
        $response['locations'] = $this->db->select('
            l.location_id,
            l.location_name,
            l.description as location_description,
            z.zone_id,
            a.area_id,
            ')
            ->from('locations as l')
            ->join('areas as a', 'l.area_id = a.area_id')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            ->where('l.deleted_at', null)
            ->where('a.deleted_at', null)
            ->where('a.area_status',1)
            ->where('z.zone_status', 1)
            ->order_by('l.location_name', "ASC")
            ->get()->result();
        /********************************************* */
        $response['landmarks'] = $this->db->select('
            lm.landmark_id,
            lm.landmark_name,
            lm.description as landmark_description,
            z.zone_id,
            a.area_id,
            l.location_id,
            ')
            ->from('landmarks as lm')
            ->join('locations as l', 'lm.location_id = l.location_id')
            ->join('areas as a', 'l.area_id = a.area_id')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            ->where('lm.deleted_at', null)
            ->where('l.deleted_at', null)
            ->where('a.deleted_at', null)
            ->where('z.zone_status', 1)
            ->where('a.area_status',1)
            ->order_by('lm.landmark_name', "ASC")
            ->get()->result();
        $response['status'] = true;
        $response['message'] = "Data fetched successfully.";
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function get_zone()
    {
        header('Content-Type: application/json; charset=utf-8');
        $response['zone'] = $this->db->select('z.zone_id,z.zone_name,z.description')
            ->from('zones as z')->where('z.zone_id', $this->input->get('zone_id'))->get()->row();
        $response['id'] = $this->input->get('zone_id');
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function get_area()
    {
        header('Content-Type: application/json; charset=utf-8');
        $response['area'] = $this->db->select('a.area_id,a.area_name,a.description,a.min_booking_hour,a.area_status')
            ->from('areas as a')->where('a.area_id', $this->input->get('area_id'))
            ->where('a.area_status',1)->get()->row();
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function get_location()
    {
        header('Content-Type: application/json; charset=utf-8');
        $response['location'] = $this->db->select('l.location_id,l.location_name,l.description,l.min_booking_hour,l.latitude,l.longitude,a.area_id,a.area_name,z.zone_id,z.zone_name')
            ->from('locations as l')
            ->join('areas as a', 'l.area_id = a.area_id')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            ->where('l.location_id', $this->input->get('location_id'))->get()->row();
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function get_landmark()
    {
        header('Content-Type: application/json; charset=utf-8');
        $response['landmark'] = $this->db->select('lm.landmark_id,lm.landmark_name,lm.description,lm.location_id,l.location_name,a.area_id,a.area_name,z.zone_id,z.zone_name')
            ->from('landmarks as lm')
            ->join('locations as l', 'lm.location_id = l.location_id')
            ->join('areas as a', 'l.area_id = a.area_id')
            ->join('zones as z', 'a.zone_id = z.zone_id')
            ->where('lm.landmark_id', $this->input->get('landmark_id'))->get()->row();
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function save_zone()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');
            $data['zone_name'] = $this->input->post('name');
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Zone Name', 'required|trim|xss_clean|callback_unique_zone_name');
            $this->form_validation->set_message('unique_zone_name', '%s ' . $data['zone_name'] . ' is already exist.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $data['description'] = $this->input->post('description') ?: null;
            $this->db->insert('zones', $data);
            $insert_id = $this->db->insert_id();
            if ($insert_id == 0) {
                $response['status'] = false;
                throw new Exception($this->db->_error_message(), 100);
            }
            /********************************************* */
            $response['zones'] = $this->db->select('
            z.zone_id,
            z.zone_name,
            z.description as zone_description,
            ')
                ->from('zones as z')
                ->where('z.zone_status', 1)
                ->order_by('z.zone_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $insert_id;
            $response['message'] = "Zone added successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function update_zone()
    {
        header('Content-Type: application/json; charset=utf-8');
        $zone_id = $this->input->post('zone_id');
        $data['zone_name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description') ?: null;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('zone_id', $zone_id);
        $this->db->update('zones', $data);
        /********************************************* */
        $response['zones'] = $this->db->select('
            z.zone_id,
            z.zone_name,
            z.description as zone_description,
            ')
            ->from('zones as z')
            ->where('z.zone_status', 1)
            ->order_by('z.zone_name', "ASC")
            ->get()->result();
        /********************************************* */
        $response['status'] = true;
        $response['id'] = $zone_id;
        $response['message'] = "Zone updated successfully.";
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function unique_zone_name($field, $zone_id)
    {
        // useful for both insert and update validation
        $zone_name = $this->input->post('name');
        /****************************************************** */
        $this->db->select('z.zone_id')->from('zones as z');
        if ($zone_id) {
            $this->db->where('z.zone_id !=', $zone_id);
        }
        $this->db->where('z.zone_name', $zone_name);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return false;
        } else {
            return true;
        }
    }
    public function save_area()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Area Name', 'required|trim|xss_clean|callback_unique_area_name');
            // $this->form_validation->set_rules('min_booking_hour', 'Minimum Hour', 'numeric|trim|xss_clean');
            $this->form_validation->set_message('unique_area_name', 'This %s is already exist for same zone.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $data['zone_id'] = $this->input->post('zone_id');
            $data['area_name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description') ?: null;
            // $data['min_booking_hour'] = $this->input->post('min_booking_hour') ?: null;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->insert('areas', $data);
            $insert_id = $this->db->insert_id();
            if ($insert_id == 0) {
                $response['status'] = false;
                $response['message'] = $this->db->_error_message();
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            /********************************************* */
            $response['areas'] = $this->db->select('
            a.area_id,
            a.area_name,
            a.description as area_description,
            z.zone_id,
            ')
                ->from('areas as a')
                ->join('zones as z', 'a.zone_id = z.zone_id')
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->order_by('a.area_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $insert_id;
            $response['message'] = "Area added successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function update_area()
    {
        log_message('error','post datas'.json_encode($this->input->post()));
        try {
            header('Content-Type: application/json; charset=utf-8');
            $area_id = $this->input->post('area_id');
            $zone_id = $this->input->post('zone_id');
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Area Name', 'required|trim|xss_clean|callback_unique_area_name[' . $area_id . ']');
            $this->form_validation->set_message('unique_area_name', 'This %s is already exist for same zone.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $data['area_name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description') ?: null;
            $data['zone_id']=$zone_id;
            // $data['new_booking_hour'] = $this->input->post('new_booking_hour') ?: null;
            // $data['min_booking_hour'] = $this->input->post('min_booking_hour') ?: null;

            $this->settings_model->update_existing_area_zone($data, $area_id,$area_id);

            // /********************************************* */
            $response['areas'] = $this->db->select('
            a.area_id,
            a.area_name,
            a.min_booking_hour,
            a.description as area_description,
            z.zone_id,
            ')
                ->from('areas as a')
                ->join('zones as z', 'a.zone_id = z.zone_id')
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->order_by('a.area_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $area_id;
            $response['message'] = "Area updated successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function unique_area_name($field, $area_id)
    {
        // useful for both insert and update validation
        $area_name = $this->input->post('name');
        /****************************************************** */
        if ($area_id) {
            $area = $this->db->select('a.zone_id')->from('areas as a')->where('a.area_id', $area_id)->get()->row();
            $zone_id = $area->zone_id;
        } else {
            $zone_id = $this->input->post('zone_id');
        }
        /****************************************************** */
        $this->db->select('a.area_id')->from('areas as a');
        if ($area_id) {
            $this->db->where('a.area_id !=', $area_id);
        }
        $this->db->where('a.zone_id', $zone_id);
        $this->db->where('a.area_name', $area_name);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return false;
        } else {
            return true;
        }
    }
    public function save_location()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');        
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Location Name', 'required|trim|xss_clean|callback_unique_location_name');
            $this->form_validation->set_rules('min_booking_hour', 'Minimum Hour', 'trim|xss_clean');
            $this->form_validation->set_message('unique_location_name', 'This %s is already exist for same area.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $data['area_id'] = $this->input->post('area_id');
            $data['location_name'] = $this->input->post('name');
            $data['latitude'] = $this->input->post('latitude') ?: null;
            $data['longitude'] = $this->input->post('longitude') ?: null;
            $data['description'] = $this->input->post('description') ?: null;
            $data['min_booking_hour'] = $this->input->post('min_booking_hour') ?: null;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->insert('locations', $data);
            $insert_id = $this->db->insert_id();
            if ($insert_id == 0) {
                throw new Exception($this->db->_error_message(), 100);
            }
            /********************************************* */
            $response['locations'] = $this->db->select('
            l.location_id,
            l.location_name,
            l.min_booking_hour,
            l.description as location_description,
            z.zone_id,
            a.area_id,
            ')
                ->from('locations as l')
                ->join('areas as a', 'l.area_id = a.area_id')
                ->join('zones as z', 'a.zone_id = z.zone_id')
                ->where('l.deleted_at', null)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->order_by('l.location_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $insert_id;
            $response['message'] = "Location added successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function update_location()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Location Name', 'required|trim|xss_clean|callback_unique_location_name[' . $this->input->post('location_id') . ']');
            $this->form_validation->set_rules('min_booking_hour', 'Minimum Hour', 'trim|xss_clean');
            $this->form_validation->set_message('unique_location_name', 'This %s is already exist for same area.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $location_id = $this->input->post('location_id');
            $data['location_name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description') ?: null;
            $data['min_booking_hour'] = $this->input->post('min_booking_hour') ?: null;
            $data['latitude'] = $this->input->post('latitude') ?: null;
            $data['longitude'] = $this->input->post('longitude') ?: null;
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('location_id', $location_id);
            $this->db->update('locations', $data);
            /********************************************* */
            $response['locations'] = $this->db->select('
            l.location_id,
            l.location_name,
            l.min_booking_hour,
            l.description as location_description,
            z.zone_id,
            a.area_id,
            ')
                ->from('locations as l')
                ->join('areas as a', 'l.area_id = a.area_id')
                ->join('zones as z', 'a.zone_id = z.zone_id')
                ->where('l.deleted_at', null)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->order_by('l.location_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $location_id;
            $response['message'] = "Location updated successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function unique_location_name($field, $location_id)
    {
        // useful for both insert and update validation
        $location_name = $this->input->post('name');
        /****************************************************** */
        if ($location_id) {
            $location = $this->db->select('l.area_id')->from('locations as l')->where('l.location_id', $location_id)->get()->row();
            $area_id = $location->area_id;
        } else {
            $area_id = $this->input->post('area_id');
        }
        /****************************************************** */
        $this->db->select('l.location_id')->from('locations as l');
        if ($location_id) {
            $this->db->where('l.location_id !=', $location_id);
        }
        $this->db->where('l.area_id', $area_id);
        $this->db->where('l.location_name', $location_name);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return false;
        } else {
            return true;
        }
    }
    public function save_landmark()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Landmark Name', 'required|trim|xss_clean|callback_unique_landmark_name');
            $this->form_validation->set_message('unique_landmark_name', 'This %s is already exist for same location.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $data['location_id'] = $this->input->post('location_id');
            $data['landmark_name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description') ?: null;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->insert('landmarks', $data);
            $insert_id = $this->db->insert_id();
            if ($insert_id == 0) {
                $response['status'] = false;
                $response['message'] = $this->db->_error_message();
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            /********************************************* */
            $response['landmarks'] = $this->db->select('
            lm.landmark_id,
            lm.landmark_name,
            lm.description as landmark_description,
            z.zone_id,
            a.area_id,
            l.location_id,
            ')
                ->from('landmarks as lm')
                ->join('locations as l', 'lm.location_id = l.location_id')
                ->join('areas as a', 'l.area_id = a.area_id')
                ->join('zones as z', 'a.zone_id = z.zone_id')
                ->where('lm.deleted_at', null)
                ->where('l.deleted_at', null)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->order_by('lm.landmark_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $insert_id;
            $response['message'] = "Landmark added successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function update_landmark()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');
            $landmark_id = $this->input->post('landmark_id');
            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('name', 'Landmark Name', 'required|trim|xss_clean|callback_unique_landmark_name[' . $landmark_id . ']');
            $this->form_validation->set_message('unique_landmark_name', 'This %s is already exist for same location.');
            if ($this->form_validation->run() == false) {
                throw new Exception(validation_errors(), 100);
            }
            $data['landmark_name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description') ?: null;
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('landmark_id', $landmark_id);
            $this->db->update('landmarks', $data);
            /********************************************* */
            $response['landmarks'] = $this->db->select('
            lm.landmark_id,
            lm.landmark_name,
            lm.description as landmark_description,
            z.zone_id,
            a.area_id,
            l.location_id,
            ')
                ->from('landmarks as lm')
                ->join('locations as l', 'lm.location_id = l.location_id')
                ->join('areas as a', 'l.area_id = a.area_id')
                ->join('zones as z', 'a.zone_id = z.zone_id')
                ->where('lm.deleted_at', null)
                ->where('l.deleted_at', null)
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->order_by('lm.landmark_name', "ASC")
                ->get()->result();
            /********************************************* */
            $response['status'] = true;
            $response['id'] = $landmark_id;
            $response['message'] = "Landmark updated successfully.";
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function unique_landmark_name($field, $landmark_id)
    {
        // useful for both insert and update validation
        $landmark_name = $this->input->post('name');
        /****************************************************** */
        if ($landmark_id) {
            $landmark = $this->db->select('lm.location_id')->from('landmarks as lm')->where('lm.landmark_id', $landmark_id)->get()->row();
            $location_id = $landmark->location_id;
        } else {
            $location_id = $this->input->post('location_id');
        }
        /****************************************************** */
        $this->db->select('lm.landmark_id')->from('landmarks as lm');
        if ($landmark_id) {
            $this->db->where('lm.landmark_id !=', $landmark_id);
        }
        $this->db->where('lm.location_id', $location_id);
        $this->db->where('lm.landmark_name', $landmark_name);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return false;
        } else {
            return true;
        }
    }
}
