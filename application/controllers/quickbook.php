<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quickbook extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
        
		$this->load->helper('settings_helper');
		$this->load->model('quickbook_model');
		$this->load->model('customers_model');
		$this->load->model('invoice_model');
		// $this->load->model('service_types_model');
		// $this->load->helper('google_api_helper');
	}
	
	public function index()
	{
		$data = array();
		$data['status'] = "";
        $layout_data['content_body'] = $this->load->view('quickbook/quickbook_connect', $data, TRUE);
		$layout_data['page_title'] = 'Quickbook Connection';
		$layout_data['meta_description'] = 'Quickbook Connection';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['quickbook_active'] = '1'; 
		$layout_data['js_files'] = array('');
		$this->load->view('layouts/default', $layout_data);
	}
	
	public function connect_progress()
	{
		require(QUICK_CLIENT_PATH);
		$quickbookvalues = get_quick_book_values();
		$client_id = $quickbookvalues->quickBookClientId;
		$client_secret = $quickbookvalues->quickBookClientSecret;
		
		if($client_id != "" && $client_secret != "")
		{
			$authorizationRequestUrl = QUICK_AUTH_REQ_URL;
			$scope = QUICK_AUTH_SCOPE;
			$tokenEndPointUrl = QUICK_TOKEN_END_URL;
			$redirect_uri = base_url().'quickbook/connect_progress';
			$response_type = 'code';
			$state = 'RandomState';
			$grant_type= 'authorization_code';
			//$certFilePath = './Certificate/all.platform.intuit.com.pem';
			$certFilePath = QUICK_CERTIFICATE_PATH;
			
			$client = new Client($client_id, $client_secret, $certFilePath);
			
			if (!isset($_GET["code"]))
			{
				$authUrl = $client->getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state);
				redirect($authUrl);
				exit();
			} else {
				$code = $_GET["code"];
				$responseState = $_GET['state'];
				if(strcmp($state, $responseState) != 0){
				  throw new Exception("The state is not correct from Intuit Server. Consider your app is hacked.");
				}
				$result = $client->getAccessToken($tokenEndPointUrl,  $code, $redirect_uri, $grant_type);
				
				$updatefield = array();
				$updatefield['access_token'] = $result['access_token'];
				$updatefield['refresh_token'] = $result['refresh_token'];
				$updatess = $this->quickbook_model->update_quickbookdetails($updatefield,1);
				
				$data = array();
				$data['status'] = "success";
				$data['access_token'] = $result['access_token'];
				$data['refresh_token'] = $result['refresh_token'];
				
				$layout_data['content_body'] = $this->load->view('quickbook/quickbook_connect', $data, TRUE);
				$layout_data['page_title'] = 'Quickbook Connection';
				$layout_data['meta_description'] = 'Quickbook Connection';
				$layout_data['css_files'] = array();
				$layout_data['quickbook_active'] = '1'; 
				$layout_data['external_js_files'] = array();
				$layout_data['js_files'] = array('');
				$this->load->view('layouts/default', $layout_data);
			}
		} else {
			$data = array();
			$data['status'] = "error";
			$layout_data['content_body'] = $this->load->view('quickbook/quickbook_connect', $data, TRUE);
			$layout_data['page_title'] = 'Quickbook Connection';
			$layout_data['meta_description'] = 'Quickbook Connection';
			$layout_data['css_files'] = array();
			$layout_data['quickbook_active'] = '1'; 
			$layout_data['external_js_files'] = array();
			$layout_data['js_files'] = array('');
			$this->load->view('layouts/default', $layout_data);
		}
	}
	
	public function refresh_token()
	{
		require(QUICK_CLIENT_PATH);
		$quickbookvalues = get_quick_book_values();
		$client_id = $quickbookvalues->quickBookClientId;
		$client_secret = $quickbookvalues->quickBookClientSecret;
		
		if($client_id != "" && $client_secret != "")
		{
			$tokenEndPointUrl = QUICK_TOKEN_END_URL;
			$grant_type= 'refresh_token';
			$certFilePath = QUICK_CERTIFICATE_PATH;
			$refresh_token = $quickbookvalues->refresh_token;
			if($refresh_token != "")
			{
				$client = new Client($client_id, $client_secret, $certFilePath);
				$result = $client->refreshAccessToken($tokenEndPointUrl, $grant_type, $refresh_token);
				if(isset($result['error']))
				{
					$response = array();
					$response['status'] = "error";
					$response['message'] = "Token not generated.";
					echo json_encode($response);
					exit();
				} else {
					$updatefield = array();
					$updatefield['access_token'] = $result['access_token'];
					$updatefield['refresh_token'] = $result['refresh_token'];
			 		$updatess = $this->quickbook_model->update_quickbookdetails($updatefield,1);
					
					$response = array();
					$response['status'] = "success";
					$response['message'] = "Token generated successfully.";
					echo json_encode($response);
					exit();
				}
			} else {
				$response = array();
				$response['status'] = "error";
				$response['message'] = "Token not available.";
				echo json_encode($response);
				exit();
			}
		} else {
			$response = array();
			$response['status'] = "error";
			$response['message'] = "Client Keys not available.";
			echo json_encode($response);
			exit();
		}
	}
	
	public function sync_customers()
	{
		$data = array();
        // $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = get_quick_book_values();
        $layout_data['content_body'] = $this->load->view('quickbook/quickbook_customer_list', $data, TRUE);
        $layout_data['page_title'] = 'Customers';
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'quickbook_customer.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function list_ajax_customer_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        // $useractive = $_POST['useractive'];
        // $regdate = $_POST['regdate'];
        // $regdateto = $_POST['regdateto'];
        // $paytype = $_POST['paytype'];
        // $sourceval = $_POST['sourceval'];
        // $customertype = $_POST['customertype'];
        $keywordsearch = $_POST['keywordsearch'];

        $recordsTotal = $this->quickbook_model->count_all_customers();
        $recordsTotalFilter = $this->quickbook_model->get_all_customersnew($keywordsearch);

        $orders = $this->quickbook_model->get_all_newcustomers($columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $keywordsearch);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
	
	public function sync_all_customer_quickbooks()
	{
		$quickbookvalues = get_quick_book_values();
		$sync_cust_count=0;$error_cust_count=0;$error_array=array();
		if($quickbookvalues->enableQuickBook == "1")
		{
			if($quickbookvalues->access_token != "")
			{
				$get_all_customers = $this->quickbook_model->get_all_newcustomers_for_sync();
				$total_count=count($get_all_customers);
				foreach($get_all_customers as $val)
				{
					$customer_array = array();
					$customer_array['DisplayName'] = $val['name'];
					$customer_array['PrimaryPhone'] = array(
						"FreeFormNumber" => $val['mobile']
					);
					if($val['email'] != "")
					{
						$customer_array['PrimaryEmailAddr'] = array(
							"Address" => $val['email']
						);
					}
					
					$addresssss = $val['address'];
					
					$customer_array['BillAddr'] = array(
						"City" => $val['area'],
						"Line1" => $addresssss,
						"Country" => "United Arab Emirates"
					);
								
					$input_json = json_encode($customer_array);
					$headerarray = array(
						"Accept: application/json",
						"Content-Type: application/json",
						"Authorization: Bearer ".$quickbookvalues->access_token
					);
					//print_r(json_encode($headerarray));exit();
					$ch = curl_init();
					// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
					$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						// echo $error_msg = curl_error($ch);
						// exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);
					// print_r($json);exit();
					if(!empty($json['Customer']))
					{
						if($json['Customer']['Id'] != "")
						{
							$updatefield = array();
							$updatefield['quickbook_id'] = $json['Customer']['Id'];
							$updatefield['quickbookTokenId'] = $json['Customer']['SyncToken'];
							$updatefield['quickbook_sync_status'] = 1;
							$updatess = $this->customers_model->update_booktype($updatefield,$val['id']);
							$sync_cust_count++;
						}
					}
								
					if(isset($json['fault']))
					{
						$error_array[$error_cust_count]['customerid']=$val['id'];
						$error_array[$error_cust_count]['customer_name']=$val['name'];
						$error_array[$error_cust_count]['customer_mobile']=$val['mobile'];
						$error_array[$error_cust_count]['err_reason']=$json['fault']['error'][0]['detail'].$json['fault']['error'][0]['code'];
						$error_cust_count++;				
					} else if(isset($json['Fault']))
					{
						$error_array[$error_cust_count]['customerid']=$val['id'];
						$error_array[$error_cust_count]['customer_name']=$val['name'];
						$error_array[$error_cust_count]['customer_mobile']=$val['mobile'];
						$error_array[$error_cust_count]['err_reason']=$json['Fault']['Error'][0]['Detail'].$json['Fault']['Error'][0]['code'];
						$error_cust_count++;
					}
				}
				
				$msg=$sync_cust_count.'/'.$total_count.' customers synced to QuickBook';

				$err_sl=1;
				if($error_cust_count>0)
				{
					$msg.="<br/><br/><div class='text-left'>Following customer have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th>
						<th style='text-align:center;'>Name</th><th style='text-align:center;'>Mob</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
					 $burl=base_url();
					foreach ($error_array as $err) 
						{
							$custid=$err['customerid'];
							$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['customer_mobile']."</td><td style='text-align:center;'>".$err['err_reason']."</td></tr>";
							$err_sl++;
						}    
					$msg.="</tbody></table>";
					
				}
				$msgvar = $msg;
				$msg=json_encode($msg);
				//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
				$msgdata=array();
				$msgdata['tmp_msg_content']=$msg;
				$msgdata['tmp_msg_stat']=1;
				$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,1);
				
				$ret_array = array();
				$ret_array['status'] = 'success';
				$ret_array['message'] = $msgvar;
				echo json_encode($ret_array);		
			} else {
				$ret_array = array();
				$ret_array['status'] = 'error';
				$ret_array['message'] = 'Token not available. Quickbook sync failed.';
				echo json_encode($ret_array);
			}
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Quickbooks not enabled.';
			echo json_encode($ret_array);
		}
	}
	
	public function sync_invoices()
	{
		$data = array();
        // $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = get_quick_book_values();
		$data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('quickbook/quickbook_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Invoices';
        $layout_data['meta_description'] = 'Invoices';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'quickbook_invoice.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function list_ajax_quickbook_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        // $useractive = $_POST['useractive'];
        // $regdate = $_POST['regdate'];
        // $regdateto = $_POST['regdateto'];
        // $paytype = $_POST['paytype'];
        // $sourceval = $_POST['sourceval'];
        // $customertype = $_POST['customertype'];
		$custid = $_POST['custid'];
        $keywordsearch = $_POST['keywordsearch'];

        $recordsTotal = count($this->quickbook_model->count_all_invoices());
        $recordsTotalFilter = count($this->quickbook_model->get_all_invoicesnew($keywordsearch,$custid));

        $orders = $this->quickbook_model->get_all_newinvoices($columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $keywordsearch,$custid);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
	
	public function sync_all_invoices_quickbooks()
	{
		$custid = $this->input->post('custid');
		$search = $this->input->post('search');
		$quickbookvalues = get_quick_book_values();
		$sync_inv_count=0;$error_inv_count=0;$error_array=array();
		if($quickbookvalues->enableQuickBook == "1")
		{
			if($quickbookvalues->access_token != "")
			{
				$get_all_invoices = $this->quickbook_model->get_all_newinvoices_for_sync($custid,$search);
				// echo '<pre>';
				// print_r($get_all_invoices);
				// exit();
				$total_count=count($get_all_invoices);
				if($total_count > 0)
				{
					foreach($get_all_invoices as $val)
					{
						if($val['customerquickbook_sync_status'] == 0)
						{
							$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
							$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
							$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
							$error_array[$error_inv_count]['invoice_error']="Customer not synced to quickbook";
							$error_inv_count++;
							// continue;
						} else {
							$customer_array = array();
							$line = array();
							$line_ctr=0;
							foreach($val['invoice_lines'] as $lineval)
							{
								$line[$line_ctr]['DetailType']='SalesItemLineDetail';
								$line[$line_ctr]['Amount']=$lineval->line_net_amount;
								$line[$line_ctr]['Description']=$lineval->description;
								$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$lineval->service_date;
								$line[$line_ctr]['SalesItemLineDetail']['TaxCodeRef']['value']="TAX";
								// $line[$line_ctr]['SalesItemLineDetail']['Qty']=$lineval->service_hrs;
								// $line[$line_ctr]['SalesItemLineDetail']['UnitPrice']=$lineval->inv_unit_price;
								$line_ctr++;
							}

							$customer_array=array();
							$custquickid = $val['customer_quickbook_id'];
							$customer_array['Line'] = $line;
							$customer_array['CustomerRef'] = array('value' => $custquickid);
							$customer_array['TotalAmt'] = $val['invoice_net_amount'];
							$customer_array['TxnDate'] = $val['invoice_date'];
							$customer_array['DueDate'] = $val['invoice_due_date'];
							$customer_array['DocNumber'] = $val['invoice_num'];
							$customer_array['TxnTaxDetail'] = array('TotalTax' => $val['invoice_tax_amount']);
							$customer_array['BillEmail'] = array('Address' => $val['email']);
							
							$input_json = json_encode($customer_array);
							$headerarray = array(
								"Accept: application/json",
								"Content-Type: application/json",
								"Authorization: Bearer ".$quickbookvalues->access_token
							);
							
							$ch = curl_init();
							// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/invoice?minorversion=57";
							$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/invoice?minorversion=57";
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							$output = curl_exec($ch); 
							if (curl_errno($ch)) {
								// echo $error_msg = curl_error($ch);
								// exit();
							}
							curl_close($ch);
							$json = json_decode($output, true);
							
							if(isset($json['Invoice']))
							{
								$inv_fields=array();
								$invoice_qbid= $json['Invoice']['Id'];
								$inv_fields['invoice_qb_id']= $invoice_qbid;
								$inv_fields['invoice_qb_syncId']= $json['Invoice']['SyncToken'];
								$inv_fields['invoice_qb_sync_stat']=1;
								$this->quickbook_model->update_invoicepay_detail($val['invoice_id'],$inv_fields);
								$sync_inv_count++;
								
								// $invstat=$val['invoice_paid_status'];
								// if($invstat==1||$invstat==2)
								// {
									// $invid = $val['invoice_num'];
									// $invoice_payments = $this->quickbook_model->get_invoice_paymentdetail_sync($val['invoice_id']);
									// $ii=0;
									// foreach ($invoice_payments as $payment) 
									// {
										// $payment_array=array();
										// $payment_array['TotalAmt'] = $payment->paid_amount;
										// $payment_array['CustomerRef'] = array('value' => $custquickid);
										// $payment_array['TxnDate'] = date('Y-m-d',strtotime($payment->paid_datetime));
										// $payment_array['PaymentRefNum'] = $payment->receipt_no;
										// if($payment->quickbook_payment_sync_status != 0)
										// {
											// $headerarray_new = array(
												// "Accept: application/json",
												// "Content-Type: application/json",
												// "Authorization: Bearer ".$quickbookvalues->access_token
											// );
											
											// $ch = curl_init();
											// $url_new = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/payment/".$payment->quickbook_payment_id."?minorversion=65";
											// curl_setopt($ch, CURLOPT_URL, $url_new);
											// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
											// curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray_new);
											// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
											// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
											// $output = curl_exec($ch); 
											// if (curl_errno($ch)) {
											// }
											// curl_close($ch);
											// $json_new = json_decode($output, true);
											// $payment_array['SyncToken'] = $json_new['Payment']['SyncToken'];
											// $payment_array['Id'] = $payment->quickbook_payment_id;
										// }
										// $payment_array['Line'][$ii]['LinkedTxn'][0]['TxnId']=$invoice_qbid;
										// $payment_array['Line'][$ii]['LinkedTxn'][0]['TxnType']='Invoice';
										// $payment_array['Line'][$ii]['Amount']= $payment->payment_amount;
										// $input_jsons = json_encode($payment_array);

										// $headerarray = array(
											// "Accept: application/json",
											// "Content-Type: application/json",
											// "Authorization: Bearer ".$quickbookvalues->access_token
										// );
															
										// $ch = curl_init();
										// $url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/payment?minorversion=57";
										// curl_setopt($ch, CURLOPT_URL, $url);
										// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
										// curl_setopt($ch, CURLOPT_POST, 1);
										// curl_setopt($ch, CURLOPT_POSTFIELDS, $input_jsons);
										// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
										// curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
										// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
										// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
										// $output = curl_exec($ch); 
										// if (curl_errno($ch)) {
										// }
										// curl_close($ch);
										// $json = json_decode($output, true);
										
										// if(isset($json['Payment']))
										// {
											// $inv_fields=array();
											// $inv_fields['qb_sync_stat']=1;
											// $this->quickbook_model->update_invoice_payment_by_ipmid($payment->inv_map_id,$inv_fields);

											// if($payment->quickbook_payment_sync_status == 0)
											// {
												// $pay_fields=array();
												// $pay_fields['quickbook_payment_id']=$json['Payment']['Id'];
												// $pay_fields['quickbookPaymentTokenId']=$json['Payment']['SyncToken'];
												// $pay_fields['quickbook_payment_sync_status']=1;
												// $this->quickbook_model->update_invoice_payment_by_payid($payment->paymentId,$pay_fields);
											// } else {
												// $pay_fields=array();
												// $pay_fields['quickbookPaymentTokenId']=$json['Payment']['SyncToken'];
												// $this->quickbook_model->update_invoice_payment_by_payid($payment->paymentId,$pay_fields);
											// }
										// }
									// }
								// }
							}
							if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
								$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
								$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
								$error_array[$error_inv_count]['invoice_error']=$json['Fault']['Error'][0]['Detail'].$json['Fault']['Error'][0]['code'];
								$error_inv_count++;
							}
							if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
								$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
								$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
								$error_array[$error_inv_count]['invoice_error']=$json['fault']['error'][0]['detail'].$json['fault']['error'][0]['code'];
								$error_inv_count++;
							}
						}
					} 
					$err_sl=1;
					$msg=$sync_inv_count.'/'.$total_count.' invoices synced to QuickBook';
					if($error_inv_count>0)
					{
						$msg.="<br/><br/><div class='text-left'>Following Invoice have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Number</th><th style='text-align:center;'>Customer</th><th style='text-align:center;'>Invoice Date</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
						foreach ($error_array as $err) 
							{
								$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['invoice']."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['invoice_date']."</td><td style='text-align:center;'>".$err['invoice_error']."</td></tr>";
								$err_sl++;
							}    
						$msg.="</tbody></table>";
						
					}

					$msgvar = $msg;
					$msg=json_encode($msg);
					//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
					$msgdata=array();
					$msgdata['tmp_msg_content']=$msg;
					$msgdata['tmp_msg_stat']=1;
					$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,2);//exit();
					
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = $msgvar;
					echo json_encode($ret_array);
					exit();
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = 'No invoice available';
					echo json_encode($ret_array);
					exit();
				}	
			} else {
				$ret_array = array();
				$ret_array['status'] = 'error';
				$ret_array['message'] = 'Token not available. Quickbook sync failed.';
				echo json_encode($ret_array);
				exit();
			}
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Quickbooks not enabled';
			echo json_encode($ret_array);
			exit();
		}	
	}
	
	public function sync_invoice_payments()
	{
		$data = array();
        // $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = get_quick_book_values();
		$data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('quickbook/quickbook_invoice_payment_list', $data, TRUE);
        $layout_data['page_title'] = 'Invoices';
        $layout_data['meta_description'] = 'Invoices';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'quickbook_invoice_payment.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function list_ajax_quickbook_payment_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        // $useractive = $_POST['useractive'];
        // $regdate = $_POST['regdate'];
        // $regdateto = $_POST['regdateto'];
        // $paytype = $_POST['paytype'];
        // $sourceval = $_POST['sourceval'];
        // $customertype = $_POST['customertype'];
		$custid = $_POST['custid'];
        $keywordsearch = $_POST['keywordsearch'];

        $recordsTotal = count($this->quickbook_model->count_all_invoice_payments());
        $recordsTotalFilter = count($this->quickbook_model->get_all_invoicepaymentsnew($keywordsearch,$custid));

        $orders = $this->quickbook_model->get_all_newinvoicepayments($columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $keywordsearch, $custid);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
	
	public function sync_all_invoice_payments_quickbooks()
	{
		$custid = $this->input->post('custid');
		$search = $this->input->post('search');
		$quickbookvalues = get_quick_book_values();
		$sync_inv_count=0;$error_inv_count=0;$error_array=array();
		if($quickbookvalues->enableQuickBook == "1")
		{
			if($quickbookvalues->access_token != "")
			{
				$get_all_invoices = $this->quickbook_model->get_all_newinvoicepayments_for_sync($custid,$search);
				$total_count=count($get_all_invoices);
				if($total_count > 0)
				{
					foreach($get_all_invoices as $val)
					{
						if($val['customer_quickbook_id'] == 0)
						{
							$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
							$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
							$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
							$error_array[$error_inv_count]['invoice_error']="Customer not synced to quickbook";
							$error_inv_count++;
							// continue;
						} else if($val['invoice_qb_id'] == 0)
						{
							$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
							$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
							$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
							$error_array[$error_inv_count]['invoice_error']="Invoice not synced to quickbook";
							$error_inv_count++;
						} else {
							$custquickid = $val['customer_quickbook_id'];
							$paymentquickid = $val['quickbookpaymentid'];
							$invoicequickid = $val['invoice_qb_id'];
							$txnDate = $val['amountpaiddate'];
							$payment_array=array();
							$payment_array['TotalAmt'] = $val['paidtotal'];
							$payment_array['CustomerRef'] = array('value' => $custquickid);
							$payment_array['TxnDate'] = date('Y-m-d',strtotime($txnDate));
							$payment_array['PaymentRefNum'] = $val['receipt'];
							if($paymentquickid > 0)
							{
								$headerarray_new = array(
									"Accept: application/json",
									"Content-Type: application/json",
									"Authorization: Bearer ".$quickbookvalues->access_token
								);
								
								$ch = curl_init();
								$url_new = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/payment/".$paymentquickid."?minorversion=65";
								curl_setopt($ch, CURLOPT_URL, $url_new);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray_new);
								curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
								$output = curl_exec($ch); 
								if (curl_errno($ch)) {
								}
								curl_close($ch);
								$json_new = json_decode($output, true);
								$payment_array['SyncToken'] = $json_new['Payment']['SyncToken'];
								$payment_array['Id'] = $paymentquickid;
							}
							if($val['paymethod'] == 0)
							{
								$payId = 1;
								$bankId = 93;
							} else if($val['paymethod'] == 1)
							{
								$payId = 9;
								$bankId = 94;
							} else if($val['paymethod'] == 2)
							{
								$payId = 10;
								$bankId = 95;
							} else if($val['paymethod'] == 3)
							{
								$payId = 10;
								$bankId = 95;
							} else if($val['paymethod'] == 4)
							{
								$payId = 9;
								$bankId = 94;
							}
							$payment_array['PaymentMethodRef'] = array('value' => $payId);
							$payment_array['DepositToAccountRef'] = array('value' => $bankId);
							
							$payment_array['Line'][0]['LinkedTxn'][0]['TxnId']=$invoicequickid;
							$payment_array['Line'][0]['LinkedTxn'][0]['TxnType']='Invoice';
							$payment_array['Line'][0]['Amount']= $val['allocatedamount'];
							$input_jsons = json_encode($payment_array);
							// echo $input_jsons;
							// exit();

							$headerarray = array(
								"Accept: application/json",
								"Content-Type: application/json",
								"Authorization: Bearer ".$quickbookvalues->access_token
							);
												
							$ch = curl_init();
							// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/payment?minorversion=57";
							$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/payment?minorversion=57";
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $input_jsons);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							$output = curl_exec($ch); 
							if (curl_errno($ch)) {
								// echo $error_msg = curl_error($ch);
								// exit();
							}
							curl_close($ch);
							$json = json_decode($output, true);
							// echo '<pre>';
							// print_r($output);//print_r($payment_array);exit();
							if(isset($json['Payment']))
							{
								$invoice_pay_id = $val['invoice_pay_id'];
								$inv_fields=array();
								$inv_fields['qb_sync_stat']=1;
								$this->quickbook_model->update_invoice_payment_by_ipmid($invoice_pay_id,$inv_fields);
								$sync_inv_count++;

								if($paymentquickid == 0)
								{
									$pay_fields=array();
									$pay_fields['quickbook_payment_id']=$json['Payment']['Id'];
									$pay_fields['quickbookPaymentTokenId']=$json['Payment']['SyncToken'];
									$pay_fields['quickbook_payment_sync_status']=1;
									$this->quickbook_model->update_invoice_payment_by_payid($val['paymentId'],$pay_fields);
								} else {
									$pay_fields=array();
									$pay_fields['quickbookPaymentTokenId']=$json['Payment']['SyncToken'];
									$this->quickbook_model->update_invoice_payment_by_payid($val['paymentId'],$pay_fields);
								}
							}
							
							
							if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
								$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
								$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
								$error_array[$error_inv_count]['invoice_error']=$json['Fault']['Error'][0]['Detail'].$json['Fault']['Error'][0]['code'];
								$error_inv_count++;
							}
							if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$val['invoice_num'];
								$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
								$error_array[$error_inv_count]['invoice_date']=$val['invoice_date'];
								$error_array[$error_inv_count]['invoice_error']=$json['fault']['error'][0]['detail'].$json['fault']['error'][0]['code'];
								$error_inv_count++;
							}
						}
					} 
					$err_sl=1;
					$msg=$sync_inv_count.'/'.$total_count.' invoice payments synced to QuickBook';
					if($error_inv_count>0)
					{
						$msg.="<br/><br/><div class='text-left'>Following Invoice Payments have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Number</th><th style='text-align:center;'>Customer</th><th style='text-align:center;'>Invoice Date</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
						foreach ($error_array as $err) 
							{
								$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['invoice']."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['invoice_date']."</td><td style='text-align:center;'>".$err['invoice_error']."</td></tr>";
								$err_sl++;
							}    
						$msg.="</tbody></table>";
						
					}

					$msgvar = $msg;
					$msg=json_encode($msg);
					//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
					$msgdata=array();
					$msgdata['tmp_msg_content']=$msg;
					$msgdata['tmp_msg_stat']=1;
					$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,4);//exit();
					
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = $msgvar;
					echo json_encode($ret_array);
					exit();
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = 'No invoice payment available';
					echo json_encode($ret_array);
					exit();
				}	
			} else {
				$ret_array = array();
				$ret_array['status'] = 'error';
				$ret_array['message'] = 'Token not available. Quickbook sync failed.';
				echo json_encode($ret_array);
				exit();
			}
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Quickbooks not enabled.';
			echo json_encode($ret_array);
			exit();
		}
	}
	
	public function sync_customer_payments()
	{
		$data = array();
        // $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = get_quick_book_values();
		$data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('quickbook/quickbook_customer_payment_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Payments';
        $layout_data['meta_description'] = 'Customer Payments';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'quickbook_payment.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function list_ajax_quickbookpayment_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        // $useractive = $_POST['useractive'];
        // $regdate = $_POST['regdate'];
        // $regdateto = $_POST['regdateto'];
        // $paytype = $_POST['paytype'];
        // $sourceval = $_POST['sourceval'];
        $custid = $_POST['custid'];
        $keywordsearch = $_POST['keywordsearch'];

        $recordsTotal = count($this->quickbook_model->count_all_payments());
        $recordsTotalFilter = count($this->quickbook_model->get_all_paymentsnew($keywordsearch,$custid));

        $orders = $this->quickbook_model->get_all_newpayments($columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $keywordsearch, $custid);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
	
	public function sync_all_payments_quickbooks()
	{
		$custid = $this->input->post('custid');
		$search = $this->input->post('search');
		$quickbookvalues = get_quick_book_values();
		$sync_inv_count=0;$error_inv_count=0;$error_array=array();
		if($quickbookvalues->enableQuickBook == "1")
		{
			if($quickbookvalues->access_token != "")
			{
				$get_all_invoices = $this->quickbook_model->get_all_newpayments_for_sync($custid,$search);
				$total_count=count($get_all_invoices);
				if($total_count > 0)
				{
					foreach($get_all_invoices as $val)
					{
						if($val['customerquickbook_sync_status'] == 0)
						{
							$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
							$error_array[$error_inv_count]['paid_date']=$val['paidtime'];
							$error_array[$error_inv_count]['payment_error']="Customer not synced to quickbook";
							$error_inv_count++;
							// continue;
						} else {
							if($val['paymethod'] == 0)
							{
								$payId = 1;
								$bankId = 93;
							} else if($val['paymethod'] == 1)
							{
								$payId = 9;
								$bankId = 94;
							} else if($val['paymethod'] == 2)
							{
								$payId = 10;
								$bankId = 95;
							} else if($val['paymethod'] == 3)
							{
								$payId = 10;
								$bankId = 95;
							} else if($val['paymethod'] == 4)
							{
								$payId = 9;
								$bankId = 94;
							}
							$custquickid = $val['customer_quickbook_id'];
							$paydate = $val['paidtime'];
							$payment_array=array();
							$payment_array['TotalAmt'] = $val['paidamount'];
							$payment_array['CustomerRef'] = array('value' => $custquickid);
							$payment_array['TxnDate'] = date('Y-m-d',strtotime($paydate));
							if($val['receipt_no'] != "")
							{
								$payment_array['PaymentRefNum'] = $val['receipt_no'];
							}
							$payment_array['PaymentMethodRef'] = array('value' => $payId);
							$payment_array['DepositToAccountRef'] = array('value' => $bankId);
							$input_jsons = json_encode($payment_array);

							$headerarray = array(
								"Accept: application/json",
								"Content-Type: application/json",
								"Authorization: Bearer ".$quickbookvalues->access_token
							);
												
							$ch = curl_init();
							$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/payment?minorversion=57";
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $input_jsons);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							$output = curl_exec($ch); 
							if (curl_errno($ch)) {
							}
							curl_close($ch);
							$json = json_decode($output, true);
							
							if(isset($json['Payment']))
							{
								if($val['quickbook_payment_sync_status'] == 0)
								{
									$pay_fields=array();
									$pay_fields['quickbook_payment_id']=$json['Payment']['Id'];
									$pay_fields['quickbookPaymentTokenId']=$json['Payment']['SyncToken'];
									$pay_fields['quickbook_payment_sync_status']=1;
									$this->quickbook_model->update_invoice_payment_by_payid($val['paymentId'],$pay_fields);
								} else {
									$pay_fields=array();
									$pay_fields['quickbookPaymentTokenId']=$json['Payment']['SyncToken'];
									$this->quickbook_model->update_invoice_payment_by_payid($val['paymentId'],$pay_fields);
								}
								$sync_inv_count++;
							}
							
							if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
								$error_array[$error_inv_count]['paid_date']=$val['paidtime'];
								$error_array[$error_inv_count]['payment_error']=$json['Fault']['Error'][0]['Detail'].$json['Fault']['Error'][0]['code'];
								$error_inv_count++;
							}
							if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['customer_name']=$val['customer_name'];
								$error_array[$error_inv_count]['paid_date']=$val['paidtime'];
								$error_array[$error_inv_count]['payment_error']=$json['fault']['error'][0]['detail'].$json['fault']['error'][0]['code'];
								$error_inv_count++;
							}
						}
					} 
					$err_sl=1;
					$msg=$sync_inv_count.'/'.$total_count.' payments synced to QuickBook';
					if($error_inv_count>0)
					{
						$msg.="<br/><br/><div class='text-left'>Following Payment have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Customer</th><th style='text-align:center;'>Payment Date</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
						foreach ($error_array as $err) 
							{
								$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['paid_date']."</td><td style='text-align:center;'>".$err['payment_error']."</td></tr>";
								$err_sl++;
							}    
						$msg.="</tbody></table>";
						
					}

					$msgvar = $msg;
					$msg=json_encode($msg);
					//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
					$msgdata=array();
					$msgdata['tmp_msg_content']=$msg;
					$msgdata['tmp_msg_stat']=1;
					$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,6);//exit();
					
					$ret_array = array();
					$ret_array['status'] = 'success';
					$ret_array['message'] = $msgvar;
					echo json_encode($ret_array);
					exit();
				} else {
					$ret_array = array();
					$ret_array['status'] = 'error';
					$ret_array['message'] = 'No payments available';
					echo json_encode($ret_array);
					exit();
				}	
			} else {
				$ret_array = array();
				$ret_array['status'] = 'error';
				$ret_array['message'] = 'Token not available. Quickbook sync failed.';
				echo json_encode($ret_array);
				exit();
			}
		} else {
			$ret_array = array();
			$ret_array['status'] = 'error';
			$ret_array['message'] = 'Quickbooks not enabled';
			echo json_encode($ret_array);
			exit();
		}	
	}
	
	public function read_invoice()
	{
		$quickbookvalues = get_quick_book_values();
		$headerarray_new = array(
			"Accept: application/json",
			"Content-Type: application/json",
			"Authorization: Bearer ".$quickbookvalues->access_token
		);
		
		$ch = curl_init();
		// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/invoice?minorversion=57";
		$url_new = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/invoice/214?minorversion=65";
		curl_setopt($ch, CURLOPT_URL, $url_new);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray_new);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch); 
		if (curl_errno($ch)) {
			// echo $error_msg = curl_error($ch);
			// exit();
		}
		curl_close($ch);
		$json_new = json_decode($output, true);
		echo '<pre>';
		print_r($json_new);
		exit();
	}
	
	public function payment_types()
	{
		$quickbookvalues = get_quick_book_values();
		$headerarray_new = array(
			"Accept: application/json",
			"Content-Type: application/json",
			"Authorization: Bearer ".$quickbookvalues->access_token
		);
		
		$ch = curl_init();
		// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/invoice?minorversion=57";
		$url_new = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/query?query=SELECT%20*%20FROM%20PaymentMethod&minorversion=65";
		curl_setopt($ch, CURLOPT_URL, $url_new);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray_new);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch); 
		if (curl_errno($ch)) {
			// echo $error_msg = curl_error($ch);
			// exit();
		}
		curl_close($ch);
		$json_new = json_decode($output, true);
		echo '<pre>';
		print_r($json_new);
		exit();
	}
	
	public function list_accounts()
	{
		$quickbookvalues = get_quick_book_values();
		$headerarray_new = array(
			// "Accept: application/json",
			"Content-Type: text/plain",
			"Authorization: Bearer ".$quickbookvalues->access_token
		);
		
		$ch = curl_init();
		// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/invoice?minorversion=57";
		$url_new = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/query?query=SELECT%20*%20FROM%Account&minorversion=40";
		curl_setopt($ch, CURLOPT_URL, $url_new);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray_new);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch); 
		if (curl_errno($ch)) {
			// echo $error_msg = curl_error($ch);
			// exit();
		}
		curl_close($ch);
		$json_new = json_decode($output, true);
		echo '<pre>';
		print_r($json_new);
		exit();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function create_customer()
	{
		$quickbookvalues = get_quick_book_values();
		if($quickbookvalues->access_token != "")
		{
			$get_customer_details = $this->quickbook_model->get_customers_detail(12);
			if(!empty($get_customer_details))
			{
				$customer_array = array();
				$customer_array['DisplayName'] = $get_customer_details->customer_name;
				$customer_array['PrimaryPhone'] = array(
					"FreeFormNumber" => $get_customer_details->mobile_number_1
				);
				if($get_customer_details->email_address != "")
				{
					$customer_array['PrimaryEmailAddr'] = array(
						"Address" => $get_customer_details->email_address
					);
				}
				$customer_array['BillAddr'] = array(
					"City" => $get_customer_details->area_name,
					"Line1" => $get_customer_details->customer_address,
					"Country" => "United Arab Emirates"
				);
				
				$input_json = json_encode($customer_array);
				$headerarray = array(
					"Accept: application/json",
					"Content-Type: application/json",
					"Authorization: Bearer ".$quickbookvalues->access_token
				);
				
				$ch = curl_init();
				// $url = "https://quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=57";
				$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/customer?minorversion=65";
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$output = curl_exec($ch); 
				if (curl_errno($ch)) {
					echo $error_msg = curl_error($ch);
					exit();
				}
				// curl_close($ch);
				// echo $output;
				// exit();
				$json = json_decode($output, true);
				
				if(!empty($json['Customer']))
				{
					if($json['Customer']['Id'] != "")
					{
						$updatefield = array();
						$updatefield['quickbook_id'] = $json['Customer']['Id'];
						$updatefield['quickbook_sync_status'] = 1;
						$updatess = $this->customers_model->update_booktype($updatefield,$get_customer_details->customer_id);
					}
				}
				
				
				echo '<pre>';
				print_r($json['Customer']['Balance']);
				exit();
				//echo "Token Done. Please connect to quickbooks first";
				exit();
			} else {
				echo "Customer Not Found.";
				exit();
			}
		} else {
			echo "Connect to quickbook.";
			exit();
		}
	}
	
	public function index_old()
	{
		$quickbookvalues = get_quick_book_values();
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);
		
		$headerarray = array(
			"Accept: application/json",
			"Content-Type: application/json",
			"Authorization: Bearer ".$quickbookvalues->access_token
		);
		
		$ch = curl_init();
        // $url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/query?query=select%20*%20from%20Customer";
        // $url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/query?query=SELECT%20*%20FROM%20Customer%20ORDERBY%20Id%20ASC%20STARTPOSITION%205651%20MAXRESULTS%20100";
        $url = "https://sandbox-quickbooks.api.intuit.com/v3/company/".$quickbookvalues->quickbookCompanyId."/query?query=SELECT%20*%20FROM%20Customer";
        // $url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/companyinfo/9130350203752886?minorversion=12";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch); 
		if (curl_errno($ch)) {
			echo $error_msg = curl_error($ch);
			exit();
		}
        curl_close($ch);
        $json = json_decode($output, true);
		echo '<pre>';
		print_r($json);
		exit();
		if(empty($json['QueryResponse']))
		{
			echo 'hai';
			exit();
		} else {
			$customerlist = $json['QueryResponse']['Customer'];
			foreach($customerlist as $customerval)
			{
				$quickbookid = $customerval['Id'];
				$balance = $customerval['Balance'];
				$FullyQualifiedName = $customerval['FullyQualifiedName'];
				$DisplayName = $customerval['DisplayName'];
				$PrintOnCheckName = $customerval['PrintOnCheckName'];
				$phone = $customerval['PrimaryPhone']['FreeFormNumber'];
				$data = array(
					'quickbook_id' => $quickbookid,
					'FullyQualifiedName' => $FullyQualifiedName,
					'DisplayName' => $DisplayName,
					'PrintOnCheckName' => $PrintOnCheckName,
					'PrimaryPhone' => $phone,
					'balance' => $balance,
					'status' => 1
				);
				
				//$insert_id = $this->quickbook_model->add_quick_customers($data);
				
				if($insert_id > 0)
				{
					echo 'Inserted - '.$quickbookid;
					echo '<br>';
				} else {
					echo 'Not Inserted - '.$quickbookid;
					echo '<br>';
				}
			}
		}
		// echo '<pre>';
		// print_r($json['QueryResponse']['Customer']);
		exit();
	}


	public function quickbook_sync_customers()
	{
		
		$data = array();
		$data['customers_count']=$this->customers_model->fetch_customers_for_quickbook_count();
		$data['customers']=$this->customers_model->fetch_customers_for_quickbook_sync();
		$data['qb_sync_msg']=$this->quickbook_model->get_quickbook_cust_sync_msg();
		$this->quickbook_model->update_quickbook_cust_sync_msg(array('tmp_msg_stat' => 0 ),1);
        $layout_data['content_body'] = $this->load->view('quickbook_sync_customers', $data, TRUE);
		$layout_data['page_title'] = 'Quickbook Sync Customers';
		$layout_data['meta_description'] = 'Quickbook Sync Customers';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['quickbook_active'] = '1'; 
		$layout_data['js_files'] = array('');
		$this->load->view('layouts/default', $layout_data);
	}
	
	
	public function quickbook_sync_customer_process()
	{
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);
		$customer_ids=$this->input->post('customer_ids');
		$customer_ids=explode(",",$customer_ids);
		$sync_cust_count=0;$error_cust_count=0;$error_array=array();
		$total_count=count($customer_ids);
		if($total_count>0)
		{	
			$get_access_token = $this->quickbook_model->get_quickbookdetails(1);

			if($get_access_token->access_token != "")
			{
				foreach ($customer_ids as $customer_id) 
				{
					$get_customer_details = $this->quickbook_model->get_customers_detail($customer_id);
					if(!empty($get_customer_details))
					{
						$customer_array = array();
						$customer_array['DisplayName'] = $get_customer_details->customer_name;
						$customer_array['PrimaryPhone'] = array(
							"FreeFormNumber" => $get_customer_details->mobile_number_1
						);
						if($get_customer_details->email_address != "")
						{
							$customer_array['PrimaryEmailAddr'] = array(
								"Address" => $get_customer_details->email_address
							);
						}

						$customer_array['BillAddr'] = array(
							"City" => $get_customer_details->area_name,
							"Line1" => $get_customer_details->customer_address,
							"Country" => "United Arab Emirates"
						);
						
						$input_json = json_encode($customer_array);
						$headerarray = array(
							"Accept: application/json",
							"Content-Type: application/json",
							"Authorization: Bearer ".$get_access_token->access_token
						);
						//print_r(json_encode($headerarray));exit();
						$ch = curl_init();
						$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/customer?minorversion=57";
						//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/customer?minorversion=57";
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
						$output = curl_exec($ch); 
						if (curl_errno($ch)) {
							echo $error_msg = curl_error($ch);
							exit();
						}
						curl_close($ch);
						$json = json_decode($output, true);
						//print_r($output);//exit();
						if(!empty($json['Customer']))
						{
							if($json['Customer']['Id'] != "")
							{
								$updatefield = array();
								$updatefield['quickbook_id'] = $json['Customer']['Id'];
								$updatefield['quickbook_sync_status'] = 1;
								$updatess = $this->customers_model->update_booktype($updatefield,$get_customer_details->customer_id);
								$sync_cust_count++;
							}
						}
						
						if(isset($json['fault']))
						{
							if($json['fault']['error'][0]['code']=='3200')
								{
									//echo "Connect to quickbook";exit();
									redirect("quickbook",'refresh');exit();
								}							
						}
						// echo '<pre>';
						// print_r($json);exit();
						if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";

								$error_array[$error_cust_count]['customerid']=$customer_id;
								$error_array[$error_cust_count]['customer_name']=$get_customer_details->customer_name;
								$error_array[$error_cust_count]['customer_mobile']=$get_customer_details->mobile_number_1;
								//$error_array[$error_cust_count]['customer_email']=$get_customer_details->email_address;
								$error_array[$error_cust_count]['err_reason']=$json['Fault']['Error'][0]['Message'].$json['Fault']['Error'][0]['code'];
								$cust_addr_dtl=$customer_array['BillAddr'];
								$cust_addr_dtl = implode(', ', array_map(
										    function ($v, $k) { return sprintf("%s-'%s'", $k, $v); },
										    $cust_addr_dtl,
										    array_keys($cust_addr_dtl)
										));
								$error_array[$error_cust_count]['customer_address']=$cust_addr_dtl;
								$error_cust_count++;

								if($json['Fault']['Error'][0]['code']=='3200')
								{
									//echo "Connect to quickbook";exit();
									redirect("quickbook",'refresh');exit();
								}

							}


						 //print_r($json);echo "<br/><br/>";
						// exit();
						
						
					} 
					else 
					{
						//print_r($get_customer_details);echo "<br/>";
						//echo $this->db->last_query();
						echo "Customer Not Found.ID - ".$customer_id;
						//exit();
					}

					//print_r($get_customer_details);exit();
				}

				$msg=$sync_cust_count.'/'.$total_count.' customers synced to QuickBook';

				$err_sl=1;
				if($error_cust_count>0)
				{
					$msg.="<br/><br/><div class='text-left'>Following customer have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th>
                        <th style='text-align:center;'>ID</th><th style='text-align:center;'>Name</th><th style='text-align:center;'>Mob</th><th style='text-align:center;'>Email</th><th style='text-align:center;'>Reason</th><th style='text-align:center;'>Action</th></tr></thead><tbody>";
                     $burl=base_url();
                    foreach ($error_array as $err) 
                    	{
                    		$custid=$err['customerid'];
                        	$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'><a href='".$burl."customer/view/".$custid."' target='_blank' title='Click to view customer details'>".$custid."</a></td><td style='text-align:center;'><a href='".$burl."customer/view/".$custid."' target='_blank' title='Click to view customer details' class='custname_".$custid."'>".$err['customer_name']."</a></td><td style='text-align:center;'>".$err['customer_mobile']."</td><td style='text-align:center;'>".$err['customer_email']."</td><td style='text-align:center;'>".$err['err_reason']."</td><td style='text-align:center;' class='cust_action_".$custid."'><a><i class='fa fa-edit edt_cust' style='font-size:24px' data-custid='".$custid."'></i></a>&nbsp;&nbsp;&nbsp;<a><i class='fa fa-ban dis_cust' style='font-size:24px' data-custid='".$custid."'></i></a></td></tr>";
                        	$err_sl++;
                        }    
                    $msg.="</tbody></table>";
                    
				}

				//echo $msg;
				//exit();
				$msg=json_encode($msg);
				//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
				$msgdata=array();
				$msgdata['tmp_msg_content']=$msg;
				$msgdata['tmp_msg_stat']=1;
				$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,1);//exit();
				redirect("quickbook/quickbook_sync_customers",'refresh');
				
			} 
			else 
			{
				echo "Connect to quickbook.";
				exit();
			}
		}
		else 
			{
				echo "0 customers";
				exit();
			}


		//print_r($customer_ids);
	}

	public function quickbook_update_custname()
	{
		$custid   = $this->input->post('custid');
		$custname = $this->input->post('custname');

		$data=array();
		$data['customer_name']=$custname;
		$affected_rows=$this->customers_model->update_customers($data, $custid);
		echo $affected_rows;
		exit();
	}

	public function quickbook_disable_syncshowstat()
	{
		$custid   = $this->input->post('custid');
		$data=array();
		$data['quickbook_sync_show_status']=0;
		$affected_rows=$this->customers_model->update_customers($data, $custid);
		echo $affected_rows;
		exit();
	}


  public function quickbook_sync_customer_process1()
	{
		$error_array=array();
		$error_array[0]['customerid']=123;
		$error_array[0]['customer_name']='Jose Robert';
		$error_array[0]['customer_mobile']='8907449846';
		$error_array[0]['customer_email']='jose@gmail.com';
		$cust_addr_dtl = 'address testingg address testingg address testingg address testingg';
		$error_array[0]['customer_address']=$cust_addr_dtl;


		$error_array[1]['customerid']=123;
		$error_array[1]['customer_name']='Prinu Anna';
		$error_array[1]['customer_mobile']='9495483753';
		$error_array[1]['customer_email']='prinu@gmail.com';
		$cust_addr_dtl = 'address testingg address testingg address testingg address testingg';
		$error_array[1]['customer_address']=$cust_addr_dtl;

		$data['error_cust']=$error_array;
		$this->load->library('email');
		$html = $this->load->view('amountdue_template_email', $data, True);
		//$config['mailtype'] = 'html';
		//$config['charset'] = 'iso-8859-1';
		//$this->email->initialize($config);
        
        
        $config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'husam@servicemaster.ae',
			'smtp_pass' => 'joan200214463899',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('husam@servicemaster.ae', 'Service Master');
		$this->email->to($email);
		//$this->email->to('jose.robert@azinova.info');
		$this->email->subject($subject);
		$this->email->message($html);
		$this->email->send();
		echo 'success';

	}

  public function quickbook_sync_invoice() 
	{
		$data = array();
        $date_from = date('Y-m-d');
		$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
		
		if($this->input->post())
		{			
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			}
		}
		
        $data['invoice_count']=$this->quickbook_model->get_customer_invoices_for_qb_sync_count($date_from);
        $data['invoice_report']=$this->quickbook_model->get_customer_invoices_for_qb_sync($date_from);
        $data['qb_sync_msg']=$this->quickbook_model->get_quickbook_inv_sync_msg();
        $this->quickbook_model->update_quickbook_cust_sync_msg(array('tmp_msg_stat' => 0 ),2);
        $layout_data['content_body'] = $this->load->view('quickbook_customer_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Quickbook Sync Invoice';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['quickbook_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function quickbook_sync_invoice_process() 
	{
		$invoice_ids=$this->input->post('invoice_ids');
		$invoice_ids=explode(",",$invoice_ids);
		$sync_inv_count=0;$error_inv_count=0;$error_array=array();
		$total_count=count($invoice_ids);
		//echo $total_count;
		if($total_count>0)
		{	
			$get_access_token = $this->quickbook_model->get_quickbookdetails(1);
			if($get_access_token->access_token != "")
			{
				foreach ($invoice_ids as $invoice_id) 
				{
					$invoice_detail = $this->quickbook_model->get_invoice_detailbyid($invoice_id);
					if($invoice_detail[0]->quickbook_id==0)
					{
						//echo "Customer not synced to quickbook";
						//exit();
						$error_array[$error_inv_count]['invoice']=$invoice_detail[0]->invoice_num;
						$error_array[$error_inv_count]['customer_name']=$invoice_detail[0]->customer_name;
						$error_array[$error_inv_count]['invoice_date']=$invoice_detail[0]->invoice_date;
						$error_array[$error_inv_count]['invoice_error']="Customer not synced to quickbook";
						$error_inv_count++;
						continue;
					}

					$customer_array = array();$line = array();
					$line_ctr=0;
					foreach($invoice_detail as $jobs)
			        {

			        	$line[$line_ctr]['DetailType']='SalesItemLineDetail';
			        	$line[$line_ctr]['Amount']=$jobs->line_amount;
			        	$line[$line_ctr]['Description']=$jobs->description;
			        	if($jobs->qb_sync_stat=='1'&&$jobs->qb_sync_id>0)
			        	{
			        		$line[$line_ctr]['SalesItemLineDetail']['ItemRef']['value']=$jobs->qb_sync_id;
			        	}
			        	//$line[$line_ctr]['SalesItemLineDetail']['ItemRef']=array('name' => 'Services','value' => 1);
						if($invoice_detail[0]->jobdate != "")
						{
							$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$invoice_detail[0]->jobdate;
						} else {
							$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$invoice_detail[0]->newjobdate;
						}
			        	//$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$invoice_detail[0]->invoice_date;
			        	$line[$line_ctr]['SalesItemLineDetail']['TaxCodeRef']['value']=12;
			        	$line_ctr++;
			        }

			        $customer_array=array();
			        $customer_array['Line'] = $line;
					$customer_array['CustomerRef'] = array('value' => $invoice_detail[0]->quickbook_id);
					$customer_array['TxnDate'] = $invoice_detail[0]->invoice_date;
					$customer_array['DueDate'] = $invoice_detail[0]->invoice_due_date;
					$customer_array['DocNumber'] = $invoice_detail[0]->invoice_num;


					$input_json = json_encode($customer_array);

					//print_r($input_json);exit();
					$headerarray = array(
						"Accept: application/json",
						"Content-Type: application/json",
						"Authorization: Bearer ".$get_access_token->access_token
					);
		
					$ch = curl_init();
					$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/invoice?minorversion=57";
					//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/invoice?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						echo $error_msg = curl_error($ch);
						exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);
					//if(isset($json['fault'])){echo 'error';}
					if(isset($json['Fault']))
					{
							if($json['Fault']['Error'][0]['code']=='3200')
							{
								//echo "Connect to quickbook";exit();
								redirect("quickbook",'refresh');exit();
							}
					}		
					if(isset($json['fault']))
					{
						if($json['fault']['error'][0]['code']=='3200')
							{
								//echo "Connect to quickbook";exit();
								redirect("quickbook",'refresh');exit();
							}							
					}


					
					if(isset($json['Invoice']))
						{
							$inv_fields=array();
							$invoice_qbid=$json['Invoice']['Id'];
							$inv_fields['invoice_qb_id']=$invoice_qbid;
							$inv_fields['invoice_qb_sync_stat']=1;
							$this->quickbook_model->update_invoicepay_detail($invoice_id,$inv_fields);
							$sync_inv_count++;

							$invstat=$invoice_detail[0]->invoice_paid_status;
							if($invstat==1||$invstat==2)
							{
								$invnum=$invoice_detail[0]->invoice_num;
								$invoice_payments=$this->quickbook_model->get_invoice_payment_by_invnum($invnum);
								foreach ($invoice_payments as $payment) 
								{
									//echo $payment->payment_amount;payment_added_datetime
									$payment_array=array();
									$payment_array['TotalAmt'] = $payment->payment_amount;
									$payment_array['CustomerRef'] = array('value' => $invoice_detail[0]->quickbook_id);
									$payment_array['Line'][0]['LinkedTxn'][0]['TxnId']=$invoice_qbid;
									$payment_array['Line'][0]['LinkedTxn'][0]['TxnType']='Invoice';
									$payment_array['Line'][0]['Amount']= $payment->payment_amount;
									$payment_array['TxnDate'] = date('Y-m-d',strtotime($payment->payment_added_datetime));
									$input_json = json_encode($payment_array);

									$headerarray = array(
															"Accept: application/json",
															"Content-Type: application/json",
															"Authorization: Bearer ".$get_access_token->access_token
														);
														
									$ch = curl_init();
									$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
									//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
									curl_setopt($ch, CURLOPT_URL, $url);
									curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
									$output = curl_exec($ch); 
									if (curl_errno($ch)) {
										echo $error_msg = curl_error($ch);
										exit();
									}
									curl_close($ch);
									$json = json_decode($output, true);
									print_r($output);print_r($payment_array);exit();
									if(isset($json['Payment']))
									{
										// if($json['Payment']['UnappliedAmt']==0)
										// {
										// 	$inv_fields=array();
										// 	$inv_fields['qb_sync_stat']=1;
										// 	//$this->quickbook_model->update_invoicepay_detail($invoice_id,$inv_fields);
										// }

										$inv_fields=array();
										$inv_fields['qb_sync_stat']=1;
										$this->quickbook_model->update_invoice_payment_by_ipmid($payment->inv_map_id,$inv_fields);
									}
								}
								
							}
						}
					else if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$invoice_detail[0]->invoice_num;
								$error_array[$error_inv_count]['customer_name']=$invoice_detail[0]->customer_name;
								$error_array[$error_inv_count]['invoice_date']=$invoice_detail[0]->invoice_date;
								$error_array[$error_inv_count]['invoice_error']=$json['Fault']['Error'][0]['Message'];
								$error_inv_count++;
							}
					else if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$invoice_detail[0]->invoice_num;
								$error_array[$error_inv_count]['customer_name']=$invoice_detail[0]->customer_name;
								$error_array[$error_inv_count]['invoice_date']=$invoice_detail[0]->invoice_date;
								$error_array[$error_inv_count]['invoice_error']=$json['fault']['error'][0]['message'];
								$error_inv_count++;
							}
				}

				$err_sl=1;
				$msg=$sync_inv_count.'/'.$total_count.' invoices synced to QuickBook';
				if($error_inv_count>0)
				{
					$msg.="<br/><br/><div class='text-left'>Following Invoice have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Number</th><th style='text-align:center;'>Customer</th><th style='text-align:center;'>Invoice Date</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
                     $burl=base_url();
                    foreach ($error_array as $err) 
                    	{
                    		$custid=$err['invoice'];
                        	$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['invoice']."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['invoice_date']."</td><td style='text-align:center;'>".$err['invoice_error']."</td></tr>";
                        	$err_sl++;
                        }    
                    $msg.="</tbody></table>";
                    
				}

				//echo $msg;
				//exit();
				$msg=json_encode($msg);
				//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
				$msgdata=array();
				$msgdata['tmp_msg_content']=$msg;
				$msgdata['tmp_msg_stat']=1;
				$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,2);//exit();
				redirect("quickbook/quickbook_sync_invoice",'refresh');


			}
		}
	}


	public function quickbook_sync_invoices()
	{
		$get_access_token = $this->quickbook_model->get_quickbookdetails(1);
		$invoice_id=125;
		$invoice_detail = $this->quickbook_model->get_invoice_detailbyid($invoice_id);
		if($invoice_detail[0]->quickbook_id==0)
		{
			echo "Customer not synced to quickbook";
			exit();
		}
		$customer_array = array();$line = array();
		$line_ctr=0;
		foreach($invoice_detail as $jobs)
        {

        	$line[$line_ctr]['DetailType']='SalesItemLineDetail';
        	$line[$line_ctr]['Amount']=$jobs->line_amount;
        	$line[$line_ctr]['Description']=$jobs->description;
        	//$line[$line_ctr]['SalesItemLineDetail']['ItemRef']=array('name' => 'Services','value' => 1);
			if($invoice_detail[0]->jobdate != "")
			{
				$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$invoice_detail[0]->jobdate;
			} else {
				$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$invoice_detail[0]->newjobdate;
			}
        	// $line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$invoice_detail[0]->invoice_date;
        	$line[$line_ctr]['SalesItemLineDetail']['TaxCodeRef']['value']=12;
        	$line_ctr++;
        }


		$customer_array['Line'] = $line;
		$customer_array['CustomerRef'] = array('value' => $invoice_detail[0]->quickbook_id);
		$customer_array['TxnDate'] = $invoice_detail[0]->invoice_date;
		$customer_array['DueDate'] = $invoice_detail[0]->invoice_due_date;
		$customer_array['DocNumber'] = $invoice_detail[0]->invoice_num;

		$input_json = json_encode($customer_array);

		//print_r($input_json);exit();
		$headerarray = array(
			"Accept: application/json",
			"Content-Type: application/json",
			"Authorization: Bearer ".$get_access_token->access_token
		);
		
		$ch = curl_init();
		
		$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/invoice?minorversion=57";
		//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/invoice?minorversion=57";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch); 
		if (curl_errno($ch)) {
			echo $error_msg = curl_error($ch);
			exit();
		}
		curl_close($ch);
		$json = json_decode($output, true);
		//if(isset($json['fault'])){echo 'error';}
		if(isset($json['Fault']))
		{
				if($json['Fault']['Error'][0]['code']=='3200')
				{
					//echo "Connect to quickbook";exit();
					redirect("quickbook",'refresh');exit();
				}
		}		
		if(isset($json['fault']))
		{
			if($json['fault']['error'][0]['code']=='3200')
				{
					//echo "Connect to quickbook";exit();
					redirect("quickbook",'refresh');exit();
				}							
		}
		if(isset($json['Invoice']))
		{
			$inv_fields=array();
			$invoice_qbid=$json['Invoice']['Id'];
			$inv_fields['invoice_qb_id']=$invoice_qbid;
			$inv_fields['invoice_qb_sync_stat']=1;
			$this->quickbook_model->update_invoicepay_detail($invoice_id,$inv_fields);


			$invstat=$invoice_detail[0]->invoice_paid_status;
			if($invstat==1||$invstat==2)
			{
				$invnum=$invoice_detail[0]->invoice_num;
				$invoice_payments=$this->quickbook_model->get_invoice_payment_by_invnum($invnum);
				foreach ($invoice_payments as $payment) 
				{
					//echo $payment->payment_amount;payment_added_datetime
					$payment_array=array();
					$payment_array['TotalAmt'] = $payment->payment_amount;
					$payment_array['CustomerRef'] = array('value' => $invoice_detail[0]->quickbook_id);
					$payment_array['Line'][0]['LinkedTxn'][0]['TxnId']=$invoice_qbid;
					$payment_array['Line'][0]['LinkedTxn'][0]['TxnType']='Invoice';
					$payment_array['Line'][0]['Amount']= $payment->payment_amount;
					$payment_array['TxnDate'] = date('Y-m-d',strtotime($payment->payment_added_datetime));
					$input_json = json_encode($payment_array);

					$headerarray = array(
											"Accept: application/json",
											"Content-Type: application/json",
											"Authorization: Bearer ".$get_access_token->access_token
										);
										
					$ch = curl_init();
					$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
					//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						echo $error_msg = curl_error($ch);
						exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);
					//print_r($output);
					if(isset($json['Payment']))
					{
						if($json['Payment']['UnappliedAmt']==0)
						{
							$inv_fields=array();
							$inv_fields['qb_sync_stat']=1;
							$this->quickbook_model->update_invoice_payment_by_ipmid($payment->inv_map_id,$inv_fields);
							//$this->quickbook_model->update_invoicepay_detail($invoice_id,$inv_fields);
						}
					}
				}
				
			}
		}
		print_r($output);
	}

	public function quickbook_sync_invoice_payments()
	{
		$get_access_token = $this->quickbook_model->get_quickbookdetails(1);
		$invoice_id=125;
		$invoice_detail = $this->quickbook_model->get_invoice_detailbyid($invoice_id);
		$invstat=$invoice_detail[0]->invoice_paid_status;
		if($invstat==1||$invstat==2)
		{
			$invnum=$invoice_detail[0]->invoice_num;
			$invoice_payments=$this->quickbook_model->get_invoice_payment_by_invnum($invnum);
			foreach ($invoice_payments as $payment) 
			{
				//echo $payment->payment_amount;payment_added_datetime
				$payment_array=array();
				$payment_array['TotalAmt'] = $payment->payment_amount;
				$payment_array['CustomerRef'] = array('value' => $invoice_detail[0]->quickbook_id);
				$payment_array['Line'][0]['LinkedTxn'][0]['TxnId']=21529;
				$payment_array['Line'][0]['LinkedTxn'][0]['TxnType']='Invoice';
				$payment_array['Line'][0]['Amount']= $payment->payment_amount;
				$payment_array['TxnDate'] = date('Y-m-d',strtotime($payment->payment_added_datetime));
				$input_json = json_encode($payment_array);

				$headerarray = array(
										"Accept: application/json",
										"Content-Type: application/json",
										"Authorization: Bearer ".$get_access_token->access_token
									);
									
				$ch = curl_init();
				$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
				//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$output = curl_exec($ch); 
				if (curl_errno($ch)) {
					echo $error_msg = curl_error($ch);
					exit();
				}
				curl_close($ch);
				$json = json_decode($output, true);
				print_r($output);
			}
			
		}
	}


	public function quickbook_sync_invoicee()
	{
		$data = array();
        $date = NULL;
		$data['date'] = NULL;

		if($this->input->post())
		{
			if($this->input->post('invoice_date') != "")
			{
				$date = $this->input->post('invoice_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $date)));
				$data['date'] = date('d/m/Y', strtotime($date_from));
			} 
			else 
			{
				$date = NULL;
				$data['date'] = NULL;
			}
		}

		$data['invoice_report']=$this->quickbook_model->get_customer_invoices($date);
		$layout_data['content_body'] = $this->load->view('customer_invoice_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Invoices';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['reports_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
	}

	public function quickbook_sync_reciept() 
	{
		$data = array();
        $date_from = date('Y-m-d');
		$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
		
		if($this->input->post())
		{			
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			}
		}
		
        $data['invoice_count']=$this->quickbook_model->get_customer_reciepts_for_qb_sync_count($date_from);
        $data['invoice_report']=$this->quickbook_model->get_customer_reciepts_for_qb_sync($date_from);
        $data['qb_sync_msg']=$this->quickbook_model->get_quickbook_inv_sync_msg();
        $this->quickbook_model->update_quickbook_cust_sync_msg(array('tmp_msg_stat' => 0 ),2);
        $layout_data['content_body'] = $this->load->view('quickbook_sync_reciepts', $data, TRUE);
        $layout_data['page_title'] = 'Quickbook Sync Invoice';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['quickbook_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }


    public function quickbook_sync_quotations()
	{
		$data = array();
        $date_from = date('Y-m-d');
		$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
		
		if($this->input->post())
		{			
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			}
		}
		
        $data['quotation_count']=$this->quickbook_model->get_customer_quotations_for_qb_sync_count($date_from);
        $data['quotation_report']=$this->quickbook_model->get_customer_quotations_for_qb_sync($date_from);
        $data['qb_sync_msg']=$this->quickbook_model->get_quickbook_quo_sync_msg();
        $this->quickbook_model->update_quickbook_cust_sync_msg(array('tmp_msg_stat' => 0 ),3);
        $layout_data['content_body'] = $this->load->view('quickbook_customer_quotation_list', $data, TRUE);
        $layout_data['page_title'] = 'Quickbook Sync Quotation';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['quickbook_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
	}


	public function quickbook_sync_quotation_process()
	{
		
		$quotation_ids=$this->input->post('quotation_ids');
		$quotation_ids=explode(",",$quotation_ids);
		$sync_quo_count=0;$error_quo_count=0;$error_array=array();
		$total_count=count($quotation_ids);
		//echo $total_count;
		if($total_count>0)
		{	
			$get_access_token = $this->quickbook_model->get_quickbookdetails(1);
			if($get_access_token->access_token != "")
			{
				foreach ($quotation_ids as $quotation_id) 
				{
					$quotation_detail = $this->quickbook_model->get_quotation_detailbyid($quotation_id);
					if($quotation_detail->quickbook_id==0)
					{
						//echo "Customer not synced to quickbook";
						//exit();
						$error_array[$error_quo_count]['quotation']=$quotation_detail->quo_reference;
						$error_array[$error_quo_count]['customer_name']=$quotation_detail->customer_name;
						$error_array[$error_quo_count]['quotation_date']=date("d/m/Y",strtotime($quotation_detail->quo_date));
						$error_array[$error_quo_count]['quotation_error']="Customer not synced to quickbook";
						$error_quo_count++;
						continue;
					}

					$customer_array = array();$line = array();
					$line_ctr=0;
					$quotation_line_items = $this->invoice_model->get_quotation_line_items_byqid($quotation_id);
					 foreach($quotation_line_items as $line_item)
			         {
						$timefrom=date("h:i a",strtotime($quotation_detail->quo_time_from));
						$timeto=date("h:i a",strtotime($quotation_detail->quo_time_to));
			        	$line[$line_ctr]['DetailType']='SalesItemLineDetail';
			        	$amount=$line_item->qli_price/1.05;
			        	$line[$line_ctr]['Amount']=$amount;
			        	/*$line[$line_ctr]['Description']=$line_item->service_type_name.', Date '.$quotation_detail->quo_date.', From '.$timefrom.' To '.$timeto;*/
			        	$line[$line_ctr]['Description']=$line_item->qli_description;
			        	if($line_item->qb_sync_stat=='1'&&$line_item->qb_sync_id>0)
			        	{
			        		$line[$line_ctr]['SalesItemLineDetail']['ItemRef']['value']=$line_item->qb_sync_id;
			        	}
			        	//$line[$line_ctr]['SalesItemLineDetail']['ItemRef']=array('name' => 'Services','value' => 1);
			        	$line[$line_ctr]['SalesItemLineDetail']['Qty']=$line_item->qli_quantity;
			        	$line[$line_ctr]['SalesItemLineDetail']['UnitPrice']=$amount/$line_item->qli_quantity;
			        	$line[$line_ctr]['SalesItemLineDetail']['ServiceDate']=$quotation_detail->quo_date;
			        	$line[$line_ctr]['SalesItemLineDetail']['TaxCodeRef']['value']=12;
			        	$line_ctr++;
			         }

			        $customer_array=array();
			        $customer_array['Line'] = $line;
			        $customer_array['CustomerMemo']['value']='Fill the account details here';
					$customer_array['CustomerRef'] = array('value' => $quotation_detail->quickbook_id);
					$customer_array['TxnDate'] = $quotation_detail->quo_date;
					//$customer_array['DueDate'] = $quotation_detail->invoice_due_date;
					$customer_array['DocNumber'] = $quotation_detail->quo_reference;


					$input_json = json_encode($customer_array);

					//print_r($input_json);exit();
					$headerarray = array(
						"Accept: application/json",
						"Content-Type: application/json",
						"Authorization: Bearer ".$get_access_token->access_token
					);
		
					$ch = curl_init();
					$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/estimate?minorversion=57";
					//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/estimate?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						echo $error_msg = curl_error($ch);
						exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);
					//if(isset($json['fault'])){echo 'error';}
					//print_r($output);exit();
					if(isset($json['Fault']))
					{
							if($json['Fault']['Error'][0]['code']=='3200')
							{
								//echo "Connect to quickbook";exit();
								redirect("quickbook",'refresh');exit();
							}
					}		
					if(isset($json['fault']))
					{
						if($json['fault']['error'][0]['code']=='3200')
							{
								//echo "Connect to quickbook";exit();
								redirect("quickbook",'refresh');exit();
							}							
					}


					
					if(isset($json['Estimate']))
						{
							$quo_fields=array();
							$quo_qbid=$json['Estimate']['Id'];
							$quo_fields['quo_qb_id']=$quo_qbid;
							$quo_fields['quo_qb_sync_stat']=1;
							$this->quickbook_model->update_quotation($quotation_id,$quo_fields);
							$sync_quo_count++;

							
						}
					else if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_quo_count]['quotation']=$quotation_detail->quo_reference;
								$error_array[$error_quo_count]['customer_name']=$quotation_detail->customer_name;
								$error_array[$error_quo_count]['quotation_date']=date("d/m/Y",strtotime($quotation_detail->quo_date));
								$error_array[$error_quo_count]['quotation_error']=$json['Fault']['Error'][0]['Message'];
								$error_quo_count++;
							}
					else if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_quo_count]['quotation']=$quotation_detail->invoice_num;
								$error_array[$error_quo_count]['customer_name']=$quotation_detail->customer_name;
								$error_array[$error_quo_count]['quotation_date']=date("d/m/Y",strtotime($quotation_detail->quo_date));
								$error_array[$error_quo_count]['quotation_error']=$json['fault']['error'][0]['message'];
								$error_quo_count++;
							}
				}

				$err_sl=1;
				$msg=$sync_quo_count.'/'.$total_count.' quotations synced to QuickBook';
				if($error_quo_count>0)
				{
					$msg.="<br/><br/><div class='text-left'>Following Invoice have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Number</th><th style='text-align:center;'>Customer</th><th style='text-align:center;'>Quotation Date</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
                     $burl=base_url();
                    foreach ($error_array as $err) 
                    	{
                    		$custid=$err['quotation'];
                        	$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['quotation']."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['quotation_date']."</td><td style='text-align:center;'>".$err['quotation_error']."</td></tr>";
                        	$err_sl++;
                        }    
                    $msg.="</tbody></table>";
                    
				}

				//echo $msg;
				//exit();
				$msg=json_encode($msg);
				//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
				$msgdata=array();
				$msgdata['tmp_msg_content']=$msg;
				$msgdata['tmp_msg_stat']=1;
				$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,3);//exit();
				redirect("quickbook/quickbook_sync_quotations",'refresh');


			}
		}
	}



	public function quickbook_sync_invoice_payment() 
	{


		$data = array();
        $date_from = date('Y-m-d');
		$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
		
		if($this->input->post())
		{			
			if($this->input->post('vehicle_date') != "")
			{
				$from_date = $this->input->post('vehicle_date');
				$date_from = date('Y-m-d', strtotime(str_replace("/", "-", $from_date)));
				$data['search_date_from'] = date('d/m/Y', strtotime($date_from));
			}
		}
		
        $data['invoice_count']=$this->quickbook_model->get_invoice_payments_for_qb_sync_count($date_from);
        $data['invoice_report']=$this->quickbook_model->get_invoice_payments_for_qb_sync($date_from);
        $data['qb_sync_msg']=$this->quickbook_model->get_quickbook_inv_pay_sync_msg();
        $this->quickbook_model->update_quickbook_cust_sync_msg(array('tmp_msg_stat' => 0 ),4);
        $layout_data['content_body'] = $this->load->view('quickbook_invoice_payment_list', $data, TRUE);
        $layout_data['page_title'] = 'Quickbook Sync Invoice Payment';
        $layout_data['meta_description'] = 'reports';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['quickbook_active'] = '1'; 
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function quickbook_sync_invoice_payment_process()
    {
		$invoice_ids=$this->input->post('invoice_ids');
		$invoice_ids=explode(",",$invoice_ids);
		$sync_inv_count=0;$error_inv_count=0;$error_array=array();
		$total_count=count($invoice_ids);
		$error_array=array();
		if($total_count>0)
		{	
			$get_access_token = $this->quickbook_model->get_quickbookdetails(1);
			if($get_access_token->access_token != "")
			{
				foreach ($invoice_ids as $invoice_id) 
				{
					$invoice_detail = $this->quickbook_model->get_invoice_payment_detailbyid($invoice_id);
					if($invoice_detail->invoice_qb_sync_stat==0)
					{
						
						$error_array[$error_inv_count]['invoice']=$invoice_detail->inv_reference;
						$error_array[$error_inv_count]['customer_name']=$invoice_detail->customer_name;
						$error_array[$error_inv_count]['invoice_date']=date("d/m/Y",strtotime($invoice_detail->payment_added_datetime));
						$error_array[$error_inv_count]['invoice_error']="Invoice not synced to quickbook";
						$error_inv_count++;
						continue;
					}

					$customer_array = array();$line = array();
					$line_ctr=0;
					

					$payment_array=array();
					$payment_array['TotalAmt'] = $invoice_detail->payment_amount;
					$payment_array['CustomerRef'] = array('value' => $invoice_detail->cust_quickbook_id);
					$payment_array['Line'][0]['LinkedTxn'][0]['TxnId']=$invoice_detail->invoice_qb_id;
					$payment_array['Line'][0]['LinkedTxn'][0]['TxnType']='Invoice';
					$payment_array['Line'][0]['Amount']= $invoice_detail->payment_amount;
					$payment_array['TxnDate'] = date('Y-m-d',strtotime($invoice_detail->payment_added_datetime));
					$input_json = json_encode($payment_array);

					$headerarray = array(
											"Accept: application/json",
											"Content-Type: application/json",
											"Authorization: Bearer ".$get_access_token->access_token
										);
										
					$ch = curl_init();
					$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
					//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/payment?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						echo $error_msg = curl_error($ch);
						exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);
					//print_r($output);exit();
					if(isset($json['fault']))
						{
							if($json['fault']['error'][0]['code']=='3200')
								{
									//echo "Connect to quickbook";exit();
									redirect("quickbook",'refresh');exit();
								}							
						}
					else if(isset($json['Fault']))
						{
							if($json['Fault']['Error'][0]['code']=='3200')
								{
									//echo "Connect to quickbook";exit();
									redirect("quickbook",'refresh');exit();
								}
						}
						
					if(isset($json['Payment']))
					{
						// if($json['Payment']['UnappliedAmt']==0)
						// {
						// 	$inv_fields=array();
						// 	$inv_fields['qb_sync_stat']=1;
						// 	//$this->quickbook_model->update_invoicepay_detail($invoice_id,$inv_fields);
						// }
						$sync_inv_count++;
						$inv_fields=array();
						$inv_fields['qb_sync_stat']=1;
						$inv_fields['ipm_qb_sync_id']=$json['Payment']['DepositToAccountRef']['Id'];
						$this->quickbook_model->update_invoice_payment_by_ipmid($invoice_detail->inv_map_id,$inv_fields);
					}
					else if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$invoice_detail->inv_reference;
								$error_array[$error_inv_count]['customer_name']=$invoice_detail->customer_name;
								$error_array[$error_inv_count]['invoice_date']=date("d/m/Y",strtotime($invoice_detail->payment_added_datetime));
								$error_array[$error_inv_count]['invoice_error']=$json['Fault']['Error'][0]['Message'];
								$error_inv_count++;
							}
					else if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_inv_count]['invoice']=$invoice_detail->inv_reference;
								$error_array[$error_inv_count]['customer_name']=$invoice_detail->customer_name;
								$error_array[$error_inv_count]['invoice_date']=date("d/m/Y",strtotime($invoice_detail->payment_added_datetime));
								$error_array[$error_inv_count]['invoice_error']=$json['fault']['error'][0]['message'];
								$error_inv_count++;
							}




					


					
					
					
				}

				$err_sl=1;
				$msg=$sync_inv_count.'/'.$total_count.' invoice payments synced to QuickBook';
				if($error_inv_count>0)
				{
					$msg.="<br/><br/><div class='text-left'>Following Invoice have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Number</th><th style='text-align:center;'>Customer</th><th style='text-align:center;'>Invoice Date</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
                     $burl=base_url();
                    foreach ($error_array as $err) 
                    	{
                    		$custid=$err['invoice'];
                        	$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['invoice']."</td><td style='text-align:center;'>".$err['customer_name']."</td><td style='text-align:center;'>".$err['invoice_date']."</td><td style='text-align:center;'>".$err['invoice_error']."</td></tr>";
                        	$err_sl++;
                        }    
                    $msg.="</tbody></table>";
                    
				}

				// echo $msg;
				// exit();
				$msg=json_encode($msg);
				//$this->session->set_flashdata('qb_sync_msg',$msg);update_quickbook_cust_sync_msg
				$msgdata=array();
				$msgdata['tmp_msg_content']=$msg;
				$msgdata['tmp_msg_stat']=1;
				$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,4);//exit();
				redirect("quickbook/quickbook_sync_invoice_payment",'refresh');


			}
		}
	}


	public function quickbook_sync_service_types()
	{
		
		$data = array();
		$data['service_types_count']=$this->quickbook_model->fetch_service_types_for_quickbook_count();
		$data['service_types']=$this->quickbook_model->fetch_service_types_for_quickbook_sync();
		$data['qb_sync_msg']=$this->quickbook_model->get_quickbook_servicetype_sync_msg();
		$this->quickbook_model->update_quickbook_cust_sync_msg(array('tmp_msg_stat' => 0 ),5);
        $layout_data['content_body'] = $this->load->view('quickbook_sync_service_types', $data, TRUE);
		$layout_data['page_title'] = 'Quickbook Sync Service Types';
		$layout_data['meta_description'] = 'Quickbook Sync Service Types';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['quickbook_active'] = '1'; 
		$layout_data['js_files'] = array('');
		$this->load->view('layouts/default', $layout_data);
	}

	public function quickbook_sync_service_type_process()
	{


		$service_type_ids=$this->input->post('service_type_ids');
		$service_type_ids=explode(",",$service_type_ids);
		$sync_st_count=0;$error_st_count=0;$error_array=array();
		$total_count=count($service_type_ids);
		$error_array=array();

		if($total_count>0)
		{
			$get_access_token = $this->quickbook_model->get_quickbookdetails(1);
			if($get_access_token->access_token != "")
			{
				foreach ($service_type_ids as $st_id) 
				{
					$service_detail = $this->quickbook_model->get_service_details($st_id);


					$st_array=array();
					$st_array['Name'] = $service_detail->service_type_name;
					$st_array['Type'] = 'NonInventory';
					$st_array['IncomeAccountRef']['value'] = 4;
					$input_json = json_encode($st_array);

					$headerarray = array(
											"Accept: application/json",
											"Content-Type: application/json",
											"Authorization: Bearer ".$get_access_token->access_token
										);
										
					$ch = curl_init();
					$url = "https://quickbooks.api.intuit.com/v3/company/9130350203752886/item?minorversion=57";
					//$url = "https://sandbox-quickbooks.api.intuit.com/v3/company/9130350203752886/item?minorversion=57";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headerarray);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$output = curl_exec($ch); 
					if (curl_errno($ch)) {
						echo $error_msg = curl_error($ch);
						exit();
					}
					curl_close($ch);
					$json = json_decode($output, true);

					if(isset($json['fault']))
						{
							if($json['fault']['error'][0]['code']=='3200')
								{
									//echo "Connect to quickbook";exit();
									redirect("quickbook",'refresh');exit();
								}							
						}
					else if(isset($json['Fault']))
						{
							if($json['Fault']['Error'][0]['code']=='3200')
								{
									//echo "Connect to quickbook";exit();
									redirect("quickbook",'refresh');exit();
								}
						}

					//print_r($output);echo $service_detail->service_type_name;exit();

					if(isset($json['Item']))
					{
						$sync_st_count++;
						$st_fields=array();
						$st_fields['qb_sync_stat']=1;
						$st_fields['qb_sync_id']=$json['Item']['Id'];
						$this->service_types_model->update_service_type($st_id, $st_fields);
					}
					else if(isset($json['Fault']['Error']))
							{
								//echo "Error<br/>";
								$error_array[$error_st_count]['service_name']=$service_detail->service_type_name;
								$error_array[$error_st_count]['service_error']=$json['Fault']['Error'][0]['Message'];
								$error_st_count++;
							}
					else if(isset($json['fault']['error']))
							{
								//echo "Error<br/>";
								$error_array[$error_st_count]['service_name']=$service_detail->service_type_name;
								$error_array[$error_st_count]['service_error']=$json['fault']['error'][0]['message'];
								$error_st_count++;
							}



					//exit();

				}



				$err_sl=1;
				$msg=$sync_st_count.'/'.$total_count.' services synced to QuickBook';
				if($error_st_count>0)
				{
					$msg.="<br/><br/><div class='text-left'>Following Service have sync errors</div><table class='table table-bordered'><thead><tr><th style='text-align:center;'>Sl No</th><th style='text-align:center;'>Service</th><th style='text-align:center;'>Reason</th></tr></thead><tbody>";
                     $burl=base_url();
                    foreach ($error_array as $err) 
                    	{
                    		$custid=$err['invoice'];
                        	$msg.="<tr><td style='text-align:center;'>".$err_sl."</td><td style='text-align:center;'>".$err['service_name']."</td><td style='text-align:center;'>".$err['service_error']."</td></tr>";
                        	$err_sl++;
                        }    
                    $msg.="</tbody></table>";
                    
				}

				$msg=json_encode($msg);
				
				$msgdata=array();
				$msgdata['tmp_msg_content']=$msg;
				$msgdata['tmp_msg_stat']=1;
				$this->quickbook_model->update_quickbook_cust_sync_msg($msgdata,5);//exit();
				redirect("quickbook/quickbook_sync_service_types",'refresh');
			}
		}
	}
}