<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Package extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!is_user_loggedin()) {
			redirect('logout');
		}
		if (!user_permission(user_authenticate(), 3)) {
			show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
		}
		$this->load->model('customers_model');
		$this->load->model('maids_model');
		$this->load->model('package_model');
		$this->load->model('package_subs_model');
		$this->load->model('service_types_model');
		$this->load->model('bookings_model');
		$this->load->model('tablets_model');
		$this->load->model('zones_model');
		$this->load->model('settings_model');
		$this->load->library('upload');
		$this->load->helper('google_api_helper');
		$this->load->helper('curl_helper');
	}

	public function index()
	{
		$data = array();
		$status = $this->input->get('status');
		$data['packages'] = $this->package_model->get_all_packages($status);
		$data['status'] = $status;
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_list', $data, TRUE);
		$layout_data['page_title'] = 'Packages';
		$layout_data['meta_description'] = 'Packages';
		$layout_data['css_files'] = array('demo.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('jquery.dataTables.min.js');
		$this->load->view('layouts/default', $layout_data);
	}
	public function new()
	{
		$data = array();
		$active = $this->uri->segment(2) ? $this->uri->segment(2) : 2;
		$data['packages'] = $this->package_model->get_all_packages($active);
		$data['service_types'] = $this->package_model->get_all_service_types($active);
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_new', $data, TRUE);
		$layout_data['page_title'] = 'New Package';
		$layout_data['meta_description'] = 'New Package';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array();
		$this->load->view('layouts/default', $layout_data);
	}
	public function edit()
	{
		$data['data'] = $this->package_model->get_package_by_id($this->uri->segment(3));
		if(!$data['data']['package_id']){
			redirect('/package?not_found');
		}
		$active = $this->uri->segment(2) ? $this->uri->segment(2) : 2;
		$data['packages'] = $this->package_model->get_all_packages($active);
		$data['service_types'] = $this->package_model->get_all_service_types($active);
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_new', $data, TRUE);
		$layout_data['page_title'] = 'Edit Package';
		$layout_data['meta_description'] = 'Edit Package';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array();
		$this->load->view('layouts/default', $layout_data);
	}
	public function view()
	{
		$data['data'] = $this->package_model->get_package_by_id($this->uri->segment(3));
		if (!$data['data']['package_id']) {
			redirect('/package?not_found');
		}
		$active = $this->uri->segment(2) ? $this->uri->segment(2) : 2;
		$data['packages'] = $this->package_model->get_all_packages($active);
		$data['service_types'] = $this->package_model->get_all_service_types($active);
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_new', $data, TRUE);
		$layout_data['page_title'] = 'View Package';
		$layout_data['meta_description'] = 'View Package';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array();
		$this->load->view('layouts/default', $layout_data);
	}
	public function save()
	{
		if ($this->input->post('id')) {
			// update old
			if ($this->package_model->update_package($this->input->post())) {
				// save success
				redirect('/package?success');
			} else {
				// save failed
				redirect('/package?failed');
			}
		} else {
			// save new
			if ($this->package_model->save_package($this->input->post())) {
				// save success
				redirect('/package?success');
			} else {
				// save failed
				redirect('/package?add_failed');
			}
		}
	}
	public function disable()
	{
		if ($this->package_model->disable_package($this->input->post('id'))) {
			redirect('/package?status=' . $this->input->get('status'));
		} else {
			redirect('/package?status=' . $this->input->get('status'));
		}
	}
	public function enable()
	{
		if ($this->package_model->enable_package($this->input->post('id'))) {
			redirect('/package?status=' . $this->input->get('status'));
		} else {
			redirect('/package?status=' . $this->input->get('status'));
		}
	}
}
