<?php
/********************************** */
// Author : Samnad. S
// Usage : Only for search dropdowns in UI
// Created on : 16-01-2023
// Last edited by :
// Last edited on :
//
/********************************** */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dropdown extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            $result['message'] = 'Authentication Error !';
            $result['success'] = false;
            echo json_encode($result, TRUE);
        }
    }
    public function customer()
    {
        // CAUTION - Editing this will cause changing the entire customer dynamic search
        // Used in dynamic dropdown for select customer
        // exclusively created for select2 plugin
        $search = $this->input->get('search');
        $this->db->select('c.customer_id as id,c.customer_name as text')
            ->from('customers as c');
        if (isset($search) && strlen($search) > 0) {
            $this->db->where("(customer_name LIKE '%" . $search . "%' OR customer_nick_name LIKE '%" . $search . "%' OR mobile_number_1 LIKE '%" . $search . "%' OR mobile_number_2 LIKE '%" . $search . "%' OR mobile_number_3 LIKE '%" . $search . "%' OR phone_number LIKE '%" . $search . "%')", NULL, FALSE);
        }
        $this->db->order_by('c.customer_name', 'ASC');
        $this->db->limit(50);
        $this->db->where('customer_status', 1);
        $query = $this->db->get();
        $result['data'] = $query->result();
        $result['success'] = true;
        echo json_encode($result, TRUE);
    }
}