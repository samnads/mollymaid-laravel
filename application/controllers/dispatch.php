<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Dispatch extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('dispatch_model');
        $this->load->model('settings_model');
        $this->load->model('bookings_model');
    }
    public function index()
    {
        echo 'Hello :/';
    }
    public function update_dispatch()
    {
        header('Content-Type: application/json; charset=utf-8');
        $day_service_id = $this->input->post('day_service_id');
        $this->db->select('ds.*')
            ->from('day_services as ds')
            ->where('ds.day_service_id', $day_service_id);
        $day_service = $this->db->get();
        $day_service = $day_service->row();
        $maid_id = $day_service->maid_id;
        $service_date = $day_service->service_date;
        if ($day_service->dispatch_status == 2) {
            $response['status'] = false;
            $response['message'] = "Can't edit confirmed schedule.";
        } else if ($day_service->dispatch_status == 1) {
            $data['time_from'] = $this->input->post('time_from');
            $data['time_to'] = $this->input->post('time_to');
            /************************************************************ */
            // check for selected time availability with day service
            $this->db->select('ds.day_service_id,ds.time_from,ds.time_to,m.maid_name')
                ->from('day_services as ds')
                ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
                ->where('ds.day_service_id !=', $day_service_id)
                ->where('ds.maid_id', $maid_id)
                ->where('ds.service_date', $service_date)
                ->where("(
            (ds.time_from = " . $this->db->escape($data['time_from']) . ") OR
            (ds.time_from = " . $this->db->escape($data['time_from']) . " AND ds.time_to = " . $this->db->escape($data['time_to']) . ") OR
            (ds.time_from > " . $this->db->escape($data['time_from']) . " AND ds.time_to <= " . $this->db->escape($data['time_to']) . ") OR
            (ds.time_from < " . $this->db->escape($data['time_from']) . " AND ds.time_to = " . $this->db->escape($data['time_to']) . ") OR
            (ds.time_from < " . $this->db->escape($data['time_to']) . " AND ds.time_to > " . $this->db->escape($data['time_to']) . ") OR
            (ds.time_from < " . $this->db->escape($data['time_from']) . " AND ds.time_to > " . $this->db->escape($data['time_from']) . ") OR
            (ds.time_from < " . $this->db->escape($data['time_from']) . " AND ds.time_to > " . $this->db->escape($data['time_to']) . "))");
            $busy_service = $this->db->get();
            $busy_service = $busy_service->row();
            $this->db->flush_cache();
            /************************************************************ */
            if ($busy_service->day_service_id) {
                $response['status'] = false;
                $response['message'] = "Sorry, maid <b>" . $busy_service->maid_name . "</b> is busy from <b>" . DateTime::createFromFormat('H:i:s', $data['time_from'])->format('h:i A') . "</b> to <b>" . DateTime::createFromFormat('H:i:s', $data['time_to'])->format('h:i A') . "</b>.";
            } else {
                $data['service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
                $data['service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
                $data['cleaning_material'] = $this->input->post('cleaning_materials') == "Y" ? "Y" : "N";
                $data['material_fee'] = $this->input->post('material_fee');
                $data['serviceamount'] = $this->input->post('service_amount');
                $data['vatamount'] = $this->input->post('service_vat_amount');
                $data['total_fee'] = $this->input->post('taxed_total');
                $this->db->where('day_service_id', $day_service_id);
                $this->db->update('day_services', $data);
                $response['status'] = true;
                $response['message'] = "Schedule updated successfully.";
            }
        } else {
            $response['status'] = false;
            $response['message'] = "Unknown status found.";
        }
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function get_bookings_for_dispatch()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        $bookings = $this->dispatch_model->get_bookings_for_dispatch_by_date($service_date);
        echo json_encode($bookings, JSON_PRETTY_PRINT);
    }
    public function get_day_services_for_confirm_dispatch()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        $bookings = $this->dispatch_model->get_day_services_for_confirm_dispatch($service_date);
        echo json_encode($bookings, JSON_PRETTY_PRINT);
    }
    public function get_day_services_for_undo_confirmed_dispatch()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        $bookings = $this->dispatch_model->get_day_services_for_undo_confirmed_dispatch($service_date);
        echo json_encode($bookings, JSON_PRETTY_PRINT);
    }
    public function get_dispatched_schedule_for_edit()
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->select('
        ds.day_service_id,
        ds.booking_id,
        ds.customer_id,
        ds.time_from,
        ds.time_to,
        c.customer_name,
        m.maid_name,
        ds.maid_id,
        ds.service_date,
        m.maid_leader_id,
        ds.dispatch_status,
        st.service_type_name,
        ds.service_rate_per_hour,
        ds.service_discount_rate_per_hour,
        ds.cleaning_material,
        ds.material_fee,
        ds.total_fee,
        ')
            ->from('day_services as ds')
            ->join('bookings as b', 'b.booking_id = ds.booking_id', 'left')
            ->join('service_types as st', 'b.service_type_id = st.service_type_id', 'left')
            ->join('customers as c', 'c.customer_id = ds.customer_id', 'left')
            ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
            ->where('ds.day_service_id', $this->input->get('day_service_id'))
            ->order_by('ds.day_service_id', 'ASC');
        $query = $this->db->get();
        $day_service = $query->row();
        if ($day_service->dispatch_status == 2) {
            $response['status'] = false;
            $response['message'] = "Can't edit confirmed schedule.";
            die(json_encode($response, JSON_PRETTY_PRINT));
        }
        $response['status'] = true;
        $response['data'] = $day_service;
        die(json_encode($response, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK));
    }
    public function dispatch_list()
    {
        $date = $this->uri->segment(3) ?: date('Y-m-d');
        /************************************************************* */
        $this->db->select('
        ds.day_service_id,
        ds.booking_id,
        ds.customer_id,
        c.customer_name,
        m.maid_name,
        ds.maid_id,
        ds.service_date,
        m.maid_leader_id
        ')
            ->from('day_services as ds')
            ->join('bookings as b', 'b.booking_id = ds.booking_id', 'left')
            ->join('customers as c', 'c.customer_id = ds.customer_id', 'left')
            ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
            ->where('ds.service_date', $date)
            ->where('ds.dispatch_status', 2)
            ->order_by('ds.day_service_id', 'ASC');
        $query = $this->db->get();
        $day_services = $query->result();
        $day_service_ids = array_unique(array_column($day_services, 'day_service_id'));
        $service_customer_ids = array_unique(array_column($day_services, 'customer_id'));
        $service_maid_ids = array_unique(array_column($day_services, 'maid_id'));
        //echo '<pre>';print_r($day_services);echo '</pre>';die();
        /************************************************************* */
        // maid leaders
        $this->db->select('
        m.maid_name,
        m.maid_leader_id,
        ')
            ->from('maids as m')
            ->where('m.maid_leader_id = m.maid_id')
            ->where('m.maid_status', 1);
        $maid_leaders = $this->db->get();
        $maid_leaders = $maid_leaders->result();
        //echo '<pre>';print_r($maid_leaders);echo '</pre>';die();
        /************************************************************* */
        $this->db->select('
        m.maid_id,
        m.maid_name,
        m.maid_leader_id,
        ')
            ->from('maids as m')
            ->where_in('m.maid_id', $service_maid_ids ?: '[]')
            ->where('m.maid_status', 1)
            ->order_by('m.maid_id', 'ASC');
        $maids = $this->db->get();
        $maids = $maids->result();
        //echo '<pre>';print_r($maids);echo '</pre>';die();
        /************************************************************* */
        foreach ($maid_leaders as $key => $leader) {
            $data['dispatch_keys'][$leader->maid_leader_id]['leader'] = array('name' => $leader->maid_name);
            $data['dispatch_keys'][$leader->maid_leader_id]['maids'] = [];
            $i = -1;
            foreach ($maids as $key1 => $maid) {
                if ($maid->maid_leader_id == $leader->maid_leader_id) {
                    // take based on parent maid
                    $i++;
                    $data['dispatch_keys'][$leader->maid_leader_id]['maids'][$i] = array('name' => $maid->maid_name);
                    // get customer based on maids
                    /************************************************************* */
                    $this->db->select('
                        ds.customer_id,
                        ds.maid_id,
                        m.maid_leader_id
                        ')
                        ->from('day_services as ds')
                        ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
                        ->where('m.maid_id', $maid->maid_id)
                        ->where('m.maid_leader_id', $leader->maid_leader_id)
                        ->where_in('ds.day_service_id', $day_service_ids)
                        ->group_by('ds.customer_id');
                    $query = $this->db->get();
                    $day_services_2 = $query->result();
                    //echo '<pre>';print_r($day_services_2);echo '</pre>';die();
                    /************************************************************* */
                    foreach ($day_services_2 as $key3 => $day_service) {
                        if ($day_service->maid_leader_id == $leader->maid_leader_id) {
                            $this->db->select('
                            cao.id,
                            cao.customer_access_id,
                            ca.code as customer_access_code,
                            cat.name as customer_access_type
                            ')
                                ->from('customer_access_owned as cao')
                                ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
                                ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
                                ->where('cao.customer_id', $day_service->customer_id)
                                ->where('cao.returned_at', null)
                                ->where('cao.return_added_by', null);
                            $keys = $this->db->get();
                            $keys = $keys->result_array();
                            if (!isset($data['dispatch_keys'][$leader->maid_leader_id]['maids'][$i]['accesses'])) {
                                $data['dispatch_keys'][$leader->maid_leader_id]['maids'][$i]['accesses'] = [];
                            }

                            // Append the new keys to the maid's accesses (if they are not already present)
                            foreach ($keys as $key_data) {
                                // Ensure no duplicate entries are added to the accesses array
                                $existing_access = false;
                                foreach ($data['dispatch_keys'][$leader->maid_leader_id]['maids'][$i]['accesses'] as $existing_key) {
                                    if ($existing_key['id'] == $key_data['id']) {
                                        $existing_access = true;
                                        break;
                                    }
                                }
                                if (!$existing_access) {
                                    $data['dispatch_keys'][$leader->maid_leader_id]['maids'][$i]['accesses'][] = $key_data;
                                }
                            }
                            // $data['dispatch_keys'][$leader->maid_leader_id]['maids'][$i]['accesses'] = $keys;
                        }
                    }
                }
            }
        }
        //echo '<pre>';die(json_encode($data['dispatch_keys'], JSON_PRETTY_PRINT));die();echo '</pre>';
        /************************************************************* */
        $data['settings'] = $this->settings_model->get_settings();
        $data['unconfirmed_dispatches'] = $this->dispatch_model->get_dispatches_by_service_date($date);
        $data['confirmed_dispatches'] = $this->dispatch_model->get_confirmed_dispatches_by_service_date($date);
        $data['payment_modes'] = $this->dispatch_model->payment_modes($date);
        $data['service_date'] = $date;
        //var_dump($data['rows']);die();
        $layout_data['content_body'] = $this->load->view('dispatch/dispatch-list', $data, true);
        $layout_data['page_title'] = 'Booking Dispatches';
        $layout_data['meta_description'] = 'Booking Dispatches';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js', 'dispatch.js', 'printThis.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function confirm_dispatched_services()
    {
        $day_service_ids = $this->input->post('day_service_ids') ?: null;
        $this->db->trans_begin();
        try {
                $this->db->select('ds.day_service_id,ds.time_from, ds.time_to,ds.booking_id,ds.customer_id,ds.maid_id, ca.customer_address,m.maid_id,m.maid_name')
                ->from('day_services as ds')
                ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
                ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                ->join('locations as l', 'ca.location_id = l.location_id', 'left')
                ->join('areas as a', 'a.area_id = l.area_id', 'left')
                ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
                ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
            ->where_in('ds.day_service_id', $day_service_ids)
            ->order_by('ds.day_service_id');
        $query = $this->db->get();
        $day_services = $query->result();
        $day_service_data = [];
        // print_r($day_services);
        foreach ($day_services as $key => $schedule) {
            // $day_service_data[$key]['day_service_id'] = $schedule->day_service_id;  
            // $day_service_data[$key]['drop_address'] = $schedule->customer_address;  
            // $day_service_data[$key]['drop_time'] = $schedule->time_from;
          
            if ($previous_maid_id == $schedule->maid_id) {   
                // $day_service_data[$key]['pick_address'] = $previous_drop_address;
                // $date = new DateTime($previous_time);
                // $new_time = $date->format('h:i:A');
                // $day_service_data[$key]['pick_time'] = $previous_time;
                $day_service_data[] = [
                    'day_service_id' => $schedule->day_service_id,
                    // 'maid_id' => $schedule->maid_id,
                    'pick_address' => $previous_drop_address,
                    'drop_time' => $schedule->time_from,
                    'drop_address' => $schedule->customer_address,
                    'pick_time' => $previous_time,
                ];
               
            }
            
            else {
                $day_service_data[] = [
                    'day_service_id' => $schedule->day_service_id,
                    // 'maid_id' => $schedule->maid_id,
                    'drop_address' => $schedule->customer_address,
                    'drop_time' => $schedule->time_from,
                    'pick_address' => "Accommodation",
                    'pick_time' => "06:45:00",
                ];
            }
            $previous_maid_id = $schedule->maid_id;
            $previous_drop_address = $schedule->customer_address;   
            $previous_time = $schedule->time_to; 
            $next_maid_id = $schedule->maid_id; // Switching maid_id (for example)
            $next_pick_time = $schedule->time_to; // Start time for next maid to pick up
            // if (isset($day_services[$key + 1]) && $day_services[$key + 1]->maid_id = $schedule->maid_id) {
            //     // Next entry has the same maid, so this is not the last entry
            //     $day_service_data[] = [
            //         'day_service_id' => $schedule->day_service_id,
            //         'maid_id' => $schedule->maid_id,
            //         'drop_address' => "Accommodation",
            //         'drop_time' => "19:00:00", // Fixed drop time at 7:00 PM
            //         'pick_address' => $previous_drop_address,
            //         'pick_time' =>  $next_pick_time, // Start time for the next maid to pick up
            //     ];
            // }

            // If the next entry is not the same maid, it means this is the last service for this maid
            if (!isset($day_services[$key + 1]) || $day_services[$key + 1]->maid_id != $schedule->maid_id) {
                $day_service_data[] = [
                    'day_service_id' => $schedule->day_service_id,
                    // 'maid_id' => $schedule->maid_id,
                    'drop_address' => "Accommodation",
                    'drop_time' => "19:00:00", // Set the drop time to 7:00 PM for the last service of this maid
                    'pick_address' => $previous_drop_address,
                    'pick_time' =>  $next_pick_time,
                ];
            };
        }
        //  print_r($day_service_data);
        foreach ($day_service_ids as $key => $day_service_id) {
            $row_data = array();
            $update = $this->dispatch_model->confirm_dispatched_service_by_id($day_service_id, $row_data);
        }
        $this->db->insert_batch('driver_unconfirmed_schedules', $day_service_data);
        $unconfirmed_id = $this->db->insert_id();
        $this->db->trans_commit();
        $respose['status'] = true;
        $respose['message'] = "Dispatched bookings confirmed successfully.";
        $respose['data'] = $rows_data;
        die(json_encode($respose, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    }
    public function undo_confirmed_dispatched_services()
    {
        $day_service_ids = $this->input->post('undo_day_service_ids') ?: null;
        $this->db->trans_begin();
        try {
            foreach ($day_service_ids as $key => $day_service_id) {
                $row_data = array();
                $update = $this->dispatch_model->undo_confirmed_dispatch_service_by_id($day_service_id, $row_data);
            }
            $this->db->trans_commit();
            $respose['status'] = true;
            $respose['message'] = "Confirmed Dispatch removed successfully.";
            $respose['data'] = $rows_data;
            die(json_encode($respose, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    }
    public function dispatch_bookings()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->post('dispatch_service_date');
        $dispatch_booking_ids = $this->input->post('dispatch_booking_ids') ?: null;
        $this->db->trans_begin();
        try {
            $rows_data = $this->dispatch_model->get_data_for_dispatch_insert($dispatch_booking_ids, $service_date);
            foreach ($rows_data as $key => $row) {
                $day_service_data = [];
                //$rows_data[$key]['service_discount_rate_per_hour'] = $row['service_discount_rate_per_hour'] > 0 ? $row['service_discount_rate_per_hour'] : $row['service_rate_per_hour'];
                //$rows_data[$key]['vatamount'] = $rows_data[$key]['vatamount'] ?: 0;
                //$rows_data[$key]['serviceamount'] = $rows_data[$key]['serviceamount'] ?: 0;
                /************************************************* */
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $row['booking_id'])
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                $this->db->select("b.*")
                ->from('bookings as b')
                ->where_in('b.booking_id', $dispatch_booking_ids);
                $query1 = $this->db->get();
                $bookings = $query1->result();;
                /************************************************* */
                if ($day_service->day_service_id == null) {
                    // no day service entry found, so create one
                    if ($this->dispatch_model->dispatch_insert($rows_data[$key]) > 0) {
                        $day_service_id = $this->db->insert_id();
                        foreach ($bookings as  $booking) {
                            if ($booking->booking_id == $row['booking_id']) {
                                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                                ->where('day_service_id', $day_service_id)
                                ->update('day_services');
                            }
                        }
                           
                        // update reference id
                        // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                        //     ->where('day_service_id', $day_service_id)
                        //     ->update('day_services');
                        
                        // update reference id END
                    } else {
                        $respose['status'] = false;
                        $respose['message'] = $this->db->_error_message();
                        $this->db->trans_rollback();
                        die(json_encode($respose, JSON_PRETTY_PRINT));
                    }
                } else {
                    // schedule entry exist, so update something
                    $day_service_data['dispatch_status'] = 1;
                    $this->db->set($day_service_data)
                        ->where('day_service_id', $day_service->day_service_id)
                        ->update('day_services');
                }
            }
            //var_dump($rows_data);die();
            /*if ($this->dispatch_model->dispatch_insert_batch($rows_data) == true) {
            } else {
            $respose['status'] = false;
            $respose['message'] = $this->db->_error_message();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
            }*/
            $this->db->trans_commit();
            $respose['status'] = true;
            $respose['message'] = "Bookings dispatched successfully.";
            $respose['data'] = $rows_data;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
        }
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
}
