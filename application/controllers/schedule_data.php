<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Schedule_data extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 2)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('zones_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('driver_model');
        $this->load->model('tablets_model');
        $this->load->model('settings_model');
        $this->load->model('justmop_model');
        $this->load->helper('google_api_helper');
        $this->load->model('employee_model');
        $this->load->model('schedule_model');
        header('Content-Type: application/json; charset=utf-8');
    }
    public function index()
    {
        echo 'Schedule_data';
    }
    public function get_bookings_by_week()
    {
        $this->db->select('
        b.booking_id,
        b.customer_id,
        c.customer_name,
        b.customer_address_id,
        b.time_from,
        b.time_to,
        b.service_week_day,
        b.maid_id,
        cl.location_name as booking_location,
        TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes
        ',
            false)
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as cl', 'ca.location_id = cl.location_id', 'left')
            ->where('b.booking_type', "WE")
            ->where('b.deleted_at', null)
            ->order_by('b.time_from', 'ASC');
        $response['bookings'] = $this->db->get()->result();
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function day_schedules()
    {
        $service_date = $this->input->get('service_date');
        $service_week_day = date('w', strtotime($service_date));
        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select('
        b.booking_id,
        b.booking_type,
        ds.day_service_id,
        ds.service_date,
        b.customer_id,
        c.customer_name,
        b.customer_address_id,
        b.time_from,
        ds.time_from as ds_time_from,
        ds.time_to as ds_time_to,
        b.time_to,
        b.service_week_day,
        b.service_actual_end_date,
        b.service_start_date,
        b.maid_id,
        ds.dispatch_status,
        cl.location_name as booking_location,
        ar.area_id,
        z.zone_id,
        z.zone_name as booking_zone,
        bd.booking_delete_id,
        bd.delete_from_date,
        bd.service_date as booking_delete_service_date,
        b.parent_booking_id,
        b.parent_maid_name,
        b.booking_reassign_reason,
        IFNULL(ds.charge_type,b.charge_type) as charge_type,
        TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes,
        TIMESTAMPDIFF(MINUTE,ds.time_from ,ds.time_to) as ds_working_minutes
        ',
            false)
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as cl', 'ca.location_id = cl.location_id', 'left')
            ->join('areas as ar', 'ca.area_id = ar.area_id', 'left')
            ->join('zones as z', 'ar.zone_id = z.zone_id', 'left')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = "' . $service_date . '"','left')
            ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id AND bd.service_date = "' . $service_date . '"', 'left')
            ->where('b.booking_status', 1)
            ->where('b.service_week_day', $service_week_day)
            ->where("((b.service_end = 0) OR (b.service_end = 1 AND b.service_actual_end_date >= " . $this->db->escape($service_date) . "))")
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false)
            ->order_by('b.time_from', 'ASC');
            if (count($deleted_bookings) > 0) {
                $this->db->where_not_in('b.booking_id', $deleted_bookings);
            }
        $response['schedules'] = $this->db->get()->result();
        $booking_data1 = array();
    //   print_r($response['schedules']);die();
        foreach ($response['schedules'] as $key => $schedule) {
            if ($schedule->booking_type == "WE") {
                $response['schedules'][$key]->css_class = "bg-we";
            }
            else if ($schedule->booking_type == "OD") {
                $response['schedules'][$key]->css_class = "bg-od";
            }
            else if ($schedule->day_service_id != null && $schedule->dispatch_status == 1) {
                $response['schedules'][$key]->css_class = "pending";
            }
            else if ($schedule->day_service_id != null && $schedule->dispatch_status == 2) {
                $response['schedules'][$key]->css_class = "confirmed";
            } else {
                $response['schedules'][$key]->css_class = "source-schedule";
            }
            if ($schedule->day_service_id != "")
            {
                $response['schedules'][$key]->time_from = $schedule->ds_time_from;
                $response['schedules'][$key]->time_to = $schedule->ds_time_to;
                $response['schedules'][$key]->working_minutes = $schedule->ds_working_minutes;
                
            }
            $working_minutes = $schedule->working_minutes;
            $booking_data1[$key]['booking_id']  = $schedule->booking_id;
            $booking_data1[$key]['maid_id']  = $schedule->maid_id;
            $booking_data1[$key]['working_minutes']  = $working_minutes;
            $booking_data1[$key]['service_week_day']  = $schedule->service_week_day;
                    
            if ($schedule->charge_type == "1") {
                $booking_charge[$key]['booking_id']  = $schedule->booking_id;
                $booking_charge[$key]['maid_id']  = $schedule->maid_id;
                $booking_charge[$key]['working_minutes']  = $working_minutes;
                $booking_charge[$key]['service_week_day']  = $schedule->service_week_day;
            } 
            if ($schedule->charge_type == "2") {
                $booking_charge1[$key]['booking_id']  = $schedule->booking_id;
                $booking_charge1[$key]['maid_id']  = $schedule->maid_id;
                $booking_charge1[$key]['working_minutes']  = $working_minutes;
                $booking_charge1[$key]['service_week_day']  = $schedule->service_week_day;
            }

            // if ($schedule->booking_delete_id != null && $schedule->booking_delete_service_date == $service_date)
            // {
            //     unset($response['schedules'][$key]);
            // }
        }
        // print_r($booking_data1);
        $free_hours = 0;
        foreach ($booking_charge as $value) {   
            $free_hours += $value['working_minutes'];    
        }
 
        $training_hours = 0;
        foreach ($booking_charge1 as $value) {   
            $training_hours += $value['working_minutes'];    
        }
        $res = array_reduce (
            $booking_data1,
            function($res, $el) {
                if (isset($res[$el['maid_id'].$el['service_week_day']])) {
                    $res[$el['maid_id'].$el['service_week_day']]['working_minutes'] += $el['working_minutes']; 
                } else {
                    $res[$el['maid_id'].$el['service_week_day']] = $el;
                }
                
                return $res;
            },
            [] // initial result - empty array
        );
       $total_hour_week = array_values($res);

       // Grand Total
        $grand_total = 0;
        foreach ($booking_data1 as $value) {   
            $grand_total += $value['working_minutes'];    
        
        }
        $maids = $this->schedule_model->get_maids(1, []);
        $maid_count = count($maids);
        $leaveData = $this->schedule_model->get_maids_leave($service_date);
        // print_r($emergency_leave);
        $leaveData_count = count($leaveData);
        //maid emergency leaves
        $emergency_leave = $this->schedule_model->get_maids_emergency_leave($service_date);
        // print_r($emergency_leave);
        $emergency_leave_count = count($emergency_leave);

        //maid vacations
        $vacations = $this->schedule_model->get_maids_vacations($service_date);
        $vacations_count = count($vacations);

        //maid holidays
        $holidays = $this->schedule_model->get_maids_holidays($service_date);
        $holidays_count = count($holidays);

        //maid medical_leaves
        $medical_leaves = $this->schedule_model->get_maids_medical_leaves($service_date);
        $medical_leaves_count = count($medical_leaves);

        // Absent maid count 
        $total_maid_leave_count = $leaveData_count + $emergency_leave_count + $vacations_count + $holidays_count + $medical_leaves_count;
        //present maid count
        $present_maid_count = $maid_count -  $total_maid_leave_count;

        $deletes1 = $this->get_booking_deletes_suspend_by_date($service_date);
        if ($deletes1) {
            $deleted_bookings1 = array();
            foreach ($deletes1 as $delete) {
                $deleted_bookings1[] = $delete->booking_id;
            }
            
            $booking_data6 = $this->get_booking($deleted_bookings1);
            $booking_data6 = json_decode(json_encode($booking_data6, true), true);
            $grand_suspend_total = 0;
            foreach ($booking_data6 as $value) {   
                $grand_suspend_total += $value['working_minutes'];    
            
            }

        }
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
        $get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);

        // $leave_maid_ids = array();
        // foreach ($get_leave_maids as $leave) {
        //     array_push($leave_maid_ids, $leave->maid_id);
        // }

        $leave_maid_types = array();
        foreach ($get_leave_maids as $leave) {
            $leave_date = $leave->$leave_date;
            $leavetypearay = array();
            $leavetypearay['service_week_day'] = date('w', strtotime($leave_date));
            $leavetypearay['maid_id'] = $leave->maid_id;
            $leavetypearay['maid_type'] = $leave->typeleaves;
            array_push($leave_maid_types, $leavetypearay);
        }

        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }

        $emergency_leave_maids = $this->schedule_model->get_all_leave_by_maids($service_date);
        // print_r($emergency_leave_maids);die();

        $maidData = $this->schedule_model->get_maids(1, []);
        $week_days = $this->schedule_model->get_week_days();
        $response['maidData'] = $maidData;
        $response['emergency_leave_maids'] = $emergency_leave_maids;
        $response['week_days'] = $week_days; 
        $response['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
        $response['leave_maid_types'] = $leave_maid_types; // Maid Leave
        $response['service_week_day'] = $service_week_day;
        $response['emergency_leave_count'] = $emergency_leave_count;
        $response['vacations_count'] = $vacations_count;
        $response['holidays_count'] = $holidays_count;
        $response['medical_leaves_count'] = $medical_leaves_count;
        $response['maid_count'] = $maid_count;
        $response['grand_total'] = $grand_total;
        $response['grand_suspend_total'] = $grand_suspend_total;
        $response['total_hour_week'] = $total_hour_week;
        $response['training_hours'] = $training_hours;
        $response['free_hours'] = $free_hours;
        $response['total_maid_leave_count'] =  $total_maid_leave_count;
        $response['present_maid_count'] =  $present_maid_count;
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        if ($service_end_date != NULL) {
            $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
        } else {
            $this->db->where("service_date", $service_date);
        }


        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }

    function get_booking_deletes_suspend_by_date($service_date, $service_end_date = NULL)
    {
        $this->db->select('booking_id')
            ->from('booking_deletes');
        if ($service_end_date != NULL) {
            $this->db->where("service_date BETWEEN '$service_date' AND '$service_end_date'");
            $this->db->where('delete_booking_type', 'Suspend');
        } else {
            $this->db->where("service_date", $service_date);
            $this->db->where('delete_booking_type', 'Suspend');
        }


        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }
    
    function get_booking($deleted_bookings1)
    {
        $this->db->select("b.booking_id, b.customer_id, b.maid_id, b.service_start_date,
        
        b.service_week_day, 
        b.booking_type, b.booking_status, 
        b.time_from,
        b.time_to,
        TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes", FALSE)
        ->from('bookings b');
       
        $this->db->where_in("b.booking_id", $deleted_bookings1);
       
        $get_booking_deletes_by_date_qry = $this->db->get();

        return $get_booking_deletes_by_date_qry->result();
    }
    public function get_booking_data()
    {
        $this->db->select('
        b.booking_id,
        b.reference_id,
        b.customer_id,
        c.customer_name,
        b.customer_address_id,
        b.time_from,
        b.time_to,
        b.service_week_day,
        b.service_start_date,
        IF(b.service_end = 1, service_end_date, null) as service_end_date,
        b.price_per_hr,
        b.discount_price_per_hr,
        b.cleaning_material,
        b.maid_id,
        m.maid_name,
        b.service_type_id,
        cl.location_name as booking_location,
        TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes,
        b._service_hours,
        b._service_rate_per_hour,
        b._service_discount_rate_per_hour,
        b._service_amount,
        b._service_discount,
        b._net_service_amount,
        b._cleaning_material,
        b._cleaning_material_rate_per_hour,
        b._net_cleaning_material_amount,
        b._total_discount,
        b._taxable_amount,
        b._vat_percentage,
        b._vat_amount,
        b._total_amount,
        ',
            false)
            ->from('bookings as b')
            ->join('maids as m', 'm.maid_id = b.maid_id', 'left')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as cl', 'ca.location_id = cl.location_id', 'left')
            ->where('booking_id', $this->input->get('booking_id'))
            ->where('b.deleted_at', null)
            ->order_by('b.time_from', 'ASC');
        $response['booking_data'] = $this->db->get()->row();
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function get_schedule_data()
    {
        $service_date = $this->input->get('service_date');
        $booking_id = $this->input->get('booking_id');
        $query = $this->db->select('
        b.booking_id,
        b.reference_id as booking_reference_id,
        b.booking_type,
        ds.day_service_id,
        ds.day_service_reference_id,
        ds.service_date,
        ds.service_status as day_service_status,
        b.customer_id,
        c.customer_name,
        b.customer_address_id,
        b.time_from,
        b.time_to,
        b.service_week_day,
        b.service_start_date,
        b.service_end_date,
        b.price_per_hr,
        b.discount_price_per_hr,
        b.cleaning_material,
        b.maid_id,
        m.maid_name,
        b.service_type_id,
        cl.location_name as booking_location,
        ds.dispatch_status,
        TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes,
        IFNULL(ds._service_hours,b._service_hours) as _service_hours,
        IFNULL(ds._service_rate_per_hour,b._service_rate_per_hour) as _service_rate_per_hour,
        IFNULL(ds._service_discount_rate_per_hour,b._service_discount_rate_per_hour) as _service_discount_rate_per_hour,
        IFNULL(ds._service_amount,b._service_amount) as _service_amount,
        IFNULL(ds._service_discount,b._service_discount) as _service_discount,
        IFNULL(ds._net_service_amount,b._net_service_amount) as _net_service_amount,
        IFNULL(ds._cleaning_material,b._cleaning_material) as _cleaning_material,
        IFNULL(ds._cleaning_material_rate_per_hour,b._cleaning_material_rate_per_hour) as _cleaning_material_rate_per_hour,
        IFNULL(ds._net_cleaning_material_amount,b._net_cleaning_material_amount) as _net_cleaning_material_amount,
        IFNULL(ds._total_discount,b._total_discount) as _total_discount,
        IFNULL(ds._taxable_amount,b._taxable_amount) as _taxable_amount,
        IFNULL(ds._vat_percentage,b._vat_percentage) as _vat_percentage,
        IFNULL(ds._vat_amount,b._vat_amount) as _vat_amount,,
        IFNULL(ds._total_amount,b._total_amount) as _total_amount,
        IFNULL(ds.time_from,b.time_from) as time_from,
        IFNULL(ds.time_to,b.time_to) as time_to,
        IFNULL(ds.charge_type,b.charge_type) as charge_type,
        IFNULL(ds.material_cost,b.material_cost) as material_cost,
        ',
            false)
            ->from('bookings as b')
            ->join('maids as m', 'm.maid_id = b.maid_id', 'left')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as cl', 'ca.location_id = cl.location_id', 'left')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = "' . $service_date . '"', 'left')
            ->where('b.booking_id', $booking_id)
            ->order_by('b.time_from', 'ASC')->get();
        $response['schedule_data'] = $query->row();
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function day_schedules_list()
    {
        $service_date = $this->input->get('service_date');
        $service_week_day = date('w', strtotime($service_date));
        $deletes = $this->get_booking_deletes_by_date($service_date);

        $deleted_bookings = array();
        foreach ($deletes as $delete) {
            $deleted_bookings[] = $delete->booking_id;
        }
        $this->db->select('
        b.booking_id,
        b.booking_type,
        ds.day_service_id,
        ds.service_date,
        b.customer_id,
        c.customer_name,
        c.customer_code,
        c.customer_booktype,
        b.customer_address_id,
        b._total_amount,
        b._service_rate_per_hour,
        b.time_from,
        ds.time_from as ds_time_from,
        ds.service_status as day_service_status,
        ds.time_to as ds_time_to,
        b.time_to,
        b.service_week_day,
        b.service_actual_end_date,
        b.service_start_date,
        b.maid_id,
        m.maid_name,
        ds.dispatch_status,
        cl.location_name as booking_location,
        ar.area_id,
        ar.area_name,
        z.zone_id,
        z.zone_name as booking_zone,
        bd.booking_delete_id,
        bd.delete_from_date,
        bd.service_date as booking_delete_service_date,
        b.parent_booking_id,
        b.parent_maid_name,
        b.booking_reassign_reason,
        bd.delete_booking_type,
        bd.delete_status,
        bd.reassign_status,
        IFNULL(ds._service_rate_per_hour,b._service_rate_per_hour) as _service_rate_per_hour,
        IFNULL(ds._total_amount,b._total_amount) as _total_amount,
        IFNULL(ds.charge_type,b.charge_type) as charge_type,
        TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes,
        TIMESTAMPDIFF(MINUTE,ds.time_from ,ds.time_to) as ds_working_minutes
        ',
            false)
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as cl', 'ca.location_id = cl.location_id', 'left')
            ->join('areas as ar', 'ca.area_id = ar.area_id', 'left')
            ->join('zones as z', 'ar.zone_id = z.zone_id', 'left')
            ->join('maids as m', 'm.maid_id = b.maid_id', 'left')
            ->join('day_services as ds', 'b.booking_id = ds.booking_id AND ds.service_date = "' . $service_date . '"','left')
            ->join('booking_deletes as bd', 'b.booking_id = bd.booking_id AND bd.service_date = "' . $service_date . '"', 'left')
            ->where('b.booking_status', 1)
            ->where('b.service_week_day', $service_week_day)
            ->where("((b.service_end = 0) OR (b.service_end = 1 AND b.service_actual_end_date >= " . $this->db->escape($service_date) . "))")
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false)
            ->order_by('b.time_from', 'ASC');
            // ->group_by('bd.booking_id');
            if (count($deleted_bookings) > 0) {
                $this->db->group_by('b.booking_id', $deleted_bookings);
            }
            if (count($deleted_bookings) > 0) {
                $group_by_bookings = array();
                foreach ($deleted_bookings as $delete) {
                    if (is_object($delete) && property_exists($delete, 'booking_id')) {
                        $group_by_bookings[] = $delete->booking_id;
                    }
                }
                if (!empty($group_by_bookings)) {
                    $this->db->where_in('b.booking_id', $group_by_bookings);
                }
            }
        $response['schedules'] = $this->db->get()->result();
    //   print_r($response['schedules']);die();
        foreach ($response['schedules'] as $key => $schedule) {
            if ($schedule->booking_type == "WE") {
                $response['schedules'][$key]->css_class = "bg-we";
            }
            else if ($schedule->booking_type == "OD") {
                $response['schedules'][$key]->css_class = "bg-od";
            }
            else if ($schedule->day_service_id != null && $schedule->dispatch_status == 1) {
                $response['schedules'][$key]->css_class = "pending";
            }
            else if ($schedule->day_service_id != null && $schedule->dispatch_status == 2) {
                $response['schedules'][$key]->css_class = "confirmed";
            } else {
                $response['schedules'][$key]->css_class = "source-schedule";
            }
            if ($schedule->day_service_id != "")
            {
                $response['schedules'][$key]->time_from = $schedule->ds_time_from;
                $response['schedules'][$key]->time_to = $schedule->ds_time_to;
                $response['schedules'][$key]->working_minutes = $schedule->ds_working_minutes;
                
            }
            $working_minutes = $schedule->working_minutes;
            $booking_data1[$key]['booking_id']  = $schedule->booking_id;
            $booking_data1[$key]['maid_id']  = $schedule->maid_id;
            $booking_data1[$key]['working_minutes']  = $working_minutes;
            $booking_data1[$key]['service_week_day']  = $schedule->service_week_day;
            // if ($schedule->booking_delete_id != null && $schedule->booking_delete_service_date == $service_date)
            // {
            //     unset($response['schedules'][$key]);
            // }
            if ($schedule->charge_type == "1") {
                $booking_charge[$key]['booking_id']  = $schedule->booking_id;
                $booking_charge[$key]['maid_id']  = $schedule->maid_id;
                $booking_charge[$key]['working_minutes']  = $working_minutes;
                $booking_charge[$key]['service_week_day']  = $schedule->service_week_day;
            } 
            if ($schedule->charge_type == "2") {
                $booking_charge1[$key]['booking_id']  = $schedule->booking_id;
                $booking_charge1[$key]['maid_id']  = $schedule->maid_id;
                $booking_charge1[$key]['working_minutes']  = $working_minutes;
                $booking_charge1[$key]['service_week_day']  = $schedule->service_week_day;
            }
        }
        $res = array_reduce (
            $booking_data1,
            function($res, $el) {
                if (isset($res[$el['maid_id'].$el['service_week_day']])) {
                    $res[$el['maid_id'].$el['service_week_day']]['working_minutes'] += $el['working_minutes']; 
                } else {
                    $res[$el['maid_id'].$el['service_week_day']] = $el;
                }
                
                return $res;
            },
            [] // initial result - empty array
        );
       $total_hour_week = array_values($res);

       // Grand Total
        $grand_total = 0;
        foreach ($booking_data1 as $value) {   
            $grand_total += $value['working_minutes'];    
        
        }

        $free_hours = 0;
        foreach ($booking_charge as $value) {   
            $free_hours += $value['working_minutes'];    
        }
 
        $training_hours = 0;
        foreach ($booking_charge1 as $value) {   
            $training_hours += $value['working_minutes'];    
        }

        $maids = $this->schedule_model->get_maids(1, []);
        $maid_count = count($maids);
        $leaveData = $this->schedule_model->get_maids_leave($service_date);
        // print_r($emergency_leave);
        $leaveData_count = count($leaveData);
        //maid emergency leaves
        $emergency_leave = $this->schedule_model->get_maids_emergency_leave($service_date);
        // print_r($emergency_leave);
        $emergency_leave_count = count($emergency_leave);

        //maid vacations
        $vacations = $this->schedule_model->get_maids_vacations($service_date);
        $vacations_count = count($vacations);

        //maid holidays
        $holidays = $this->schedule_model->get_maids_holidays($service_date);
        $holidays_count = count($holidays);

        //maid medical_leaves
        $medical_leaves = $this->schedule_model->get_maids_medical_leaves($service_date);
        $medical_leaves_count = count($medical_leaves);

        // Absent maid count 
        $total_maid_leave_count = $leaveData_count + $emergency_leave_count + $vacations_count + $holidays_count + $medical_leaves_count;
        //present maid count
        $present_maid_count = $maid_count -  $total_maid_leave_count;

        $deletes1 = $this->get_booking_deletes_suspend_by_date($service_date);
        if ($deletes1) {
            $deleted_bookings1 = array();
            foreach ($deletes1 as $delete) {
                $deleted_bookings1[] = $delete->booking_id;
            }
            
            $booking_data6 = $this->get_booking($deleted_bookings1);
            $booking_data6 = json_decode(json_encode($booking_data6, true), true);
            $grand_suspend_total = 0;
            foreach ($booking_data6 as $value) {   
                $grand_suspend_total += $value['working_minutes'];    
            
            }

        }
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
        $get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);

        // $leave_maid_ids = array();
        // foreach ($get_leave_maids as $leave) {
        //     array_push($leave_maid_ids, $leave->maid_id);
        // }

        $leave_maid_types = array();
        foreach ($get_leave_maids as $leave) {
            $leave_date = $leave->$leave_date;
            $leavetypearay = array();
            $leavetypearay['service_week_day'] = date('w', strtotime($leave_date));
            $leavetypearay['maid_id'] = $leave->maid_id;
            $leavetypearay['maid_type'] = $leave->typeleaves;
            array_push($leave_maid_types, $leavetypearay);
        }

        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }
        $maidData = $this->schedule_model->get_maids(1, []);
        $week_days = $this->schedule_model->get_week_days();
        $response['maidData'] = $maidData;
        $response['week_days'] = $week_days; 
        $response['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
        $response['leave_maid_types'] = $leave_maid_types; // Maid Leave
        $response['service_week_day'] = $service_week_day;
        $response['emergency_leave_count'] = $emergency_leave_count;
        $response['vacations_count'] = $vacations_count;
        $response['holidays_count'] = $holidays_count;
        $response['medical_leaves_count'] = $medical_leaves_count;
        $response['maid_count'] = $maid_count;
        $response['free_hours'] = $free_hours;
        $response['training_hours'] = $training_hours;
        $response['grand_total'] = $grand_total;
        $response['grand_suspend_total'] = $grand_suspend_total;
        $response['total_hour_week'] = $total_hour_week;
        $response['total_maid_leave_count'] =  $total_maid_leave_count;
        $response['present_maid_count'] =  $present_maid_count;
        $response['service_week_day'] = $service_week_day;
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
}
