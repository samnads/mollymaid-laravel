<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Customerexcel extends CI_Controller {

    public function __construct() {
        parent::__construct();
		if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 6)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
    }

    public function index()
	{
        
    }
	
	public function customerexportexcel_new()
    {
		$fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $payment_type = $this->input->post('payment_type');
        $source = $this->input->post('source');
        $sort_cust_type = $this->input->post('sort_cust_type');
        $search = $this->input->post('search');
		
		$data['customers'] = $this->customers_model->get_all_newcustomers_excel($fromdate, $todate, $status, $payment_type, $source, $sort_cust_type, $search);
		
        $this->load->view('customer_excel_view', $data);
    }

	public function customerexportexcel()
    {
        // echo 'hi';die;
		$fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $payment_type = $this->input->post('payment_type');
        $source = $this->input->post('source');
        $sort_cust_type = $this->input->post('sort_cust_type');
        $search = $this->input->post('search');
		
		$customers = $this->customers_model->get_all_newcustomers_excel($fromdate, $todate, $status, $payment_type, $source, $sort_cust_type, $search);
        // echo "<pre>";print_r($customers);die;
		
		$fileName = 'customers.xlsx'; 
        $styleArray = array(
            'font' => array(
                    'bold' => true,
            )
        );
		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Customer Name');
        $sheet->setCellValue('C1', 'Contact No');
        $sheet->setCellValue('D1', 'Email-Id');  
        $sheet->setCellValue('E1', 'Area');  
        $sheet->setCellValue('F1', 'Address');  
        $sheet->setCellValue('G1', 'Source');  
        $sheet->setCellValue('H1', 'Last Job');  
        $sheet->setCellValue('I1', 'Paytype');  
        $sheet->setCellValue('J1', 'Price Per Hr');  
        $sheet->setCellValue('K1', 'Added Date time');  
        $sheet->setCellValue('L1', 'Status');
		
		$sheet->getStyle("A1:K1")->applyFromArray($styleArray);   
        $sheet->getColumnDimension("A")->setAutoSize(true);       
        $sheet->getColumnDimension("B")->setAutoSize(true);       
        $sheet->getColumnDimension("C")->setAutoSize(true);       
        $sheet->getColumnDimension("D")->setAutoSize(true);       
        $sheet->getColumnDimension("E")->setAutoSize(true);       
        $sheet->getColumnDimension("F")->setAutoSize(true);       
        $sheet->getColumnDimension("G")->setAutoSize(true);       
        $sheet->getColumnDimension("H")->setAutoSize(true);       
        $sheet->getColumnDimension("I")->setAutoSize(true);       
        $sheet->getColumnDimension("J")->setAutoSize(true);
        $sheet->getColumnDimension("K")->setAutoSize(true);
        $sheet->getColumnDimension("L")->setAutoSize(true);
		
		$rows = 2;
        $i = 1;
		foreach($customers as $customer)
        {
            if($customer->building != "")
            {
                $apartmnt_no = 'Apartment No:'.$customer->building.', '.$customer->unit_no.', '.$customer->street.',';
            } else {
                $apartmnt_no = "";
            }
            if ($customer->payment_type == "D") {
                $paytype = "Daily";
            } else if ($customer->payment_type == "W") {
                $paytype = "Weekly";
            } else if ($customer->payment_type == "M") {
                $paytype = "Monthly";
            } else {
                $paytype = "";
            }
			
			if($customer->customer_status == 1)
			{
				$stats = 'Active';
			} else {
				$stats = 'Inactive';
			}
			
			$sheet->setCellValue('A' . $rows, $i);
            $sheet->setCellValue('B' . $rows, $customer->customer_name);
            $sheet->setCellValue('C' . $rows, $customer->mobile_number_1);
            $sheet->setCellValue('D' . $rows, $customer->email_address);
            $sheet->setCellValue('E' . $rows, $customer->area_name);
            $sheet->setCellValue('F' . $rows, $apartmnt_no.$customer->customer_address);
            $sheet->setCellValue('G' . $rows, $customer->customer_source);
            $sheet->setCellValue('H' . $rows, $customer->last_date);
            $sheet->setCellValue('I' . $rows, $paytype);
            $sheet->setCellValue('J' . $rows, $customer->price_hourly);
            $sheet->setCellValue('K' . $rows, $customer->customer_added_datetime);
            $sheet->setCellValue('L' . $rows, $stats);
			$rows++;
            $i++;
		}
		$writer = new Xlsx($spreadsheet);
		$writer->save("upload/".$fileName);
		header("Content-Type: application/vnd.ms-excel");
		//header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        //$writer->save('php://output');
		echo base_url()."upload/".$fileName;
        exit();
    }
	
	public function customeroutstandingexportexcel()
    {
        // echo 'hi';die;
		$fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $custid = $this->input->post('custid');
		
		$customers = $this->customers_model->get_all_newcustomers_outstanding_excel($fromdate, $todate, $status, $custid);
        // echo "<pre>";print_r($customers);die;
		
		$fileName = 'customers-outstanding.xlsx'; 
        $styleArray = array(
            'font' => array(
                    'bold' => true,
            )
        );
		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Customer Name');
        $sheet->setCellValue('C1', 'Contact No');
        $sheet->setCellValue('D1', 'Area');  
        $sheet->setCellValue('E1', 'Address');  
        $sheet->setCellValue('F1', 'Balance');  
        $sheet->setCellValue('G1', 'Added Date time');
		
		$sheet->getStyle("A1:G1")->applyFromArray($styleArray);   
        $sheet->getColumnDimension("A")->setAutoSize(true);       
        $sheet->getColumnDimension("B")->setAutoSize(true);       
        $sheet->getColumnDimension("C")->setAutoSize(true);       
        $sheet->getColumnDimension("D")->setAutoSize(true);       
        $sheet->getColumnDimension("E")->setAutoSize(true);       
        $sheet->getColumnDimension("F")->setAutoSize(true);       
        $sheet->getColumnDimension("G")->setAutoSize(true);
		
		$rows = 2;
        $i = 1;
		foreach($customers as $customer)
        {
            if($customer->building != "")
            {
                $apartmnt_no = 'Apartment No:'.$customer->building.', '.$customer->unit_no.', '.$customer->street.',';
            } else {
                $apartmnt_no = "";
            }
			
			$sheet->setCellValue('A' . $rows, $i);
            $sheet->setCellValue('B' . $rows, $customer->customer_name);
            $sheet->setCellValue('C' . $rows, $customer->mobile_number_1);
            $sheet->setCellValue('D' . $rows, $customer->area_name);
            $sheet->setCellValue('E' . $rows, $apartmnt_no.$customer->customer_address);
            $sheet->setCellValue('F' . $rows, $customer->balance);
            $sheet->setCellValue('G' . $rows, $customer->customer_added_datetime);
			$rows++;
            $i++;
		}
		$writer = new Xlsx($spreadsheet);
		$writer->save("upload/".$fileName);
		header("Content-Type: application/vnd.ms-excel");
		//header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        //$writer->save('php://output');
		echo base_url()."upload/".$fileName;
        exit();
    }
	
	public function requestexportexcel()
    {
        // echo 'hi';die;
		$fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        $status = $this->input->post('status');
        $search = $this->input->post('search');
		
		$requests = $this->users_model->get_all_newrequests_excel($fromdate, $todate, $status, $search);
        // echo "<pre>";print_r($customers);die;
		
		$fileName = 'requests.xlsx'; 
        $styleArray = array(
            'font' => array(
                    'bold' => true,
            )
        );
		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Sl.No');
        $sheet->setCellValue('B1', 'Customer Name');
        $sheet->setCellValue('C1', 'Service Date');
        $sheet->setCellValue('D1', 'Shift');  
        $sheet->setCellValue('E1', 'Maid');  
        $sheet->setCellValue('F1', 'Reason');  
        $sheet->setCellValue('G1', 'Requested By');  
        $sheet->setCellValue('H1', 'Approved By');  
        $sheet->setCellValue('I1', 'Status');
		
		$sheet->getStyle("A1:I1")->applyFromArray($styleArray);   
        $sheet->getColumnDimension("A")->setAutoSize(true);       
        $sheet->getColumnDimension("B")->setAutoSize(true);       
        $sheet->getColumnDimension("C")->setAutoSize(true);       
        $sheet->getColumnDimension("D")->setAutoSize(true);       
        $sheet->getColumnDimension("E")->setAutoSize(true);       
        $sheet->getColumnDimension("F")->setAutoSize(true);       
        $sheet->getColumnDimension("G")->setAutoSize(true);       
        $sheet->getColumnDimension("H")->setAutoSize(true);       
        $sheet->getColumnDimension("I")->setAutoSize(true);
		
		$rows = 2;
        $i = 1;
		foreach($requests as $val)
        {
			if($val->request_status == 0)
			{
				$status = "Pending";
			} else if($val->request_status == 1)
			{
				$status = "Approved";
			} else if($val->request_status == 2)
			{
				$status = "Rejected";
			} else if($val->request_status == 3)
			{
				$status = "Cancelled";
			}
			
			$sheet->setCellValue('A' . $rows, $i);
            $sheet->setCellValue('B' . $rows, $val->customer_name);
            $sheet->setCellValue('C' . $rows, ($val->service_date) ? date("d/m/Y", strtotime($val->service_date)) : "");
            $sheet->setCellValue('D' . $rows, $val->time_from . '-' . $val->time_to);
            $sheet->setCellValue('E' . $rows, $val->maid_name);
            $sheet->setCellValue('F' . $rows, $val->request_reason);
            $sheet->setCellValue('G' . $rows, $val->user_fullname);
            $sheet->setCellValue('H' . $rows, $val->approvedBy);
            $sheet->setCellValue('I' . $rows, $status);
			$rows++;
            $i++;
		}
		$writer = new Xlsx($spreadsheet);
		$writer->save("upload/".$fileName);
		header("Content-Type: application/vnd.ms-excel");
		//header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        //$writer->save('php://output');
		echo base_url()."upload/".$fileName;
        exit();
    }

}
