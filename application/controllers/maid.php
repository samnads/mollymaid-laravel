<?php
// error_reporting(E_ALL);
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Maid extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!is_user_loggedin()) {
			redirect('logout');
		}
		if (!user_permission(user_authenticate(), 3)) {
			show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
		}
		$this->load->model('customers_model');
		$this->load->model('maids_model');
		$this->load->model('service_types_model');
		$this->load->model('bookings_model');
		$this->load->model('tablets_model');
		$this->load->model('zones_model');
		$this->load->model('settings_model');
		$this->load->library('upload');
		$this->load->helper('google_api_helper');
		$this->load->helper('curl_helper');
		$this->load->model('call_management_model');
	}

	public function index()
	{

		$data['message'] = "No Message";

		if ($this->input->post('maid_sub')) {
			//echo '<pre>';print_r($_POST);exit;

			$maid_name      = $this->input->post('maid_name');
			$gender         = $this->input->post('gender');
			$nationality    = $this->input->post('nationality');
			//$image          = $this->input->post('img_name_resp');
			$present_address    = $this->input->post('present_address');
			$permanent_address  = $this->input->post('permanent_address');
			$mobile_number1 = $this->input->post('mobile1');
			$mobile_number2 = $this->input->post('mobile2');
			$doj            = $this->input->post('doj');
			$flat           = $this->input->post('flat');
			$team           = $this->input->post('team') ?: NULL;
			$services       = $this->input->post('services');
			$notes          = $this->input->post('notes');
			$username          = $this->input->post('username');
			$password          = $this->input->post('password');
			$passport_number = $this->input->post('passport_number');
			$passport_exp   = $this->input->post('passport_expiry');
			if ($passport_exp != '') {
				list($day, $month, $year) = explode("/", $passport_exp);
				$passport_expiry = "$year-$month-$day";
			} else {
				$passport_expiry = "";
			}
			$visa_number    = $this->input->post('visa_number');
			$visa_exp       = $this->input->post('visa_expiry');
			if ($visa_exp != '') {
				list($day, $month, $year) = explode("/", $visa_exp);
				$visa_expiry = "$year-$month-$day";
			} else {
				$visa_expiry = "";
			}
			$labour_number  = $this->input->post('labour_number');
			$labour_exp     = $this->input->post('labour_expiry');
			if ($labour_exp != '') {
				list($day, $month, $year) = explode("/", $labour_exp);
				$labour_expiry = "$year-$month-$day";
			} else {
				$labour_expiry = "";
			}
			$emirates_id    = $this->input->post('emirates_id');
			$emirates_exp   = $this->input->post('emirates_expiry');
			if ($emirates_exp != '') {
				list($day, $month, $year) = explode("/", $emirates_exp);
				$emirates_expiry = "$year-$month-$day";
			} else {
				$emirates_expiry = "";
			}
			$added = date('Y-m-d h:i:s');
			//echo $added;exit;
			$image_file = "";
			$vimage_file = "";
			$limage_file = "";
			$eimage_file = "";
			$employee_type = $this->input->post('employee_type');

			$data = array(
				'maid_name' => $maid_name,
				'maid_gender' => $gender,
				'maid_nationality' => $nationality,
				'maid_present_address' => $present_address,
				'maid_permanent_address' => $permanent_address,
				'maid_mobile_1' => $mobile_number1,
				'maid_mobile_2' => $mobile_number2,
				'maid_whatsapp_no_1' => $this->input->post('whatsapp_number_1') ?: NULL,
				'flat_id' => $flat,
				'team_id' => $team,
				//'maid_photo_file' => $image,
				'maid_passport_number' => $passport_number,
				//'maid_passport_expiry_date' => $passport_expiry,
				'maid_visa_number' => $visa_number,
				//'maid_visa_expiry_date' => $visa_expiry,
				'maid_labour_card_number' => $labour_number,
				//'maid_labour_card_expiry_date' => $labour_expiry,
				'maid_emirates_id' => $emirates_id,
				//'maid_emirates_expiry_date' => $emirates_expiry,
				'maid_notes' => $notes,
				'username' => $username,
				'password' => $password,
				'maid_status' => 1,
				'maid_added_datetime' => $added,
				'maid_joining' => $doj,
				'maid_priority' => $this->input->post('maid_priority') ?: NULL,
				'employee_type_id' => $employee_type,
				'odoo_new_maid_id' => 0,
			);
			if ($this->input->post('avatar_base64') != null) {
				$data['maid_photo_file'] = upload_base64($this->input->post('avatar_base64'));
			}
			$result = $this->maids_model->add_maids($data);
			$id = $result;
			// $odoo_maid_id = $this->do_maids_odoo_api($data);
			// $this->maids_model->update_maids(array('odoo_package_maid_id' => $odoo_maid_id,'odoo_package_maid_status' => 1),$id);

			if ($_FILES['attach_passport'] != '') {
				//$config['image_library'] = 'gd2';
				$original_path = './maid_passport';
				$extension = end(explode(".", $_FILES['attach_passport']['name']));
				$uniqueid = date('Ymdhis');
				//              $filename = $uniqueid . "." . $extension;
				$filename = "mpassport_" . $id . "." . $extension;
				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);

				if (!$this->upload->do_upload("attach_passport")) {
					//echo $this->upload->display_errors();     
				} else {
					$img_data = array('upload_data' => $this->upload->data("attach_passport"));
					$image_file = $img_data['upload_data']['file_name'];
					//echo $image_file;exit;
				}
			}

			if ($_FILES['attach_visa'] != '') {
				//$config['image_library'] = 'gd2';
				$original_path = './maid_visa';

				$extension = end(explode(".", $_FILES['attach_visa']['name']));
				$uniqueid = date('Ymdhis');
				$filename = "mvisa_" . $id . "." . $extension;
				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("attach_visa")) {
					//echo $this->upload->display_errors();     
				} else {
					$img_dat = array('upload_data' => $this->upload->data("attach_visa"));
					$vimage_file = $img_dat['upload_data']['file_name'];
					//echo $vimage_file;exit;
				}
			}
			if ($_FILES['attach_labour'] != '') {

				//$config['image_library'] = 'gd2';
				$original_path = './maid_labour';

				$extension = end(explode(".", $_FILES['attach_labour']['name']));
				$uniqueid = date('Ymdhis');
				$filename = "mlabour_" . $id . "." . $extension;

				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("attach_labour")) {
					//echo $this->upload->display_errors();     
				} else {
					//$this->load->library('upload', $config);
					$img_da = array('upload_data' => $this->upload->data("attach_labour"));
					$limage_file = $img_da['upload_data']['file_name'];
					//echo $limage_file;exit;
				}
			}
			if ($_FILES['attach_emirates'] != '') {
				//$config['image_library'] = 'gd2';
				$original_path = './maid_emirates';

				$extension = end(explode(".", $_FILES['attach_emirates']['name']));
				$uniqueid = date('Ymdhis');
				$filename = "memirates_" . $id . "." . $extension;

				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("attach_emirates")) {
					//echo $this->upload->display_errors();     
				} else {
					//$this->load->library('upload', $config);
					$img_d = array('upload_data' => $this->upload->data("attach_emirates"));
					$eimage_file = $img_d['upload_data']['file_name'];
					//echo $eimage_file;
				}
			}

			$datas = array(
				'maid_passport_file' => $image_file,
				'maid_visa_file' => $vimage_file,
				'maid_labour_card_file' => $limage_file,
				'maid_emirates_file' => $eimage_file,
			);

			$this->maids_model->update_attachments($datas, $id);

			for ($i = 0; $i < count($services); $i++) {
				$dat = array(
					'maid_id' => $id,
					'service_type_id' => $services[$i],
				);
				$this->maids_model->update_services($dat, $id);
			}

			/* For storing address datas */

			$local_house = $this->input->post('local_house');
			$local_street = $this->input->post('local_street');
			$local_town = $this->input->post('local_town');
			$local_city = $this->input->post('local_city');
			$local_country = $this->input->post('local_country');
			$perm_house = $this->input->post('perm_house');
			$perm_street = $this->input->post('perm_street');
			$perm_town = $this->input->post('perm_town');
			$perm_city = $this->input->post('perm_city');
			$perm_country = $this->input->post('perm_country');
			$emergency_local_name = $this->input->post('emergency_local_name');
			$emergency_local_relationship = $this->input->post('emergency_local_relationship');
			$emergency_local_contact = $this->input->post('emergency_local_contact');
			$emergency_local_whatsapp = $this->input->post('emergency_local_whatsapp');
			$emergency_home_name = $this->input->post('emergency_home_name');
			$emergency_home_relationship = $this->input->post('emergency_home_relationship');
			$emergency_home_contact = $this->input->post('emergency_home_contact');
			$emergency_home_whatsapp = $this->input->post('emergency_home_whatsapp');
			$emergency_home_country = $this->input->post('emergency_home_country');

			$localAddress = array(
				'maid_id' => $id,
				'address_type_id' => 1,
				'country_id' => $local_country,
				'house_number' => $local_house,
				'street' => $local_street,
				'town' => $local_town,
				'city' => $local_city,

			);

			$permanentAddress = array(
				'maid_id' => $id,
				'address_type_id' => 2,
				'country_id' => $perm_country,
				'house_number' => $perm_house,
				'street' => $perm_street,
				'town' => $perm_town,
				'city' => $perm_city,
			);

			$emergencylocalAddress = array(
				'maid_id' => $id,
				'address_type_id' => 3,
				'name' => $emergency_local_name,
				'relationship' => $emergency_local_relationship,
				'mobile_no_1' => $emergency_local_contact,
				'whatsapp_no_1' => $emergency_local_whatsapp,
			);

			$emergencyhomeAddress = array(
				'maid_id' => $id,
				'address_type_id' => 4,
				'country_id' => $emergency_home_country,
				'name' => $emergency_home_name,
				'relationship' => $emergency_home_relationship,
				'mobile_no_1' => $emergency_home_contact,
				'whatsapp_no_1' => $emergency_home_whatsapp,
			);

			$this->maids_model->add_maids_address($localAddress, $permanentAddress, $emergencylocalAddress, $emergencyhomeAddress);

			//print_r($result);
			//$days = $this->input->post('days');
			// $locations = $this->input->post('locations');
			// $time_from = $this->input->post('time_from');
			// $time_to = $this->input->post('time_to');

			/*	for ($i = 0; $i < count($days); $i++) {
           
            if (isset($days[$i])) {
                $data = array(
					'employee_id'=>$result,
                    'week_day' => $days[$i],
                    // 'location_id' => $locations[$i],
                    // 'time_from' => $time_from[$i],
                    // 'time_to' => $time_to[$i],
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
                ); */



			/* $updated_fields = array(); 
			   
			   
						for ($i = 0; $i < count($days); $i++) {
						   if (isset($days[$i])) {
						   $day_locations = $locations[$days[$i]];
						   
						   for ($j = 0; $j < count($day_locations); $j++) {
						   
						$updated_fields[$i]['employee_id'] = 56;
						$updated_fields[$i]['week_day'] = $days[$i];
						$updated_fields[$i]['location_id'] = $day_locations[$j];
					   //  $updated_fields[$i]['time_from'] = $time_from[$i];
					   //  $updated_fields[$i]['time_to'] =  $time_to[$i];
						$updated_fields[$i]['created_at'] =date('Y-m-d H:i:s');
						$updated_fields[$i]['updated_at'] =date('Y-m-d H:i:s');
						
						   
			   
					   }
				   
					   $this->maids_model->insertAvailability($updated_fields);
				   }
				   
			   } */







			$selectedDays = $_POST['days'];
			$locations = $_POST['locations'];
			$time_from = $_POST['time_from'];
			$time_to = $_POST['time_to'];
			// print_r($_POST['locations']);die;

			$updated_fields = array();

			for ($i = 0; $i < count($selectedDays); $i++) {
				if (isset($selectedDays[$i])) {
					$day = $selectedDays[$i];
					//$day_locations = $locations[$day];
					$location = $locations[$day];
					$day_time_from = date('H:i', strtotime($time_from[$day]));
					$day_time_to = date('H:i', strtotime($time_to[$day]));

					// for ($j = 0; $j < count($day_locations); $j++) {
					$data = array(
						'employee_id' => $result,
						'week_day' => $day,
						'location_id' => $location,
						'time_from' => $day_time_from,
						'time_to' => $day_time_to,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					);

					$updated_fields[] = $data;
					// }
				}
			}

			$this->maids_model->insertAvailability($updated_fields);
			/* ******************************** ***************************/

			/* For storing skills */

			$employeeId = $result;
			$skills = $this->input->post('skills');
			$levels = $this->input->post('levels');
			$note = $this->input->post('note');

			$affectedRows = $this->maids_model->insert_skills($employeeId, $skills, $levels, $note);

			/* ******************************************************************* */

			/* For storing driver availability */

			if ($this->input->post('employee_type') == '2') {
				$driverId = $result;
				$location_id = $this->input->post('d_location');
				$from_date = $this->input->post('from_date');
				$to_date = $this->input->post('to_date');
				$converted_date_from = date('Y-m-d', strtotime($from_date));
				$converted_date_to = date('Y-m-d', strtotime($to_date));
				$details = array(
					'driver_id' => $driverId,
					'location_id' => $location_id,
					'date_from' => $converted_date_from,
					'date_to' => $converted_date_to,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),

				);

				$this->maids_model->insert_driver_availability($details);
			}





			/* ******************************************************************* */


			if ($this->input->post('teamLeaderSelect') != '') {
				$maidId = $result;
				$leader_id = $this->input->post('teamLeaderSelect');
				$data = array(
					'maid_leader_id' => $leader_id
				);

				$this->maids_model->insert_leader($maidId, $data);
			}

			if ($this->input->post('leader') == '1') {
				$maidId = $result;
				$data = array(
					'maid_leader_id' => $maidId
				);
				$this->maids_model->insert_leader($maidId, $data);
			}





			$data['message'] = "success";
			$activity = $this->maids_model->add_activity($id, 3);
			redirect('maid/view/' . $id);
			exit();
		}

		if ($this->input->post('skill_add')) {
			if ($this->input->post('newSkillInput') != '') {

				$newSkill = $this->input->post('newSkillInput');
				$data = array(
					'skill' => $newSkill,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				$this->maids_model->insert_new_skills($data);
			}

			redirect('maid/add');
		}
		//$data = array();
		$data['flats'] = $this->settings_model->get_flats();
		$data['services'] = $this->settings_model->get_services_maid();
		$data['teams'] = $this->settings_model->get_teams();
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$data['employee_type'] = $this->settings_model->get_employee_types();
		$data['countries'] = $this->settings_model->get_countries();
		$data['locations'] = $this->settings_model->get_locations_list();
		$data['skills'] = $this->settings_model->get_skills_list();
		$data['levels'] = $this->settings_model->get_levels_list();
		$data['team_leaders'] = $this->settings_model->get_leaders_list();
		$layout_data['content_body'] = $this->load->view('maid', $data, TRUE);
		$layout_data['page_title'] = 'New Staff';
		$layout_data['meta_description'] = 'New Staff';
		$layout_data['css_files'] = array('datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
		$this->load->view('layouts/default', $layout_data);
	}

	function maidimgupload()
	{
		$uploaddir = './maidimg/';
		$extension = end(explode(".", $_FILES['uploadfile']['name']));
		$uniqueid = date('Ymdhis');
		$file = $uploaddir . $uniqueid . "." . $extension;
		if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
			echo $uniqueid . "." . $extension;
		} else {
			echo "error";
		}
	}

	function maid_list()
	{
		$data = array();
		$active = $this->uri->segment(2) ? $this->uri->segment(2) : 2;
		//$data['maids'] = $this->maids_model->get_all_maids($active);

		// $data['maids'] = $this->maids_model->get_all_maids_new($active);
		$data['employee_types'] = $this->maids_model->get_all_employee_types();
		if ($this->input->post('zone_report')) {
			$status = $this->input->post('maid-status');
			$employeeType = $this->input->post('employee-type');
			$data['status'] = $this->input->post('maid-status');
			$data['employeeType'] = $this->input->post('employee-type');
			$data['maids'] = $this->maids_model->get_all_maids_new_filter($status, $employeeType);
		} else {
			$data['maids'] = $this->maids_model->get_all_maids_new($active);
		}
		/******************************************* */
		if (is_iterable($data['maids'])) {
			foreach ($data['maids'] as $key => $maid) {
				$data['maids'][$key]['maid_photo_file'] = check_and_get_img_url('./upload/maid_avatars/' . $maid['maid_photo_file'], 'maid-avatar.png');
			}
		}
		/******************************************* */
		$data['active'] = $active;
		// echo '<pre>';
		// print_r($data['maids']);exit;
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('maid_list', $data, TRUE);
		$layout_data['page_title'] = 'Employees';
		$layout_data['meta_description'] = 'Employees';
		$layout_data['css_files'] = array('demo.css');
		$layout_data['external_js_files'] = array();
		$layout_data['maids_active'] = '1';
		$layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'moment.min.js', 'jquery.dataTables.min.js');
		$this->load->view('layouts/default', $layout_data);
	}

	function toExcel()
	{
		$data = array();
		$active = $this->uri->segment(3) ? $this->uri->segment(3) : 2;
		$data['maids'] = $this->maids_model->get_all_maids($active);
		//$data['active'] = $active;
		//print_r($data['maids']);exit;
		$this->load->view('maid_spreadsheet_view', $data);
	}

	//    function edit_maid()
	//    {
	//        $maid_id = $this->input->post('maid_id');
	//        $data = $this->maids_model->get_maid_details($maid_id);
	//        echo json_encode($data);
	//    }
	function remove_maid()
	{
		$maid_id = $this->input->post('maid_id');
		$this->maids_model->delete_maid($maid_id);
		$this->maids_model->delete_maid_services($maid_id);
	}

	function view_maid()
	{
		$data = $this->maids_model->get_maid_details($this->input->post('maid_id'));
		$data[0]['maid_photo_file'] = check_and_get_img_url('./upload/maid_avatars/' . $data[0]['maid_photo_file'], 'maid-avatar.png');
		echo json_encode($data);
	}

	function edit_maid($maid_id)
	{
		$data['message'] = "No Message";
		if ($this->input->post('maid_edit')) {
			$maid_id = $this->input->post('maid_id');
			$maid_name = $this->input->post('maid_name');
			$gender = $this->input->post('gender');
			$nationality = $this->input->post('nationality');
			$present_address = $this->input->post('present_address');
			$permanent_address = $this->input->post('permanent_address');
			$mobile_number1 = $this->input->post('mobile1') ?: NULL;
			$mobile_number2 = $this->input->post('mobile2') ?: NULL;
			$flat = $this->input->post('flat') ?: NULL;
			$appstatus = $this->input->post('appstatus');
			// $team = $this->input->post('team');
			$team = null;
			$services = $this->input->post('services');
			$notes = $this->input->post('notes');
			$passport_number = $this->input->post('passport_number') ?: NULL;
			$passport_exp = $this->input->post('passport_expiry');
			//$old_image = $this->input->post('old_image');
			//$img = $this->input->post('img_name_resp');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			/*if ($img == "") {
                $image = $old_image;
            } else {
                $image = $img;
            }*/
			if ($passport_exp != '') {
				list($day, $month, $year) = explode("/", $passport_exp);
				$passport_expiry = "$year-$month-$day";
			} else {
				$passport_expiry = "";
			}
			$visa_number = $this->input->post('visa_number');
			$visa_exp = $this->input->post('visa_expiry');
			if ($visa_exp != '') {
				list($day, $month, $year) = explode("/", $visa_exp);
				$visa_expiry = "$year-$month-$day";
			} else {
				$visa_expiry = "";
			}
			$labour_number = $this->input->post('labour_number') ?: NULL;
			$labour_exp = $this->input->post('labour_expiry');
			if ($labour_exp != '') {
				list($day, $month, $year) = explode("/", $labour_exp);
				$labour_expiry = "$year-$month-$day";
			} else {
				$labour_expiry = "";
			}
			$emirates_id = $this->input->post('emirates_id') ?: NULL;
			$emirates_exp = $this->input->post('emirates_expiry');
			if ($emirates_exp != '') {
				list($day, $month, $year) = explode("/", $emirates_exp);
				$emirates_expiry = "$year-$month-$day";
			} else {
				$emirates_expiry = "";
			}
			$added = date('Y-m-d h:i:s');
			//echo $added;exit;

			$image_file = $this->input->post('old_attach_passport');
			$vimage_file = $this->input->post('old_attach_visa');
			$limage_file = $this->input->post('old_attach_labour');
			$eimage_file = $this->input->post('old_attach_emirates');
			$employee_type = $this->input->post('employee_type');

			$data = array(
				'maid_name' => $maid_name,
				'maid_full_name' => NULL,
				'maid_gender' => $gender,
				'maid_nationality' => $nationality,
				'maid_present_address' => $present_address,
				'maid_permanent_address' => $permanent_address,
				'maid_mobile_1' => $mobile_number1,
				'maid_mobile_2' => $mobile_number2,
				'maid_whatsapp_no_1' => $this->input->post('whatsapp_number_1') ?: NULL,
				'flat_id' => $flat,
				'team_id' => $team,
				'maid_login_status' => $appstatus,
				'maid_passport_number' => $passport_number,
				// 'maid_passport_expiry_date' => $passport_expiry,
				'maid_visa_number' => $visa_number,
				// 'maid_visa_expiry_date' => $visa_expiry,
				'maid_labour_card_number' => $labour_number,
				// 'maid_labour_card_expiry_date' => $labour_expiry,
				'maid_emirates_id' => $emirates_id,
				// 'maid_emirates_expiry_date' => $emirates_expiry,
				'maid_notes' => $notes,
				'username' => $username,
				'password' => $password,
				'maid_status' => 1,
				'maid_last_modified_datetime' => $added,
				'maid_priority' => $this->input->post('maid_priority') ?: NULL,
				'employee_type_id' => $employee_type,
				'odoo_new_maid_id' => 0,
			);
			if ($this->input->post('avatar_base64') != NULL) {
				$data['maid_photo_file'] = upload_base64($this->input->post('avatar_base64'));
			}
			$this->maids_model->update_maids($data, $maid_id);

			$get_maid_details = $this->maids_model->get_maid_by_id($maid_id);

			// if($get_maid_details->odoo_package_maid_status == 0)
			// {
			// $odoo_maid_id = $this->do_maids_odoo_api($data);
			// $this->maids_model->update_maids(array('odoo_package_maid_id' => $odoo_maid_id,'odoo_package_maid_status' => 1),$maid_id);
			// } else {
			// $odo_maid_affected = $this->update_maid_inodoo($data, $get_maid_details->odoo_package_maid_id);
			// }

			if ($_FILES['attach_passport'] != '') {
				//$config['image_library'] = 'gd2';
				$original_path = './maid_passport';
				$extension = end(explode(".", $_FILES['attach_passport']['name']));
				$uniqueid = date('Ymdhis');
				//              $filename = $uniqueid . "." . $extension;
				$filename = "mpassport_" . $maid_id . "." . $extension;
				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);

				if (!$this->upload->do_upload("attach_passport")) {
					//echo $this->upload->display_errors();     
				} else {
					$img_data = array('upload_data' => $this->upload->data("attach_passport"));
					$image_file = $img_data['upload_data']['file_name'];
					//echo $image_file;exit;
				}
			}
			if ($_FILES['attach_visa'] != '') {
				//$config['image_library'] = 'gd2';
				$original_path = './maid_visa';

				$extension = end(explode(".", $_FILES['attach_visa']['name']));
				$uniqueid = date('Ymdhis');
				$filename = "mvisa_" . $maid_id . "." . $extension;
				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("attach_visa")) {
					//echo $this->upload->display_errors();     
				} else {
					$img_dat = array('upload_data' => $this->upload->data("attach_visa"));
					$vimage_file = $img_dat['upload_data']['file_name'];
					//echo $vimage_file;exit;
				}
			}
			if ($_FILES['attach_labour'] != '') {

				//$config['image_library'] = 'gd2';
				$original_path = './maid_labour';

				$extension = end(explode(".", $_FILES['attach_labour']['name']));
				$uniqueid = date('Ymdhis');
				$filename = "mlabour_" . $maid_id . "." . $extension;

				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("attach_labour")) {
					//echo $this->upload->display_errors();     
				} else {
					//$this->load->library('upload', $config);
					$img_da = array('upload_data' => $this->upload->data("attach_labour"));
					$limage_file = $img_da['upload_data']['file_name'];
					//echo $limage_file;exit;
				}
			}
			if ($_FILES['attach_emirates'] != '') {
				//$config['image_library'] = 'gd2';
				$original_path = './maid_emirates';

				$extension = end(explode(".", $_FILES['attach_emirates']['name']));
				$uniqueid = date('Ymdhis');
				$filename = "memirates_" . $maid_id . "." . $extension;

				$config = array(
					'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
					'upload_path' => $original_path,
					'file_name' => $filename
				);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload("attach_emirates")) {
					//echo $this->upload->display_errors();     
				} else {
					//$this->load->library('upload', $config);
					$img_d = array('upload_data' => $this->upload->data("attach_emirates"));
					$eimage_file = $img_d['upload_data']['file_name'];
					//echo $eimage_file;
				}
			}

			$datas = array(
				'maid_passport_file' => $image_file,
				'maid_visa_file' => $vimage_file,
				'maid_labour_card_file' => $limage_file,
				'maid_emirates_file' => $eimage_file,
			);

			$this->maids_model->update_attachments($datas, $maid_id);
			$this->maids_model->delete_maid_services($maid_id);
			for ($i = 0; $i < count($services); $i++) {
				$dat = array(
					'maid_id' => $maid_id,
					'service_type_id' => $services[$i],
				);
				$this->maids_model->update_services($dat, $maid_id);
			}


			/* For storing address datas */

			$local_house = $this->input->post('local_house');
			$local_street = $this->input->post('local_street');
			$local_town = $this->input->post('local_town');
			$local_city = $this->input->post('local_city');
			$local_country = $this->input->post('local_country');
			$perm_house = $this->input->post('perm_house');
			$perm_street = $this->input->post('perm_street');
			$perm_town = $this->input->post('perm_town');
			$perm_city = $this->input->post('perm_city');
			$perm_country = $this->input->post('perm_country');
			$emergency_local_name = $this->input->post('emergency_local_name');
			$emergency_local_relationship = $this->input->post('emergency_local_relationship');
			$emergency_local_contact = $this->input->post('emergency_local_contact');
			$emergency_local_whatsapp = $this->input->post('emergency_local_whatsapp');
			$emergency_home_name = $this->input->post('emergency_home_name');
			$emergency_home_relationship = $this->input->post('emergency_home_relationship');
			$emergency_home_contact = $this->input->post('emergency_home_contact');
			$emergency_home_whatsapp = $this->input->post('emergency_home_whatsapp');
			$emergency_home_country = $this->input->post('emergency_home_country');

			$localAddress = array(
				'country_id' => $local_country,
				'house_number' => $local_house,
				'street' => $local_street,
				'town' => $local_town,
				'city' => $local_city,
				'address_type_id' => 1,

			);

			$permanentAddress = array(
				'country_id' => $perm_country,
				'house_number' => $perm_house,
				'street' => $perm_street,
				'town' => $perm_town,
				'city' => $perm_city,
				'address_type_id' => 2,
			);

			$emergencylocalAddress = array(
				'name' => $emergency_local_name,
				'relationship' => $emergency_local_relationship,
				'mobile_no_1' => $emergency_local_contact,
				'whatsapp_no_1' => $emergency_local_whatsapp,
				'address_type_id' => 3,
			);

			$emergencyhomeAddress = array(
				'country_id' => $emergency_home_country,
				'name' => $emergency_home_name,
				'relationship' => $emergency_home_relationship,
				'mobile_no_1' => $emergency_home_contact,
				'whatsapp_no_1' => $emergency_home_whatsapp,
				'address_type_id' => 4,
			);

			$resultt = $this->maids_model->update_maids_address($maid_id, $localAddress, $permanentAddress, $emergencylocalAddress, $emergencyhomeAddress);


			/* for storing daily preference details of maid */

			$selectedDays = $_POST['days'];
			$locations = $_POST['locations'];
			$time_from = $_POST['time_from'];
			$time_to = $_POST['time_to'];


			$updated_fields = array();

			for ($i = 0; $i < count($selectedDays); $i++) {
				if (isset($selectedDays[$i])) {
					$day = $selectedDays[$i];
					//$day_locations = $locations[$day];
					$location = $locations[$day];
					$day_time_from = date('H:i', strtotime($time_from[$day]));
					$day_time_to = date('H:i', strtotime($time_to[$day]));

					// for ($j = 0; $j < count($day_locations); $j++) {
					$data = array(
						'week_day' => $day,
						'location_id' => $location,
						'time_from' => $day_time_from,
						'time_to' => $day_time_to,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
					);

					$updated_fields[] = $data;
					// }
				}
			}

			$this->maids_model->update_availability($updated_fields, $maid_id);
			/* ******************************************************************* */

			/* For updating skills */

			$employeeId = $maid_id;
			$skills = $this->input->post('skills');
			$levels = $this->input->post('levels');
			$note = $this->input->post('note');

			$affectedRows = $this->maids_model->update_employee_skills($employeeId, $skills, $levels, $note);

			/* For storing driver availability */

			if ($this->input->post('employee_type') == '2') {
				$driverId = $maid_id;
				$location_id = $this->input->post('d_location');
				$from_date = $this->input->post('from_date');
				$to_date = $this->input->post('to_date');
				$converted_date_from = date('Y-m-d', strtotime($from_date));
				$converted_date_to = date('Y-m-d', strtotime($to_date));
				$details = array(
					'location_id' => $location_id,
					'date_from' => $converted_date_from,
					'date_to' => $converted_date_to,
					'updated_at' => date('Y-m-d H:i:s'),

				);

				$this->maids_model->update_driver_availability($driverId, $details);
			}



			$data['message'] = "success";
			$activity = $this->maids_model->add_activity($maid_id, 4);
		}


		//$data = array();
		$data['maid_details'] = $this->maids_model->get_maid_details($maid_id);
		$maid_services = $this->maids_model->get_maid_services($maid_id);
		$data['maid_address_local'] = $this->maids_model->get_maid_address_local($maid_id);
		$data['maid_address_perm'] = $this->maids_model->get_maid_address_perm($maid_id);
		$data['maid_address_emer_local'] = $this->maids_model->get_maid_address_emer_local($maid_id);
		$data['maid_address_emer_home'] = $this->maids_model->get_maid_address_emer_home($maid_id);

		$user_modules = $this->maids_model->get_maid_preference($maid_id);
		$user_module_ids = array();
		foreach ($user_modules as $u_modlue) {
			array_push($user_module_ids, $u_modlue->week_day);
		}

		$modules_array = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');




		$data['modules'] = $modules_array;

		$data['user_modules'] = $user_modules;
		$data['sunday'] = $this->maids_model->get_weekday_sun($maid_id);
		$data['monday'] = $this->maids_model->get_weekday_mon($maid_id);
		$data['tuesday'] = $this->maids_model->get_weekday_tues($maid_id);
		$data['wed'] = $this->maids_model->get_weekday_wed($maid_id);
		$data['thursday'] = $this->maids_model->get_weekday_thursday($maid_id);
		$data['friday'] = $this->maids_model->get_weekday_friday($maid_id);
		$data['sat'] = $this->maids_model->get_weekday_sat($maid_id);

		$data['sun_time_from'] = $this->maids_model->get_timefrom_sun($maid_id);
		$data['mon_time_from'] = $this->maids_model->get_timefrom_mon($maid_id);
		$data['tues_time_from'] = $this->maids_model->get_timefrom_tues($maid_id);
		$data['wed_time_from'] = $this->maids_model->get_timefrom_wed($maid_id);
		$data['thurs_time_from'] = $this->maids_model->get_timefrom_thurs($maid_id);
		$data['friday_time_from'] = $this->maids_model->get_timefrom_friday($maid_id);
		$data['sat_time_from'] = $this->maids_model->get_timefrom_sat($maid_id);

		$data['sun_time_to'] = $this->maids_model->get_timeto_sun($maid_id);
		$data['mon_time_to'] = $this->maids_model->get_timeto_mon($maid_id);
		$data['tues_time_to'] = $this->maids_model->get_timeto_tues($maid_id);
		$data['wed_time_to'] = $this->maids_model->get_timeto_wed($maid_id);
		$data['thurs_time_to'] = $this->maids_model->get_timeto_thurs($maid_id);
		$data['friday_time_to'] = $this->maids_model->get_timeto_friday($maid_id);
		$data['sat_time_to'] = $this->maids_model->get_timeto_sat($maid_id);

		$data['selectedSkills'] = $this->maids_model->get_employee_skills($maid_id);
		$data['selectedLevels'] = $this->maids_model->get_employee_levels($maid_id);
		$data['note'] = $this->maids_model->get_employee_notes($maid_id);

		$data['driver_availability'] = $this->maids_model->get_driver_availability($maid_id);







		$service_maid = array();
		foreach ($maid_services as $mservices) {
			array_push($service_maid, $mservices->service_type_id);
		}
		$data['service_maid'] = $service_maid;

		//print_r($data['maid_details']);exit;
		$data['flats'] = $this->settings_model->get_flats();
		$data['services'] = $this->settings_model->get_services_maid();
		$data['teams'] = $this->settings_model->get_teams();
		$data['employee_type'] = $this->settings_model->get_employee_types_new();
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$data['countries'] = $this->settings_model->get_countries();
		$data['locations'] = $this->settings_model->get_locations_list();
		$data['skills'] = $this->settings_model->get_skills_list();
		$data['levels'] = $this->settings_model->get_levels_list();
		$layout_data['content_body'] = $this->load->view('edit_maid', $data, TRUE);
		$layout_data['page_title'] = 'Edit Staff';
		$layout_data['meta_description'] = 'Edit Staff';
		$layout_data['css_files'] = array('datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['maids_active'] = '1';
		$layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
		$this->load->view('layouts/default', $layout_data);
	}

	function editmaidimgupload()
	{
		$delmainfile = "./maidimg/" . $this->input->post('old_image');
		@unlink($delmainfile);
		$uploaddir = './maidimg/';
		$extension = end(explode(".", $_FILES['uploadfile']['name']));
		$uniqueid = date('Ymdhis');
		$file = $uploaddir . $uniqueid . "." . $extension;
		if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) {
			echo $uniqueid . "." . $extension;
		} else {
			echo "error";
		}
	}
	function change_status()
	{
		$maid_id = $this->input->post('maid_id');
		$status = $this->input->post('status');
		if ($status == 1) {
			$action = 0;
			$data = array(
				'maid_status' => 0,
				'maid_disabled_datetime' => date('Y-m-d H:i:s')
			);
			$this->maids_model->disable_status($maid_id, $data);
			$maid_details = $this->maids_model->get_maid_details($maid_id);
			if ($maid_details[0]['odoo_package_maid_id']) {
				$odoo_employee_delete = $this->delete_odoo_employee($maid_details[0]['odoo_package_maid_id']);
				$data1 = array(
					'odoo_package_maid_id' => $odoo_employee_delete,
					'odoo_package_maid_status' => 0
				);
				$this->maids_model->disable_status($maid_id, $data1);
			} 
		}
		if ($status == 0) {
			$action = 1;
			$data = array(
				'maid_status' => 1
			);
			$this->maids_model->activate_status($maid_id, $data);
		}
		if ($status == 2) {
			$action = 2;
			$data = array(
				'maid_status' => 2
			);
			$this->maids_model->disable_status($maid_id, $data);
		}
		$activity = $this->maids_model->add_activity($maid_id, $action);
	}

	public function delete_odoo_employee($employee_id)
    {
     
        $post['params']['user_id'] = 1;
        $post['params']['id'] = $employee_id;
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "employee_deletion";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_employee_id = 0;
        }
    }
	function maid_view($maid_id)
	{
		$data['maid_details']       = $this->maids_model->get_maid_details($maid_id);
		$data['maid_services']      = $this->maids_model->maid_services($maid_id);
		$data['maid_skills']      = $this->maids_model->get_maid_skills($maid_id);
		$data['maid_documents']      = $this->maids_model->get_maid_documents($maid_id);
		$data['maid_availability']      = $this->maids_model->get_maid_availability($maid_id);
		$data['maid_address']      = $this->maids_model->get_maid_addresses($maid_id);
		$data['call_complaint_report'] = $this->call_management_model->get_call_complaints_maid($maid_id);
		$data['pay_employee'] = $this->maids_model->get_pay_employee_maid($maid_id);
		$data['link_maid_id']      = $maid_id;
		// print_r($data['maid_availability']);exit;
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('view_maid', $data, TRUE);
		$layout_data['page_title']  = 'View Staff';
		$layout_data['meta_description'] = 'View Staff';
		$layout_data['css_files']   = array('jquery.lighter.css');
		$layout_data['external_js_files'] = array();
		$layout_data['maids_active'] = '1';
		$layout_data['js_files']    = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js');
		$this->load->view('layouts/default', $layout_data);
	}

	public function schedule()
	{

		if (!user_permission(user_authenticate(), 4)) {
			show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
		}
		if ($this->uri->segment(3) && strlen(trim($this->uri->segment(3) > 0))) {
			$s_date = explode('-', trim($this->uri->segment(2)));
			if (count($s_date) == 3 && checkdate($s_date[1], $s_date[0], $s_date[2]) && $s_date[2] >= 2014 && $s_date[2] <= date('Y') + 2) {
				$schedule_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];
			}
		} else {
			$schedule_date = date('d-M-Y');
			//$schedule_date = date('d-M-Y', strtotime('-1 day'));
		}

		if (!isset($schedule_date)) {
			redirect('booking');
			exit();
		}

		$week_day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

		$service_date = date('Y-m-d', strtotime($schedule_date));
		$service_week_day = date('w', strtotime($schedule_date));

		$maids = $this->maids_model->get_maids();
		$zones = $this->zones_model->get_all_zones();


		if ($this->input->post('search_maid_schedule')) {
			$s_date = explode("/", $this->input->post('start_date'));
			$e_date = explode("/", $this->input->post('end_date'));
			$maid_id = trim($this->input->post('maid_id'));

			$start_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
			$end_date = $e_date[2] . '-' . $e_date[1] . '-' . $e_date[0];
		} else {

			//edited by Betsy Bernard
			if (trim($this->input->post('start_date')) && trim($this->input->post('end_date'))) {
				$s_date = explode("/", $this->input->post('start_date'));
				$e_date = explode("/", $this->input->post('end_date'));
				$start_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
				$end_date = $e_date[2] . '-' . $e_date[1] . '-' . $e_date[0];
				$maid_id = trim($this->input->post('maid_id'));
			} else {
				$start_date = date('Y-m-d', strtotime('-1 week'));
				$end_date = date('Y-m-d');
				$maid_id = 0;
			}
		}

		$s_date = $start_date;
		$all_bookings = array();
		$maid_bookings = array();
		$leave_maid_ids = array(); // Maid Leave
		$i = 0;
		while (strtotime($end_date) >= strtotime($s_date)) {
			$bookings = $this->bookings_model->get_maid_schedule_by_date($maid_id, $s_date);


			foreach ($bookings as $booking) {
				if ($booking->customer_source == "Helpling") {
					$zonecolor = "helpling";
				} else if ($booking->customer_source == "Justmop") {
					$zonecolor = "justmop";
				} else if ($booking->customer_source == "Helper Squad") {
					$zonecolor = "helpersquad";
				} else if ($booking->customer_source == "Book any Service") {
					$zonecolor = "bookservice";
				} else if ($booking->customer_source == "Helpbit") {
					$zonecolor = "helpbit";
				} else if ($booking->customer_source == "Mr Usta") {
					$zonecolor = "mrusta";
				} else if ($booking->customer_source == "ServiceMarket") {
					$zonecolor = "servicemarket";
				} else if ($booking->customer_source == "Spec John") {
					$zonecolor = "specjohn";
				} else if ($booking->customer_source == "Spec Ian") {
					$zonecolor = "specian";
				} else if ($booking->customer_source == "Spec KC") {
					$zonecolor = "speckc";
				} else if ($booking->customer_source == "Spec Mark") {
					$zonecolor = "specmark";
				} else if ($booking->customer_source == "Spec Ayen") {
					$zonecolor = "specayen";
				} else {
					$zonecolor = "spectrum";
				}

				$maid_bookings[$s_date][strtotime($booking->time_from)]['booking_id'] = $booking->booking_id;
				$maid_bookings[$s_date][strtotime($booking->time_from)]['customer'] = $booking->customer_nick_name;
				$maid_bookings[$s_date][strtotime($booking->time_from)]['zone'] = $booking->zone_name;
				$maid_bookings[$s_date][strtotime($booking->time_from)]['start_time'] = strtotime($booking->time_from);
				$maid_bookings[$s_date][strtotime($booking->time_from)]['end_time'] = strtotime($booking->time_to);
				$maid_bookings[$s_date][strtotime($booking->time_from)]['type'] = $booking->booking_type;
				$maid_bookings[$s_date][strtotime($booking->time_from)]['zonetype'] = $zonecolor;
				$maid_bookings[$s_date][strtotime($booking->time_from)]['user'] = $booking->user_fullname ? $booking->user_fullname : 'N/A';

				$booked_slots[$s_date]['time'][strtotime($booking->time_from)] = strtotime($booking->time_to);

				$all_bookings[$booking->booking_id] = new stdClass();
				$all_bookings[$booking->booking_id]->customer_id = $booking->customer_id;
				$all_bookings[$booking->booking_id]->customer_nick_name = $booking->customer_nick_name;
				$all_bookings[$booking->booking_id]->customer_address_id = $booking->customer_address_id;
				$all_bookings[$booking->booking_id]->customer_zone = $booking->zone_name;
				$all_bookings[$booking->booking_id]->customer_area = $booking->area_name;
				$all_bookings[$booking->booking_id]->customer_address = $booking->customer_address;
				$all_bookings[$booking->booking_id]->maid_id = $booking->maid_id;
				$all_bookings[$booking->booking_id]->maid_name = $booking->maid_name;
				$all_bookings[$booking->booking_id]->service_type_id = $booking->service_type_id;
				$all_bookings[$booking->booking_id]->service_week_day = $week_day_names[$booking->service_week_day];
				$all_bookings[$booking->booking_id]->time_from = date('g:i a', strtotime($booking->time_from));
				$all_bookings[$booking->booking_id]->time_to = date('g:i a', strtotime($booking->time_to));
				$all_bookings[$booking->booking_id]->time_from_stamp = strtotime($booking->time_from);
				$all_bookings[$booking->booking_id]->time_to_stamp =  strtotime($booking->time_to);
				$all_bookings[$booking->booking_id]->service_end = $booking->service_end;
				$all_bookings[$booking->booking_id]->service_end_date = date('d/m/Y', strtotime($booking->service_end_date));
				$all_bookings[$booking->booking_id]->service_actual_end_date = date('d/m/Y', strtotime($booking->service_actual_end_date));
				$all_bookings[$booking->booking_id]->booking_note = $booking->booking_note;
				$all_bookings[$booking->booking_id]->booking_type = $booking->booking_type;
				$all_bookings[$booking->booking_id]->pending_amount = $booking->pending_amount;
				$all_bookings[$booking->booking_id]->price_hourly = $booking->price_hourly;
				$all_bookings[$booking->booking_id]->is_locked = $booking->is_locked;
				$all_bookings[$booking->booking_id]->discount = $booking->discount ? $booking->discount : 0;
				$all_bookings[$booking->booking_id]->mobile_number_1 = $booking->mobile_number_1;
				$all_bookings[$booking->booking_id]->total_amount = $booking->total_amount ? $booking->total_amount : 0;
				$all_bookings[$booking->booking_id]->cleaning_type = $booking->cleaning_material;


				$i++;
			}
			// Maid Leave
			$maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($s_date, $maid_id);

			if (isset($maids_leave_on_date) && !empty($maids_leave_on_date)) {
				$leave_maid_ids[$s_date] = 1;
			} else {
				$leave_maid_ids[$s_date] = 0;
			}
			$s_date = date("Y-m-d", strtotime("+1 day", strtotime($s_date)));
		}

		$maid_schedule = array();
		$s_date = $start_date;
		$i = 0;
		while (strtotime($end_date) >= strtotime($s_date)) {
			$maid_schedule[$i] = new stdClass();
			$maid_schedule[$i]->date = $s_date;

			++$i;
			$s_date = date("Y-m-d", strtotime("+1 day", strtotime($s_date)));
		}

		//print_r($all_bookings); exit();

		$times = array();
		$current_hour_index = 0;
		$time = '12:00 am';
		$time_stamp = strtotime($time);

		//for ($i = 0; $i < 24; $i++)
		for ($i = 0; $i < 48; $i++) {
			if (!isset($times['t-' . $i])) {
				$times['t-' . $i] = new stdClass();
			}

			$times['t-' . $i]->stamp = $time_stamp;
			$times['t-' . $i]->display = $time;

			//if(date('H') == $i && $service_date == date('Y-m-d'))
			if (date('H') == $i) {
				$current_hour_index = 't-' . (($i * 2) - 1);
				//$current_hour_index = 't-' . ($i - 1);
			}

			//$time_stamp = strtotime('+60mins', strtotime($time));
			$time_stamp = strtotime('+30mins', strtotime($time));
			$time = date('g:i a', $time_stamp);
		}




		if ($this->input->is_ajax_request()) {
			if ($this->input->post('action') && $this->input->post('action') == 'book-maid') {
				if ($this->input->post('service_date') && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0 && $this->input->post('customer_address_id') && is_numeric($this->input->post('customer_address_id')) && $this->input->post('customer_address_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 && $this->input->post('service_type_id') && is_numeric($this->input->post('service_type_id')) && $this->input->post('service_type_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('booking_type') && ($this->input->post('booking_type') == 'OD' || $this->input->post('booking_type') == 'WE' || $this->input->post('booking_type') == 'BW')) {
					/*************************************************************************************************************** */
					showPrepareTimeError($this->input->post('service_date'), $this->input->post('time_from'), $this->input->post('booking_type'), $this->input->post('repeat_days'), 'json');
					/*************************************************************************************************************** */

					$service_date = trim($this->input->post('service_date'));
					$service_date = date('Y-m-d', strtotime($service_date));
					$service_week_day = date('w', strtotime($service_date));
					//exit();

					if (strtotime($service_date) < strtotime(date('d-M-Y'))) {
						echo 'refresh';
						exit();
					}

					$customer_id = trim($this->input->post('customer_id'));
					$customer_address_id = trim($this->input->post('customer_address_id'));
					$maid_id = trim($this->input->post('maid_id'));
					$service_type_id = trim($this->input->post('service_type_id'));
					$time_from =  date('H:i', trim($this->input->post('time_from')));
					$time_to = date('H:i', trim($this->input->post('time_to')));
					$booking_type = trim($this->input->post('booking_type'));
					$is_locked = $this->input->post('is_locked') ? 1 : 0;
					$repeat_days = array($service_week_day);
					$repeat_end = 'ondate';
					$service_end_date = $service_date;
					$pending_amount = $this->input->post('pending_amount');
					$hourly_amount = $this->input->post('price_per_amount');
					$cleaning_material = $this->input->post('cleaning_material');
					$discount = trim($this->input->post('discount'));
					$total_amt = trim($this->input->post('tot_amt'));
					$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';

					if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
						echo 'refresh';
						exit();
					}

					if ($booking_type != 'OD') {
						if ($this->input->post('repeat_days') && is_array($this->input->post('repeat_days')) && count($this->input->post('repeat_days')) > 0 &&  count($this->input->post('repeat_days')) <= 7 && $this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate')) {
							$repeat_days = $this->input->post('repeat_days');
							$repeat_end = $this->input->post('repeat_end');

							if ($repeat_end == 'ondate') {
								if ($this->input->post('repeat_end_date')) {
									$repeat_end_date = $this->input->post('repeat_end_date');

									$repeat_end_date_split = explode('/', $repeat_end_date);
									if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);

										$diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

										$years = floor($diff / (365 * 60 * 60 * 24));
										$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
										$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

										if ($days < 7) //if($diff->days < 7)
										{
											echo 'refresh';
											exit();
										}

										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									} else {
										echo 'refresh';
										exit();
									}
								} else {
									echo 'refresh';
									exit();
								}
							}
						} else {
							echo 'refresh';
							exit();
						}
					}

					$time_from_stamp = trim($this->input->post('time_from'));
					$time_to_stamp = trim($this->input->post('time_to'));

					if ($booking_type == 'OD') {
						if (isset($booked_slots[$maid_id]['time'])) {
							foreach ($booked_slots[$maid_id]['time'] as $f_time => $t_time) {
								if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
									$return = array();
									$return['status'] = 'error';
									$return['message'] = 'The selected time slot is not available for this maid';

									echo json_encode($return);
									exit();
								}
							}
						}
					}

					$today_week_day = date('w', strtotime($service_date));

					if ($booking_type == 'WE') {
						foreach ($repeat_days as $repeat_day) {
							if ($repeat_day < $today_week_day) {
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							} else {
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							foreach ($bookings_on_day as $booking_on_day) {
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
								if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//latest hideif(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);

									if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
					} else if ($booking_type == 'BW') {
						foreach ($repeat_days as $repeat_day) {
							if ($repeat_day < $today_week_day) {
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							} else {
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							//$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_new($service_start_date,$maid_id, $repeat_day);
							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							//                                                echo '<pre>';
							//                                                print_r($bookings_on_day);
							//                                                exit();
							foreach ($bookings_on_day as $booking_on_day) {
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

								if ($booking_on_day->booking_type == "BW") {
									$now = strtotime($service_start_date); // or your date as well
									$your_date = strtotime($booking_on_day->service_start_date);
									$datediff = $now - $your_date;

									$week_diff = round($datediff / (60 * 60 * 24));
									$week_difference = fmod($week_diff, 14);

									if ($week_difference == 0 || $week_difference == '-0') {
										$f_time = strtotime($booking_on_day->time_from);
										$t_time = strtotime($booking_on_day->time_to);

										if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
											$return = array();
											$return['status'] = 'error';
											$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

											echo json_encode($return);
											exit();
										}
									}
								} else {
									//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//edited by vishnu
									if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
									{
										$f_time = strtotime($booking_on_day->time_from);
										$t_time = strtotime($booking_on_day->time_to);

										if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
											$return = array();
											$return['status'] = 'error';
											$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

											echo json_encode($return);
											exit();
										}
									}
								}
							}
						}
					}
				} else {
					echo 'refresh';
					exit();
				}

				$todays_new_booking = array();
				foreach ($repeat_days as $repeat_day) {
					if ($repeat_day < $today_week_day) {
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					} else {
						$day_diff = $repeat_day - $today_week_day;
					}

					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

					$booking_fields = array();
					$booking_fields['customer_id'] = $customer_id;
					$booking_fields['customer_address_id'] = $customer_address_id;
					$booking_fields['maid_id'] = $maid_id;
					$booking_fields['service_type_id'] = $service_type_id;
					$booking_fields['service_start_date'] = $service_start_date;
					$booking_fields['service_week_day'] = $repeat_day;
					$booking_fields['time_from'] = $time_from;
					$booking_fields['time_to'] = $time_to;
					$booking_fields['booking_type'] = $booking_type;
					$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
					$booking_fields['service_end_date'] = $service_end_date;
					$booking_fields['booking_note'] = $booking_note;
					$booking_fields['is_locked'] = $is_locked;
					$booking_fields['pending_amount'] = $pending_amount;
					$booking_fields['price_per_hr'] = $hourly_amount;
					if ($cleaning_material == 'Y') {
						$booking_fields['cleaning_material'] = 'Y';
					} else {
						$booking_fields['cleaning_material'] = 'N';
					}
					$booking_fields['discount'] = $discount;
					$booking_fields['booking_category'] = 'C';
					$booking_fields['total_amount'] = $total_amt;
					$booking_fields['booked_by'] = user_authenticate();
					$booking_fields['booking_status'] = 1;
					$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
					if ($booking_type == 'WE') {
						$updatefield = array();
						$updatefield['customer_booktype'] = 1;
						$updatess = $this->customers_model->update_booktype($updatefield, $customer_id);
					}
					$booking_id = $this->bookings_model->add_booking($booking_fields);

					if ($booking_id) {
						$booking_done = TRUE;

						//$this->appointment_bill($booking_id);

						if ($service_start_date == date('Y-m-d')) {
							$todays_new_booking[] = $booking_id;
						}
					}
				}

				if (isset($booking_done)) {
					if (count($todays_new_booking) > 0 && isset($customer_address_id)) {
						$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
						if (isset($c_address->zone_id)) {
							$booking = $this->bookings_model->get_booking_by_id($booking_id);
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
							$deviceid = $tablet->google_reg_id;

							$push_fields = array();
							$push_fields['tab_id'] = $tablet->tablet_id;
							$push_fields['type'] = 1;
							$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
							$push_fields['maid_id'] = $maid_id;
							$push_fields['title'] = "New Booking";
							$push_fields['customer_name'] = $booking->customer_name;
							$push_fields['maid_name'] = $booking->maid_name;
							$push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
							$push = $this->bookings_model->add_push_notifications($push_fields);
							// optional payload
							$payload = array();
							$payload['isfeedback'] = false;
							if ($push > 0) {
								$payload['pushid'] = $push;
							} else {
								$payload['pushid'] = 0;
							}

							$title = "New Booking";
							$message = $push_fields['message'];
							$res = array();
							$res['data']['title'] = $title;
							$res['data']['is_background'] = false;
							$res['data']['message'] = $message;
							$res['data']['image'] = "";
							$res['data']['payload'] = $payload;
							$res['data']['customer'] = $booking->customer_name;
							$res['data']['maid'] = $booking->maid_name;
							$res['data']['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
							$res['data']['timestamp'] = date('Y-m-d G:i:s');
							$regId = $deviceid;
							$fields = array(
								'to' => $regId,
								'data' => $res,
							);
							$return = android_customer_push($fields);
							//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking'));
						}
					}

					$return = array();
					$return['status'] = 'success';
					$return['maid_id'] = $maid_id;
					$return['customer_id'] = $customer_id;
					$return['time_from'] = $time_from;
					$return['time_to'] = $time_to;
					//$user_logged_in_session = $CI->session->userdata('user_logged_in');
					//$return['user'] = $user_logged_in_session['user_fullname'];

					echo json_encode($return);
					exit();
				}
			}
			// Edited byt Geethu
			if ($this->input->post('action') && $this->input->post('action') == 'copy-maid') {
				if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0) {
					if (strtotime($schedule_date) < strtotime(date('d-M-Y'))) {
						echo 'refresh';
						exit();
					}
					$booking_id = trim($this->input->post('booking_id'));
					$booking = $this->bookings_model->get_booking_by_id($booking_id);

					//                                        if($booking->is_locked == 1 && $booking->booked_by != user_authenticate())
					//                                        {
					//                                            echo 'locked';
					//                                            exit();
					//                                        }
					$customer_id = html_escape($booking->customer_id);
					$customer_address_id = html_escape($booking->customer_address_id);
					$maid_id = trim($this->input->post('maid_id'));
					$service_type_id = html_escape($booking->service_type_id);
					$time_from =  date('H:i', strtotime(html_escape($booking->time_from)));
					$time_to = date('H:i', strtotime(html_escape($booking->time_to)));
					$booking_type = html_escape($booking->booking_type);
					$repeat_days = array(html_escape($booking->service_week_day));
					$repeat_end = html_escape($booking->service_end) == 0 ? 'never' : 'ondate';
					$service_end_date = html_escape($booking->service_actual_end_date);
					$service_start_date = html_escape($booking->service_start_date);
					$pending_amount = html_escape($booking->pending_amount);
					$payment_type = html_escape($booking->payment_type);
					$balance = html_escape($booking->balance);
					$signed = html_escape($booking->signed);
					$cleaning_material = html_escape($booking->cleaning_material);

					$discount = html_escape($booking->discount);
					$total_amts = html_escape($booking->total_amount);
					$is_locked = html_escape($booking->is_locked) ? 1 : 0;
					$booking_note = html_escape($booking->booking_note) ? html_escape($booking->booking_note) : '';

					if (preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
						echo 'refresh';
						exit();
					}

					if ($booking_type != 'OD') {
						if ($repeat_days && is_array($repeat_days) && count($repeat_days) > 0 &&  count($repeat_days) <= 7 && $repeat_end && ($repeat_end == 'never' || $repeat_end == 'ondate')) {
							//$repeat_days = $this->input->post('repeat_days');
							//$repeat_end = $this->input->post('repeat_end');

							if ($repeat_end == 'ondate') {
								if ($this->input->post('repeat_end_date')) {
									$repeat_end_date = $this->input->post('repeat_end_date');

									$repeat_end_date_split = explode('/', $repeat_end_date);
									if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);

										$diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

										$years = floor($diff / (365 * 60 * 60 * 24));
										$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
										$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

										if ($days < 7) //if($diff->days < 7)
										{
											echo 'refresh';
											exit();
										}

										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									} else {
										echo 'refresh';
										exit();
									}
								} else {
									echo 'refresh';
									exit();
								}
							}
						} else {
							echo 'refresh';
							exit();
						}
					}

					$time_from_stamp = (html_escape($booking->time_from));
					$time_to_stamp = (html_escape($booking->time_to));

					if ($booking_type == 'OD') {
						if (isset($booked_slots[$maid_id]['time'])) {
							foreach ($booked_slots[$maid_id]['time'] as $f_time => $t_time) {
								if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
									$return = array();
									$return['status'] = 'error';
									$return['message'] = 'The selected time slot is not available for this maid';

									echo json_encode($return);
									exit();
								}
							}
						}
					}

					$today_week_day = date('w', strtotime($service_date));

					if ($booking_type == 'WE') {
						foreach ($repeat_days as $repeat_day) {
							if ($repeat_day < $today_week_day) {
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							} else {
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							foreach ($bookings_on_day as $booking_on_day) {
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

								if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//latest hideif(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);

									if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
					} else if ($booking_type == 'BW') {
						foreach ($repeat_days as $repeat_day) {
							if ($repeat_day < $today_week_day) {
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							} else {
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							//$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_new($service_start_date,$maid_id, $repeat_day);
							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							//                                                echo '<pre>';
							//                                                print_r($bookings_on_day);
							//                                                exit();
							foreach ($bookings_on_day as $booking_on_day) {
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

								if ($booking_on_day->booking_type == "BW") {
									$now = strtotime($service_start_date); // or your date as well
									$your_date = strtotime($booking_on_day->service_start_date);
									$datediff = $now - $your_date;

									$week_diff = round($datediff / (60 * 60 * 24));
									$week_difference = fmod($week_diff, 14);

									if ($week_difference == 0 || $week_difference == '-0') {
										$f_time = strtotime($booking_on_day->time_from);
										$t_time = strtotime($booking_on_day->time_to);

										if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
											$return = array();
											$return['status'] = 'error';
											$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

											echo json_encode($return);
											exit();
										}
									}
								} else {
									//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//edited by vishnu
									if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
									{
										$f_time = strtotime($booking_on_day->time_from);
										$t_time = strtotime($booking_on_day->time_to);

										if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
											$return = array();
											$return['status'] = 'error';
											$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

											echo json_encode($return);
											exit();
										}
									}
								}
							}
						}
					}
				} else {
					echo 'refresh';
					exit();
				}

				$todays_new_booking = array();
				foreach ($repeat_days as $repeat_day) {
					if ($repeat_day < $today_week_day) {
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					} else {
						$day_diff = $repeat_day - $today_week_day;
					}

					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

					$booking_fields = array();
					$booking_fields['customer_id'] = $customer_id;
					$booking_fields['customer_address_id'] = $customer_address_id;
					$booking_fields['maid_id'] = $maid_id;
					$booking_fields['service_type_id'] = $service_type_id;
					$booking_fields['service_start_date'] = $service_start_date;
					$booking_fields['service_week_day'] = $repeat_day;
					$booking_fields['time_from'] = $time_from;
					$booking_fields['time_to'] = $time_to;
					$booking_fields['booking_type'] = $booking_type;
					$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
					$booking_fields['service_end_date'] = $service_end_date;
					$booking_fields['booking_note'] = $booking_note;
					$booking_fields['pending_amount'] = $pending_amount;
					$booking_fields['discount'] = $discount;
					$booking_fields['total_amount'] = $total_amts;
					$booking_fields['booked_by'] = user_authenticate();
					$booking_fields['booking_status'] = 1;
					$booking_fields['booking_category'] = 'C';
					$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
					$booking_fields['cleaning_material'] = $cleaning_material;

					$booking_id = $this->bookings_model->add_booking($booking_fields);

					if ($booking_id) {
						$booking_done = TRUE;

						//$this->appointment_bill($booking_id);

						if ($service_start_date == date('Y-m-d')) {
							$todays_new_booking[] = $booking_id;
						}
					}
				}

				if (isset($booking_done)) {
					if (count($todays_new_booking) > 0 && isset($customer_address_id)) {
						$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
						if (isset($c_address->zone_id)) {
							$booking = $this->bookings_model->get_booking_by_id($booking_id);
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
							$deviceid = $tablet->google_reg_id;
							$push_fields = array();
							$push_fields['tab_id'] = $tablet->tablet_id;
							$push_fields['type'] = 1;
							$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
							$push_fields['maid_id'] = $maid_id;
							$push_fields['title'] = "New Booking";
							$push_fields['customer_name'] = $booking->customer_name;
							$push_fields['maid_name'] = $booking->maid_name;
							$push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
							$push = $this->bookings_model->add_push_notifications($push_fields);
							// optional payload
							$payload = array();
							$payload['isfeedback'] = false;
							if ($push > 0) {
								$payload['pushid'] = $push;
							} else {
								$payload['pushid'] = 0;
							}

							$title = "New Booking";
							$message = $push_fields['message'];
							$res = array();
							$res['data']['title'] = $title;
							$res['data']['is_background'] = false;
							$res['data']['message'] = $message;
							$res['data']['image'] = "";
							$res['data']['payload'] = $payload;
							$res['data']['customer'] = $booking->customer_name;
							$res['data']['maid'] = $booking->maid_name;
							$res['data']['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
							$res['data']['timestamp'] = date('Y-m-d G:i:s');
							$regId = $deviceid;
							$fields = array(
								'to' => $regId,
								'data' => $res,
							);
							$return = android_customer_push($fields);
							//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking'));
						}
					}

					$return = array();
					$return['status'] = 'success';
					$return['maid_id'] = $maid_id;
					$return['customer_id'] = $customer_id;
					$return['time_from'] = $time_from;
					$return['time_to'] = $time_to;

					echo json_encode($return);
					exit();
				}
			}
			// End
			if ($this->input->post('action') && $this->input->post('action') == 'update-booking') {
				if ($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('update_type') && ($this->input->post('update_type') == 'permanent' || $this->input->post('update_type') == 'one-day')) {
					$booking_idnewww = trim($this->input->post('booking_id'));
					$get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_idnewww, $service_date);
					if (count($get_dayservice_details) > 0) {
						if ($get_dayservice_details->odoo_package_activity_status == 1) {
							echo 'odoorefresh';
							exit();
						}
					}
					$service_date = $this->input->post('service_date');
					$update_type = trim($this->input->post('update_type'));
					$booking_id = trim($this->input->post('booking_id'));
					$time_from =  date('H:i', trim($this->input->post('time_from')));
					$time_to = date('H:i', trim($this->input->post('time_to')));
					$is_locked = $this->input->post('is_locked') ? 1 : 0;
					$cleaning_material = $this->input->post('cleaning_material');
					$pending_amount = $this->input->post('pending_amount');
					$discount = $this->input->post('discount');
					$total_amt = $this->input->post('total_amt');
					$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';

					//if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					if (preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE) {
						echo 'refresh';
						exit();
					}

					$booking = $this->bookings_model->get_booking_by_id($booking_id);

					if ($booking->is_locked == 1 && $booking->booked_by != user_authenticate()) {
						echo 'locked';
						exit();
					}
					if (!isset($booking->booking_id) || ($booking->service_end == 1 && strtotime($booking->service_actual_end_date) < strtotime(date('Y-m-d')))) {
						echo 'refresh';
						exit();
					}

					if (is_numeric(trim($this->input->post('customer_address_id'))) && trim($this->input->post('customer_address_id')) > 0 && $booking->customer_address_id != trim($this->input->post('customer_address_id'))) {
						$customer_address_id = trim($this->input->post('customer_address_id'));
					} else {
						$customer_address_id = $booking->customer_address_id;
					}

					$time_from_stamp = trim($this->input->post('time_from'));
					$time_to_stamp = trim($this->input->post('time_to'));

					if ($booking->booking_type == 'OD') {
						if (isset($booked_slots[$booking->maid_id]['time'])) {
							foreach ($booked_slots[$booking->maid_id]['time'] as $f_time => $t_time) {
								if (strtotime($booking->time_from) != $f_time && strtotime($booking->time_to) != $t_time) {
									if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid';

										echo json_encode($return);
										exit();
									}
								}
							}
						}

						$update_fields = array();
						$update_fields['customer_address_id'] = $customer_address_id;
						$update_fields['time_from'] = $time_from;
						$update_fields['time_to'] = $time_to;
						$update_fields['pending_amount'] = $pending_amount;
						$update_fields['discount'] = $discount;
						$update_fields['total_amount'] = $total_amt;
						$update_fields['is_locked'] = $is_locked;
						$update_fields['cleaning_material'] = $cleaning_material;
						$update_fields['booking_note'] = $booking_note;

						$this->bookings_model->update_booking($booking_id, $update_fields);

						if (count($get_dayservice_details) > 0) {
							if ($get_dayservice_details->odoo_package_activity_status == 1) {
								echo 'odoorefresh';
								exit();
							} else {
								$updatearry = array();
								$updatearry['total_fee'] = $total_amt;
								$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
							}
						}

						if ($booking->service_start_date == date('Y-m-d')) {
							$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
							if (isset($c_address->zone_id)) {
								$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
								$push_fields = array();
								$push_fields['tab_id'] = $tablet->tablet_id;
								$push_fields['type'] = 1;
								$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A", strtotime($time_from)) . '-' . date("h:i A", strtotime($time_to));
								$push_fields['maid_id'] = $booking->maid_id;
								$push_fields['title'] = "Booking Updated";
								$push_fields['customer_name'] = $booking->customer_name;
								$push_fields['maid_name'] = $booking->maid_name;
								$push_fields['booking_time'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
								$push = $this->bookings_model->add_push_notifications($push_fields);
								$deviceid = $tablet->google_reg_id;
								// optional payload
								$payload = array();
								$payload['isfeedback'] = false;
								if ($push > 0) {
									$payload['pushid'] = $push;
								} else {
									$payload['pushid'] = 0;
								}

								$title = "Update Booking";
								$message = $push_fields['message'];
								$res = array();
								$res['data']['title'] = $title;
								$res['data']['is_background'] = false;
								$res['data']['message'] = $message;
								$res['data']['image'] = "";
								$res['data']['payload'] = $payload;
								$res['data']['customer'] = $booking->customer_name;
								$res['data']['maid'] = $booking->maid_name;
								$res['data']['bookingTime'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
								$res['data']['timestamp'] = date('Y-m-d G:i:s');
								$regId = $deviceid;
								$fields = array(
									'to' => $regId,
									'data' => $res,
								);
								$return = android_customer_push($fields);
								//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Updated'));
							}
						}

						$return = array();
						$return['status'] = 'success';
						$return['maid_id'] = $booking->maid_id;
						$return['customer_id'] = $booking->customer_id;
						$return['time_from'] = $time_from;
						$return['time_to'] = $time_to;

						echo json_encode($return);
						exit();
					} else {
						if ($this->input->post('repeat_end') && (trim($this->input->post('repeat_end')) == 'never' ||  trim($this->input->post('repeat_end')) == 'ondate')) {
							$repeat_end = $this->input->post('repeat_end');

							if ($repeat_end == 'ondate') {
								if ($this->input->post('repeat_end_date')) {
									$repeat_end_date = $this->input->post('repeat_end_date');

									$repeat_end_date_split = explode('/', $repeat_end_date);
									if (count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1], $repeat_end_date_split[0], $repeat_end_date_split[2])) {
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);

										$diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

										$years = floor($diff / (365 * 60 * 60 * 24));
										$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
										$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
										if ($days < 0) //if($diff->days < 0)
										{
											echo 'refresh';
											exit();
										}

										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									} else {
										echo 'refresh';
										exit();
									}
								} else {
									echo 'refresh';
									exit();
								}
							} else {
								$service_end_date = $service_date;
							}

							$today_week_day = date('w', strtotime($service_date));
							$service_start_date = $service_date;

							if ($booking->booking_type == 'WE') {
								$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($booking->maid_id, $today_week_day);
								foreach ($bookings_on_day as $booking_on_day) {
									if ($booking_on_day->booking_id != $booking->booking_id) {
										$s_date_stamp = strtotime($service_start_date);
										$e_date_stamp = strtotime($service_end_date);
										$bs_date_stamp = strtotime($booking_on_day->service_start_date);
										$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
										if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										//latest hide if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
										{
											$f_time = strtotime($booking_on_day->time_from);
											$t_time = strtotime($booking_on_day->time_to);

											if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
												$return = array();
												$return['status'] = 'error';
												$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$today_week_day] . 's';

												echo json_encode($return);
												exit();
											}
										}
									}
								}

								if ($update_type == 'permanent') {
									$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

									// End current booking and add new booking if address id or time changes
									if ($booking->customer_address_id != $customer_address_id || $booking->time_from != $time_from || $booking->time_to != $time_to) {
										// End current booking
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($service_start_date . ' - 1 day')), 'service_end' => 1));

										$booking_fields = array();
										$booking_fields['customer_id'] = $booking->customer_id;
										$booking_fields['customer_address_id'] = $customer_address_id;
										$booking_fields['maid_id'] = $booking->maid_id;
										$booking_fields['service_type_id'] = $booking->service_type_id;
										$booking_fields['service_start_date'] = $service_start_date;
										$booking_fields['service_week_day'] = $today_week_day;
										$booking_fields['time_from'] = $time_from;
										$booking_fields['time_to'] = $time_to;
										$booking_fields['booking_type'] = $booking->booking_type;
										$booking_fields['cleaning_material'] = $cleaning_material;
										$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$booking_fields['service_end_date'] = $service_end_date;
										$booking_fields['booking_note'] = $booking_note;
										$booking_fields['is_locked'] = $is_locked;
										$booking_fields['pending_amount'] = $pending_amount;
										$booking_fields['total_amount'] = $total_amt;
										$booking_fields['discount'] = $discount;
										$booking_fields['booked_by'] = user_authenticate();
										$booking_fields['booking_status'] = 1;
										$booking_fields['booking_category'] = 'C';
										$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

										$booking_id = $this->bookings_model->add_booking($booking_fields);

										if ($booking_id) {
											if (count($get_dayservice_details) > 0) {
												if ($get_dayservice_details->odoo_package_activity_status == 1) {
													echo 'odoorefresh';
													exit();
												} else {
													$updatearry = array();
													$updatearry['total_fee'] = $total_amt;
													$updatearry['booking_id'] = $booking_id;
													$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
												}
											}
											if ($service_start_date == date('Y-m-d')) {
												$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
												if (isset($c_address->zone_id)) {
													$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
													$deviceid = $tablet->google_reg_id;
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 1;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A", strtotime($time_from)) . '-' . date("h:i A", strtotime($time_to));
													$push_fields['maid_id'] = $booking->maid_id;
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $booking->customer_name;
													$push_fields['maid_name'] = $booking->maid_name;
													$push_fields['booking_time'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
													$push = $this->bookings_model->add_push_notifications($push_fields);
													// optional payload
													$payload = array();
													$payload['isfeedback'] = false;
													if ($push > 0) {
														$payload['pushid'] = $push;
													} else {
														$payload['pushid'] = 0;
													}

													$title = "Booking Updated";
													$message = $push_fields['message'];
													$res = array();
													$res['data']['title'] = $title;
													$res['data']['is_background'] = false;
													$res['data']['message'] = $message;
													$res['data']['image'] = "";
													$res['data']['payload'] = $payload;
													$res['data']['customer'] = $booking->customer_name;
													$res['data']['maid'] = $booking->maid_name;
													$res['data']['bookingTime'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
													$res['data']['timestamp'] = date('Y-m-d G:i:s');
													$regId = $deviceid;
													$fields = array(
														'to' => $regId,
														'data' => $res,
													);
													$return = android_customer_push($fields);
													//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => 'New Booking'));
												}
											}

											$return = array();
											$return['status'] = 'success';
											$return['maid_id'] = $booking->maid_id;
											$return['customer_id'] = $booking->customer_id;
											$return['time_from'] = $time_from;
											$return['time_to'] = $time_to;

											echo json_encode($return);
											exit();
										}
									} else {
										$update_fields = array();
										$update_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$update_fields['service_end_date'] = $service_end_date;
										$update_fields['service_actual_end_date'] = $service_end_date;
										$update_fields['pending_amount'] = $pending_amount;
										$update_fields['total_amount'] = $total_amt;
										$update_fields['discount'] = $discount;
										$update_fields['is_locked'] = $is_locked;
										$update_fields['cleaning_material'] = $cleaning_material;
										$update_fields['booking_note'] = $booking_note;

										$this->bookings_model->update_booking($booking_id, $update_fields);
										if (count($get_dayservice_details) > 0) {
											if ($get_dayservice_details->odoo_package_activity_status == 1) {
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}

										if ($booking->service_start_date == date('Y-m-d')) {
											$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
											if (isset($c_address->zone_id)) {
												$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
												$deviceid = $tablet->google_reg_id;
												$push_fields = array();
												$push_fields['tab_id'] = $tablet->tablet_id;
												$push_fields['type'] = 1;
												$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
												$push_fields['maid_id'] = $booking->maid_id;
												$push_fields['title'] = "Booking Updated";
												$push_fields['customer_name'] = $booking->customer_name;
												$push_fields['maid_name'] = $booking->maid_name;
												$push_fields['booking_time'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
												$push = $this->bookings_model->add_push_notifications($push_fields);
												// optional payload
												$payload = array();
												$payload['isfeedback'] = false;
												if ($push > 0) {
													$payload['pushid'] = $push;
												} else {
													$payload['pushid'] = 0;
												}

												$title = "Booking Updated";
												$message = $push_fields['message'];
												$res = array();
												$res['data']['title'] = $title;
												$res['data']['is_background'] = false;
												$res['data']['message'] = $message;
												$res['data']['image'] = "";
												$res['data']['payload'] = $payload;
												$res['data']['customer'] = $booking->customer_name;
												$res['data']['maid'] = $booking->maid_name;
												$res['data']['bookingTime'] = $booking->newtime_from . ' - ' . $booking->newtime_to;
												$res['data']['timestamp'] = date('Y-m-d G:i:s');
												$regId = $deviceid;
												$fields = array(
													'to' => $regId,
													'data' => $res,
												);
												$return = android_customer_push($fields);
												//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Updated'));
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								} else if ($update_type == 'one-day') {
									/* Delete booking one day */
									$delete_b_fields = array();
									$delete_b_fields['booking_id'] = $booking->booking_id;
									$delete_b_fields['service_date'] = $service_date;
									$delete_b_fields['deleted_by'] = user_authenticate();

									$this->bookings_model->add_booking_delete($delete_b_fields);

									/* Add one day booking */
									$booking_fields = array();
									$booking_fields['customer_id'] = $booking->customer_id;
									$booking_fields['customer_address_id'] = $customer_address_id;
									$booking_fields['maid_id'] = $booking->maid_id;
									$booking_fields['service_type_id'] = $booking->service_type_id;
									$booking_fields['service_start_date'] = $service_date;
									$booking_fields['service_week_day'] = $today_week_day;
									$booking_fields['time_from'] = $time_from;
									$booking_fields['time_to'] = $time_to;
									$booking_fields['booking_type'] = 'OD';
									$booking_fields['service_end'] = 1;
									$booking_fields['service_end_date'] = $service_date;
									$booking_fields['booking_note'] = $booking_note;
									$booking_fields['cleaning_material'] = $cleaning_material;
									$booking_fields['is_locked'] = $is_locked;
									$booking_fields['pending_amount'] = $pending_amount;
									$booking_fields['total_amount'] = $total_amt;
									$booking_fields['discount'] = $discount;
									$booking_fields['booked_by'] = user_authenticate();
									$booking_fields['booking_status'] = 1;
									$booking_fields['booking_category'] = 'C';
									$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

									$booking_id = $this->bookings_model->add_booking($booking_fields);

									if ($booking_id) {
										if (count($get_dayservice_details) > 0) {
											if ($get_dayservice_details->odoo_package_activity_status == 1) {
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												$updatearry['booking_id'] = $booking_id;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										if ($service_date == date('Y-m-d')) {
											$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
											if (isset($c_address->zone_id)) {
												$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
												$deviceid = $tablet->google_reg_id;
												$push_fields = array();
												$push_fields['tab_id'] = $tablet->tablet_id;
												$push_fields['type'] = 1;
												$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A", strtotime($time_from)) . '-' . date("h:i A", strtotime($time_to));
												$push_fields['maid_id'] = $booking->maid_id;
												$push_fields['title'] = "Booking Updated";
												$push_fields['customer_name'] = $booking->customer_name;
												$push_fields['maid_name'] = $booking->maid_name;
												$push_fields['booking_time'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
												$push = $this->bookings_model->add_push_notifications($push_fields);
												// optional payload
												$payload = array();
												$payload['isfeedback'] = false;
												if ($push > 0) {
													$payload['pushid'] = $push;
												} else {
													$payload['pushid'] = 0;
												}

												$title = "Booking Updated";
												$message = $push_fields['message'];
												$res = array();
												$res['data']['title'] = $title;
												$res['data']['is_background'] = false;
												$res['data']['message'] = $message;
												$res['data']['image'] = "";
												$res['data']['payload'] = $payload;
												$res['data']['customer'] = $booking->customer_name;
												$res['data']['maid'] = $booking->maid_name;
												$res['data']['bookingTime'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
												$res['data']['timestamp'] = date('Y-m-d G:i:s');
												$regId = $deviceid;
												$fields = array(
													'to' => $regId,
													'data' => $res,
												);
												$return = android_customer_push($fields);
												//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => 'New Booking'));
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
							} else if ($booking->booking_type == 'BW') {

								//$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($booking->maid_id, $today_week_day);
								$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_dayupdate($booking->maid_id, $today_week_day, $update_date);
								foreach ($bookings_on_day as $booking_on_day) {
									if ($booking_on_day->booking_id != $booking->booking_id) {
										$s_date_stamp = strtotime($service_start_date);
										$e_date_stamp = strtotime($service_end_date);
										$bs_date_stamp = strtotime($booking_on_day->service_start_date);
										$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

										if ($booking_on_day->booking_type == "BW") {
											$now = strtotime($service_start_date); // or your date as well
											$your_date = strtotime($booking_on_day->service_start_date);
											$datediff = $now - $your_date;

											$week_diff = round($datediff / (60 * 60 * 24));
											$week_difference = fmod($week_diff, 14);

											if ($week_difference == 0 || $week_difference == '-0') {
												$f_time = strtotime($booking_on_day->time_from);
												$t_time = strtotime($booking_on_day->time_to);

												if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
													$return = array();
													$return['status'] = 'error';
													$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

													echo json_encode($return);
													exit();
												}
											}
										} else {
											if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
											//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
											//latest hide if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
											{
												$f_time = strtotime($booking_on_day->time_from);
												$t_time = strtotime($booking_on_day->time_to);

												if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
													$return = array();
													$return['status'] = 'error';
													$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$today_week_day] . 's';

													echo json_encode($return);
													exit();
												}
											}
										}
									}
								}

								if ($update_type == 'permanent') {

									$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

									// End current booking and add new booking if address id or time changes
									if ($booking->customer_address_id != $customer_address_id || $booking->time_from != $time_from || $booking->time_to != $time_to) {
										// End current booking
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($service_start_date . ' - 1 day')), 'service_end' => 1));

										$booking_fields = array();
										$booking_fields['customer_id'] = $booking->customer_id;
										$booking_fields['customer_address_id'] = $customer_address_id;
										$booking_fields['maid_id'] = $booking->maid_id;
										$booking_fields['service_type_id'] = $booking->service_type_id;
										$booking_fields['service_start_date'] = $service_start_date;
										$booking_fields['service_week_day'] = $today_week_day;
										$booking_fields['time_from'] = $time_from;
										$booking_fields['time_to'] = $time_to;
										$booking_fields['booking_type'] = $booking->booking_type;
										$booking_fields['cleaning_material'] = $booking->cleaning_material;
										$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$booking_fields['service_end_date'] = $service_end_date;
										$booking_fields['booking_note'] = $booking_note;
										$booking_fields['is_locked'] = $is_locked;
										$booking_fields['cleaning_material'] = $cleaning_material;
										$booking_fields['pending_amount'] = $pending_amount;
										$booking_fields['total_amount'] = $total_amt;
										$booking_fields['discount'] = $discount;
										$booking_fields['booked_by'] = user_authenticate();
										$booking_fields['booking_status'] = 1;
										$booking_fields['booking_category'] = 'C';
										$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

										$booking_id = $this->bookings_model->add_booking($booking_fields);

										if ($booking_id) {
											if (count($get_dayservice_details) > 0) {
												if ($get_dayservice_details->odoo_package_activity_status == 1) {
													echo 'odoorefresh';
													exit();
												} else {
													$updatearry = array();
													$updatearry['total_fee'] = $total_amt;
													$updatearry['booking_id'] = $booking_id;
													$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
												}
											}
											if ($service_start_date == date('Y-m-d')) {
												$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
												if (isset($c_address->zone_id)) {
													$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
													if ($tablet) {
														$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 5;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
														$push_fields['maid_id'] = $upd_booking->maid_id;
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $upd_booking->customer_name;
														$push_fields['maid_name'] = $upd_booking->maid_name;
														$push_fields['booking_time'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
														if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;
															if ($push > 0) {
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['data']['title'] = $title;
															$res['data']['is_background'] = false;
															$res['data']['message'] = $message;
															$res['data']['image'] = "";
															$res['data']['payload'] = $payload;
															$res['data']['customer'] = $upd_booking->customer_name;
															$res['data']['maid'] = $upd_booking->maid_name;
															$res['data']['bookingTime'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
															$res['data']['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'data' => $res,
															);
															$return = android_customer_push($fields);
															//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
													}
												}
											}

											$return = array();
											$return['status'] = 'success';
											$return['maid_id'] = $booking->maid_id;
											$return['customer_id'] = $booking->customer_id;
											$return['time_from'] = $time_from;
											$return['time_to'] = $time_to;

											echo json_encode($return);
											exit();
										}
									} else {

										$update_fields = array();
										$update_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$update_fields['service_end_date'] = $service_end_date;
										$update_fields['service_actual_end_date'] = $service_end_date;
										$update_fields['pending_amount'] = $pending_amount;
										$update_fields['total_amount'] = $total_amt;
										$update_fields['discount'] = $discount;
										$update_fields['is_locked'] = $is_locked;
										$update_fields['cleaning_material'] = $cleaning_material;
										$update_fields['booking_note'] = $booking_note;

										$this->bookings_model->update_booking($booking_id, $update_fields);

										if (count($get_dayservice_details) > 0) {
											if ($get_dayservice_details->odoo_package_activity_status == 1) {
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												//$updatearry['booking_id'] = $booking_id;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}

										if ($booking->service_start_date == date('Y-m-d')) {
											$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
											if (isset($c_address->zone_id)) {
												$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
												if ($tablet) {
													$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 5;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
													$push_fields['maid_id'] = $upd_booking->maid_id;
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $upd_booking->customer_name;
													$push_fields['maid_name'] = $upd_booking->maid_name;
													$push_fields['booking_time'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
													if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
														$push = $this->bookings_model->add_push_notifications($push_fields);

													if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
														$deviceid = $tablet->google_reg_id;
														// optional payload
														$payload = array();
														$payload['isfeedback'] = false;
														if ($push > 0) {
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}

														$title = "Booking Updated";
														$message = $push_fields['message'];
														$res = array();
														$res['data']['title'] = $title;
														$res['data']['is_background'] = false;
														$res['data']['message'] = $message;
														$res['data']['image'] = "";
														$res['data']['payload'] = $payload;
														$res['data']['customer'] = $upd_booking->customer_name;
														$res['data']['maid'] = $upd_booking->maid_name;
														$res['data']['bookingTime'] = $upd_booking->newtime_from . ' - ' . $upd_booking->newtime_to;
														$res['data']['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'data' => $res,
														);
														$return = android_customer_push($fields);
														//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
													}
													$booking = $this->bookings_model->get_booking_by_id($booking_id);
												}
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								} else if ($update_type == 'one-day') {



									/* Delete booking one day */
									$delete_b_fields = array();
									$delete_b_fields['booking_id'] = $booking->booking_id;
									$delete_b_fields['service_date'] = $service_date;
									$delete_b_fields['deleted_by'] = user_authenticate();

									$this->bookings_model->add_booking_delete($delete_b_fields);

									/* Add one day booking */
									$booking_fields = array();
									$booking_fields['customer_id'] = $booking->customer_id;
									$booking_fields['customer_address_id'] = $customer_address_id;
									$booking_fields['maid_id'] = $booking->maid_id;
									$booking_fields['service_type_id'] = $booking->service_type_id;
									$booking_fields['service_start_date'] = $service_date;
									$booking_fields['service_week_day'] = $today_week_day;
									$booking_fields['time_from'] = $time_from;
									$booking_fields['time_to'] = $time_to;
									$booking_fields['booking_type'] = 'OD';
									$booking_fields['service_end'] = 1;
									$booking_fields['service_end_date'] = $service_date;
									$booking_fields['booking_note'] = $booking_note;
									$booking_fields['cleaning_material'] = $booking->cleaning_material;;
									$booking_fields['is_locked'] = $is_locked;
									$booking_fields['cleaning_material'] = $cleaning_material;
									$booking_fields['pending_amount'] = $pending_amount;
									$booking_fields['total_amount'] = $total_amt;;
									$booking_fields['discount'] = $discount;;
									$booking_fields['booked_by'] = user_authenticate();
									$booking_fields['booking_status'] = 1;
									$booking_fields['booking_category'] = 'C';
									$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');

									$booking_id = $this->bookings_model->add_booking($booking_fields);


									if ($booking_id) {
										if (count($get_dayservice_details) > 0) {
											if ($get_dayservice_details->odoo_package_activity_status == 1) {
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												$updatearry['booking_id'] = $booking_id;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										if ($service_date == date('Y-m-d')) {
											// Edited by sandeep

											/* check bookingID in dayservice table and update new bookingId    */

											$day_service_row = $this->bookings_model->check_day_service_booking($booking->booking_id);

											if ($day_service_row) {
												$day_service_id = $day_service_row[0]->day_service_id;

												$day_service_fields = array();

												$day_service_fields['booking_id'] = $booking_id;

												//$day_service_fields['start_time']=$time_from;

												//$day_service_fields['end_time']=$time_to;

												$day_service_fields['total_fee'] = $this->get_price_booking($booking_id);

												$this->bookings_model->update_day_service_booking($day_service_id, $day_service_fields);
											}

											$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
											if (isset($c_address->zone_id)) {
												$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
												if ($tablet) {
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 3;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A", strtotime($time_from)) . '-' . date("h:i A", strtotime($time_to));
													$push_fields['maid_id'] = $booking->maid_id;
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $booking->customer_name;
													$push_fields['maid_name'] = $booking->maid_name;
													$push_fields['booking_time'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
													if (strtotime($schedule_date) <= strtotime(date('d-M-Y')))
														$push = $this->bookings_model->add_push_notifications($push_fields);

													if (strtotime($schedule_date) <= strtotime(date('d-M-Y'))) {
														$deviceid = $tablet->google_reg_id;
														// optional payload
														$payload = array();
														$payload['isfeedback'] = false;
														if ($push > 0) {
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}

														$title = "Booking Updated";
														$message = $push_fields['message'];
														$res = array();
														$res['data']['title'] = $title;
														$res['data']['is_background'] = false;
														$res['data']['message'] = $message;
														$res['data']['image'] = "";
														$res['data']['payload'] = $payload;
														$res['data']['customer'] = $booking->customer_name;
														$res['data']['maid'] = $booking->maid_name;
														$res['data']['bookingTime'] = date("h:i A", strtotime($time_from)) . ' - ' . date("h:i A", strtotime($time_to));
														$res['data']['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'data' => $res,
														);
														$return = android_customer_push($fields);
														//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
													}
													$booking = $this->bookings_model->get_booking_by_id($booking_id);
												}
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
							}
						} else {
							echo 'refresh';
							exit();
						}
					}

					echo 'refresh';
					exit();
				}
			}

			if ($this->input->post('action') && $this->input->post('action') == 'refresh-grid') {
				$data = array();
				$data['maids'] = $maids;
				$data['maid_bookings'] = $maid_bookings;
				$data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
				$data['all_bookings'] = $all_bookings;
				$data['times'] = $times;
				$data['booking_allowed'] = 1; // Enable past bookings by Geethu
				//$data['booking_allowed'] = strtotime($schedule_date) >= strtotime( date('d-M-Y')) ? 1 : 0;
				$schedule_day = date('d F Y, l', strtotime($schedule_date));
				$data['schedule_day'] = $schedule_day;
				$data['maid_schedule'] = $maid_schedule;
				$data['current_hour_index'] = $current_hour_index;
				$schedule_grid = $this->load->view('partials/maid_schedule_grid', $data, TRUE);
				$schedule_report = $this->load->view('partials/maid_schedule_report', $data, TRUE);
				/******************************************************** */
				// code snippet added by Samnad. S
				// because maid schedule ajax call not updating left days columns date
				$days = null;
				$start_date = DateTime::createFromFormat('d/m/Y', $this->input->post('start_date'))->format('Y-m-d');
				$end_date = DateTime::createFromFormat('d/m/Y', $this->input->post('end_date'))->format('Y-m-d');
				$end_date = date('Y-m-d', strtotime("+1 day", strtotime($end_date))); // last day missing
				$period = new DatePeriod(
					new DateTime($start_date),
					new DateInterval('P1D'),
					new DateTime($end_date)
				);
				foreach ($period as $key => $value) {
					$days .= '<div class="maid">' . $value->format('d/m/Y') . ' (' . date('l', strtotime($value->format('Y-m-d'))) . ')</div>';
				}
				/******************************************************** */
				die(json_encode(array('grid' => $schedule_grid, 'report' => $schedule_report, 'days' => $days)));
			}
			if ($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
				$customer_id = trim($this->input->post('customer_id'));

				$customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


				echo json_encode($customer_addresses);
				exit();
			}
			if ($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
				$customer_id = trim($this->input->post('customer_id'));

				$customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


				echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses)));
				exit();
			}
			// Edited byt Geethu
			if ($this->input->post('action') && $this->input->post('action') == 'get-free-maids' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
				$booking_id = trim($this->input->post('booking_id'));
				$booking = $this->bookings_model->get_booking_by_id($booking_id);
				$same_zone = $this->input->post('same_zone');

				$not_free_maids = $this->bookings_model->get_not_free_maids_by_booking_id($booking_id, $booking->service_week_day, /*$booking->service_start_date . ' ' .*/ $booking->time_from, /*$booking->service_start_date . ' ' .*/ $booking->time_to, $booking->service_start_date);
				$all_maids = $this->maids_model->get_maids();

				$nf_maids = array();
				$all_maid_list = array();
				foreach ($not_free_maids as $nmaid) {
					array_push($nf_maids, $nmaid->maid_id);
				}
				foreach ($all_maids as $maid) {
					array_push($all_maid_list, $maid->maid_id);
				}
				$free_maids = array_diff($all_maid_list, $nf_maids);


				if (!empty($free_maids)) {
					//$f_maids = array_diff($free_maids, $not_free_maids);
					$free_maid_dtls = array();
					$free_maid_ids = array();
					foreach ($free_maids as $f_maid) {
						$maid = $this->maids_model->get_maid_by_id($f_maid);
						array_push($free_maid_dtls, $maid);
						array_push($free_maid_ids, $f_maid);
					}
					if ($same_zone == 1) {
						$zone = $this->customers_model->get_customer_zone_by_address_id($booking->customer_address_id);
						$same_zone_maids = $this->bookings_model->get_same_zone_maids($zone->zone_id, $free_maid_ids, $booking->service_week_day);
						$free_maid_dtls = array();
						foreach ($same_zone_maids as $same_zone_maid) {
							$maid = $this->maids_model->get_maid_by_id($same_zone_maid->maid_id);
							array_push($free_maid_dtls, $maid);
						}
						if (empty($free_maid_dtls)) {
							$return = array();
							$return['status'] = 'error';
							$return['message'] = 'There are no same zone maids available';

							echo json_encode($return);
							exit();
						}
					}
					echo json_encode($free_maid_dtls);
					exit();
				} else {
					$return = array();
					$return['status'] = 'error';
					$return['message'] = 'There are no maids available for the selected time slot';

					echo json_encode($return);
					exit();
				}
			}
			// End 
			if ($this->input->post('action') && $this->input->post('action') == 'delete-booking-permanent' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
				$booking_idnewww = trim($this->input->post('booking_id'));
				$get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_idnewww, $service_date);
				if (count($get_dayservice_details) > 0) {
					if ($get_dayservice_details->odoo_package_activity_status == 1) {
						echo 'odoorefresh';
						exit();
					}
				}

				$schedule_date = $this->input->post('service_date');

				$booking_id = trim($this->input->post('booking_id'));

				$d_booking = $this->bookings_model->get_booking_by_id($booking_id);

				if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
					echo 'locked';
					exit();
				}
				if (isset($d_booking->booking_id)) {
					if ($d_booking->booking_type == 'OD') {
						$this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
					} else {
						$newsearchdate = date('Y-m-d', strtotime($schedule_date));
						$enddate = $d_booking->service_start_date;
						if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
							$this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
						} else {
							if ($d_booking->booking_type == 'WE') {
								$current = strtotime($newsearchdate);
								$last = strtotime($enddate);
								while ($current >= $last) {
									$current = strtotime('-7 days', $current);
									$deletedatee = date('Y-m-d', $current);
									$check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
									if (count($check_buk_delete) > 0) {
									} else {
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
										break;
									}
								}
							} else if ($d_booking->booking_type == 'BW') {
								$current = strtotime($newsearchdate);
								$last = strtotime($enddate);
								while ($current >= $last) {
									$current = strtotime('-14 days', $current);
									$deletedatee = date('Y-m-d', $current);
									$check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
									if (count($check_buk_delete) > 0) {
									} else {
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
										break;
									}
								}
							}
						}
						// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 1 day'));
						// if($d_booking->booking_type == 'WE')
						// {
						// $last_repeat_date=date('Y-m-d', strtotime($schedule_date . ' - 7 day'));
						// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 6 day'));
						// $check_buk_delete=$this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id,$last_repeat_date);
						// if(count($check_buk_delete)>0)
						// {   
						// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 8 day'));
						// }
						// }
						// else if($d_booking->booking_type == 'BW')
						// {
						// $last_repeat_date=date('Y-m-d', strtotime($schedule_date . ' - 14 day'));
						// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 13 day'));
						// $check_buk_delete=$this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id,$last_repeat_date);
						// if(count($check_buk_delete)>0)
						// {   
						// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 15 day'));
						// }

						// }
						// $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $ser_ac_end_date, 'service_end' => 1));    
					}

					//adding for odoo issue
					if (count($get_dayservice_details) > 0) {
						if ($get_dayservice_details->odoo_package_activity_status == 1) {
							echo 'odoorefresh';
							exit();
						} else {
							$updatearry = array();
							$updatearry['service_status'] = 3;
							$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
						}
					}
					//odoo issue ends

					if (($d_booking->booking_type == 'WE' && $d_booking->service_start_date == date('Y-m-d')) || ($d_booking->service_week_day == date('w') && strtotime(date('Y-m-d')) >= strtotime($d_booking->service_start_date) && strtotime(date('Y-m-d')) < strtotime($schedule_date))) {
						$c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
						if (isset($c_address->zone_id)) {
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
							$deviceid = $tablet->google_reg_id;
							$push_fields = array();
							$push_fields['tab_id'] = $tablet->tablet_id;
							$push_fields['type'] = 1;
							$push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
							$push_fields['maid_id'] = $d_booking->maid_id;
							$push_fields['title'] = "Booking Cancelled";
							$push_fields['customer_name'] = $d_booking->customer_name;
							$push_fields['maid_name'] = $d_booking->maid_name;
							$push_fields['booking_time'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
							$push = $this->bookings_model->add_push_notifications($push_fields);
							// optional payload
							$payload = array();
							$payload['isfeedback'] = false;
							if ($push > 0) {
								$payload['pushid'] = $push;
							} else {
								$payload['pushid'] = 0;
							}

							$title = "Booking Cancelled";
							$message = $push_fields['message'];
							$res = array();
							$res['data']['title'] = $title;
							$res['data']['is_background'] = false;
							$res['data']['message'] = $message;
							$res['data']['image'] = "";
							$res['data']['payload'] = $payload;
							$res['data']['customer'] = $d_booking->customer_name;
							$res['data']['maid'] = $d_booking->maid_name;
							$res['data']['bookingTime'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
							$res['data']['timestamp'] = date('Y-m-d G:i:s');
							$regId = $deviceid;
							$fields = array(
								'to' => $regId,
								'data' => $res,
							);
							$return = android_customer_push($fields);
							//android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Deleted'));
						}
					}

					echo 'success';
					exit();
				}

				echo 'refresh';
				exit();
			}

			if ($this->input->post('action') && $this->input->post('action') == 'delete-booking-one-day' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0) {
				$booking_idnewww = trim($this->input->post('booking_id'));
				$get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_idnewww, $service_date);
				if (count($get_dayservice_details) > 0) {
					if ($get_dayservice_details->odoo_package_activity_status == 1) {
						echo 'odoorefresh';
						exit();
					}
				}

				$service_date = $this->input->post('service_date');

				$booking_id = trim($this->input->post('booking_id'));

				$d_booking = $this->bookings_model->get_booking_by_id($booking_id);

				if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
					echo 'locked';
					exit();
				}
				if (isset($d_booking->booking_id)) {
					if ($d_booking->booking_type != 'OD') {
						$delete_b_fields = array();
						$delete_b_fields['booking_id'] = $booking_id;
						$delete_b_fields['service_date'] = $service_date;
						$delete_b_fields['deleted_by'] = user_authenticate();

						$this->bookings_model->add_booking_delete($delete_b_fields);

						//adding for odoo issue
						if (count($get_dayservice_details) > 0) {
							if ($get_dayservice_details->odoo_package_activity_status == 1) {
								echo 'odoorefresh';
								exit();
							} else {
								$updatearry = array();
								$updatearry['service_status'] = 3;
								$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
							}
						}
						//odoo issue ends
					}
				}

				//if($service_date == date('Y-m-d'))
				{
					$c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
					if (isset($c_address->zone_id)) {
						$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
						$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
						$deviceid = $tablet->google_reg_id;
						$push_fields = array();
						$push_fields['tab_id'] = $tablet->tablet_id;
						$push_fields['type'] = 1;
						$push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
						$push_fields['maid_id'] = $d_booking->maid_id;
						$push_fields['title'] = "Booking Cancelled";
						$push_fields['customer_name'] = $d_booking->customer_name;
						$push_fields['maid_name'] = $d_booking->maid_name;
						$push_fields['booking_time'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
						$push = $this->bookings_model->add_push_notifications($push_fields);
						// optional payload
						$payload = array();
						$payload['isfeedback'] = false;
						if ($push > 0) {
							$payload['pushid'] = $push;
						} else {
							$payload['pushid'] = 0;
						}

						$title = "Booking Cancelled";
						$message = $push_fields['message'];
						$res = array();
						$res['data']['title'] = $title;
						$res['data']['is_background'] = false;
						$res['data']['message'] = $message;
						$res['data']['image'] = "";
						$res['data']['payload'] = $payload;
						$res['data']['customer'] = $d_booking->customer_name;
						$res['data']['maid'] = $d_booking->maid_name;
						$res['data']['bookingTime'] = $d_booking->newtime_from . ' - ' . $d_booking->newtime_to;
						$res['data']['timestamp'] = date('Y-m-d G:i:s');
						$regId = $deviceid;
						$fields = array(
							'to' => $regId,
							'data' => $res,
						);
						$return = android_customer_push($fields);
						//android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => 'Booking Deleted'));
					}
				}

				echo 'success';
				exit();
			}

			/*Edited by Betsy Bernard */

			if ($this->input->post('action') && $this->input->post('action') == 'maid-leave' && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0) {
				$maid_id = trim($this->input->post('maid_id'));

				if (!empty($all_bookings)) {
					echo "exists";
					exit();
				} else {
					/*$leave = $this->maids_model->check_maid_leave_by_date($maid_id, $start_date, $end_date);
                                if($leave == 0)
                                {
                                    echo "Leave";
                                    exit();
                                }
                                else if($leave == 1)
                                {*/
					$begin = new DateTime($start_date);
					$end   = new DateTime($end_date);

					$leave_done = array();
					for ($i = $begin; $begin <= $end; $i->modify('+1 day')) {
						//echo $i->format("Y-m-d");
						$maid_fields = array();
						$maid_fields['maid_id'] = $maid_id;
						$maid_fields['leave_date'] = $i->format("Y-m-d");
						$maid_fields['leave_status'] = 1;
						$maid_fields['added_by'] = user_authenticate();

						if ($leave_maid_ids[$i->format("Y-m-d")] !== 1) {

							$leave_id = $this->maids_model->add_maid_leave($maid_fields);
							array_push($leave_done, $leave_id);
						}
					}
					if (!empty($leave_done)) {
						echo "success";
						exit();
					} else {
						echo "Leave";
						exit();
					}
					//} 
				}
			}

			echo 'refresh';
			exit();
		}

		$schedule_day = date('l d / M / Y', strtotime($schedule_date));
		$next_day = date('d-m-Y', strtotime($schedule_date . ' + 1 day'));
		$prev_day = date('d-m-Y', strtotime($schedule_date . ' - 1 day'));
		$day_number = date('w', strtotime($schedule_date));

		$customers = $this->customers_model->get_customers();

		$service_types = $this->service_types_model->get_service_types();

		$all_maids = array();
		foreach ($maids as $maid) {
			$all_maids['m-' . $maid->maid_id] = $maid->maid_name;
		}


		$data = array();
		$data['start_date'] = date("d/m/Y", strtotime($start_date));
		$data['end_date'] = date("d/m/Y", strtotime($end_date));
		$data['maid_id'] = $maid_id;
		$data['schedule_day'] = $schedule_day;
		$data['schedule_day_c'] = date('d-m-Y', strtotime($schedule_date));
		$data['repeate_end_start_c'] = date('Y, m, d', strtotime($schedule_date . ' + 7 day'));
		$data['next_day'] = $next_day;
		$data['prev_day'] = $prev_day;
		$data['day_number'] = $day_number;
		$data['times'] = $times;
		$data['maid_bookings'] = $maid_bookings;
		$data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
		$data['maids'] = $maids;
		$data['zones'] = $zones;
		$data['all_maids'] = json_encode($all_maids);
		$data['customers'] = $customers;
		$data['service_types'] = $service_types;
		$data['booking_allowed'] = strtotime($schedule_date) >= strtotime(date('d-M-Y')) ? 1 : 0;
		$data['current_hour_index'] = $current_hour_index;
		$data['maid_schedule'] = $maid_schedule;
		$data['schedule_grid'] = $this->load->view('partials/maid_schedule_grid', $data, TRUE);

		$layout_data = array();
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('maid_schedule', $data, TRUE);
		$layout_data['page_title'] = 'Schedule';
		$layout_data['meta_description'] = 'Schedule';
		$layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'maid.js');
		$layout_data['bookings_active'] = '1';
		$this->load->view('layouts/default', $layout_data);
	}

	function leave()
	{
		$leave_date = date('Y-m-d');
		$data = array();
		$data['maid_list'] = $this->maids_model->get_maid_leave($leave_date);

		$layout_data['content_body'] = $this->load->view('maid_leave_list', $data, TRUE);
		$layout_data['page_title'] = 'Maid Leave';
		$layout_data['meta_description'] = 'Maid Leave';
		$layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'maid_leave.js');
		$this->load->view('layouts/default', $layout_data);
	}
	function search_maid_leave_by_date()
	{

		$l_date = explode("/", $this->input->post('leave_date'));
		$leave_date = $l_date[2] . '-' . $l_date[1] . '-' . $l_date[0];


		$search_results = $this->maids_model->get_maid_leave($leave_date);
		if (!empty($search_results)) {
			$i = 0;
			foreach ($search_results as $maids) {

				$le_date = explode("-", $maids_val->leave_date);
				$leave_date1 = $l_date[0] . '/' . $l_date[1] . '/' . $l_date[2];

				echo '<tr>
                <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                <td style="line-height: 18px;">' . $maids->maid_name . '</td>
                <td style="line-height: 18px;">' . $maids->maid_mobile_1 . '</td>
                <td style="line-height: 18px;">' . $leave_date1 . '</td>
                <td style="line-height: 18px;">' . $maids->user_fullname . '</td>
                <td style="line-height: 18px; width: 110px"  class="td-actions"><center>
                   ';

				echo '<a href="javascript:void" style="text-decoration:none;" onclick="undo_leave(' . $maids->leave_id . ')"><span class="not-started">Cancel</span></a>';

				//                if ($maids->leave_status == 1) {
				//                    echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="undo_leave(' . $maids->leave_id . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
				//                } else {
				//                    echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="undo_leave(' . $maids->leave_id . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
				//                }
				echo ' </center></td>
              </tr>';
			}
		} else {
			echo '<tr><td colspan="6" style="line-height: 18px; text-align:center;">No Records!</td></tr>';
		}
	}
	function cancel_maid_leave_by_id()
	{

		$leave_id = $this->input->post('leave_id');

		$leave_fields = array();
		$leave_fields['leave_id'] = $leave_id;
		$leave_fields['leave_status'] = 0;
		$leave_fields['updated_by'] = user_authenticate();

		$this->maids_model->update_maid_leave($leave_fields);


		$l_date = explode("/", $this->input->post('leave_date'));
		$leave_date = $l_date[2] . '-' . $l_date[1] . '-' . $l_date[0];

		$search_results = $this->maids_model->get_maid_leave($leave_date);
		if (!empty($search_results)) {
			$i = 0;
			foreach ($search_results as $maids) {

				$le_date = explode("-", $maids_val->leave_date);
				$leave_date1 = $l_date[0] . '/' . $l_date[1] . '/' . $l_date[2];

				echo '<tr>
                <td style="line-height: 18px; width: 20px"><center>' . ++$i . '</center></td>
                <td style="line-height: 18px;">' . $maids->maid_name . '</td>
                <td style="line-height: 18px;">' . $maids->maid_mobile_1 . '</td>
                <td style="line-height: 18px;">' . $leave_date1 . '</td>
                <td style="line-height: 18px;">' . $maids->user_fullname . '</td>
                <td style="line-height: 18px; width: 110px"  class="td-actions"><center>
                   ';
				echo '<a href="javascript:void" style="text-decoration:none;" onclick="undo_leave(' . $maids->leave_id . ')"><span class="not-started">Cancel</span></a>';


				//                if ($maids->leave_status == 1) {
				//                    echo '<a class="btn btn-small btn-success" href="javascript::void();" onclick="undo_leave(' . $maids->leave_id . ')" ><i class="btn-icon-only icon-ok"> </i></a>';
				//                } else {
				//                    echo '<a class="btn btn-small btn-danger" href="javascript::void();" onclick="undo_leave(' . $maids->leave_id . ')" ><i class="btn-icon-only icon-remove"> </i></a>';
				//                }
				echo ' </center></td>
              </tr>';
			}
		} else {
			echo '<tr><td colspan="6" style="line-height: 18px; text-align:center;">No Records!</td></tr>';
		}
	}

	public function do_maids_odoo_api($data = array())
	{
		$get_flat_odoo_id = $this->settings_model->get_flat_details($data['flat_id']);
		if ($data['maid_mobile_2'] == "") {
			$work_phone = $data['maid_mobile_1'];
		} else {
			$work_phone = $data['maid_mobile_2'];
		}
		$post['params']['user_id'] = 1;
		$post['params']['name'] = $data['maid_name'];
		$post['params']['complete_name'] = $data['maid_name'];
		$post['params']['mobile_phone'] = $data['maid_mobile_1'];
		$post['params']['work_phone'] = $work_phone;
		$post['params']['flat_id'] = $get_flat_odoo_id[0]['odoo_package_flat_id'];
		$post_values = json_encode($post);

		$url = $this->config->item('odoo_url') . "employee_creation";
		$login_check = curl_api_service($post_values, $url);
		$returnData = json_decode($login_check);

		if ($returnData->result->status == "success") {
			return $odoo_maid_id = $returnData->result->response->id;
		}
	}

	public function update_maid_inodoo($data, $maid_id)
	{
		$get_flat_odoo_id = $this->settings_model->get_flat_details($data['flat_id']);
		if ($data['maid_mobile_2'] == "") {
			$work_phone = $data['maid_mobile_1'];
		} else {
			$work_phone = $data['maid_mobile_2'];
		}
		$post['params']['user_id'] = 1;
		$post['params']['id'] = $maid_id;
		$post['params']['name'] = $data['maid_name'];
		$post['params']['complete_name'] = $data['maid_name'];
		$post['params']['mobile_phone'] = $data['maid_mobile_1'];
		$post['params']['work_phone'] = $work_phone;
		$post['params']['flat_id'] = $get_flat_odoo_id[0]['odoo_package_flat_id'];
		$post_values = json_encode($post);
		$url = $this->config->item('odoo_url') . "employee_edition";
		$login_check = curl_api_service($post_values, $url);
		$returnData = json_decode($login_check);

		if ($returnData->result->status == "success") {
			return $odoo_maid_id = $maid_id;
		}
	}

	public function export_maid_to_odoo_api()
	{
		//$maids = $this->maids_model->get_maids_for_odoo();
		$maids = $this->maids_model->get_maids_for_odoo_package();
		// echo '<pre>';
		// print_r($maids);
		// exit();
		$i = 0;
		foreach ($maids as $maid) {
			$post['params']['user_id'] = 1;
			$post['params']['name'] = $maid->maid_name;
			$post['params']['complete_name'] = $maid->maid_name;
			$post['params']['mobile_phone'] = $maid->maid_mobile_1;
			$post['params']['work_phone'] = $maid->maid_mobile_1;
			$post['params']['flat_id'] = $maid->odoo_package_flat_id;
			$post_values = json_encode($post);
			$url = $this->config->item('odoo_url') . "employee_creation";
			$login_check = curl_api_service($post_values, $url);
			$returnData = json_decode($login_check);

			if ($returnData->result->status == "success") {
				$odoo_maid_id = $returnData->result->response->id;
				if ($odoo_maid_id > 0) {
					// $response = $this->maids_model->update_maids(array('odoo_new_maid_id' => $odoo_maid_id,'odoo_new_maid_status' => 1),$maid->maid_id);
					$response = $this->maids_model->update_maids(array('odoo_package_maid_id' => $odoo_maid_id, 'odoo_package_maid_status' => 1), $maid->maid_id);
				}
				echo ++$i, '.', $odoo_maid_id, '-', $maid->maid_name . '<br>';
			}
		}
	}

	public function import_total_maids()
	{
		$this->load->library('Excel');
		$file = "./upload/maidlist.xlsx";
		$obj = PHPExcel_IOFactory::load($file);
		$cell = $obj->getActiveSheet()->getCellCollection();
		foreach ($cell as $cl) {
			$column = $obj->getActiveSheet()->getCell($cl)->getColumn();
			$row = $obj->getActiveSheet()->getCell($cl)->getRow();
			$data_value = $obj->getActiveSheet()->getCell($cl)->getFormattedValue();

			if ($row == 1) {
				$header[$row][$column] = $data_value;
			} else {
				$arr_data[$row][$column] = $data_value;
			}
		}
		$data['header'] = $header;
		$data['values'] = $arr_data;

		// echo '<pre>';
		// print_r($arr_data);
		// exit();

		foreach ($arr_data as $val) {
			$name = $val['B'];
			if ($val['C'] == "Female") {
				$gender = "F";
			} else if ($val['C'] == "Male") {
				$gender = "M";
			}
			$present_addr = $val['D'];
			$permanent_addr = $val['E'];
			$mobile_val = $val['F'];
			$mobile1 = str_replace(["-", "–"], '', $mobile_val);
			$mobile_val2 = $val['G'];
			$mobile2 = str_replace(["-", "–"], '', $mobile_val2);
			$joining = $val['H'];
			$joiningdate = date('Y-m-d', strtotime($joining));

			$data = array(
				'maid_name' => $name,
				'maid_gender' => $gender,
				'maid_nationality' => "",
				'maid_present_address' => $present_addr,
				'maid_permanent_address' => $permanent_addr,
				'maid_mobile_1' => $mobile1,
				'maid_mobile_2' => $mobile2,
				'flat_id' => "",
				'team_id' => "",
				'maid_photo_file' => "",
				'maid_passport_number' => "",
				'maid_passport_expiry_date' => "",
				'maid_visa_number' => "",
				'maid_visa_expiry_date' => "",
				'maid_labour_card_number' => "",
				'maid_labour_card_expiry_date' => "",
				'maid_emirates_id' => "",
				'maid_emirates_expiry_date' => "",
				'maid_notes' => "",
				'username' => "",
				'password' => "",
				'maid_status' => 1,
				'maid_added_datetime' => date('Y-m-d H:i:s'),
				'maid_joining' => $joiningdate,
			);
			$result = $this->maids_model->add_maids($data);
		}
		exit();
	}
	public function get_available_maids_for_move()
	{
		// in development mode
		header('Content-type: application/json; charset=UTF-8');
		try {
			//$time_from = '06:00:00';
			//$time_to = '07:30:00';
			//$_POST['booking_id'] = 3506;
			//$_POST['service_date'] = '26-05-2023';
			$booking = $this->bookings_model->get_booking_by_id($this->input->post('booking_id'));
			//print_r($booking);die();
			$timed_free_maids = array();
			if ($booking->booking_type == "OD") {
				// ONE DAY
				$service_date = DateTime::createFromFormat('d-m-Y', $this->input->post('service_date'))->format('Y-m-d'); // input format is d-m-Y
				$time_from = $booking->time_from;
				$time_to = $booking->time_to;
				$timed_bookings = $this->maids_model->get_ongoing_bookings_on_time($service_date, $time_from, $time_to, true); // check on one date
				$timed_busy_maids = array_column($timed_bookings, 'maid_id');
				$timed_free_maids = $this->maids_model->get_maids_except_maids($timed_busy_maids); // maids except busy maids
			} else if ($booking->booking_type == "WE") {
				// WEEKLY
				$service_start_date = $booking->service_start_date;
				$service_end_date = $booking->service_end_date;
				$service_end = $booking->service_end;
				$service_week_day = $booking->service_week_day;
				$timed_bookings = $this->maids_model->get_same_timed_bookings($booking); // get bookings by same creteria of the booking
				//print_r($timed_bookings);die();
				$timed_busy_maids = array_column($timed_bookings, 'maid_id');
				$timed_free_maids = $this->maids_model->get_maids_except_maids($timed_busy_maids); // maids except busy maids
			} else if ($booking->booking_type == "BW") {
				// BI-WEEKLY
				$service_start_date = $booking->service_start_date;
				$service_end_date = $booking->service_end_date;
				$service_end = $booking->service_end;
				$service_week_day = $booking->service_week_day;
				$timed_bookings = $this->maids_model->get_same_timed_bookings($booking); // get bookings by same creteria of the selected booking
				// trick to exclude some bw bookings
				foreach ($timed_bookings as $key => $value) {
					if ($value->service_start_date_week_difference & 1) { // if odd number
						// week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
						unset($timed_bookings[$key]);
					}
				}
				//
				//print_r($timed_bookings);die();
				$timed_busy_maids = array_column($timed_bookings, 'maid_id');
				$timed_free_maids = $this->maids_model->get_maids_except_maids($timed_busy_maids); // maids except busy maids
			} else {
				die(json_encode(array('status' => false, 'message' => 'Unknown booking type !'), JSON_PRETTY_PRINT));
			}
			//echo('<pre>');print_r($timed_free_maids);echo('<pre>');die();
		} catch (Exception $e) {
			die(json_encode(array('status' => false, 'message' => $e->getMessage()), JSON_PRETTY_PRINT));
		}
		die(json_encode(array('status' => true, 'data' => $timed_free_maids, 'message' => 'Free maids retrieved successfully !'), JSON_PRETTY_PRINT));
	}
	public function move_booking()
	{
		// in development mode
		header('Content-type: application/json; charset=UTF-8');
		$this->db->trans_begin();
		try {
		} catch (Exception $e) {
			$this->db->trans_rollback();
			die(json_encode(array('status' => false, 'message' => $e->getMessage()), JSON_PRETTY_PRINT));
		}
		$this->db->trans_commit();
		die(json_encode(array('status' => true, 'data' => [], 'message' => 'Booking moved successfully !'), JSON_PRETTY_PRINT));
	}
}
function upload_base64($base64)
{
	// upload avatar
	if ($base64 != null) {
		list($type, $base64) = explode(';', $base64);
		list(, $base64) = explode(',', $base64);
		$base64 = base64_decode($base64);
		$file_name = date('d-m-Y_') . time() . '_' . md5($base64) . ".jpg";
		if (file_put_contents(DIR_UPLOAD_MAID_AVATAR . $file_name, $base64)) {
			return $file_name;
		}
		return null;
	}
	return null;
}
