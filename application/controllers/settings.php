<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('bookings_model');
        $this->load->helper('google_api_helper');
        $this->load->helper('curl_helper');
    }
    public function index()
    {
        if (!user_permission(user_authenticate(), 7)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        //$data = array();
        if ($this->input->post('zone_sub')) {
            $zone_name = $this->input->post('zonename');
            $drivername = $this->input->post('drivername');
            $spare = $this->input->post('spare') ?: 'N';

            $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
            );

            $zone_odoo_fields = array();
            $zone_odoo_fields['zone_name'] = $this->input->post('zonename');
            $zone_odoo_fields['driver_name'] = $this->input->post('drivername');
            $zone_odoo_fields['spare_zone'] = $spare;

            $zone_id = $this->settings_model->add_zones($data);
            //$odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields);
            //$this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);
        } else if ($this->input->post('zone_edit')) {
            $zone_id = $this->input->post('edit_zoneid');
            $zone_name = $this->input->post('edit_zonename');
            $drivername = $this->input->post('edit_drivername');
            $spare = $this->input->post('edit_spare');
            $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
            );
            $this->settings_model->update_zones($data, $zone_id);

            $get_zone_by_id = $this->settings_model->get_zone_details($zone_id);

            $zone_odoo_fields = array();
            $zone_odoo_fields['zone_name'] = $zone_name;
            $zone_odoo_fields['driver_name'] = $drivername;
            $zone_odoo_fields['spare_zone'] = $spare;
            //$odo_zone_affected = $this->update_zone_inodoo($zone_odoo_fields, $get_zone_by_id[0]['odoo_package_zone_id']);
        } else if ($this->input->post('settings_update')) {
            $data['normal_hours_fee'] = $this->input->post('normal_hours_fee');
            $data['extra_hours_fee'] = $this->input->post('extra_hours_fee');
            $data['weekend_hours_fee'] = $this->input->post('weekend_hours_fee');
            $data['service_vat_percentage'] = $this->input->post('service_vat_percentage');
            $data['google_map_api_key'] = $this->input->post('google_map_api_key');
            $data['site_name'] = $this->input->post('site_name');
            $data['company_name'] = $this->input->post('company_name');
            $data['company_address_lines_html'] = $this->input->post('company_address_lines_html');
            $data['company_trn'] = $this->input->post('company_trn');
            $data['company_tel_number'] = $this->input->post('company_tel_number');
            $data['company_email'] = $this->input->post('company_email');
            $data['company_website_url'] = $this->input->post('company_website_url');
            $data['company_website_label'] = $this->input->post('company_website_label');
            $data['root_directory'] = $this->input->post('root_directory');
            $data['color_bg_booking_od'] = $this->input->post('color_bg_booking_od');
            $data['color_bg_booking_we'] = $this->input->post('color_bg_booking_we');
            $data['color_bg_booking_bw'] = $this->input->post('color_bg_booking_bw');
            $data['color_bg_text_booking_od'] = $this->input->post('color_bg_text_booking_od');
            $data['color_bg_text_booking_we'] = $this->input->post('color_bg_text_booking_we');
            $data['color_bg_text_booking_bw'] = $this->input->post('color_bg_text_booking_bw');
            $data['site_footer_copyright_line_html'] = $this->input->post('site_footer_copyright_line_html');
            $data['booking_preparation_time'] = $this->input->post('booking_preparation_time');
            $data['od_booking_text'] = $this->input->post('od_booking_text');
            $data['we_booking_text'] = $this->input->post('we_booking_text');
            $data['bw_booking_text'] = $this->input->post('bw_booking_text');
            $data['maid_listing_priority_type'] = $this->input->post('maid_listing_priority_type') ?: 'lifo';
            $data['smtp_host'] = $this->input->post('smtp_host') ?: NULL;
            $data['smtp_port'] = $this->input->post('smtp_port') ?: NULL;
            $data['smtp_username'] = $this->input->post('smtp_username') ?: NULL;
            $data['smtp_password'] = $this->input->post('smtp_password') ?: NULL;
            $data['mail_protocol'] = $this->input->post('mail_protocol') ?: NULL;
            if ($this->input->post('site_favicon_base64')) {
                $data['site_favicon'] = upload_base64_image($this->input->post('site_favicon_base64'), 'favicon', SETTINGS_IMG_PATH);
            }
            if ($this->input->post('website_logo_base64')) {
                $data['website_logo'] = upload_base64_image($this->input->post('website_logo_base64'), 'website-logo', SETTINGS_IMG_PATH);
            }
            if ($this->input->post('invoice_logo_base64')) {
                $data['invoice_logo'] = upload_base64_image($this->input->post('invoice_logo_base64'), 'invoice-logo', SETTINGS_IMG_PATH);
            }
            if ($this->input->post('login_page_logo_base64')) {
                $data['login_page_logo'] = upload_base64_image($this->input->post('login_page_logo_base64'), 'login-page-logo', SETTINGS_IMG_PATH);
            }
            $this->settings_model->update_settings($data);
            redirect('/settings');
        } else {
            $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
            $layout_data['content_body'] = $this->load->view('settings_view', $data, TRUE);
            $layout_data['page_title'] = 'Settings';
            $layout_data['meta_description'] = 'Settings';
            $layout_data['css_files'] = array();
            $layout_data['external_js_files'] = array();
            $layout_data['js_files'] = array();
            $this->load->view('layouts/default', $layout_data);
        }
    }
    public function zones()
    {
        if ($this->input->post('zone_sub')) {
            $zone_name = $this->input->post('zonename');
            $drivername = $this->input->post('drivername');
            $spare = $this->input->post('spare') ?: 'N';
            $description = $this->input->post('description');

            $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
                'odoo_zone_id' => 0,
                'odoo_new_zone_id' => 0,
                'description' => $description,
            );

            $zone_odoo_fields = array();
            $zone_odoo_fields['zone_name'] = $this->input->post('zonename');
            $zone_odoo_fields['driver_name'] = $this->input->post('drivername');
            // $zone_odoo_fields['spare_zone'] = $spare;
            $zone_id = $this->settings_model->add_zones($data);
            $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields);
            // $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);
        } else if ($this->input->post('zone_edit')) {
            $zone_id = $this->input->post('edit_zoneid');
            $zone_name = $this->input->post('edit_zonename');
            $drivername = $this->input->post('edit_drivername');
            $spare = $this->input->post('edit_spare');
            $description = $this->input->post('edit_description');
            $data = array(
                'zone_name' => $zone_name,
                'driver_name' => $drivername,
                'spare_zone' => $spare,
                'zone_status' => 1,
                'odoo_zone_id' => 0,
                'odoo_new_zone_id' => 0,
                'description' => $description,
            );
            $this->settings_model->update_zones($data, $zone_id);

            $get_zone_by_id = $this->settings_model->get_zone_details($zone_id);

            $zone_odoo_fields = array();
            $zone_odoo_fields['zone_name'] = $zone_name;
            $zone_odoo_fields['driver_name'] = $drivername;
            $zone_odoo_fields['spare_zone'] = $spare;
            //$odo_zone_affected = $this->update_zone_inodoo($zone_odoo_fields, $get_zone_by_id[0]['odoo_package_zone_id']);
        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['zones'] = $this->settings_model->get_zones();
        // print_r($data['zones']);exit();
        $layout_data['content_body'] = $this->load->view('zone_list', $data, TRUE);
        $layout_data['page_title'] = 'Zones List';
        $layout_data['meta_description'] = 'zones';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function edit_team()
    {
        $team_id = $this->input->post('team_id');
        $result = $this->settings_model->get_team_details($team_id);
        echo json_encode($result);
    }
    public function edit_zone()
    {
        $zone_id = $this->input->post('zone_id');
        $result = $this->settings_model->get_zone_details($zone_id);
        echo json_encode($result);
    }
    public function view_zone()
    {
        $zone_id = $this->input->post('zone_id');
        $result = $this->settings_model->get_zone_details_view($zone_id);
        echo json_encode($result);
    }
    public function remove_zone()
    {
        $zone_id = $this->input->post('zone_id');
        $data = $this->settings_model->delete_zone_new(array('zone_status' => 0), $zone_id);
        //$data = $this->settings_model->delete_zone($zone_id);
        echo $data;
    }
    public function team()
    {
        //$data = array();
        if ($this->input->post('team_sub')) {
            $team_name = $this->input->post('teamname');

            $data = array(
                'team_name' => $team_name,
                'team_status' => 1,
            );
            $this->settings_model->add_teams($data);
        }
        if ($this->input->post('team_edit')) {
            $team_id = $this->input->post('edit_teamid');
            $team_name = $this->input->post('edit_teamname');
            //$drivername = $this->input->post('edit_drivername');
            //$spare = $this->input->post('edit_spare');
            $data = array(
                'team_name' => $team_name,
                //'driver_name' => $drivername,
                //'spare_zone' => $spare,
                'team_status' => 1,
            );
            $this->settings_model->update_teams($data, $team_id);
        }
        $data['teams'] = $this->settings_model->get_teams();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('team_list', $data, TRUE);
        $layout_data['page_title'] = 'Teams';
        $layout_data['meta_description'] = 'Teams';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function area()
    {
        if (!user_permission(user_authenticate(), 8)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('area_sub')) {
            $areaname = $this->input->post('areaname');
            $zone_id = $this->input->post('zone_id');
            $description = $this->input->post('description');
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $areaname,
                'area_status' => 1,
                'odoo_area_id' => 0,
                'odoo_new_area_id' => 0,
                'description' => $description,
            );
            $area_id = $this->settings_model->add_area($data);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            if ($getodoozone_id[0]['odoo_package_zone_id'] == 0) {
                $zone_odoo_fields = array();
                $zone_odoo_fields['zone_name'] = $getodoozone_id[0]['zone_name'];
                $zone_odoo_fields['driver_name'] = $getodoozone_id[0]['driver_name'];
                $zone_odoo_fields['description'] = $getodoozone_id[0]['description'];
                $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
                $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

            } 
            $get_odoo_zone_id = $this->settings_model->get_zone_details($zone_id);
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $get_odoo_zone_id[0]['odoo_package_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
            // print_r($zone_area_fields);die();
            $odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area_id);
            //$odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            //$this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id,'odoo_package_area_status' => 1),$area_id);
        }
        if ($this->input->post('area_edit')) {
            $areaname = $this->input->post('edit_areaname');
            $area_id = $this->input->post('edit_areaid');
            $zone_id = $this->input->post('edit_zone_id');
            $description = $this->input->post('edit_description');
            $min_booking_hour = $this->input->post('min_booking_hour') ?: null;
            $data = array(
                'zone_id' => $zone_id,
                'area_name' => $areaname,
                'area_status' => 1,
                'odoo_area_id' => 0,
                'odoo_new_area_id' => 0,
                'description' => $description,
                'min_booking_hour' => $min_booking_hour,
            );
            $this->settings_model->update_area($data, $area_id);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            $get_area_by_id = $this->settings_model->get_area_details($area_id);
            if ($getodoozone_id[0]['odoo_package_zone_id'] == 0) {
                $zone_odoo_fields = array();
                $zone_odoo_fields['zone_name'] = $getodoozone_id[0]['zone_name'];
                $zone_odoo_fields['driver_name'] = $getodoozone_id[0]['driver_name'];
                $zone_odoo_fields['description'] = $getodoozone_id[0]['description'];
                $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
                $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

            } 
            $get_odoo_zone_id = $this->settings_model->get_zone_details($zone_id);
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $get_odoo_zone_id[0]['odoo_package_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
            $zone_area_fields['description'] = $description;
            if ($get_area_by_id[0]['odoo_package_area_id'] == 0) {
                
                $odoo_area_id = $this->do_area_odoo_api($zone_area_fields); 
                $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area_id);
            } 
            $getarea_by_id = $this->settings_model->get_area_details($area_id);
            $odo_area_affected = $this->update_area_inodoo($zone_area_fields, $getarea_by_id[0]['odoo_package_area_id']);
            //$odo_area_affected = $this->update_area_inodoo($zone_area_fields, $get_area_by_id[0]['odoo_package_area_id']);
        }
        $data['areas'] = $this->settings_model->get_areas();
        $data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('area_list', $data, TRUE);
        $layout_data['page_title'] = 'Area List';
        $layout_data['meta_description'] = 'area';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function edit_area()
    {
        $area_id = $this->input->post('area_id');
        $result = $this->settings_model->get_area_details($area_id);
        echo json_encode($result);
    }
    public function get_zone_area()
    {
        $areaId = $this->input->post('edit_areaid');
        $zoneId = $this->input->post('edit_zone_id');
        $getZoneArea = $this->settings_model->getZoneArea($areaId, $zoneId);
        if (sizeof($getZoneArea))
        {
            $response['status'] = "success";
            $response['message'] = "Selected location /area got changed ,please map again.";
            echo json_encode($response);
        } else{
            $response['status'] = "fail";
            echo json_encode($response);
        }
        // echo json_encode($getZoneArea);
    }
    public function remove_area()
    {
        $area_id = $this->input->post('area_id');
        $get_area_by_id = $this->settings_model->get_area_details($area_id);
        $odoo_area_id = $get_area_by_id[0]['odoo_package_area_id'];
        // $odoo_customer_delete = $this->delete_odoo_area($odoo_area_id);
        $data = $this->settings_model->update_area(array('area_status' => 0), $area_id);
        //$data = $this->settings_model->delete_area($area_id);
    }
    public function delete_odoo_area($odoo_area_id)
    {
     
        $post['params']['user_id'] = 1;
        $post['params']['id'] = $odoo_area_id;
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') ."area_deletion";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);
        if ($returnData->result->status == "success") {
            return $odoo_area_id = 0;
        }
    }
    public function flats()
    {
        if (!user_permission(user_authenticate(), 9)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('flat_sub')) {
            $flatname = $this->input->post('flatname');
            $tablet_imei = $this->input->post('tablet_imei');
            $data = array(
                'flat_name' => $flatname,
                'tablet_imei' => $tablet_imei,
                'flat_status' => 1,
            );
            $flat_id = $this->settings_model->add_flats($data);
            $flat_odoo_fields = array();
            $flat_odoo_fields['name'] = $this->input->post('flatname');
            $flat_odoo_fields['code'] = $this->input->post('tablet_imei');

            //$odoo_flat_id = $this->do_flat_odoo_api($flat_odoo_fields);
            //$this->settings_model->update_flats(array('odoo_package_flat_id' => $odoo_flat_id,'odoo_package_flat_status' => 1),$flat_id);
            redirect('flats', 'refresh');
        }
        if ($this->input->post('flat_edit')) {
            $flat_id = $this->input->post('edit_flatid');
            $flat_name = $this->input->post('edit_flatname');
            $tablet_imei = $this->input->post('edit_tablet_imei');
            $data = array(
                'flat_name' => $flat_name,
                'tablet_imei' => $tablet_imei,
                'flat_status' => 1,
            );
            $this->settings_model->update_flats($data, $flat_id);
            $get_flat_by_id = $this->settings_model->get_flat_details($flat_id);
            $flat_odoo_fields = array();
            $flat_odoo_fields['name'] = $flat_name;
            $flat_odoo_fields['code'] = $tablet_imei;
            //$odo_flat_affected = $this->update_flat_inodoo($flat_odoo_fields, $get_flat_by_id[0]['odoo_package_flat_id']); 
            redirect('flats', 'refresh');
        }
        $data['flats'] = $this->settings_model->get_flats();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('flat_list', $data, TRUE);
        $layout_data['page_title'] = 'Flats List';
        $layout_data['meta_description'] = 'flats';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function edit_flat()
    {
        $flat_id = $this->input->post('flat_id');
        $result = $this->settings_model->get_flat_details($flat_id);
        echo json_encode($result);
    }
    public function remove_flat()
    {
        $flat_id = $this->input->post('flat_id');
        $data = $this->settings_model->delete_flat($flat_id);
    }
    public function services()
    {
        if (!user_permission(user_authenticate(), 11)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('services_sub')) {
            $service_type_name = $this->input->post('service_type_name');
            $service_rate = $this->input->post('service_rate');
            $data = array(
                'service_type_name' => $service_type_name,
                'service_rate' => $service_rate,
                'service_type_status' => 1,
            );
            $this->settings_model->add_services($data);
        }
        if ($this->input->post('services_edit')) {
            $service_id = $this->input->post('edit_service_type_id');
            $service_type_name = $this->input->post('edit_service_type_name');
            $service_rate = $this->input->post('edit_service_rate');
            $data = array(
                'service_type_name' => $service_type_name,
                'service_rate' => $service_rate,
                'service_type_status' => 1,
            );
            $this->settings_model->update_services($data, $service_id);
        }
        $data['services'] = $this->settings_model->get_services();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('services_list', $data, TRUE);
        $layout_data['page_title'] = 'services';
        $layout_data['meta_description'] = 'services';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function edit_services()
    {
        $service_id = $this->input->post('service_id');
        $result = $this->settings_model->get_service_details($service_id);
        echo json_encode($result);
    }
    function remove_services()
    {
        $service_id = $this->input->post('service_id');
        $data = $this->settings_model->delete_services($service_id);
    }
    public function tablets()
    {
        if (!user_permission(user_authenticate(), 10)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('tablet_sub')) {
            $zone_id = $this->input->post('zone_id');
            $access_code = $this->input->post('access_code');
            $imei = $this->input->post('imei');
            $drivername = $this->input->post('drivernameval');
            $tablet_username = $this->input->post('username');
            $tablet_password = $this->input->post('password');
            $data = array(
                'zone_id' => $zone_id,
                'flat_id' => null,
                'access_code' => $access_code,
                'imei' => $imei,
                'tablet_driver_name' => $drivername,
                'tablet_username' => $tablet_username,
                'tablet_password' => $tablet_password,
                'tablet_status' => 1,
            );
            $this->settings_model->add_tablet($data);
        }
        if ($this->input->post('tablet_edit')) {
            $access_code = $this->input->post('edit_access_code');
            $tablet_id = $this->input->post('edit_tabletid');
            $imei = $this->input->post('edit_imei');
            $zone_id = $this->input->post('edit_zone_id');
            $drivername = $this->input->post('editdrivernameval');
            $tablet_username = $this->input->post('edit_username');
            $tablet_password = $this->input->post('edit_password');
            $data = array(
                'zone_id' => $zone_id,
                'tablet_driver_name' => $drivername,
                'imei' => $imei,
                'tablet_username' => $tablet_username,
                'tablet_password' => $tablet_password,
            );
            $this->settings_model->update_tablet($data, $tablet_id);
        }
        if ($this->uri->segment(2) != "") {
            $data['tablets'] = $this->settings_model->get_tablets_by_status($this->uri->segment(2));
            $data['tabstatus'] = $this->uri->segment(2);
        } else {
            $data['tablets'] = $this->settings_model->get_tablets();
            $data['tabstatus'] = "3";
        }

        $data['zones'] = $this->settings_model->get_zones();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('tablets_list', $data, TRUE);
        $layout_data['page_title'] = 'tablets';
        $layout_data['meta_description'] = 'tablets';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function edit_tablet()
    {
        $tablet_id = $this->input->post('tablet_id');
        $result = $this->settings_model->get_tablet_details($tablet_id);
        echo json_encode($result);
    }
    function tablet_status()
    {
        $tablet_id = $this->input->post('tablet_id');
        $status = $this->input->post('status');
        if ($status == 1) {
            $data = array(
                'tablet_status' => 0
            );
            $this->settings_model->disable_status($tablet_id, $data);
        } else {
            $data = array(
                'tablet_status' => 1
            );
            $this->settings_model->activate_status($tablet_id, $data);
        }
    }
    /*
    * Modules
    */
    public function modules()
    {
        $e_module_id = 0;
        $e_module = array();

        if ($this->uri->segment(3) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0) {
            $e_module_id = $this->uri->segment(3);
            $e_module = $this->modules_model->get_module_by_id($e_module_id);
        }

        if ($this->input->post('add_module')) {
            $errors = array();

            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

            $this->form_validation->set_rules('module_name', 'Module Name', 'required');
            $this->form_validation->set_message('required', 'Please enter module name', 'module_name');

            if ($this->form_validation->run()) {
                $module_fields = array();

                $module_fields['module_name'] = strtolower(trim($this->input->post('module_name')));
                $module_fields['parent_id'] = trim($this->input->post('parent_id'));

                $chk_module = $this->users_model->get_module_by_module_name($module_fields['module_name'], $module_fields['parent_id']);

                if ($e_module_id > 0) {
                    if (isset($chk_module->module_id) && $chk_module->module_id != $e_module->module_id) {
                        $errors['module_name_error'] = 'This module already exists';
                    } else {
                        $update_module = $this->users_model->update_module($e_module->module_id, $module_fields);

                        if (is_numeric($update_module) && $update_module > 0) {
                            $this->session->set_flashdata('success_msg', 'Module details has been updated successfully.');
                            redirect(base_url() . 'settings/modules');
                            exit();
                        }
                    }
                } else {
                    if (isset($chk_module->module_id)) {
                        $errors['module_name_error'] = 'This module already exists';
                    } else {
                        $module_id = $this->users_model->add_module($module_fields);

                        if (is_numeric($module_id) && $module_id > 0) {
                            $this->session->set_flashdata('success_msg', 'Module has been added successfully.');
                            redirect(base_url() . 'settings/modules');
                            exit();
                        }
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
            }
        }

        $modules = $this->users_model->get_all_modules(FALSE);

        $parent_modules = $this->users_model->get_parent_modules(FALSE);

        $module_array = array();
        $module_array['0'] = 'Select Module';
        foreach ($parent_modules as $module) {
            $module_array[$module->module_id] = ucwords($module->module_name);
        }

        $data = array();
        $data['error'] = isset($errors) ? array_shift($errors) : '';
        $data['modules'] = $modules;
        $data['sel_modules'] = $module_array;
        $data['e_module'] = $e_module;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('modules', $data, TRUE);
        $layout_data['page_title'] = 'Modules';
        $layout_data['meta_description'] = 'Modules';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function delete_module($id)
    {
        if ($this->users_model->delete_module($id)) {

            $this->session->set_flashdata('success_msg', 'Module has been deleted successfully.');
        } else {
            $this->session->set_flashdata('failure_msg', 'Can\'t delete the module.');
        }
        redirect('/settings/modules/', 'refresh');
    }
    function backpayment()
    {

        $this->load->model('customers_model');

        if ($this->input->post('payment_date') && strlen($this->input->post('payment_date')) > 0) {
            $payment_date = trim($this->input->post('payment_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));


        $payments = $this->customers_model->get_back_payment($date);
        $back_payment = array();
        foreach ($payments as $payment) {
            $customer_balance = $this->customers_model->get_customer_balance($payment->customer_id);
            $payment->balance = ($customer_balance->amount ? $customer_balance->amount : 0) - $payment->collected_amount;
            array_push($back_payment, $payment);
        }

        $data = array();
        $data['back_payment'] = array();
        $data['payment_date'] = $payment_date;
        $data['back_payment'] = $back_payment;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('back_payment', $data, TRUE);
        $layout_data['page_title'] = 'Back Payment';
        $layout_data['meta_description'] = 'Back Payment';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js', 'mymaids.js');
        $layout_data['reports_active'] = '1';
        $this->load->view('layouts/default', $layout_data);
    }
    function add_backpayment()
    {

        $this->load->model('customers_model');
        $this->load->model('tablets_model');

        if ($this->input->is_ajax_request()) {
            if ($this->input->post('action') && $this->input->post('action') == 'get-balance-amount') {
                if ($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0) {
                    $customer_id = $this->input->post('customer_id');

                    $customer_balance = $this->customers_model->get_customer_balance($customer_id);

                    $response = array();
                    $response['status'] = 'success';
                    $response['balance'] = $customer_balance->amount ? $customer_balance->amount : 0;

                    echo json_encode($response);
                    exit();
                } else {
                    $response = array();
                    $response['status'] = 'error';
                    $response['message'] = 'Please select customer';

                    echo json_encode($response);
                    exit();
                }
            }
        }
        $errors = array();
        if ($this->input->post('add_back_payment')) {
            $customer_id = trim($this->input->post('customer_id'));
            $ids = explode("-", trim($this->input->post('tablet_id')));
            $tablet_id = $ids[0];
            $zone_id = $ids[1];
            //edited by vishnu
            //$collected_amount = trim($this->input->post('collected_amount'));
            //$requested_amount = trim($this->input->post('requested_amount'));
            $requested_amount = trim($this->input->post('collected_amount'));
            //ends
            $collected_date = date("Y-m-d", strtotime(str_replace("/", "-", trim($this->input->post('collected_date')))));

            $back_payment_fields = array();
            $back_payment_fields['customer_id'] = $customer_id;
            $back_payment_fields['tablet_id'] = $tablet_id;
            $back_payment_fields['zone_id'] = $zone_id;
            //edited by vishnu
            //$back_payment_fields['collected_amount'] = $collected_amount;
            //$back_payment_fields['requested_amount'] = $requested_amount;
            $back_payment_fields['requested_amount'] = $requested_amount;
            //ends
            $back_payment_fields['collected_date'] = $collected_date;
            $back_payment_fields['collected_time'] = date('H:i:s');

            $back_payment_id = $this->customers_model->add_backpayment($back_payment_fields);
            $customer_name = $this->customers_model->getcustomernamebyid($customer_id);

            $tzone_tablet = $this->tablets_model->get_tablet_by_zone($zone_id);

            if (isset($tzone_tablet->google_reg_id)) {
                //edited by vishnu
                //$messages = "Back Payment added for customer ".$customer_name->customer_name." an amount of ".$collected_amount;
                $messages = "Back Payment added for customer " . $customer_name->customer_name . " an amount of " . $requested_amount;
                //ends
                $push_fields = array();
                $push_fields['tab_id'] = $tablet_id;
                $push_fields['type'] = 4;
                $push_fields['message'] = $messages;
                $push = $this->bookings_model->add_push_notifications($push_fields);

                $return = android_push(array($tzone_tablet->google_reg_id), array('message' => $messages));
            }

            if ($back_payment_id > 0) {
                $errors['class'] = 'success';
                $errors['message'] = '<strong>Success!</strong> Back Payment added Successfully.';
            } else {
                $errors['class'] = 'warning';
                $errors['message'] = '<strong>Error!</strong> Something went wrong!';
            }
            /*
            $payment_fields = array();
            $payment_fields['customer_id'] = $customer_id;
            $payment_fields['tablet_id'] = $tablet_id;
            $payment_fields['zone_id'] = $zone_id;
            $payment_fields['collected_amount'] = $collected_amount;
            $payment_fields['requested_amount'] = $requested_amount;
            $this->customers_model->add_customer_payment($payment_fields);
            */
        }
        $data = array();
        $data['errors'] = $errors;
        $data['collected_date'] = date('d/m/Y');
        $data['customers'] = $this->customers_model->get_customers();
        $data['tablets'] = $this->tablets_model->get_all_tablets(TRUE, FALSE);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('add_back_payment', $data, TRUE);
        $layout_data['page_title'] = 'Add Back Payment';
        $layout_data['meta_description'] = 'Add Back Payment';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'backpayment.js');
        $this->load->view('layouts/default', $layout_data);
    }


    /*
     * Payment settings 
     * Author ; Jiby
     * Date : 09-10-17
     */

    public function payment_settings()
    {
        if (!user_permission(user_authenticate(), 11)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('hrly_price_sub')) {
            $from_hrly_name = $this->input->post('from_hrly_name');
            $to_hrly_name = $this->input->post('to_hrly_name');
            $service_rate = $this->input->post('hrly_price');
            $data = array(
                'from_hr' => $from_hrly_name,
                'to_hr' => $to_hrly_name,
                'price' => $service_rate,
                'status' => 1,
            );
            $this->settings_model->add_hrly_price($data);
        }
        if ($this->input->post('price_charges_edit')) {
            $service_id = $this->input->post('edit_ps_id');

            $from_hrly_name = $this->input->post('edit_from_hrly_name');
            $to_hrly_name = $this->input->post('edit_to_hrly_name');

            $service_rate = $this->input->post('edit_hourly_rate');
            $data = array(
                'from_hr' => $from_hrly_name,
                'to_hr' => $to_hrly_name,
                'price' => $service_rate,
                'status' => 1,
            );
            $this->settings_model->update_hourly_charges($data, $service_id);
        }
        $data['price_charges'] = $this->settings_model->get_prices();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('price_list', $data, TRUE);
        $layout_data['page_title'] = 'Price settings';
        $layout_data['meta_description'] = 'price settings';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1';
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js');
        $this->load->view('layouts/default', $layout_data);
    }


    public function edit_hourly_price()
    {
        $service_id = $this->input->post('ps_id');
        $result = $this->settings_model->get_hourly_price_details($service_id);
        echo json_encode($result);
    }


    function remove_hrly_charge()
    {
        $service_id = $this->input->post('ps_id');
        $data = $this->settings_model->delete_hrly_price($service_id);
    }


    public function sms_settings()
    {
        if (!user_permission(user_authenticate(), 11)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }

        if ($this->input->post('sms_edit_sub')) {
            $sms_id = $this->input->post('edit_sms_id');
            $user_name = $this->input->post('edit_user');
            $pass = $this->input->post('edit_pass');

            $sender_id = $this->input->post('edit_sender_id');
            $api = $this->input->post('edit_api');
            $data = array(
                'sender_id' => $sender_id,
                'api_url' => $api,
                'user' => $user_name,
                'pass' => $pass,
            );
            $this->settings_model->update_sms_settings($data, $sms_id);
        }
        $data['sms_settings']           = $this->settings_model->get_sms_info();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body']    = $this->load->view('sms_list', $data, TRUE);
        $layout_data['page_title']      = 'SMS settings';
        $layout_data['meta_description'] = 'SMS settings';
        $layout_data['css_files']       = array();
        $layout_data['external_js_files'] = array();
        $layout_data['account_active'] = '1';
        $layout_data['js_files']        = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function edit_sms_settings()
    {
        $sms_id = $this->input->post('sms_id');
        $result = $this->settings_model->get_sms_details($sms_id);
        echo json_encode($result);
    }

    public function email_settings()
    {
        if (!user_permission(user_authenticate(), 11)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }

        if ($this->input->post('email_edit_sub')) {
            $email_id = $this->input->post('edit_email_id');
            $header_name = $this->input->post('edit_header_name');
            $from = $this->input->post('edit_from');
            $subject = $this->input->post('edit_subject');

            $data = array(
                'id' => $email_id,
                'header_name' => $header_name,
                'subject' => $subject,
                'from_address' => $from,
            );
            $this->settings_model->update_email_settings($data, $email_id);
        }
        $data['email_settings']           = $this->settings_model->get_email_info();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body']    = $this->load->view('email_list', $data, TRUE);
        $layout_data['page_title']      = 'Email settings';
        $layout_data['meta_description'] = 'Email settings';
        $layout_data['css_files']       = array();
        $layout_data['external_js_files'] = array();
        $layout_data['account_active'] = '1';
        $layout_data['js_files']        = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function edit_email_settings()
    {
        $email_id = $this->input->post('e_id');
        $result = $this->settings_model->get_email_details($email_id);
        echo json_encode($result);
    }

    public function check_hourly_price()
    {
        $hrs    = $this->input->post('total_week_hours');
        $result = $this->settings_model->get_prices();
        $rate   = '';
        foreach ($result as $res) {
            if (($hrs >= $res->from_hr) && ($hrs <= $res->to_hr)) {
                $rate = $res->price;
                break;
            }
        }

        echo $rate;
        exit;
    }

    public function check_hourly_price_new()
    {
        $customerid    = $this->input->post('customer_id');
        $result = $this->settings_model->get_customer_per_hrprices($customerid);
        $rate   = $result->price_hourly;
        //        foreach ($result as $res) {
        //            if (($hrs >= $res->from_hr) && ($hrs <= $res->to_hr)) {
        //                $rate = $res->price;
        //                break;
        //            }
        //        }
        echo $rate;
        exit;
    }

    public function tax_settings()
    {
        if (!user_permission(user_authenticate(), 11)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }

        if ($this->input->post('tax_edit_sub')) {
            $tax_id = $this->input->post('edit_taxt_id');
            $percentage = $this->input->post('edit_percentage');

            $data = array(
                'tax_id' => $tax_id,
                'percentage' => $percentage,
                'status' => 1,
            );
            $this->settings_model->update_tax_settings($data, $tax_id);
        }
        $data['tax_settings']           = $this->settings_model->get_tax_info();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body']    = $this->load->view('tax_list', $data, TRUE);
        $layout_data['page_title']      = 'Tax settings';
        $layout_data['meta_description'] = 'Tax settings';
        $layout_data['css_files']       = array();
        $layout_data['external_js_files'] = array();
        $layout_data['account_active'] = '1';
        $layout_data['js_files']        = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function edit_tax_settings()
    {
        $tax_id = $this->input->post('t_id');
        $result = $this->settings_model->get_tax_details($tax_id);
        echo json_encode($result);
    }

    public function coupons()
    {
        $data = array();
        $data['coupons'] = $this->settings_model->get_coupons();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('coupons', $data, TRUE);
        $layout_data['page_title'] = 'Coupons';
        $layout_data['meta_description'] = 'Coupons';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('users.js');
        $layout_data['account_active'] = '1';
        $this->load->view('layouts/default', $layout_data);
    }

    public function add_coupons()
    {
        $e_coupon = array();
        if ($this->input->post('coupon_sub')) {
            $errors = array();

            $this->load->library('form_validation');


            //$this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

            $this->form_validation->set_rules('couponname', 'Coupon Name', 'required');
            //$this->form_validation->set_message('required', 'Enter coupon name', 'couponname');

            $this->form_validation->set_rules('expirydate', 'Expiry Date', 'required');
            //$this->form_validation->set_message('required', 'Select a date', 'expirydate');

            $this->form_validation->set_rules('service_type', 'Service type', 'required');

            $this->form_validation->set_rules('coupontype', 'Coupon Type', 'required');

            //$this->form_validation->set_rules('offertype', 'Offer Type', 'required');
            //$this->form_validation->set_message('required', 'Select offer type', 'offertype');

            $this->form_validation->set_rules('discount_price', 'Price', 'required');
            //$this->form_validation->set_message('required', 'Enter Price', 'discountprice');

            //$this->form_validation->set_rules('discounttype', 'Discount Type', 'required');
            //$this->form_validation->set_message('required', 'Select an option', 'discounttype');

            $this->form_validation->set_rules('minhour', 'Minimum Hour', 'required');

            $this->form_validation->set_rules('w_day', 'Week Day', 'required');
            //$this->form_validation->set_message('required', 'Please select the days', 'w_day');

            $this->form_validation->set_rules('status', 'Status', 'required');
            // $this->form_validation->set_message('required', 'Select status', 'status');

            if ($this->form_validation->run() == TRUE) {
                $coupon_name = $this->input->post('couponname');
                $expirydate = $this->input->post('expirydate');
                $service_type = $this->input->post('service_type');
                $coupontype = $this->input->post('coupontype');
                //$offertype = 'P';
                $offertype = $this->input->post('offertype');
                if ($this->input->post('offertype') == 'F') {
                    //$discountprice = $this->input->post('discountprice');
                    $discount_type = $this->input->post('discounttype');
                } else {
                    //$discountprice = $this->input->post('discount_price');
                    $discount_type  = 1;
                }
                $discountprice = $this->input->post('discount_price');
                //$discount_type  = 1;
                $valid_week_day = implode(",", $this->input->post('w_day'));
                //$valid_week_day = '0,1,2,3,4,6';	
                $status      = $this->input->post('status');
                $added_date     = date('Y-m-d');
                $min_hr = $this->input->post('minhour');

                $time_fields = array();
                $time_fields['coupon_name']        = $coupon_name;
                $time_fields['service_id']        = $service_type;
                $time_fields['expiry_date']        = $expirydate;
                $time_fields['type']               = "C";
                $time_fields['coupon_type']         = $coupontype;
                $time_fields['offer_type']         = $offertype;
                $time_fields['percentage']         = $discountprice;
                $time_fields['discount_type']      = $discount_type;
                $time_fields['valid_week_day']     = $valid_week_day;
                $time_fields['status']             = $status;
                $time_fields['min_hrs']             = $min_hr;
                $time_fields['added_date']         = date('Y-m-d H:i:s');

                $coupon_add                        = $this->settings_model->add_coupon($time_fields);
                if ($coupon_add > 0) {
                    redirect(base_url() . 'coupons');
                    exit();
                }
            } else {
                $errors = $this->form_validation->error_array();
            }
        }
        $data = array();
        $data['e_coupon'] = $e_coupon;
        $data['services'] = $this->settings_model->get_services();
        $data['error'] = isset($errors) ? array_shift($errors) : '';
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('add_coupon', $data, TRUE);
        $layout_data['page_title'] = 'Coupons';
        $layout_data['meta_description'] = 'Coupons';
        $layout_data['css_files'] = array('datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'bootstrap-datepicker.js', 'users.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function edit_coupon()
    {
        $e_coupon_id = 0;
        $e_coupon = array();
        if ($this->uri->segment(3) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0) {
            if ($this->input->post('coupon_sub')) {
                $errors = array();

                $this->load->library('form_validation');


                //$this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

                $this->form_validation->set_rules('couponname', 'Coupon Name', 'required');
                //$this->form_validation->set_message('required', 'Enter coupon name', 'couponname');

                $this->form_validation->set_rules('expirydate', 'Expiry Date', 'required');
                //$this->form_validation->set_message('required', 'Select a date', 'expirydate');

                $this->form_validation->set_rules('service_type', 'Expiry Date', 'required');

                $this->form_validation->set_rules('discountprice', 'Price', 'required');
                //$this->form_validation->set_message('required', 'Enter Price', 'discountprice');

                $this->form_validation->set_rules('minhour', 'Minimum Hour', 'required');

                $this->form_validation->set_rules('w_day', 'Week Day', 'required');
                //$this->form_validation->set_message('required', 'Please select the days', 'w_day');

                $this->form_validation->set_rules('status', 'Status', 'required');
                // $this->form_validation->set_message('required', 'Select status', 'status');

                if ($this->form_validation->run() == TRUE) {
                    $e_coupon_id = $this->uri->segment(3);
                    $coupon_name = $this->input->post('couponname');
                    $expirydate = $this->input->post('expirydate');
                    $service_type = $this->input->post('service_type');
                    $discountprice = $this->input->post('discountprice');
                    $valid_week_day = implode(",", $this->input->post('w_day'));
                    $status      = $this->input->post('status');
                    $min_hr = $this->input->post('minhour');
                    $added_date     = date('Y-m-d');

                    $time_fields = array();
                    $time_fields['coupon_name']        = $coupon_name;
                    $time_fields['service_id']        = $service_type;
                    $time_fields['expiry_date']        = $expirydate;
                    $time_fields['percentage']         = $discountprice;
                    $time_fields['valid_week_day']     = $valid_week_day;
                    $time_fields['status']             = $status;
                    $time_fields['min_hrs']             = $min_hr;
                    $time_fields['added_date']         = date('Y-m-d H:i:s');

                    $coupon_add                        = $this->settings_model->update_coupon($e_coupon_id, $time_fields);
                    if ($coupon_add) {
                        redirect(base_url() . 'coupons');
                        exit();
                    }
                } else {
                    $errors = $this->form_validation->error_array();
                }
            }

            $e_coupon_id = $this->uri->segment(3);
            $data = array();
            $data['coupon_info'] = $this->settings_model->get_coupon_by_id($e_coupon_id);
            $data['services'] = $this->settings_model->get_services();
            $data['coupon_id'] = $e_coupon_id;
            $data['error'] = isset($errors) ? array_shift($errors) : '';
            $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
            $layout_data['content_body'] = $this->load->view('edit_coupon', $data, TRUE);
            $layout_data['page_title'] = 'Coupons';
            $layout_data['meta_description'] = 'Coupons';
            $layout_data['css_files'] = array('datepicker.css');
            $layout_data['external_js_files'] = array();
            $layout_data['js_files'] = array('base.js', 'bootstrap-datepicker.js', 'users.js');
            $this->load->view('layouts/default', $layout_data);
        } else {
            redirect(base_url() . 'coupons');
            exit();
        }
    }

    public function remove_coupon()
    {
        $coupon_id = $this->input->post('coupon_id');
        $data = array(
            'status' => '0',
        );
        $datas = $this->settings_model->update_coupon($coupon_id, $data);
        echo "success";
    }

    public function souqmaid_price()
    {
        if ($this->input->post('services_rate_sub')) {
            $service_date_val = $this->input->post('service_date_val');
            $service_rate = $this->input->post('service_rate');
            list($day, $month, $year) = explode("/", $service_date_val);
            $service_date = "$year-$month-$day";
            $checkexist = $this->settings_model->get_offer_list($service_date);
            if (count($checkexist) > 0) {
                echo '<script language="javascript">';
                echo 'alert("Already added...")';
                echo '</script>';
            } else {
                $data = array(
                    'offer_date' => $service_date,
                    'offer_price' => $service_rate,
                    'added_date_time' => date('Y-m-d h:i:s'),
                    'added_by' => user_authenticate(),
                );
                $this->settings_model->add_souqmaid_price($data);
            }
        }
        if ($this->input->post('services_rate_edit')) {
            $service_price_id = $this->input->post('edit_service_offer_id');
            $service_rate = $this->input->post('edit_service_rate');
            $data = array(
                'offer_price' => $service_rate,
                'updated_date_time' => date('Y-m-d h:i:s'),
                'updated_by' => user_authenticate(),
            );
            $this->settings_model->update_souqmaid_price($data, $service_price_id);
        }
        $data['prices'] = $this->settings_model->get_souqmaid_pricelist();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('souq_price_list', $data, TRUE);
        $layout_data['page_title'] = 'Souq Maids Price';
        $layout_data['meta_description'] = 'Souq Maids Price';
        $layout_data['css_files'] = array('datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1';
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function edit_souqprice()
    {
        $price_id = $this->input->post('price_id');
        $result = $this->settings_model->get_souqmaid_pricelist_byid($price_id);
        echo json_encode($result);
    }

    public function update_service_webshow_status()
    {
        $serviceid = $this->input->post('serviceid');
        $webshow_status = $this->input->post('webshow_status');
        $result = $this->settings_model->update_ser_webshow_stat($serviceid, $webshow_status);
        if ($result) {
            echo "success";
        } else {
            echo "fail";
        }
    }

    public function do_zone_odoo_api($data = array())
    {
        // if ($data['spare_zone'] == 'Y') {
        //     $spare = TRUE;
        // } else {
        //     $spare = FALSE;
        // }
        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['zone_name'];
        $post['params']['driver_name'] = $data['driver_name'];
        // $post['params']['spare_zone'] = $spare;
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "zone_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_zone_id = $returnData->result->response->id;
        }
    }

    public function update_zone_inodoo($data, $zone_id)
    {
        // if ($data['spare_zone'] == 'Y') {
        //     $spare = TRUE;
        // } else {
        //     $spare = FALSE;
        // }
        $post['params']['user_id'] = 1;
        $post['params']['id'] = $zone_id;
        $post['params']['name'] = $data['zone_name'];
        $post['params']['driver_name'] = $data['driver_name'];
        $post['params']['description'] = $data['description'];
        // $post['params']['spare_zone'] = $spare;
        // echo '<pre>';
        // print_r($post);die();
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "zone_edition";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_zone_id = $zone_id;
        }
    }

    public function do_area_odoo_api($data = array())
    {
        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['area_name'];
        $post['params']['zone_id'] = (int) $data['zone_id'];
        $post['params']['location_charge'] = $data['area_charge'];
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "area_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_area_id = $returnData->result->response->id;
        }
    }

    public function update_area_inodoo($data, $area_id)
    {
        $post['params']['user_id'] = 1;
        $post['params']['id'] = $area_id;
        $post['params']['name'] = $data['area_name'];
        $post['params']['zone_id'] = (int) $data['zone_id'];
        $post['params']['description'] = $data['description'];
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "area_edition";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);
        if ($returnData->result->status == "success") {
            return $odoo_area_id = $area_id;
        }
    }

    public function do_flat_odoo_api($data = array())
    {
        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['name'];
        $post['params']['code'] = $data['code'];
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "flat_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_flat_id = $returnData->result->response->id;
        }
    }

    public function update_flat_inodoo($data, $flat_id)
    {
        $post['params']['user_id'] = 1;
        $post['params']['id'] = $flat_id;
        $post['params']['name'] = $data['name'];
        $post['params']['code'] = $data['code'];
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "flat_edition";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_flat_id = $flat_id;
        }
    }

    public function export_zone_to_odoo_api()
    {
        //$zones = $this->settings_model->get_zones_api();
        $zones = $this->settings_model->get_zones_packages_api();
        $i = 0;
        foreach ($zones as $zone) {
            if ($zone['spare_zone'] == 'Y') {
                $spare = TRUE;
            } else {
                $spare = FALSE;
            }

            $post['params']['user_id'] = 1;
            $post['params']['name'] = $zone['zone_name'];
            $post['params']['driver_name'] = $zone['driver_name'];
            $post['params']['spare_zone'] = $spare;
            $post_values = json_encode($post);
            $url = $this->config->item('odoo_url') . "zone_creation";
            $login_check = curl_api_service($post_values, $url);
            $returnData = json_decode($login_check);

            if ($returnData->result->status == "success") {
                $odoo_zone_id = $returnData->result->response->id;
                if ($odoo_zone_id > 0) {
                    //$response = $this->settings_model->update_zones(array('odoo_new_zone_id' => $odoo_zone_id,'odoo_new_zone_status' => 1),$zone['zone_id']);
                    $response = $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id, 'odoo_package_zone_status' => 1), $zone['zone_id']);
                }
                echo $i++ . '-' . $odoo_zone_id . '<br>';
            }
        }
        exit();
    }

    public function export_areas_to_odoo_api()
    {
        // $areas = $this->settings_model->get_areas_api();
        $areas = $this->settings_model->get_packages_areas_api();
        $i = 0;
        foreach ($areas as $area) {
            $post['params']['user_id'] = 1;
            $post['params']['name'] = $area['area_name'];
            $post['params']['zone_id'] = $area['odoo_zone_id'];
            $post['params']['location_charge'] = $area['area_charge'];
            $post_values = json_encode($post);
            $url = $this->config->item('odoo_url') . "area_creation";
            $login_check = curl_api_service($post_values, $url);
            $returnData = json_decode($login_check);

            if ($returnData->result->status == "success") {
                $odoo_area_id = $returnData->result->response->id;
                if ($odoo_area_id > 0) {
                    // $response = $this->settings_model->update_area(array('odoo_new_area_id' => $odoo_area_id,'odoo_new_area_status' => 1),$area['area_id']);
                    $response = $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area['area_id']);
                }
                echo $i++ . '-' . $odoo_area_id . '<br>';
            }
        }
        exit();
    }

    public function export_flats_to_odoo_api()
    {
        // $flats = $this->settings_model->get_flats_api();
        $flats = $this->settings_model->get_package_flats_api();
        $i = 0;
        foreach ($flats as $flat) {
            $post['params']['user_id'] = 1;
            $post['params']['name'] = $flat['flat_name'];
            $post['params']['code'] = $flat['tablet_imei'];
            $post_values = json_encode($post);
            $url = $this->config->item('odoo_url') . "flat_creation";
            $login_check = curl_api_service($post_values, $url);
            $returnData = json_decode($login_check);

            if ($returnData->result->status == "success") {
                $odoo_flat_id = $returnData->result->response->id;
                if ($odoo_flat_id > 0) {
                    // $response = $this->settings_model->update_flats(array('odoo_new_flat_id' => $odoo_flat_id,'odoo_new_syncflat_status' => 1),$flat['flat_id']);
                    $response = $this->settings_model->update_flats(array('odoo_package_flat_id' => $odoo_flat_id, 'odoo_package_flat_status' => 1), $flat['flat_id']);
                }
                echo $i++ . '-' . $odoo_flat_id . '<br>';
            }
        }
        exit();
    }
    public function change_status()
    {
        $this->settings_model->update_status();

        echo true;
        exit;
    }
    public function change_status_enable()
    {
        $this->settings_model->update_status_enable();

        echo true;
        exit;
    }

    function remove_area_click()
    {
        $area_id = $this->input->post('area_id');
        $status = $this->input->post('web_status');
        $status = $status == 1 ? 0 : 1;


        // if($status == 0)
        // {
        //     $have_bookings = $this->customers_model->get_bookings_by_customer_id_new($customer_id);

        //     // echo "<pre>";print_r($have_bookings);die;

        //     if($have_bookings > 0)
        //     {
        //         echo 'exist_bookings';
        //         exit();
        //     }
        // }
        // End

        $this->settings_model->delete_area_click($area_id, $status);
        $areas = $this->settings_model->get_area_by_id($area_id);


        echo $status;
        exit;
    }

    function areaweb_status()
    {
        $area_id = $this->input->post('area_id');
        $status = $this->input->post('web_status');
        if ($status == 1) {
            $data = array(
                'web_status' => 0
            );
            $this->settings_model->disable_webstatus($area_id, $data);
        } else {
            $data = array(
                'web_status' => 1
            );
            $this->settings_model->activate_webstatus($area_id, $data);
        }
    }

    public function location()
    {
        if (!user_permission(user_authenticate(), 8)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('location_sub')) {
            $location_name = $this->input->post('locationname');
            $area_id = $this->input->post('area_id');
            $description = $this->input->post('description');
            $longitude = $this->input->post('longitude');
            $latitude = $this->input->post('latitude');
            $data = array(
                'area_id' => $area_id,
                'location_name' => $location_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'description' => $description,
                'longitude' => $longitude,
                'latitude' => $latitude,
            );
            $location_id = $this->settings_model->add_location($data);
            // $getodoozone_id = $this->settings_model->get_zone_details($zone_id);

            // $zone_area_fields = array();
            // $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
            // $zone_area_fields['area_name'] = $areaname;
            // $zone_area_fields['area_charge'] = 40.00;

            //$odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            //$this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id,'odoo_package_area_status' => 1),$area_id);
        }
        if ($this->input->post('location_edit')) {
            $location_name = $this->input->post('edit_locationname');
            $location_id = $this->input->post('edit_locationid');
            $area_id = $this->input->post('edit_area_id');
            $description = $this->input->post('edit_description');
            $longitude = $this->input->post('edit_longitude');
            $latitude = $this->input->post('edit_latitude');
            $min_booking_hour = $this->input->post('edit_min_booking_hour');
            $data = array(
                'area_id' => $area_id,
                'location_name' => $location_name,
                'updated_at' => date('Y-m-d H:i:s'),
                'description' => $description,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'min_booking_hour' => $min_booking_hour,
            );

            $this->settings_model->update_location($data, $location_id);
            $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            $get_area_by_id = $this->settings_model->get_area_details($area_id);
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
            $zone_area_fields['area_name'] = $areaname;
            $zone_area_fields['area_charge'] = 40.00;
            //$odo_area_affected = $this->update_area_inodoo($zone_area_fields, $get_area_by_id[0]['odoo_package_area_id']);
        }
        $data['areas'] = $this->settings_model->get_areas();
        $data['zones'] = $this->settings_model->get_zones();
        $data['locations'] = $this->settings_model->get_locations();
        // print_r($data['locations']);die();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('location_list', $data, TRUE);
        $layout_data['page_title'] = 'Location List';
        $layout_data['meta_description'] = 'area';
        $layout_data['css_files'] = array('demo.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    function edit_location()
    {
        $location_id = $this->input->post('location_id');
        $result = $this->settings_model->get_location_details($location_id);
        echo json_encode($result);
    }

    public function change_status_enable_location()
    {
        $this->settings_model->update_location_status_enable();

        echo true;
        exit;
    }
    public function change_status_location()
    {
        $this->settings_model->update_status_location();

        echo true;
        exit;
    }

    public function remove_location()
    {
        $location_id = $this->input->post('location_id');

        //$data = $this->settings_model->update_location(array('location_status' => 0),$location_id);
        $data = $this->settings_model->delete_location($location_id);
    }

    function zone_status()
    {
        $zone_id = $this->input->post('zone_id');
        $status = $this->input->post('zone_status');
        log_message('error','zone id '. $zone_id);
        log_message('error','zone status '. $status);

        if ($status == 1) {
            $data = array(
                'zone_status' => 0,
            );
            $this->settings_model->disable_zone_status($zone_id, $data);
        } else {
            $data = array(
                'zone_status' => 1,
            );
            $this->settings_model->enable_zone_status($zone_id, $data);
        }
    }
    function locweb_status()
    {
        $location_id = $this->input->post('location_id');
        $status = $this->input->post('location_status');
        log_message('error','location id '. $location_id);
        log_message('error','location status '. $status);

        if ($status == 1) {
            $data = array(
                'deleted_at' => date('Y-m-d H:i:s'),
            );
            $this->settings_model->loc_disable_webstatus($location_id, $data);
        } else {
            $data = array(
                'deleted_at' => null,
            );
            $this->settings_model->loc_activate_webstatus($location_id, $data);
        }
    }

    function landmark_status()
    {

        $landmark_id = $this->input->post('landmark_id');
        $status = $this->input->post('landmark_status');

        if ($status == 1) {
            $data = array(
                'deleted_at' => date('Y-m-d H:i:s'),
            );
            $this->settings_model->land_disable_webstatus($landmark_id, $data);
        } else {
            $data = array(
                'deleted_at' => null,
            );
            $this->settings_model->land_activate_webstatus($landmark_id, $data);
        }
    }

    public function landmark()
    {
        if (!user_permission(user_authenticate(), 8)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        if ($this->input->post('landmark_sub')) {
            $landmark_name = $this->input->post('landmarkname');
            $area_id = $this->input->post('area_id');
           
            $location_id = $this->input->post('location_id');
            $description = $this->input->post('description');
            $data = array(
                // 'area_id' => $area_id,
                'location_id' => $location_id,
                'landmark_name' => $landmark_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'description' => $description,
            );
            $landmark_id = $this->settings_model->add_landmark($data);
            $get_area_by_id = $this->settings_model->get_area_details($area_id);
            $get_location_by_id = $this->settings_model->get_location_details($location_id);

            if (empty($get_location_by_id->odoo_package_location_id)) {
            
                if (empty($get_area_by_id->odoo_package_area_id)) {
                    // echo "odoo_package_area_id is empty or zero.";
                    $getodoozone_id = $this->settings_model->get_zone_details($get_area_by_id[0]['zone_id']);
                    // $get_area_by_id = $this->settings_model->get_area_details($area_id);

                    if (empty($getodoozone_id->odoo_package_zone_id)) {
                        $zone_odoo_fields = array();
                        $zone_odoo_fields['zone_name'] = $getodoozone_id[0]['zone_name'];
                        $zone_odoo_fields['driver_name'] = $getodoozone_id[0]['driver_name'];
                        $zone_odoo_fields['description'] = $getodoozone_id[0]['description'];
                        $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
                        $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

                    } 
                    $zone_area_fields = array();
                    $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
                    $zone_area_fields['area_name'] = $get_area_by_id[0]['area_name'];
                    $zone_area_fields['area_charge'] = $get_area_by_id[0]['area_charge'];
                    $odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
                    $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area_id);
                    $area_location_fields = array();
                    $area_location_fields['location_name'] = $get_location_by_id->location_name;
                    $area_location_fields['area_id'] = $odoo_area_id;
                    $area_location_fields['description'] = $get_location_by_id->description;
                    $area_location_fields['longitude'] = $get_location_by_id->longitude;
                    $area_location_fields['latitude'] =$get_location_by_id->latitude;
                    $area_location_fields['minimum_hour'] = $get_location_by_id->min_booking_hour;
                    $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
                    $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_new_location_status' => 1),$location_id);
                      
                    $location_landmark_fields = array();
                    $location_landmark_fields['name']=  $landmark_name;
                    $location_landmark_fields['location_id'] =  $odoo_location_id;
                    $location_landmark_fields['area_id'] = $odoo_area_id;
                    $location_landmark_fields['description'] = $description;

                    $odoo_landmark_id = $this->do_landmark_odoo_api($location_landmark_fields);
                    $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);

                } 
                else {
                    // echo "odoo_package_area_id is not empty.";
                    $area_location_fields = array();
                    $area_location_fields['location_name'] =  $get_location_by_id->location_name;
                    $area_location_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                    $area_location_fields['description'] = $get_location_by_id->description;
                    $area_location_fields['longitude'] = $get_location_by_id->longitude;
                    $area_location_fields['latitude'] = $get_location_by_id->latitude;
                    $area_location_fields['minimum_hour'] = $get_location_by_id->min_booking_hour;
                    $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
                    $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_new_location_status' => 1),$location_id);

                    $location_landmark_fields = array();
                    $location_landmark_fields['name']=  $landmark_name;
                    $location_landmark_fields['location_id'] =  $odoo_location_id;
                    $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                    $location_landmark_fields['description'] = $description;

                    $odoo_landmark_id = $this->do_landmark_odoo_api($location_landmark_fields);
                    $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);
                }
                
            } else {
                $location_landmark_fields = array();
                $location_landmark_fields['name']=  $landmark_name;
                $location_landmark_fields['location_id'] =  $get_location_by_id->odoo_package_location_id;
                $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                $location_landmark_fields['description'] = $description;
             
                $odoo_landmark_id = $this->do_landmark_odoo_api($location_landmark_fields);
                // print_r("helooo".$odoo_landmark_id);
                $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);
                
            }
            
            // print_r($area_location_fields);die();
            // $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_new_location_status' => 1),$location_id);
        
        }
        if ($this->input->post('landmark_edit')) {
            $landmark_name = $this->input->post('edit_landmarkname');
            $landmark_id = $this->input->post('edit_landmarkid');
            $area_id = $this->input->post('edit_area_id');
            $location_id = $this->input->post('edit_location_id');
            $description = $this->input->post('edit_description');
            $data = array(
                'location_id' => $location_id,
                'landmark_name' => $landmark_name,
                'updated_at' => date('Y-m-d H:i:s'),
                'description' => $description,
            );

            $this->settings_model->update_landmark($data, $landmark_id);
            // $getodoozone_id = $this->settings_model->get_zone_details($zone_id);
            // $get_area_by_id = $this->settings_model->get_area_details($area_id);
            $get_area_by_id = $this->settings_model->get_area_details($area_id);
            $get_location_by_id = $this->settings_model->get_location_details_by_id($location_id);
        
                if (empty($get_location_by_id[0]['odoo_package_location_id'])) {
            
                    if (empty($get_area_by_id[0]['odoo_package_area_id'])) {
                        // echo "odoo_package_area_id is empty or zero.";
                        $getodoozone_id = $this->settings_model->get_zone_details($get_area_by_id[0]['zone_id']);
                        // $get_area_by_id = $this->settings_model->get_area_details($area_id);

                        if (empty($getodoozone_id[0]['odoo_package_zone_id'])) {
                            $zone_odoo_fields = array();
                            $zone_odoo_fields['zone_name'] = $getodoozone_id[0]['zone_name'];
                            $zone_odoo_fields['driver_name'] = $getodoozone_id[0]['driver_name'];
                            $zone_odoo_fields['description'] = $getodoozone_id[0]['description'];
                            $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
                            $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

                        } 
                        $zone_area_fields = array();
                        $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
                        $zone_area_fields['area_name'] = $get_area_by_id[0]['area_name'];
                        $zone_area_fields['area_charge'] = $get_area_by_id[0]['area_charge'];
                        $odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
                        $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area_id);
                        $area_location_fields = array();
                        $area_location_fields['location_name'] = $get_location_by_id->location_name;
                        $area_location_fields['area_id'] = $odoo_area_id;
                        $area_location_fields['description'] = $get_location_by_id->description;
                        $area_location_fields['longitude'] = $get_location_by_id->longitude;
                        $area_location_fields['latitude'] =$get_location_by_id->latitude;
                        $area_location_fields['minimum_hour'] = $get_location_by_id->minimum_hour;
                        $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
                        $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_package_location_status' => 1),$location_id);
                        
                        // $location_landmark_fields = array();
                        // $location_landmark_fields['name']=  $landmark_name;
                        // $location_landmark_fields['location_id'] =  $odoo_location_id;
                        // $location_landmark_fields['area_id'] = $odoo_area_id;
                        // $location_landmark_fields['description'] = $description;

                        // $odo_landmark_affected = $this->update_landmark_inodoo($location_landmark_fields);
                        // print_r("heyy".$odoo_landmark_id);die();
                        // $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);

                    } 
                    else {
                        // echo "odoo_package_area_id is not empty.";
                        $area_location_fields = array();
                        $area_location_fields['location_name'] =  $get_location_by_id[0]['location_name'];
                        $area_location_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                        $area_location_fields['description'] = $get_location_by_id[0]['description'];
                        // $area_location_fields['longitude'] = $get_location_by_id[0]['longitude'];
                        $area_location_fields['latitude'] = $get_location_by_id[0]['latitude'];
                        $area_location_fields['minimum_hour'] = $get_location_by_id[0]['minimum_hour'];
                        $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
                        // print_r($get_location_by_id);die();
                        $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_new_location_status' => 1),$location_id);

                        // $location_landmark_fields = array();
                        // $location_landmark_fields['name']=  $landmark_name;
                        // $location_landmark_fields['location_id'] =  $odoo_location_id;
                        // $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                        // $location_landmark_fields['description'] = $description;
                        // $odoo_landmark_id = $this->do_landmark_odoo_api($location_landmark_fields);
                        // // print_r("helloo".$odoo_landmark_id);die();
                        // $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);
                        
                    }
                    
                }
                $get_landmark_by_id = $this->settings_model->get_landmark_details($landmark_id);
                
                if (empty($get_landmark_by_id[0]['odoo_package_landmark_id'])) {
                    // print_r("helloo");die();
                    $location_landmark_fields = array();
                    $location_landmark_fields['name']=  $landmark_name;
                    $location_landmark_fields['location_id'] =  $get_location_by_id[0]['odoo_package_location_id'];
                    $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                    $location_landmark_fields['description'] = $description;
                    $odoo_landmark_id = $this->do_landmark_odoo_api($location_landmark_fields);
                    $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);
                }  else {
                    $location_landmark_fields = array();
                    $location_landmark_fields['name']=  $landmark_name;
                    $location_landmark_fields['location_id'] =  $get_location_by_id[0]['odoo_package_location_id'];
                    $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                    $location_landmark_fields['description'] = $description;
                    $odo_landmark_affected = $this->update_landmark_inodoo($location_landmark_fields, $get_landmark_by_id[0]['odoo_package_landmark_id']);
                }
                // else {
                //     $location_landmark_fields = array();
                //     $location_landmark_fields['name']=  $landmark_name;
                //     $location_landmark_fields['location_id'] =  $get_location_by_id->odoo_package_location_id;
                //     $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
                //     $location_landmark_fields['description'] = $description;
                //     $odoo_landmark_id = $this->update_landmark_inodoo($location_landmark_fields);
                //     $this->settings_model->update_landmark(array('odoo_package_landmark_id' => $odoo_landmark_id,'odoo_package_landmark_status' => 1),$landmark_id);
                    
                // }
            // } else {
            //     $location_landmark_fields = array();
            //     $location_landmark_fields['name']=  $landmark_name;
            //     $location_landmark_fields['location_id'] =  $get_location_by_id->odoo_package_location_id;
            //     $location_landmark_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
            //     $location_landmark_fields['description'] = $description;
            //     $odo_landmark_affected = $this->update_landmark_inodoo($location_landmark_fields, $get_landmark_by_id[0]['odoo_package_landmark_id']);
            // }
            // $zone_area_fields = array();
            // $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
            // $zone_area_fields['area_name'] = $areaname;
            // $zone_area_fields['area_charge'] = 40.00;
            //$odo_area_affected = $this->update_area_inodoo($zone_area_fields, $get_area_by_id[0]['odoo_package_area_id']);
        }
        $data['areas'] = $this->settings_model->get_areas();
        $data['zones'] = $this->settings_model->get_zones();
        $data['locations'] = $this->settings_model->get_locations();
        $data['landmarks'] = $this->settings_model->get_landmarks();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('landmark_list', $data, TRUE);
        $layout_data['page_title'] = 'Landmark List';
        $layout_data['meta_description'] = 'area';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function do_landmark_odoo_api($data = array())
    {
        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['name'];
        $post['params']['area_id'] = (int) $data['area_id'];
        $post['params']['location_id'] = (int) $data['location_id'];
        $post['params']['description'] = $data['description'];
        $post_values = json_encode($post);
        

        $url = $this->config->item('odoo_url')."landmark_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);
        if ($returnData->result->status == "success") {
            return $odoo_landmark_id = $returnData->result->response->id;
        }
    }

    public function update_landmark_inodoo($data, $landmark_id)
    {
 
        $post['params']['user_id'] = 1;
        $post['params']['landmark_id'] = $landmark_id;
        $post['params']['name'] = $data['name'];
        $post['params']['area_id'] = (int) $data['area_id'];
        $post['params']['location_id'] = (int) $data['location_id'];
        $post['params']['description'] = $data['description'];
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "landmark_update";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);
        
        if ($returnData->result->status == "success") {
            return $odoo_landmark_id = $landmark_id;
        }
    }
    public function getLocations()
    {
        $areaId = $this->input->post('area_id');
        $locations = $this->settings_model->getLocationsByArea($areaId);
        echo json_encode($locations);
    }

    function edit_landmark()
    {
        $landmark_id = $this->input->post('landmark_id');
        $result = $this->settings_model->get_landmark_details($landmark_id);
        echo json_encode($result);
    }

    function view_landmark()
    {
        $landmark_id = $this->input->post('landmark_id');
        $result = $this->settings_model->get_landmark_details_view($landmark_id);
        echo json_encode($result);
    }

    public function remove_landmark()
    {
        $landmark_id = $this->input->post('landmark_id');
        $data = $this->settings_model->delete_landmark($landmark_id);
    }

    public function check_zone_name()
    {
        //echo 'God is Love'; die;
        $zoneName = $this->input->post('zone_name');
        $exists = $this->settings_model->check_zone_exists($zoneName);
        echo json_encode(array('exists' => $exists));
    }

    function save_zone()
    {
        $zone_name = $this->input->post('zone_name');

        $description = $this->input->post('description');

        $data = array(
            'zone_name' => $zone_name,
            'zone_status' => 1,
            'driver_name' => 0,
            'spare_zone' => 'N',
            'odoo_zone_id' => 0,
            'odoo_new_zone_id' => 0,
            'description' => $description,
        );

        $zone_odoo_fields = array();
        $zone_odoo_fields['zone_name'] = $this->input->post('zone_name');
        $zone_odoo_fields['driver_name'] = $this->input->post('drivername');
        $zone_odoo_fields['description'] = $this->input->post('description');
        // $zone_odoo_fields['spare_zone'] = $spare;
        // print_r($zone_odoo_fields);die();
        $zone_id = $this->settings_model->add_zones($data);
        $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields);
        $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);
        echo json_encode(array('success' => true));
    }
    function edit_zone_new()
    {
        $zone_id = $this->input->post('edit_zoneid');
        $zone_name = $this->input->post('edit_zonename');
        $drivername = $this->input->post('edit_drivername');
        $spare = $this->input->post('edit_spare');
        $description = $this->input->post('edit_description');
        $data = array(
            'zone_name' => $zone_name,
            'driver_name' => 0,
            'spare_zone' => 'N',
            'zone_status' => 1,
            'odoo_zone_id' => 0,
            'odoo_new_zone_id' => 0,
            'description' => $description,
        );
        $zone_odoo_fields = array();
        $zone_odoo_fields['id'] = $this->input->post('edit_zoneid');
        $zone_odoo_fields['zone_name'] = $this->input->post('edit_zonename');
        $zone_odoo_fields['driver_name'] = $this->input->post('edit_drivername');
        $zone_odoo_fields['description'] = $this->input->post('edit_description');
        // $zone_odoo_fields['spare_zone'] = $spare;
        $this->settings_model->update_zones($data, $zone_id);
        $get_zone_by_id = $this->settings_model->get_zone_details($zone_id);
        // print_r($get_zone_by_id);
        if ($get_zone_by_id[0]['odoo_package_zone_id'] == 0) {
            // print_r("huhfghgb");die();
            $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
            $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

        } 
        $odo_zone_affected = $this->update_zone_inodoo($zone_odoo_fields, $get_zone_by_id[0]['odoo_package_zone_id']);

        echo json_encode(array('success' => true));
    }

    public function check_zone_name_edit()
    {
        $zoneName = $this->input->post('zone_name');
        $zoneId = $this->input->post('zone_id');
        $exists = $this->settings_model->check_zone_exists_edit($zoneName, $zoneId);
        echo json_encode(array('exists' => $exists));
    }
    public function check_area_name()
    {
        $area_name = $this->input->post('area_name');
        $exists = $this->settings_model->check_area_exists($area_name);
        echo json_encode(array('exists' => $exists));
    }

    function save_area()
    {

        $areaname = $this->input->post('areaname');
        $zone_id = $this->input->post('zone_id');
        $description = $this->input->post('description');
        $new_booking_hour = $this->input->post('new_booking_hour') ?: null;
        $data = array(
            'zone_id' => $zone_id,
            'area_name' => $areaname,
            'area_status' => 1,
            'odoo_area_id' => 0,
            'odoo_new_area_id' => 0,
            'description' => $description,
            'min_booking_hour' => $new_booking_hour,
        );
        $area_id = $this->settings_model->add_area($data);
        echo json_encode(array('success' => true));
    }

    function check_area_name_edit()
    {
        $area_name = $this->input->post('area_name');
        $area_id = $this->input->post('area_id');
        $exists = $this->settings_model->check_area_exists_edit($area_name, $area_id);
        echo json_encode(array('exists' => $exists));
    }

    function edit_area_new()
    {

        $areaname = $this->input->post('edit_areaname');
        $area_id = $this->input->post('edit_areaid');
        $zone_id = $this->input->post('edit_zone_id');
        $description = $this->input->post('edit_description');
        $min_hour = $this->input->post('edit_booking_hour') ?: null;
        $data = array(
            'zone_id' => $zone_id,
            'area_name' => $areaname,
            'area_status' => 1,
            'odoo_area_id' => 0,
            'odoo_new_area_id' => 0,
            'description' => $description,
            'min_booking_hour' => $min_hour,
        );
        $this->settings_model->update_area($data, $area_id);
        echo json_encode(array('success' => true));
    }

    function check_location_name()
    {
        $location_name = $this->input->post('locationname');
        $exists = $this->settings_model->check_location_exists($location_name);
        echo json_encode(array('exists' => $exists));
    }

    function save_location()
    {
        $location_name = $this->input->post('locationname');
        $area_id = $this->input->post('area_id');
        $description = $this->input->post('description');
        $longitude = $this->input->post('longitude');
        $latitude = $this->input->post('latitude');
        $new_min_booking_hour = $this->input->post('new_min_booking_hour') ?: null;
        $data = array(
            'area_id' => $area_id,
            'location_name' => $location_name,
            'updated_at' => date('Y-m-d H:i:s'),
            'description' => $description,
            'longitude' => $longitude,
            'latitude' => $latitude,
            'min_booking_hour' => $new_min_booking_hour,
        );
        $get_area_by_id = $this->settings_model->get_area_details($area_id);
        $location_id = $this->settings_model->add_location($data);
        if (empty($get_area_by_id->odoo_package_area_id)) {
            // echo "odoo_package_area_id is empty or zero.";
            $getodoozone_id = $this->settings_model->get_zone_details($get_area_by_id[0]['zone_id']);
            // $get_area_by_id = $this->settings_model->get_area_details($area_id);

            if (empty($getodoozone_id->odoo_package_zone_id)) {
                $zone_odoo_fields = array();
                $zone_odoo_fields['zone_name'] = $getodoozone_id[0]['zone_name'];
                $zone_odoo_fields['driver_name'] = $getodoozone_id[0]['driver_name'];
                $zone_odoo_fields['description'] = $getodoozone_id[0]['description'];
                $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
                $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

            } 
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
            $zone_area_fields['area_name'] = $get_area_by_id[0]['area_name'];
            $zone_area_fields['area_charge'] = $get_area_by_id[0]['area_charge'];
            $odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area_id);
            $area_location_fields = array();
            $area_location_fields['location_name'] = $location_name;
            $area_location_fields['area_id'] = $odoo_area_id;
            $area_location_fields['description'] = $description;
            $area_location_fields['longitude'] = $longitude;
            $area_location_fields['latitude'] = $latitude;
            $area_location_fields['minimum_hour'] = $new_min_booking_hour;
            $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
            // $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_package_location_status' => 1),$location_id);
        } 
        else {
            // echo "odoo_package_area_id is not empty.";
            $area_location_fields = array();
            $area_location_fields['location_name'] = $location_name;
            $area_location_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
            $area_location_fields['description'] = $description;
            $area_location_fields['longitude'] = $longitude;
            $area_location_fields['latitude'] = $latitude;
            $area_location_fields['minimum_hour'] = $new_min_booking_hour;
            $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
        // $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_package_location_status' => 1),$location_id);
        }
        // print_r($area_location_fields);die();
        $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id,'odoo_new_location_status' => 1),$location_id);
       
        echo json_encode(array('success' => true));
    }

    public function do_location_odoo_api($data = array())
    {
        $post['params']['user_id'] = 1;
        $post['params']['location_name'] = $data['location_name'];
        $post['params']['area_id'] = (int) $data['area_id'];
        $post['params']['description'] = $data['description'];
        $post['params']['longitude'] = $data['longitude'];
        $post['params']['latitude'] = $data['latitude'];
        $post['params']['minimum_hour'] = $data['minimum_hour'];
        $post_values = json_encode($post);
        

        $url = $this->config->item('odoo_url')."location_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_location_id = $returnData->result->response->id;
        }
    }

    function check_location_name_edit()
    {
        $locationname = $this->input->post('locationname');
        $locationid = $this->input->post('edit_locationid');
        $exists = $this->settings_model->check_location_exists_edit($locationname, $locationid);
        echo json_encode(array('exists' => $exists));
    }


    function edit_location_new()
    {
        $location_name = $this->input->post('edit_locationname');
        $location_id = $this->input->post('edit_locationid');
        $area_id = $this->input->post('edit_area_id');
        $description = $this->input->post('edit_description');
        $longitude = $this->input->post('edit_longitude');
        $latitude = $this->input->post('edit_latitude');
        $edit_min_booking_hour = $this->input->post('edit_min_booking_hour');
        $data = array(
            'area_id' => $area_id,
            'location_name' => $location_name,
            'updated_at' => date('Y-m-d H:i:s'),
            'description' => $description,
            'longitude' => $longitude,
            'latitude' => $latitude,
            'min_booking_hour' => $edit_min_booking_hour,
        );
        $this->settings_model->update_location($data, $location_id);
        $get_area_by_id = $this->settings_model->get_area_details($area_id);
        // $location_id = $this->settings_model->add_location($data);
        if ($get_area_by_id[0]['odoo_package_area_id'] == 0) {
            // echo "odoo_package_area_id is empty or zero.";
            $getodoozone_id = $this->settings_model->get_zone_details($get_area_by_id[0]['zone_id']);
            // $get_area_by_id = $this->settings_model->get_area_details($area_id);

            if ($getodoozone_id[0]['odoo_package_zone_id'] == 0) {
                $zone_odoo_fields = array();
                $zone_odoo_fields['zone_name'] = $getodoozone_id[0]['zone_name'];
                $zone_odoo_fields['driver_name'] = $getodoozone_id[0]['driver_name'];
                $zone_odoo_fields['description'] = $getodoozone_id[0]['description'];
                $odoo_zone_id = $this->do_zone_odoo_api($zone_odoo_fields); 
                $this->settings_model->update_zones(array('odoo_package_zone_id' => $odoo_zone_id,'odoo_package_zone_status' => 1),$zone_id);

            } 
            $zone_area_fields = array();
            $zone_area_fields['zone_id'] = $getodoozone_id[0]['odoo_package_zone_id'];
            $zone_area_fields['area_name'] = $get_area_by_id[0]['area_name'];
            $zone_area_fields['area_charge'] = $get_area_by_id[0]['area_charge'];
            $odoo_area_id = $this->do_area_odoo_api($zone_area_fields);
            $this->settings_model->update_area(array('odoo_package_area_id' => $odoo_area_id, 'odoo_package_area_status' => 1), $area_id);
            $area_location_fields = array();
            $area_location_fields['location_name'] = $location_name;
            $area_location_fields['area_id'] = $odoo_area_id;
            $area_location_fields['description'] = $description;
            $area_location_fields['longitude'] = $longitude;
            $area_location_fields['latitude'] = $latitude;
            $area_location_fields['minimum_hour'] = $edit_min_booking_hour;
        } 
        else {
            // echo "odoo_package_area_id is not empty.";
            $area_location_fields = array();
            $area_location_fields['location_name'] = $location_name;
            $area_location_fields['area_id'] = $get_area_by_id[0]['odoo_package_area_id'];
            $area_location_fields['description'] = $description;
            $area_location_fields['longitude'] = $longitude;
            $area_location_fields['latitude'] = $latitude;
            $area_location_fields['minimum_hour'] = $edit_min_booking_hour;
           
        }
        $get_location_by_id = $this->settings_model->get_location_details($location_id);
        if (empty($get_location_by_id->odoo_package_location_id)) {
            $odoo_location_id = $this->do_location_odoo_api($area_location_fields);
            $this->settings_model->update_location(array('odoo_package_location_id' => $odoo_location_id, 'odoo_new_location_status' => 1), $location_id);
        } else {
            $odo_location_affected = $this->update_location_inodoo($area_location_fields, $get_location_by_id->odoo_package_location_id);
        }
        echo json_encode(array('success' => true));
       
    }

    public function update_location_inodoo($data, $location_id)
    {
   
        $post['params']['user_id'] = 1;
        $post['params']['location_id'] = $location_id;
        $post['params']['location_name'] = $data['location_name'];
        $post['params']['area_id'] = (int) $data['area_id'];
        $post['params']['description'] = $data['description'];
        $post['params']['longitude'] = $data['longitude'];
        $post['params']['latitude'] = $data['latitude'];
        $post['params']['minimum_hour'] = $data['minimum_hour'];
        
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') ."location_update";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_location_id = $location_id;
        }
    }
}
