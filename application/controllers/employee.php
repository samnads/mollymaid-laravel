<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Employee extends CI_Controller
{
    public function __construct()
    {
        // error_reporting(E_ALL);
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->library('upload');
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('daily_collection_model');
        $this->load->model('zones_model');
        $this->load->model('call_management_model');
        $this->load->helper('curl_helper');
        $this->load->helper('google_api_helper');
    }
    public function new()
    {
        /************************************************ */
        $data['countries'] = $this->db->select('c.*')->from('countries as c')->order_by('c.id', 'ASC')->get()->result();
        $data['genders'] = $this->db->select('g.*')->from('genders as g')->order_by('g.gender_id', 'ASC')->get()->result();
        $data['services'] = $this->settings_model->get_services_maid();
        $data['skills'] = $this->db->select('s.*')->from('skills as s')->order_by('s.skill_id', 'ASC')->get()->result();
        $data['rating_levels'] = $this->db->select('rl.*')->from('rating_level as rl')->order_by('rl.rating_level_id', 'ASC')->get()->result();
        $data['maid_leaders'] = $this->db->select('ml.*')->from('maids as ml')->where('ml.maid_id = ml.maid_leader_id')->order_by('ml.maid_id', 'ASC')->get()->result();
        $data['locations'] = $this->db->select('l.*')->from('locations as l')->where('l.deleted_at', null)->order_by('l.location_id', 'ASC')->get()->result();
        $data['zones'] = $this->db->select('z.*')->from('zones as z')->order_by('z.zone_id', 'ASC')->get()->result();
        // print_r($data['countries']);die;
        /************************************************ */
        $from_time = DateTime::createFromFormat('H:i:s', "00:00:00");
        $to_time = DateTime::createFromFormat('H:i:s', "23:59:59");
        $i = 0;
        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
            $time_clone = clone $time;
            $i++;
            $data['time_slots'][$time->format('H:i:s')] = $time->format('h:i A');
        }
        /************************************************ */
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('employee/new-employee-form', $data, true);
        $layout_data['page_title'] = 'New Employee';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'jquery.flexdatalist.css', 'toastr.min.css', 'datepicker.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'jquery.flexdatalist.js', 'bootstrap-datepicker.js', 'cropper.js', 'employee.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function edit()
    {
        $employee_id = $this->uri->segment(3);
        /************************************************ */
        // data for prefills
        $data['employee'] = $this->db->select('m.*')->from('maids as m')->where('m.maid_id', $employee_id)->get()->row();
        $service_types = $this->db->select('mstj.*')->from('maid_service_type_joins as mstj')->where('mstj.maid_id', $employee_id)->get()->result();
        $data['service_types'] = array_column($service_types, 'service_type_id');
        $addresses = $this->db->select('ea.*')->from('employee_addresses as ea')->where('ea.maid_id', $employee_id)->get()->result();
        foreach ($addresses as $key => $address) {
            $data['addresses'][$address->address_type_id] = $address;
        }
        $maid_availabilities = $this->db->select('ma.*')->from('maid_availability as ma')->where('ma.employee_id', $employee_id)->where('ma.deleted_at', null)->get()->result();
        foreach ($maid_availabilities as $key => $availability) {
            $data['maid_availabilities'][$availability->week_day] = $availability;
        }
        $data['employee_skills'] = $this->db->select('es.employee_skill_id,es.skill_id,es.rating_level_id,s.skill,rl.rating_level,es.notes')
            ->from('employee_skills as es')->join('skills as s', 's.skill_id = es.skill_id')
            ->join('rating_level as rl', 'es.rating_level_id = rl.rating_level_id')
            ->where('es.employee_id', $employee_id)->where('es.deleted_at', null)->get()->result();
        // print_r($data['employee_skills']);die();
        /************************************************ */

        $data['employee_documents'] = $this->db->select('ed.*,document_types.*')
            ->from('employee_documents as ed')
            ->join('document_types', 'ed.document_type_id =document_types.document_type_id', 'left')
            ->where('ed.employee_id', $employee_id)->where('ed.deleted_at', null)->get()->result();
        // print_r($data['employee_documents']);die();
        // ***********************************************************************************
        $data['countries'] = $this->db->select('c.*')->from('countries as c')->order_by('c.id', 'ASC')->get()->result();
        $data['genders'] = $this->db->select('g.*')->from('genders as g')->order_by('g.gender_id', 'ASC')->get()->result();
        $data['services'] = $this->settings_model->get_services_maid();
        $data['skills'] = $this->db->select('s.*')->from('skills as s')->order_by('s.skill_id', 'ASC')->get()->result();
        $data['rating_levels'] = $this->db->select('rl.*')->from('rating_level as rl')->order_by('rl.rating_level_id', 'ASC')->get()->result();
        $data['maid_leaders'] = $this->db->select('ml.*')->from('maids as ml')->where('ml.maid_id = ml.maid_leader_id')->order_by('ml.maid_id', 'ASC')->get()->result();
        $data['locations'] = $this->db->select('l.*')->from('locations as l')->where('l.deleted_at', null)->order_by('l.location_id', 'ASC')->get()->result();
        $data['zones'] = $this->db->select('z.*')->from('zones as z')->order_by('z.zone_id', 'ASC')->get()->result();
        $data['call_complaint_report'] = $this->call_management_model->get_call_complaints_maid($employee_id);
        /************************************************ */
        $from_time = DateTime::createFromFormat('H:i:s', "00:00:00");
        $to_time = DateTime::createFromFormat('H:i:s', "23:59:59");
        $i = 0;
        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
            $time_clone = clone $time;
            $i++;
            $data['time_slots'][$time->format('H:i:s')] = $time->format('h:i A');
        }
        /************************************************ */
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('employee/edit-employee-form', $data, true);
        $layout_data['page_title'] = 'Edit Employee - ' . $data['employee']->maid_name;
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'toastr.min.css', 'datepicker.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'cropper.js', 'employee-edit.js','jquery-ui.min.js','ajaxupload.3.5.js','bootstrap.min.js', 'main.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function update()
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->trans_begin();
        $employee_id = $this->input->post('employee_id');
        $employee = $this->db->select('m.*')->from('maids as m')->where('m.maid_id', $employee_id)->get()->row();
        $data['maid_name'] = $this->input->post('employee_name');
        $data['maid_full_name'] = $this->input->post('employee_name');
        $data['gender_id'] = $this->input->post('gender_id') ?: null;
        $data['country_id'] = $this->input->post('nationality');
        $data['maid_present_address'] = $this->input->post('present_address');
        $data['maid_permanent_address'] = $this->input->post('permanent_address') ?: null;
        $data['maid_mobile_1'] = $this->input->post('mobile_number_1');
        $data['maid_mobile_2'] = $this->input->post('mobile_number_2');
        $data['maid_whatsapp_no_1'] = $this->input->post('whatsapp_number_1');
        $data['maid_notes'] = $this->input->post('notes');
        $data['maid_last_modified_datetime'] = date('Y-m-d H:i:s');
        if ($this->input->post('employee_avatar_base64')) {
            $data['maid_photo_file'] = upload_base64_image($this->input->post('employee_avatar_base64'), 'employee', 'uploads/images//avatars/employee/');
        }
        $data['maid_joining	'] = $this->input->post('date_of_joining') ? DateTime::createFromFormat('d/m/Y', $this->input->post('date_of_joining'))->format('Y-m-d') : null;
        $data['username'] = $this->input->post('username') ?: null;
        $data['password'] = $this->input->post('password') ?: null;
        $data['maid_priority'] = $this->input->post('priority_number') ?: null;
        $data['employee_type_id'] = $this->input->post('employee_type_id');
        $this->db->where('maid_id', $employee_id);
        $this->db->update('maids', $data);
        $this->db->flush_cache();
        // $odoo_maid_id = $this->do_maids_odoo_api($data);
        $odoo_package_maid_id = $employee->odoo_package_maid_id;
        // print_r($odoo_package_maid_id);die();
                if($odoo_package_maid_id > 0)
                {
                    $odo_employee_affected = $this->update_employee_inodoo($data, $odoo_package_maid_id);
                    // $this->db->where('maid_id', $employee_id);
                    // $this->db->update('maids', array('odoo_package_maid_id' => $odoo_maid_id, 'odoo_package_maid_status' => 1));
                } else {
                    $odoo_maid_id = $this->do_maids_odoo_api($data);
                    if($odoo_maid_id > 0)
                    {
                        $this->db->where('maid_id', $employee_id);
                        $this->db->update('maids', array('odoo_package_maid_id' => $odoo_maid_id, 'odoo_package_maid_status' => 1));
                    } 
                }
        /************************************************** */
        // update addressess
        $address_type_ids = $this->input->post('address_type_ids');
        foreach ($address_type_ids as $key => $address_type_id) {
            $address = array();
            $address['maid_id'] = $employee_id;
            $address['address_type_id'] = $address_type_id;
            $address['country_id'] = $this->input->post('address_type_country_id')[$key] ?: null;
            $address['name'] = $this->input->post('address_type_contact_name')[$key];
            $address['house_number'] = $this->input->post('address_type_house_no')[$key];
            $address['street'] = $this->input->post('address_type_street')[$key];
            $address['town'] = $this->input->post('address_type_town')[$key];
            $address['city'] = $this->input->post('address_type_city')[$key];
            $address['telephone_no_1'] = null;
            $address['mobile_no_1'] = $this->input->post('address_type_mobile_no_1')[$key];
            $address['whatsapp_no_1'] = $this->input->post('address_type_whatsapp_no_1')[$key];
            $address['relationship'] = $this->input->post('address_type_relationship')[$key];
            $this->db->where(array('maid_id' => $employee_id, 'address_type_id' => $address_type_id));
            $query = $this->db->get('employee_addresses');
            $this->db->flush_cache();
            if ($query->num_rows() > 0) {
                $this->db->where(array('maid_id' => $employee_id, 'address_type_id' => $address_type_id));
                $this->db->update('employee_addresses', $address);
            } else {
                $this->db->insert('employee_addresses', $address);
            }
            $this->db->flush_cache();
        }
        /************************************************** */
        // update skills
        $this->db->where(array('employee_id' => $employee_id));
        $this->db->update('employee_skills', array('deleted_at' => date('Y-m-d H:i:s')));
        $this->db->flush_cache();
        if ($this->input->post('skill_ids')) {
            $skill_ids = $this->input->post('skill_ids');
            foreach ($skill_ids as $key => $skill_id) {
                $this->db->where(array('employee_id' => $employee_id, 'skill_id' => $skill_id));
                $query = $this->db->get('employee_skills');
                $this->db->flush_cache();
                $skill['employee_id'] = $employee_id;
                $skill['skill_id'] = $skill_id;
                $skill['rating_level_id'] = $this->input->post('skill_rating_level_ids')[$key];
                $skill['notes'] = $this->input->post('skill_notes')[$key] ?: null;
                $skill['updated_at'] = date('Y-m-d H:i:s');
                $skill['deleted_at'] = null;
                if ($query->num_rows() > 0) {
                    $this->db->where(array('employee_id' => $employee_id, 'skill_id' => $skill_id));
                    $this->db->update('employee_skills', $skill);
                } else {
                    $skill['created_at'] = date('Y-m-d H:i:s');
                    $this->db->insert('employee_skills', $skill);
                }
                $this->db->flush_cache();
            }
        }
        /************************************************** */
        // update services
        $this->db->where('maid_id', $employee_id);
        $this->db->delete('maid_service_type_joins');
        foreach ($this->input->post('services_ids') as $key => $service_id) {
            $service = array();
            $service['maid_id'] = $employee_id;
            $service['service_type_id'] = $service_id;
            $this->db->insert('maid_service_type_joins', $service);
        }
        /************************************************** */
        // update employee documents
        // $document_type_ids = $this->input->post('document_type_ids');
        // log_message('error','post document ids '.json_encode( $document_type_ids));
        // foreach ($document_type_ids as $key => $document_type_id) {
        //     $document = array();
        //     $document['employee_id'] = $employee_id;
        //     $document['document_type_id'] = $document_type_id;
        //     $document['custom_document_name'] = $this->input->post('document_name')[$key];
        //     $document['document_number'] = $this->input->post('document_number')[$key];
        //     $document['expiry_date'] = $this->input->post('document_expiry_date')[$key];
        //     $document['grace_period'] = $this->input->post('grace_period')[$key];
        //     $document['updated_at'] = date('Y-m-d H:i:s');
        //     log_message('error','post document name with key  '.json_encode( $this->input->post('document_name')[$key]));
        //     $test = $_FILES;
        //     log_message('error', '$_FILES array: ' . json_encode($_FILES));
        //     if (!empty($test['document_image']['name'][$key])) {
        //         log_message('error',' not empty test documents');
        //         $original_path = './uploads/employee/documents/';
        //         $extension = pathinfo($test['document_image']['name'][$key], PATHINFO_EXTENSION);
        //         $allowed_extensions = array('jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'pdf');
        //         if (in_array(strtolower($extension), $allowed_extensions)) {
        //             $uniqueid = date('Ymdhis');
        //             $filename = "employee_document_" . $employee_id . "_" . $uniqueid . '.' . $extension;
        //             $config = array(
        //                 'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
        //                 'upload_path' => $original_path,
        //                 'file_name' => $filename,
        //                 'overwrite' => false,
        //             );
        //             $this->upload->initialize($config);
        //             $files = $test['document_image']['name'][$key];
        //             $_FILES['userfile']['name'] = $test['document_image']['name'][$key];
        //             $_FILES['userfile']['type'] = $test['document_image']['type'][$key];
        //             $_FILES['userfile']['tmp_name'] = $test['document_image']['tmp_name'][$key];
        //             $_FILES['userfile']['error'] = $test['document_image']['error'][$key];
        //             $_FILES['userfile']['size'] = $test['document_image']['size'][$key];
        //             if ($this->upload->do_upload()) {
        //                 $img_data = array('upload_data' => $this->upload->data("document_image[" . $key . "]"));
        //                 $document['file_name'] = $img_data['upload_data']['file_name'];
        //             } else {
        //                 log_message('error', 'Upload failed: ' . $this->upload->display_errors());
        //                 $response['status'] = false;
        //                 $response['message'] = $this->upload->display_errors();
        //                 $this->db->trans_rollback();
        //                 die(json_encode($response, JSON_PRETTY_PRINT));
        //             }
        //         } else {
        //             $response['status'] = false;
        //             $response['message'] = 'The uploaded file type is not allowed.';
        //         }
        //         //   $this->db->update('employee_documents', $document);

        //         $this->db->where(array('employee_id' => $employee_id, 'document_type_id' => $document_type_id));
        //         $query = $this->db->get('employee_documents');
        //         log_message('error','query '.$query);
        //         $this->db->flush_cache();
        //         if ($query->num_rows() > 0) {
        //             $this->db->where(array('employee_id' => $employee_id, 'document_type_id' => $document_type_id));
        //             $this->db->update('employee_documents', $document);
        //         } else {
        //             $this->db->insert('employee_documents', $document);
        //         }
        //         $this->db->flush_cache();
        //     }
        // }

        // ****************************************************
        if ($employee->employee_type_id == 1) {
            // if maid employee
            $this->db->where(array('employee_id' => $employee_id));
            $this->db->update('maid_availability', array('deleted_at' => date('Y-m-d H:i:s')));
            $this->db->flush_cache();
            if ($this->input->post('availability_week_days')) {
                foreach ($this->input->post('availability_week_days') as $key => $week_day) {
                    if (isset($this->input->post('availability_checked')[$key]) && $this->input->post('availability_checked')[$key] == "Y") {
                        log_message('error', 'available post zones' . $this->input->post('availability_zone')[$key]);
                        $availability = array();
                        $availability['employee_id'] = $employee_id;
                        $availability['week_day'] = $this->input->post('availability_week_days')[$key];
                        $availability['zone_id'] = $this->input->post('availability_zone')[$key];
                        $availability['time_from'] = $this->input->post('availability_time_from')[$key];
                        $availability['time_to'] = $this->input->post('availability_time_to')[$key];
                        $this->db->where(array('employee_id' => $employee_id, 'week_day' => $week_day));
                        $query = $this->db->get('maid_availability');
                        $this->db->flush_cache();
                        if ($query->num_rows() > 0) {
                            $availability['deleted_at'] = null;
                            $this->db->where(array('employee_id' => $employee_id, 'week_day' => $this->input->post('availability_week_days')[$key]));
                            $this->db->update('maid_availability', $availability);
                        } else {
                            $availability['created_at'] = date('Y-m-d H:i:s');
                            $availability['updated_at'] = date('Y-m-d H:i:s');
                            $this->db->insert('maid_availability', $availability);
                        }
                        $this->db->flush_cache();
                    }
                }
            }
        }
        /************************************************** */
        $respose['status'] = true;
        $respose['message'] = "Employee updated successfully.";
        $this->db->trans_commit();
        die(json_encode($respose, JSON_PRETTY_PRINT));
    }
    public function update_employee_inodoo($data, $employee_id)
    {
        if ($data['employee_type_id'] == 1) {
            $type = 'maid';
        } else if ($data['employee_type_id'] == 2) {
            $type = 'driver';
        } else if ($data['employee_type_id'] == 3) {
            $type = 'accountant';
        } else if ($data['employee_type_id'] == 4) {
            $type = 'mechanic';
        }
		if($data['maid_mobile_2'] == "")
		{
			$work_phone = $data['maid_mobile_1'];
		} else {
			$work_phone = $data['maid_mobile_2'];
		}
		$post['params']['user_id'] = 1;
		$post['params']['id'] = $employee_id;
		$post['params']['name'] = $data['maid_name'];
		$post['params']['complete_name'] = $data['maid_name'];
		$post['params']['mobile_phone'] = $data['maid_mobile_1'];
		$post['params']['type'] = $type;
		$post['params']['work_phone'] = $work_phone;
		// $post['params']['gender'] = $data['gender'];
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "employee_edition";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_employee_id = $employee_id;
        }
    }
    public function save()
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->trans_begin();
        try {
            $data['maid_name'] = $this->input->post('employee_name');
            $data['maid_full_name'] = $this->input->post('employee_name');
            $data['gender_id'] = $this->input->post('gender_id') ?: null;
            $data['country_id'] = $this->input->post('nationality');
            $data['maid_present_address'] = $this->input->post('present_address');
            $data['maid_permanent_address'] = $this->input->post('permanent_address') ?: null;
            $data['maid_mobile_1'] = $this->input->post('mobile_number_1');
            $data['maid_mobile_2'] = $this->input->post('mobile_number_2');
            $data['maid_whatsapp_no_1'] = $this->input->post('whatsapp_number_1');
            $data['maid_notes'] = $this->input->post('notes');
            $data['maid_last_modified_datetime'] = date('Y-m-d H:i:s');
            $data['maid_added_datetime'] = date('Y-m-d H:i:s');
            $data['maid_photo_file'] = null;
            $data['maid_status'] = 1;
            $data['maid_joining	'] = $this->input->post('date_of_joining') ? DateTime::createFromFormat('d/m/Y', $this->input->post('date_of_joining'))->format('Y-m-d') : null;
            $data['username'] = $this->input->post('username') ?: null;
            $data['password'] = $this->input->post('password') ?: null;
            $data['maid_priority'] = $this->input->post('priority_number') ?: null;
            $data['employee_type_id'] = $this->input->post('employee_type_id');
            if ($this->input->post('employee_avatar_base64')) {
                $data['maid_photo_file'] = upload_base64_image($this->input->post('employee_avatar_base64'), 'employee', 'uploads/images//avatars/employee/');
            }
            $this->db->insert('maids', $data);
            $employee_id = $this->db->insert_id();
            if ($employee_id > 0) {
                $odoo_maid_id = $this->do_maids_odoo_api($data);
                if($odoo_maid_id > 0)
                {
                    $this->db->where('maid_id', $employee_id);
                    $this->db->update('maids', array('odoo_package_maid_id' => $odoo_maid_id, 'odoo_package_maid_status' => 1));
                }
                /************************************************** */
                // add skills
                if ($this->input->post('skill_ids')) {
                    $skill_ids = $this->input->post('skill_ids');
                    foreach ($skill_ids as $key => $skill_id) {
                        $this->db->insert('employee_skills', array('employee_id' => $employee_id, 'skill_id' => $skill_id, 'rating_level_id' => $this->input->post('skill_rating_level_ids')[$key], 'notes' => $this->input->post('skill_notes')[$key] ?: null, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')));
                        if ($this->db->insert_id() == 0) {
                            $respose['status'] = false;
                            $respose['message'] = $this->db->_error_message();
                            $this->db->trans_rollback();
                            die(json_encode($respose, JSON_PRETTY_PRINT));
                        }
                    }
                }
                /************************************************** */
                // add multiple addressess
                $address_type_ids = $this->input->post('address_type_ids');
                foreach ($address_type_ids as $key => $address_type_id) {
                    $address = array();
                    $address['maid_id'] = $employee_id;
                    $address['address_type_id'] = $address_type_id;
                    $address['country_id'] = $this->input->post('address_type_country_id')[$key] ?: null;
                    $address['name'] = $this->input->post('address_type_contact_name')[$key];
                    $address['house_number'] = $this->input->post('address_type_house_no')[$key];
                    $address['street'] = $this->input->post('address_type_street')[$key];
                    $address['town'] = $this->input->post('address_type_town')[$key];
                    $address['city'] = $this->input->post('address_type_city')[$key];
                    $address['telephone_no_1'] = null;
                    $address['mobile_no_1'] = $this->input->post('address_type_mobile_no_1')[$key];
                    $address['whatsapp_no_1'] = $this->input->post('address_type_whatsapp_no_1')[$key];
                    $address['relationship'] = $this->input->post('address_type_relationship')[$key];
                    $this->db->insert('employee_addresses', $address);
                    if ($this->db->insert_id() == 0) {
                        $respose['status'] = false;
                        $respose['message'] = $this->db->_error_message();
                        $this->db->trans_rollback();
                        die(json_encode($respose, JSON_PRETTY_PRINT));
                    }
                }
                // ******************************************************
                // save employee documents
                // $document_type_ids = $this->input->post('document_type_ids');
                // foreach ($document_type_ids as $key => $document_type_id) {
                //     $document = array();
                //     $document['employee_id'] = $employee_id;
                //     $document['document_type_id'] = $document_type_id;
                //     $document['custom_document_name'] = $this->input->post('document_name')[$key];
                //     $document['document_number'] = $this->input->post('document_number')[$key];
                //     $document['expiry_date'] = $this->input->post('document_expiry_date')[$key];
                //     $document['grace_period'] = $this->input->post('grace_period')[$key];
                //     $document['created_at'] = date('Y-m-d H:i:s');
                //     $document['updated_at'] = date('Y-m-d H:i:s');
                //     $test = $_FILES;
                //     if (!empty($test['document_image']['name'][$key])) {
                //         $original_path = './uploads/employee/documents/';
                //         $extension = pathinfo($test['document_image']['name'][$key], PATHINFO_EXTENSION);
                //         $allowed_extensions = array('jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'pdf');
                //         if (in_array(strtolower($extension), $allowed_extensions)) {
                //             $uniqueid = date('Ymdhis');
                //             $filename = "employee_document_" . $employee_id . "_" . $uniqueid . '.' . $extension;
                //             $config = array(
                //                 'allowed_types' => 'jpg|jpeg|gif|png|doc|docx|pdf',
                //                 'upload_path' => $original_path,
                //                 'file_name' => $filename,
                //                 'overwrite' => false,
                //             );
                //             $this->upload->initialize($config);
                //             $files = $test['document_image']['name'][$key];
                //             $_FILES['userfile']['name'] = $test['document_image']['name'][$key];
                //             $_FILES['userfile']['type'] = $test['document_image']['type'][$key];
                //             $_FILES['userfile']['tmp_name'] = $test['document_image']['tmp_name'][$key];
                //             $_FILES['userfile']['error'] = $test['document_image']['error'][$key];
                //             $_FILES['userfile']['size'] = $test['document_image']['size'][$key];
                //             if ($this->upload->do_upload()) {
                //                 $img_data = array('upload_data' => $this->upload->data("document_image[" . $key . "]"));
                //                 $document['file_name'] = $img_data['upload_data']['file_name'];
                //             } else {
                //                 $response['status'] = false;
                //                 $response['message'] = $this->upload->display_errors();
                //                 $this->db->trans_rollback();
                //                 die(json_encode($response, JSON_PRETTY_PRINT));
                //             }
                //         } else {
                //             $response['status'] = false;
                //             $response['message'] = 'The uploaded file type is not allowed.';
                //         }
                //         $this->db->insert('employee_documents', $document);
                //         if ($this->db->insert_id() == 0) {
                //             $response['status'] = false;
                //             $response['message'] = $this->db->_error_message();
                //             $this->db->trans_rollback();
                //             die(json_encode($response, JSON_PRETTY_PRINT));
                //         }
                //     }
                // }
                /************************************************** */
                if ($this->input->post('employee_type_id') == 1) {
                    // leader adding
                    /************************************************** */
                    if ($this->input->post('is_maid_leader') == "Y") {
                        $this->db->where('maid_id', $employee_id);
                        $this->db->update('maids', array('maid_leader_id' => $employee_id));
                    } else {
                        $this->db->where('maid_id', $employee_id);
                        $this->db->update('maids', array('maid_leader_id' => $this->input->post('maid_leader_id')));
                    }
                    /************************************************** */
                    // maid availability add
                    foreach ($this->input->post('availability_week_days') as $key => $week_day) {
                        if (isset($this->input->post('availability_checked')[$key]) && $this->input->post('availability_checked')[$key] == "Y") {
                            $availability = array();
                            $availability['employee_id'] = $employee_id;
                            $availability['week_day'] = $this->input->post('availability_week_days')[$key];
                            $availability['zone_id'] = $this->input->post('availability_zone')[$key];
                            $availability['time_from'] = $this->input->post('availability_time_from')[$key];
                            $availability['time_to'] = $this->input->post('availability_time_to')[$key];
                            $availability['created_at'] = date('Y-m-d H:i:s');
                            $availability['updated_at'] = date('Y-m-d H:i:s');
                            $this->db->insert('maid_availability', $availability);
                            if ($this->db->insert_id() == 0) {
                                $respose['status'] = false;
                                $respose['message'] = $this->db->_error_message();
                                $this->db->trans_rollback();
                                die(json_encode($respose, JSON_PRETTY_PRINT));
                            }
                        }
                    }
                    /************************************************** */
                    // maid service type add
                    foreach ($this->input->post('services_ids') as $key => $service_id) {
                        $service = array();
                        $service['maid_id'] = $employee_id;
                        $service['service_type_id'] = $service_id;
                        $this->db->insert('maid_service_type_joins', $service);
                        if ($this->db->insert_id() == 0) {
                            $respose['status'] = false;
                            $respose['message'] = $this->db->_error_message();
                            $this->db->trans_rollback();
                            die(json_encode($respose, JSON_PRETTY_PRINT));
                        }
                    }
                }
                /************************************************** */
                $respose['status'] = true;
                $respose['message'] = "Employee saved successfully.";
                $this->db->trans_commit();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            } else {
                $respose['status'] = false;
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
        }
        die(json_encode($respose, JSON_PRETTY_PRINT));
    }
	public function do_maids_odoo_api($data = array())
    {
        if ($data['employee_type_id'] == 1) {
            $type = 'maid';
        } else if ($data['employee_type_id'] == 2) {
            $type = 'driver';
        } else if ($data['employee_type_id'] == 3) {
            $type = 'accountant';
        } else if ($data['employee_type_id'] == 4) {
            $type = 'mechanic';
        }
        // $get_flat_odoo_id = $this->settings_model->get_flat_details($data['flat_id']);
		if($data['maid_mobile_2'] == "")
		{
			$work_phone = $data['maid_mobile_1'];
		} else {
			$work_phone = $data['maid_mobile_2'];
		}
		$post['params']['user_id'] = 1;
		$post['params']['name'] = $data['maid_name'];
		$post['params']['complete_name'] = $data['maid_name'];
		$post['params']['mobile_phone'] = $data['maid_mobile_1'];
		$post['params']['type'] = $type;
		$post['params']['work_phone'] = $work_phone;
		// $post['params']['flat_id'] = $get_flat_odoo_id[0]['odoo_package_flat_id'];
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "employee_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);
        if ($returnData->result->status == "success") {
            return $odoo_employee_id = $returnData->result->response->id;
        }
    }
    public function validatemobilenumber()
    {
        if (array_key_exists('mobile_number_1', $_POST)) {
            $mobile_number_1 = $_POST["mobile_number_1"];
            $this->load->model("maids_model");
            $this->maids_model->verifyMobilenumberExist($mobile_number_1);
            if ($this->maids_model->verifyMobilenumberExist($mobile_number_1) == TRUE) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function editvalidatemobilenumber()
    {
        if (array_key_exists('mobile_number_1', $_POST)) {
            $mobile_number_1 = $_POST["mobile_number_1"];
            $id = $_POST["id"];
            $this->load->model("maids_model");
            $result = $this->maids_model->verifyMobilenumberEditExist($mobile_number_1, $id);
            if ($result == TRUE) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function validate_adress_type_mobilenumber()
    {
        if (array_key_exists('phone', $_POST)) {
            $mobile_number_1 = $_POST["phone"];
            $this->load->model("employee_model");
            $this->employee_model->verifyMobilenumberExist($mobile_number_1);
            if ($this->employee_model->verifyMobilenumberExist($mobile_number_1) == TRUE) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }
    public function edit_validate_adress_type_mobilenumber()
    {
        if (array_key_exists('phone', $_POST)) {
            $mobile_number_1 = $_POST["phone"];
            $id = $_POST["id"];
            $this->load->model("employee_model");
            $result =  $this->employee_model->verifyMobilenumberEditExist($mobile_number_1, $id);
            if ($result == TRUE) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    // *************************************************************************************
    public function daily_collection()
    {
        $data['post'] = $this->input->post(NULL, TRUE);

        if (!empty($data['post'])) {
            // log_message('error', 'post datas' . json_encode($data['post']));
            $filter_date = $data['post']['filter_date'];
            $maid_id = $data['post']['filter_maid'];
            $status = $data['post']['collection_status'];
            $leader_id = $data['post']['filter_leader'];
            $data['collection_datas'] = $this->daily_collection_model->get_collection_data($filter_date, $maid_id, $status);
            $data['leader_name']  = $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_id', $leader_id)->get()->row();
            $data['maid_name']  = $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.maid_id', $maid_id)->get()->row();
            // print_r($data['collection_datas']);die;
        }

        $data['employees'] = $this->daily_collection_model->get_all_maids_new();
        // print_r($data['employees']);die;
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('employee_daily_collection', $data, true);
        $layout_data['page_title'] = 'Employees Daily Collection';
        $layout_data['meta_description'] = 'Employees Collection';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'FileSaver.js', 'xlsx.full.min.js', 'daily_collection.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'moment.min.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    // *********************************************************************************************************
    public function get_leader_maid()
    {
        $post = $this->input->post(NULL, TRUE);
        if ($this->input->is_ajax_request()) {
            // log_message('error', 'post datas' . json_encode($post));
            $leader_id = $post['leader_id'];
            $data['response'] = $this->daily_collection_model->get_all_leader_maid($leader_id);

            echo json_encode($data['response']);
        }
    }

    // **************************************************************************************************
}
