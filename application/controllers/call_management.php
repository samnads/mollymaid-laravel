<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Call_management extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->library('form_validation');
        $this->load->model('settings_model');
        $this->load->model('call_management_model');
        $this->load->model('customer_mollymaid_model');
        $this->load->model('maids_model');
        $this->load->model('bookings_model');
        $this->load->helper('booking_helper');
        $this->load->model('schedule_model');
        

    }

    public function call_list()
    {
        $data['settings'] = $this->settings_model->get_settings();
        $data['zones'] = $this->call_management_model->get_zones();
        $data['booking_types'] = $this->call_management_model->get_booking_types();
        $data['maids'] = $this->call_management_model->get_pref_maids();
        $data['pref_maids'] = $this->db->select('m.maid_id,m.maid_name')->from('maids as m')->where('m.maid_status', 1)->where('m.employee_type_id', 1)->order_by('m.maid_id', 'ASC')->get()->result();
        $data['services'] = $this->settings_model->get_services_maid();
        $data['areas'] = $this->call_management_model->get_areas();
        $data['call_lists'] = $this->call_management_model->get_call_list();
        $data['refer_sources'] = $this->customer_mollymaid_model->get_customer_sources();
        $data['cancel_reasons'] = $this->schedule_model->get_cancel_resons();
        $data['suspend_reasons'] = $this->schedule_model->get_suspend_resons();
        //         echo'<pre>';
        // print_r($data['call_lists']);die();
        // $from_time = DateTime::createFromFormat('H:i:s', "00:00:00");
        // $to_time = DateTime::createFromFormat('H:i:s', "23:59:59");
        $from_time = DateTime::createFromFormat('H:i:s', "08:00:00");
        $to_time = DateTime::createFromFormat('H:i:s', "19:30:00");
        $i = 0;
        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
            $time_clone = clone $time;
            $i++;
            $data['time_slots'][$time->format('H:i:s')] = $time->format('h:i A');
        }
        $data['residence_types'] = $this->call_management_model->residence_types();
        $data['enquiry_types'] = $this->call_management_model->enquiry_types();
        $data['complaint_types'] = $this->call_management_model->complaint_types();
        $data['complaint_statuses'] = $this->call_management_model->complaint_statuses();
       
        $layout_data['content_body'] = $this->load->view('call-management/call-list', $data, true);
        $layout_data['page_title'] = 'Call Management';
        $layout_data['meta_description'] = 'Call Management';
        $layout_data['css_files'] = array('jquery.flexdatalist.css', 'datepicker.css', 'toastr.min.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.flexdatalist.js', 'bootstrap-datepicker.js', 'cropper.js', 'jquery.dataTables.min.js','call-management.js');
        // $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js');
        
        $this->load->view('layouts/default', $layout_data);
    }
    public function call_details()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        $customer_id = $post['cust_id'];
        $data['call_lists'] = $this->call_management_model->get_call_list_for_sel_customer($customer_id);
        $call_list_details = $this->load->view('call-management/call-list-details-by-customer', $data, TRUE);
        echo $call_list_details;
        exit();  
    }
    public function getLocations()
    {
        
        $areaId = $this->input->post('area_id');
        $locations = $this->call_management_model->getLocationsByArea($areaId);
        echo json_encode($locations);
    }

    public function customers_search()
    {
        header('Content-Type: application/json; charset=utf-8');
        $query = $this->input->post('query');
        $search_result = $this->call_management_model->customers_search($this->input->post('query'));
        $data = array();
        foreach ($search_result as $user) {
            if (strpos($user->mobile_number_1, $query) !== false) {
                $userdetail = $user->customer_name . ',  ' . $user->mobile_number_1;
            } else if (strpos($user->customer_code, $query) !== false) {
                $userdetail = $user->customer_name . ',  ' . $user->customer_code;

            } else {
                $userdetail = $user->customer_name . ',  ' . $user->mobile_number_1;
            }
            // $userdetail = $user->customer_name . ',  ' . $user->mobile_number_1;
            // $userdetail = $user->customer_name;
            $data[] = array("id" => $user->customer_id, "text" => nl2br($userdetail));
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
    
    public function getlandmark()
    {
        $locationId = $this->input->post('location_id');
        $landmarks = $this->call_management_model->getLandmarkByLocation($locationId);
        echo json_encode($landmarks);
    }

    public function customersave()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $data['customer_type_id'] = $post['customer_type_id'];
            $data['customer_company_id'] = null;
            $data['customer_dealer_id'] = null;
            
            $data['customer_name'] = $post['customer_name'];
            $data['mobile_number_1'] = $post['customer_mobilenumber'];
            $data['email_address'] = $post['customer_email'] ?: null;
            $data['payment_frequency_id'] = 1;
            $data['payment_mode_id'] = 1;
            $data['created_by_user'] = user_authenticate();
            $data['created_at'] = $data['updated_at'] = $data['customer_added_datetime'] = $data['customer_last_modified_datetime'] = date('Y-m-d H:i:s');
            // $preferred_maids = explode(",", $post['pref_maids']);
            $customer_id = $this->customer_mollymaid_model->save_customer($data);
            if ($customer_id > 0) {
                /****************************************************** */
                //foreach ($this->input->post('address_zone_ids') as $key => $zone_id) {
                    $address_data['customer_id'] = $customer_id;
                    $address_data['residence_type'] = $post['customer_residence_type'];
                    $address_data['location_id'] = $post['customer_location'] ?: null;
                    $address_data['area_id'] = $post['customer_area_id'] ?: null;
                    $address_data['customer_address'] = $post['customer_address'];
                    
                    $address_data['landmark_id'] = $post['customer_landmark'] ?: null;
                    
                    $address_data['address_category'] = null;
                    $address_data['address_status'] = 0; // 0 means not deleted :/
                    $address_data['home_type'] = null;
                    // print_r($address_data);die();
                    $address_id = $this->customer_mollymaid_model->save_customer_address($address_data);
                    if ($address_id > 0) {
                        $address_ids = $address_id;
                    } else {
                        $respose['status'] = "failed";
                        $respose['message'] = $this->db->_error_message();
                        $this->db->trans_rollback();
                        die(json_encode($respose, JSON_PRETTY_PRINT));
                    }
                
                $this->customer_mollymaid_model->update_customer($customer_id, array('customer_code' => "MM-" . sprintf('%05d', $customer_id) . "-" . date('m-Y'), 'default_booking_address' => $address_ids));
                $this->db->trans_commit();
                $respose['status'] = "success";
                $respose['message'] = "Customer saved successfully.";
                die(json_encode($respose, JSON_PRETTY_PRINT));
            } else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }

    }

    public function get_customer_details()
    {
        $customer_id = $this->input->post('cust_id');
        $data['customer_details'] = $this->call_management_model->get_customer_details($customer_id);
        $data['customer_address'] = $this->call_management_model->get_customer_address($customer_id);
        $data['no_of_address'] = count($data['customer_address']);
        $data['areas'] = $this->settings_model->get_areas();
        echo json_encode($data);

    }

    public function get_customer_address_details()
    {
        $customer_id = $this->input->post('customer_id');
        $data['customer_address'] = $this->call_management_model->get_customer_address($customer_id);
        echo json_encode($data);

    }

    public function save_enquiry()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        // print_r($post);die();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $is_maidflagged = $this->schedule_model->is_maidflagged($post['maidId'], $post['enquiry_customer_id']);
            // print_r($is_maidflagged);die();

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $call_history_data['customer_id']    = $post['enquiry_customer_id'];
            $call_history_data['mobile_number']   = $post['enquiry_customer_phoneno'] ?: null;
            $call_history_data['enquiry_type_id'] = $post['caller_enquiry_typ_id'] ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            if ($post['availability_week_days']) {
                foreach ($post['availability_week_days'] as $key => $week_day) {
                    if (isset($post['availability_checked'][$key]) && $post['availability_checked'][$key] == "Y") {
                        $enquiry_data = array();
                        $enquiry_data['enquiry_week_day '] = $post['availability_week_days'][$key];
                        $enquiry_data['call_history_id'] = $call_history_id;
                        $enquiry_data['enquiry_type_id'] = $post['caller_enquiry_typ_id'];
                        $enquiry_data['customer_id'] = $post['enquiry_customer_id'];
                        $enquiry_data['date_from'] = $post['enquiry_booking_date'] ? DateTime::createFromFormat('d/m/Y', $post['enquiry_booking_date'])->format('Y-m-d') : null;
                        $enquiry_data['time_from'] = $post['availability_time_from'][$key];
                        $enquiry_data['time_to'] = $post['availability_time_to'][$key];
                        $enquiry_data['total_hours'] = $post['working_hour'][$key];
                        $enquiry_data['enquiry_customer_name'] = $post['enquiry_customer_name'];
                        $enquiry_data['enquiry_customer_phoneno'] = $post['enquiry_customer_phoneno'] ?: null;
                        $enquiry_data['enquiry_customer_email'] = $post['enquiry_customer_email'] ?: null;
                        $enquiry_data['service_type_id'] = $post['service_id'] ?: null;
                        $enquiry_data['refer_source_id'] = $post['enquiry_source_id'] ?: null;
                        if (isset($post['outside_service_checked']) && $post['outside_service_checked'] == "Y") {
                            $customer_service_area = $post['outside_service_area'];
                        }
                        if (isset($post['outside_new_area'])) {
                            $new_service_area = $post['outside_new_area'];
                        }
                        $enquiry_data['new_service_area'] = $new_service_area ?: null;
                        $enquiry_data['customer_service_area'] = $customer_service_area ?: 0;
                        $enquiry_data['area_id'] = $post['area_id'] ?: null;
                        $enquiry_data['location_id'] = $post['location_id'] ?: null;
                        $enquiry_data['maid_id'] = $post['maidId'] ?: null;
                        $enquiry_data['enquiry_status'] = "Saved" ?: null;
                        $enquiry_data['booking_type'] = $post['booking_type_enquiry'] ?: null;
                        $enquiry_data['created_by_user'] = user_authenticate();
                        $enquiry_data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
                        $enquiry_data['enquiry_notes'] = $post['enquiry_notes'];
                        
                        // $this->call_management_model->save_service($enquiry_data);
                        $this->db->insert('service_enquiry', $enquiry_data);
                    }  
                    // print_r($enquiry_data);
 
                }
            }
            $this->db->trans_commit();
            $respose['status'] = "success";
            $respose['message'] = "Enquiry saved successfully.";
            die(json_encode($respose, JSON_PRETTY_PRINT));
           
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }

    }

    public function validatemobilenumber()
    {
        if (array_key_exists('customer_mobilenumber',$_POST))
        {
            $customer_mobilenumber=$_POST["customer_mobilenumber"];
           // $this->load->model("maids_model");
            $this->call_management_model->verifyMobilenumberExist($customer_mobilenumber);
            if ($this->call_management_model->verifyMobilenumberExist($customer_mobilenumber) == TRUE)
            {
                echo json_encode(false);
            }
            else
            {
                echo json_encode(true);
            }
        }
    }

    public function validateemail()
    {
        if (array_key_exists('customer_email',$_POST))
        {
            $customer_email=$_POST["customer_email"];
           // $this->load->model("maids_model");
            $this->call_management_model->verifyEmailExist($customer_email);
            if ($this->call_management_model->verifyEmailExist($customer_email) == TRUE)
            {
                echo json_encode(false);
            }
            else
            {
                echo json_encode(true);
            }
        }
    }

    public function getemployeedetails()
    {
        $employee_type_id = $this->input->post('employee_type_id');
        $employee = $this->call_management_model->getEmployeedetails($employee_type_id);
        echo json_encode($employee);
    }

    public function save_complaint()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $call_history_data['customer_id']    = $post['complainer_id'];
            $call_history_data['mobile_number']   = $post['complainer_phone'] ?: null;
            $call_history_data['enquiry_type_id'] = $post['caller_complaints_typ_id'] ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            if ($call_history_id > 0) {
                $complaint_data['customer_id'] = $post['complainer_id'] ?: null;
                $complaint_data['complaint_time'] = $post['complaint_time'] ?: null;
                $complaint_data['service_date'] = $post['complaint_date'] ?: null;
                $complaint_data['complaint_type'] = $post['complaint_type'] ?: null;
                $complaint_data['existing_customer'] = $post['existing_customer'] ?: null;
                $complaint_data['complaint_against'] = $post['complaint_against'] ?: null;
                $complaint_data['complaint_against_id'] = $post['employee_name'] ?: null;
                $complaint_data['complaint_against_name'] = $post['complaint_against_name'] ?: null;
                $complaint_data['complaint'] = $post['complaint'] ?: null;
                $complaint_data['action_taken'] = $post['action_taken'] ?: null;
                $complaint_data['status'] = $post['complaint_status'] ?: null;
                $complaint_data['call_history_id'] = $call_history_id;
                $complaint_data['added_by'] = user_authenticate();
                // print_r($complaint_data);die();

                $complaint_id = $this->call_management_model->save_complaint($complaint_data);

                $complaint_no =  "MM-C".$complaint_id ."-" . date('d-m-Y') ?: null;
                $this->call_management_model->update_complaint($complaint_id, array(
                    'complaint_no' => $complaint_no,
                ));
                $this->db->trans_commit();
                $respose['status'] = "success";
                $respose['message'] = "Complaint saved successfully.";
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
            else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }   
        }
        catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    } 

    public function save_preference()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $is_maidflagged = $this->schedule_model->is_maidflagged($post['preference_maidId'], $post['customer_preference_id']);

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $call_history_data['customer_id']    = $post['customer_preference_id'] ?: null;
            $call_history_data['mobile_number']   = $post['customer_preference_phone'] ?: null;
            $call_history_data['enquiry_type_id'] = $post['caller_preferences_typ_id'] ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            if($call_history_id > 0) {
                $preference_data['call_history_id'] = $call_history_id;
                $preference_data['customer_id'] = $post['customer_preference_id'] ?: null;
                $preference_data['booking_type'] = $post['preference_booking_type'] ?: null;
                $preference_data['maid_id'] = $post['preference_maidId'] ?: null;
                $preference_data['service_type_id'] = $post['preference_service_id'] ?: null;
                $preference_data['area_id'] = $post['preference_area_id'] ?: null;
                $preference_data['location_id'] = $post['preference_location'] ?: null;
                $preference_data['date_from'] = $post['preference_date_from'] ? DateTime::createFromFormat('d/m/Y', $post['preference_date_from'])->format('Y-m-d') : null;
                $preference_data['date_to'] = $post['preference_date_to'] ? DateTime::createFromFormat('d/m/Y', $post['preference_date_to'])->format('Y-m-d') : null;
                $preference_data['time_from'] = $post['preference_time_from'] ?: null;
                $preference_data['time_to'] = $post['preference_time_to'] ?: null;
                $preference_data['created_by_user'] = user_authenticate();
                $preference_data['created_at'] = $preference_data['updated_at'] = date('Y-m-d H:i:s');

                $this->call_management_model->save_preference($preference_data);
                $this->db->trans_commit();
                $respose['status'] = "success";
                $respose['message'] = "Preference saved successfully.";
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
            else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }   
        }
        catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    } 
    
    public function search_booking_maid()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $service_date = $post['booking_date'] ? DateTime::createFromFormat('d/m/Y', $post['booking_date'])->format('Y-m-d') : null;
            $customer_id = $post['booking_customer_ids'];
            $customer_name = $post['booking_customer_name'];
            
            $from_time = $this->input->post('availability_time_from');
            $book_type = $this->input->post('booking_type');
            $to_time = $this->input->post('availability_time_to');
            $filter = array();
            $filter['customer_id'] = $customer_id;
            $filter['service_date'] = $service_date;
            $zone_id = $this->input->post('booking_zone_id');
            $filter['zone_id'] = $zone_id;
            $filter['from_time'] = $from_time;
            $filter['to_time'] = $to_time;
            // $zone_id = $this->input->post('enquiry_zone_id');
            $maid_id = $this->input->post('booking_maid_id');
            $filter['maid_id'] = $maid_id;
            $data['service_date'] = $service_date;
            $data['customer_phoneno'] = $this->input->post('booking_customer_phoneno');
            $data['caller_booking_typ_id'] = $this->input->post('caller_booking_typ_id');
            $data['booking_customer_address_id'] = $this->input->post('booking_customer_address_id');
            $data['customer_name'] = $customer_name;
            $data['from_time'] = $from_time;
            $data['to_time'] = $to_time;
            $data['customer_id'] = $customer_id;
            $data['zone_id'] = $zone_id;
            $data['new_book_type'] = $book_type;
            // $search_maids = $this->maids_model->get_all_maids_active($filter);
            $flagged_maids = $this->schedule_model->flagged_maids($customer_id);
            $search_maids = $this->maids_model->get_all_maids_active_enquiry($filter);
            $flagged_ids = array_map(function($maid) {
                return $maid->maid_id;
            }, $flagged_maids);
            
            // Filter out flagged maids from search_maids
            $filtered_maids = array_filter($search_maids, function($maid) use ($flagged_ids) {
                return !in_array($maid->maid_id, $flagged_ids);
            });
            
            // Reset the array keys (optional)
            $search_maids = array_values($filtered_maids);
            
      
            $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
            $leave_maid_ids = array();
            foreach ($maids_leave_on_date as $leave) {
                array_push($leave_maid_ids, $leave->maid_id);
            }
            $time_from_stamp = $from_time;
            $time_to_stamp = $to_time;
            $maid_array = array();
            $maids_det = array();
            if ($book_type == 'OD') {
                $today_bookingss = $this->bookings_model->get_schedule_by_date_avail($service_date);
            }
    
            if ($book_type == 'BW' || $book_type == 'WE') {
                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day);
            }
            // $time=$this->bookings_model->get_shifts_maid_new($service_date,$list->maid_id);
            // $begin = DateTime::createFromFormat('H:i:s', "08:00:00");
            // $end = DateTime::createFromFormat('H:i:s', "19:59:59");
            // $timeRanges = [];
            // while($begin < $end) {
            
            //     $output = $begin->format('h:i A')."-";
            //     $begin->modify('+30 minutes');          /** Note, it modifies time by 15 minutes */
            //     $output .= $begin->format('h:i A');
            
            //     $timeRanges[] = $output;
            // }
            
            foreach ($search_maids as $s_maids) {

                $today_week_day = date('w', strtotime($service_date));
                //One day
                if ($book_type == 'OD') {
                    //$time_from_stamp = strtotime(trim($booking->time_from));
                    //$time_to_stamp = strtotime(trim($booking->time_to));
    
                    // $today_bookingss = $this->bookings_model->get_schedule_by_date_avail($service_date); habeeb moved this out of loop
                    // echo $service_date;
                    // exit();
                    foreach ($today_bookingss as $t_booking) {
                        $booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);
    
                    }
    
                    //$maids_s = $this->maids_model->get_maids();
    
                    //foreach($maids_s as $smaid)
                    //{
                    if (isset($booked_slots[$s_maids->maid_id]['time'])) {
                        foreach ($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time) {
                            if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                //array_push($nf_maids, $smaid->maid_id);
                                array_push($maids_det, $s_maids->maid_id);
                            }
                        }
                    }
                    //}
                } else if ($book_type == 'BW' || $book_type == 'WE') {
                    if ($repeat_day < $today_week_day) {
                        $day_diff = (6 - $today_week_day + $repeat_day + 1);
                    } else {
                        $day_diff = $repeat_day - $today_week_day;
                    }
                    $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                    //echo $repeat_day;
                    //$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day); habeeb moved this out of loop
                    //                    echo '<pre>';
                    //                    print_r($bookings_on_day);
                    //exit();
                    if (!empty($bookings_on_day)) {
                        foreach ($bookings_on_day as $booking_on_day) {
                            $s_date_stamp = strtotime($service_start_date);
                            $e_date_stamp = strtotime($service_start_date);
                            $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                            $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
    
                            if ($booking_on_day->booking_type == "BW" || $booking_on_day->booking_type == "WE") {
                                $now = strtotime($service_start_date);
                                $your_date = strtotime($booking_on_day->service_start_date);
                                $datediff = $now - $your_date;
                                //echo $booking_on_day->service_start_date;
                                $week_diff = round($datediff / (60 * 60 * 24));
                                $week_difference = fmod($week_diff, 14);
                                //echo $week_difference;
                                if ($week_difference == 0 || $week_difference == '-0') {
                                    $f_time = strtotime($booking_on_day->time_from);
                                    $t_time = strtotime($booking_on_day->time_to);
    
                                    if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                        array_push($maids_det, $s_maids->maid_id);
                                    } else {
    
                                    }
                                }
                            } /* else {
                             if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                             //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                             {
                             $f_time = strtotime($booking_on_day->time_from);
                             $t_time = strtotime($booking_on_day->time_to);
                             if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) )//|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                             {
                             array_push($maids_det, $s_maids->maid_id);
                             } else {
                             }
                             }
                             }*/
                        }
                    } else {
    
                    }
                }
            }
            $service_week_day = date('w', strtotime($service_date));
            $data['maids'] = $search_maids;
            $data['service_week_day'] = $service_week_day;
            $data['n_maids'] = $maids_det;
            $data['leave_maid_ids'] = $leave_maid_ids;
            $data['flagged_maid_ids'] =  $flagged_maid_ids;
           
            //          echo $this->db->last_query();
            // print_r($data);die();
            $data['settings'] = $this->settings_model->get_settings();
            $maids = $this->load->view('call-management/maid-view-list', $data, TRUE);
            echo $maids;
            exit();
        } catch (Exception $e) {
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }
    }
    public function save_booking()
    {
        // print_r("hiii");die();
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
       
            $service_date = $post['booking_date'] ? DateTime::createFromFormat('d/m/Y', $post['booking_date'])->format('Y-m-d') : null;
            $customer_id = $post['booking_customer_id'];
            $filter = array();
            $filter['customer_id'] = $customer_id;
            $filter['service_date'] = $service_date;
            $search_maids = $this->call_management_model->get_all_maid($filter);
            $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
            $leave_maid_ids = array();
            foreach ($maids_leave_on_date as $leave) {
                array_push($leave_maid_ids, $leave->maid_id);
            }
        // showPrepareTimeError($this->input->post('booking_date'), $this->input->post('from_time'),$this->input->post('booking_type'),[],'html');

            $maid_id = $post['booking_maid_id'];
            
            if ($post['booking_type']== 'OD') {
                $today_week_day = date('w', strtotime($service_date));
                // showPrepareTimeError($post['booking_date'], $post['booking_time_from'],$post['booking_type'],[],'html');
                $today_bookingss  = $this->call_management_model->get_schedule_by_date_avail($service_date);
                $data['service_week_day'] = $today_week_day;
             
            } else {
                if ($post['availability_week_days']) {
                    foreach ($post['availability_week_days'] as $key => $week_day) {
                        if (isset($post['availability_checked']) && $post['availability_checked'][$key] == "Y") {
                            $service_time[] = $post['availability_time_from'][$key];
                        
                            $service_week_day[] = $week_day;
                    
                        }  
                    }
                    // showPrepareTimeError($post['booking_date'], $service_time,$post['booking_type'],$service_week_day,'html');

                    // print_r($service_time);
                }
                
                $deletes = $this->call_management_model->get_booking_deletes_by_date($service_date);

                $deleted_bookings = array();
                foreach ($deletes as $delete) {
                    $deleted_bookings[] = $delete->booking_id;
                }

                $this->db->select("b.booking_id, b.justmop_reference, b.customer_id, c.customer_name,b.customer_address_id, b.maid_id, b.service_type_id, b.service_start_date, b.service_week_day, b.is_locked, b.cleaning_material, DATE_FORMAT(b.time_from, '%H:%i') AS time_from, DATE_FORMAT(b.time_to, '%H:%i') AS time_to, b.booking_type, b.service_end, b.service_end_date, b.service_actual_end_date, b.booking_note, b.is_locked, b.pending_amount, b.discount, b.total_amount, b.booking_status, c.customer_name, c.price_hourly, c.balance, c.signed, c.customer_nick_name, c.mobile_number_1, c.key_given, c.payment_type, c.longitude, c.latitude, c.customer_source, z.zone_id, z.zone_name, a.area_name, ca.customer_address, m.maid_name, m.maid_nationality, m.maid_mobile_1, m.maid_photo_file,  u.user_fullname, TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes", FALSE)
                ->from('bookings b')
                ->join('customers c', 'b.customer_id = c.customer_id')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
                ->join('areas a', 'ca.area_id = a.area_id')
                ->join('zones z', 'a.zone_id = z.zone_id')
                ->join('users u', 'b.booked_by = u.user_id', 'left')
                ->where('b.booking_status', 1)
                ->where('b.booking_category', 'C')
                ->where('a.deleted_at', null)
                ->where('z.zone_status', 1)
                ->where('m.maid_status', 1)
                ->where('m.maid_id', $maid_id)
                ->where_in('service_week_day', $service_week_day)
                ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND service_end = 1) OR (service_end = 0))", NULL, FALSE)
                ->order_by('b.time_from');
                // $query = $this->db->get();
                if (count($deleted_bookings) > 0) {
                    $this->db->where_not_in('b.booking_id', $deleted_bookings);
                }
                $bookings_on_day =$this->db->get()->result();
                // print_r($get_schedule_by_date_qry);die();
                // $respose['status'] = "success";
                // // $respose['message'] = "Booking saved successfully.";
                // die(json_encode($respose, JSON_PRETTY_PRINT));
                $data['service_week_day'] = $service_week_day;


            }
            foreach ($search_maids as $s_maids) {
                $today_week_day = date('w', strtotime($service_date));

            if ($post['booking_type']== 'OD') {
                $time_from_stamp = $post['booking_time_from'];
                $data['from_time'] = $post['booking_time_from'];
                $data['to_time'] = $post['booking_time_to'];
                $time_to_stamp = $post['booking_time_to'];
                $maid_array = array();
                $maids_det = array();
                foreach ($today_bookingss as $t_booking) {
                    $booked_slots[$t_booking->maid_id]['time'][] = strtotime($t_booking->time_to);

                }
                //  print_r($booked_slots[$t_booking->maid_id]['time']);
                if (isset($booked_slots[$t_booking->maid_id]['time'])) {
                    foreach ($booked_slots[$t_booking->maid_id]['time'] as $f_time => $t_time) {
                        if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                            //array_push($nf_maids, $smaid->maid_id);
                            array_push($maids_det, $s_maids->maid_id);
                        }
                    }
                }
            // print_r($maids_det);

            }
            else if ($post['booking_type'] == 'WE') {
                foreach ($post['availability_week_days'] as $key => $week_day) {
                    if (isset($post['availability_checked']) && $post['availability_checked'][$key] == "Y") {
                        $time_from_stamp = $post['availability_time_from'][$key];
                        $time_to_stamp = $post['availability_time_to'][$key];
                    // print_r($week_day);
                        $service_week_day = $week_day;
                        $service_week_days[] = $post['availability_week_days'][$key];
                        $data['service_week_day'] = $service_week_days;
                        if ($service_week_day <= $today_week_day) {
                            // print_r("helloo");die();
                                $day_diff = (6 - $today_week_day + $service_week_day + 1);
                            } else {
                                $day_diff = $service_week_day - $today_week_day;
                            } 
                        $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                        if (!empty($bookings_on_day)) {
                            foreach ($bookings_on_day as $booking_on_day) {
                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_start_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
        
                                if ($booking_on_day->booking_type == "BW" || $booking_on_day->booking_type == "WE") {
                                    $now = strtotime($service_start_date);
                                    $your_date = strtotime($booking_on_day->service_start_date);
                                    $datediff = $now - $your_date;
                                    //echo $booking_on_day->service_start_date;
                                    $week_diff = round($datediff / (60 * 60 * 24));
                                    $week_difference = fmod($week_diff, 14);
                                    //echo $week_difference;
                                    if ($week_difference == 0 || $week_difference == '-0') {
                                        $f_time = strtotime($booking_on_day->time_from);
                                        $t_time = strtotime($booking_on_day->time_to);
        
                                        if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                            array_push($maids_det, $s_maids->maid_id);
                                        } else {
        
                                        }
                                    }
                                } 
                            }
                        }
                    }  
                    // print_r($service_time);die();
                  

                }
            
              
            }
            }
            $data['new_book_type'] = $post['booking_type'];
            $data['service_date'] = $service_date;
            $data['maids'] = $search_maids;
            $data['n_maids'] = $maids_det;
            //   print_r($data);die();
            $data['leave_maid_ids'] = $leave_maid_ids;
            // print_r($data);die();
            //          echo $this->db->last_query();
            $data['settings'] = $this->settings_model->get_settings();
            $maids = $this->load->view('call-management/maid-view-list', $data, TRUE);
            echo $maids;
            exit();
           
        } catch (Exception $e) {
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }

    }

    public function search_booking()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $enquiry_type_id = $post['caller_suspend_service_typ_id'] ?: null;
            $customer_id = $post['suspend_customer_id'];
            
            $f_date = explode("/", $this->input->post('suspend_date_from'));
            $t_date = explode("/", $this->input->post('suspend_date_to'));

            $date_from = $f_date[2] . '-' . $f_date[1] . '-' . $f_date[0];
            $date_to = $t_date[2] . '-' . $t_date[1] . '-' . $t_date[0];

            //
            $ndate = $date_from;
            $all_bookings = array();
            $maid_bookings = array();
            $i = 0;
            $maid_schedule = array();
            $ndate = $date_from;
            $i = 0;
            while (strtotime($date_to) >= strtotime($ndate)) {
                $maid_schedule[$i] = new stdClass();
                $maid_schedule[$i]->date = $ndate;

                ++$i;
                $ndate = date("Y-m-d", strtotime("+1 day", strtotime($ndate)));
            }
           
            $res_arr_values = array();
            foreach ($maid_schedule as $rowschedule) {
                $schedle_date = $rowschedule->date;
                // $booking_by_date = $this->bookings_model->get_schedule_by_date_and_customer($customer_id,$schedle_date);
                $booking_by_date = $this->call_management_model->get_schedule_by_date_and_customer_new($customer_id, $schedle_date);
                if (!empty($booking_by_date)) {
                    $res_arr_values[] = $booking_by_date;
                }
            
                if (!empty($response['schedules'])) {
                    $res_arr_values[] = $response['schedules'];
                }
            }
            $single_dimensional_array = array_merge(...$res_arr_values);
        
            $result = array_reverse(array_values(array_column(
                array_reverse($single_dimensional_array),
                null,
                'booking_id'
            )));
            $result_array = array_chunk($result, count($res_arr_values[0]));
            $data['schedules'] = $result_array;
            $data['settings'] = $this->settings_model->get_settings();
            $bookings = $this->load->view('call-management/active-booking-list', $data, TRUE);
            echo $bookings;
            exit();  
                
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }

    }

    public function booking_delete_one_day()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $call_history_data['customer_id']    = $this->input->post('customer_id');
            $call_history_data['mobile_number']   = $this->input->post('mobile_number') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_suspend_service_typ_id') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remarks');
            $delete_date_from = $this->input->post('delete_date_from');
            // print_r($delete_date_from);de();
            $delete_date_from = DateTime::createFromFormat('d/m/Y', $delete_date_from)->format('Y-m-d');
            // print_r($delete_date_from);die();
            $delete_date_to = $this->input->post('delete_date_to');
            $delete_date_to = DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
          
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            //  print_r($bookings->service_start_date);die();
            
            if ($bookings->service_end == 1 && $delete_date_from > $bookings->service_end_date  )
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if($bookings->service_end == 1 && $delete_date_to > $bookings->service_end_date)
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_from) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_to) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            $start = strtotime($delete_date_from);
            $weekday = $bookings->service_week_day;
            $end = strtotime($delete_date_to);
            $weekdays = array();
            // print_r($delete_date_from);die();
            // $weekdays = array();
            while (date("w", $start) != $weekday) {
                $start += 86400;
             
            }
            while ($start <= $end)
            {

            $weekdays[] = date('Y-m-d', $start);
            $start += 604800;

            }
    
            
            $delete_from_date =  current($weekdays);
            $delete_to_date =  end($weekdays);
            $day_service = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
            ->where_in('ds.service_date', $weekdays)->get()->result();
            // print_r($day_service);die();
            /***************************************************************** */
            
            $response['day_service'] = $day_service;
            // if(sizeof($result) > 0)
            if (sizeof($day_service) > 0){
                $delete_b_fields = [];
                foreach ($day_service as $key => $dayservice) {
                    if ($daservice->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 3) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                   
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                    if (user_authenticate() != 1) {
                        if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                            echo 'locked';
                            exit();
                        }
                    }

                    $date_range = $delete_date_from .' - '. $delete_date_to ;
                    // $delete_from_date =  current($weekdays);
                    // $delete_to_date =  end($weekdays);
                    if (isset($d_booking->booking_id)) {
                        if ($d_booking->booking_type != 'OD') {
                            $delete_b_fields[$key]['booking_id'] = $booking_id;
                            $delete_b_fields[$key]['remarks'] = $remarks;
                            $delete_b_fields[$key]['service_date'] = $dayservice->service_date;
                            $delete_b_fields[$key]['day_service_id'] = $dayservice->day_service_id;
                            $delete_b_fields[$key]['delete_from_date'] = $delete_from_date;
                            $delete_b_fields[$key]['delete_to_date'] = $delete_to_date;
                            $delete_b_fields[$key]['added_datetime'] = date('Y-m-d H:i:s');
                            $delete_b_fields[$key]['delete_date_range'] = $date_range;
                            $delete_b_fields[$key]['deleted_by'] = user_authenticate();
                            $delete_b_fields[$key]['call_history_id'] = $call_history_id;
                            
                        }
                    }
                }
            //  print_r($delete_b_fields);die();
                $this->db->insert_batch('booking_deletes', $delete_b_fields);
                $response['status'] = "success";
                $response['call_history_id'] = $call_history_id;
                $response['message'] = "Schedule deleted successfully.";
            }
           
            else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;

                $start = strtotime($delete_date_from);
                $weekday = $booking->service_week_day;
                // print_r($start);die();
                $end = strtotime($delete_date_to);
                $weekdays = array();
                while (date("w", $start) != $weekday) {
                    $start += 86400;
                }
                while ($start <= $end)
                {
                    $weekdays[] = date('Y-m-d', $start);

                    $start += 604800;  
                    
                }
                $delete_from_date =  current($weekdays);
                $delete_to_date =  end($weekdays);
                $day_service_data = [];
                foreach ($weekdays as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $booking->customer_id;
                    $day_service_data[$key]['maid_id'] = $booking->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $booking->time_from;
                    $day_service_data[$key]['time_to'] = $booking->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $booking->service_type_id;
                    $day_service_data[$key]['customer_name'] = $booking->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $booking->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
                // print_r($day_service_data[$key]['service_date']);die();

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

            $date_range = $delete_date_from .' - '. $delete_date_to ;
            $booking_deleted_data = [];
            // $delete_from_date =  current($weekdays);
            // $delete_to_date =  end($weekdays);
            foreach ($day_services as $key => $day_service) {
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] = $delete_from_date;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $date_range;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['call_history_id'] = $call_history_id;
            }
                
               
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();   
                $response['status'] = "success";
                $response['call_history_id'] = $call_history_id;
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function permeant_booking_delete_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remarks');
            $call_history_data['customer_id']    = $this->input->post('customer_id');
            $call_history_data['mobile_number']   = $this->input->post('mobile_number') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_suspend_service_typ_id') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            $moved_to_booking_id = $booking_id;
            $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
            if (user_authenticate() != 1) {
                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
            }
            if (isset($d_booking->booking_id)) {
                $newsearchdate = date('Y-m-d', strtotime($schedule_date));
                $enddate = $d_booking->service_start_date;
                if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');
                } else {
                    if ($d_booking->booking_type == 'WE') {
                        if ($d_booking->service_start_date > $newsearchdate)
                        {
                            $current_date = strtotime($d_booking->service_start_date);
                            $current_date = strtotime('-7 days', $current_date);
                            $delete_datee = date('Y-m-d', $current_date);
                            // print_r($deletedatee);die();
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $delete_datee);
                            if (count($check_buk_delete) > 0) {

                            } else {
                                $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $delete_datee, 'service_end_date' => $delete_datee, 'service_end' => 1), 'Delete');
                                
                            }
                        } else{
                            $current = strtotime($newsearchdate);
                            $last = strtotime($enddate);
                            while ($current >= $last) {
                                $current = strtotime('-7 days', $current);
                                $deletedatee = date('Y-m-d', $current);
                                // print_r($deletedatee);die();
                                $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                if (count($check_buk_delete) > 0) {

                                } else {
                                    $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                    break;
                                }
                            }
                        }
                    }
                }
                $delete_b_fields = array();
                $delete_b_fields['booking_id'] = $booking_id;
                $delete_b_fields['remarks'] = $remarks;
                $delete_b_fields['service_date'] = $service_date;
                // $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_from_date'] = $service_date;
                $delete_b_fields['delete_to_date'] = $service_date;
                $delete_b_fields['added_datetime'] = date('Y-m-d H:i:s');
                $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['call_history_id'] = $call_history_id;
                
                $this->bookings_model->add_booking_delete($delete_b_fields);

                // $delete_b_fields_new = array();
                // $delete_b_fields_new['booking_id'] = $booking_id;
                // $delete_b_fields_new['service_date'] = $service_date;
                // $delete_b_fields_new['remarks'] = $remarks;
                // $delete_b_fields_new['deleted_by'] = user_authenticate();
                // $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');
                // $this->bookings_model->add_booking_remarks($delete_b_fields_new);

                $response['status'] = "success";
                $response['call_history_id'] = $call_history_id;
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function schedule_delete()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
            $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            // print_r($call_history_data);die();
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            $service_date = $this->input->post('service_date');
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*")
                ->from('day_services as ds')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
        //  print_r($day_service);die();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {
                // print_r("helloo");die();
                // day service entry exist
                if ($day_service->service_status == 1) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                if ($day_service->service_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                // if ($day_service->service_status == 3) {
                //     $response['status'] = "failed";
                //     $response['message'] = "schedule can't be deleted.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                if ($day_service->dispatch_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "Confirmed schedule can't be changed.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                
                // set update data
                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] = $this->input->post('service_date');
                $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['call_history_id'] = $call_history_id;

                /***************************************************************** */
              //    print_r($booking_deleted_data);die();
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                
               
                $response['status'] = "success";
                $response['call_history_id'] = $call_history_id;
                $response['message'] = "Schedule deleted successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                // echo'<pre>';
                // print_r($booking);die();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                // $booking->time_from = $this->input->post('time_from');
                // $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $booking->time_from;
                $day_service_data['time_to'] = $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $booking->service_start_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $booking->cleaning_material;
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                $day_service_data['_total_discount'] = $booking->_total_discount;
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                ->where('day_service_id', $day_service_id)
                ->update('day_services');
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                
                $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                $booking_deleted_data['day_service_id'] = $day_service_id;
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['call_history_id'] = $call_history_id;
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                    
                $response['call_history_id'] = $call_history_id;
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
                // // update reference id END
                // $response['status'] = "success";
                // $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    // public function re_activation_booking()
    // {
    //     header('Content-Type: application/json; charset=utf-8');
    //     $post = $this->input->post();
    //     log_message('error', 'Post datas' . json_encode($post));
    //     try {
    //         $this->db->trans_begin();
    //         // $enquiry_type_id = $post['caller_suspend_service_typ_id'] ?: null;
    //         $customer_id = $post['re_actvation_customer_id'];
    //         $date_from= $post['re_activation_date_from'] ? DateTime::createFromFormat('d/m/Y', $post['re_activation_date_from'])->format('Y-m-d') : null;
    //         // $date_to = $post['re_activation_date_to'] ? DateTime::createFromFormat('d/m/Y', $post['re_activation_date_to'])->format('Y-m-d') : null;
            
    //         $service_date = $date_from;
    //         $service_week_day = date('w', strtotime($service_date)); 
    //         $this->db->select('
    //         b.booking_id,
    //         b.booking_type,
    //         b.customer_id,
    //         c.customer_name,
    //         b.maid_id,
    //         m.maid_name,
    //         bd.booking_delete_id,
    //         bd.delete_from_date,
    //         bd.delete_date_range,
    //         bd.delete_to_date',false)
    //         ->from('booking_deletes as bd')
    //         ->join('bookings as b', 'bd.booking_id = b.booking_id', 'left')
    //         ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
    //         ->join('maids as m', 'b.maid_id = m.maid_id', 'left')
    //         ->where('bd.delete_from_date >='. $this->db->escape($service_date)) 
    //         ->where('b.customer_id', $customer_id)
    //         ->where('bd.reassign_status', null)
    //         ->group_by('b.booking_id')
    //         ->group_by('bd.delete_from_date');
    //         // ->group_by('bd.delete_to_date');
    //         $response['schedules'] = $this->db->get()->result();
    //         //   print_r($response['schedules']);die();
    //         // foreach ($response['schedules'] as $key=>$booking) {
    //         //     if ($booking->booking_delete_id != null && $booking->delete_date_range == "") {
    //         //         unset($response['schedules'][$key]);
    //         //     }   
    //         // }
            
    //         // $response['service_date'] = $service_date;
    //         echo json_encode($response, JSON_PRETTY_PRINT);
                
    //     } catch (Exception $e) {
    //         $respose['status'] = "failed";
    //         $respose['message'] = $e->getMessage();
    //         $this->db->trans_rollback();
    //         die(json_encode($respose, JSON_PRETTY_PRINT));
    //     }
    // }
    public function re_activation_booking()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        
        $response = array(); // Initialize $response as an empty array
        
        try {
            $this->db->trans_begin();
            
            $customer_id = $post['re_actvation_customer_id'];
            $date_from = $post['re_activation_date_from'] ? DateTime::createFromFormat('d/m/Y', $post['re_activation_date_from'])->format('Y-m-d') : null;
            
            $service_date = $date_from;
            $service_week_day = date('w', strtotime($service_date)); 
            
            $this->db->select('
                b.booking_id,
                b.booking_type,
                b.customer_id,
                c.customer_name,
                b.maid_id,
                m.maid_name,
                MAX(bd.booking_delete_id) as booking_delete_id,
                bd.delete_from_date,
               MAX(bd.delete_date_range) as delete_date_range, 
                MAX(bd.delete_to_date) as delete_to_date', false)
                ->from('booking_deletes as bd')
                ->join('bookings as b', 'bd.booking_id = b.booking_id', 'left')
                ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                ->join('maids as m', 'b.maid_id = m.maid_id', 'left')
                ->where('bd.delete_from_date >=' . $this->db->escape($service_date)) 
                ->where('b.customer_id', $customer_id)
                ->where('bd.reassign_status', null)
                ->group_by('b.booking_id')
                ->group_by('bd.delete_from_date');
            
            $query = $this->db->get();
    
            if ($query === FALSE) {
                // Query failed
                $error = $this->db->_error_message(); // Fetch the database error message
                $response['error'] = 'Database Error (' . $this->db->_error_number() . '): ' . $error;
            } else {
                // Query successful, process results
                $response['schedules'] = $query->result();
            }
            
            echo json_encode($response, JSON_PRETTY_PRINT);
                
        } catch (Exception $e) {
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }
    }
    
    public function booking_re_activate()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            $booking_delete_id = $this->input->post('booking_delete_id');
            $service_date = $this->input->post('service_date');
            // print_r($service_date);die();
            // print_r($booking_id);die();
            $call_history_data['customer_id']    = $this->input->post('customer_id');
            $call_history_data['mobile_number']   = $this->input->post('mobile_number') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_re_activate_typ_id') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            $this->db->select('bd.*')
            ->from('booking_deletes as bd');
            $this->db->where('bd.delete_from_date >='. $this->db->escape($service_date));
            $this->db->where("bd.booking_id", $booking_id);
            $get_booking_deletes_qry = $this->db->get()->result();
            $booking_delete_ids = array();
            foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                $booking_re_activate[$key]['booking_id'] = $booking_id;
                $booking_re_activate[$key]['service_date'] = $get_booking_data->service_date;
                $booking_re_activate[$key]['re_activate_date_from'] = $get_booking_data->delete_from_date;
                $booking_re_activate[$key]['re_activate_date_to'] = $get_booking_data->delete_to_date;
                $booking_re_activate[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_re_activate[$key]['re_activated_by'] = user_authenticate();
                $booking_re_activate[$key]['call_history_id'] = $call_history_id;
                $booking_delete_ids[] = $get_booking_data->booking_delete_id;
            }
            $this->db->insert_batch('booking_re_activate', $booking_re_activate);

            $this->db->where_in("booking_delete_id", $booking_delete_ids);
            // $this->db->where("booking_delete_id", $booking_delete_id);
      
            $this->db->delete('booking_deletes');
            $response['status'] = "success";
            $response['message'] = "Schedule activated successfully.";
        
        $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function save_schedule()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');

            // Check if maid is flagged
            $maid_id = $this->input->post('maid_id');
            $customer_id = $this->input->post('customer_ids');
            $is_maidflagged = $this->schedule_model->is_maidflagged($maid_id, $customer_id);
            // print_r($is_maidflagged);die();

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $call_history_data['customer_id']    = $customer_id;
            $call_history_data['mobile_number']   = $this->input->post('customer_booking_phone_no') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_booking_typ_id') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);

            $settings = $this->settings_model->get_settings();
            $time_from = strtotime($this->input->post('schedule_time_from'));
            $time_to = strtotime($this->input->post('schedule_time_to'));
            $working_minutes = ($time_to - $time_from) / 60;
            $working_hours = $working_minutes / 60;
            $booking_data['customer_id'] = $this->input->post('customer_ids');
            /***************************************************************** */
            $customer = $this->db->select("c.default_booking_address")
                ->from('customers as c')
                ->where('c.customer_id', $booking_data['customer_id'])->get()->row();

            if ($customer->default_booking_address == "") {
                $response['status'] = "failed";
                $response['message'] = "Please fill the customer address";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            /***************************************************************** */
            $service_start_date1 = $this->input->post('service_start_date1') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_start_date1'))->format('Y-m-d') : null;
            $service_end_date1 = $this->input->post('service_end_date1') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_end_date1'))->format('Y-m-d') : null;
        
            if ($this->input->post('booking_type') == "OD") {
                $booking_data['service_start_date'] = $service_start_date1;
                $service_week_day = date('w', strtotime($service_start_date1));
                $booking_data['booking_type'] = "OD";
                $booking_data['service_end_date'] = $booking_data['service_start_date'];
                $booking_data['service_actual_end_date'] = $booking_data['service_start_date'];
                $booking_data['service_end'] = 1;
            } else {
                $booking_data['booking_type'] = "WE";
                $service_start_date = $this->input->post('service_start_date') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_start_date'))->format('Y-m-d') : null;
                $service_end_date = $this->input->post('service_end_date') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_end_date'))->format('Y-m-d') : null;
                $booking_data['service_start_date'] = $service_start_date;
                $booking_data['service_end_date'] = $service_end_date;
                $service_week_day = date('w', strtotime($service_start_date));
                if ($this->input->post('service_end_date') == "") {
                    $booking_data['service_end_date'] = $service_start_date;
                    $booking_data['service_actual_end_date'] = $service_start_date;
                    $booking_data['service_end'] = 0;
                } else {
                    $booking_data['service_end_date'] = $service_end_date;
                    $booking_data['service_actual_end_date'] = $service_end_date;
                    $booking_data['service_end'] = 1;
                }
            }
            /***************************************************************** */
            $booking_data['customer_address_id'] = $this->input->post('booking_customer_address_ids') ?: $customer->default_booking_address;
            $booking_data['maid_id'] = $this->input->post('maid_id');
            $booking_data['service_type_id'] = $this->input->post('service_type_id');
            $booking_data['time_from'] = $this->input->post('schedule_time_from');
            $booking_data['time_to'] = $this->input->post('schedule_time_to');
            $booking_data['price_per_hr'] = $this->input->post('service_rate_per_hour');
            $booking_data['discount_price_per_hr'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['discount'] = $this->input->post('service_discount');
            $booking_data['vat_charge'] = $this->input->post('service_vat_amount');
            $booking_data['net_service_charge'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['service_charge'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['net_service_cost'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['service_week_day'] = $service_week_day;
            $booking_data['total_amount'] = $this->input->post('taxed_total');
            $booking_data['booking_status'] = 1;
            $booking_data['booked_datetime'] = date('Y-m-d H:i:s');
            /***************************************************************** */
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['cleaning_material'] = "Y";
                $booking_data['cleaning_material_fee'] = $this->input->post('material_fee');
            } else {
                $booking_data['cleaning_material'] = "N";
                $booking_data['cleaning_material_fee'] = 0;
            }
            $booking_data['booking_category'] = "C";
            /************************************************ */
            // some understandable new fields
            $booking_data['_service_hours'] = $working_hours;
            $booking_data['_cleaning_material_rate_per_hour'] = 10;
            $booking_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
            $booking_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['_service_amount'] = $booking_data['_service_rate_per_hour'] * $working_hours;
            $booking_data['_service_discount'] = $booking_data['_service_amount'] - ($booking_data['_service_discount_rate_per_hour'] * $working_hours);
            $booking_data['_net_service_amount'] = $booking_data['_service_amount'] - $booking_data['_service_discount'];
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['_cleaning_material'] = "Y";
                $booking_data['_net_cleaning_material_amount'] = $booking_data['_cleaning_material_rate_per_hour'] * $working_hours;
            } else {
                $booking_data['_cleaning_material'] = "N";
                $booking_data['_net_cleaning_material_amount'] = 0;
            }
            $booking_data['_total_discount'] = $booking_data['_service_discount'];
            $booking_data['_taxable_amount'] = $booking_data['_net_service_amount'] + $booking_data['_net_cleaning_material_amount'];
            $booking_data['_vat_percentage'] = $settings->service_vat_percentage;
            $booking_data['_vat_amount'] = ($booking_data['_vat_percentage'] / 100) * $booking_data['_taxable_amount'];
            $booking_data['_total_amount'] = $booking_data['_taxable_amount'] + $booking_data['_vat_amount'];
            $booking_data['created_at'] = date('Y-m-d H:i:s');
            $booking_data['updated_at'] = date('Y-m-d H:i:s');
            $booking_data['call_history_id'] = $call_history_id ;
            
            /***************************************************************** */
            // check if it's overlapping other bookings
            $booking = new stdClass();
            $booking->service_start_date = $booking_data['service_start_date'];
            $booking->service_week_day = $booking_data['service_week_day'];
            $booking->service_actual_end_date = $booking_data['service_actual_end_date'];
            $booking->time_from = $booking_data['time_from'];
            $booking->time_to = $booking_data['time_to'];
            $booking->service_end = $booking_data['service_end'];
            $booking->booking_type = $booking_data['booking_type'];
            $timed_bookings = get_same_timed_bookings($booking, null);
            $booking_ids = array_column($timed_bookings,'booking_id');
            $deleted_booking_data = get_deleted_booking($booking_data);
          
            $book_id = array_column($deleted_booking_data,'day_service_booking_id');
            $result = array_diff($booking_ids,$book_id);
            // if ($this->input->post('booking_type') == "WE") {
                if(sizeof($result) > 0)
                {
                    $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                    if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                    throw new Exception('Selected time slot not available.', 100);
                    }
                }  
            // }
            $day_services = new stdClass();
            $day_services->time_from = $booking_data['time_from'];
            $day_services->time_to =  $booking_data['time_from'];
            $day_services->service_date = $booking_data['service_start_date'];
            
            $timed_day_service = get_same_timed_day_service($day_services, null);
            $busy_maid_id = array_column($timed_day_service,'maid_id');
            // print_r($busy_maid_id);die();
            // echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
            // if (in_array($booking_data['maid_id'], $busy_maid_id)) {
            //     throw new Exception('Selected time slot not available.', 100);
            // }
            $day_service_id = array_column($deleted_booking_data,'day_service_id');
            $day_service_ids = array_column($timed_day_service,'day_service_id');
            $resultarray = array_diff($day_service_ids,$day_service_id);
            // print_r($resultarray);die();
            if ($this->input->post('booking_type') == "WE") {
                if(sizeof($resultarray) > 0)
                {
                    $busy_maid_id = array_column($timed_day_service,'maid_id');
                    //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    if (in_array($day_service->maid_id, $busy_maid_id)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }
                } 
            }
            // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
            // $booking_ids = array_column($timed_bookings,'booking_id');
            // // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
            // // $result = array_diff($book_id,$booking_ids);
            // if(sizeof($booking_ids) > 0)
            // {
            //     $busy_maid_ids = array_column($timed_bookings,'maid_id');
            //     if (in_array($booking->maid_id, $busy_maid_ids)) {
            //         throw new Exception('Selected time slot not available.', 100);
            //     }
            // }  
            /***************************************************************** */
            $this->db->trans_begin();
            $this->db->insert('bookings', $booking_data);
            $booking_id = $this->db->insert_id();
            if ($booking_id > 0) {
                $response['status'] = "success";
                if ($this->input->post('booking_type') == "OD") {
                    $response['message'] = "Schedule created successfully.";
                } else {
                    $response['message'] = "New booking created successfully.";
                }
                // add more data
                $booking_data['reference_id'] = "MM-" . $booking_data['booking_type'] . "-" . sprintf("%06d", $booking_id);
                $this->db->set($booking_data)
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                $this->db->trans_commit();
            } else {
                $response['status'] = "failed";
                throw new Exception($this->db->_error_message(), 100);
            }
            
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    // *************************************************************
    function call_report()
    {
        // log_message('error','post call Report'.json_encode($this->input->post()));
        if ($this->input->post('from_date')) {
            $activity_date = $this->input->post('from_date');
            $s_date = explode("/", $activity_date);
            $from_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $from_date = date('Y-m-d', strtotime('-7 days'));
            $activity_date = date('d/m/Y', strtotime('-5 days'));
        }
        if ($this->input->post('to_date')) {
            $activity_date_to = $this->input->post('to_date');
            $s_date = explode("/", $activity_date_to);
            $to_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        } else {
            $to_date = date('Y-m-d');
            $activity_date_to = date('d/m/Y');
        }
    
        $data['activity_date'] = $activity_date;
        $data['activity_date_to'] = $activity_date_to;

        $data['call_report_all'] = $this->call_management_model->get_call_history_all($from_date,$to_date);
        $data['call_book_report'] = $this->call_management_model->get_call_booking_all($from_date,$to_date);
        $data['call_reactivate_report'] = $this->call_management_model->get_call_reactivation_all($from_date,$to_date);
        $data['call_suspend_report'] = $this->call_management_model->get_call_suspend_all($from_date,$to_date);
        $data['call_complaint_report'] = $this->call_management_model->get_call_complaints_all($from_date,$to_date);
        $data['call_enquiry_report'] = $this->call_management_model->get_call_enquiry_all($from_date,$to_date);
        $data['call_preference_report'] = $this->call_management_model->get_call_preference_all($from_date,$to_date);
        $data['call_feedback_report'] = $this->call_management_model->get_feedback_all($from_date,$to_date);

    
        // print_r($data['call_enquiry_report']);die();
        /**************************************************************************** */
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('call-management/call-report', $data, TRUE);
        $layout_data['page_title'] = 'Call-Management-Report';
        $layout_data['meta_description'] = 'Call Mangement Report';
        $layout_data['css_files'] = array('jquery.fancybox.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('jquery.fancybox.pack.js', 'base.js','jquery-ui.min.js','main.js', 'mymaids.js', 'ajaxupload.3.5.js','bootstrap.min.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function booking_schedule_re_activate()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            $call_history_id = $this->input->post('call_history_id');

            // $call_history_data['customer_id']    = $this->input->post('customer_id');
            // $call_history_data['mobile_number']   = $this->input->post('mobile_number') ?: null;
            // $call_history_data['enquiry_type_id'] = $this->input->post('caller_re_activate_typ_id') ?: null;
            // $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            // $call_history_data['attended_by']     = user_authenticate();
            // $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            // $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            $this->db->select('bd.*')
            ->from('booking_deletes as bd');
            // $this->db->where('bd.delete_from_date >='. $this->db->escape($service_date));
            $this->db->where("bd.booking_id", $booking_id);
            $this->db->where("bd.call_history_id", $call_history_id);
            $get_booking_deletes_qry = $this->db->get()->result();

            $this->db->select('b.*,c.customer_id as customerid, c.mobile_number_1')
            ->from('bookings as b')
            ->join('customers c', 'b.customer_id = c.customer_id');
            $this->db->where("b.booking_id", $booking_id);
            $get_booking_qry = $this->db->get()->row();
            // echo'<pre>';
            // print_r($get_booking_qry->customerid);die();
            $call_history_data['customer_id']    = $get_booking_qry->customerid;
            $call_history_data['mobile_number']   = $get_booking_qry->mobile_number_1 ?: null;
            $call_history_data['enquiry_type_id'] = 5 ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_historyid = $this->call_management_model->save_call_history($call_history_data);
            // $this->db->select('bd.*')
            // ->from('booking_deletes as bd');
            // $this->db->where('bd.delete_from_date >='. $this->db->escape($service_date));
            // $this->db->where("bd.booking_id", $booking_id);
            // $get_booking_deletes_qry = $this->db->get()->result();
            // $booking_delete_ids = array();
            foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                $booking_re_activate[$key]['booking_id'] = $booking_id;
                $booking_re_activate[$key]['service_date'] = $get_booking_data->service_date;
                $booking_re_activate[$key]['re_activate_date_from'] = $get_booking_data->delete_from_date;
                $booking_re_activate[$key]['re_activate_date_to'] = $get_booking_data->delete_to_date;
                $booking_re_activate[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_re_activate[$key]['re_activated_by'] = user_authenticate();
                $booking_re_activate[$key]['call_history_id'] = $call_historyid;
                $booking_delete_ids[] = $get_booking_data->booking_delete_id;
            }
            $this->db->insert_batch('booking_re_activate', $booking_re_activate);

            $this->db->where_in("booking_delete_id", $booking_delete_ids);
   
      
            $this->db->delete('booking_deletes');
            $response['status'] = "success";
            $response['message'] = "Schedule activated successfully.";
        
        $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function save_feedback()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $is_maidflagged = $this->schedule_model->is_maidflagged($post['feedback_maid_id'], $post['feedback_customer_id']);

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $call_history_data['customer_id']    = $post['feedback_customer_id'] ?: null;
            $call_history_data['mobile_number']   = $post['feedback_customer_phone'] ?: null;
            $call_history_data['enquiry_type_id'] = $post['caller_feedback_typ_id'] ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            if($call_history_id > 0) {
                $feedback_data['call_history_id'] = $call_history_id;
                $feedback_data['customer_id'] = $post['feedback_customer_id'] ?: null;
                $feedback_data['enquiry_type_id'] = $post['caller_feedback_typ_id'] ?: null;
                $feedback_data['maid_id'] = $post['feedback_maid_id'] ?: null;
                $feedback_data['message'] = $post['message'] ?: null;
                $feedback_data['call_history_id'] = $call_history_id;
                $feedback_data['date'] = $post['feedback_date'] ? DateTime::createFromFormat('d/m/Y', $post['feedback_date'])->format('Y-m-d') : null;
                $feedback_data['created_by_user'] = user_authenticate();
                $feedback_data['created_at'] = $feedback_data['updated_at'] = date('Y-m-d H:i:s');

                $this->call_management_model->save_feedback($feedback_data);
                $this->db->trans_commit();
                $respose['status'] = "success";
                $respose['message'] = "Feedback saved successfully.";
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
            else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }   
        }
        catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    } 

    

  public function search_maid_availability_enquiry()
  {
      header('Content-Type: application/json; charset=utf-8');
      $post = $this->input->post();
      log_message('error', 'Post datas' . json_encode($post));
      try {
          $this->db->trans_begin();
          $service_date = $post['enquiry_booking_date'] ? DateTime::createFromFormat('d/m/Y', $post['enquiry_booking_date'])->format('Y-m-d') : null;
          $customer_id = $post['enquiry_customer_id'];
          $customer_name = $post['enquiry_customer_name'];
          $area_id = $this->input->post('area_id');
          $zone_id = $this->input->post('enquiry_zone_id');
          $location_id = $this->input->post('location_id');

          $from_time = $this->input->post('enquiry_time_from');
          $book_type = $this->input->post('booking_type_enquiry');
          $to_time = $this->input->post('enquiry_time_to');
          $filter = array();
          $filter['customer_id'] = $customer_id;
          $filter['service_date'] = $service_date;
          $zone_id = $this->input->post('enquiry_zone_id');
          $maid_id = $this->input->post('maidId');
          $filter['zone_id'] = $zone_id;
          $filter['maid_id'] = $maid_id;
          $filter['location_id'] = $location_id;
          $filter['from_time'] = $from_time;
          $filter['to_time'] = $to_time;
        //   $filter['location_type'] = $location_type;

          $data['service_date'] = $service_date;
          $data['customer_phoneno'] = $this->input->post('enquiry_customer_phoneno');
          $data['caller_enquiry_typ_id'] = $this->input->post('caller_enquiry_typ_id');
          $data['enquiry_customer_address_id'] = $this->input->post('enquiry_customer_address_id');
          $data['customer_name'] = $customer_name;
          $data['from_time'] = $from_time;
          $data['to_time'] = $to_time;
          $data['customer_id'] = $customer_id;
          $data['zone_id'] = $zone_id;
          $data['new_book_type'] = $book_type;
          $search_maids = $this->maids_model->get_all_maids_active_enquiry($filter);
          $flagged_maids = $this->schedule_model->flagged_maids($customer_id);
            //   $search_maids = $this->maids_model->get_all_maids_active_enquiry($filter);
          $flagged_ids = array_map(function($maid) {
              return $maid->maid_id;
          }, $flagged_maids);
          
          // Filter out flagged maids from search_maids
          $filtered_maids = array_filter($search_maids, function($maid) use ($flagged_ids) {
              return !in_array($maid->maid_id, $flagged_ids);
          });
          
          $search_maids = array_values($filtered_maids);
          $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
          $leave_maid_ids = array();
          foreach ($maids_leave_on_date as $leave) {
              array_push($leave_maid_ids, $leave->maid_id);
          }
          $time_from_stamp = $from_time;
          $time_to_stamp = $to_time;
          $maid_array = array();
          $maids_det = array();
          if ($book_type == 'OD') {
              $today_bookingss = $this->bookings_model->get_schedule_by_date_avail($service_date);
          }
  
          if ($book_type == 'BW' || $book_type == 'WE') {
              $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day);
          }

          foreach ($search_maids as $s_maids) {

              $today_week_day = date('w', strtotime($service_date));
              //One day
              if ($book_type == 'OD') {
                 
                  foreach ($today_bookingss as $t_booking) {
                      $booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);
  
                  }
  
                  if (isset($booked_slots[$s_maids->maid_id]['time'])) {
                      foreach ($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time) {
                          if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                              //array_push($nf_maids, $smaid->maid_id);
                              array_push($maids_det, $s_maids->maid_id);
                          }
                      }
                  }
                  
              } else if ($book_type == 'BW' || $book_type == 'WE') {
                  if ($repeat_day < $today_week_day) {
                      $day_diff = (6 - $today_week_day + $repeat_day + 1);
                  } else {
                      $day_diff = $repeat_day - $today_week_day;
                  }
                  $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                  
                  if (!empty($bookings_on_day)) {
                      foreach ($bookings_on_day as $booking_on_day) {
                          $s_date_stamp = strtotime($service_start_date);
                          $e_date_stamp = strtotime($service_start_date);
                          $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                          $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
  
                          if ($booking_on_day->booking_type == "BW" || $booking_on_day->booking_type == "WE") {
                              $now = strtotime($service_start_date);
                              $your_date = strtotime($booking_on_day->service_start_date);
                              $datediff = $now - $your_date;
                              //echo $booking_on_day->service_start_date;
                              $week_diff = round($datediff / (60 * 60 * 24));
                              $week_difference = fmod($week_diff, 14);
                              //echo $week_difference;
                              if ($week_difference == 0 || $week_difference == '-0') {
                                  $f_time = strtotime($booking_on_day->time_from);
                                  $t_time = strtotime($booking_on_day->time_to);
  
                                  if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                      array_push($maids_det, $s_maids->maid_id);
                                  } else {
  
                                  }
                              }
                          } 
                      }
                  } else {
  
                  }
              }
          }
          $service_week_day = date('w', strtotime($service_date));
          $data['maids'] = $search_maids;
          $data['service_week_day'] = $service_week_day;
          $data['n_maids'] = $maids_det;
          $data['leave_maid_ids'] = $leave_maid_ids;         
          $data['settings'] = $this->settings_model->get_settings();
          $maids = $this->load->view('call-management/enquiry-maid-availablity-list', $data, TRUE);
          echo $maids;
          exit();
      } catch (Exception $e) {
          $response['status'] = "failed";
          $response['message'] = $e->getMessage();
          $this->db->trans_rollback();
          die(json_encode($response, JSON_PRETTY_PRINT));
      }
    }
    public function save_schedule_enquiry()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');

            // Check if maid is flagged
            $maid_id = $this->input->post('maid_id');
            $customer_id = $this->input->post('customer_enquiryids');
            $is_maidflagged = $this->schedule_model->is_maidflagged($maid_id, $customer_id);
            // print_r($is_maidflagged);die();

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $call_history_data['customer_id']    = $customer_id;
            $call_history_data['mobile_number']   = $this->input->post('customer_enquiry_phone_no') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_enquiry_typ_ids') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);

            $settings = $this->settings_model->get_settings();
            $time_from = strtotime($this->input->post('schedule_enquiry_time_from'));
            $time_to = strtotime($this->input->post('schedule_enquiry_time_to'));
            $working_minutes = ($time_to - $time_from) / 60;
            $working_hours = $working_minutes / 60;
            $booking_data['customer_id'] = $this->input->post('customer_enquiryids');
            /***************************************************************** */
            $customer = $this->db->select("c.default_booking_address")
                ->from('customers as c')
                ->where('c.customer_id', $booking_data['customer_id'])->get()->row();

            if ($customer->default_booking_address == "") {
                $response['status'] = "failed";
                $response['message'] = "Please fill the customer address";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            /***************************************************************** */
            $service_start_date1 = $this->input->post('service_start_date1') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_start_date1'))->format('Y-m-d') : null;
            $service_end_date1 = $this->input->post('service_end_date1') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_end_date1'))->format('Y-m-d') : null;
        
            if ($this->input->post('booking_type') == "OD") {
                $booking_data['service_start_date'] = $service_start_date1;
                $service_week_day = date('w', strtotime($service_start_date1));
                $booking_data['booking_type'] = "OD";
                $booking_data['service_end_date'] = $booking_data['service_start_date'];
                $booking_data['service_actual_end_date'] = $booking_data['service_start_date'];
                $booking_data['service_end'] = 1;
            } else {
                $booking_data['booking_type'] = "WE";
                $service_start_date = $this->input->post('service_start_date') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_start_date'))->format('Y-m-d') : null;
                $service_end_date = $this->input->post('service_end_date') ? DateTime::createFromFormat('d/m/Y', $this->input->post('service_end_date'))->format('Y-m-d') : null;
                $booking_data['service_start_date'] = $service_start_date;
                $booking_data['service_end_date'] = $service_end_date;
                $service_week_day = date('w', strtotime($service_start_date));
                if ($this->input->post('service_end_date') == "") {
                    $booking_data['service_end_date'] = $service_start_date;
                    $booking_data['service_actual_end_date'] = $service_start_date;
                    $booking_data['service_end'] = 0;
                } else {
                    $booking_data['service_end_date'] = $service_end_date;
                    $booking_data['service_actual_end_date'] = $service_end_date;
                    $booking_data['service_end'] = 1;
                }
            }
            /***************************************************************** */
            $booking_data['customer_address_id'] = $this->input->post('enquiry_customer_address_ids') ?: $customer->default_booking_address;
            $booking_data['maid_id'] = $this->input->post('maid_id');
            $booking_data['service_type_id'] = $this->input->post('service_type_id');
            $booking_data['time_from'] = $this->input->post('schedule_enquiry_time_from');
            $booking_data['time_to'] = $this->input->post('schedule_enquiry_time_to');
            $booking_data['price_per_hr'] = $this->input->post('service_rate_per_hour');
            $booking_data['discount_price_per_hr'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['discount'] = $this->input->post('service_discount');
            $booking_data['vat_charge'] = $this->input->post('service_vat_amount');
            $booking_data['net_service_charge'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['service_charge'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['net_service_cost'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['service_week_day'] = $service_week_day;
            $booking_data['total_amount'] = $this->input->post('taxed_total');
            $booking_data['booking_status'] = 1;
            $booking_data['booked_datetime'] = date('Y-m-d H:i:s');
            /***************************************************************** */
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['cleaning_material'] = "Y";
                $booking_data['cleaning_material_fee'] = $this->input->post('material_fee');
            } else {
                $booking_data['cleaning_material'] = "N";
                $booking_data['cleaning_material_fee'] = 0;
            }
            $booking_data['booking_category'] = "C";
            /************************************************ */
            // some understandable new fields
            $booking_data['_service_hours'] = $working_hours;
            $booking_data['_cleaning_material_rate_per_hour'] = 10;
            $booking_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
            $booking_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['_service_amount'] = $booking_data['_service_rate_per_hour'] * $working_hours;
            $booking_data['_service_discount'] = $booking_data['_service_amount'] - ($booking_data['_service_discount_rate_per_hour'] * $working_hours);
            $booking_data['_net_service_amount'] = $booking_data['_service_amount'] - $booking_data['_service_discount'];
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['_cleaning_material'] = "Y";
                $booking_data['_net_cleaning_material_amount'] = $booking_data['_cleaning_material_rate_per_hour'] * $working_hours;
            } else {
                $booking_data['_cleaning_material'] = "N";
                $booking_data['_net_cleaning_material_amount'] = 0;
            }
            $booking_data['_total_discount'] = $booking_data['_service_discount'];
            $booking_data['_taxable_amount'] = $booking_data['_net_service_amount'] + $booking_data['_net_cleaning_material_amount'];
            $booking_data['_vat_percentage'] = $settings->service_vat_percentage;
            $booking_data['_vat_amount'] = ($booking_data['_vat_percentage'] / 100) * $booking_data['_taxable_amount'];
            $booking_data['_total_amount'] = $booking_data['_taxable_amount'] + $booking_data['_vat_amount'];
            $booking_data['created_at'] = date('Y-m-d H:i:s');
            $booking_data['updated_at'] = date('Y-m-d H:i:s');
            $booking_data['call_history_id'] = $call_history_id ;
            
            /***************************************************************** */
            // check if it's overlapping other bookings
            $booking = new stdClass();
            $booking->service_start_date = $booking_data['service_start_date'];
            $booking->service_week_day = $booking_data['service_week_day'];
            $booking->service_actual_end_date = $booking_data['service_actual_end_date'];
            $booking->time_from = $booking_data['time_from'];
            $booking->time_to = $booking_data['time_to'];
            $booking->service_end = $booking_data['service_end'];
            $timed_bookings = get_same_timed_bookings($booking, null);
            $booking_ids = array_column($timed_bookings,'booking_id');
            $deleted_booking_data = get_deleted_booking($booking_data);
          
            $book_id = array_column($deleted_booking_data,'day_service_booking_id');
            $result = array_diff($booking_ids,$book_id);
            if ($this->input->post('booking_type') == "WE") {
                if(sizeof($result) > 0)
                {
                    $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                    if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                    throw new Exception('Selected time slot not available.', 100);
                    }
                }  
            }
            $day_services = new stdClass();
            $day_services->time_from = $booking_data['time_from'];
            $day_services->time_to =  $booking_data['time_from'];
            $day_services->service_date = $booking_data['service_start_date'];
            
            $timed_day_service = get_same_timed_day_service($day_services, null);
            $busy_maid_id = array_column($timed_day_service,'maid_id');
            // echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
            // if (in_array($booking_data['maid_id'], $busy_maid_id)) {
            //     throw new Exception('Selected time slot not available.', 100);
            // }
            $day_service_id = array_column($deleted_booking_data,'day_service_id');
            $day_service_ids = array_column($timed_day_service,'day_service_id');
            $resultarray = array_diff($day_service_ids,$day_service_id);
            if ($this->input->post('booking_type') == "WE") {
                if(sizeof($resultarray) > 0)
                {
                    $busy_maid_id = array_column($timed_day_service,'maid_id');
                    //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    if (in_array($day_service->maid_id, $busy_maid_id)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }
                } 
            }
            // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
            // $booking_ids = array_column($timed_bookings,'booking_id');
            // // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
            // // $result = array_diff($book_id,$booking_ids);
            // if(sizeof($booking_ids) > 0)
            // {
            //     $busy_maid_ids = array_column($timed_bookings,'maid_id');
            //     if (in_array($booking->maid_id, $busy_maid_ids)) {
            //         throw new Exception('Selected time slot not available.', 100);
            //     }
            // }  
            /***************************************************************** */
            $this->db->trans_begin();

            // Enquiry save
            $data['call_history_id'] = $call_history_id;
            $data['enquiry_type_id'] = $this->input->post('caller_enquiry_typ_ids') ?: null;
            $data['customer_id'] = $customer_id;
            $data['date_from'] = $this->input->post('enquirydate') ? DateTime::createFromFormat('d/m/Y', $this->input->post('enquirydate'))->format('Y-m-d') : null;
            // $data['date_to'] = $post['date_to'] ? DateTime::createFromFormat('d/m/Y', $post['date_to'])->format('Y-m-d') : null;
            $data['time_from'] = $this->input->post('schedule_enquiry_time_from') ?: null;
            $data['time_to'] = $this->input->post('schedule_enquiry_time_to') ?: null;
            $data['total_hours'] = $working_hours ?: null;
            $data['enquiry_customer_name'] = $this->input->post('customer_enquiryname1');
            $data['enquiry_customer_phoneno'] = $this->input->post('customer_enquiry_phone_no') ?: null;
            $data['enquiry_customer_email'] = $this->input->post('enquiry_customer_email1') ?: null;
            $data['service_type_id'] = $this->input->post('service_type_id') ?: null;
            $data['enquiry_status'] = 'Booked' ?: null;
            $data['booking_type'] =  $this->input->post('booking_type') ?: null;
            $data['enquiry_notes'] =  $this->input->post('enquirynote') ?: null;
            $data['refer_source_id'] = $this->input->post('enquirysourceId') ?: null;
            // if (isset($this->input->post('outside_service_checked'))) && ($this->input->post('outside_service_checked') == "Y"){
            //     $customer_service_area = $post['outside_service_area'];
            // }
            $outside_service_checked = $this->input->post('outside_service_checked1');
            $outside_new_area1 = $this->input->post('outside_new_area1');
            if (isset($outside_service_checked) && $outside_service_checked == "Y") {
                $customer_service_area = $this->input->post('outside_service_area1');
            }
            
            if (isset($outside_new_area1)) {
                $new_service_area = $this->input->post('outside_new_area1');
            }
            $data['new_service_area'] = $new_service_area ?: null;
            $data['customer_service_area'] = $customer_service_area ?: 0;
            $data['area_id'] = $this->input->post('areaId') ?: null;
            $data['location_id'] = $this->input->post('locationId') ?: null;
            $data['maid_id'] = $maid_id ?: null;
            $data['created_by_user'] = user_authenticate();
            $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');

            $this->call_management_model->save_service($data);

            // end
 
            $this->db->insert('bookings', $booking_data);
            $booking_id = $this->db->insert_id();
            if ($booking_id > 0) {
                $response['status'] = "success";
                if ($this->input->post('booking_type') == "OD") {
                    $response['message'] = "Schedule created successfully.";
                } else {
                    $response['message'] = "New booking created successfully.";
                }
                // add more data
                $booking_data['reference_id'] = "MM-" . $booking_data['booking_type'] . "-" . sprintf("%06d", $booking_id);
                $this->db->set($booking_data)
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                $this->db->trans_commit();
            } else {
                $response['status'] = "failed";
                throw new Exception($this->db->_error_message(), 100);
            }
            
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function customer_enquiry_save()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        // print_r($post);die();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $is_maidflagged = $this->schedule_model->is_maidflagged($post['maid_id'], $post['customer_id']);
            // print_r($is_maidflagged);die();

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $call_history_data['customer_id']    = $post['customer_id'];
            $call_history_data['mobile_number']   = $post['customer_phoneno'] ?: null;
            $call_history_data['enquiry_type_id'] = $post['caller_type'] ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            if($call_history_id > 0) {
                $data['call_history_id'] = $call_history_id;
                $data['enquiry_type_id'] = $post['caller_type'] ?: null;
                $data['customer_id'] = $post['customer_id'];
                $data['date_from'] = $post['enquiry_date'] ? DateTime::createFromFormat('d/m/Y', $post['enquiry_date'])->format('Y-m-d') : null;
                // $data['date_to'] = $post['date_to'] ? DateTime::createFromFormat('d/m/Y', $post['date_to'])->format('Y-m-d') : null;
                $data['time_from'] = $post['enquiry_time_from'] ?: null;
                $data['time_to'] = $post['enquiry_time_to'] ?: null;
                $data['total_hours'] = $post['working_hours'] ?: null;
                $data['enquiry_customer_name'] = $post['customer_name'];
                $data['enquiry_customer_phoneno'] = $post['customer_phoneno'] ?: null;
                $data['enquiry_customer_email'] = $post['enquiry_customer_email'] ?: null;
                $data['service_type_id'] = $post['service_type_id'] ?: null;
                $data['enquiry_status'] = 'Saved' ?: null;
                $data['booking_type'] = $post['booking_type'] ?: null;
                $data['enquiry_notes'] = $post['enquiry_notes'] ?: null;
                $data['refer_source_id'] = $post['enquiry_source_id'] ?: null;
                if (isset($post['outside_service_checked']) && $post['outside_service_checked'] == "Y") {
                    $customer_service_area = $post['outside_service_area'];
                }
                if (isset($post['outside_new_area'])) {
                    $new_service_area = $post['outside_new_area'];
                }
                $data['new_service_area'] = $new_service_area ?: null;
                $data['customer_service_area'] = $customer_service_area ?: 0;
                $data['area_id'] = $post['area_id'] ?: null;
                $data['location_id'] = $post['location_id'] ?: null;
                $data['maid_id'] = $post['maid_id'] ?: null;
                $data['created_by_user'] = user_authenticate();
                $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
    
                $this->call_management_model->save_service($data);
                $this->db->trans_commit();
                $respose['status'] = "success";
                $respose['message'] = "Enquiry saved successfully.";
                die(json_encode($respose, JSON_PRETTY_PRINT));
            } else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }

    }

    // *************************************************************************
    public function update_status_complaint() {
        log_message('error',json_encode($this->input->post()));
        if ($this->input->is_ajax_request()) {

            $call_history_id = $this->input->post('call_history_id');
            $status = $this->input->post('status');

            if($status==''){
                $response['status'] = 'error';
                $response['message'] = 'Status Cannot be Empty';
                return $response;
            }else{
            $data=array(
                'status'=>$status,
            );
            $result = $this->db->where('call_history_id',$call_history_id)->update('complaints_new',$data);
            
            if ($result) {
                $response['status'] = 'success';
                $response['message'] = 'Status updated successfully.';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Failed to update status.';
            }
         }
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Direct Access Not Allowed';
            
            echo json_encode($response);
        }
    }

    // *******************************************************************

    public function Enquiry_Edit() {
        $call_history_id = $this->uri->segment(3);
        $call_enquiry_data = $this->call_management_model->get_enquiry_details($call_history_id);
        foreach ($call_enquiry_data as $key => $enquiry_data) {
            $data['enquiry_data1'][$enquiry_data->enquiry_week_day] = $enquiry_data;
            $data['enquiry'] = $enquiry_data;
        }
        // echo '<pre>';
        // // print_r($call_enquiry_data);
        // print_r($data['enquiry']->service_type_id);die();
        $data['settings'] = $this->settings_model->get_settings();
        $data['zones'] = $this->call_management_model->get_zones();
        $data['booking_types'] = $this->call_management_model->get_booking_types();
        $data['maids'] = $this->call_management_model->get_pref_maids();
        $data['pref_maids'] = $this->db->select('m.maid_id,m.maid_name')->from('maids as m')->where('m.maid_status', 1)->where('m.employee_type_id', 1)->order_by('m.maid_id', 'ASC')->get()->result();
        $data['services'] = $this->settings_model->get_services_maid();
        $data['areas'] = $this->call_management_model->get_areas();
        $data['locations'] = $this->db->select('l.*')->from('locations as l')->where('l.deleted_at', null)->order_by('l.location_id', 'ASC')->get()->result();
        $data['refer_sources'] = $this->customer_mollymaid_model->get_customer_sources();
        //         echo'<pre>';
        // print_r($data['call_lists']);die();
        // $from_time = DateTime::createFromFormat('H:i:s', "00:00:00");
        $from_time = DateTime::createFromFormat('H:i:s', "08:00:00");
        // $to_time = DateTime::createFromFormat('H:i:s', "23:59:59");
        $to_time = DateTime::createFromFormat('H:i:s', "19:30:00");
        $i = 0;
        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
            $time_clone = clone $time;
            $i++;
            $data['time_slots'][$time->format('H:i:s')] = $time->format('h:i A');
        }
        $data['residence_types'] = $this->call_management_model->residence_types();
        $data['enquiry_types'] = $this->call_management_model->enquiry_types();
        $layout_data['content_body'] = $this->load->view('call-management/call-enquiry-edit', $data, true);
        $layout_data['page_title'] = 'Call Enquiry Edit';
        $layout_data['meta_description'] = 'Call Enquiry Edit';
        $layout_data['css_files'] = array('jquery.flexdatalist.css', 'datepicker.css', 'toastr.min.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.flexdatalist.js', 'bootstrap-datepicker.js', 'cropper.js', 'jquery.dataTables.min.js','call-enquiry-edit.js');
        // $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js');
        
        $this->load->view('layouts/default', $layout_data);
    }

    public function update_enquiry()
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->trans_begin();
        $call_history_id = $this->input->post('call_history_id');
        $enquiry = $this->db->select('se.*')->from('service_enquiry as se')->where('call_history_id', $call_history_id)->get()->result();
        $enquiryIDS =  array_column($enquiry,'service_enquiry_id');
     
        $this->db->where(array('call_history_id' => $call_history_id));
        $this->db->update('service_enquiry', array('deleted_at' => date('Y-m-d H:i:s')));
       
        $post = $this->input->post();
        $this->db->where('call_history_id', $call_history_id);
        $this->db->update('service_enquiry', array('enquiry_status' => 'Saved'));
        $this->db->flush_cache();
        if ($post['availability_week_days']) {
            foreach ($post['availability_week_days'] as $key => $week_day) {
                if (isset($post['availability_checked'][$key]) && $post['availability_checked'][$key] == "Y") {
          
                    $enquiry_data['enquiry_week_day '] = $post['availability_week_days'][$key];
                    $enquiry_data['call_history_id'] = $call_history_id;
                    $enquiry_data['enquiry_type_id'] = $post['caller_enquiry_typ_id'];
                    $enquiry_data['customer_id'] = $post['enquiry_customer_id'];
                    $enquiry_data['date_from'] = $post['enquiry_booking_date'] ? DateTime::createFromFormat('d/m/Y', $post['enquiry_booking_date'])->format('Y-m-d') : null;
                    $enquiry_data['time_from'] = $post['availability_time_from'][$key];
                    $enquiry_data['total_hours'] = $post['working_hour'][$key];
                    $enquiry_data['time_to'] = $post['availability_time_to'][$key];
                    $enquiry_data['enquiry_customer_name'] = $post['enquiry_customer_name'];
                    $enquiry_data['enquiry_customer_phoneno'] = $post['enquiry_customer_phoneno'] ?: null;
                    $enquiry_data['enquiry_customer_email'] = $post['enquiry_customer_email'] ?: null;
                    $enquiry_data['enquiry_notes'] = $post['enquiry_notes'] ?: null;
                    $enquiry_data['service_type_id'] = $post['service_id'] ?: null;
                    $enquiry_data['refer_source_id'] = $post['enquiry_source_id'] ?: null;
                    if (isset($post['outside_service_checked']) && $post['outside_service_checked'] == "Y") {
                        $customer_service_area = $post['outside_service_area'];
                    }
                    if (isset($post['outside_new_area'])) {
                        $new_service_area = $post['outside_new_area'];
                    }
                    $enquiry_data['new_service_area'] = $new_service_area ?: null;
                    $enquiry_data['customer_service_area'] = $customer_service_area ?: 0;
                    $enquiry_data['area_id'] = $post['area_id'] ?: null;
                    $enquiry_data['location_id'] = $post['location_id'] ?: null;
                    $enquiry_data['maid_id'] = $post['maidId'] ?: null;
                    $enquiry_data['enquiry_status'] = "Saved" ?: null;
                    $enquiry_data['booking_type'] = $post['booking_type_enquiry'] ?: null;
                    $enquiry_data['created_by_user'] = user_authenticate();
                    $enquiry_data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
                    
                    $this->db->where(array('call_history_id' => $call_history_id, 'enquiry_week_day' => $post['availability_week_days'][$key]));
                    $query = $this->db->get('service_enquiry');
                    $this->db->flush_cache();
                    if ($query->num_rows() > 0) {
                        $enquiry_data['deleted_at'] = null;
                        $this->db->where(array('call_history_id' => $call_history_id, 'enquiry_week_day' => $post['availability_week_days'][$key]));
                        $this->db->update('service_enquiry', $enquiry_data);
                    } else {
                        $enquiry_data['created_at'] = date('Y-m-d H:i:s');
                        $enquiry_data['updated_at'] = date('Y-m-d H:i:s');
                        $this->db->insert('service_enquiry', $enquiry_data);
                    }
                   
                    // $this->call_management_model->save_service($enquiry_data);
                    // $this->db->insert('service_enquiry', $enquiry_data);
                }
            }
            $this->db->trans_commit();
            $respose['status'] = "success";
            $respose['message'] = "Enquiry updated successfully.";
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }

    }

    public function update_enquiry_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        $call_history_id = $post['call_history_id'];
        // print_r($post);die();
        log_message('error', 'Post datas' . json_encode($post));
        try {
            $this->db->trans_begin();
            $is_maidflagged = $this->schedule_model->is_maidflagged($post['maid_id'], $post['customer_id']);
            // print_r($is_maidflagged);die();

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
          
            $enquiry_data['call_history_id'] = $call_history_id;
            $enquiry_data['enquiry_type_id'] = $post['caller_enquiry_typ_id'] ?: null;
            $enquiry_data['customer_id'] = $post['enquiry_customer_id'];
            $enquiry_data['date_from'] = $post['enquiry_booking_date'] ? DateTime::createFromFormat('d/m/Y', $post['enquiry_booking_date'])->format('Y-m-d') : null;
            $enquiry_data['time_from'] = $post['enquiry_time_from'] ?: null;
            $enquiry_data['time_to'] = $post['enquiry_time_to'] ?: null;
            $enquiry_data['total_hours'] = $post['working_hours'][$key];
            $enquiry_data['enquiry_customer_name'] = $post['enquiry_customer_name'];
            $enquiry_data['enquiry_customer_phoneno'] = $post['enquiry_customer_phoneno'] ?: null;
            $enquiry_data['enquiry_customer_email'] = $post['enquiry_customer_email'] ?: null;
            $enquiry_data['enquiry_notes'] = $post['enquiry_notes'] ?: null;
            $enquiry_data['service_type_id'] = $post['service_id'] ?: null;
            $enquiry_data['refer_source_id'] = $post['enquiry_source_id'] ?: null;
            if (isset($post['outside_service_checked']) && $post['outside_service_checked'] == "Y") {
                $customer_service_area = $post['outside_service_area'];
            }
            if (isset($post['outside_new_area'])) {
                $new_service_area = $post['outside_new_area'];
            }
            $enquiry_data['new_service_area'] = $new_service_area ?: null;
            $enquiry_data['customer_service_area'] = $customer_service_area ?: 0;
            $enquiry_data['area_id'] = $post['area_id'] ?: null;
            $enquiry_data['location_id'] = $post['location_id'] ?: null;
            $enquiry_data['maid_id'] = $post['maidId'] ?: null;
            $enquiry_data['enquiry_status'] = "Saved" ?: null;
            $enquiry_data['booking_type'] = $post['booking_type_enquiry'] ?: null;
            $enquiry_data['created_by_user'] = user_authenticate();
            $enquiry_data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
         
            $this->db->where('call_history_id', $call_history_id);
            $this->db->update('service_enquiry', $enquiry_data);
            // $this->call_management_model->save_service($data);
            $this->db->trans_commit();
            $respose['status'] = "success";
            $respose['message'] = "Enquiry updated successfully.";
            die(json_encode($respose, JSON_PRETTY_PRINT));
           
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }

    }

    public function suspend_date_range_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('delete_date_from');
            $delete_date_from = $this->input->post('delete_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('delete_date_from'))->format('Y-m-d') : null;
            $delete_date_to = $this->input->post('delete_date_to') ? DateTime::createFromFormat('d/m/Y', $this->input->post('delete_date_to'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $delete_date_from)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $delete_date_from);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                    // set update data
                    $date_range = $delete_date_from .' - '. $delete_date_to;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $delete_date_from;
                    $booking_deleted_data['delete_to_date'] =  $delete_date_to;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_date';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['call_history_id'] = $call_history_id;
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $delete_date_from .' - '. $delete_date_to;
                    
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);


                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $delete_date_from;
                    $booking_deleted_data['delete_to_date'] =  $delete_date_to;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_date';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
            
                    $response['call_history_id'] = $call_history_id;
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspend_date_range_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remark');
            $delete_date_from = $this->input->post('delete_date_from');
            // print_r($delete_date_from);de();
            $delete_date_from = DateTime::createFromFormat('d/m/Y', $delete_date_from)->format('Y-m-d');
            // print_r($delete_date_from);die();
            $delete_date_to = $this->input->post('delete_date_to');
            $delete_date_to = DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d');
            //    print_r($delete_date_from);
            // print_r($delete_date_to);die();
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            // $this->db->select("ds.*")
            //     ->from('day_services as ds')
            //     ->where('ds.booking_id', $booking_id)
            //     ->where('ds.service_date', $service_date);
            // $query = $this->db->get();
            // $day_service = $query->row();
            // $start = strtotime($delete_date_from);
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings->service_end == 1 && $delete_date_from > $bookings->service_end_date  )
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if($bookings->service_end == 1 && $delete_date_to > $bookings->service_end_date)
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_from) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_to) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            
            $start = strtotime($delete_date_from);
            $weekday = $bookings->service_week_day;
            $end = strtotime($delete_date_to);
            $weekdays = array();
            // $weekdays = array();
            while (date("w", $start) != $weekday) {
                $start += 86400;
            }
            while ($start <= $end)
            {
                $weekdays[] = date('Y-m-d', $start);
                $start += 604800;
            }
            if (empty($weekdays)) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            $day_service = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
            ->where_in('ds.service_date', $weekdays)->get()->result();
            // print_r($day_service);die();
            /***************************************************************** */
            
            $response['day_service'] = $day_service;
            // if(sizeof($result) > 0)
            if (sizeof($day_service) > 0){
                $delete_b_fields = [];
                foreach ($day_service as $key => $dayservice) {
                    if ($daservice->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 3) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if(is_any_ongoing_day_service_for_booking_by_service_date($booking_id,$service_date)){
                    //     exit('ongoing');
                    // }
                    // $get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_id, $service_date);
                    // if (count($get_dayservice_details) > 0) {
                    //     if (user_authenticate_isadmin() != 'Y' && $get_dayservice_details->odoo_package_activity_status == 1) {
                    //         echo 'odoorefresh';
                    //         exit();

                    //     }
                    // }
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                    if (user_authenticate() != 1) {
                        if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                            echo 'locked';
                            exit();
                        }
                    }
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);
                    $date_range = $delete_date_from .' - '. $delete_date_to ;
                   $delete_from_date =  current($weekdays);
                   $delete_to_date =  end($weekdays);
                    if (isset($d_booking->booking_id)) {
                        if ($d_booking->booking_type != 'OD') {
                            $delete_b_fields[$key]['booking_id'] = $booking_id;
                            $delete_b_fields[$key]['remarks'] = $remarks;
                            $delete_b_fields[$key]['service_date'] = $dayservice->service_date;
                            $delete_b_fields[$key]['day_service_id'] = $dayservice->day_service_id;
                            $delete_b_fields[$key]['delete_from_date'] = $delete_from_date;
                            $delete_b_fields[$key]['delete_to_date'] = $delete_to_date;
                            $delete_b_fields[$key]['added_datetime'] = date('Y-m-d H:i:s');
                            $delete_b_fields[$key]['delete_date_range'] = $date_range;
                            $delete_b_fields[$key]['deleted_by'] = user_authenticate();
                            $delete_b_fields[$key]['delete_booking_type'] = 'Suspend';
                            $delete_b_fields[$key]['delete_status'] = 'suspend_date';
                            $delete_b_fields[$key]['call_history_id'] = $call_history_id;
                            $delete_b_fields[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                        }
                    }
                }
            //  print_r($delete_b_fields);die();
                $this->db->insert_batch('booking_deletes', $delete_b_fields);
                $response['call_history_id'] = $call_history_id;
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
           
            else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;

                $start = strtotime($delete_date_from);
                $weekday = $booking->service_week_day;
                // print_r($start);die();
                $end = strtotime($delete_date_to);
                $weekdays = array();
                while (date("w", $start) != $weekday) {
                    $start += 86400;
                }
                while ($start <= $end)
                {
                    $weekdays[] = date('Y-m-d', $start);
                    $start += 604800;  
                }
                if (empty($weekdays)) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                $day_service_data = [];
                foreach ($weekdays as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $booking->customer_id;
                    $day_service_data[$key]['maid_id'] = $booking->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $booking->time_from;
                    $day_service_data[$key]['time_to'] = $booking->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $booking->service_type_id;
                    $day_service_data[$key]['customer_name'] = $booking->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $booking->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
                // print_r($day_service_data[$key]['service_date']);die();

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

            $date_range = $delete_date_from .' - '. $delete_date_to ;
            $booking_deleted_data = [];
            $delete_from_date =  current($weekdays);
            $delete_to_date =  end($weekdays);
            $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
            $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
            $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
            $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
            $call_history_data['attended_by']     = user_authenticate();
            $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
            // print_r($call_history_data);die();
            $call_history_id = $this->call_management_model->save_call_history($call_history_data);
            foreach ($day_services as $key => $day_service) {
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $date_range;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';
                $booking_deleted_data[$key]['delete_status'] = 'suspend_date';
                $booking_deleted_data[$key]['call_history_id'] = $call_history_id;
                $booking_deleted_data[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;

            }
                
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();  
                $response['call_history_id'] = $call_history_id; 
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspend_one_day_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $service_date)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);
                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;

                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    $this->db->trans_commit();
                    // print_r($booking_delete_id);die();
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $service_date .' - '. $service_date;
                    
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;

                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspend_one_day_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('service_date');
            $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_week_day = date('w', strtotime($service_date));
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_week_day', $service_week_day)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                if ($bookings->service_end == 1 && $service_date > $bookings->service_end_date  )
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                // if($bookings->service_end == 1 && $service_date > $bookings->service_end_date)
                // {
                //     $response['status'] = "failed";
                //     $response['message'] = "Booking not available in this date.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }

                if ($bookings->service_start_date > $service_date) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not started in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $service_date;
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // print_r($day_service_id);die();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);
    
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] =  $service_date;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    // print_r($booking_deleted_data);die();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    $this->db->trans_commit();
                        
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function cancel_permeantly_od()
    {
        

        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*")
                ->from('day_services as ds')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
        //  print_r($day_service);die();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {
                // print_r("helloo");die();
                // day service entry exist
                if ($day_service->service_status == 1) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                if ($day_service->service_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                // if ($day_service->service_status == 3) {
                //     $response['status'] = "failed";
                //     $response['message'] = "schedule can't be deleted.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                if ($day_service->dispatch_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "Confirmed schedule can't be changed.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                $call_history_data['attended_by']     = user_authenticate();
                $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                $moved_to_booking_id = $booking_id;
                $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');
                // set update data
                $date_range = $service_date .' - '. $service_date;
                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] = $this->input->post('service_date');
                $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Cancel';
                $booking_deleted_data['delete_status'] = 'cancel_permeantly';
                $booking_deleted_data['delete_date_range'] = $date_range;
                $booking_deleted_data['call_history_id'] = $call_history_id;
                $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                /***************************************************************** */
            //    print_r($booking_deleted_data);die();
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                
                $response['call_history_id'] = $call_history_id; 
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                // echo'<pre>';
                // print_r($booking);die();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                // $booking->time_from = $this->input->post('time_from');
                // $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $booking->time_from;
                $day_service_data['time_to'] = $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $booking->service_start_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $booking->cleaning_material;
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                $day_service_data['_total_discount'] = $booking->_total_discount;
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                // print_r($day_service_data);die();
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                $date_range = $service_date .' - '. $service_date;
                
                $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                $call_history_data['attended_by']     = user_authenticate();
                $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');

                $call_history_id = $this->call_management_model->save_call_history($call_history_data);
                
                $moved_to_booking_id = $booking_id;
                $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');

                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                // $booking_deleted_data['day_service_id'] = $day_service_id;
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Cancel';
                $booking_deleted_data['delete_status'] = 'cancel_permeantly';
                $booking_deleted_data['delete_date_range'] = $date_range;
                $booking_deleted_data['call_history_id'] = $call_history_id;
                $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                // print_r($booking_deleted_data);die();

                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                    
                $response['call_history_id'] = $call_history_id; 
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
                // // update reference id END
                // $response['status'] = "success";
                // $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function cancel_permeantly_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remark');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            $moved_to_booking_id = $booking_id;
            $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
            if (user_authenticate() != 1) {
                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
            }
            if (isset($d_booking->booking_id)) {
                $newsearchdate = date('Y-m-d', strtotime($schedule_date));
                $enddate = $d_booking->service_start_date;
                if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');
                } else {
                    if ($d_booking->booking_type == 'WE') {

                        if ($d_booking->service_start_date > $newsearchdate)
                        {
                            $current_date = strtotime($d_booking->service_start_date);
                            $current_date = strtotime('-7 days', $current_date);
                            $delete_datee = date('Y-m-d', $current_date);
                            // print_r($deletedatee);die();
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $delete_datee);
                            if (count($check_buk_delete) > 0) {

                            } else {
                                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'service_actual_end_date' => $delete_datee, 'service_end_date' => $delete_datee, 'service_end' => 1), 'Delete');
                                
                            }
                        } else{
                            $current = strtotime($newsearchdate);
                            $last = strtotime($enddate);
    
                            
                            while ($current >= $last) {
                                $current = strtotime('-7 days', $current);
                                $deletedatee = date('Y-m-d', $current);
                                //  print_r($deletedatee);die();
                                $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                if (count($check_buk_delete) > 0) {
    
                                } else {
                                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                    break;
                                }
                            }
                        }
                       
                    }
                }
                $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                $call_history_data['attended_by']     = user_authenticate();
                $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                $date_range = $service_date .' - '. $service_date;
                $delete_b_fields = array();
                $delete_b_fields['booking_id'] = $booking_id;
                $delete_b_fields['remarks'] = $remarks;
                $delete_b_fields['service_date'] = $service_date;
                // $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_from_date'] = $service_date;
                $delete_b_fields['delete_to_date'] = $service_date;
                $delete_b_fields['added_datetime'] = date('Y-m-d H:i:s');
                $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_booking_type'] = 'Cancel';
                $delete_b_fields['delete_status'] = 'cancel_permeantly';
                $delete_b_fields['delete_date_range'] = $date_range;
                $delete_b_fields['call_history_id'] = $call_history_id;
                $delete_b_fields['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                $this->bookings_model->add_booking_delete($delete_b_fields);

                $response['call_history_id'] = $call_history_id; 
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function update_status_enquiry() {
        log_message('error',json_encode($this->input->post()));
        if ($this->input->is_ajax_request()) {

            $call_history_id = $this->input->post('call_history_id');
            $status = $this->input->post('enquiry_status');

            if($status==''){
                $response['status'] = 'error';
                $response['message'] = 'Status Cannot be Empty';
                return $response;
            }else{
            $data=array(
                'status'=>$status,
            );

            $enquiry = $this->db->select('se.*')->from('service_enquiry as se')->where('call_history_id', $call_history_id)->get()->result();
           
                $enquiryIDS =  array_column($enquiry,'service_enquiry_id');
               
                $this->db->where(array('call_history_id' => $call_history_id));
                $this->db->update('service_enquiry', array('enquiry_status' => $status));
                    // if ($result) {
                $response['status'] = 'success';
                $response['message'] = 'Status updated successfully.';
            // } else {
            //     $response['status'] = 'error';
            //     $response['message'] = 'Failed to update status.';
            // }
         }
            echo json_encode($response);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Direct Access Not Allowed';
            
            echo json_encode($response);
        }
    }
    public function cancel_one_day_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $service_date)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();

                $time_from = $bookings->time_from;
                $time_to = $bookings->time_to;
                $timestamp_from = strtotime($time_from);
                $timestamp_to = strtotime($time_to);
                // Calculate the difference in seconds
                $time_difference_seconds = ($timestamp_to - $timestamp_from) / 60;
                // Convert seconds to minutes
                $time_difference_minutes = $time_difference_seconds / 60;
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    $pay_customer1 = $this->input->post('pay_customer1');
                    if (isset($pay_customer1) && $pay_customer1 == "Y") {
                        $this->db->set(array('charge_customer' => 1))
                        ->where('day_service_id', $day_service->day_service_id)
                        ->update('day_services');
                        // $day_service_data['charge_customer'] = 1;
                    } 
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);
                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;  
                    $pay_empoyee = $this->input->post('pay_employee1');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $this->input->post('booking_customer_id');
                        $maid_hour_data['service_date'] = $this->input->post('service_date');
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $time_difference_minutes;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }

                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    $this->db->trans_commit();
                    // print_r($booking_delete_id);die();
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $pay_customer1 = $this->input->post('pay_customer1');
                    if (isset($pay_customer1) && $pay_customer1 == "Y") {
                        $day_service_data['charge_customer'] = 1;
                    } 
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $service_date .' - '. $service_date;
                    
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;  
                    $pay_empoyee = $this->input->post('pay_employee1');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $this->input->post('booking_customer_id');
                        $maid_hour_data['service_date'] = $this->input->post('service_date');
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $time_difference_minutes;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }

                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function cancel_one_day_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('service_date');
            $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_week_day = date('w', strtotime($service_date));
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_week_day', $service_week_day)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                if ($bookings->service_end == 1 && $service_date > $bookings->service_end_date  )
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                // if($bookings->service_end == 1 && $service_date > $bookings->service_end_date)
                // {
                //     $response['status'] = "failed";
                //     $response['message'] = "Booking not available in this date.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }

                if ($bookings->service_start_date > $service_date) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not started in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                $time_from = $bookings->time_from;
                $time_to = $bookings->time_to;
                $timestamp_from = strtotime($time_from);
                $timestamp_to = strtotime($time_to);
                // Calculate the difference in seconds
                $time_difference_seconds = ($timestamp_to - $timestamp_from) / 60;
                // Convert seconds to minutes
                $time_difference_minutes = $time_difference_seconds / 60;
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    $pay_customer = $this->input->post('pay_customer');
                    if (isset($pay_customer) && $pay_customer == "Y") {
                        $this->db->set(array('charge_customer' => 1))
                        ->where('day_service_id', $day_service->day_service_id)
                        ->update('day_services');
                        // $day_service_data['charge_customer'] = 1;
                    } 
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);

                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;   
                    $pay_empoyee = $this->input->post('pay_employee');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $this->input->post('booking_customer_id');
                        $maid_hour_data['service_date'] = $service_date;
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $time_difference_minutes;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }

                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $service_date;
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $pay_customer = $this->input->post('pay_customer');
                    if (isset($pay_customer) && $pay_customer == "Y") {
                        $day_service_data['charge_customer'] = 1;
                    } 
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // print_r($day_service_id);die();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s",$day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $call_history_data['customer_id']    = $this->input->post('booking_customer_id');
                    $call_history_data['mobile_number']   = $this->input->post('customer_phoneno') ?: null;
                    $call_history_data['enquiry_type_id'] = $this->input->post('caller_service_typ_id') ?: null;
                    $call_history_data['added_date_time'] = date('Y-m-d H:i:s');
                    $call_history_data['attended_by']     = user_authenticate();
                    $call_history_data['created_at'] = $call_history_data['updated_at'] = date('Y-m-d H:i:s');
                    // print_r($call_history_data);die();
                    $call_history_id = $this->call_management_model->save_call_history($call_history_data);
    
                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;   
                    $pay_empoyee = $this->input->post('pay_employee');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $this->input->post('booking_customer_id');
                        $maid_hour_data['service_date'] = $service_date;
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $working_hours;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }

                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] =  $service_date;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['call_history_id'] = $call_history_id;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    // print_r($booking_deleted_data);die();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    $this->db->trans_commit();
                        
                    $response['call_history_id'] = $call_history_id; 
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    
}