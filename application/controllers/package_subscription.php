<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Package_subscription extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if (!is_user_loggedin()) {
			redirect('logout');
		}
		if (!user_permission(user_authenticate(), 3)) {
			show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
		}
		$this->load->model('customers_model');
		$this->load->model('package_model');
		$this->load->model('package_subs_model');
		$this->load->model('settings_model');
	}
	public function index()
	{
		$data = array();
		$status = $this->input->get('status');
		$data['packages'] = $this->package_subs_model->get_all_subscriptions($status);
		$data['status'] = $status;
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_subscription_list', $data, TRUE);
		$layout_data['page_title'] = 'Package Subscriptions';
		$layout_data['meta_description'] = 'Package Subscriptions';
		$layout_data['css_files'] = array('demo.css');
		$layout_data['external_js_files'] = array();
		$layout_data['maids_active'] = '1';
		$layout_data['js_files'] = array('jquery.dataTables.min.js');
		$this->load->view('layouts/default', $layout_data);
	}
	public function new()
	{
		$data = array();
		$data['packages'] = $this->package_model->get_all_packages(1);
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_subscription_new', $data, TRUE);
		$layout_data['page_title'] = 'New Subscription';
		$layout_data['meta_description'] = 'New Subscription';
		$layout_data['css_files'] = array('demo.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array();
		$this->load->view('layouts/default', $layout_data);
	}
	public function edit()
	{
		$data['data'] = $this->package_subs_model->get_subscription_for_edit($this->uri->segment(3));
		if (!$data['data']['id']) {
			redirect('/package_subscription?not_found');
		}
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_subscription_new', $data, TRUE);
		$layout_data['page_title'] = 'Update Package Subscription';
		$layout_data['meta_description'] = 'Update Package Subscription';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array();
		$this->load->view('layouts/default', $layout_data);
	}
	public function view()
	{
		$data['data'] = $this->package_subs_model->get_subscription_for_edit($this->uri->segment(3));
		if (!$data['data']['id']) {
			redirect('/package_subscription?not_found');
		}
		$data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
		$layout_data['content_body'] = $this->load->view('package_subscription_new', $data, TRUE);
		$layout_data['page_title'] = 'View Package Subscription';
		$layout_data['meta_description'] = 'View Package Subscription';
		$layout_data['css_files'] = array();
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array();
		$this->load->view('layouts/default', $layout_data);
	}
	public function save()
	{
		if ($this->input->post('id')) {
			// update old
			if ($this->package_subs_model->update_subscription($this->input->post())) {
				// save success
				redirect('/package_subscription?status=1&success');
			} else {
				// save failed
				redirect('/package_subscription?status=1&failed');
			}
		} else {
			// save new
			if ($this->package_subs_model->save_subscription($this->input->post())) {
				// save success
				redirect('/package_subscription?status=1&success');
			} else {
				// save failed
				redirect('/package_subscription?status=1&failed');
			}
		}
	}
	public function disable()
	{
		if ($this->package_subs_model->change_status($this->input->post('id'), 0)) {
			redirect('/package_subscription?status=' . $this->input->get('status'));
		} else {
			redirect('/package_subscription?status=' . $this->input->get('status'));
		}
	}
	public function enable()
	{
		if ($this->package_subs_model->change_status($this->input->post('id'), 1)) {
			redirect('/package_subscription?status=' . $this->input->get('status'));
		} else {
			redirect('/package_subscription?status=' . $this->input->get('status'));
		}
	}
	public function customer_search()
	{
		return $this->package_subs_model->customer_search($this->input->get('query'));
	}
	public function package_search()
	{
		return $this->package_subs_model->package_search($this->input->get('query'));
	}
}
