<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Vehicle extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('vehicle_model');
    }
    public function index()
    {
    }
    public function list_vehicles()
    {
        $data['settings'] = $this->settings_model->get_settings();
        $data['rows'] = $this->vehicle_model->vehicles_list(array('v.deleted_at' => null));
        $layout_data['content_body'] = $this->load->view('vehicle/vehicle-list', $data, true);
        $layout_data['page_title'] = 'Vehicles';
        $layout_data['meta_description'] = 'Vehicles List';
        $layout_data['css_files'] = array('demo.css','datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js',"vehicles.js");
        $this->load->view('layouts/default', $layout_data);
    }
    public function save_vehicle()
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->trans_begin();
        try {
            $data = $this->input->post();
            if (empty($this->input->post('vehicle_id'))) {
                unset($data['vehicle_id']);
                $data['created_by'] = user_authenticate();
                $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
                $this->vehicle_model->save_vehicle($data);
                $respose['message'] = "Vehicle saved successfully.";
            } else {
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->vehicle_model->update_vehicle($this->input->post('vehicle_id'),$data);
                $respose['message'] = "Vehicle updated successfully.";
            }
            $this->db->trans_commit();
            $respose['status'] = true;
            $respose['data'] = ['id' => $id];
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
        }
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function get_vehicle_data()
    {
        header('Content-Type: application/json; charset=utf-8');
        $vehicle = $this->vehicle_model->get_vehicle_data_by_id($this->input->get('id'));
        echo json_encode($vehicle, JSON_PRETTY_PRINT);
    }
    public function assign_driver()
    {
        header('Content-Type: application/json; charset=utf-8');
        $vehicle = $this->vehicle_model->get_vehicle_data_by_id($this->input->post('assign_vehicle_id'));
        try {
            $post = $this->input->post();
            $data['driver_id'] = $post['employee_id'];
            $data['driver_assign_note'] = $post['driver_assign_note'];
            $data['driver_assigned_by'] = user_authenticate();
            $data['driver_assigned_at'] = $data['updated_at'] = date('Y-m-d H:i:s');
            $id = $this->vehicle_model->driver_assign($this->input->post('assign_vehicle_id'), $data);
            $this->db->trans_commit();
            $respose['status'] = true;
            $respose['message'] = "Key returned successfully.";
            $respose['data'] = ['id' => $post['return_access_id']];
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
        }
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function get_drivers_for_assign()
    {
        $search = $this->input->get('search');
        $this->db->select('m.maid_id as id,m.maid_name as text')
            ->from('maids as m');
        if (isset($search)) {
            $this->db->where("(maid_name LIKE '%" . $search . "%' )", null, false);
        }
        $this->db->where('m.employee_type_id', 2)
            ->limit(50);
        $query = $this->db->get();
        echo json_encode($query->result(), true);
    }
}
