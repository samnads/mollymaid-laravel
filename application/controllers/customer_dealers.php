<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_dealers extends CI_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if(!user_permission(user_authenticate(), 3))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('settings_model');
        $this->load->library('upload');
        $this->load->helper('google_api_helper');
		$this->load->helper('curl_helper');
        $this->load->model('customer_dealer_model');
    }

    public function index(){

        //echo 'God is Love';

        if($this->input->post('dealer_sub'))
        {
            $dealer_name = $this->input->post('dealer_name');
            $trn = $this->input->post('trn');
            $commision = $this->input->post('commision');
           // $sort_priority = $this->input->post('sort_priority');
            $description = $this->input->post('description');
            $created_by = user_authenticate();
          
            $data = array(
                'dealer_name ' => $dealer_name ,
                'trn' => $trn,
                'commision' => $commision,
                'description' => $description,
                'created_by' =>$created_by,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $dealer_odoo_fields = array();
            $dealer_odoo_fields['name'] = $dealer_name;
            $dealer_odoo_fields['dealer_commission'] =  $commision;
            $dealer_odoo_fields['dealer_description'] = $description;
            $dealer_odoo_fields['trn_number'] = $trn;
            // print_r($zone_odoo_fields);die();
            $dealer_id = $this->customer_dealer_model->add_dealers($data);

            $odoo_dealer_id = $this->do_dealer_odoo_api($dealer_odoo_fields);
            $data1 = array('odoo_package_dealer_id' => $odoo_dealer_id,
            'odoo_package_dealer_status' => 1);
            $this->customer_dealer_model->update_dealers($data1,$dealer_id);
            // $this->customer_dealer_model->update_dealers($data,$dealer_id);

        }

        else if($this->input->post('dealer_edit'))
        {
            
            $dealer_id = $this->input->post('edit_dealerid');
            $dealer_name = $this->input->post('edit_dealername');
            $trn = $this->input->post('edit_trn');
            $commision = $this->input->post('edit_commision');
           
            $description = $this->input->post('edit_description');
            $created_by = user_authenticate();
            $data = array(
                'dealer_name ' => $dealer_name ,
                'trn' => $trn,
                'commision' => $commision,
                'description' => $description,
                'created_by' =>$created_by,
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $get_dealer_by_id = $this->customer_dealer_model->get_dealer_details($dealer_id);
            $this->customer_dealer_model->update_dealers($data,$dealer_id);

            // print_r($get_dealer_by_id[0]['odoo_package_dealer_id']);die();
            if (empty($get_dealer_by_id[0]['odoo_package_dealer_id'])) {
                $dealer_odoo_fields = array();
                $dealer_odoo_fields['name'] = $dealer_name;
                $dealer_odoo_fields['dealer_commission'] =  $commision;
                $dealer_odoo_fields['dealer_description'] = $description;
                $dealer_odoo_fields['trn_number'] = $trn;
                $odoo_dealer_id = $this->do_dealer_odoo_api($dealer_odoo_fields);
                $data1 = array('odoo_package_dealer_id' => $odoo_dealer_id,
                'odoo_package_dealer_status' => 1);
                $this->customer_dealer_model->update_dealers($data1,$dealer_id);
            }

        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['customer_dealers'] = $this->customer_dealer_model->get_customer_dealers();
        $layout_data['content_body'] = $this->load->view('customer-dealers/customer_dealers_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Dealers List';
        $layout_data['meta_description'] = 'customer dealers';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();  
        $layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function do_dealer_odoo_api($data = array())
    {
        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['name'];
        $post['params']['dealer_commission'] = $data['dealer_commission'];
        $post['params']['dealer_description'] = $data['dealer_description'];
        $post['params']['trn_number'] = $data['trn_number'];
        // print_r($post);
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') ."create_dealer";
        $login_check = curl_api_service($post_values, $url);
        // print_r($login_check);die();
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_dealer_id = $returnData->result->response->id;
        }
    }

    public function edit_dealer()
    {
        $customer_dealer_id = $this->input->post('customer_dealer_id');
        $result = $this->customer_dealer_model->get_dealer_details($customer_dealer_id);
        echo json_encode($result);
    }
    public function remove_dealer()
    {
        $customer_dealer_id = $this->input->post('customer_dealer_id');
        $data = $this->customer_dealer_model->delete_dealer(array('deleted_at' =>date('Y-m-d H:i:s')),$customer_dealer_id);

    }
}