<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Offers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('offer_model');
        $this->load->model('bookings_model');
        $this->load->helper('google_api_helper');
        $this->load->helper('curl_helper');
        $this->load->model('settings_model');
        $this->load->helper(array('form', 'url'));
    }
    public function index()
    {
        $settings = $this->settings_model->get_settings();
        if (!user_permission(user_authenticate(), 8)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url('dashboard') . '">Go to Dashboard</a>', 401, 'Access denied');
        }
        else if ($this->input->post('offer_sub')) {
            $offer_heading = $this->input->post('offer_heading');
            $offer_type = $this->input->post('offer_type');
            $offer_subheading = $this->input->post('offer_subheading');
            $offer_content = $this->input->post('offer_content');
            if ($offer_type == 'Image Type') {
                $offer_image = time() . '_' . rand(999, 9999) . '.gif';
                $offer_subheading = '';
                $offer_content = '';
                $config['upload_path'] = DIR_UPLOAD_OFFER_IMG;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = $offer_image;
                $config['max_size'] = 4000;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('offer_image')) {
                    $error = array('error' => $this->upload->display_errors());
                    echo '<script>alert("' . implode(',', $error) . '");</script>';
                }
                $android_title = "A new offer launched on " . $settings->site_name;
                $offer_image_android = OFFER_IMG_ANDROID_ABSOLUTE_URL . $offer_image;
            } else { $offer_image = '';
                $android_title = $offer_heading;
                $offer_image_android = '';
            }
            $data = array(
                'offer_heading' => $offer_heading,
                'offer_type' => $offer_type,
                'offer_subheading' => $offer_subheading,
                'offer_content' => $offer_content,
                'offer_image' => $offer_image,
            );
            $this->offer_model->add_offer($data);
            ////Android Push////
            $payload = array();
            $payload['isfeedback'] = false;
            $res = array();
            $res['title'] = $android_title;
            $res['is_background'] = false;
            $res['body'] = $offer_content;
            $res['image'] = $offer_image_android;
            $res['hasRedirect'] = "false";
            $res['payload'] = $payload;
            $res['timestamp'] = date('Y-m-d G:i:s');
            $res['click_action'] = "com.emaid.elitemaids";
            $res['badge'] = 1;
            //$regId = $device_id;
            $fields = array(
                'notification' => $res,
                'data' => $res,
                "aps" => $res,
                "condition" => "'offerEMDEMO' in topics",
            );
            android_customer_offer_push($fields);
        }
        else if ($this->input->post('offer_edit')) {
            $id_offers_sb = $this->input->post('edit_id_offers_sb');
            $offer_heading = $this->input->post('edit_offer_heading');
            $offer_type = $this->input->post('edit_offer_type');
            $offer_subheading = $this->input->post('edit_offer_subheading');
            $offer_content = $this->input->post('edit_offer_content');
            if ($offer_type == 'Image Type') {
                $offer_image = time() . '_' . rand(999, 9999) . '.gif';
                $offer_subheading = null;
                $offer_content = null;
                $config['upload_path'] = DIR_UPLOAD_OFFER_IMG;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = $offer_image;
                $config['max_size'] = 4000;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('offer_image')) {
                    redirect('offers?error='.$this->upload->display_errors(), 'refresh');
                }
            } else { $offer_image = '';}
            $data = array(
                'offer_heading' => $offer_heading,
                'offer_type' => $offer_type,
                'offer_subheading' => $offer_subheading,
                'offer_content' => $offer_content,
                'offer_image' => $offer_image,
            );
            $result = $this->offer_model->update_offer($data, $id_offers_sb);
        }
        $data['offers'] = $this->offer_model->get_offers();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('sb_offers', $data, true);
        $layout_data['page_title'] = 'offers';
        $layout_data['meta_description'] = 'offers';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['settings_active'] = '1';
        $layout_data['js_files'] = array('mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function remove_offers()
    {
        $id_offers_sb = $this->input->post('id_offers_sb');
        $data = $this->offer_model->remove_offer($id_offers_sb);
    }
    public function edit_offers()
    {
        $id_offers_sb = $this->input->post('id_offers_sb');
        $result = $this->offer_model->get_offer_details($id_offers_sb);
        echo json_encode($result);
    }
    public function change_status()
    {
        $this->offer_model->change_status();
    }
    public function android_test()
    {
        $payload = array();
        $payload['isfeedback'] = false;

        $res = array();
        $res['title'] = 'Title 123';
        $res['is_background'] = false;
        $res['body'] = 'Test Manual 29-08-2021 02:07 PM';
        $res['image'] = "";
        $res['hasRedirect'] = "false";
        $res['payload'] = $payload;
        $res['timestamp'] = date('Y-m-d G:i:s');
        $res['badge'] = 1;
        //$regId = $device_id;
        $fields = array(
            'notification' => $res,
            'data' => $res,
            "aps" => $res,
            "condition" => "'offerDemo' in topics",
        );
        $result = android_customer_offer_push($fields);
        print_r($result);
    }
}
