<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Customer_mollymaid extends CI_Controller
{
    public function __construct()
    {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->library('form_validation');
        $this->load->model('settings_model');
        $this->load->model('customer_mollymaid_model');
        $this->load->model('customers_model');
        $this->load->model('customer_access_model');
        $this->load->helper('form');
        $this->load->helper('curl_helper');
        $this->load->helper('google_api_helper');
    }
    public function new_customer()
    {
        $data['settings'] = $this->settings_model->get_settings();
        $data['zones'] = $this->customer_mollymaid_model->get_zones();
        $data['areas'] = $this->customer_mollymaid_model->get_areas();
        $data['locations'] = $this->db->select('l.location_id,l.location_name,l.area_id')
            ->from('locations as l')
            ->where('l.deleted_at', null)
            ->order_by('l.location_name', 'ASC')->get()->result();
        $data['payment_frequencies'] = $this->customer_mollymaid_model->get_payment_frequencies();
        $data['refer_sources'] = $this->customer_mollymaid_model->get_customer_sources();
        $data['residence_types'] = $this->customer_mollymaid_model->residence_types();
        $data['payment_modes'] = $this->customer_mollymaid_model->payment_modes();
        $data['genders'] = $this->customer_mollymaid_model->get_genders();
        $data['landmarks'] = $this->db->select('lm.landmark_id,lm.landmark_name')
            ->from('landmarks as lm')
            ->where('lm.deleted_at', null)
            ->order_by('lm.landmark_name', 'ASC')->get()->result();
        $data['pref_maids'] = $this->db->select('m.maid_id,m.maid_name')->from('maids as m')->where('m.maid_status', 1)->where('m.employee_type_id', 1)->order_by('m.maid_id', 'ASC')->get()->result();
        $data['flaged_maids'] = $this->db->select('m.maid_id,m.maid_name')->from('maids as m')->where('m.maid_status', 1)->where('m.employee_type_id', 1)->order_by('m.maid_id', 'ASC')->get()->result();
        $data['customer_companies'] = $this->customer_mollymaid_model->get_customer_companies();
        $data['customer_dealers'] = $this->customer_mollymaid_model->get_customer_dealers();
        $layout_data['content_body'] = $this->load->view('customer/new-customer', $data, true);
        $layout_data['page_title'] = 'New Customer';
        $layout_data['meta_description'] = 'New Customer';
        $layout_data['css_files'] = array('jquery.flexdatalist.css', 'datepicker.css', 'toastr.min.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.flexdatalist.js', 'bootstrap-datepicker.js', 'cropper.js', 'customer-mollymaid.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function edit_customer()
    {
        $customer_id = $this->uri->segment(3);
        /************************************************ */
        // data for prefills
        $data['customer'] = $this->db->select('c.*')->from('customers as c')->where('c.customer_id', $customer_id)->where('c.customer_status', 1)->get()->row();
        $data['customer_addresses'] = $this->db->select('ca.*,z.zone_id,z.zone_name, a.area_id,a.area_name')->from('customer_addresses as ca')
            ->join('areas as a', 'ca.area_id = a.area_id', 'left')
            ->join('zones as z', 'z.zone_id = a.zone_id', 'left')
            ->where('ca.customer_id', $customer_id)
            // ->where('ca.address_status', 0)
            ->get()->result();
        $data['pref_maid_ids'] = array_column($this->db->select('cpm.*')->from('customer_pref_maids as cpm')->where('cpm.customer_id', $data['customer']->customer_id)->get()->result(), 'maid_id');
        $data['flag_maid_ids'] = array_column($this->db->select('fmc.*')->from('flaged_maids_customers as fmc')->where('fmc.customer_id', $data['customer']->customer_id)->get()->result(), 'maid_id');
        $data['active_accesses'] = $this->db->select('cao.id,
                cao.customer_access_id,
                cat.name as access_type,
                c.customer_name,
                cao.received_at,
                cao.returned_at,
                ca.code,
                ur.user_fullname as received_user,
                urt.user_fullname as returned_user,
                rbe.maid_name as returned_by_employee')
            ->from('customer_access_owned as cao')
            ->join('customers as c', 'cao.customer_id = c.customer_id', 'left')
            ->join('customer_access as ca', 'cao.customer_access_id = ca.id', 'left')
            ->join('customer_access_types as cat', 'ca.customer_access_type = cat.id', 'left')
            ->join('users as ur', 'cao.received_by_user = ur.user_id', 'left')
            ->join('users as urt', 'cao.return_added_by = urt.user_id', 'left')
            ->join('maids as rbe', 'cao.returned_by_employee = rbe.maid_id', 'left')
            ->where('cao.deleted_at', null)
            ->where('cao.customer_id', $customer_id)
            ->order_by('cao.received_at', 'DESC')->get()->result();
        /************************************************ */
        $data['settings'] = $this->settings_model->get_settings();
        $data['zones'] = $this->customer_mollymaid_model->get_zones();
        $data['areas'] = $this->customer_mollymaid_model->get_areas();
        $data['locations'] = $this->customer_mollymaid_model->get_locations();
        $data['payment_frequencies'] = $this->customer_mollymaid_model->get_payment_frequencies();
        $data['refer_sources'] = $this->customer_mollymaid_model->get_customer_sources();
        $data['residence_types'] = $this->customer_mollymaid_model->residence_types();
        $data['payment_modes'] = $this->customer_mollymaid_model->payment_modes();
        $data['genders'] = $this->customer_mollymaid_model->get_genders();
        $data['landmarks'] = $this->customer_mollymaid_model->get_landmarks();
        $data['pref_maids'] = $this->customer_mollymaid_model->get_pref_maids();
        $data['flaged_maids'] = $this->customer_mollymaid_model->get_flaged_maids();
        $data['customer_companies'] = $this->customer_mollymaid_model->get_customer_companies();
        $data['customer_dealers'] = $this->customer_mollymaid_model->get_customer_dealers();
        $layout_data['content_body'] = $this->load->view('customer/new-customer', $data, true);
        $layout_data['page_title'] = 'Edit Customer - ' . $data['customer']->customer_name;
        $layout_data['meta_description'] = 'New Customer';
        $layout_data['css_files'] = array('jquery.flexdatalist.css', 'datepicker.css', 'toastr.min.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.flexdatalist.js', 'bootstrap-datepicker.js', 'cropper.js', 'customer-mollymaid.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function customer_list()
    {
        // log_message('error','post customer list '.json_encode($this->input->post()));
        $data['zones'] = $this->customer_mollymaid_model->get_zones();
        $data['areas'] = $this->customer_mollymaid_model->get_areas();
        $data['locations'] = $this->customer_mollymaid_model->get_locations();
        if ($this->input->post('customer_filter')) {
            $ctype = $this->input->post('filter_customer_type');
            $ptype = $this->input->post('filter_payment_type');
            $ztype = $this->input->post('filter_zone_type');
            $atype = $this->input->post('filter_area_type');
            $ltype = $this->input->post('filter_location_type');
            $status = $this->input->post('filter_status');
            $data['ctype'] = $this->input->post('filter_customer_type');
            $data['ptype'] = $this->input->post('filter_payment_type');
            $data['ztype'] = $this->input->post('filter_zone_type');
            $data['atype'] = $this->input->post('filter_area_type');
            $data['ltype'] = $this->input->post('filter_location_type');
            $data['status'] = $this->input->post('filter_status');
            $data['rows'] = $this->customer_mollymaid_model->get_filtered_customers($ctype, $ptype, $ztype, $atype, $ltype, $status);
        } else {
            $data['rows'] = $this->customer_mollymaid_model->customer_list();
            // print_r($data['rows']);die();
        }
        $data['settings'] = $this->settings_model->get_settings();
        // $data['rows'] = $this->customer_mollymaid_model->customer_list();
        $layout_data['content_body'] = $this->load->view('customer/list-customers', $data, true);
        $layout_data['page_title'] = 'Customers';
        $layout_data['meta_description'] = 'Customers';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', "access.js?v=" . time());
        $this->load->view('layouts/default', $layout_data);
    }
    public function update_customer()
    {
        header('Content-Type: application/json; charset=utf-8');
        $customer_id = $this->input->post('customer_id');
        $customer = $this->db->select('c.*')->from('customers as c')->where('c.customer_id', $customer_id)->where('c.customer_status', 1)->get()->row();
        $post = $this->input->post();
        // log_message('error', 'post customers update' . json_encode($post));

        $pref_maids =  $post['pref_maids'];
        $flaged_maids = $post['flaged_maid'];
        $intersection = array_intersect($pref_maids, $flaged_maids);
        if (!empty($intersection)) {
            echo json_encode(array('status' => 'error', 'message' => 'Preferred maids and flagged maids cannot be the same.'));
            return;
        }
        $this->db->trans_begin();
        try {
            /***************************************************************** */
            // update customer table
            $data['customer_type_id'] = $post['customer_type_id'];
            $data['customer_company_id'] = null;
            $data['customer_dealer_id'] = null;
            if ($post['customer_type_id'] == 2) {
                // company
                $data['customer_company_id'] = $post['customer_company_id'];
                $data['is_company'] = 'Y';
            } else if ($post['customer_type_id'] == 3) {
                // dealer
                $data['customer_dealer_id'] = $post['customer_dealer_id'];
                $data['is_company'] = 'N';
            }
            $data['customer_name'] = $post['customer_name'];
            $data['gender_id'] = $post['gender_id'] ?: null;
            $data['customer_nick_name'] = $post['customer_nick_name'] ?: null;
            $data['is_flag'] = $post['is_flagged'];
            if ($post['pet'] == 'Y') {
                $data['pet'] = $post['pet'];
                $data['pet_type'] = $post['pet_type'] ?: null;
            } else if ($post['pet'] == 'N') {
                $data['pet'] = 'N';
                $data['pet_type'] = null;
            }
            $data['customer_booktype'] = $post['customers_book_type'];
            $data['flag_reason'] = $post['flag_reason'];
            //$data['is_blocked_new_booking'] = $post['is_blocked_new_booking'] == 1 ? 1 : 0;
            $data['is_allowed_new_booking'] = $post['is_allowed_new_booking'];
            $data['blocked_new_booking_reason'] = $post['blocked_new_booking_reason'] ?: null;
            //$data['is_allowed_new_booking_reason'] = $post['blocked_new_booking_reason'] ?: null;
            $data['mobile_number_1'] = $post['mobile_number_1'];
            $data['customer_code'] = $post['customer_code'];
            $data['mobile_number_2'] = $post['mobile_number_2'] ?: null;
            $data['whatsapp_no_1'] = $post['whatsapp_no_1'] ?: null;
            $data['phone_number'] = $post['phone'] ?: null;
            $data['email_address'] = $post['email'] ?: null;
            $data['fax_number'] = $post['fax'] ?: null;
            $data['trnnumber'] = $post['trnnumber'] ?: null;
            $data['vatnumber'] = $post['vatnumber'] ?: null;
            $data['refer_source_id'] = $post['refer_source_id'] ?: null;
            $data['refer_source_note'] = $post['refer_source_note'];
            $data['company_name'] = $post['company_name'];
            $data['contact_person'] = $post['contact_person'] ?: null;
            $data['website_url'] = $post['website_url'];
            $data['customer_username'] = $post['username'] ?: null;
            $data['customer_password'] = $post['password'] ?: null;
            $data['customer_notes'] = $post['notes'];
            $data['latitude'] = $post['latitude'];
            $data['longitude'] = $post['longitude'];
            $data['booking_note'] = $post['booking_note'];
            $data['driver_note'] = $post['driver_note'];
            $data['payment_frequency_id'] = $post['payment_frequency_id'];
            $data['payment_mode_id'] = $post['payment_mode_id'];
            $data['price_hourly'] = $post['hourly'];
            $data['price_extra'] = $post['extra'];
            $data['price_weekend'] = $post['weekend'];
            $data['initial_balance'] = $post['initial_balance'];
            $data['initial_bal_date'] = $post['initial_balance_date'] ? DateTime::createFromFormat('d/m/Y', $post['initial_balance_date'])->format('Y-m-d') : null;
            $data['initial_bal_sign'] = $post['initial_bal_sign'];
            $data['updated_at'] = $data['customer_last_modified_datetime'] = date('Y-m-d H:i:s');
            if ($this->input->post('customer_avatar_base64')) {
                $data['customer_photo_file'] = upload_base64_image($this->input->post('customer_avatar_base64'), 'customer', 'uploads/images/avatars/customer/');
            }

            $this->db->where('customer_id', $customer_id);
            $this->db->update('customers', $data);
            $this->db->flush_cache();
            /***************************************************************** */
            // access data of customer
            if ($post['access']) {
                // check and save new access data
                foreach ($post['access'] as $key => $customer_access_id) {
                    // validate and insert to customer access
                    $customer_access_owned_row = [];
                    $customer_access_owned_row['customer_id'] = $customer_id;
                    $customer_access_owned_row['customer_access_id'] = $customer_access_id;
                    $customer_access_owned_row['received_by_user'] = user_authenticate();
                    $customer_access_owned_row['received_at'] = date('Y-m-d H:i:s');
                    $this->db->insert('customer_access_owned', $customer_access_owned_row);
                }
            }
            $odoo_package_customer_id = $customer->odoo_package_customer_id;
            // print_r($odoo_package_customer_id);die();
            if ($odoo_package_customer_id > 0) {
                $odo_customer_affected = $this->update_customer_inodoo($data, $odoo_package_customer_id);
            }
            /***************************************************************** */
            // update pref maids
            $this->db->where('customer_id', $customer_id);
            $this->db->delete('customer_pref_maids');
            $this->db->flush_cache();
            /**********/
            $preferred_maids = $post['pref_maids'];
            $preferred_maids = array_unique($preferred_maids);
            foreach ($preferred_maids as $key => $maid_id) {
                if (is_numeric($maid_id)) {
                    $this->customer_mollymaid_model->save_preferred_maid(array('customer_id' => $customer_id, 'maid_id' => $maid_id));
                }
            }
            $this->db->flush_cache();

            // update pref maids
            $this->db->where('customer_id', $customer_id);
            $this->db->delete('flaged_maids_customers');
            $this->db->flush_cache();

            /**********/
            $flaged_maids = $post['flaged_maid'];
            if (!empty($flaged_maids)) {
                $flaged_maids = array_unique($flaged_maids);
                foreach ($flaged_maids as $key => $maid_id) {
                    if (is_numeric($maid_id)) {
                        $flagged_data = array(
                            'customer_id' => $customer_id,
                            'maid_id' => $maid_id,
                            'flaged_status' => 0,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->customer_mollymaid_model->save_flagged_maid($flagged_data);
                    }
                }
                $this->db->flush_cache();
            }

            /***************************************************************** */
            // update or add address
            $this->db->where('customer_id', $customer_id);
            $this->db->update('customer_addresses', array('address_status' => 1));
            $this->db->flush_cache();
            foreach ($this->input->post('customer_address') as $key => $address) {
                $address_data[$key]['residence_type'] = $post['address_residence_type_ids'][$key];
                $address_data[$key]['location_id'] = $post['address_location_ids'][$key];
                $address_data[$key]['makkani_number'] = $post['address_makkani_numbers'][$key] ?: null;
                $address_data[$key]['area_id'] = $post['address_area_ids'][$key];
                // $address_data[$key]['zip_code'] = $post['address_zip_codes'][$key];
                $address_data[$key]['other_area'] = null;
                $address_data[$key]['customer_address'] = $post['address_addresses'][$key];
                $address_data[$key]['latitude'] = $post['address_latitudes'][$key];
                $address_data[$key]['longitude'] = $post['address_longitudes'][$key];
                $address_data[$key]['landmark_id'] = $post['address_landmark_ids'][$key] ?: null;
                $address_data[$key]['building'] = $post['address_building'][$key] ?: null;
                $address_data[$key]['unit_no'] = $post['address_unit_nos'][$key] ?: null;
                $address_data[$key]['street'] = null;
                $address_data[$key]['address_category'] = null;
                // $address_data[$key]['address_status'] = 0; // 0 means not deleted :/
                $address_data[$key]['address_status'] = $post['address_status'][$key]; // 1-means Active 0-means Inactive 
                $address_data[$key]['home_type'] = null;
                if (isset($this->input->post('customer_address_ids')[$key])) {
                    // update
                    $this->db->where('customer_id', $customer_id);
                    $this->db->where('customer_address_id', $this->input->post('customer_address_ids')[$key]);
                    $this->db->update('customer_addresses', $address_data[$key]);
                } else {
                    // insert
                    $address_data[$key]['customer_id'] = $customer_id;
                    $this->db->insert('customer_addresses', $address_data[$key]);
                }
                $this->db->flush_cache();
            }
            /***************************************************************** */
            $this->db->trans_commit();
            $respose['status'] = "success";
            $respose['message'] = "Customer updated successfully.";
            die(json_encode($respose, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    }

    // *****************************************************************************************
    public function save_customer()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        // log_message('error', 'Post datas' . json_encode($post));
        $pref_maids =  $post['pref_maids'];
        $flaged_maids = $post['flaged_maid'];
        $intersection = array_intersect($pref_maids, $flaged_maids);
        if (!empty($intersection)) {
            echo json_encode(array('status' => 'error', 'message' => 'Preferred maids and flagged maids cannot be the same.'));
            return;
        }
        $customer_code = isset($post['customer_code']) ? filter_var($post['customer_code'], FILTER_SANITIZE_STRING) : '';
        try {
            $this->db->trans_begin();
            $data['customer_type_id'] = $post['customer_type_id'];
            $data['customer_company_id'] = null;
            $data['customer_dealer_id'] = null;
            if ($post['customer_type_id'] == 2) {
                // company
                $data['customer_company_id'] = $post['customer_company_id'];
                $data['is_company'] = 'Y';
            } else if ($post['customer_type_id'] == 3) {
                // dealer
                $data['customer_dealer_id'] = $post['customer_dealer_id'];
                $data['is_company'] = 'N';
            }
            $data['customer_name'] = $post['customer_name'];
            $data['gender_id'] = $post['gender_id'] ?: null;
            $data['customer_nick_name'] = $post['customer_nick_name'] ?: null;
            $data['is_flag'] = $post['is_flagged'];
            $data['flag_reason'] = $post['flag_reason'] ?: null;
            if ($post['pet']) {
                $data['pet'] = $post['pet'];
                $data['pet_type'] = $post['pet_type'] ?: null;
            }
            //$data['is_blocked_new_booking'] = $post['is_blocked_new_booking'] != 1 ? null : 0;
            //$data['blocked_new_booking_reason'] = $post['blocked_new_booking_reason'] ?: null;
            $data['is_allowed_new_booking'] = $post['is_allowed_new_booking'];
            $data['blocked_new_booking_reason'] = $post['blocked_new_booking_reason'] ?: null;
            $data['mobile_number_1'] = $post['mobile_number_1'] ?: null;
            $data['mobile_number_2'] = $post['mobile_number_2'] ?: null;
            $data['whatsapp_no_1'] = $post['whatsapp_no_1'] ?: null;
            // $data['phone_number'] = $post['phone'] ?: null;
            $data['email_address'] = $post['email'] ?: null;
            $data['fax_number'] = $post['fax'] ?: null;
            $data['trnnumber'] = $post['trnnumber'] ?: null;
            // $data['vatnumber'] = $post['vatnumber'] ?: null;
            $data['refer_source_id'] = $post['refer_source_id'] ?: null;
            $data['refer_source_note'] = $post['refer_source_note'] ?: null;
            // $data['company_name'] = $post['company_name'] ?: null;
            $data['contact_person'] = $post['contact_person'] ?: null;
            $data['website_url'] = $post['website_url'] ?: null;
            $data['customer_username'] = $post['username'] ?: null;
            $data['customer_password'] = $post['password'] ?: null;
            $data['customer_notes'] = $post['notes'] ?: null;
            // $data['latitude'] = $post['latitude'] ?: null;
            // $data['longitude'] = $post['longitude'] ?: null;
            $data['booking_note'] = $post['booking_note'] ?: null;
            $data['driver_note'] = $post['driver_note'] ?: null;
            $data['payment_frequency_id'] = $post['payment_frequency_id'];
            $data['payment_mode_id'] = $post['payment_mode_id'];
            $data['price_hourly'] = $post['hourly'];
            $data['price_extra'] = $post['extra'];
            $data['price_weekend'] = $post['weekend'];
            $data['initial_balance'] = $post['initial_balance'];
            $data['initial_bal_date'] = $post['initial_balance_date'] ? DateTime::createFromFormat('d/m/Y', $post['initial_balance_date'])->format('Y-m-d') : null;
            $data['initial_bal_sign'] = $post['initial_bal_sign'];
            $data['created_by_user'] = user_authenticate();
            $data['customer_booktype'] = $post['customers_book_type'];
            $data['created_at'] = $data['updated_at'] = $data['customer_added_datetime'] = $data['customer_last_modified_datetime'] = date('Y-m-d H:i:s');
            $preferred_maids = $post['pref_maids'];
            $preferred_maids = array_unique($preferred_maids);
            $flaged_maids = $post['flaged_maid'];
            $customer_id = $this->customer_mollymaid_model->save_customer($data);
            if ($customer_id > 0) {
                /****************************************************** */
                // access data of customer
                if ($post['access']) {
                    // check and save access data
                    foreach ($post['access'] as $key => $customer_access_id) {
                        // validate and insert to customer access
                        $customer_access_owned_row = [];
                        $customer_access_owned_row['customer_id'] = $customer_id;
                        $customer_access_owned_row['customer_access_id'] = $customer_access_id;
                        $customer_access_owned_row['received_by_user'] = user_authenticate();
                        $customer_access_owned_row['received_at'] = date('Y-m-d H:i:s');
                        $this->db->insert('customer_access_owned', $customer_access_owned_row);
                        $data['key_given'] = $customer_access_id;
                    }
                }
                $odoo_new_customer_add = $this->odoo_new_customer_add($data, $customer_id);
                // print_r($odoo_new_customer_add);die();
                /****************************************************** */
                foreach ($this->input->post('address_zone_ids') as $key => $zone_id) {
                    // log_message('error','KEY '.json_encode($key));
                    $address_data[$key]['customer_id'] = $customer_id;
                    $address_data[$key]['residence_type'] = $post['address_residence_type_ids'][$key];
                    $address_data[$key]['location_id'] = $post['address_location_ids'][$key];
                    $address_data[$key]['makkani_number'] = $post['address_makkani_numbers'][$key] ?: null;
                    $address_data[$key]['area_id'] = $post['address_area_ids'][$key];
                    // $address_data[$key]['zip_code'] = $post['address_zip_codes'][$key];
                    $address_data[$key]['other_area'] = null;
                    $address_data[$key]['customer_address'] = $post['address_addresses'][$key];
                    $address_data[$key]['latitude'] = $post['address_latitudes'][$key] ?: null;
                    $address_data[$key]['longitude'] = $post['address_longitudes'][$key] ?: null;
                    $address_data[$key]['landmark_id'] = $post['address_landmark_ids'][$key] ?: null;
                    $address_data[$key]['building'] = $post['address_building'][$key] ?: null;
                    $address_data[$key]['unit_no'] = $post['address_unit_nos'][$key] ?: null;
                    $address_data[$key]['street'] = null;
                    $address_data[$key]['default_address'] = $key == 0 ? 1 : 0;
                    $address_data[$key]['address_category'] = null;
                    // $address_data[$key]['address_status'] = 0; // 0 means not deleted :/
                    $address_data[$key]['address_status'] = $post['address_status'][$key]; // 1-means Active 0-means Inactive 
                    $address_data[$key]['home_type'] = null;
                    $address_id = $this->customer_mollymaid_model->save_customer_address($address_data[$key]);
                    $odoo_new_customer_location_add = $this->odoo_new_customer_location_add($address_data[$key], $odoo_new_customer_add, $zone_id);

                    if ($address_id > 0) {
                        $address_ids[] = $address_id;
                    } else {
                        $respose['status'] = "failed";
                        $respose['message'] = $this->db->_error_message();
                        $this->db->trans_rollback();
                        die(json_encode($respose, JSON_PRETTY_PRINT));
                    }
                    // $data['area_id'] = $post['address_area_ids'][$key];
                    // $data['customer_address'] = $post['address_addresses'][$key];
                    // $data['customer_id'] = $customer_id;
                    // $data['apartment'] = $post['address_building'][$key];
                    // $data['latitude1'] = $post['address_latitudes'][$key];
                    // $data['longitude1'] = $post['address_longitudes'][$key];
                    // print_r($data);die();
                }
                // $odoo_new_customer_add = $this->odoo_new_customer_add($data,$customer_id);
                // if($odoo_new_customer_add > 0)
                // {
                // $this->customers_model->update_booktype(array('odoo_package_customer_id' => $odoo_new_customer_add,'odoo_package_customer_status' => 1), $customer_id);
                // }
                foreach ($preferred_maids as $key => $maid_id) {
                    if (is_numeric($maid_id)) {
                        $this->customer_mollymaid_model->save_preferred_maid(array('customer_id' => $customer_id, 'maid_id' => $maid_id));
                    }
                }

                // Save flagged maids
                if (!empty($flaged_maids)) {
                    foreach ($flaged_maids as $key => $maid_id) {
                        if (is_numeric($maid_id)) {
                            $flagged_data = array(
                                'customer_id' => $customer_id,
                                'maid_id' => $maid_id,
                                'flaged_status' => 0,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            );
                            $this->customer_mollymaid_model->save_flagged_maid($flagged_data);
                        }
                    }
                }


                if (empty($customer_code)) {

                    $default_customer_code = "MM-" . sprintf('%05d', $customer_id) . "-" . date('m-Y');
                    $customer_code = $default_customer_code;
                } else {

                    $customer_code = $customer_code;
                }
                // $odoo_new_customer_add = $this->odoo_new_customer_add($data,$customer_id);
                if ($odoo_new_customer_add > 0) {
                    $this->customer_mollymaid_model->update_customer($customer_id, array(
                        'customer_code' => $customer_code,
                        'default_booking_address' => $address_ids[0],
                        'odoo_package_customer_id' => $odoo_new_customer_add,
                        'odoo_package_customer_status' => 1

                    ));
                    // $this->customers_model->update_booktype(array('odoo_package_customer_id' => $odoo_new_customer_add,'odoo_package_customer_status' => 1), $customer_id);
                } else {
                    $this->customer_mollymaid_model->update_customer($customer_id, array(
                        'customer_code' => $customer_code,
                        'default_booking_address' => $address_ids[0],

                    ));
                }

                // $this->customer_mollymaid_model->update_customer($customer_id, array('customer_code' => "MM-" . sprintf('%05d', $customer_id) . "-" . date('m-Y'), 'default_booking_address' => $address_ids[0]));
                $this->db->trans_commit();
                $respose['status'] = "success";
                $respose['message'] = "Customer saved successfully.";
                die(json_encode($respose, JSON_PRETTY_PRINT));
            } else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($respose, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $respose['status'] = "failed";
            $respose['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    }
    public function odoo_new_customer_add($data = array(), $res)
    {

        // $data['payment_frequency_id'] = $data['payment_mode_id'] == 'D' ? 'daily' : ($data['payment_mode_id'] == 'W' ? 'weekly' : 'monthly');
        if ($data['payment_frequency_id'] == 1) {
            $payment_type = "daily";
        } else if ($data['payment_frequency_id'] == 2) {
            $payment_type = "weekly";
        } else if ($data['payment_frequency_id'] == 3) {
            $payment_type = "monthly";
        }
        if ($data['customer_type_id'] == 1) {
            $company_type = "person";
        } else if ($data['customer_type_id'] == 2) {
            $company_type = "company";
        } else if ($data['customer_type_id'] == 3) {
            $company_type = "dealer";
        }

        if ($data['payment_mode_id'] == 1) {
            $data['payment_mode'] = 'cash';
        } else if ($data['payment_mode_id'] == 3) {
            $data['payment_mode'] = 'cheque';
        } else if ($data['payment_mode_id'] == 4) {
            $data['payment_mode'] = 'Online transfer';
        } else if ($data['payment_mode_id'] == 2) {
            $data['payment_mode'] = 'credit_card';
        }

        if ($data['refer_source_id'] == 1) {
            $source = "Google";
        } else if ($data['refer_source_id'] == 2) {
            $source = "Facebook";
        } else if ($data['refer_source_id'] == 3) {
            $source = "Instagram";
        } else if ($data['refer_source_id'] == 4) {
            $source = "Other";
        }
        // $data['key_given'] = $data['key_given'] == 'Y' ? 'yes' : 'no';
        // $company_type = "person";
        if ($data['is_company'] == 'Y') {
            $company_name = $data['customer_company_id'];
            $get_company_details = $this->customers_model->get_company_details_for_customer($company_name);
            if (!empty($get_company_details)) {
                $company = $get_company_details->company_name;
            }
            // } else {
            //     $company = null;
            // }

        } else if ($data['is_company'] == 'N') {
            $dealer_name = $data['customer_dealer_id'];
            $get_dealer_details = $this->customers_model->get_dealer_details_for_customer($dealer_name);
            // print_r($get_dealer_details->odoo_package_dealer_id);die();
            if (!empty($get_dealer_details)) {
                $dealer_name = $get_dealer_details->odoo_package_dealer_id;
            } else {
                $dealer_name  = null;
            }
        }
        // print_r($company_type);
        if ($data['customer_booktype'] == 0) {
            $booktype = "one-off";
        } else if ($data['customer_booktype'] == 1) {
            $booktype = "regular";
        }

        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['customer_name'];
        $post['params']['mobile'] = $data['mobile_number_1'];
        $post['params']['company_type'] =  $company_type;
        if ($company_type == 'company') {
            $post['params']['company'] =  $company;
        }
        if ($company_type == "dealer") {
            $post['params']['dealer_name_id'] =  (int) $dealer_name;
        }
        // $post['params']['parent_id'] = $parent_id;
        $post['params']['customer_nick_name'] = $data['customer_nick_name'];
        $post['params']['customer_id'] = (int) $res;
        $post['params']['cust_book_type'] = $booktype;
        $post['params']['payment_type'] = $payment_type;
        $post['params']['source'] = $source;
        $post['params']['website'] = $data['website_url'];
        $post['params']['mobile2'] = $data['mobile_number_2'];
        $post['params']['mobile3'] = $data['whatsapp_no_1'];
        // $post['params']['fax'] = $data['fax_number'];
        $post['params']['email'] = $data['email_address'];
        //$post['params']['zone_id'] = $data['driver_name'];
        $post['params']['comment'] = $data['customer_notes'];
        $post['params']['hourly'] = $data['price_hourly'];
        $post['params']['extra'] = $data['price_extra'];
        $post['params']['weekend'] = $data['price_weekend'];
        // $post['params']['key'] = $data['key_given'];
        $post['params']['trn'] = $data['trnnumber'];
        // $post['params']['vat_no'] = $data['vatnumber'];
        // print_r($post);
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "customer_creation";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);
        // print_r($login_check);die();
        if ($returnData->result->status == "success") {
            return $odoo_customer_id = $returnData->result->response->id;
        }
    }
    public function odoo_new_customer_location_add($data = array(), $res, $zone_id)
    {
        $getarea = $this->settings_model->get_area_details($data['area_id']);
        $getzone = $this->settings_model->get_zone_details($zone_id);
        $getlocation = $this->settings_model->get_location_details_by_id($data['location_id']);
        $getlandmark = $this->settings_model->get_landmark_details($data['landmark_id']);
        if ($data['residence_type'] == 1) {
            $residence_type = 'villa';
        } else if ($data['residence_type'] == 2) {
            $residence_type = 'apartment';
        } else if ($data['residence_type'] == 3) {
            $residence_type = 'office';
        } else if ($data['residence_type'] == 4) {
            $residence_type = 'warehouse';
        } else if ($data['residence_type'] == 5) {
            $residence_type = 'home';
        }
        $post['params']['user_id'] = 1;
        $post['params']['partner_id'] = $res;
        $post['params']['locations'] = [
            [
                'zone_id' => (int) $getzone[0]['odoo_package_zone_id'],
                'area_id' => (int) $getarea[0]['odoo_package_area_id'],
                'location_id' => (int) $getlocation[0]['odoo_package_location_id'],
                'landmark_id' => (int) $getlandmark[0]['odoo_package_landmark_id'],
                'residence_type' => $residence_type,
                'building' => $data['building'],
                'unit_no' => $data['unit_no'],
                'address' => $data['customer_address'],
                'makkani_no' => $data['makkani_number'],
                'longitude' => $data['longitude'],
                'latitude' => $data['latitude']
            ]
        ];
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "customer/add_location";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_customer_location_id = $returnData->result->response->id;
        }
    }

    public function update_customer_inodoo($data, $customer_id)
    {

        $post['params']['user_id'] = 1;
        $post['params']['id'] = $customer_id;
        $post['params']['name'] = $data['customer_name'];
        // $post['params']['company_type'] = $data['company_type'];
        $post['params']['mobile'] = $data['mobile_number_1'];
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "customer_edition";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_customer_id = $customer_id;
        }
    }
    public function getArea()
    {
        //  $post = $this->input->post();
        $zoneId = $this->input->post('zoneId');
        $areas = $this->customer_mollymaid_model->getZonesByArea($zoneId);
        echo json_encode($areas);
    }

    public function getLocations()
    {
        $areaId = $this->input->post('area_id');
        $locations = $this->customer_mollymaid_model->getLocationsByArea($areaId);
        echo json_encode($locations);
    }

    public function getLandmark()
    {
        $locationId = $this->input->post('location_id');
        $landmarks = $this->customer_mollymaid_model->getLandmarkByLocation($locationId);
        echo json_encode($landmarks);
    }

    public function editvalidatemobilenumber()
    {
        if ($this->input->post('mobile_number_1')) {
            // log_message('error','post mobile number 1'.json_encode($this->input->post()));
            $mobile_number_1 = $this->input->post("mobile_number_1");
            $id = $this->input->post("id");
            $form = $this->input->post("form");
            $result = $this->customer_mollymaid_model->check_mob_number($mobile_number_1, $id, $form);
            echo json_encode($result);
        }
    }
    public function editvalidatewhatsnum()
    {
        if ($this->input->post('whatsapp_no_1')) {
            $whatsnum = $this->input->post("whatsapp_no_1");
            $id = $this->input->post("id");
            $form = $this->input->post("form");
            $result = $this->customer_mollymaid_model->check_whats_number($whatsnum, $id, $form);
            echo json_encode($result);
        }
    }
    public function editvalidateemail()
    {
        if ($this->input->post('email')) {
            $email = $this->input->post('email');
            $id = $this->input->post("id");
            $form = $this->input->post("form");
            $result = $this->customer_mollymaid_model->check_email($email, $id, $form);
            echo json_encode($result);
        }
    }
    public function validate_customer_code()
    {
        if ($this->input->post('customer_code')) {
            $customer_code = $this->input->post("customer_code");
            $form = $this->input->post("form");
            $id = $this->input->post("id");
            $result = $this->customer_mollymaid_model->check_customer_code($customer_code, $form, $id);
            echo json_encode($result);
        }
    }

    // *****************************************************************
    public function delete_customer()
    {
        $customer_id = $this->input->post('customer_id');
        $customer = $this->db->select('c.*')->from('customers as c')->where('c.customer_id', $customer_id)->where('c.customer_status', 1)->get()->row();
        $odoo_customer_id = $customer->odoo_package_customer_id;
        if (empty($customer_id)) {
            echo json_encode(array('status' => 'error', 'message' => 'Customer ID is required'));
            return;
        }
        $odoo_customer_delete = $this->delete_odoo_customer($customer, $odoo_customer_id);
        $result = $this->customer_mollymaid_model->delete_customer($customer_id);

        if ($result) {
            echo json_encode(array('status' => 'success', 'message' => 'Customer deleted successfully'));
        } else {
            echo json_encode(array('status' => 'error', 'message' => 'Failed to delete customer'));
        }
    }
    public function delete_odoo_customer($data, $customer_id)
    {

        $post['params']['user_id'] = 1;
        $post['params']['id'] = $customer_id;
        $post_values = json_encode($post);
        $url = $this->config->item('odoo_url') . "customer_deletion";
        $login_check = curl_api_service($post_values, $url);
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_customer_id = 0;
        }
    }

    public function do_zone_odoo_api($data = array())
    {
        // if ($data['spare_zone'] == 'Y') {
        //     $spare = TRUE;
        // } else {
        //     $spare = FALSE;
        // }
        $post['params']['user_id'] = 1;
        $post['params']['name'] = $data['zone_name'];
        // $post['params']['driver_name'] = $data['driver_name'];
        // $post['params']['spare_zone'] = $spare;
        // print_r($post);
        $post_values = json_encode($post);

        $url = $this->config->item('odoo_url') . "zone_creation";

        $login_check = curl_api_service($post_values, $url);
        // print_r($login_check);die();
        $returnData = json_decode($login_check);

        if ($returnData->result->status == "success") {
            return $odoo_zone_id = $returnData->result->response->id;
        }
    }
}
