<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        if(!is_user_loggedin())
        {
            redirect('logout');
        }
        if(!user_permission(user_authenticate(), 22))
        { 
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('users_model');
        $this->load->model('settings_model');
	$this->load->helper('form');
    }
    public function index()
    {
        $data = array();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['user_status'] = (!is_numeric($this->input->get('status'))) ? '1' : $this->input->get('status');
        $data['users'] = $this->users_model->get_users_by_status($data['user_status']);
        //die($data['user_status']);
        $layout_data['content_body'] = $this->load->view('user', $data, TRUE);
        $layout_data['page_title'] = 'Admin Users';
        $layout_data['meta_description'] = 'Admin Users';
        $layout_data['css_files'] = array('demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('users.js', 'jquery.dataTables.min.js','toastr.min.js');
        $layout_data['account_active'] = '1'; 
        $this->load->view('layouts/default', $layout_data);
    }
    function update(){
        if($this->input->is_ajax_request()){
            if($this->input->post('action') == 'disable-user'){
                if($this->users_model->update_user($this->input->post('id'), array('user_status' => '0')));{
                    die(json_encode(array('status'=>true,'message'=>'User disabled successfully !','type'=>'success')));
                }
                die(json_encode(array('status'=>false,'message'=>'Update action failed !','type'=>'error')));
            }
            else if($this->input->post('action') == 'enable-user'){
                if($this->users_model->update_user($this->input->post('id'), array('user_status' => '1')));{
                    die(json_encode(array('status'=>true,'message'=>'User enabled successfully !','type'=>'success')));
                }
                die(json_encode(array('status'=>false,'message'=>'Update action failed !','type'=>'error')));
            }
            else{
                die(json_encode(array('status'=>false,'message'=>'Update action not specified !','type'=>'error')));
            }
        }
    }
    public function permissions()
    {
        
        $user_id = $this->uri->segment(3); 
        $error = array();
        if($this->input->post('update_user'))
        {
            
            $modlues = $this->input->post('permissions');  
            
            if(!empty($modlues))
            {
                
                    $updated_fields = array(); 
                    $i = 0;
                    foreach ($modlues as $module)
                    {
                        $updated_fields[$i]['user_id'] = $user_id;
                        $updated_fields[$i]['module_id'] = $module;
                        $updated_fields[$i]['permission'] = 'Y';
                        $updated_fields[$i]['added'] = date('Y-m-d H:i:s');

                        ++$i;
                    }
                    
                    $affected_rows = $this->users_model->add_permission($updated_fields, $user_id);
                    if($affected_rows > 0)
                    {
                        $this->session->set_flashdata('success_msg', 'Permissions has been added successfully.');
                        redirect('/users/permissions/' . $user_id, 'refresh');
                        exit();
                        $error['class'] = 'alert-success';
                        $error['message'] = '<strong>Success!</strong> Permission added successfully!';
                    }
                    else
                    {
                        $this->session->set_flashdata('error_msg', 'Something went wrong!');
                        redirect('/users/permissions/' . $user_id, 'refresh');
                        exit();
                        $error['class'] = '';
                        $error['message'] = '<strong>Error!</strong> Something went wrong!';
                    }
            }
            else
            {
                    $error['class'] = '';
                    $error['message'] = '<strong>Error!</strong> Please select atleast one module!';
            }
            
        }
        
        $user_modules = $this->users_model->get_permission_by_user($user_id);
        $user_module_ids = array();
        foreach ($user_modules as $u_modlue)
        {
            array_push($user_module_ids, $u_modlue->module_id);
        }
        
        $modules = $this->users_model->get_all_modules(FALSE);
                
        $modules_array = array();
        foreach ($modules as $module)
        {
            if($module->parent_id == 0){
                //$modules_array[$module->module_id] =  new stdClass();
                $modules_array[$module->module_id] = array('module_id' => $module->module_id, 'module_name' => $module->sub_module_name);


            }else{
                ////array('module_id' => $module->module_id, 'sub_module' => $module->sub_module_name);
                //$modules_array[$module->parent_id]['module_name']['sub_modules'] = array();
                $modules_array[$module->parent_id]['sub_modules'][] = array('parent_id' => $module->parent_id, 'module_id' => $module->module_id, 'sub_module' => $module->sub_module_name);

            }
        }
        
        $data = array();
        $data['user_id'] = $user_id;
        $data['modules'] = $modules_array;//$this->users_model->get_modules();
        $data['user'] = $this->users_model->get_user_by_id($user_id);
        $data['user_modules'] = $user_module_ids;
        $data['error'] = $error;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('user_permission', $data, TRUE);
	$layout_data['page_title'] = 'Admin Users';
	$layout_data['meta_description'] = 'Admin Users';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
	$layout_data['js_files'] = array('users.js');
	$this->load->view('layouts/default', $layout_data);
    }
    
    function userlogininfo() 
    {
        $user_id = $this->uri->segment(3); 
        $error = array();
        
        $data['userlogininfo'] = $this->users_model->get_user_login_info($user_id);
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('user_login_info_view', $data, TRUE);

        $layout_data['page_title'] = 'User Activity Report';
        $layout_data['meta_description'] = 'User Activity Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'mymaids.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }
    
    public function add_user()
    {
        $e_user_id = 0;
        $e_user = array();
        if($this->uri->segment(3) && is_numeric($this->uri->segment(3)) && $this->uri->segment(3) > 0)
        {
            $e_user_id = $this->uri->segment(3);
            $e_user = $this->users_model->get_user_by_id($e_user_id);
        }
        
        if($this->input->post('user_sub'))
        {
            $errors = array();
			
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<br /><div class="error">', '</div>');

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_message('required', 'Please enter name', 'name');

            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_message('required', 'Please enter user name', 'username');

            if($this->form_validation->run())
            {
                $users_fullname = $this->input->post('name');
                $username = $this->input->post('username');
                $is_admin = $this->input->post('is_admin');
				$delete_permission = $this->input->post('delete_permission');
                $password = $this->input->post('password');
                $user_role = $this->input->post('user_role');
                $chk_user = $this->users_model->get_user_by_user_name($username);
                if($e_user_id > 0)
                {
                    if(isset($chk_user->user_id) && $chk_user->user_id != $e_user->user_id)
                    {
                        $errors['user_name_error'] = 'This user already exists';
                    }
                    else
                    {
                        $data = array(
                            'username' => $username,
                            'user_fullname' => $users_fullname,
                            'is_admin' => $is_admin,
							'delete_permission' => $delete_permission,
							'user_role' => $user_role,
                        );
                        
                        $update_user = $this->users_model->update_user($e_user->user_id, $data);

                        if(is_numeric($update_user) && $update_user > 0)
                        {
                            //$this->session->set_flashdata('success_msg', 'User details has been updated successfully.');
                            redirect(base_url() . 'users/permissions/'.$e_user->user_id);
                            exit();
                        }
                    }
                }
                else
                {
                    if(isset($chk_user->user_id))
                    {
                        $errors['user_name_error'] = 'This user already exists';
                    }
                    else
                    {
                        $data = array(
                            'username' => $username,
                            'password' => md5($password),//md5(12345),
                            'user_fullname' => $users_fullname,
                            'is_admin' => $is_admin,
							'delete_permission' => $delete_permission,
                            'user_status' => '1',
                            'user_role' => $user_role,
                        );
                        $user_ids = $this->users_model->add_user($data);

                        if(is_numeric($user_ids) && $user_ids > 0)
                        {
                            redirect(base_url() . 'users/permissions/'.$user_ids);
                            exit();
                        }
                    }
                }
            } 
            else
            {
                $errors = $this->form_validation->error_array();
            }
        }
        $data = array();
        $data['e_user'] = $e_user;
        $data['error'] = isset($errors) ? array_shift($errors) : '';
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('add_user', $data, TRUE);
	$layout_data['page_title'] = 'Users';
	$layout_data['meta_description'] = 'Users';
	$layout_data['css_files'] = array();
	$layout_data['external_js_files'] = array();
	$layout_data['js_files'] = array('mymaids.js');
	$this->load->view('layouts/default', $layout_data);
    }
    
    public function remove_user()
    {
        $user_id = $this->input->post('user_id');
        $data = array(
            'user_status' => '0',
        );
        $datas = $this->users_model->update_user($user_id, $data);
        //$data = $this->users_model->delete_user($user_id);
    }
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */