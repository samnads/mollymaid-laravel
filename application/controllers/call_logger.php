<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Call_logger extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('call_logger_model');
        
        ini_set("memory_limit","256M");
    }
    
    public function call_connect()
	{
		if($this->input->get('phone') != "" && $this->input->get('refid') != "")
        {
            $phone = $this->input->get('phone');
            $refid = $this->input->get('refid');
            $calltype = $this->input->get('calltype');
            if($this->input->get('TrunkName') != "")
            {
                $trunkname = $this->input->get('TrunkName');
            } else {
                $trunkname = "";
            }
            $datetime = date('Y-m-d H:i:s');
            $num = $phone;
            $check_customer = $this->call_logger_model->getCustomerMobSearch($num);
            
			$call_array = array();
			if(!empty($check_customer))
			{
				$customerid = $check_customer[0]->custId;
				$customername = $check_customer[0]->custName;
				$customerarea = $check_customer[0]->area;
				$outstanding = $check_customer[0]->balance.$check_customer[0]->signed;
				$clickurl = $check_customer[0]->url;
				//For Push
				$body_content="Name : ". $customername ."\n";
				$body_content.="Mobile : ". $phone ."\n";
				$body_content.="Area : ". $customerarea ."\n";
				$body_content.="Outstanding balance : ". $outstanding."\n";
				//Ends
				$call_array['customer_id'] = $check_customer[0]->custId;
			} else {
				$customerid = "";
				$customername = "Unknown";
				$customerarea = "";
				$outstanding = "";
				$clickurl = base_url().'customer/add/'.$phone;
				//For Push
				$body_content="Name : Unknown\n";
				$body_content.="Mobile : ". $phone ."\n";
				$body_content.="Area : \n";
				$body_content.="Outstanding balance : \n";
				//Ends
				$call_array['customer_id'] = "";
			}
            $call_array['mobile_no'] = $phone;
            $call_array['refid'] = $refid;
            $call_array['calltype'] = $calltype;
            $call_array['trunkName'] = $trunkname;
            $call_array['added_date_time'] = $datetime;
            $insert = $this->call_logger_model->add_call_detail($call_array);
            if($insert > 0)
            {
                //Push Code Starts
				$click_url=$clickurl;
				
				$res = array();
				$res['name'] = $customername;
				$res['phone'] = $phone;
				$res['area'] = $customerarea;
				$res['balance'] = $outstanding;
				$res['time'] = $datetime;
				
				$post_values['notification']=array('title' => 'Elitemaids',
			   'body' => $body_content,
			   'click_action' => $click_url,
				'data' => $res,
			   'icon' => 'https://elitemaids.emaid.info/images/elite-maids-logo.png',
				 );
				$post_values['to']='/topics/elitemaids_notification';
                        
				
				$data = json_encode($post_values);
                       
				$url = 'https://fcm.googleapis.com/fcm/send';
				$server_key = 'AAAAPZRF_5U:APA91bFeR_IoaMoYiWJj92vjEAiCuaDBJ9DdzZkVOeD_xOg7nvRXfPIU41U0hwH5BY-pq9veuOtuKsbcRNcLFk5fP5iwXbkdsurAUeByanZ_6QnOEFGHiz1nW8EOhCHDRQI2e1kRwYiN';

				$headers = array(
					'Content-Type:application/json',
					'Authorization:key='.$server_key
				);

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				$result = curl_exec($ch);
		 
				if ($result === FALSE) {
					echo 'Curl error: ' . curl_error($ch);
					exit();
				}
				curl_close($ch);
				
				//Push Code Ends
				$message = "Datas inserted successfully";
				$response               = array();
				$response['status']     = 'success';
				$response['message']    = $message;

				echo json_encode($response);
				exit();
            } else {
                $message = 'Could not enter data';
				$response               = array();
				$response['status']     = 'error';
				$response['error_code'] = '101';
				$response['message']    = $message;

				echo json_encode($response);
				exit();
            }
        } else {
            $response               = array();
            $response['status']     = 'error';
            $response['error_code'] = '101';
            $response['message']    = 'Invalid phone number';

            echo json_encode($response);
            exit();
        }
	}
	
	public function call_disconnect()
	{
		if($this->input->get('phone') != "" && $this->input->get('refid') != "")
        {
            $phone = $this->input->get('phone');
            $refid = $this->input->get('refid');
            $status = $this->input->get('status');
            $duration = $this->input->get('duration');
            $waveid = $this->input->get('waveid');
            $tdurn = $this->input->get('TDurn');
            $rdurn = $this->input->get('RDurn');
            if($this->input->get('TrunkName') != "")
            {
                $trunkname = $this->input->get('TrunkName');
            } else {
                $trunkname = "";
            }
            $datetime = date('Y-m-d H:i:s');
            $check_call = $this->call_logger_model->check_call_exist($refid,$phone);
            if(count($check_call) > 0)
            {
                $edit_array = array();
                $edit_array['mobile_no'] = $phone;
                $edit_array['refid'] = $refid;
                $edit_array['status'] = $status;
                $edit_array['duration'] = $duration;
                $edit_array['waveid'] = $waveid;
                $edit_array['tdurn'] = $tdurn;
                $edit_array['rdurn'] = $rdurn;
                $edit_array['trunkName'] = $trunkname;
                $edit_array['updated_datetime'] = $datetime;

                $update_call = $this->call_logger_model->update_call_detail($refid,$edit_array);
                //$update_call = array();
                if($update_call)
                {
                    $message = "Datas updated successfully";
                    $response               = array();
                    $response['status']     = 'success';
                    $response['message']    = $message;

                    echo json_encode($response);
                    exit();
                } else {
                    $message = 'Could not update data ';
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = $message;

                    echo json_encode($response);
                    exit();
                }
            } else {
                $call_array = array();
                $call_array['mobile_no'] = $phone;
                $call_array['refid'] = $refid;
                $call_array['status'] = $status;
                $call_array['duration'] = $duration;
                $call_array['waveid'] = $waveid;
                $call_array['tdurn'] = $tdurn;
                $call_array['rdurn'] = $rdurn;
                $call_array['trunkName'] = $trunkname;
                $call_array['added_date_time'] = $datetime;
                $insert = $this->call_logger_model->add_call_detail($call_array);
                //$insert = 0;
                if($insert > 0)
                {
                    $message = "Datas inserted successfully";
                    $response               = array();
                    $response['status']     = 'success';
                    $response['message']    = $message;

                    echo json_encode($response);
                    exit();
                } else {
                    $message = 'Could not enter data ';
                    $response               = array();
                    $response['status']     = 'error';
                    $response['error_code'] = '101';
                    $response['message']    = $message;

                    echo json_encode($response);
                    exit();
                }
            }
        }
	}
}