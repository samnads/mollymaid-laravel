<?php if (!defined('BASEPATH')) {

    exit('No direct script access allowed');
}
class Customer_access extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('customer_access_model');
    }
    public function index()
    {
    }
    public function assign_list()
    {
        if ($this->uri->segment(4) != "") {
            $data['rows'] = $this->customer_access_model->get_access_assign_by_types($this->uri->segment(4));
            $data['accesstype'] = $this->uri->segment(4);
        } else {
            $data['rows'] = $this->customer_access_model->customer_access_owned_list();
            $data['accesstype'] = "0";
        }

        $data['settings'] = $this->settings_model->get_settings();
        //$data['rows'] = $this->customer_access_model->customer_access_owned_list();
        $layout_data['content_body'] = $this->load->view('customer-access/assign-list', $data, true);
        $layout_data['page_title'] = 'Key / Card Assign List';
        $layout_data['meta_description'] = 'Key / Card Assign List';
        $layout_data['css_files'] = array('demo.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', "access-mollymaid.js?v=" . time());
        $this->load->view('layouts/default', $layout_data);
    }
    public function get_available_accesses()
    {
        header('Content-Type: application/json; charset=utf-8');
        $active = $this->customer_access_model->get_active_accesses();
        $active_access_ids = array_column($active, "customer_access_id");
       $available = $this->customer_access_model->get_accesses_except($active_access_ids, array("ca.customer_access_type" => $this->input->get('access_type')));
        // $active = $this->customer_access_model->get_inactive_accesses();
        // $active_access_ids = array_column($active, "id");
        // $available = $this->customer_access_model->get_inative_accesses_except($active_access_ids, array("ca.customer_access_type" => $this->input->get('access_type')));
        echo json_encode($available, JSON_PRETTY_PRINT);
    }
    public function assign()
    {
        header('Content-Type: application/json; charset=utf-8');
        $active_access = $this->customer_access_model->get_active_access_owned_by_id($this->input->post('access'));
        if (isset($active_access->id)) {
            $respose['status'] = false;
            $respose['message'] = "Sorry, the " . $active_access->access_type . " <b>" . $active_access->code . "</b> is currently active to customer " . $active_access->customer_name . ".";
            $respose['data'] = $active_access;
        } else {
            $this->db->trans_begin();
            try {
                $post = $this->input->post();
                $data['customer_id'] = $post['customer_id'];
                $data['customer_access_id'] = $post['access'];
                $data['received_notes'] = trim($post['received_notes']) ?: null;
                $data['received_by_user'] = user_authenticate();
                $data['received_at'] = date('Y-m-d H:i:s');
                $id = $this->customer_access_model->access_assign($data);
                $this->db->trans_commit();
                $respose['status'] = true;
                $respose['message'] = "Key assigned successfully.";
                $respose['data'] = ['id' => $id];
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $respose['status'] = false;
                $respose['message'] = $e->getMessage();
            }
        }
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function return ()
    {
        header('Content-Type: application/json; charset=utf-8');
        $access_owned = $this->customer_access_model->get_access_owned_by_id($this->input->post('return_access_id'));
        if (!empty($access_owned->returned_at)) {
            $respose['status'] = false;
            $respose['message'] = "Sorry, the " . $access_owned->access_type . " <b>" . $access_owned->code . "</b> is already returned to customer " . $access_owned->customer_name . ".";
            $respose['data'] = $access_owned;
        } else {
            $this->db->trans_begin();
            try {
                $post = $this->input->post();
                $data['returned_notes'] = $post['returned_notes'] ?: null;
                $data['return_added_by'] = user_authenticate();
                $data['returned_by_employee'] = $post['returned_by_employee_id'] ?: null;
                $data['returned_at'] = date('Y-m-d H:i:s');
                $id = $this->customer_access_model->return_assign($this->input->post('return_access_id'), $data);
                $this->db->trans_commit();
                $respose['status'] = true;
                $respose['message'] = "Key returned successfully.";
                $respose['data'] = ['id' => $post['return_access_id']];
            } catch (Exception $e) {
                $this->db->trans_rollback();
                $respose['status'] = false;
                $respose['message'] = $e->getMessage();
            }
        }
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function get_assigned_data()
    {
        header('Content-Type: application/json; charset=utf-8');
        $access_owned = $this->customer_access_model->get_access_owned_by_id($this->input->get('id'));
        echo json_encode($access_owned, JSON_PRETTY_PRINT);
    }
    public function customers_search()
    {
        header('Content-Type: application/json; charset=utf-8');
        $search_result = $this->customer_access_model->customers_search($this->input->post('query'));
        $data = array();
        foreach ($search_result as $user) {
            //$userdetail = $user->customer_name . ',  ' . $user->mobile_number_1 . ', ' . $user->mobile_number_2;
            $userdetail = $user->customer_name;
            $data[] = array("id" => $user->customer_id, "text" => nl2br($userdetail));
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
    public function returned_by_employee_field_search()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data = $this->db->select('m.maid_id as id,m.maid_name as text')
            ->from('maids as m')
            ->where('m.employee_type_id', 1)->get()->result_array();
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function view_access()
    {
        $id = $this->input->post('id');
        $result = $this->customer_access_model->get_access_details_by_id($id);
        echo json_encode($result);

    }
    public function check_returned()
    {
        $key_id = $this->input->post('key_id');
        $access_owned_data = $this->customer_access_model->get_access_owned_data($key_id);

        if ($access_owned_data !== null) {
            if ($access_owned_data->return_added_by !== null && $access_owned_data->returned_at !== null) {
                $response = array('status' => 'not_null');
            } else {
                $response = array('status' => 'null');
            }
        } else {
            $response = array('status' => 'not_found');
        }

        echo json_encode($response);

    }

    public function remove_access()
    {

        $key_id = $this->input->post('key_id');
        $access_owned_data = $this->customer_access_model->get_access_owned_data($key_id);

        if ($access_owned_data !== null) {
            if ($access_owned_data->return_added_by !== null && $access_owned_data->returned_at !== null) {
                $this->customer_access_model->delete_access($key_id);
                $response = array('status' => 'success');
            } else {
                $response = array('status' => 'error');
            }
        } else {
            $this->customer_access_model->delete_access($key_id);
            $response = array('status' => 'not_found');
        }
        echo json_encode($response);
    }

    public function report()
    {
       
        $data['rows'] = $this->customer_access_model->customer_access_owned_list();
        $data['accesstype'] = "0";

        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer-access/report', $data, true);
        $layout_data['page_title'] = 'Key / Card Report';
        $layout_data['meta_description'] = 'Key / Card Report';
        $layout_data['css_files'] = array('demo.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', "access-report.js?v=" . time());
        $this->load->view('layouts/default', $layout_data);
    }

    public function view_report($accesstype)
    {
        $data['rows'] = $this->customer_access_model->get_access_assign_by_types($accesstype);
        $data['accesstype'] = $accesstype;

        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('customer-access/report', $data, true);
        $layout_data['page_title'] = 'Key / Card Report';
        $layout_data['meta_description'] = 'Key / Card Report';
        $layout_data['css_files'] = array('demo.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', "access-report.js?v=" . time());
        $this->load->view('layouts/default', $layout_data);
    }

}
