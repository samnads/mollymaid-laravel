<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Justmop extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 6)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('justmop_model');
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('bookings_model');
        
    }

    public function index() 
    {
        //error_reporting(1);
        //ini_set('memory_limit', '300M');
        $data = array();
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('excel_import', $data, TRUE);		
        $layout_data['page_title'] = 'Justmop';
        $layout_data['meta_description'] = 'Justmop';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
        //$this->load->view('excel_import');
    }
	
	public function successlist() {
        //error_reporting(1);
        //ini_set('memory_limit', '300M');
        $data = array();
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('success_list', $data, TRUE);		
        $layout_data['page_title'] = 'Justmop';
        $layout_data['meta_description'] = 'Justmop';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
        //$this->load->view('excel_import');
    }
    
    public function import_data()
    {
        $config = array(
            'upload_path' => FCPATH.'upload/',
            'allowed_types' => 'xls|xlsx'
        );
        $this->load->library('upload',$config);
        if($this->upload->do_upload('file'))
        {
            $data = $this->upload->data();
            @chmod($data['full_path'], 0777);
            
            $this->load->library('Excel');
            
            $file = $data['full_path'];
            
            $obj=PHPExcel_IOFactory::load($file);
            $cell=$obj->getActiveSheet()->getCellCollection();
            foreach($cell as $cl)
            {
                $column=$obj->getActiveSheet()->getCell($cl)->getColumn();
                $row=$obj->getActiveSheet()->getCell($cl)->getRow();
                $data_value=$obj->getActiveSheet()->getCell($cl)->getFormattedValue();

                if($row==1){
                        $header[$row][$column]=$data_value;
                }else{
                        $arr_data[$row][$column]=$data_value;
                }
            }
            
            $data['header']=$header;
            $data['values']=$arr_data;
            $html = "";
            //$det_array = array();
            $html .= "<ul>"; 
			// echo '<pre>';
			// print_r($arr_data);
			// exit();
            foreach ($arr_data as $val)
            {
                $book_reference = $val['A'];
                if($val['B'] == "One Time")
                {
                    $book_type = "OD";
                    $regular = 0;
                    $service_end = 1;
                } else {
                    $book_type = "WE";
                    $regular = 1;
                    $service_end = 0;
                }
                $customer_name = $val['C'];
            
                $customer_location = $val['D'];
                $area = $val['E'];
				$odate = $val['F'];
				//convert_date_format($odate);
				//echo $newDate = date("Y-m-d", strtotime($odate));
                //echo $service_date = date('Y-m-d', strtotime($val['F']));
				
				$old_date = trim($odate);
 
				if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $old_date)) // MySQL-compatible YYYY-MM-DD format
				{
					$new_date = $old_date;
                                        //$ndate=explode("-",$odate);
                                        //$odate=$ndate[0]."-".$ndate[2]."-".$ndate[1];
				}
				elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/', $old_date)) // DD-MM-YYYY format
				{
					$new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-' . substr($old_date, 0, 2);
				}
				elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{2}$/', $old_date)) // DD-MM-YY format
				{
					$new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-20' . substr($old_date, 0, 2);
                                        
                                        
				}
				else // Any other format. Set it as an empty date.
				{
					$new_date = '0000-00-00';
				}
				if($new_date == '0000-00-00')
				{
					//$var = '20/04/2012';
					$datessss = str_replace('/', '-', $odate);
					$service_date = date('Y-m-d', strtotime($datessss));
				} else {
					$service_date = date('Y-m-d', strtotime($odate));
				}
				//echo $service_date;
				//ends
				//exit();
                $service_week_day = date('w', strtotime($service_date));
                $starttime = date('H:i:s',strtotime($val['G']));//$val['G'].':00';
				//exit();
                
				$bookvals = $val['H'];
				$bookvals_array = explode(" ", $bookvals);
				$cleaners = substr($bookvals_array[8], 0, -1);
				$clean_material = $bookvals_array[11];
				//echo $array[2].','.substr($array[8], 0, -1).','.$array[11];
				$no_of_hours = $bookvals_array[2];
                $endtime = date('H:i:s', strtotime($starttime.'+'.$no_of_hours.' hour'));
                $no_of_maids = $cleaners;
                $clean_material= str_replace(",", "", $clean_material);
                if($clean_material == 'Yes')
                {
                    $cleaning_mat = 'Y';
                } else {
                    $cleaning_mat = 'N';
                }
                $booking_note = $val['I'];
                $crew_in = $val['J'];
                $total_amount = $val['N'];
				
                $vat_percentage_amount = ($total_amount * (5/100));
            
                $discount = $val['M'];
                $get_area_id = $this->justmop_model->get_area_id_name($area);

                if(!empty($get_area_id))
                {
                    $area_id = $get_area_id->area_id;
                } else {
                    $area_id = 0;
                }
                if($val['O'] == 'Partnership')
                {
                    $payment_mode = 'Credit Card';
                } else {
                    $payment_mode = $val['O'];
                }
                if($val['O'] == 'Cash')
                {
                    $paytype = 0;
                } else {
                    $paytype = 1;
                }
                $customer_array = array();
                $customer_array['odoo_customer_id'] = 0;
                $customer_array['customer_username'] = $customer_name;
                $customer_array['customer_password'] = '123456';
                $customer_array['customer_type'] = 'HO';
                $customer_array['customer_name'] = $customer_name;
                $customer_array['customer_nick_name'] = $customer_name;
                $customer_array['is_company'] = 'Y';
                $customer_array['company_name'] = 'Justmop';
                $customer_array['mobile_number_1'] = '045578370';
                $customer_array['contact_person'] = $customer_name;
                $customer_array['payment_type'] = 'D';
                $customer_array['payment_mode'] = $payment_mode;
                $customer_array['key_given'] = 'N';
                $customer_array['price_hourly'] = 35;
                $customer_array['price_extra'] = 35;
                $customer_array['price_weekend'] = 35;
                $customer_array['balance'] = 0;
                $customer_array['customer_source'] = 'Justmop';
                $customer_array['customer_source_id'] = 0;
                $customer_array['customer_added_datetime'] = date('Y-m-d H:i:s');
                $customer_array['customer_last_modified_datetime'] = date('Y-m-d H:i:s');
                $customer_array['customer_booktype'] = $regular;
                $customer_array['customer_status'] = 1;
                $customer_array['odoo_synch_status'] = 0;
            
                $customer_address_array = array();
                $customer_address_array['area_id'] = $area_id;
                $customer_address_array['customer_address'] = $customer_location;
                $customer_address_array['default_address'] = 1;

                //$get_customer_by_name = $this->justmop_model->get_customer_by_name($customer_name);
                $get_customer_by_name = $this->justmop_model->get_customer_by_name_and_address($customer_name,$customer_location);
                
           
                if(!empty($get_customer_by_name))
                {
                    $customer_id = $get_customer_by_name->customer_id;
                    $get_address_by_customerid = $this->justmop_model->get_customer_address_by_id($customer_id);
                    if(!empty($get_address_by_customerid))
                    {
                        $customer_address_id = $get_address_by_customerid->customer_address_id;
                    } else {
                        $customer_id = $this->justmop_model->add_customer($customer_array);
                        if($customer_id > 0)
                        {
                            $customer_address_array['customer_id'] = $customer_id;
                            $customer_address_id = $this->justmop_model->add_customer_address($customer_address_array);
                        } else {
                            echo 'Some thing went wrong';
                            exit();
                        }
                    }
                } else {
                    $customer_id = $this->justmop_model->add_customer($customer_array);
                    if($customer_id > 0)
                    {
                        $customer_address_array['customer_id'] = $customer_id;
                        $customer_address_id = $this->justmop_model->add_customer_address($customer_address_array);
                    } else {
                        echo 'Some thing went wrong';
                        exit();
                    }
                }
                
                $push_array = array();
                if($customer_id > 0)
                {
                    $check_booking_exist = $this->justmop_model->check_booking_exist($book_reference,$service_date,$starttime,$endtime);
                    
                    if(count($check_booking_exist) == 0)
                    {
                        if($book_type == "WE")
                        {
                            $check_we_booking_assigned = $this->justmop_model->check_we_booking_assigned($customer_id,$customer_address_id,$service_week_day,$starttime,$endtime,$book_type);
                            if(count($check_we_booking_assigned) == 0)
                            {   
                                $booking_reference = "SM".$customer_id.mt_rand(1000, 9999);
                                $booking_array = array();
                                $booking_array['reference_id'] =  $booking_reference;
                                $booking_array['customer_id'] =  $customer_id;
                                $booking_array['customer_address_id'] =  $customer_address_id;
                                $booking_array['maid_id'] =  0;
                                $booking_array['service_type_id'] =  1;
                                $booking_array['service_start_date'] =  $service_date;
                                $booking_array['service_week_day'] =  $service_week_day;
                                $booking_array['time_type'] =  "";
                                $booking_array['time_from'] =  $starttime;
                                $booking_array['time_to'] =  $endtime;
                                $booking_array['booking_note'] =  $booking_note;
                                $booking_array['booking_type'] =  $book_type;
                                $booking_array['booking_category'] =  'C';
                                $booking_array['service_end'] =  $service_end;
                                $booking_array['service_end_date'] =  $service_date;
                                $booking_array['service_actual_end_date'] =  $service_date;
                                $booking_array['price_per_hr'] =  35;
                                $booking_array['pending_amount'] =  0;
                                $booking_array['discount'] =  $discount;
                                $booking_array['service_charge'] =  0;
                                $booking_array['vat_charge'] =  $vat_percentage_amount;
                                $booking_array['total_amount'] =  ($total_amount / $no_of_maids);
                                $booking_array['is_locked'] =  0;
                                $booking_array['no_of_maids'] =  $no_of_maids;
                                $booking_array['no_of_hrs'] =  $no_of_hours;
                                $booking_array['cleaning_material'] =  $cleaning_mat;
                                $booking_array['booked_from'] =  'B';
                                $booking_array['booked_by'] =  0;
                                $booking_array['booking_status'] =  0;
                                $booking_array['from_justmop'] =  1;
                                $booking_array['booked_datetime'] =  date('Y-m-d H:i:s');
                                $booking_array['payment_type'] =  $paytype;
                                $booking_array['crew_in'] =  $crew_in;
                                $booking_array['justmop_reference'] =  $book_reference;

                                for ($k = 1; $k <= $no_of_maids; $k++)
                                {
                                    $booking_id = $this->justmop_model->add_booking($booking_array);
                                    if($booking_id > 0)
                                    {
                                        //$det_array['status'] = $book_reference.',1';
                                        //array_push($push_array, (object) $det_array);
                                        //$det_array['status'] = 1;
                                        //array_push($push_array,$book_reference.',1');
                                       //echo $book_reference. 'Successfull';
                                        $html .= "<li>".$book_reference." <lable> <i class='fa fa-check-circle'></i></lable></li>";
										$insert_array = array();
										$insert_array['booking_id'] = $booking_id;
										$insert_array['just_mop_ref'] = $book_reference;
										$insert_array['mop'] = $payment_mode;
										$insert_array['ref'] = $booking_reference;
										$insert_array['service_date'] = $service_date;
										$check_ref_exist = $this->justmop_model->check_ref_exist($booking_id,$service_date);
										if(count($check_ref_exist) == 0)
										{
											$reffff_id = $this->justmop_model->add_ref($insert_array);
										}
                                    }
                                }
                            } else {
                                $html .= "<li>".$book_reference." <lable> <i class='fa fa-times-circle'></i></lable>Already Assigned</li>";
								foreach($check_we_booking_assigned as $aasigned)
								{
									$insert_array = array();
									$insert_array['booking_id'] = $aasigned->booking_id;
									$insert_array['just_mop_ref'] = $book_reference;
									$insert_array['mop'] = $payment_mode;
									$insert_array['ref'] = $aasigned->reference_id;
									$insert_array['service_date'] = $service_date;
									$check_ref_exist = $this->justmop_model->check_ref_exist($aasigned->booking_id,$service_date);
									if(count($check_ref_exist) == 0)
									{
										$reffff_id = $this->justmop_model->add_ref($insert_array);
									}
								}
								
								
                            }
                        } else {
                            $booking_reference = "SM".$customer_id.mt_rand(1000, 9999);
                            $booking_array = array();
                            $booking_array['reference_id'] =  $booking_reference;
                            $booking_array['customer_id'] =  $customer_id;
                            $booking_array['customer_address_id'] =  $customer_address_id;
                            $booking_array['maid_id'] =  0;
                            $booking_array['service_type_id'] =  1;
                            $booking_array['service_start_date'] =  $service_date;
                            $booking_array['service_week_day'] =  $service_week_day;
                            $booking_array['time_type'] =  "";
                            $booking_array['time_from'] =  $starttime;
                            $booking_array['time_to'] =  $endtime;
                            $booking_array['booking_note'] =  $booking_note;
                            $booking_array['booking_type'] =  $book_type;
                            $booking_array['booking_category'] =  'C';
                            $booking_array['service_end'] =  $service_end;
                            $booking_array['service_end_date'] =  $service_date;
                            $booking_array['service_actual_end_date'] =  $service_date;
                            $booking_array['price_per_hr'] =  35;
                            $booking_array['pending_amount'] =  0;
                            $booking_array['discount'] =  $discount;
                            $booking_array['service_charge'] =  0;
                            $booking_array['vat_charge'] =  $vat_percentage_amount;
                            $booking_array['total_amount'] =  ($total_amount / $no_of_maids);
                            $booking_array['is_locked'] =  0;
                            $booking_array['no_of_maids'] =  $no_of_maids;
                            $booking_array['no_of_hrs'] =  $no_of_hours;
                            $booking_array['cleaning_material'] =  $cleaning_mat;
                            $booking_array['booked_from'] =  'B';
                            $booking_array['booked_by'] =  0;
                            $booking_array['booking_status'] =  0;
                            $booking_array['from_justmop'] =  1;
                            $booking_array['booked_datetime'] =  date('Y-m-d H:i:s');
                            $booking_array['payment_type'] =  $paytype;
                            $booking_array['crew_in'] =  $crew_in;
                            $booking_array['justmop_reference'] =  $book_reference;
							
							// echo '<pre>';
							// print_r($booking_array);
							// exit();

                            for ($k = 1; $k <= $no_of_maids; $k++)
                            {
                                $booking_id = $this->justmop_model->add_booking($booking_array);
                                if($booking_id > 0)
                                {
                                    //$det_array['status'] = $book_reference.',1';
                                    //array_push($push_array, (object) $det_array);
                                    //$det_array['status'] = 1;
                                    //array_push($push_array,$book_reference.',1');
                                   //echo $book_reference. 'Successfull';
                                    $html .= "<li>".$book_reference." <lable> <i class='fa fa-check-circle'></i></lable></li>";
									$insert_array = array();
									$insert_array['booking_id'] = $booking_id;
									$insert_array['just_mop_ref'] = $book_reference;
									$insert_array['mop'] = $payment_mode;
									$insert_array['ref'] = $booking_reference;
									$insert_array['service_date'] = $service_date;
									$check_ref_exist = $this->justmop_model->check_ref_exist($booking_id,$service_date);
									if(count($check_ref_exist) == 0)
									{
										$reffff_id = $this->justmop_model->add_ref($insert_array);
									}
                                }
                            }
                        }
                    } else {
                        $html .= "<li>".$book_reference." <lable> <i class='fa fa-times-circle'></i></lable></li>";
                    }
                } else {
                    //$det_array['status'] = $book_reference.',0';
                    //array_push($push_array, (object) $det_array);
                    //array_push($push_array,$book_reference.',0');
                    //array_push($push_array, $det_array);
                    //echo $book_reference. 'Not successfull';
                    $html .= "<li>".$book_reference." <lable> <i class='fa fa-times-circle'></i></lable></li>";
                }
            }
            $html .= "</ul>";
        } else {
            //echo $this->upload->display_errors();
        }
        
        $data['html'] = $html;
        
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('success_list', $data, TRUE);		
        $layout_data['page_title'] = 'Justmop';
        $layout_data['meta_description'] = 'Justmop';
        $layout_data['css_files'] = array('jquery.fancybox.css','demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js','jquery.validate.min.js','mymaids.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    
    public function test()
    {
        $this->load->library('Excel');
        $file="./upload/export_appointment_2018_03_11_11_44_00 sunday.xlsx";
        $obj=PHPExcel_IOFactory::load($file);
        $cell=$obj->getActiveSheet()->getCellCollection();
        foreach($cell as $cl){
                $column=$obj->getActiveSheet()->getCell($cl)->getColumn();
                $row=$obj->getActiveSheet()->getCell($cl)->getRow();
                $data_value=$obj->getActiveSheet()->getCell($cl)->getFormattedValue();

                if($row==1){
                        $header[$row][$column]=$data_value;
                }else{
                        $arr_data[$row][$column]=$data_value;
                }
        }
        $data['header']=$header;
        $data['values']=$arr_data;
        
        foreach ($arr_data as $val)
        {
            $book_reference = $val['A'];
            if($val['B'] == "One Time")
            {
                $book_type = "OD";
                $regular = 0;
                $service_end = 1;
            } else {
                $book_type = "WE";
                $regular = 1;
                $service_end = 0;
            }
            $customer_name = $val['C'];
            
            $customer_location = $val['D'];
            $area = $val['E'];
            $service_date = $val['F'];
            $service_week_day = date('w', strtotime($service_date));
            $starttime = $val['G'];
            $no_of_hours = $val['H'];
            $endtime = date('H:i:s', strtotime($starttime.'+'.$no_of_hours.' hour'));
            $no_of_maids = $val['I'];
            if($val['J'] == 'Yes')
            {
                $cleaning_mat = 'Y';
            } else {
                $cleaning_mat = 'N';
            }
            $booking_note = $val['K'];
            $crew_in = $val['L'];
            $total_amount = $val['N'];
            $vat_percentage_amount = ($total_amount * (5/100));
            
            $discount = $val['O'];
            $get_area_id = $this->justmop_model->get_area_id_name($area);
            
            if(!empty($get_area_id))
            {
                $area_id = $get_area_id->area_id;
            } else {
                $area_id = 0;
            }
            if($val['Q'] == 'Partnership')
            {
                $payment_mode = 'Credit Card';
            } else {
                $payment_mode = $val['Q'];
            }
            if($val['Q'] == 'Cash')
            {
                $paytype = 0;
            } else {
                $paytype = 1;
            }
            $customer_array = array();
            $customer_array['odoo_customer_id'] = 0;
            $customer_array['customer_username'] = $customer_name;
            $customer_array['customer_password'] = '123456';
            $customer_array['customer_type'] = 'HO';
            $customer_array['customer_name'] = $customer_name;
            $customer_array['customer_nick_name'] = $customer_name;
            $customer_array['is_company'] = 'Y';
            $customer_array['company_name'] = 'Justmop';
            $customer_array['mobile_number_1'] = '045578370';
            $customer_array['contact_person'] = $customer_name;
            $customer_array['payment_type'] = 'D';
            $customer_array['payment_mode'] = $payment_mode;
            $customer_array['key_given'] = 'N';
            $customer_array['price_hourly'] = 35;
            $customer_array['price_extra'] = 35;
            $customer_array['price_weekend'] = 35;
            $customer_array['balance'] = 0;
            $customer_array['customer_source'] = 'Justmop';
            $customer_array['customer_source_id'] = 0;
            $customer_array['customer_added_datetime'] = date('Y-m-d H:i:s');
            $customer_array['customer_last_modified_datetime'] = date('Y-m-d H:i:s');
            $customer_array['customer_booktype'] = $regular;
            $customer_array['customer_status'] = 1;
            $customer_array['odoo_synch_status'] = 0;
            
            $customer_address_array = array();
            $customer_address_array['area_id'] = $area_id;
            $customer_address_array['customer_address'] = $customer_location;
            $customer_address_array['default_address'] = 1;
            
            $get_customer_by_name = $this->justmop_model->get_customer_by_name($customer_name);
           
            if(!empty($get_customer_by_name))
            {
                $customer_id = $get_customer_by_name->customer_id;
                $get_address_by_customerid = $this->justmop_model->get_customer_address_by_id($customer_id);
                if(!empty($get_address_by_customerid))
                {
                    $customer_address_id = $get_address_by_customerid->customer_address_id;
                } else {
                    $customer_id = $this->justmop_model->add_customer($customer_array);
                    if($customer_id > 0)
                    {
                        $customer_address_array['customer_id'] = $customer_id;
                        $customer_address_id = $this->justmop_model->add_customer($customer_address_array);
                    } else {
                        echo 'Some thing went wrong';
                        exit();
                    }
                }
            } else {
                $customer_id = $this->justmop_model->add_customer($customer_array);
                if($customer_id > 0)
                {
                    $customer_address_array['customer_id'] = $customer_id;
                    $customer_address_id = $this->justmop_model->add_customer_address($customer_address_array);
                } else {
                    echo 'Some thing went wrong';
                    exit();
                }
            }
            if($customer_id > 0)
            {
                $booking_reference = "SM".$customer_id.mt_rand(1000, 9999);
                $booking_array = array();
                $booking_array['reference_id'] =  $booking_reference;
                $booking_array['customer_id'] =  $customer_id;
                $booking_array['customer_address_id'] =  $customer_address_id;
                $booking_array['maid_id'] =  0;
                $booking_array['service_type_id'] =  1;
                $booking_array['service_start_date'] =  $service_date;
                $booking_array['service_week_day'] =  $service_week_day;
                $booking_array['time_type'] =  "";
                $booking_array['time_from'] =  $starttime;
                $booking_array['time_to'] =  $endtime;
                $booking_array['booking_note'] =  $booking_note;
                $booking_array['booking_type'] =  $book_type;
                $booking_array['booking_category'] =  'C';
                $booking_array['service_end'] =  $service_end;
                $booking_array['service_end_date'] =  $service_date;
                $booking_array['service_actual_end_date'] =  $service_date;
                $booking_array['price_per_hr'] =  35;
                $booking_array['pending_amount'] =  0;
                $booking_array['discount'] =  $discount;
                $booking_array['service_charge'] =  0;
                $booking_array['vat_charge'] =  $vat_percentage_amount;
                $booking_array['total_amount'] =  ($total_amount / $no_of_maids);
                $booking_array['is_locked'] =  0;
                $booking_array['no_of_maids'] =  $no_of_maids;
                $booking_array['no_of_hrs'] =  $no_of_hours;
                $booking_array['cleaning_material'] =  $cleaning_mat;
                $booking_array['booked_from'] =  'B';
                $booking_array['booked_by'] =  0;
                $booking_array['booking_status'] =  0;
                $booking_array['from_justmop'] =  1;
                $booking_array['booked_datetime'] =  date('Y-m-d H:i:s');
                $booking_array['payment_type'] =  $paytype;
                $booking_array['crew_in'] =  $crew_in;
                $booking_array['justmop_reference'] =  $book_reference;
                
                for ($k = 1; $k <= $no_of_maids; $k++)
                {
                    $booking_id = $this->justmop_model->add_booking($booking_array);
                    if($booking_id > 0)
                    {
                       echo $book_reference. 'Successfull';
                    }
                }
            } else {
                echo $book_reference. 'Not successfull';
            }
            
            //echo $book_reference;
            echo '<br/>';
            //exit();
        }
        
        
        
        
        
        
        
        
//        echo '<pre>';
//        print_r($arr_data);
//        echo '</pre>';
        exit();
    }
    
    public function import_data1()
    {
        $config = array(
            'upload_path' => FCPATH.'upload/',
            'allowed_types' => 'xls|xlsx'
        );
        $this->load->library('upload',$config);
        if($this->upload->do_upload('file'))
        {
            $data = $this->upload->data();
            @chmod($data['full_path'], 0777);
            
            $this->load->library('Spreadsheet_Excel_Reader');
            $this->spreadsheet_excel_reader->setOutputEncoding('CP1251');
            $this->spreadsheet_excel_reader->read($data['full_path']);
            $sheets = $this->spreadsheet_excel_reader->sheets[0];
            error_reporting(0);
            $data_excel = array();
            for ($i = 1; $i <= $sheets['numRows']; $i++) {
                if($sheets['cells'][$i][1] == '') break;
                
                $data_excel[$i - 1]['Reference No'] = $sheets['cells'][$i][1];
                $data_excel[$i - 1]['Client Type'] = $sheets['cells'][$i][2];
                $data_excel[$i - 1]['Client Name'] = $sheets['cells'][$i][3];
                //$data_excel[$i - 1][''] = $sheets['cells'][$i][1];
                
//                    for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
//                            echo "\"".$data->sheets[0]['cells'][$i][$j]."\",";
//                    }
//                    echo "\n";

            }
            echo '<pre>';
            print_r($data_excel);
            echo '</pre>';
            die();
        } else {
            echo $this->upload->display_errors();
        }
    }
    
    public function approvallist() 
    {
        ini_set('memory_limit','-1');
        if($this->input->is_ajax_request())
        {
            if($this->input->post('action') && $this->input->post('action') == 'reject-booking')
            {
                if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
                {
                    $booking_id = $this->input->post('booking_id');
                                
                    $delete_affected = $this->bookings_model->reject_booking($booking_id);
                    //$delete_affected = 1;
                    if($delete_affected > 0)
                    {
                        $response = array();
                        $response['status'] = 'success';

                        //$this->sent_booking_confirmation_mail($booking_id, 'Rejected');
                        echo json_encode($response);
                        exit();
                    }
                    else
                    {
                        $response = array();
                        $response['status'] = 'error';
                        $response['message'] = 'Invalid booking id';

                        echo json_encode($response);
                        exit();
                    }
                }
                else
                {
                    $response = array();
                    $response['status'] = 'error';
                    $response['message'] = 'Unexpected error!';

                    echo json_encode($response);
                    exit();
                }
            } 
            if($this->input->post('action') && $this->input->post('action') == 'get-free-maids' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
            {
                $booking_id = trim($this->input->post('booking_id'));
                $booking = $this->bookings_model->get_booking_by_id($booking_id);
                $same_zone = $this->input->post('same_zone');

                $not_free_maids = $this->bookings_model->get_not_free_maids_by_booking_id($booking_id, $booking->service_week_day, /*$booking->service_start_date . ' ' .*/$booking->time_from, /*$booking->service_start_date . ' ' .*/ $booking->time_to, $booking->service_start_date);
                $all_maids = $this->maids_model->get_maids();
                $time_from_stamp = strtotime(trim($booking->time_from));
                $time_to_stamp = strtotime(trim($booking->time_to));
                $booking_type = $booking->booking_type;
                $service_date = $booking->service_start_date;
                $service_end_date = $booking->service_end_date;
                                
                $nf_maids = array();
                $all_maid_list = array();    
                                
                foreach ($not_free_maids as $nmaid)
                {

                    $maid_id = $nmaid->maid_id;
                    $f_time = strtotime($nmaid->time_from);
                    $t_time = strtotime($nmaid->time_to);
                    if($booking_type == 'OD')
                    {
                                            
                        //if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                        //{
                                //continue;


                        //}
                        //else
                        //{
                        if(($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp > $t_time) || ($time_from_stamp < $f_time && $time_to_stamp > $f_time && $time_to_stamp < $t_time/**/) || ($time_from_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp <= $t_time))
                        {
                            array_push($nf_maids, $nmaid->maid_id);
                        }
                        else
                        {
                            continue;
                        }
                                                
                        //}
                                                    
                    }

                    $today_week_day = date('w', strtotime(date('Y-m-d')));                                    
                    $repeat_days = array(date('w', strtotime($service_date)));
                                    
                    if($booking_type == 'WE')
                    {
                                            
                        foreach($repeat_days as $repeat_day)
                        {
                            if($repeat_day < $today_week_day)
                            {
                                    $day_diff = (6 - $today_week_day + $repeat_day + 1);
                            }
                            else
                            {
                                    $day_diff = $repeat_day - $today_week_day;
                            }

                            $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

                            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
                            foreach($bookings_on_day as $booking_on_day)
                            {
                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_end_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                                //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                {
                                    $f_time = strtotime($booking_on_day->time_from);
                                    $t_time = strtotime($booking_on_day->time_to);

                                    if(($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp > $t_time) || ($time_from_stamp < $f_time && $time_to_stamp > $f_time && $time_to_stamp < $t_time/**/) || ($time_from_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp <= $t_time))
                                    {
                                        array_push($nf_maids, $nmaid->maid_id);
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
                                
                foreach ($all_maids as $maid)
                {
                    array_push($all_maid_list, $maid->maid_id);
                }
                $free_maids = array_diff($all_maid_list, $nf_maids);     
                                
                                 
                if(!empty($free_maids))
                {
                    //$f_maids = array_diff($free_maids, $not_free_maids);
                    $free_maid_dtls = array();
                    $free_maid_ids = array();
                    foreach ($free_maids as $f_maid)
                    {
                        $maid = $this->maids_model->get_maid_by_id($f_maid);
                        array_push($free_maid_dtls, $maid);
                        array_push($free_maid_ids, $f_maid);
                    }
                    if($same_zone == 1)
                    {
                        $zone = $this->customers_model->get_customer_zone_by_address_id($booking->customer_address_id);
                        $same_zone_maids = $this->bookings_model->get_same_zone_maids($zone->zone_id, $free_maid_ids, $booking->service_week_day);
                        $free_maid_dtls = array();
                        foreach ($same_zone_maids as $same_zone_maid){
                            $maid = $this->maids_model->get_maid_by_id($same_zone_maid->maid_id);
                            array_push($free_maid_dtls, $maid);                                            
                        }
                        if(empty($free_maid_dtls))
                        {                                           
                            $return = array();
                            $return['status'] = 'error';
                            $return['message'] = 'There are no same zone maids available';

                            echo json_encode($return);
                            exit();

                        }
                    }
                    echo json_encode($free_maid_dtls);
                    exit();
                }
                else
                {
                    $return = array();
                    $return['status'] = 'error';
                    $return['message'] = 'There are no maids available for the selected time slot';

                    echo json_encode($return);
                    exit();
                }
            }
            if($this->input->post('action') && $this->input->post('action') == 'assign-maid-status'  )
            {
                $booking_id = $this->input->post('booking_id');
                $update_booking_fields = array();
                $update_booking_fields['booked_by'] = user_authenticate();
                $update_booking_fields['booking_status'] = 1; 

                $affected_rows = $this->bookings_model->update_booking($booking_id, $update_booking_fields);
                $response = array();
                $response['status'] = 'success';

                echo json_encode($response);
                exit();

            }
            if($this->input->post('action') && $this->input->post('action') == 'delete-assign-maid-status'  )
            { 
                $booking_id = $this->input->post('booking_id');
                if($this->input->post('remarks') != "")
                {
                    $remarks = $this->input->post('remarks');
                } else {
                    $remarks = "";
                }
                $datefordelete = date('d-M-Y');
                $service_datefordelete = date('Y-m-d', strtotime($datefordelete));

                $d_booking1 = $this->bookings_model->get_booking_by_id($booking_id);

                if($d_booking1->is_locked == 1 && $d_booking1->booked_by != user_authenticate())
                {
                    echo 'locked';
                    exit();
                }
                if(isset($d_booking1->booking_id))
                {
                    if($d_booking1->booking_type == 'OD')
                    {
                        $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'delete_remarks' => $remarks), 'Delete');
                        $delete_b_fields = array();
                        $delete_b_fields['booking_id'] = $booking_id;
                        $delete_b_fields['service_date'] = $service_datefordelete;
                        $delete_b_fields['deleted_by'] = user_authenticate();

                        $affectedrows_del = $this->bookings_model->add_booking_cancel($delete_b_fields);
                        $response = array();
                        $response['status'] = 'success';
                    }
                    else
                    {
                        $this->bookings_model->update_booking($booking_id, array('booking_status' => 2, 'service_actual_end_date' => date('Y-m-d', strtotime($datefordelete . ' - 1 day')), 'service_end' => 1,'delete_remarks' => $remarks), 'Delete');
                        $delete_b_fields = array();
                        $delete_b_fields['booking_id'] = $booking_id;
                        $delete_b_fields['service_date'] = $service_datefordelete;
                        $delete_b_fields['deleted_by'] = user_authenticate();

                        $affectedrows_del = $this->bookings_model->add_booking_cancel($delete_b_fields);
                        $response = array();
                        $response['status'] = 'success';
                    }
                    echo json_encode($response);
                    exit();
                }              
            }
                       
            if($this->input->post('action') && $this->input->post('action') == 'assign-maid' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('maid_id') )//&& is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0
            {

                $booking_id = $this->input->post('booking_id');
                $booking = $this->bookings_model->get_booking_by_id($booking_id);
                $maid_id = explode(",", trim($this->input->post('maid_id')));                               

                $update_booking_fields = array();
                $update_booking_fields['maid_id'] = $maid_id[0];
                $update_booking_fields['booked_by'] = user_authenticate();
                $update_booking_fields['booking_status'] = 1;

                $affected_rows = $this->bookings_model->update_booking($booking_id, $update_booking_fields);

                array_shift($maid_id);

                if(is_array($maid_id))
                {
                                        
                    foreach ($maid_id as $m_id)
                    {
                        $booking_fields = array();
                        $booking_fields['customer_id'] = $booking->customer_id;
                        $booking_fields['customer_address_id'] = $booking->customer_address_id;
                        $booking_fields['maid_id'] = $m_id;
                        $booking_fields['service_type_id'] = $booking->service_type_id;
                        $booking_fields['service_start_date'] = $booking->service_start_date;
                        $booking_fields['service_week_day'] = $booking->service_week_day;
                        $booking_fields['time_from'] = $booking->time_from;
                        $booking_fields['time_to'] = $booking->time_to;
                        $booking_fields['booking_type'] = $booking->booking_type;
                        $booking_fields['service_end'] = $booking->service_end;
                        $booking_fields['service_end_date'] = $booking->service_end_date;					
                        $booking_fields['booking_note'] = $booking->booking_note;					
                        $booking_fields['pending_amount'] = $booking->pending_amount;                                        
                        $booking_fields['discount'] = $booking->discount;
                        $booking_fields['booked_from'] = 'W';
						$booking_fields['booking_category'] = 'C';
                        $booking_fields['booked_by'] = user_authenticate();
                        $booking_fields['booking_status'] = 1;
                        $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
                        if($booking->booking_type == 'WE')
                        {
                            $updatefield = array();
                            $updatefield['customer_booktype'] = 1;
                            $updatess = $this->customers_model->update_booktype($updatefield,$customer_id);
                        }
                        $b_id = $this->bookings_model->add_booking($booking_fields);
                                            
                    }
                }
                                
                                
                //$this->sent_booking_confirmation_mail($booking_id, 'Approved');
                //if($affected_rows > 0)
                //{
                    $response = array();
                    $response['status'] = 'success';

                    echo json_encode($response);
                    exit();
                //}
                                
            }
            else
            {
                $response = array();
                $response['status'] = 'error';
                $response['message'] = 'Unexpected error!';

                echo json_encode($response);
                exit();
            }
        }
                
        if($this->input->post('filter') && strlen($this->input->post('filter')) > 0)
        {
            $filter = $this->input->post('filter');
        }
        else
        {
            $filter = 'Pending';
        }
		
		if($this->input->post('service_date'))
		{
			$s_date = $this->input->post('service_date');
			$s_date = explode("/", $s_date);
			$service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
		}
		else
		{                
			$service_date = date('Y-m-d');
			//$service_date = '2015-06-17';
		}
		
                
        $data = array();
		$data['formatted_date'] = $service_date;
		$data['service_date'] = date('d/m/Y', strtotime($service_date));
        $data['approval_list'] = $this->justmop_model->get_all_booking_approval_list($filter,$service_date);
        $data['filter'] = $filter;
        $book_source=array("M"=>"Mobile","W"=>"Web","A"=>"Admin");
        $data['source']=$book_source;
        $layout_data['content_body'] = $this->load->view('justmop_booking_approval_list', $data, TRUE);
        $layout_data['page_title'] = 'Booking Approval List';
        $layout_data['meta_description'] = 'Booking Approval List';
        $layout_data['css_files'] = array('datepicker.css','jquery.fancybox.css','demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['bookings_active'] = '1';
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','bootstrap-datepicker.js','base.js', 'mymaids.js', 'moment.min.js','jquery.dataTables.min.js', 'bootstrap.js'); //'hm.js');
        $this->load->view('layouts/default', $layout_data);
                
    }
	
	function convert_date_format($old_date = '')
	{
		echo 'hai';
		exit();
		$old_date = trim($old_date);
 
		if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $old_date)) // MySQL-compatible YYYY-MM-DD format
		{
			$new_date = $old_date;
		}
		elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/', $old_date)) // DD-MM-YYYY format
		{
			$new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-' . substr($old_date, 0, 2);
		}
		elseif (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{2}$/', $old_date)) // DD-MM-YY format
		{
			$new_date = substr($old_date, 6, 4) . '-' . substr($old_date, 3, 2) . '-20' . substr($old_date, 0, 2);
		}
		else // Any other format. Set it as an empty date.
		{
			$new_date = '0000-00-00';
		}
		echo $new_date;
		exit();
	}
	
	public function testingg()
	{
		$val = "Duration : 4 Hours, Number of Cleaners : 10, Material : No";
		$array = explode(" ", $val);
		echo $array[2].','.substr($array[8], 0, -1).','.$array[11];
		
		
		//preg_match('/\d+/', $val, $matches); 
		//echo $matches[0];
		exit();
	}
}
