<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Driver extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('dispatch_model');
        $this->load->model('settings_model');
        $this->load->model('zones_model');
        $this->load->model('driver_model');
    }
    public function index()
    {
        $data['settings'] = $this->settings_model->get_settings();
        $data['drivers'] = $this->driver_model->get_drivers_for_dropdown();
        $data['zones'] = $this->db->select('z.zone_id,z.zone_name')->from('zones as z')->where('z.zone_status', 1)->order_by('z.zone_name')->get()->result();
        $data['areas'] = $this->db->select('a.area_id,a.area_name')->from('areas as a')->where('a.deleted_at', null)->order_by('a.area_name')->get()->result();
        $data['maids'] = $this->db->select('m.maid_id,m.maid_name')
            ->from('maids as m')
            ->where('m.employee_type_id', 1)
            ->where('m.maid_status', 1)
            ->order_by('m.maid_name', 'ASC')->get()->result();
        // $data['maids'] = $this->db->select('a.area_id,a.area_name')->from('areas as a')->where('a.deleted_at', null)->order_by('a.area_name')->get()->result();
        $data['service_date'] = date('Y-m-d');
        //die(json_encode($data['drivers'], JSON_PRETTY_PRINT));
        $layout_data['page_title'] = 'Driver';
        $layout_data['meta_description'] = 'Driver';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css', 'toastr.min.css', 'flatpickr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js', 'driver.js', 'driver.new_availability.js', 'driver.edit_availability.js', 'flatpickr.js');
        $layout_data['content_body'] = $this->load->view('driver/driver-tabbed', $data, true);
        $this->load->view('layouts/default', $layout_data);
    }
    public function get_areas_by_zone()
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->select('a.area_id,a.area_name')
            ->from('areas as a')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            ->where('a.zone_id', $this->input->get('zone_id'))
            ->where('z.zone_status', 1)
            ->where('a.deleted_at', null);
        $areas = $this->db->get();
        $areas = $areas->result();
        echo json_encode($areas, JSON_PRETTY_PRINT);
    }

    public function edit_driver_availability()
    {
        $driver_availability_id = $this->input->post('driver_availability_id');
        $result = $this->driver_model->get_driver_details($driver_availability_id);
        echo json_encode($result);
    }
    public function save_driver_availability()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data['driver_id'] = $this->input->post('driver_id');
        $data1 = $this->input->post('area_id'); 
        $data['date_from'] = DateTime::createFromFormat('d/m/Y', $this->input->post('date_from'))->format('Y-m-d');
        $data['date_to'] = DateTime::createFromFormat('d/m/Y', $this->input->post('date_to'))->format('Y-m-d');
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        foreach ($data1 as  $area_id) {
            $areaid = $area_id;
            $this->driver_model->save_driver_availability(array('driver_id' => $data['driver_id'], 'area_id' => $areaid, 'date_from' => $data['date_from'], 'date_to' =>$data['date_to'], 'created_at' =>$data['created_at'], 'updated_at' =>$data['updated_at']));
        }
        //    $this->db->insert('driver_availability', $data);
        //     $this->db->insert_id();
        $respose['status'] = true;
        $respose['message'] = "Driver avalability saved successfully.";
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function edit_driver()
    {
        // log_message('error','edit_driver function '.json_encode($this->input->post()));
        $edit_area_ids = $this->input->post('edit_area_id');
        $edit_date_from = DateTime::createFromFormat('d/m/Y', $this->input->post('edit_date_from'))->format('Y-m-d');
        $edit_date_to = DateTime::createFromFormat('d/m/Y', $this->input->post('edit_date_to'))->format('Y-m-d');
        // $edit_date_from = $this->input->post('edit_date_from');
        // $edit_date_to = $this->input->post('edit_date_to');
        $edit_driver_avalability_id = $this->input->post('edit_driver_avalability_id');
        $edit_driver_id = $this->input->post('edit_driver_id');

        foreach ($edit_area_ids as $area_id) {
            $area_exist = $this->driver_model->get_driver_availability_by_area($edit_date_from,$edit_date_to, $area_id, $edit_driver_id);
            if ($area_exist) {
                $data = array(
                    'driver_id' => $edit_driver_id,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'date_from' => $edit_date_from,
                    'date_to' => $edit_date_to,
                );
    
                foreach ($area_exist as $exist) {
                    $this->driver_model->update_driver_availability($data, $exist->driver_availability_id);
                }
            } else {
                $data = array(
                    'area_id' => $area_id,
                    'driver_id' => $edit_driver_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'date_from' => $edit_date_from,
                    'date_to' => $edit_date_to,
                );
    
                $this->driver_model->save_driver_availability($data);
            }
        }
        $respose['status'] = true;
        $respose['message'] = "Driver avalability edit successfully.";
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function driver_area_exist()
    {
        log_message('error','post driver Exist '.json_encode($this->input->post()));
        $areaId = $this->input->post('edit_area_id');
        $driver_id = $this->input->post('edit_driver_id');
        $edit_driver_avalability_id = $this->input->post('edit_driver_avalability_id');
       // $edit_date_from = $this->input->post('edit_date_from');
        $edit_date_from = DateTime::createFromFormat('d/m/Y', $this->input->post('edit_date_from'))->format('Y-m-d');
        $driver_area_exist = $this->driver_model->driver_area_exist_for_driver($edit_date_from, $areaId,$driver_id);
        // print_r($driver_area_exist);
        if (sizeof($driver_area_exist))
        {
            $response['status'] = "success";
            $response['message'] = "Selected area already exist in this date";
            echo json_encode($response);
        } else {
            $response['status'] = "fail";
            echo json_encode($response);
        }
    }
    public function mapping_data()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        $response['data'] = [];
        /**************************************** */
        $this->db->select('e.maid_id as driver_id,e.maid_name as driver_name')
            ->from('maids as e')
            ->where('e.employee_type_id', 2)
            ->where('e.maid_status', 1);
        $drivers = $this->db->get();
        $drivers = $drivers->result();
        $this->db->flush_cache();
        //die(json_encode($drivers, JSON_PRETTY_PRINT));
        /**************************************** */
        $this->db->select('
        da.driver_availability_id,
        da.driver_id,
        da.area_id,
        a.area_name,
        z.zone_id,
        z.zone_name,
        da.date_from,
        da.date_to,
        m.maid_name as driver_name')
            ->from('driver_availability as da')
            ->join('areas as a', 'da.area_id = a.area_id', 'left')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids as m', 'da.driver_id = m.maid_id', 'left')
            ->where('a.deleted_at', null)
            ->where('da.deleted_at', null)
            ->where(array('da.date_from <=' => $service_date, 'da.date_to >=' => $service_date));
        $driver_availabilies = $this->db->get();
        $driver_availabilies = $driver_availabilies->result();
        //die(json_encode($driver_availabilies, JSON_PRETTY_PRINT));
        /**************************************** */
        foreach ($drivers as $driver) {
            $response['data'][$driver->driver_id]['driver'] = $driver;
            $response['data'][$driver->driver_id]['availabilities'] = [];
            foreach ($driver_availabilies as $driver_availability) {
                if ($driver_availability->driver_id == $driver->driver_id) {
                    $response['data'][$driver->driver_id]['availabilities'][] = $driver_availability;
                }
            }
        }
        $response['data'] = array_values($response['data']);
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function mapping_data_all()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        $response['data'] = [];
        /**************************************** */
        $this->db->select('a.area_id,a.area_name,z.zone_id,z.zone_name,')
            ->from('areas as a')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            ->where('a.deleted_at', null);
        $areas = $this->db->get();
        $areas = $areas->result();
        $this->db->flush_cache();
        //die(json_encode($areas, JSON_PRETTY_PRINT));
        /**************************************** */
        $this->db->select('
        da.driver_availability_id,
        da.driver_id,da.area_id,
        a.area_name,
        da.date_from,
        da.date_to,
        m.maid_name as driver_name')
            ->from('driver_availability as da')
            ->join('areas as a', 'da.area_id = a.area_id', 'left')
            ->join('maids as m', 'da.driver_id = m.maid_id', 'left')
            ->where('a.deleted_at', null)
            ->where('da.deleted_at', null);
        $driver_availabilies = $this->db->get();
        $driver_availabilies = $driver_availabilies->result();
        /**************************************** */
        foreach ($areas as $area) {
            $response['data'][$area->area_id]['area'] = $area;
            $response['data'][$area->area_id]['availabilities'] = [];
            foreach ($driver_availabilies as $driver_availability) {
                if ($driver_availability->area_id == $area->area_id) {
                    $response['data'][$area->area_id]['availabilities'][] = $driver_availability;
                }
            }
        }
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function mapping_data_all_under_driver()
    {
        header('Content-Type: application/json; charset=utf-8');
        $response['data'] = [];
        /**************************************** */
        $this->db->select('e.maid_id as driver_id,e.maid_name as driver_name')
            ->from('maids as e')
            ->where('e.employee_type_id', 2)
            ->where('e.maid_status', 1);
        $drivers = $this->db->get();
        $drivers = $drivers->result();
        $this->db->flush_cache();
        //die(json_encode($drivers, JSON_PRETTY_PRINT));
        /**************************************** */
        $this->db->select('
        da.driver_availability_id,
        da.driver_id,
        z.zone_id,
        z.zone_name,
        da.area_id,
        a.area_name,
        da.date_from,
        da.date_to,
        m.maid_name as driver_name')
            ->from('driver_availability as da')
            ->join('areas as a', 'da.area_id = a.area_id', 'left')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids as m', 'da.driver_id = m.maid_id', 'left')
            ->where('a.deleted_at', null)
            ->where('da.deleted_at', null)
            ->where('da.date_to >=', date('Y-m-d'));
        $driver_availabilies = $this->db->get();
        $driver_availabilies = $driver_availabilies->result();
        //die(json_encode($driver_availabilies, JSON_PRETTY_PRINT));
        /**************************************** */
        foreach ($drivers as $driver) {
            $response['data'][$driver->driver_id]['driver'] = $driver;
            $response['data'][$driver->driver_id]['availabilities'] = [];
            foreach ($driver_availabilies as $driver_availability) {
                if ($driver_availability->driver_id == $driver->driver_id) {
                    $response['data'][$driver->driver_id]['availabilities'][] = $driver_availability;
                }
            }
        }
        $response['data'] = array_values($response['data']);
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function update_schedule_driver()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data['driver_id'] = $this->input->post('driver_id');
        $this->db->where('day_service_id', $this->input->post('day_service_id'));
        $this->db->where('dispatch_status', 2);
        $this->db->update('day_services', $data);
        $response['status'] = true;
        $response['message'] = "Driver for schedule updated successfully.";
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function insert_unconfirmed_schedule_driver()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data = json_decode(file_get_contents('php://input'), true);
        
        if (!$data) {
            $response['status'] = false;
            $response['message'] = "No data received.";
            echo json_encode($response, JSON_PRETTY_PRINT);
            return;
        }

        $this->db->trans_begin();

        // Loop through each schedule data (day_service_id and its fields)
        foreach ($data as $day_service_id => $fields) {
            
            // Check if the required fields for insertion are present
            if (empty($fields['driver_name']) || empty($fields['pick_up_time']) || empty($fields['pick_address'])) {
                $response['status'] = false;
                $response['message'] = "Please fill manadatoryfields";
                echo json_encode($response, JSON_PRETTY_PRINT);
                $this->db->trans_rollback(); 
                return;
            }

            
            $unconfirmed_schedule_id = isset($fields['unconfirmed_schedule_id']) ? $fields['unconfirmed_schedule_id'] : null;
            
         
            if (empty($day_service_id)) {
                $response['status'] = false;
                $response['message'] = "Missing day_service_id for insertion.";
                echo json_encode($response, JSON_PRETTY_PRINT);
                $this->db->trans_rollback(); 
                return;
            }
            $driver_name =  $this->db->select('m.maid_id as driver_id,m.maid_name as driver_name')
                ->from('maids as m')
                ->where('m.employee_type_id', 2)
                ->where('m.maid_status', 1)
                ->where('m.maid_id', $fields['driver_name'])
                ->get()->result();

            // print_r($driver_name);die();
            $pick_up_time_24hr = $this->convertTo24HourFormat($fields['pick_up_time']);
            $drop_time_24hr = $this->convertTo24HourFormat($fields['drop_time']);
            $insertData = [
                'day_service_id' => $day_service_id,  // Unique ID for the schedule
                'driver_id' => $fields['driver_name'],  
                'pick_time' => $pick_up_time_24hr,  
                'drop_time' => $drop_time_24hr,  
                'drop_address' => $fields['customer_address'],  
                'pick_address' => $fields['pick_address'],  
            ];

            if (empty($fields['unconfirmed_schedule_id'])) {
               
                $this->db->insert('driver_unconfirmed_schedules', $insertData);
                $id = $this->db->insert_id();
                // print_r($id);die();
            } else {
                $this->db->where('unconfirmed_schedule_id', $fields['unconfirmed_schedule_id']);
                $this->db->update('driver_unconfirmed_schedules', $insertData);
            }
            
        }

        $this->db->trans_commit();
        $response['driver_name'] = $driver_name ;
        $response['unconfirmed_schedule_id'] = $unconfirmed_schedule_id ? : $id;
        $response['status'] = "success";
        $response['message'] = "Driver unconfirmed schedules inserted or updated successfully.";
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    private function convertTo24HourFormat($time12hr)
    {
        $dateTime = DateTime::createFromFormat('h:i A', $time12hr);
        return $dateTime ? $dateTime->format('H:i') : null;
    }
    public function confirm_drivers_for_schedule()
    {
        $day_service_ids = $this->input->post('day_service_ids') ?: null;
        $this->db->trans_begin();
        try {
            foreach ($day_service_ids as $key => $day_service_id) {
                $row_data = array();
                $update = $this->driver_model->confirm_driver_dispatch($day_service_id, $row_data);
            }
            $this->db->trans_commit();
            $respose['status'] = true;
            $respose['message'] = "Driver Dispatches confirmed successfully.";
            $respose['data'] = $rows_data;
            die(json_encode($respose, JSON_PRETTY_PRINT));
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $respose['status'] = false;
            $respose['message'] = $e->getMessage();
            die(json_encode($respose, JSON_PRETTY_PRINT));
        }
    }
    public function day_services_driver_unconfirmed()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        
        // Query the database as you already do
        $this->db->select('
            ds.day_service_id,
            ds.booking_id,
            c.customer_id,
            c.customer_name,
            ds.driver_id,
            m.maid_name,
            m.maid_id,
            dr.maid_name as driver_name,
            l.location_id,
            a.area_id,
            z.zone_id,
            l.location_name,
            a.area_name,
            z.zone_name,
            ds.service_date,
            ds.time_from,
            ds.time_to,
            ca.customer_address,
            du.unconfirmed_schedule_id,
            du.pick_time,
            du.drop_time,
            du.pick_address,
            du.drop_address,
            du.driver_id as du_driver_id,
            dus.maid_id as dus_maid_id,
            dus.maid_name as dus_maid_name,
            du.is_driver_unconfirmed,
            du.deleted_at,  
             da.deleted_at as da_deleted_at,
            da.area_id as da_area_id,
            da.date_from,
            da.driver_id as da_driver_id,
            mda.maid_name as mda_driver_name,
        ')
            ->from('day_services as ds')
            ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->join('locations as l', 'ca.location_id = l.location_id', 'left')
            ->join('areas as a', 'a.area_id = l.area_id', 'left')
            ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
            ->join('maids as dr', 'dr.maid_id = ds.driver_id', 'left')
            ->join('driver_unconfirmed_schedules as du', 'ds.day_service_id = du.day_service_id', 'left')
            ->join('maids as dus', 'du.driver_id = dus.maid_id', 'left')
            ->join('driver_availability as da',  'a.area_id = da.area_id AND da.date_from = "' . $service_date . '"', 'left')
            ->join('maids as mda',  'da.driver_id = mda.maid_id', 'left')
           ->where('ds.service_date', $service_date)
            // ->where('ds.is_driver_confirmed', null)
            // ->where('du.is_driver_unconfirmed', null)
            // ->where('du.deleted_at', null)
            ->where('ds.dispatch_status', 2)
            ->order_by('m.maid_id', "ASC")
            ->order_by('ds.time_from', "ASC")
            ->order_by('ds.day_service_id', "ASC");
                
        // filtering
        if ($this->input->get('driver_id')) {

            // $this->db->where('du.driver_id', $this->input->get('driver_id'));
            $driver_id = $this->input->get('driver_id');
            $this->db->where('(du.driver_id = ' . $driver_id . ' OR da.driver_id = ' . $driver_id . ')');
        }
        if ($this->input->get('zone_id')) {
            $this->db->where('z.zone_id', $this->input->get('zone_id'));
        }
        if ($this->input->get('maid_id')) {
            $this->db->where('m.maid_id', $this->input->get('maid_id'));
        }
        $day_services = $this->db->get();
        $response['data'] = $day_services->result();

        $previous_drop_address = null;
        $previous_maid_id = null;
        $first_booking_drop_address = null;

        foreach ($response['data'] as $key => $schedule) {
            if ($schedule->unconfirmed_schedule_id != "") {
                $response['data'][$key]->pick_address = $schedule->pick_address;
                $response['data'][$key]->drop_address = $schedule->drop_address;
                // $response['data'][$key]->pick_time = $schedule->pick_time;
                // $response['data'][$key]->drop_time = $schedule->drop_time;
                
                // if ($previous_maid_id == $schedule->maid_id) {
                //     $response['data'][$key]->pick_address = $previous_drop_address;
                // } else {
                //     // First booking case
                //     $response['data'][$key]->pick_address = $schedule->pick_address;
                // }
                $date = new DateTime($schedule->pick_time);
                // $date->modify('-30 minutes');
                $new_time = $date->format('h:i A');
                $response['data'][$key]->pick_time = $new_time;
                $response['data'][$key]->drop_time = $schedule->drop_time;
                if ($schedule->deleted_at == null) {
                    $previous_drop_address = $schedule->drop_address;
                    $previous_time = $schedule->time_to;  
                    $previous_maid_id = $schedule->maid_id;
                    if ($response['data'][$key + 1]->pick_address == null && $response['data'][$key + 1]->maid_id == $schedule->maid_id) {
                        $response['data'][$key + 1]->pick_address = $previous_drop_address;
                    } 
                    
                    // if ($key == count($response['data']) - 1 || $response['data'][$key + 1]->maid_id != $schedule->maid_id) {
                    //     // Add new entry with the same maid_id, drop_time as '07:00 PM' and drop_address as 'Accommodation'
        
                    //     $new_schedule = clone $schedule;
                    //     $new_schedule->pick_address = $schedule->drop_address; 
                    //     $new_schedule->drop_time = "19:00 PM";  
                    //     // $new_schedule->pick_time = "07:00 PM";  
                    //     $new_schedule->drop_address = "Accommodation";
                    //     $new_schedule->maid_id = $schedule->maid_id;  
                    //     // $new_schedule->service_date = null;  
                    //     // $new_schedule->booking_id = null;
                    //     $new_schedule->unconfirmed_schedule_id = null;

                    //     $new_schedule->is_driver_unconfirmed = null;
                        
                        
                    //     if ($new_schedule->drop_time != "19:00 PM") {
                    //         $response['data'][] = $new_schedule;
                    //     } 
                        
                    //     // $response['data'][] = $new_schedule;
                    // }
                    // if ($key == count($response['data']) - 1 || $response['data'][$key + 1]->maid_id != $schedule->maid_id) {
                    //     $new_schedule = clone $schedule;
                    //     $new_schedule->pick_address = $schedule->drop_address;
                    //     $new_schedule->drop_time = "19:00 PM";
                    //     $new_schedule->drop_address = "Accommodation";
                    //     $new_schedule->maid_id = $schedule->maid_id;
                    //     $new_schedule->unconfirmed_schedule_id = null;
                    //     $new_schedule->is_driver_unconfirmed = null;
                    //     $exists = false;
                    //     foreach ($response['data'] as $existing_schedule) {
                    //         if ($existing_schedule->maid_id == $new_schedule->maid_id && 
                    //             $existing_schedule->pick_address == $new_schedule->pick_address &&
                    //             $existing_schedule->drop_address == $new_schedule->drop_address) {
                    //             $exists = true;
                    //             break;
                    //         }
                    //     }
                    
                    //     if (!$exists) {
                    //         $response['data'][] = $new_schedule;
                    //     }
                    // }
                    
                    
                }
                // else {
                //     $response['data'][$key + 1]->pick_address =   $response['data'][$key + 1]->pick_address;
                    
                // }               
                if ($schedule->dus_maid_id == $schedule->du_driver_id) {
                    $response['data'][$key]->driver_name = $schedule->dus_maid_name;
                    $response['data'][$key]->driver_id = $schedule->du_driver_id;
                }
            } else {
                if ($previous_maid_id == $schedule->maid_id) {     
                    $response['data'][$key]->pick_address = $previous_drop_address;
                    // $response['data'][$key]->pick_time = $new_time;
                    $date = new DateTime($previous_time);
                    // $date->modify('-30 minutes');
                    $new_time = $date->format('h:i:A');
                    $response['data'][$key]->pick_time = $new_time;
                }
                
                else {
                    $response['data'][$key]->pick_address = "Accommodation";
                    $response['data'][$key]->pick_time = "06:45 AM";
                }
                $previous_maid_id = $schedule->maid_id;
                $previous_drop_address = $schedule->customer_address;   
                $previous_time = $schedule->time_to;   
                // if ($key == count($response['data']) - 1 || $response['data'][$key + 1]->maid_id != $schedule->maid_id) {
                //     // Add new entry with the same maid_id, drop_time as '07:00 PM' and drop_address as 'Accommodation'
                //         // $previous_drop_address = $schedule->drop_address;
    
                //     $new_schedule = clone $schedule;
                //     $new_schedule->pick_address = $schedule->drop_address; // Default pick address
                //     $new_schedule->drop_time = "19:00 PM";  // Set the drop time
                //     // $new_schedule->pick_time = "07:00 PM";  // Set the drop time
                //     $new_schedule->drop_address = "Accommodation";  // Set the drop address
                //     $new_schedule->maid_id = $schedule->maid_id;  // Same maid_id
                    
                //     // Optionally, you can set the other fields to be blank or leave them as is
                //     // $new_schedule->service_date = null;  // You can adjust this if needed
                //     $new_schedule->unconfirmed_schedule_id = null;
                //     $new_schedule->is_driver_unconfirmed = null;
                    
                //     // Add the new entry to the response data
                //     $response['data'][] = $new_schedule;
                // }
                
            }
           
            // Mapping driver
            // $this->db->select('
            // da.driver_availability_id,
            // da.driver_id,
            // da.area_id,
            // a.area_name,
            // z.zone_id,
            // z.zone_name,
            // da.date_from,
            // da.date_to,
            // m.maid_name as driver_name')
            //     ->from('driver_availability as da')
            //     ->join('areas as a', 'da.area_id = a.area_id', 'left')
            //     ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
            //     ->join('maids as m', 'da.driver_id = m.maid_id', 'left')
            //     ->where('a.deleted_at', null)
            //     ->where('da.deleted_at', null)
            //     ->where(array('da.date_from <=' => $service_date, 'da.date_to >=' => $service_date));
            // $driver_availabilies = $this->db->get();
            // $driver_availabilies = $driver_availabilies->result();
            // if ($schedule->driver_id == "") {
            //     foreach ($driver_availabilies as $driver_availability) {
            //         if ($driver_availability->area_id == $schedule->area_id) {
            //             $response['data'][$key]->driver_id = $driver_availability->driver_id;
            //             $response['data'][$key]->driver_name = $driver_availability->driver_name;
            //         }
            //     }
            // }
            if ($schedule->driver_id == "") {
                // if($schedule->date_from == $service_date) {
                if($schedule->da_deleted_at == null) {
                    if ($schedule->da_area_id == $schedule->area_id) {
                        $response['data'][$key]->driver_id = $schedule->da_driver_id;
                        $response['data'][$key]->driver_name = $schedule->mda_driver_name;
                    }
                } 
            }
            // }
            // if ($schedule->is_driver_unconfirmed != "") {
            //     unset($response['data'][$key]);
            // }    
        }
        function compare_pick_time($a, $b) {
            $timeA = DateTime::createFromFormat('h:i A', $a->pick_time);
            $timeB = DateTime::createFromFormat('h:i A', $b->pick_time);
        
            return $timeA <=> $timeB;
        }
        
        $groupedByMaid = [];
        foreach ($response['data'] as $schedule) {
            $groupedByMaid[$schedule->maid_id][] = $schedule;
        }
        
        // Sort each group by pick_time
        foreach ($groupedByMaid as $maid_id => $schedules) {
            usort($schedules, 'compare_pick_time');
            
            // Update the pick_address of the last schedule for each maid_id
            $previousSchedule = null;
            $lastSchedule = end($schedules); 
            foreach ($schedules as $index => $schedule) {
                if ($previousSchedule) {
                    // Set pick_address of the current schedule to the drop_address of the previous schedule
                    $schedules[$index]->pick_address = $previousSchedule->drop_address ? :$previousSchedule->customer_address;
                    // $schedules[$index]->driver_id = $previousSchedule->du_driver_id ? :$previousSchedule->driver_id;
                    // $schedules[$index]->driver_name= $previousSchedule->dus_maid_name ? :$previousSchedule->driver_name;
                   
                }
                if ($schedule === $lastSchedule) {
                    // Set the last schedule's drop time to '07:00 PM'
                    $schedules[$index]->drop_time = "19:00 PM";
                    
                    // Set pick_time of the last schedule to the previous maid's time_to
                    if ($previousSchedule) {
                        $time_to = new DateTime($previousSchedule->time_to);
                        
                        $time_to = $time_to->format('h:i:A');
                        $schedules[$index]->pick_time = $time_to;
                    }
                }
                // Update previous schedule to the current one for the next iteration
                $previousSchedule = $schedule;
            }
        
            // Reassign the sorted schedules back to the grouped array
            $groupedByMaid[$maid_id] = $schedules;
        }
        
        // Flatten the grouped and sorted schedules into a single array
        $sortedData = [];
        foreach ($groupedByMaid as $maid_id => $schedules) {
            foreach ($schedules as $schedule) {
                $sortedData[] = $schedule;
            }
        }
        $response['data'] = $sortedData; 
        foreach ($response['data'] as $key => $schedule) {
            if ($schedule->is_driver_unconfirmed != "") {
            unset($response['data'][$key]); 
            }
        } 
        // print_r($response['data']);die();
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function undo_unconfirm_driver()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data['deleted_at'] = null;
        // $this->db->where('day_service_id', $this->input->post('day_service_id'));
        // $this->db->where('dispatch_status', 2);
        // $this->db->update('day_services', $data);
        $this->db->where('unconfirmed_schedule_id', $this->input->post('unconfirmed_schedule_id'));
        // $this->db->where('dispatch_status', 2);
        $this->db->update('driver_unconfirmed_schedules', $data);
        $response['status'] = true;
        $response['message'] = "driver rollback successfully.";
        echo json_encode($response, JSON_PRETTY_PRINT);
    } 
    public function undo_confirm_driver()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data1['deleted_at'] = date('Y-m-d H:i:s');
        $this->db->where('confirmed_schedule_id', $this->input->post('confirmed_schedule_id'));
        // $this->db->update('driver_confirmed_schedules', $data1);
        $this->db->delete('driver_confirmed_schedules');
        $data['is_driver_confirmed'] = null;
        $this->db->where('day_service_id', $this->input->post('day_service_id'));
        $this->db->where('dispatch_status', 2);
        $this->db->update('day_services', $data);
        $data2['is_driver_unconfirmed'] = null;
        $this->db->where('unconfirmed_schedule_id', $this->input->post('unconfirmed_schedule_id'));
        // $this->db->where('dispatch_status', 2);
        $this->db->update('driver_unconfirmed_schedules', $data2);
        $response['status'] = true;
        $response['message'] = "Rollbacked driver dispatch successfully.";
        echo json_encode($response, JSON_PRETTY_PRINT);
    }    
    public function undo_confirm_driver_multiple()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data1['deleted_at'] = date('Y-m-d H:i:s');
        // $unconfirmed_schedule_ids = $this->input->post('schedulesData') ?: null;
        $schedulesData = isset($_POST['schedulesData']) ? $_POST['schedulesData'] : [];
        
        // print_r($schedules);die();
        foreach ($schedulesData as $schedule) {
            $insertData = [ 
                'is_driver_unconfirmed' => null 
            ];
            $this->db->where('unconfirmed_schedule_id', $schedule['unconfirmed_schedule_id']);
            $this->db->update('driver_unconfirmed_schedules', $insertData);

        }
        $confirmed_schedule_ids = $this->input->post('confirmed_schedule_ids') ?: null;
        $this->db->where_in('confirmed_schedule_id', $confirmed_schedule_ids);
        // $this->db->update('driver_confirmed_schedules', $data1);
        $this->db->delete('driver_confirmed_schedules');
        $data['is_driver_confirmed'] = null;
        $day_service_ids = $this->input->post('day_service_ids') ?: null;
        $this->db->where_in('day_service_id', $day_service_ids);
        $this->db->where('dispatch_status', 2);
        $this->db->update('day_services', $data);
        $response['status'] = true;
        $response['message'] = "Rollbacked driver dispatch successfully.";
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function day_services_driver_confirmed()
    {
        header('Content-Type: application/json; charset=utf-8');
        $service_date = $this->input->get('service_date');
        /**************************************** */
        // $this->db->select('
        //     ds.day_service_id,
        //     ds.booking_id,
        //     c.customer_name,
        //     ds.driver_id,
        //     m.maid_name,
        //     dr.maid_name as driver_name,
        //     l.location_id,
        //     a.area_id,
        //     z.zone_id,
        //     l.location_name,
        //     a.area_name,
        //     z.zone_name,
        //     ds.service_date,
        //     ds.time_from,
        //     ds.time_to,
        //     ca.customer_address,
        //     du.unconfirmed_schedule_id,
        //     du.pick_time,
        //     du.drop_time,
        //     du.pick_address,
        //     du.driver_id as du_driver_id,
        // ')
        //     ->from('day_services as ds')
        //     ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
        //     ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
        //     ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
        //     ->join('locations as l', 'ca.location_id = l.location_id', 'left')
        //     ->join('areas as a', 'a.area_id = l.area_id', 'left')
        //     ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
        //     ->join('maids as m', 'm.maid_id = ds.maid_id', 'left')
        //     ->join('maids as dr', 'dr.maid_id = ds.driver_id', 'left')
        //     ->join('driver_unconfirmed_schedules as du', 'ds.day_service_id = du.day_service_id', 'left')
        //     ->where('ds.service_date', $service_date)
        //     ->where('ds.is_driver_confirmed', 1)
        //     ->where('ds.dispatch_status', 2)
        //     ->order_by('ds.day_service_id', "ASC");
        $this->db->select('
        dc.confirmed_schedule_id,
        dc.pick_time,
        dc.drop_time,
        dc.pick_address,
        dc.drop_address,
        dc.driver_id as du_driver_id,
        ds.day_service_id,
        ds.booking_id,
        c.customer_name,
        ds.driver_id,
        m.maid_name,
        dr.maid_name as driver_name,
        l.location_id,
        a.area_id,
        z.zone_id,
        l.location_name,
        a.area_name,
        z.zone_name,
        ds.service_date,
        ds.time_from,
        ds.time_to,
        ca.customer_address,
        du.unconfirmed_schedule_id
       
    ')
        ->from('driver_confirmed_schedules as dc')
        ->join('day_services as ds', 'dc.day_service_id = ds.day_service_id', 'left')
        ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
        ->join('customers as c', 'dc.customer_id = c.customer_id', 'left')
        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
        ->join('locations as l', 'ca.location_id = l.location_id', 'left')
        ->join('areas as a', 'a.area_id = l.area_id', 'left')
        ->join('zones as z', 'a.zone_id = z.zone_id', 'left')
        ->join('maids as m', 'm.maid_id = dc.maid_id', 'left')
        ->join('maids as dr', 'dr.maid_id = dc.driver_id', 'left')
        ->join('driver_unconfirmed_schedules as du', 'dc.unconfirmed_schedule_id = du.unconfirmed_schedule_id', 'left')
        ->where('dc.service_date', $service_date)
        ->where('dc.deleted_at', null)
        // ->where('ds.is_driver_confirmed', 1)
        ->where('du.is_driver_unconfirmed', 1)
        ->where('ds.dispatch_status', 2)
        // ->order_by('m.maid_id', "ASC")
        ->order_by('dc.pick_time', "ASC");
        // ->order_by('dc.confirmed_schedule_id', "ASC");
        // filtering
        if ($this->input->get('driver_id')) {
            $this->db->where('dc.driver_id', $this->input->get('driver_id'));
        }
        if ($this->input->get('zone_id')) {
            $this->db->where('z.zone_id', $this->input->get('zone_id'));
        }
        if ($this->input->get('maid_id')) {
            $this->db->where('m.maid_id', $this->input->get('maid_id'));
        }
        $day_services = $this->db->get();
        $response['data'] = $day_services->result();
        // print_r($response['data']);die();
        // foreach ($response['data'] as $key => $schedule) {
        //     if ($schedule->unconfirmed_schedule_id != "")
        //     {
        //         $response['schedules'][$key]->pick_address = $schedule->pick_address;
        //         $response['schedules'][$key]->pick_time = $schedule->pick_time;
        //         $response['schedules'][$key]->drop_time = $schedule->drop_time;
        //         // $response['schedules'][$key]->drop_time = $schedule->drop_time;
                
        //     }
        // }
        //$this->db->flush_cache();
        /**************************************** */
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function delete_driver_availability()
    {
    //    log_message('error','post delete availability'.json_encode($this->input->post()));
        $driver_avalability_id = $this->input->post('driver_availability_id');
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $this->driver_model->delete_driver_availability($data, $driver_avalability_id);
        $respose['status'] = true;
        $respose['message'] = "Driver avalability Deleted successfully.";
        echo json_encode($respose, JSON_PRETTY_PRINT);
    }
    public function delete_multiple_driver_availability()
    {
        header('Content-Type: application/json; charset=utf-8');
        $driver_availability_ids = $this->input->post('driver_availability_ids') ?: null;
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $this->driver_model->delete_multiple_driver_availability($data, $driver_availability_ids);
        $respose['status'] = true;
        $respose['message'] = "Driver avalability Deleted successfully.";
        echo json_encode($respose, JSON_PRETTY_PRINT);

    }

    public function confirm_mapping()
    {
        header('Content-Type: application/json; charset=utf-8');
        $schedules = isset($_POST['schedules']) ? $_POST['schedules'] : [];
        $service_date = isset($_POST['service_date']) ? $_POST['service_date'] : [];
        $this->db->trans_begin();
        // print_r($schedules);die();
        foreach ($schedules as $schedule) {
            // if (empty($fields['driver_name']) || empty($fields['pick_up_time']) || empty($fields['pick_address'])) {
            //     $response['status'] = false;
            //     $response['message'] = "Missing required fields for day_service_id: " . $day_service_id;
            //     echo json_encode($response, JSON_PRETTY_PRINT);
            //     $this->db->trans_rollback(); 
            //     return;
            // }
            $pick_up_time_24hr = $this->convertTo24HourFormat(trim($schedule['pick_up_time']));
            $drop_time_24hr = $this->convertTo24HourFormat(trim($schedule['drop_time']));
            $unconfirmed_schedule_id = $schedule['unconfirmed_schedule_id'];
            if (empty($unconfirmed_schedule_id)) {
                $insertData1 = [
                    'day_service_id' => $schedule['schedule_id'],  // Unique ID for the schedule
                    'driver_id' => $schedule['driver_id'],  
                    'pick_time' => $pick_up_time_24hr,  
                    'drop_time' => $drop_time_24hr,  
                    'drop_address' => $schedule['drop_address'],  
                    'pick_address' => $schedule['pick_address'], 
                ];
    
                // if (empty($fields['unconfirmed_schedule_id'])) {
                   
                    $this->db->insert('driver_unconfirmed_schedules', $insertData1);
                    $id1 = $this->db->insert_id();
                    // print_r($id);
                // }
            }
            // if (empty($day_service_id)) {
            //     $response['status'] = false;
            //     $response['message'] = "Missing day_service_id for insertion.";
            //     echo json_encode($response, JSON_PRETTY_PRINT);
            //     $this->db->trans_rollback(); 
            //     return;
            // }
            // $data1['driver_id'] = $fields['driver_name'];
            // $this->db->where('day_service_id', $day_service_id);
            // $this->db->where('dispatch_status', 2);
            // $this->db->update('day_services', $data1);
            // Prepare the data for insertion
            $row_data = array();
            $unconfirmed_id = $schedule['unconfirmed_schedule_id'] ? : $id1;
            $update = $this->driver_model->confirm_driver_dispatch($schedule['schedule_id'], $row_data);
            $update1 = $this->driver_model->unconfirm_driver_dispatch($unconfirmed_id, $row_data);
            // print_r($schedule);die();
           
            $insertData = [
                'day_service_id' => $schedule['schedule_id'],  // Unique ID for the schedule
                'unconfirmed_schedule_id' => $unconfirmed_id,
                'driver_id' => $schedule['driver_id'],  
                'pick_time' => $pick_up_time_24hr,  
                'drop_time' => $drop_time_24hr,  
                'drop_address' => $schedule['drop_address'],  
                'pick_address' => $schedule['pick_address'],  
                'service_date' => $service_date,  
                'customer_id' => $schedule['customer_id'], 
                'maid_id' => $schedule['maid_id'],

            ];
            $this->db->insert('driver_confirmed_schedules', $insertData);
            $id = $this->db->insert_id();
            // print_r($id);die();
            $this->db->trans_commit();
            $response['status'] = "success";
            $response['message'] = "Driver unconfirmed schedules inserted successfully.";
        }

        echo json_encode($response, JSON_PRETTY_PRINT);
    }
    public function delete_unonfirmed_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        $schedules = isset($_POST['schedules']) ? $_POST['schedules'] : [];
        $service_date = isset($_POST['service_date']) ? $_POST['service_date'] : [];
        $this->db->trans_begin();
        // print_r($schedules);die();
        foreach ($schedules as $schedule) {
            // $row_data = array();
            // $update = $this->driver_model->confirm_driver_dispatch($schedule['schedule_id'], $row_data);
           
            // $this->db->insert('driver_confirmed_schedules', $insertData);
            // print_r($schedule);die();
            if (empty($schedule['unconfirmed_schedule_id'])) {
                $insertData = [
                'day_service_id' => $schedule['schedule_id'],  // Unique ID for the schedule
                'driver_id' => $schedule['driver_id'] ? $schedule['driver_id']: null,  
                'pick_time' => $schedule['pick_up_time'] ? $schedule['pick_up_time'] : null,  
                'drop_time' => $schedule['drop_time'] ? $schedule['drop_time'] : null,  
                'drop_address' => $schedule['drop_address'] ? $schedule['drop_address'] : null,  
                'pick_address' => $schedule['pick_address'] ? $schedule['pick_address'] : null,  
                'deleted_at' => date('Y-m-d H:i:s')  
            ];
            // print_r($insertData);die();
                $this->db->insert('driver_unconfirmed_schedules', $insertData);
            } else {
                $insertData = [ 
                    'deleted_at' => date('Y-m-d H:i:s') 
                ];
                $this->db->where('unconfirmed_schedule_id', $schedule['unconfirmed_schedule_id']);
                $this->db->update('driver_unconfirmed_schedules', $insertData);
            }
            // $id = $this->db->insert_id();
            // print_r($id);die();
            $this->db->trans_commit();
            $response['status'] = "success";
            $response['message'] = "Driver unconfirmed schedules deleted successfully.";
        }

        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function reset_unconfirmed_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        $day_service_ids = isset($_POST['day_service_ids']) ? $_POST['day_service_ids'] : [];
        // $service_date = isset($_POST['service_date']) ? $_POST['service_date'] : [];
        $this->db->trans_begin();
       
        $schedulesData = isset($_POST['schedulesData']) ? $_POST['schedulesData'] : [];
        
        // print_r($schedulesData);die();
        foreach ($schedulesData as $schedule) {
            if (!empty($schedule['unconfirmed_schedule_id'])) {
                // Proceed with deleting the record if the ID is not empty
                $this->db->where('unconfirmed_schedule_id', $schedule['unconfirmed_schedule_id']);
                $this->db->delete('driver_unconfirmed_schedules');
            }

        }
        foreach ($day_service_ids as $key => $day_service_id) {
            $row_data = array();
            $update = $this->dispatch_model->undo_confirmed_dispatch_service_by_id($day_service_id, $row_data);
        }
        $this->db->trans_commit();
        $respose['status'] = true;
        $respose['message'] = "Dispatched bookings reset successfully.";
        $respose['data'] = $rows_data;
        die(json_encode($respose, JSON_PRETTY_PRINT));
      

        echo json_encode($response, JSON_PRETTY_PRINT);
    }
}
