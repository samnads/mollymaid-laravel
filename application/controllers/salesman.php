<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Salesman extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if(!user_permission(user_authenticate(), 3))
        {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('settings_model');
        $this->load->library('upload');
        $this->load->helper('google_api_helper');
		$this->load->helper('curl_helper');
        $this->load->model('salesman_model');
    }

    public function index() {

        //echo 'God is love';

        if($this->input->post('salesman_sub'))
        {
            //echo '<pre>';print_r($_POST);exit;
            $salesman_name = $this->input->post('salesman_name');
            $description = $this->input->post('description');

            $data = array(
                'salesman_name' =>  $salesman_name,
                'description' => $description,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
             
            );
            $this->salesman_model->add_salesman($data);
        }
        else if($this->input->post('salesman_edit'))
        {
            $salesman_name = $this->input->post('edit_salesmanname'); 
            $salesman_id = $this->input->post('edit_salesmanid'); 
            $description = $this->input->post('edit_description');
            
            $data = array(
                'salesman_name' =>  $salesman_name,
                'description' => $description,
                'updated_at' => date('Y-m-d H:i:s'),
             
            );
            $this->salesman_model->update_salesman($data,$salesman_id);
        }

        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['salesman'] = $this->salesman_model->get_salesman();
        $layout_data['content_body'] = $this->load->view('salesman_list', $data, TRUE);
        $layout_data['page_title'] = 'Salesman List';
        $layout_data['meta_description'] = 'salesman';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();  
        $layout_data['js_files'] = array('mymaids.js','bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function edit_salesman()
    {
        $salesman_id = $this->input->post('salesman_id');
        $result = $this->salesman_model->get_salesman_details($salesman_id);
        echo json_encode($result);
    }

    public function remove_salesman()
    {
        $salesman_id = $this->input->post('salesman_id');
        $data = $this->salesman_model->delete_salesman(array('deleted_at' =>date('Y-m-d H:i:s')),$salesman_id);

    }


}