<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Schedule_suspended extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 2)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('zones_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('driver_model');
        $this->load->model('tablets_model');
        $this->load->model('settings_model');
        $this->load->model('justmop_model');
        $this->load->helper('google_api_helper');
        $this->load->model('employee_model');
        $this->load->model('schedule_model');
        $this->load->helper('time_helper');
        $this->load->helper('booking_helper');
    }

    public function maid_availability()
    {
 
        // log_message('error','post save booking'.json_encode($this->input->post()));
        header('Content-Type: application/json; charset=utf-8');
        $data['booking_id'] = $this->input->post('booking_id');
    
        $data['time_to'] = $this->input->post('time_to');
        $data['time_from'] = $this->input->post('time_from');
        $data['maid_id'] = $this->input->post('maid_id');
        $data['maid_name'] = $this->input->post('maid_name');
        $delete_date_range = $this->input->post('delete_date_range');
        $dates = explode(' - ', $delete_date_range);
        $startDate_range = '';
        $endDate_range = '';
        if (count($dates) === 2) {
            $startDate_range = trim($dates[0]);
            $endDate_range = trim($dates[1]);
        } 
        // $data['delete_from_date'] = $this->input->post('delete_from_date');
        $data['delete_from_date'] = $startDate_range;
        $data['delete_to_date'] = $endDate_range;
        // $data['delete_to_date'] = $this->input->post('delete_to_date');
        // $data['settings'] = $this->settings_model->get_settings();
        //var_dump($data['week_days']);die();
        $data['redirect_url'] = base_url() . 'schedule_suspended/maid_reassign';
        echo json_encode($data);
        // print_r($booking_id);die();
    }

    public function maid_reassign()
    {
        
       
        $data['booking_id'] = $this->input->get('booking_id');
        $data['delete_from_date'] = $this->input->get('delete_from_date');
        $data['delete_to_date'] = $this->input->get('delete_to_date');
        $data['time_to'] = $this->input->get('time_to');
        $data['time_from'] = $this->input->get('time_from');
        $data['maid_id'] = $this->input->get('maid_id');
        $data['maid_name'] = $this->input->get('maid_name');
        // var_dump($data);die();
        $data['settings'] = $this->settings_model->get_settings();
        $from_time = DateTime::createFromFormat('H:i:s', "06:30:00");
        $to_time = DateTime::createFromFormat('H:i:s', "19:30:00");
        $i = 0;
        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
            $time_clone = clone $time;
            $i++;
            $data['time_slots'][$time->format('H:i:s')] = $time->format('h:i A');
        }
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('schedule/reassign_maid_availabilty', $data, true);
        $layout_data['page_title'] = 'Suspended Schedule';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.maid-reassign.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function search_available_maid()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        $booking_id = trim($post['booking_id']);
        $booking = $this->bookings_model->get_booking_by_id($booking_id);
        $service_dates = $post['date_from'];
        $time_from = $post['time_from'];
        $time_to = $post['time_to'];
        $service_date_new =  DateTime::createFromFormat('d/m/Y', $service_dates)->format('Y-m-d');
        $service_end_date =  DateTime::createFromFormat('d/m/Y',$post['date_to'])->format('Y-m-d');
        $search_maids = $this->maids_model->get_all_maid_new();
        $time_from_stamp = strtotime(trim($time_from));
        // $time_from_stamp = strtotime(trim($booking->time_from));
        // $time_to_stamp = strtotime(trim($booking->time_to));
        $time_to_stamp = strtotime(trim($time_to));
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date_we($service_date_new, $service_end_date);
        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }
        $maid_array = array();
        $nf_maids = array();
        foreach ($search_maids as $s_maids) {
            $today_week_day = date('w', strtotime($service_date_new));
            $repeat_days = $today_week_day;
            $repeat_end = $booking->service_end;
            if ($booking->booking_type == 'OD') {
                

                $today_bookingss = $this->bookings_model->get_schedule_by_date_avail_new($service_date_new, $s_maids->maid_id);

                foreach ($today_bookingss as $t_booking) {
                    $booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);

                }

                //$maids_s = $this->maids_model->get_maids();

                //foreach($maids_s as $smaid)
                //{
                if (isset($booked_slots[$s_maids->maid_id]['time'])) {
                    foreach ($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time) {
                        if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                            //array_push($nf_maids, $smaid->maid_id);
                            array_push($nf_maids, $s_maids->maid_id);
                        }
                    }
                }
                
            }

            
            if ($booking->booking_type == 'WE') {
               
                if ($repeat_days < $today_week_day) {
                    $day_diff = (6 - $today_week_day + $repeat_days + 1);
                } else {
                    $day_diff = $repeat_days - $today_week_day;
                }
                $service_start_date = date('Y-m-d', strtotime($service_date_new . ' + ' . $day_diff . ' day'));

                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_forfree($s_maids->maid_id, $repeat_days, $service_date_new);
                // echo '<pre>';
                // print_r($bookings_on_day);
                // exit();

                if (!empty($bookings_on_day)) {
                    foreach ($bookings_on_day as $booking_on_day) {
                        if ($booking_on_day->booking_type == "OD") {
                            if ($booking_on_day->service_start_date == $service_date_new) {
                                $f_time = strtotime($booking_on_day->time_from);
                                $t_time = strtotime($booking_on_day->time_to);
                                if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                    //array_push($nf_maids, $smaid->maid_id);
                                    array_push($nf_maids, $s_maids->maid_id);
                                }
                            }
                        } else {
                            // if($service_end_date == "")
                            // {
                            // $service_end_date = $service_start_date;
                            // } else {
                            // $service_end_date = $service_end_date;
                            // }

                            $s_date_stamp = strtotime($service_start_date);
                            $e_date_stamp = strtotime($service_end_date);
                            $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                            $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                            if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 0) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 0) || ($repeat_end == 1 && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                            //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                            {
                                $f_time = strtotime($booking_on_day->time_from);
                                $t_time = strtotime($booking_on_day->time_to);
                                if (($time_from_stamp >= $f_time && $time_to_stamp <= $t_time)) 
                                // if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) //|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                                {
                                    array_push($nf_maids, $s_maids->maid_id);
                                } else {

                                }
                            }
                        }
                    }
                } else {

                }
                //}
            }

        }
        $all_maid_list = array();
        $all_maids = $this->maids_model->get_maids();
        foreach ($all_maids as $maid) {
            array_push($all_maid_list, $maid->maid_id);
        }
        $free_maids = array_diff($all_maid_list, $nf_maids);
        if (!empty($free_maids)) {
            //$f_maids = array_diff($free_maids, $not_free_maids);
            $free_maid_dtls = array();
            $free_maid_ids = array();
            foreach ($free_maids as $f_maid) {
                $maid = $this->maids_model->get_maid_by_id($f_maid);
                array_push($free_maid_dtls, $maid);
                array_push($free_maid_ids, $f_maid);
            }
            
            // echo json_encode($free_maid_dtls);
            // echo '<pre>';
            // print_r($free_maid_dtls);die();
            // exit();
            $data['settings'] = $this->settings_model->get_settings();
            $data['free_maid_dtls'] = $free_maid_dtls;
            $data['booking_type'] = $booking->booking_type;
            $data['service_date'] = $service_date_new;
            $data['service_end_date'] = $service_end_date;
            $data['leave_maid_ids'] = $leave_maid_ids;  
            $maids = $this->load->view('schedule/maid-availability', $data, TRUE);
            echo $maids;
            exit();

        } 
        else {
            $data = array();
            $data['status'] = 'error';
            $data['message'] = 'There are no maids available for the selected time slot';

            $maids = $this->load->view('schedule/maid-availability', $data, TRUE);
            echo $maids;
            exit();
        }

    }

    public function re_assign_schedule()
    { 
        try {
            header('Content-Type: application/json; charset=utf-8');
            $post = $this->input->post();
            $booking_id1 = $post['booking_id'];
            $delete_date_from = $post['delete_date_from'];
            $delete_date_to = $post['delete_date_to'];
            $time_from = $post['time_from'];
            $time_to = $post['time_to'];
            $maid_id = $post['maid_id'];
            $maid_name = $post['maid_name'];
            $new_maid_name = $post['new_maid_name'];
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id1)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $booking = $query->row();
            //   print_r($booking);die();
            $is_maidflagged = $this->schedule_model->is_maidflagged($maid_id, $booking->customer_id);

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $this->db->trans_begin();
            $time_from = strtotime($time_from);
            $time_to = strtotime($time_to);
            $working_minutes = ($time_to - $time_from) / 60;
            $working_hours = $working_minutes / 60;
            $booking_data['customer_id'] = $booking->customer_id;
            // $customer = $this->db->select("c.default_booking_address")
            //     ->from('customers as c')
            //     ->where('c.customer_id', $booking_data['customer_id'])->get()->row();

            // if ($customer->default_booking_address == "") {
            //     $response['status'] = "failed";
            //     $response['message'] = "Please fill the customer address";
            //     die(json_encode($response, JSON_PRETTY_PRINT));
            // }

            $service_start_date = $post['delete_date_from'] ? DateTime::createFromFormat('d/m/Y', $post['delete_date_from'])->format('Y-m-d') : null;
            $service_end_date = $delete_date_to ? DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d') : null;
        
            if ($booking->booking_type == "OD") {
                $booking_data['service_start_date'] = $service_start_date;
                $service_week_day = $booking->service_week_day;
                $booking_data['booking_type'] = "OD";
                $booking_data['service_end_date'] = $service_end_date;
                $booking_data['service_actual_end_date'] = $service_end_date;
                $booking_data['service_end'] = 1;
             
            } else {
                
                $booking_data['booking_type'] = "WE";
                // $service_start_date = $service_start_date ? DateTime::createFromFormat('d/m/Y', $service_start_date)->format('Y-m-d') : null;
                // $service_end_date = $service_end_date ? DateTime::createFromFormat('d/m/Y', $service_end_date)->format('Y-m-d') : null;
                $booking_data['service_start_date'] = $service_start_date;
                $booking_data['service_end_date'] = $service_end_date;
                
                $service_week_day = $booking->service_week_day;
               
                $booking_data['service_end'] = 1;
               
                $booking_data['service_actual_end_date'] = $service_end_date;
              
            }

            $booking_data['customer_address_id'] = $booking->customer_address_id;
            $booking_data['maid_id'] = $maid_id;
            $booking_data['service_type_id'] = $booking->service_type_id;;
            $booking_data['time_from'] = $post['time_from'];
            $booking_data['time_to'] = $post['time_to'];
            $booking_data['price_per_hr'] = $booking->price_per_hr;
            $booking_data['discount_price_per_hr'] = $booking->discount_price_per_hr;
            $booking_data['discount'] = $booking->discount;
            $booking_data['vat_charge'] = $booking->vat_charge;
            $booking_data['net_service_charge'] = $booking->net_service_charge;
            $booking_data['service_charge'] = $booking->service_charge;
            $booking_data['net_service_cost'] = $booking->net_service_cost;
            $booking_data['service_week_day'] = $service_week_day;
            $booking_data['total_amount'] = $booking->total_amount;
            $booking_data['booking_status'] = 1;
            $booking_data['booked_datetime'] = date('Y-m-d H:i:s');
            /***************************************************************** */
            // if ($this->input->post('cleaning_materials') == "Y") {
            $booking_data['cleaning_material'] = $booking->cleaning_material;
            $booking_data['cleaning_material_fee'] = $booking->cleaning_material_fee;
            $booking_data['booking_category'] = $booking->booking_category;

            $booking_data['_service_hours'] = $working_hours;
            $booking_data['_cleaning_material_rate_per_hour'] = $booking->_cleaning_material_rate_per_hour;
            $booking_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
            $booking_data['_service_discount_rate_per_hour'] = $booking->_service_rate_per_hour;
            $booking_data['_service_amount'] = $booking->_service_amount;
            $booking_data['_service_discount'] = $booking->_service_discount;
            $booking_data['_net_service_amount'] = $booking->_net_service_amount;
            // if ($this->input->post('cleaning_materials') == "Y") {
            $booking_data['_cleaning_material'] = $booking->_cleaning_material;
            $booking_data['_net_cleaning_material_amount'] = $booking->_net_cleaning_material_amount;

            $booking_data['_total_discount'] =  $booking->_total_discount;
            $booking_data['_taxable_amount'] = $booking->_taxable_amount;
            $booking_data['_vat_percentage'] = $booking->_vat_percentage;
            $booking_data['_vat_amount'] =  $booking->_vat_amount;
            $booking_data['_total_amount'] = $booking->_total_amount;
            $booking_data['created_at'] = date('Y-m-d H:i:s');
            $booking_data['updated_at'] = date('Y-m-d H:i:s');
            $booking_data['parent_booking_id'] = $booking_id1;
            $booking_data['booking_reassign_reason'] = $post['reason'];
            $booking_data['booking_reassign_status'] = 'Reassign';
            $booking_data['parent_maid_name'] = $maid_name;
            
            // $booking_data['reference_id'] =  $booking->reference_id;
            // echo '<pre>';
            // print_r($booking_data);die();
            $booking1 = new stdClass();
            $booking1->time_from = $post['time_from'];
            $booking1->time_to =  $post['time_to'];
            $booking1->service_start_date = $booking_data['service_start_date'];
            $booking1->service_end = $booking_data['service_end'];
            $booking1->service_actual_end_date = $booking_data['service_actual_end_date'];
            $booking1->service_week_day = $booking_data['service_week_day'];
            $booking1->maid_id = $maid_id;
            // print_r($booking1);
            $timed_bookings = get_same_timed_bookings($booking1, null);
            $booking_ids = array_column($timed_bookings,'booking_id');
            $deleted_booking_data = get_deleted_booking($booking_data);
          
            $book_id = array_column($deleted_booking_data,'day_service_booking_id');
            $result = array_diff($booking_ids,$book_id);
            if(sizeof($result) > 0)
            {
                $busy_maid_ids = array_column($timed_bookings,'maid_id');
                //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                throw new Exception('Selected time slot not available.', 100);
                }
            }  
            $day_services = new stdClass();
            $day_services->time_from = $booking_data['time_from'];
            $day_services->time_to =  $booking_data['time_to'];
            $day_services->service_date = $booking_data['service_start_date'];
            $day_services->maid_id = $maid_id;
            
            $deleted_booking_data = get_deleted_booking($booking_data);
            $day_service_id = array_column($deleted_booking_data,'day_service_id');
            $timed_day_service = get_same_timed_day_service($day_service, null);
                // print_r($timed_day_service);die();
            $day_service_ids = array_column($timed_day_service,'day_service_id');
            $resultarray = array_diff($day_service_ids,$day_service_id);
            if(sizeof($resultarray) > 0)
            {
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                if (in_array($day_service->maid_id, $busy_maid_id)) {
                    throw new Exception('Selected time slot not available.', 100);
                }
            } else {
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                if (in_array($day_service->maid_id, $busy_maid_id)) {
                    throw new Exception('Selected time slot not available.', 100);
                }

            }
                
            // $timed_bookings = get_same_timed_bookings($booking1, null);
            // print_r($timed_bookings);die();
            $booking_ids = array_column($timed_bookings,'booking_id');
            // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
            // $result = array_diff($book_id,$booking_ids);
            if(sizeof($booking_ids) > 0)
            {
                $busy_maid_ids = array_column($timed_bookings,'maid_id');

                if (in_array($booking1->maid_id, $busy_maid_ids)) {
                    throw new Exception('Selected time slot not available.', 100);
                }
            }  
            $this->db->insert('bookings', $booking_data);
            $booking_id = $this->db->insert_id();
            $bookin_delete['reassign_status'] = 'Reassign';
            $bookin_delete['new_booking_id'] = $booking_id;
            $bookin_delete['new_parent_maid_name'] = $new_maid_name;
            $bookin_delete['reason'] = $post['reason'];
            $this->db->where(array('booking_id' => $booking_id1));
            $this->db->update('booking_deletes', $bookin_delete);
            if ($booking_id > 0) {
                $response['status'] = "success";
                $response['message'] = "Schedule created successfully.";
                $booking_data['reference_id'] = "MM-" . $booking_data['booking_type'] . "-" . sprintf("%06d", $booking_id);
                $this->db->set($booking_data)
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                $this->db->trans_commit();
        
            } else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
              
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function maid_leave()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        
        $delete_from_date = $post['delete_from_date'];
        $delete_to_date = $post['delete_to_date'];
        $maid_id = $post['maid_id'];
        $dateRange = $post['delete_date_range'];
        $dates = explode(' - ', $dateRange);
        $startDate = '';
        $endDate = '';
        if (count($dates) === 2) {
            $startDate = trim($dates[0]);
            $endDate = trim($dates[1]);
        } 
        $data['delete_to_date'] =  $delete_to_date;
        $data['delete_from_date'] =  $delete_from_date;
        $data['maid_id'] = $post['maid_id'];
        $maid_leave_data = $this->maids_model->get_maid_leave_suspend($maid_id, $startDate, $delete_to_date);
        $data['maid_leave_data'] = $maid_leave_data;
        //  print_r($maid_leave_data);
        echo json_encode($data);
    }
    public function maid_leave_extend()
    { 
       
        try {
            header('Content-Type: application/json; charset=utf-8');
            $post = $this->input->post();           
            $maid_id     =    $post['maid_id'];
            $start_date = str_replace('/', '-', $post['delete_date_from']);
            $end_date   = str_replace('/', '-', $post['delete_date_to']);
            $start_date  =    date('Y-m-d', strtotime($start_date));
            $end_date    =   date('Y-m-d', strtotime($end_date));
            $booking_id = $post['booking_id'];
            $booking_type = $post['booking_type']; 
            // $delete_from_date1 = $post['delete_from_date1']; 
            // $delete_to_date1 = $post['delete_to_date1']; 
            $delete_from_date1 = str_replace('/', '-', $post['delete_from_date1']);
            $delete_to_date1   = str_replace('/', '-', $post['delete_to_date1']);
            $delete_from_date1  =    date('Y-m-d', strtotime($delete_from_date1));
            $delete_to_date1    =   date('Y-m-d', strtotime($delete_to_date1));
            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);
            $end->modify('+1 day'); // To include the end date in the loop
            
            $leave_days = array();
            $leave_done = array();
            
            // Fetch existing leave records for the maid within the date range
            $existing_leaves = $this->maids_model->get_existing_leaves($maid_id, $start_date, $end_date);
            $existing_leave_dates = array();
            if ($booking_type == "WE") {
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                ->from('bookings as b')
                ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                ->where('b.booking_id', $booking_id)
                ->where('b.booking_status', 1);
                $query = $this->db->get();
                $bookings = $query->row();
                $this->db->select('bd.*')
                ->from('booking_deletes as bd');
                $this->db->where("DATE(bd.delete_from_date) BETWEEN '$delete_date_from1' AND '$delete_to_date1'"); 
                $this->db->where("bd.booking_id", $booking_id);
                $get_booking_deletes_qry = $this->db->get()->result();
                $booking_delete_ids = array();
                $booking_delete_from_date = array();
                $booking_delete_to_date = array();
                $existing_booking_delete_from_date  = array();
                foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                    $existing_booking_delete_from_date[] = date('Y-m-d', strtotime($get_booking_data->service_date));
                    $booking_delete_range = $get_booking_data->delete_date_range;
                    // $booking_delete_from_date[] = $get_booking_data->delete_from_date;
                    // $booking_delete_to_date[] = $get_booking_data->delete_to_date;

                }
                $dates = explode(' - ', $booking_delete_range);
                $startDate_range = '';
                $endDate_range = '';
                if (count($dates) === 2) {
                    $startDate_range = trim($dates[0]);
                    $endDate_range = trim($dates[1]);
                } 
                $start1 = strtotime($delete_from_date1);
                $weekday = $bookings->service_week_day;
                $end1 = strtotime($end_date);
                $weekdays = array();
                while (date("w", $start1) != $weekday) {
                    $start1 += 86400;
                }
                while ($start1 <= $end1)
                {
                    $weekdays[] = date('Y-m-d', $start1);
                    $start1 += 604800;
                }
                $new_dates = array();
                $existing_dates = array();

                foreach ($weekdays as $weekday_date) {
                    if (in_array($weekday_date, $existing_booking_delete_from_date)) {
                        $existing_dates[] = $weekday_date;
                    } else {
                        $new_dates[] = $weekday_date;
                    }
                }

                $booking_deleted_data = [];
                $delete_from_date =  current($new_dates);
                $delete_to_date =  end($new_dates);
                
                foreach ($get_booking_deletes_qry as $get_booking_data) {
                    foreach ($new_dates as $date) {
                        $booking_deleted_data[] = [
                            'booking_id' => $get_booking_data->booking_id,
                            'day_service_id' => $get_booking_data->day_service_id,
                            'service_date' => $date,
                            'delete_from_date' => $delete_from_date,
                            'delete_to_date' => $delete_to_date,
                            'remarks' => $get_booking_data->remarks,
                            'delete_date_range' => $startDate_range .' - '. $delete_to_date,
                            'added_datetime' => date('Y-m-d H:i:s'),
                            'deleted_by' => user_authenticate(),
                            'delete_booking_type' => $get_booking_data->delete_booking_type,
                            'delete_status' => $get_booking_data->delete_status,
                            'cancel_reason_id' => $get_booking_data->cancel_reason_id ?: null,
                        ];
                    }
                }
                $booking_deleted_data = array_unique($booking_deleted_data, SORT_REGULAR);
                // print_r($booking_deleted_data);die();
                
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
              
                foreach ($existing_leaves as $leave) {
                    $existing_leave_dates[] = $leave->leave_date;
                }
                for ($i = $begin; $i < $end; $i->modify('+1 day')) {
                    $current_date = $i->format("Y-m-d");
                    
                    $maid_fields = array();
                    $maid_fields['maid_id'] = $maid_id;
                    $maid_fields['leave_date'] = $current_date;
                    $maid_fields['leave_status'] = 1;  
                    $maid_fields['added_by'] = user_authenticate();
                    $maid_fields['leave_type'] = $post['leave_type'];
                    $maid_fields['typeleaves'] = $post['leave_type_new'];
                    
                    if (!in_array($current_date, $existing_leave_dates)) {
                        // If the leave date does not exist, add new record
                        $leave_id = $this->maids_model->add_maid_leave($maid_fields); 
                        array_push($leave_done, $leave_id);
                    } else {
                        // If the leave date exists, update the existing record
                        $this->maids_model->update_maid_leave_suspend($maid_id, $current_date, $maid_fields);
                    }
                }

            } else {
                foreach ($existing_leaves as $leave) {
                    $existing_leave_dates[] = $leave->leave_date;
                }
                for ($i = $begin; $i < $end; $i->modify('+1 day')) {
                    $current_date = $i->format("Y-m-d");
                    
                    $maid_fields = array();
                    $maid_fields['maid_id'] = $maid_id;
                    $maid_fields['leave_date'] = $current_date;
                    $maid_fields['leave_status'] = 1;  
                    $maid_fields['added_by'] = user_authenticate();
                    $maid_fields['leave_type'] = $post['leave_type'];
                    $maid_fields['typeleaves'] = $post['leave_type_new'];
                    
                    if (!in_array($current_date, $existing_leave_dates)) {
                        // If the leave date does not exist, add new record
                        $leave_id = $this->maids_model->add_maid_leave($maid_fields); 
                        array_push($leave_done, $leave_id);
                    } else {
                        // If the leave date exists, update the existing record
                        $this->maids_model->update_maid_leave_suspend($maid_id, $current_date, $maid_fields);
                    }
                }
            }
            
            $this->db->trans_commit();
            $response['status'] = "success";
            $response['message'] = "Leave saved successfully.";
            die(json_encode($response, JSON_PRETTY_PRINT));
           
        } catch (Exception $e) {
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }

    }
    public function extend_suspend_date_range_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('delete_date_from');
            $delete_date_from = $this->input->post('delete_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('delete_date_from'))->format('Y-m-d') : null;
            $delete_date_to = $this->input->post('delete_date_to') ? DateTime::createFromFormat('d/m/Y', $this->input->post('delete_date_to'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $delete_date_from)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $delete_date_from);
                $query = $this->db->get();
                $day_service = $query->row();
                $this->db->select('bd.*')
                ->from('booking_deletes as bd');
                $this->db->where("DATE(bd.delete_from_date) BETWEEN '$delete_date_from' AND '$delete_date_to'"); 
                $this->db->where("bd.booking_id", $booking_id);
                $query1 = $this->db->get();
                $get_booking_deletes_qry = $query1->row();
                if ($get_booking_deletes_qry) {
                    $response['status'] = "failed";
                    $response['message'] = "Schedule can't be extend.";
                    die(json_encode($response, JSON_PRETTY_PRINT)); 
                }
                //  print_r($get_booking_deletes_qry);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    
                    // set update data
                    $date_range = $delete_date_from .' - '. $delete_date_to;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $delete_date_from;
                    $booking_deleted_data['delete_to_date'] =  $delete_date_to;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_date';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%07s", $day_service_id))))
                    // ->where('day_service_id', $day_service_id)
                    // ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $delete_date_from .' - '. $delete_date_to;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $delete_date_from;
                    $booking_deleted_data['delete_to_date'] =  $delete_date_to;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_date';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function extend_suspend_date_range_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remark');
            $delete_date_from = $this->input->post('delete_date_from');
            
            $delete_date_from = DateTime::createFromFormat('d/m/Y', $delete_date_from)->format('Y-m-d');
           
            $delete_date_to = $this->input->post('delete_date_to');
            $delete_date_to = DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d');
    
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings->service_end == 1 && $delete_date_from > $bookings->service_end_date  )
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if($bookings->service_end == 1 && $delete_date_to > $bookings->service_end_date)
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_from) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_to) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            
            $start = strtotime($delete_date_from);
            $weekday = $bookings->service_week_day;
            $end = strtotime($delete_date_to);
            $weekdays = array();
            // $weekdays = array();
            while (date("w", $start) != $weekday) {
                $start += 86400;
            }
            while ($start <= $end)
            {
                $weekdays[] = date('Y-m-d', $start);
                $start += 604800;
            }
            if (empty($weekdays)) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            // print_r($weekdays);die();
            $day_service = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
            ->where_in('ds.service_date', $weekdays)->get()->result();
            // print_r($day_service);die();
            /***************************************************************** */
            
            $response['day_service'] = $day_service;
            if (sizeof($day_service) > 0){
                $delete_b_fields = [];
                $existing_service_date  = array();
                foreach ($day_service as $key => $dayservice) {
                    if ($daservice->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 3) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                   
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                    if (user_authenticate() != 1) {
                        if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                            echo 'locked';
                            exit();
                        }
                    }
                    $existing_service_date[] = date('Y-m-d', strtotime($dayservice->service_date));
                 
                  
             
                }
                $new_service_dates = array();
                $existing_service_dates = array();
                foreach ($weekdays as $weekday_date) {
                    if (in_array($weekday_date, $existing_service_date)) {
                        $existing_service_dates[] = $weekday_date;
                    } else {
                        $new_service_dates[] = $weekday_date;
                    }
                }

                $day_service_data = [];
                foreach ($new_service_dates as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $bookings->customer_id;
                    $day_service_data[$key]['maid_id'] = $bookings->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $bookings->time_from;
                    $day_service_data[$key]['time_to'] = $bookings->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $bookings->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $bookings->service_type_id;
                    $day_service_data[$key]['customer_name'] = $bookings->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $bookings->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $bookings->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $bookings->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $bookings->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $bookings->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $bookings->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
              

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

                // print_r($day_services);die();
                $this->db->select('bd.*')
                ->from('booking_deletes as bd');
                // $this->db->where("DATE(bd.delete_from_date) BETWEEN '$delete_date_from1' AND '$delete_to_date1'"); 
                $this->db->where("bd.booking_id", $booking_id);
                $get_booking_deletes_qry = $this->db->get()->result();
                // print_r($get_booking_deletes_qry);die();
                $existing_booking_delete_from_date  = array();
                $existing_booking_delete_ids  = array();
                foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                    $existing_booking_delete_from_date[] = date('Y-m-d', strtotime($get_booking_data->service_date));
                    $booking_delete_range = $get_booking_data->delete_date_range;
                    $existing_booking_delete_ids = $get_booking_data->booking_delete_id;

                }
                // print_r($existing_booking_delete_ids);die();
                $new_dates = array();
                $existing_dates = array();

                foreach ($weekdays as $weekday_date) {
                    if (in_array($weekday_date, $existing_booking_delete_from_date)) {
                        $existing_dates[] = $weekday_date;
                    } else {
                        $new_dates[] = $weekday_date;
                    }
                }
                $dates = explode(' - ', $booking_delete_range);
                $startDate_range = '';
                $endDate_range = '';
                if (count($dates) === 2) {
                    $startDate_range = trim($dates[0]);
                    $endDate_range = trim($dates[1]);
                } 
                $date_range = $delete_date_from .' - '. $delete_date_to ;
                $delete_from_date =  current($new_dates);
                $delete_to_date =  end($new_dates);
                foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                    $this->db->set(array('delete_to_date' => $delete_to_date, 'delete_date_range' => $startDate_range .' - '. $delete_to_date))
                    ->where_in('booking_delete_id', $get_booking_data->booking_delete_id)
                    ->update('booking_deletes');
                }
                foreach ($day_services as $key => $day_service) {
                   
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                    // ->where_in('day_service_id', $day_service->day_service_id)
                    // ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service->day_service_id))))
                    ->where('day_service_id', $day_service->day_service_id)
                    ->update('day_services');
                    // $this->db->set(array('delete_to_date' => $delete_to_date, 'delete_date_range' => $startDate_range .' - '. $delete_to_date))
                    // ->where_in('booking_delete_id', $existing_booking_delete_ids)
                    // ->update('booking_deletes');
                    $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                    $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                    $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
                    $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                    $booking_deleted_data[$key]['remarks'] = $remarks;
                    $booking_deleted_data[$key]['delete_date_range'] = $startDate_range .' - '. $delete_to_date;
                    $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                    $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data[$key]['delete_status'] = 'suspend_date';
                    $booking_deleted_data[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                // }
            }
            //  print_r($delete_b_fields);die();
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
           
            else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
           
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;

                $start = strtotime($delete_date_from);
                $weekday = $booking->service_week_day;
                // print_r($start);die();
                $end = strtotime($delete_date_to);
                $weekdays = array();
                while (date("w", $start) != $weekday) {
                    $start += 86400;
                }
                while ($start <= $end)
                {
                    $weekdays[] = date('Y-m-d', $start);
                    $start += 604800;  
                }
                if (empty($weekdays)) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                $day_service_data = [];
                foreach ($weekdays as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $booking->customer_id;
                    $day_service_data[$key]['maid_id'] = $booking->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $booking->time_from;
                    $day_service_data[$key]['time_to'] = $booking->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $booking->service_type_id;
                    $day_service_data[$key]['customer_name'] = $booking->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $booking->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
                // print_r($day_service_data[$key]['service_date']);die();

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

            $date_range = $delete_date_from .' - '. $delete_date_to ;
            $booking_deleted_data = [];
            $delete_from_date =  current($weekdays);
            $delete_to_date =  end($weekdays);
            $this->db->select('bd.*')
            ->from('booking_deletes as bd');
            $this->db->where("bd.booking_id", $booking_id);
            $get_booking_deletes_qry = $this->db->get()->result();
            // print_r($get_booking_deletes_qry);die();
            $existing_booking_delete_from_date  = array();
            $existing_booking_delete_ids = array();
            foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                $existing_booking_delete_from_date[] = date('Y-m-d', strtotime($get_booking_data->service_date));
                $booking_delete_range = $get_booking_data->delete_date_range;
                $existing_booking_delete_ids = $get_booking_data->booking_delete_id;
            }

            $dates = explode(' - ', $booking_delete_range);
            $startDate_range = '';
            $endDate_range = '';
            if (count($dates) === 2) {
                $startDate_range = trim($dates[0]);
                $endDate_range = trim($dates[1]);
            } 
            $new_dates = array();
            $existing_dates = array();

            foreach ($weekdays as $weekday_date) {
                if (in_array($weekday_date, $existing_booking_delete_from_date)) {
                    $existing_dates[] = $weekday_date;
                } else {
                    $new_dates[] = $weekday_date;
                }
            }
            // print_r($new_dates);die();
            $booking_deleted_data = [];
            $delete_from_date1 =  current($new_dates);
            $delete_to_date1 =  end($new_dates);
            if (sizeof($get_booking_deletes_qry) > 0){
                foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                    $this->db->set(array('delete_to_date' => $delete_to_date1, 'delete_date_range' => $startDate_range .' - '. $delete_to_date))
                    ->where_in('booking_delete_id', $get_booking_data->booking_delete_id)
                    ->update('booking_deletes');
                }
               
            foreach ($day_services as $key => $day_service) {
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date1;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date1;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $startDate_range .' - '. $delete_to_date;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';
                $booking_deleted_data[$key]['delete_status'] = 'suspend_date';
                $booking_deleted_data[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
            
            }
      
        }
        // else {
        //     foreach ($day_services as $key => $day_service) {
        //         // update reference id
        //         $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
        //         ->where_in('day_service_id', $day_service->day_service_id)
        //         ->update('day_services');
                
        //         $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
        //         $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
        //         $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
        //         $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
        //         $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
        //         $booking_deleted_data[$key]['remarks'] = $remarks;
        //         $booking_deleted_data[$key]['delete_date_range'] = $date_range;
        //         $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
        //         $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
        //         $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';
        //         $booking_deleted_data[$key]['delete_status'] = 'suspend_date';
        //         $booking_deleted_data[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
        //     }
        // }
        // print_r($booking_deleted_data);die();
        $this->db->insert_batch('booking_deletes', $booking_deleted_data);
        $booking_delete_id = $this->db->insert_id();   
        $response['status'] = "success";
        $response['message'] = "Schedule deleted successfully.";
    }
    $this->db->trans_commit();
    } catch (Exception $e) {
        $this->db->trans_rollback();
        $response['status'] = "failed";
        $response['message'] = $e->getMessage();
    }
    die(json_encode($response, JSON_PRETTY_PRINT));
    }

}