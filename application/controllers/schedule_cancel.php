<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Schedule_cancel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 2)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('zones_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('driver_model');
        $this->load->model('tablets_model');
        $this->load->model('settings_model');
        $this->load->model('justmop_model');
        $this->load->helper('google_api_helper');
        $this->load->model('employee_model');
        $this->load->model('schedule_model');
        $this->load->helper('time_helper');
        $this->load->helper('booking_helper');
    }

    public function maid_availability()
    {
 
        // log_message('error','post save booking'.json_encode($this->input->post()));
        header('Content-Type: application/json; charset=utf-8');
        $data['booking_id'] = $this->input->post('booking_id');
        $data['delete_from_date'] = $this->input->post('delete_from_date');
        $data['delete_to_date'] = $this->input->post('delete_to_date');
        $data['time_to'] = $this->input->post('time_to');
        $data['time_from'] = $this->input->post('time_from');
        $data['maid_id'] = $this->input->post('maid_id');
        $data['maid_name'] = $this->input->post('maid_name');
        // $data['settings'] = $this->settings_model->get_settings();
        //var_dump($data['week_days']);die();
        $data['redirect_url'] = base_url() . 'schedule_cancel/maid_reassign';
        echo json_encode($data);
        // print_r($booking_id);die();
    }

    public function maid_reassign()
    {
        
       
        $data['booking_id'] = $this->input->get('booking_id');
        $data['delete_from_date'] = $this->input->get('delete_from_date');
        $data['delete_to_date'] = $this->input->get('delete_to_date');
        $data['time_to'] = $this->input->get('time_to');
        $data['time_from'] = $this->input->get('time_from');
        $data['maid_id'] = $this->input->get('maid_id');
        $data['maid_name'] = $this->input->get('maid_name');
        // var_dump($data);die();
        $data['settings'] = $this->settings_model->get_settings();
        $from_time = DateTime::createFromFormat('H:i:s', "06:30:00");
        $to_time = DateTime::createFromFormat('H:i:s', "19:30:00");
        $i = 0;
        for ($time = $from_time; $time <= $to_time; $time = $from_time->modify('+30 minutes')) {
            $time_clone = clone $time;
            $i++;
            $data['time_slots'][$time->format('H:i:s')] = $time->format('h:i A');
        }
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('schedule/reassign_cancel_maid_availabilty', $data, true);
        $layout_data['page_title'] = 'Cancelled Schedule';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.maid-cancel-reassign.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function search_available_maid()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post();
        $booking_id = trim($post['booking_id']);
        $booking = $this->bookings_model->get_booking_by_id($booking_id);
        $service_dates = $post['date_from'];
        $service_date_new =  DateTime::createFromFormat('d/m/Y', $service_dates)->format('Y-m-d');
        $service_end_date =  DateTime::createFromFormat('d/m/Y',$post['date_to'])->format('Y-m-d');
        $search_maids = $this->maids_model->get_all_maid_new();
        $time_from_stamp = strtotime(trim($booking->time_from));
        $time_to_stamp = strtotime(trim($booking->time_to));
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date_we($service_date_new, $service_end_date);
        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }
        $maid_array = array();
        $nf_maids = array();
        foreach ($search_maids as $s_maids) {
            $today_week_day = date('w', strtotime($service_date_new));
            $repeat_days = $today_week_day;
            $repeat_end = $booking->service_end;
            if ($booking->booking_type == 'OD') {
                

                $today_bookingss = $this->bookings_model->get_schedule_by_date_avail_new_booking($service_date_new, $s_maids->maid_id);

                foreach ($today_bookingss as $t_booking) {
                    $booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);

                }

                //$maids_s = $this->maids_model->get_maids();

                //foreach($maids_s as $smaid)
                //{
                if (isset($booked_slots[$s_maids->maid_id]['time'])) {
                    foreach ($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time) {
                        if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                            //array_push($nf_maids, $smaid->maid_id);
                            array_push($nf_maids, $s_maids->maid_id);
                        }
                    }
                }
                
            }

            
            if ($booking->booking_type == 'WE') {
               
                if ($repeat_days < $today_week_day) {
                    $day_diff = (6 - $today_week_day + $repeat_days + 1);
                } else {
                    $day_diff = $repeat_days - $today_week_day;
                }
                $service_start_date = date('Y-m-d', strtotime($service_date_new . ' + ' . $day_diff . ' day'));

                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_forfree($s_maids->maid_id, $repeat_days, $service_date_new);
                // echo '<pre>';
                // print_r($bookings_on_day);
                // exit();

                if (!empty($bookings_on_day)) {
                    foreach ($bookings_on_day as $booking_on_day) {
                        if ($booking_on_day->booking_type == "OD") {
                            if ($booking_on_day->service_start_date == $service_date_new) {
                                $f_time = strtotime($booking_on_day->time_from);
                                $t_time = strtotime($booking_on_day->time_to);
                                if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) {
                                    //array_push($nf_maids, $smaid->maid_id);
                                    array_push($nf_maids, $s_maids->maid_id);
                                }
                            }
                        } else {
                            // if($service_end_date == "")
                            // {
                            // $service_end_date = $service_start_date;
                            // } else {
                            // $service_end_date = $service_end_date;
                            // }

                            $s_date_stamp = strtotime($service_start_date);
                            $e_date_stamp = strtotime($service_end_date);
                            $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                            $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                            if (($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 0) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 0) || ($repeat_end == 1 && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                            //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                            {
                                $f_time = strtotime($booking_on_day->time_from);
                                $t_time = strtotime($booking_on_day->time_to);

                                if (($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time)) //|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                                {
                                    array_push($nf_maids, $s_maids->maid_id);
                                } else {

                                }
                            }
                        }
                    }
                } else {

                }
                //}
            }

        }
        $all_maid_list = array();
        $all_maids = $this->maids_model->get_maids();
        foreach ($all_maids as $maid) {
            array_push($all_maid_list, $maid->maid_id);
        }
        $free_maids = array_diff($all_maid_list, $nf_maids);
        if (!empty($free_maids)) {
            //$f_maids = array_diff($free_maids, $not_free_maids);
            $free_maid_dtls = array();
            $free_maid_ids = array();
            foreach ($free_maids as $f_maid) {
                $maid = $this->maids_model->get_maid_by_id($f_maid);
                // $time=$this->bookings_model->get_shifts_maid($service_date_new,$f_maid);	
                array_push($free_maid_dtls, $maid);
                array_push($free_maid_ids, $f_maid);
            }
            
            // echo json_encode($free_maid_dtls);
            // echo '<pre>';
            // print_r($free_maid_dtls);die();
            // exit();
            $data['settings'] = $this->settings_model->get_settings();
            $data['free_maid_dtls'] = $free_maid_dtls;
            $data['booking_type'] = $booking->booking_type;
            $data['service_date'] = $service_date_new;
            $data['leave_maid_ids'] = $leave_maid_ids; 
            $maids = $this->load->view('schedule/maid-availability', $data, TRUE);
            echo $maids;
            exit();

        } 
        else {
            $data = array();
            $data['status'] = 'error';
            $data['message'] = 'There are no maids available for the selected time slot';

            $maids = $this->load->view('schedule/maid-availability', $data, TRUE);
            echo $maids;
            exit();
        }

    }

    public function re_assign_schedule()
    { 
        try {
            header('Content-Type: application/json; charset=utf-8');
            $post = $this->input->post();
            $booking_id1 = $post['booking_id'];
            $delete_date_from = $post['delete_date_from'];
            $delete_date_to = $post['delete_date_to'];
            $time_from = $post['time_from'];
            $time_to = $post['time_to'];
            $maid_id = $post['maid_id'];
            $maid_name = $post['maid_name'];
          
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id1);
            // ->where('b.booking_status', 2);
            $query = $this->db->get();
            $booking = $query->row();    
          
            $is_maidflagged = $this->schedule_model->is_maidflagged($maid_id, $booking->customer_id);

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $this->db->trans_begin();
            $time_from = strtotime($time_from);
            $time_to = strtotime($time_to);
            $working_minutes = ($time_to - $time_from) / 60;
            $working_hours = $working_minutes / 60;
            $booking_data['customer_id'] = $booking->customer_id;
            // $customer = $this->db->select("c.default_booking_address")
            //     ->from('customers as c')
            //     ->where('c.customer_id', $booking_data['customer_id'])->get()->row();

            // if ($customer->default_booking_address == "") {
            //     $response['status'] = "failed";
            //     $response['message'] = "Please fill the customer address";
            //     die(json_encode($response, JSON_PRETTY_PRINT));
            // }

            $service_start_date = $post['delete_date_from'] ? DateTime::createFromFormat('d/m/Y', $post['delete_date_from'])->format('Y-m-d') : null;
            $service_end_date = $delete_date_to ? DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d') : null;
        
            if ($booking->booking_type == "OD") {
                $booking_data['service_start_date'] = $service_start_date;
                $service_week_day = $booking->service_week_day;
                $booking_data['booking_type'] = "OD";
                $booking_data['service_end_date'] = $service_end_date;
                $booking_data['service_actual_end_date'] = $service_end_date;
                $booking_data['service_end'] = 1;
             
            } else {
                
                $booking_data['booking_type'] = "WE";
                // $service_start_date = $service_start_date ? DateTime::createFromFormat('d/m/Y', $service_start_date)->format('Y-m-d') : null;
                // $service_end_date = $service_end_date ? DateTime::createFromFormat('d/m/Y', $service_end_date)->format('Y-m-d') : null;
                $booking_data['service_start_date'] = $service_start_date;
                $booking_data['service_end_date'] = $service_end_date;
                
                $service_week_day = $booking->service_week_day;
               
                $booking_data['service_end'] = 1;
               
                $booking_data['service_actual_end_date'] = $service_end_date;
              
            }

            $booking_data['customer_address_id'] = $booking->customer_address_id;
            $booking_data['maid_id'] = $maid_id;
            $booking_data['service_type_id'] = $booking->service_type_id;;
            $booking_data['time_from'] = $post['time_from'];
            $booking_data['time_to'] = $post['time_to'];
            $booking_data['price_per_hr'] = $booking->price_per_hr;
            $booking_data['discount_price_per_hr'] = $booking->discount_price_per_hr;
            $booking_data['discount'] = $booking->discount;
            $booking_data['vat_charge'] = $booking->vat_charge;
            $booking_data['net_service_charge'] = $booking->net_service_charge;
            $booking_data['service_charge'] = $booking->service_charge;
            $booking_data['net_service_cost'] = $booking->net_service_cost;
            $booking_data['service_week_day'] = $service_week_day;
            $booking_data['total_amount'] = $booking->total_amount;
            $booking_data['booking_status'] = 1;
            $booking_data['booked_datetime'] = date('Y-m-d H:i:s');
            /***************************************************************** */
            // if ($this->input->post('cleaning_materials') == "Y") {
            $booking_data['cleaning_material'] = $booking->cleaning_material;
            $booking_data['cleaning_material_fee'] = $booking->cleaning_material_fee;
            $booking_data['booking_category'] = $booking->booking_category;

            $booking_data['_service_hours'] = $working_hours;
            $booking_data['_cleaning_material_rate_per_hour'] = $booking->_cleaning_material_rate_per_hour;
            $booking_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
            $booking_data['_service_discount_rate_per_hour'] = $booking->_service_rate_per_hour;
            $booking_data['_service_amount'] = $booking->_service_amount;
            $booking_data['_service_discount'] = $booking->_service_discount;
            $booking_data['_net_service_amount'] = $booking->_net_service_amount;
            // if ($this->input->post('cleaning_materials') == "Y") {
            $booking_data['_cleaning_material'] = $booking->_cleaning_material;
            $booking_data['_net_cleaning_material_amount'] = $booking->_net_cleaning_material_amount;

            $booking_data['_total_discount'] =  $booking->_total_discount;
            $booking_data['_taxable_amount'] = $booking->_taxable_amount;
            $booking_data['_vat_percentage'] = $booking->_vat_percentage;
            $booking_data['_vat_amount'] =  $booking->_vat_amount;
            $booking_data['_total_amount'] = $booking->_total_amount;
            $booking_data['created_at'] = date('Y-m-d H:i:s');
            $booking_data['updated_at'] = date('Y-m-d H:i:s');
            $booking_data['parent_booking_id'] = $booking_id1;
            $booking_data['booking_reassign_reason'] = $post['reason'];
            $booking_data['booking_reassign_status'] = 'Reassign';
            $booking_data['parent_maid_name'] = $maid_name;

            $booking1 = new stdClass();
            $booking1->time_from = $post['time_from'];
            $booking1->time_to =  $post['time_to'];
            $booking1->service_date = $booking_data['service_start_date'];
            $booking1->service_end = $booking_data['service_end'];
            $booking1->service_actual_end_date = $booking_data['service_actual_end_date'];
            $booking1->service_week_day = $booking_data['service_week_day'];
            $booking1->maid_id = $maid_id;
            // print_r($booking1);
            $timed_bookings = get_same_timed_bookings($booking, null);
            $booking_ids = array_column($timed_bookings,'booking_id');
            $deleted_booking_data = get_deleted_booking($booking_data);
          
            $book_id = array_column($deleted_booking_data,'day_service_booking_id');
            $result = array_diff($booking_ids,$book_id);
            if(sizeof($result) > 0)
            {
                $busy_maid_ids = array_column($timed_bookings,'maid_id');
                //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                throw new Exception('Selected time slot not available.', 100);
                }
            }  
            $day_services = new stdClass();
            $day_services->time_from = $booking_data['time_from'];
            $day_services->time_to =  $booking_data['time_to'];
            $day_services->service_date = $booking_data['service_start_date'];
            $day_services->maid_id = $maid_id;
            
            $deleted_booking_data = get_deleted_booking($booking_data);
            $day_service_id = array_column($deleted_booking_data,'day_service_id');
            $timed_day_service = get_same_timed_day_service($day_service, null);
                // print_r($timed_day_service);die();
            $day_service_ids = array_column($timed_day_service,'day_service_id');
            $resultarray = array_diff($day_service_ids,$day_service_id);
            if(sizeof($resultarray) > 0)
            {
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                if (in_array($day_service->maid_id, $busy_maid_id)) {
                    throw new Exception('Selected time slot not available.', 100);
                }
            } else {
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                if (in_array($day_service->maid_id, $busy_maid_id)) {
                    throw new Exception('Selected time slot not available.', 100);
                }

            }
                
            // $timed_bookings = get_same_timed_bookings($booking1, null);
            // print_r($timed_bookings);die();
            $booking_ids = array_column($timed_bookings,'booking_id');
            // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
            // $result = array_diff($book_id,$booking_ids);
            if(sizeof($booking_ids) > 0)
            {
                $busy_maid_ids = array_column($timed_bookings,'maid_id');

                if (in_array($booking1->maid_id, $busy_maid_ids)) {
                    throw new Exception('Selected time slot not available.', 100);
                }
            }  
            // $booking_data['reference_id'] =  $booking->reference_id;
            // echo '<pre>';
            // print_r($booking_data);die();
            $this->db->insert('bookings', $booking_data);
            $booking_id = $this->db->insert_id();
            $bookin_delete['reassign_status'] = 'Reassign';
            $bookin_delete['reason'] = $post['reason'];
            $this->db->where(array('booking_id' => $booking_id1));
            $this->db->update('booking_deletes', $bookin_delete);
            if ($booking_id > 0) {
                $response['status'] = "success";
                $response['message'] = "Schedule created successfully.";
                $booking_data['reference_id'] = "MM-" . $booking_data['booking_type'] . "-" . sprintf("%06d", $booking_id);
                $this->db->set($booking_data)
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                $this->db->trans_commit();
        
            } else {
                $respose['status'] = "failed";
                $respose['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
              
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
}