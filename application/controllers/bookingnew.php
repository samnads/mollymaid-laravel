<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookingnew extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('customers_model');
		$this->load->model('maids_model');
		$this->load->model('zones_model');
		$this->load->model('service_types_model');
		$this->load->model('bookings_model');
		$this->load->model('tablets_model');
                $this->load->model('settings_model');
		$this->load->model('justmop_model');
		$this->load->helper('google_api_helper');
	}

	public function index()
	{
		ini_set('memory_limit','-1');
                
		if($this->uri->segment(2) && strlen(trim($this->uri->segment(2) > 0)))
		{
			$s_date = explode('-', trim($this->uri->segment(2)));
			if(count($s_date) == 3 && checkdate($s_date[1], $s_date[0], $s_date[2]) && $s_date[2] >= 2014 && $s_date[2] <= date('Y') + 2)
			{
				$schedule_date = $s_date[0] . '-' . $s_date[1] . '-' . $s_date[2];
			}				
		}
		else
		{
			$schedule_date = date('d-M-Y');
                        //$schedule_date = date('d-M-Y', strtotime('-1 day'));
		}
                $morning_avail='';
                $eve_avail='';
				$tot_avail='';
                
                if($this->uri->segment(3) && strlen(trim($this->uri->segment(3) > 0)))
		{
                        if($this->uri->segment(3)==1){
			$freemaid_id = 	$this->uri->segment(3);	
                        }
                        else if($this->uri->segment(3)==2)
                        {
                         $morning_avail = $this->uri->segment(3);	   
                        }
                        else if($this->uri->segment(3)==3)
                        {
                         $eve_avail   = $this->uri->segment(3);
                        } else if($this->uri->segment(3)==4)
						{
							$tot_avail = $this->uri->segment(3);
						}
		}
		else
		{
			$freemaid_id = '';
                        $morning_avail='';
                        $eve_avail='';
                        $tot_avail='';

		}
                
                if($this->uri->segment(4) && strlen(trim($this->uri->segment(4) > 0)))
		{
			$team_id = $this->uri->segment(4);		
		}
		else
		{
			$team_id = '';
		}
		
		if(!isset($schedule_date))
		{
			redirect('booking');
			exit();
		}
		
		$week_day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		
		$service_date = date('Y-m-d', strtotime($schedule_date));
		$service_week_day = date('w', strtotime($schedule_date));
		
		//new
		$get_leave_maids = $this->maids_model->get_maids_leave_by_date($service_date);
		$leave_maid_ids_new = array();
		foreach ($get_leave_maids as $leave)
		{
			array_push($leave_maid_ids_new, $leave->maid_id);
		}
		//ends
                if($freemaid_id == 1)
                {
                    //$maidss = $this->maids_model->get_maids($team_id);
                    $maidss = $this->maids_model->get_maids();
                    $maids = array();
                    foreach ($maidss as $maid)
                    {
						
						if (!in_array($maid->maid_id, $leave_maid_ids_new)) {
							$shifts = $this->bookings_model->get_schedule_by_date_new_count($service_date,$maid->maid_id);
							if($shifts == 0)
							{
								//$newdata[] = new stdClass();
								$newdata['maid_id'] = $maid->maid_id;
								$newdata['odoo_maid_id'] = $maid->odoo_package_maid_id;
								$newdata['maid_name'] = $maid->maid_name;
								$newdata['maid_nationality'] = $maid->maid_nationality;
								$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
								$newdata['maid_photo_file'] = $maid->maid_photo_file;
								$newdata['flat_name'] = $maid->flat_name;
								$newdata['odoo_flat_id'] = $maid->odoo_package_flat_id;
								$newdata['maid_status'] = $maid->maid_status;
								$newdata['odoo_synch_status'] = $maid->odoo_package_maid_status;
								//$newdata['team_name'] = $maid->team_name;
								array_push($maids, (object) $newdata);
								//$maids = ;
							}
						}
                        
                    }
                    
//                    echo '<pre>';
//                    print_r($maids);
//                    echo '</pre>';
                } 
                
                else if($morning_avail==2)
                {
					$maidss = $this->maids_model->get_maids();
					$maids  = array();
					$filter_by=2;
					foreach ($maidss as $maid)
                    {
						if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
						{
							$get_avilable_before_noon = $this->bookings_model->get_avilable_before_noon($service_date,$maid->maid_id);
							$count = count($get_avilable_before_noon);
							$hour_array = array();
							for($i=0; $i<$count; $i++)
							{
								if($i==0)
								{ 
									$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime('08:00:00')) / 3600;
									array_push($hour_array,$gap);
									if($count == 1)
									{
										$gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
										array_push($hour_array,$gap2);
									}
								} else if($i == ($count - 1))
								{
									$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
									array_push($hour_array,$gap); 
									$gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
									array_push($hour_array,$gap2);
								} else {
									$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
									array_push($hour_array,$gap);
								}
							}
							
							foreach($hour_array as $list)
							{
							   if($list >= 3)
							   {
									$newdata['maid_id'] = $maid->maid_id;
									$newdata['odoo_maid_id'] = $maid->odoo_package_maid_id;
									$newdata['maid_name'] = $maid->maid_name;
									$newdata['maid_nationality'] = $maid->maid_nationality;
									$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
									$newdata['maid_photo_file'] = $maid->maid_photo_file;
									$newdata['flat_name'] = $maid->flat_name;
									$newdata['odoo_flat_id'] = $maid->odoo_package_flat_id;
									$newdata['maid_status'] = $maid->maid_status;
									$newdata['odoo_synch_status'] = $maid->odoo_package_maid_status;
									$newdata['total_hrs'] = $total_hrs;
									array_push($maids, (object) $newdata);
							   }
							}
							
							if($count == 0)
							{
								$newdata['maid_id'] = $maid->maid_id;
								$newdata['odoo_maid_id'] = $maid->odoo_package_maid_id;
								$newdata['maid_name'] = $maid->maid_name;
								$newdata['maid_nationality'] = $maid->maid_nationality;
								$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
								$newdata['maid_photo_file'] = $maid->maid_photo_file;
								$newdata['flat_name'] = $maid->flat_name;
								$newdata['odoo_flat_id'] = $maid->odoo_package_flat_id;
								$newdata['maid_status'] = $maid->maid_status;
								$newdata['odoo_synch_status'] = $maid->odoo_package_maid_status;
								$newdata['total_hrs'] = $total_hrs;
								array_push($maids, (object) $newdata);
							}
						}
                    }
                }
                else if($eve_avail==3)
                {
					$maidss = $this->maids_model->get_maids();
					$maids  = array();
					$filter_by=3;
				  
					foreach ($maidss as $maid)
                    {
						if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
						{
							$get_avilable_after_noon = $this->bookings_model->get_avilable_after_noon($service_date,$maid->maid_id);
							$count = count($get_avilable_after_noon);
							$hour_array = array();
							for($i=0; $i<$count; $i++)
							{
								if($i==0)
								{ 
									$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime('13:00:00')) / 3600;
									array_push($hour_array,$gap);
									if($count == 1)
									{
										$gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
										array_push($hour_array,$gap2);
									}
								} else if($i == ($count - 1))
								{
									$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i-1]->time_to)) / 3600;
									array_push($hour_array,$gap); 
									$gap2 = (strtotime('19:00:00') - strtotime($get_avilable_after_noon[$i]->time_to)) / 3600;
									array_push($hour_array,$gap2);
								} else {
									$gap = (strtotime($get_avilable_after_noon[$i]->time_from) - strtotime($get_avilable_after_noon[$i-1]->time_to)) / 3600;
									array_push($hour_array,$gap);
								}
							}
							
							foreach($hour_array as $list)
							{
							   if($list >= 3)
							   {
									$newdata['maid_id'] = $maid->maid_id;
									$newdata['odoo_maid_id'] = $maid->odoo_package_maid_id;
									$newdata['maid_name'] = $maid->maid_name;
									$newdata['maid_nationality'] = $maid->maid_nationality;
									$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
									$newdata['maid_photo_file'] = $maid->maid_photo_file;
									$newdata['flat_name'] = $maid->flat_name;
									$newdata['odoo_flat_id'] = $maid->odoo_package_flat_id;
									$newdata['maid_status'] = $maid->maid_status;
									$newdata['odoo_synch_status'] = $maid->odoo_package_maid_status;
									$newdata['total_hrs'] = $total_hrs;
									array_push($maids, (object) $newdata);
							   }
							}
							
							if($count == 0)
							{
								$newdata['maid_id'] = $maid->maid_id;
								$newdata['odoo_maid_id'] = $maid->odoo_package_maid_id;
								$newdata['maid_name'] = $maid->maid_name;
								$newdata['maid_nationality'] = $maid->maid_nationality;
								$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
								$newdata['maid_photo_file'] = $maid->maid_photo_file;
								$newdata['flat_name'] = $maid->flat_name;
								$newdata['odoo_flat_id'] = $maid->odoo_package_flat_id;
								$newdata['maid_status'] = $maid->maid_status;
								$newdata['odoo_synch_status'] = $maid->odoo_package_maid_status;
								$newdata['total_hrs'] = $total_hrs;
								array_push($maids, (object) $newdata);
							}
						}
                    }
                 
                } else if($tot_avail == 4)
				{
					$maidss = $this->maids_model->get_maids();
					$maids  = array();
					$filter_by=4;
				  
					foreach ($maidss as $maid)
                    {
						if (!in_array($maid->maid_id, $leave_maid_ids_new)) 
						{
							//$newdata[] = new stdClass();
							$newdata['maid_id'] = $maid->maid_id;
							$newdata['odoo_maid_id'] = $maid->odoo_package_maid_id;
							$newdata['maid_name'] = $maid->maid_name;
							$newdata['maid_nationality'] = $maid->maid_nationality;
							$newdata['maid_mobile_1'] = $maid->maid_mobile_1;
							$newdata['maid_photo_file'] = $maid->maid_photo_file;
							$newdata['flat_name'] = $maid->flat_name;
							$newdata['odoo_flat_id'] = $maid->odoo_package_flat_id;
							$newdata['maid_status'] = $maid->maid_status;
							$newdata['odoo_synch_status'] = $maid->odoo_package_maid_status;
							//$newdata['team_name'] = $maid->team_name;
							array_push($maids, (object) $newdata);
							//$maids = ;
						}
                    }
				} else {
                    //$maids = $this->maids_model->get_maids($team_id);
                    $maids = $this->maids_model->get_maids();
//                    echo '<pre>';
//                    print_r($maids);
//                    echo '</pre>';
                }
                //exit();
		
		$bookings = $this->bookings_model->get_schedule_by_date($service_date);
		// echo "<pre>";print_r($bookings);die;
		//$bookings = $this->bookings_model->get_schedule_by_date($service_date,$team_id);


		$all_bookings = array();
		$maid_bookings = array();
		$i = 0;
		foreach($bookings as $booking)
		{
			$reff = "";
			
			if($booking->pay_by != "")
			{
				$mop = $booking->pay_by;
				$ref = $booking->justmop_reference;
			} else {
				$mop = $booking->payment_mode;
				$ref = $booking->justmop_reference;
			}
			
			if($booking->customer_address == "")
			{
				$a_address = 'Building - '.$booking->building.', '.$booking->unit_no.''.$booking->street;
			} else {
				$a_address = $booking->customer_address;
			}
			
			//check_booking_transferred
			$check_booking_transferred = $this->bookings_model->check_booking_transferred_new($booking->booking_id,$service_date);
			//ends
			
			$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['booking_id'] = $booking->booking_id;
			$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['customer'] = $booking->customer_nick_name;
			$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['zone'] = $booking->zone_name;
			$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['start_time'] = strtotime($booking->time_from);
			$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['end_time'] = strtotime($booking->time_to);
			$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['type'] = $booking->booking_type;
                        //$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['zonetype'] = $zonecolor;
                        $maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['user'] = $booking->user_fullname ? $booking->user_fullname : 'N/A';
                        //edited by vishnu
                        $maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['pending_amount'] = $booking->pending_amount;
                        $maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['balance'] = $booking->balance;
                        $maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['signed'] = $booking->signed;
                        $maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['payment_type'] = $booking->payment_type;
						$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['ref'] = $reff;
						$maid_bookings[$booking->maid_id][strtotime($booking->time_from)]['servicestatus'] = $booking->service_status;
                        //ends
			
			$booked_slots[$booking->maid_id]['time'][strtotime($booking->time_from)] = strtotime($booking->time_to);
			
			$all_bookings[$booking->booking_id] = new stdClass();
			$all_bookings[$booking->booking_id]->customer_id = $booking->customer_id;
			$all_bookings[$booking->booking_id]->customer_nick_name = html_escape($booking->customer_nick_name);
			$all_bookings[$booking->booking_id]->customer_address_id = $booking->customer_address_id;
                        $all_bookings[$booking->booking_id]->apartment_no = $booking->building;
			$all_bookings[$booking->booking_id]->customer_zone = html_escape($booking->zone_name);
			$all_bookings[$booking->booking_id]->customer_area = html_escape($booking->area_name);
			$all_bookings[$booking->booking_id]->customer_address = htmlspecialchars($a_address, ENT_QUOTES, 'UTF-8');//html_escape($a_address);
			$all_bookings[$booking->booking_id]->maid_id = $booking->maid_id;
			$all_bookings[$booking->booking_id]->maid_name = html_escape($booking->maid_name);
			$all_bookings[$booking->booking_id]->service_type_id = $booking->service_type_id;
			$all_bookings[$booking->booking_id]->service_week_day = html_escape($week_day_names[$booking->service_week_day]);
			$all_bookings[$booking->booking_id]->time_from = date('g:i a', strtotime($booking->time_from));
			$all_bookings[$booking->booking_id]->time_to = date('g:i a', strtotime($booking->time_to));
			$all_bookings[$booking->booking_id]->time_from_stamp = strtotime($booking->time_from);
			$all_bookings[$booking->booking_id]->time_to_stamp =  strtotime($booking->time_to);
			
			$all_bookings[$booking->booking_id]->service_end = $booking->service_end;
			$all_bookings[$booking->booking_id]->service_end_date = date('d/m/Y', strtotime($booking->service_end_date));
			$all_bookings[$booking->booking_id]->service_actual_end_date = date('d/m/Y', strtotime($booking->service_actual_end_date));
			$all_bookings[$booking->booking_id]->booking_note = html_escape($booking->booking_note);
			$all_bookings[$booking->booking_id]->booking_type = $booking->booking_type;
			$all_bookings[$booking->booking_id]->pending_amount = $booking->pending_amount;
			$all_bookings[$booking->booking_id]->price_hourly = $booking->price_hourly;
			$all_bookings[$booking->booking_id]->discount_price_per_hr = $booking->discount_price_per_hr;
			$all_bookings[$booking->booking_id]->is_locked = $booking->is_locked;
			$all_bookings[$booking->booking_id]->net_service_cost = $booking->net_service_cost;
                        $all_bookings[$booking->booking_id]->discount = $booking->discount ? $booking->discount : 0;
                        $all_bookings[$booking->booking_id]->total_amount = $booking->total_amount ? $booking->total_amount : 0;
                        $all_bookings[$booking->booking_id]->payment_type = $booking->payment_type;
                        $all_bookings[$booking->booking_id]->balance = $booking->balance;
                        $all_bookings[$booking->booking_id]->signed = $booking->signed;
                        $all_bookings[$booking->booking_id]->cleaning_type = $booking->cleaning_material;
                        $all_bookings[$booking->booking_id]->mobile_number_1 = $booking->mobile_number_1;
                        $all_bookings[$booking->booking_id]->crew_in = $booking->crew_in;
						//$all_bookings[$booking->booking_id]->justmop_reference = $booking->justmop_reference;
						$all_bookings[$booking->booking_id]->justmop_reference = $ref;
						$all_bookings[$booking->booking_id]->payment_mode = $mop;
						$all_bookings[$booking->booking_id]->priceperhr = $booking->price_per_hr;
						$all_bookings[$booking->booking_id]->tabletid = $booking->tabletid;
						if(count($check_booking_transferred) > 0)
						{
							$all_bookings[$booking->booking_id]->transfeeredtabletname = $check_booking_transferred->tablet_driver_name;
						} else {
							$all_bookings[$booking->booking_id]->transfeeredtabletname = "";
						}
						
			if($booking->cleaning_material == 'Y')
                        {
                            $all_bookings[$booking->booking_id]->cleaning_material = "Yes";
                        } else {
                            $all_bookings[$booking->booking_id]->cleaning_material = "No";
                        }
						$selected_services=$this->bookings_model->get_servicemapping_by_bukid($booking->booking_id);//bookingservicemapping
            $sel_ser_text='';

            foreach ($selected_services as $ss) {
                $sel_ser_text.=$ss->service_type_name." - ".$ss->service_category_name." - ".$ss->sub_category_name;
                if($ss->service_furnish_name){$sel_ser_text.=" - ".$ss->service_furnish_name;}
                $sel_ser_text.=" - ".$ss->name." - ".$ss->service_cost."<br/>";
            }

            if($sel_ser_text==''){$sel_ser_text=$booking->service_type_name;}
            $all_bookings[$booking->booking_id]->service_type_names = $sel_ser_text;
			$i++;
		}
		
		//echo '<pre>';print_r($maid_bookings); exit();

		$times = array();
		$current_hour_index = 0;
		$time = '12:00 am';  
		$time_stamp = strtotime($time);
                
		//for ($i = 0; $i < 24; $i++) 
		for ($i = 0; $i < 48; $i++)
		{
			if(!isset($times['t-' . $i]))
			{
				$times['t-' . $i] = new stdClass();
			}
			
			$times['t-' . $i]->stamp = $time_stamp;
			$times['t-' . $i]->display = $time;
			
			if(date('H') == $i)
                        //if(date('H') == $i && $service_date == date('Y-m-d'))
			{
				$current_hour_index = 't-' . (($i *2) - 1);
                            //$current_hour_index = 't-' . ($i - 1);
			}
			//For 1 hour
			//$time_stamp = strtotime('+60mins', strtotime($time));
                        //For 30 min
                        $time_stamp = strtotime('+30mins', strtotime($time));
			$time = date('g:i a', $time_stamp);
		}
                
                // Maid Leave
                //$maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
                
                $leave_maid_ids = array();
                foreach ($get_leave_maids as $leave)
                {
                    array_push($leave_maid_ids, $leave->maid_id);
                }
				
				$leave_maid_types = array();
                foreach ($get_leave_maids as $leave)
                {
					$leavetypearay = array();
					$leavetypearay['maid_id'] = $leave->maid_id;
					$leavetypearay['maid_type'] = $leave->typeleaves;
                    array_push($leave_maid_types, $leavetypearay);
                }
		
		if($this->input->is_ajax_request())
		{
			if($this->input->post('action') && $this->input->post('action') == 'book-maid')
			{
				if($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0 && $this->input->post('customer_address_id') && is_numeric($this->input->post('customer_address_id')) && $this->input->post('customer_address_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 && $this->input->post('service_type_id') && is_numeric($this->input->post('service_type_id')) && $this->input->post('service_type_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('booking_type') && ($this->input->post('booking_type') == 'OD' || $this->input->post('booking_type') == 'WE' || $this->input->post('booking_type') == 'BW'))
				{
					if(strtotime($schedule_date) < strtotime( date('d-M-Y')))
					{
						echo 'refresh';
						exit();
					}
					
					$customer_id = trim($this->input->post('customer_id'));
					$tabletidval = trim($this->input->post('tabletidval'));
					$customer_address_id = trim($this->input->post('customer_address_id'));
					$maid_id = trim($this->input->post('maid_id'));
					$service_type_id = trim($this->input->post('service_type_id'));
					$time_from =  date('H:i', trim($this->input->post('time_from')));
					$time_to = date('H:i', trim($this->input->post('time_to')));
					$booking_type = trim($this->input->post('booking_type'));
					$is_locked = $this->input->post('is_locked') ? 1 : 0;
					$repeat_days = array($service_week_day);
					$repeat_end = 'ondate';
					$service_end_date = $service_date;
					if($this->input->post('pending_amount') == '')
					{
						$pending_amount = 0;
					} else {
					$pending_amount = $this->input->post('pending_amount');
					}
					$hourly_amount = $this->input->post('hrly_amount');
					$discountratehr = $this->input->post('discountratehr');
                    $cleaning_material = $this->input->post('cleaning_material');
                    $payment_mode=   $this->input->post('payment_mode'); 
					if($this->input->post('discount') == '')
					{
						$discount = 0;
					} else {
						$discount = trim($this->input->post('discount'));
					}
                    
                    $net_amtcost = trim($this->input->post('servicetotamt'));
                    $total_amt = trim($this->input->post('tot_amt'));
					$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';
					$email_notifications = $this->input->post('email_notifications');
					$sms_notifications = $this->input->post('sms_notifications');
					$just_mop_new_ref = $this->input->post('just_mop_new_ref');
					
					if(preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					{
						echo 'refresh';
						exit();
					}
					
					if($booking_type != 'OD')
					{
						if($this->input->post('repeat_days') && is_array($this->input->post('repeat_days')) && count($this->input->post('repeat_days')) > 0 &&  count($this->input->post('repeat_days')) <= 7 && $this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate'))
						{
							$repeat_days = $this->input->post('repeat_days');
							$repeat_end = $this->input->post('repeat_end');
							
							if($repeat_end == 'ondate')
							{
								if($this->input->post('repeat_end_date'))
								{
									$repeat_end_date = $this->input->post('repeat_end_date');
									
									$repeat_end_date_split = explode('/', $repeat_end_date);
									if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
									{
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);
										
                                                                                $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                                                                $years = floor($diff / (365*60*60*24));
                                                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                                                                
										if($days < 7)//if($diff->days < 7)
										{
											echo 'refresh';
											exit();
										}
										
										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									}
									else 
									{
										echo 'refresh';
										exit();
									}
								}
								else
								{
									echo 'refresh';
									exit();
								}
							}
						}
						else 
						{
							echo 'refresh';
							exit();
						}
					}
					
					$time_from_stamp = trim($this->input->post('time_from'));
					$time_to_stamp = trim($this->input->post('time_to'));
					
					if($booking_type == 'OD')
					{
						if(isset($booked_slots[$maid_id]['time']))
						{
							foreach($booked_slots[$maid_id]['time'] as $f_time => $t_time)
							{
								if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
								{
									$return = array();
									$return['status'] = 'error';
									$return['message'] = 'The selected time slot is not available for this maid';

									echo json_encode($return);
									exit();
								}
							}
						}
					}
					
					$today_week_day = date('w', strtotime($service_date));
					
					if($booking_type == 'WE')
					{
						foreach($repeat_days as $repeat_day)
						{
							if($repeat_day < $today_week_day)
							{
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							}
							else
							{
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                                                        
                                                       

							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
                                                        
                                                  
							foreach($bookings_on_day as $booking_on_day)
							{
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
								
								if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);
                                                                        
                                                                       

									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) )//|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
									{
                                                                            
                                                                                
                                                                                 //echo "<br>".$booking_on_day->booking_id;
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

										echo json_encode($return);
										exit();
									}
                                                                        
                                                                        
								}
                                                                
                                                                
                                                                
							}
						}
					}
                                        else if($booking_type == 'BW')
					{
                                            foreach($repeat_days as $repeat_day)
                                            {
                                                if($repeat_day < $today_week_day)
                                                {
                                                        $day_diff = (6 - $today_week_day + $repeat_day + 1);
                                                }
                                                else
                                                {
                                                        $day_diff = $repeat_day - $today_week_day;
                                                }

                                                $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

                                                //$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_new($service_start_date,$maid_id, $repeat_day);
                                                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
//                                                echo '<pre>';
//                                                print_r($bookings_on_day);
//                                                exit();
                                                foreach($bookings_on_day as $booking_on_day)
                                                {
                                                    $s_date_stamp = strtotime($service_start_date);
                                                    $e_date_stamp = strtotime($service_end_date);
                                                    $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                                    $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
                                                    
                                                    if(($booking_on_day->service_end == 1) && ($booking_on_day->service_actual_end_date < $service_start_date))
                                                    {
                                                        
                                                    } else {
                                                        if($booking_on_day->booking_type == "BW")
                                                        {
                                                            $now = strtotime($service_start_date); // or your date as well
                                                            $your_date = strtotime($booking_on_day->service_start_date);
                                                            $datediff = $now - $your_date;

                                                            $week_diff = round($datediff / (60 * 60 * 24));
                                                            $week_difference = fmod($week_diff,14);

                                                            if($week_difference == 0 || $week_difference == '-0')
                                                            {
                                                                $f_time = strtotime($booking_on_day->time_from);
                                                                $t_time = strtotime($booking_on_day->time_to);

                                                                if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                                                {
                                                                        $return = array();
                                                                        $return['status'] = 'error';
                                                                        $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                                                        echo json_encode($return);
                                                                        exit();
                                                                }
                                                            }
                                                        } else {
                                                            //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                            //edited by vishnu
                                                            if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                            //latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                            //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                                            {
                                                                    $f_time = strtotime($booking_on_day->time_from);
                                                                    $t_time = strtotime($booking_on_day->time_to);

                                                                    if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                                                    {
                                                                            $return = array();
                                                                            $return['status'] = 'error';
                                                                            $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                                                            echo json_encode($return);
                                                                            exit();
                                                                    }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
					}
				}
				else
				{
					echo 'refresh';
					exit();
				}
				
				$todays_new_booking = array();
				foreach($repeat_days as $repeat_day)
				{
					if($repeat_day < $today_week_day)
					{
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					}
					else
					{
						$day_diff = $repeat_day - $today_week_day;
					}
					
					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

					$booking_fields = array();
					$booking_fields['customer_id'] = $customer_id;
					$booking_fields['customer_address_id'] = $customer_address_id;
					$booking_fields['maid_id'] = $maid_id;
					$booking_fields['service_type_id'] = $service_type_id;
					$booking_fields['service_start_date'] = $service_start_date;
					$booking_fields['service_week_day'] = $repeat_day;
					$booking_fields['time_from'] = $time_from;
					$booking_fields['time_to'] = $time_to;
					$booking_fields['booking_type'] = $booking_type;
					$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
					$booking_fields['service_end_date'] = $service_end_date;					
					$booking_fields['booking_note'] = $booking_note;
					$booking_fields['is_locked'] = $is_locked;
					$booking_fields['pending_amount'] = $pending_amount;
					$booking_fields['price_per_hr'] = $hourly_amount;
					$booking_fields['discount_price_per_hr'] = $discountratehr;
					if($cleaning_material == 'Y')
					{
						$booking_fields['cleaning_material'] = 'Y';
					} else {
						$booking_fields['cleaning_material'] = 'N';
					}
                                        
                                        					
					$booking_fields['discount'] = $discount;
					$booking_fields['booking_category'] = 'C';
					$booking_fields['net_service_cost'] = $net_amtcost;
					$booking_fields['total_amount'] = $total_amt;
					$booking_fields['booked_by'] = user_authenticate();
					$booking_fields['pay_by'] = $payment_mode;
					$booking_fields['booking_status'] = 1;
					$booking_fields['booked_from'] = 'A';
					$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
					$booking_fields['tabletid'] = $tabletidval;
					if($booking_type == 'WE')
                                        {
                                            $updatefield = array();
                                            $updatefield['customer_booktype'] = 1;
                                            $updatess = $this->customers_model->update_booktype($updatefield,$customer_id);
                                        }
					$booking_id = $this->bookings_model->add_booking($booking_fields);
					
					if($booking_id)
					{
						if($this->input->post('service_servicetypeval') == 'C')
						{
							$service_field_array = array();
							$service_field_array['booking_id'] = $booking_id;
							$service_field_array['service_id'] = $service_type_id;
							$service_field_array['category_id'] = $this->input->post('service_categoryidval');
							$service_field_array['sub_category_id'] = $this->input->post('service_subcategoryidval');
							$service_field_array['service_sub_furnish_id'] = $this->input->post('service_furnishid');
							$service_field_array['is_scrubbing'] = $this->input->post('service_scrubpolishid');
							$service_field_array['name'] = $this->input->post('service_valname');
							$service_field_array['service_cost'] = $this->input->post('service_servicecostval');
							$service_field_array['squarefeet_count'] = $this->input->post('service_sqftcount');
							
							$servicefiled = $this->service_types_model->add_service_mapping($service_field_array);
						}
						//hided for crystalblu
                        // $justmop_ref=array();
						// $justmop_ref['booking_id']= $booking_id;
              	        // $justmop_ref['mop']= ucfirst($payment_mode);
                        // $justmop_ref['service_date']= $service_start_date;
                        // $justmop_ref['just_mop_ref']= $just_mop_new_ref;
              	        // $this->bookings_model->add_justmop_ref($justmop_ref);
						//ends
						if($booking_type == "WE"){
							$last_sunday = strtotime('last Sunday');
							$service_start_date_for_not = date('l', strtotime('+'.$repeat_day.' day', $last_sunday));
						} else if($booking_type == "OD"){
							$service_start_date_for_not = date('d/m/Y', strtotime($service_start_date));
						}
						$no_of_hourss = $this->get_time_difference($time_from, $time_to);
						$time_from_not =  date('g:i A', strtotime(html_escape($time_from)));
						$booking = $this->bookings_model->get_booking_by_id($booking_id);
						$email_address_not = $booking->email_address;
						$mobile = $booking->mobile_number_1;
						$content = " Your scheduled booking on ".$service_start_date_for_not." for ".$no_of_hourss." hrs  from ".$time_from_not." has been confirmed.There might be chance of delay in dropping the crew due to the traffic, thanks for understanding in advance.";
						if($email_notifications == "Y")
						{
							// uncommented
							$this->send_email_notifications($content,$email_address_not);
						}
						if($sms_notifications == 'Y')
						{
							$sms_send = $this->send_sms_notifications($content,$mobile);
						}
						$booking_done = TRUE;
                                                
                                            if($service_start_date == date('Y-m-d'))
                                            {
                                                    $todays_new_booking[] = $booking_id;
                                            }
											if($tabletidval > 0)
											{
												//$booking = $this->bookings_model->get_booking_by_id($booking_id);

												$push_fields = array();
												$push_fields['tab_id'] = $tabletidval;
												$push_fields['type'] = 1;
												$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
												// code moved starts here
												$push_fields['maid_id'] = $booking->maid_id;
												$push_fields['title'] = "New Booking";
												$push_fields['customer_name'] = $booking->customer_name;
												$push_fields['maid_name'] = $booking->maid_name;
												$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
												// code moved ends here
												if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
												{
													$push = $this->bookings_model->add_push_notifications($push_fields);
												}
											} else {
												$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
												if(isset($c_address->zone_id))
												{
														$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
														if($tablet){
														//$booking = $this->bookings_model->get_booking_by_id($booking_id);

														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 1;
														$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
														// code moved starts here
														$push_fields['maid_id'] = $booking->maid_id;
														$push_fields['title'] = "New Booking";
														$push_fields['customer_name'] = $booking->customer_name;
														$push_fields['maid_name'] = $booking->maid_name;
														$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
														// code moved ends here
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$push = $this->bookings_model->add_push_notifications($push_fields);
														}
														}
												}
											}
						
					}
				}
				
				if(isset($booking_done))
				{
					if(count($todays_new_booking) > 0 && isset($customer_address_id))
					{
						$booking = $this->bookings_model->get_booking_by_id($booking_id);
						if($tabletidval > 0)
						{
							$tablet = $this->tablets_model->get_tablet_by_zone_tabid($tabletidval);
							
							$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
							if($tablet)
							{
								if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
								{
									$deviceid = $tablet->google_reg_id;
									// optional payload
									$payload = array();
									$payload['isfeedback'] = false;
									if(isset($push) && $push > 0)
									{
										$payload['pushid'] = $push;
									} else {
										$payload['pushid'] = 0;
									}

									$title = "New Booking";
									$message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
									$res = array();
									// $res['data']['title'] = $title;
									// $res['data']['is_background'] = false;
									// $res['data']['message'] = $message;
									// $res['data']['image'] = "";
									// $res['data']['payload'] = $payload;
									// $res['data']['timestamp'] = date('Y-m-d G:i:s');
									
									$res['title'] = $title;
									$res['is_background'] = false;
									$res['body'] = $message;
									$res['image'] = "";
									$res['payload'] = $payload;
									$res['customer'] = $booking->customer_name;
									$res['maid'] = $booking->maid_name;
									$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
									$res['timestamp'] = date('Y-m-d G:i:s');
									$regId = $deviceid;
									$fields = array(
										'to' => $regId,
										'notification' => $res,
										'data' => $res,
									);
		
									//$regId = $deviceid;
									//$fields = array(
									//    'to' => $regId,
									//    'data' => $res,
									//);
									//$return = android_customer_push($fields);
									//$return = android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->time_from . '-' . $booking->time_to));
									$return = android_customer_app_push($fields);
									if($getmaiddeviceid->device_id != "")
									{
										$maid_fields = array(
											'to' => $getmaiddeviceid->device_id,
											'notification' => $res,
											'data' => $res,
										);
										android_customer_app_push($maid_fields);
									}
								}
							}
						} else {
							$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
							if(isset($c_address->zone_id))
							{
								$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
								//$booking = $this->bookings_model->get_booking_by_id($booking_id);
								$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
								if($tablet)
								{
									if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
									{
										$deviceid = $tablet->google_reg_id;
										// optional payload
										$payload = array();
										$payload['isfeedback'] = false;
										if(isset($push) && $push > 0)
										{
											$payload['pushid'] = $push;
										} else {
											$payload['pushid'] = 0;
										}

										$title = "New Booking";
										$message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
										$res = array();
										// $res['data']['title'] = $title;
										// $res['data']['is_background'] = false;
										// $res['data']['message'] = $message;
										// $res['data']['image'] = "";
										// $res['data']['payload'] = $payload;
										// $res['data']['timestamp'] = date('Y-m-d G:i:s');
										
										$res['title'] = $title;
										$res['is_background'] = false;
										$res['body'] = $message;
										$res['image'] = "";
										$res['payload'] = $payload;
										$res['customer'] = $booking->customer_name;
																$res['maid'] = $booking->maid_name;
																$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
										$res['timestamp'] = date('Y-m-d G:i:s');
										$regId = $deviceid;
										$fields = array(
											'to' => $regId,
											'notification' => $res,
											'data' => $res,
										);
			
										//$regId = $deviceid;
										//$fields = array(
										//    'to' => $regId,
										//    'data' => $res,
										//);
										//$return = android_customer_push($fields);
										//$return = android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->time_from . '-' . $booking->time_to));
										$return = android_customer_app_push($fields);
										if($getmaiddeviceid->device_id != "")
										{
											$maid_fields = array(
												'to' => $getmaiddeviceid->device_id,
												'notification' => $res,
												'data' => $res,
											);
											android_customer_app_push($maid_fields);
										}
									}
								}
								//print_r($return);exit;
							}
						}
					}
					
					$return = array();
					$return['status'] = 'success';
					$return['maid_id'] = $maid_id;
					$return['customer_id'] = $customer_id;
					$return['time_from'] = $time_from;
					$return['time_to'] = $time_to;
					//$user_logged_in_session = $CI->session->userdata('user_logged_in');
                                        //$return['user'] = $user_logged_in_session['user_fullname'];
                                        
					echo json_encode($return);
					exit();
				}
			}
                        
                        if ($this->input->post('action') && $this->input->post('action') == 'register_contact_exists_new') 
                        {
                            $phone = $this->input->post('phone');
                            $checkmobileexist = $this->bookings_model->phone_exists_new($phone);
                            if($checkmobileexist >0)
                            {
                                $response['status'] = 'exist';
                                $response['message'] = 'Contact number already exist. Please login to continue';
                                echo json_encode($response);
                                exit();
                            } else {
                                $response['status'] = 'not';
                                echo json_encode($response);
                                exit();
                            }
                        }
			// Edited byt Geethu
			if($this->input->post('action') && $this->input->post('action') == 'copy-maid')
			{
				if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 )
				{
					if(strtotime($schedule_date) < strtotime( date('d-M-Y')))
					{
						echo 'refresh';
						exit();
					}
					$booking_id = trim($this->input->post('booking_id'));				
					$booking = $this->bookings_model->get_booking_by_id($booking_id);
					$first_booking_id=$booking_id;
//                                        if($booking->is_locked == 1 && $booking->booked_by != user_authenticate())
//                                        {
//                                            echo 'locked';
//                                            exit();
//                                        }
					$customer_id = html_escape($booking->customer_id);
					$customer_address_id = html_escape($booking->customer_address_id);
					$maid_id = trim($this->input->post('maid_id'));
					$service_type_id = html_escape($booking->service_type_id);
					$time_from =  date('H:i', strtotime(html_escape($booking->time_from)));
					$time_to = date('H:i', strtotime(html_escape($booking->time_to)));
					$booking_type = html_escape($booking->booking_type);
					$repeat_days = array(html_escape($booking->service_week_day));
					$repeat_end = html_escape($booking->service_end) == 0 ? 'never' : 'ondate';
					$service_end_date = html_escape($booking->service_actual_end_date);
                    $service_start_date = html_escape($booking->service_start_date);
					$pending_amount = html_escape($booking->pending_amount);
                    $payment_type = html_escape($booking->payment_type);
                    $balance = html_escape($booking->balance);
                    $signed = html_escape($booking->signed);
                    $cleaning_material = html_escape($booking->cleaning_material);
                    
                    $discount = html_escape($booking->discount);
                    $total_amts = html_escape($booking->total_amount);
                    $is_locked = html_escape($booking->is_locked) ? 1 : 0;
					$booking_note = html_escape($booking->booking_note) ? html_escape($booking->booking_note) : '';
					$justmop_reference=html_escape($booking->justmop_reference);
					$priceperhr=html_escape($booking->price_per_hr);
					$discountpriceperhr=html_escape($booking->discount_price_per_hr);
					$net_service_cost=html_escape($booking->net_service_cost);
					$tabletid=html_escape($booking->tabletid);
                                        
                                        
                                        
					//if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					if(preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)        
					{
						echo 'refresh2';
						exit();
					}
					
					if($booking_type != 'OD')
					{
						if($repeat_days && is_array($repeat_days) && count($repeat_days) > 0 &&  count($repeat_days) <= 7 && $repeat_end && ($repeat_end == 'never' || $repeat_end == 'ondate'))
						{
							//$repeat_days = $this->input->post('repeat_days');
							//$repeat_end = $this->input->post('repeat_end');
							
							if($repeat_end == 'ondate')
							{
								if($booking->service_actual_end_date)
								{
									//$repeat_end_date = $this->input->post('repeat_end_date');
									$repeat_end_date = $booking->service_actual_end_date;
									
									//$repeat_end_date_split = explode('/', $repeat_end_date);
									//if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
									$repeat_end_date_split = explode('-', $repeat_end_date);
									if(checkdate($repeat_end_date_split[1] , $repeat_end_date_split[2], $repeat_end_date_split[0]))
									{
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[0] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[2]);
										//$diff = $s_date->diff($e_date);
                                                                                
                                                                                $diff = abs(strtotime($repeat_end_date_split[0] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[2]) - strtotime($service_date));

                                                                                $years = floor($diff / (365*60*60*24));
                                                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
										
                                                                                if($days < 7)//if($diff->days < 7)
										{
											echo 'refresh';
											exit();
										}
										
										//$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
										$service_end_date = $repeat_end_date;
									}
									else 
									{
										echo 'refresh'.$repeat_end_date;
										exit();
									}
								}
								else
								{
									echo 'refresh';
									exit();
								}
							}
						}
						else 
						{
							echo 'refresh';
							exit();
						}
					}
					
					$time_from_stamp = (html_escape($booking->time_from));
					$time_to_stamp = (html_escape($booking->time_to));
					
					if($booking_type == 'OD')
					{
						if(isset($booked_slots[$maid_id]['time']))
						{
							foreach($booked_slots[$maid_id]['time'] as $f_time => $t_time)
							{
								if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
								{
									$return = array();
									$return['status'] = 'error';
									$return['message'] = 'The selected time slot is not available for this maid';

									echo json_encode($return);
									exit();
								}
							}
						}
					}
					
					$today_week_day = date('w', strtotime($service_date));
					
					if($booking_type == 'WE')
					{
						foreach($repeat_days as $repeat_day)
						{
							if($repeat_day < $today_week_day)
							{
								$day_diff = (6 - $today_week_day + $repeat_day + 1);
							}
							else
							{
								$day_diff = $repeat_day - $today_week_day;
							}

							$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
							foreach($bookings_on_day as $booking_on_day)
							{
								$s_date_stamp = strtotime($service_start_date);
								$e_date_stamp = strtotime($service_end_date);
								$bs_date_stamp = strtotime($booking_on_day->service_start_date);
								$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
								
								if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//latest hideif(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);

									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
									{
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
					}
                                        else if($booking_type == 'BW')
					{
                                            foreach($repeat_days as $repeat_day)
                                            {
                                                if($repeat_day < $today_week_day)
                                                {
                                                        $day_diff = (6 - $today_week_day + $repeat_day + 1);
                                                }
                                                else
                                                {
                                                        $day_diff = $repeat_day - $today_week_day;
                                                }

                                                $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

                                                //$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_new($service_start_date,$maid_id, $repeat_day);
                                                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
//                                                echo '<pre>';
//                                                print_r($bookings_on_day);
//                                                exit();
                                                foreach($bookings_on_day as $booking_on_day)
                                                {
                                                    $s_date_stamp = strtotime($service_start_date);
                                                    $e_date_stamp = strtotime($service_end_date);
                                                    $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                                    $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
                                                    
                                                    if($booking_on_day->booking_type == "BW")
                                                    {
                                                        $now = strtotime($service_start_date); // or your date as well
                                                        $your_date = strtotime($booking_on_day->service_start_date);
                                                        $datediff = $now - $your_date;

                                                        $week_diff = round($datediff / (60 * 60 * 24));
                                                        $week_difference = fmod($week_diff,14);
                                                        
                                                        if($week_difference == 0 || $week_difference == '-0')
                                                        {
                                                            $f_time = strtotime($booking_on_day->time_from);
                                                            $t_time = strtotime($booking_on_day->time_to);

                                                            if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                                            {
                                                                    $return = array();
                                                                    $return['status'] = 'error';
                                                                    $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                                                    echo json_encode($return);
                                                                    exit();
                                                            }
                                                        }
                                                    } else {
                                                        //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                        //edited by vishnu
                                                        if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                        //latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                        //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                                        {
                                                                $f_time = strtotime($booking_on_day->time_from);
                                                                $t_time = strtotime($booking_on_day->time_to);

                                                                if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                                                {
                                                                        $return = array();
                                                                        $return['status'] = 'error';
                                                                        $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                                                        echo json_encode($return);
                                                                        exit();
                                                                }
                                                        }
                                                    }
                                                }
                                            }
					}
				}
				else
				{
					echo 'refresh';
					exit();
				}
				
				$todays_new_booking = array();
				foreach($repeat_days as $repeat_day)
				{
					if($repeat_day < $today_week_day)
					{
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					}
					else
					{
						$day_diff = $repeat_day - $today_week_day;
					}
					
					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

					$booking_fields = array();
					$booking_fields['customer_id'] = $customer_id;
					$booking_fields['customer_address_id'] = $customer_address_id;
					$booking_fields['maid_id'] = $maid_id;
					$booking_fields['service_type_id'] = $service_type_id;
					$booking_fields['service_start_date'] = $service_start_date;
					$booking_fields['service_week_day'] = $repeat_day;
					$booking_fields['time_from'] = $time_from;
					$booking_fields['time_to'] = $time_to;
					$booking_fields['booking_type'] = $booking_type;
					$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
					$booking_fields['service_end_date'] = $service_end_date;					
					$booking_fields['service_actual_end_date'] = $service_end_date;					
					$booking_fields['booking_note'] = $booking_note;					
					$booking_fields['pending_amount'] = $pending_amount;                                        
					$booking_fields['discount'] = $discount;
					$booking_fields['total_amount'] = $total_amts;
					$booking_fields['booking_category'] = 'C';
					$booking_fields['booked_by'] = user_authenticate();
					$booking_fields['booking_status'] = 1;
					$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
					$booking_fields['cleaning_material'] = $cleaning_material;
					$booking_fields['justmop_reference'] = $justmop_reference;
					$booking_fields['price_per_hr'] = $priceperhr;
					$booking_fields['discount_price_per_hr'] = $discountpriceperhr;
					$booking_fields['net_service_cost'] = $net_service_cost;
					$booking_fields['tabletid'] = $tabletid;

					$check_booking_duplicate= $this->bookings_model->check_booking($booking_fields);
					
					if($check_booking_duplicate==0)
					{
						$booking_id = $this->bookings_model->add_booking($booking_fields);
					}
                                        
					
					
					if($booking_id)
					{
                                                $references_in_table=$this->justmop_model->check_ref_exist_from_date($first_booking_id,date("Y-m-d", strtotime($schedule_date)));
                                                
                                                // foreach ($references_in_table as $references) {
                                                    // $justmop_ref=array();
                                                    // $justmop_ref['booking_id']= $booking_id;
                                                    // $justmop_ref['mop']= $references->mop;
                                                    // $justmop_ref['ref']= $references->ref;
                                                    // $justmop_ref['just_mop_ref']= $references->just_mop_ref;
                                                    // $justmop_ref['service_date']= $references->service_date;
                                                    // $this->bookings_model->add_justmop_ref($justmop_ref);
                                                // }
                                                
						$booking_done = TRUE;
						
                                                //$this->appointment_bill($booking_id);
                                                
						if($service_start_date == date('Y-m-d'))
						{
							$todays_new_booking[] = $booking_id;
						}
					}
				}
				
				if(isset($booking_done))
				{
					if(count($todays_new_booking) > 0 && isset($customer_address_id))
					{
						if($tabletid > 0)
						{
							$booking = $this->bookings_model->get_booking_by_id($booking_id);
							$push_fields = array();
							$push_fields['tab_id'] = $tabletid;
							$push_fields['type'] = 1;
							$push_fields['maid_id'] = $booking->maid_id;
							$push_fields['title'] = "New Booking";
							$push_fields['customer_name'] = $booking->customer_name;
							$push_fields['maid_name'] = $booking->maid_name;
							$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
							$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
							if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
							{
								$push = $this->bookings_model->add_push_notifications($push_fields);
							}
							$tablet = $this->tablets_model->get_tablet_by_zone_tabid($tabletid);
							$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
							if($tablet)
							{
								if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
								{
									$deviceid = $tablet->google_reg_id;
									// optional payload
									$payload = array();
									$payload['isfeedback'] = false;
									if(isset($push) && $push > 0)
									{
										$payload['pushid'] = $push;
									} else {
										$payload['pushid'] = 0;
									}

									$title = "New Booking";
									$message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
									$res = array();
									$res['title'] = $title;
									$res['is_background'] = false;
									$res['body'] = $message;
									$res['image'] = "";
									$res['payload'] = $payload;
									$res['customer'] = $booking->customer_name;
									$res['maid'] = $booking->maid_name;
									$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
									$res['timestamp'] = date('Y-m-d G:i:s');
									$regId = $deviceid;
									$fields = array(
										'to' => $regId,
										'notification' => $res,
										'data' => $res,
									);
									$return = android_customer_app_push($fields);
									if($getmaiddeviceid->device_id != "")
									{
										$maid_fields = array(
											'to' => $getmaiddeviceid->device_id,
											'notification' => $res,
											'data' => $res,
										);
										android_customer_app_push($maid_fields);
									}
								}
							}
						} else {
							$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
							if(isset($c_address->zone_id))
							{
								$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
								$booking = $this->bookings_model->get_booking_by_id($booking_id);
								$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
								if($tablet)
								{
									if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
									{
										$push_fields = array();
										$push_fields['tab_id'] = $tablet->tablet_id;
										$push_fields['type'] = 1;
										$push_fields['maid_id'] = $booking->maid_id;
										$push_fields['title'] = "New Booking";
										$push_fields['customer_name'] = $booking->customer_name;
										$push_fields['maid_name'] = $booking->maid_name;
										$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
										$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
										$push = $this->bookings_model->add_push_notifications($push_fields);
										$deviceid = $tablet->google_reg_id;
										// optional payload
										$payload = array();
										$payload['isfeedback'] = false;
										if(isset($push) && $push > 0)
										{
											$payload['pushid'] = $push;
										} else {
											$payload['pushid'] = 0;
										}

										$title = "New Booking";
										$message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
										$res = array();
										$res['title'] = $title;
										$res['is_background'] = false;
										$res['body'] = $message;
										$res['image'] = "";
										$res['payload'] = $payload;
										$res['customer'] = $booking->customer_name;
																$res['maid'] = $booking->maid_name;
																$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
										$res['timestamp'] = date('Y-m-d G:i:s');
										$regId = $deviceid;
										$fields = array(
											'to' => $regId,
											'notification' => $res,
											'data' => $res,
										);
										//$return = android_customer_push($fields);
										//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking'));
										$return = android_customer_app_push($fields);
										if($getmaiddeviceid->device_id != "")
										{
											$maid_fields = array(
												'to' => $getmaiddeviceid->device_id,
												'notification' => $res,
												'data' => $res,
											);
											android_customer_app_push($maid_fields);
										}
									}
								}
							}
						}
					}
					
					$return = array();
					$return['status'] = 'success';
					$return['maid_id'] = $maid_id;
					$return['customer_id'] = $customer_id;
					$return['time_from'] = $time_from;
					$return['time_to'] = $time_to;
					
					echo json_encode($return);
					exit();
				}
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'transfer-driver')
			{
				if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 )
				{
					$booking = $this->bookings_model->get_booking_by_id($this->input->post('booking_id'));
					$users_id = user_authenticate();
					if(user_authenticate() != 1)
					{
						if($booking->is_locked == 1 && $booking->booked_by != $users_id)
						{
							echo 'locked';
							exit();
						}
					}
					$service_dates = $this->input->post('service_date');
					$service_date_new = date('Y-m-d', strtotime($service_dates));
					$check_booking_transferred = $this->bookings_model->check_booking_transferred_new($this->input->post('booking_id'),$service_date_new);
					if(count($check_booking_transferred) > 0)
					{
						$transferids = $check_booking_transferred->booking_transfer_tablet_id;
					} else {
						$transferids = 0;
					}
					$datetimes = date('Y-m-d H:i:s');
					//$dateservice = $this->input->post('dateservice');
					$toid = $this->input->post('to_tablet_id');
					$driver_chng_array = array();
					$driver_chng_array['booking_id'] = $this->input->post('booking_id');
					$driver_chng_array['transfering_to_tablet'] = $toid;
					$driver_chng_array['service_date'] = $service_date_new;
					$driver_chng_array['transferred_date_time'] = $datetimes;
					$driver_chng_array['transferred_from'] = 'B';
					$driver_chng_array['transferred_from_id'] = $this->input->post('cur_tablet_id');
					$driver_chng_array['transferred_by'] = $users_id;
					$activityid = $this->bookings_model->insert_driverchnage($driver_chng_array,$transferids);
					if($activityid > 0)
					{
						if($toid > 0)
						{
							$push_fields = array();
							$push_fields['tab_id'] = $toid;
							$push_fields['type'] = 1;
							$push_fields['maid_id'] = $booking->maid_id;
							$push_fields['message'] = 'Booking transferred. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
							$push_fields['title'] = "Booking Transfered";
							$push_fields['customer_name'] = $booking->customer_name;
							$push_fields['maid_name'] = $booking->maid_name;
							$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
							
							if(strtotime($service_dates) <= strtotime( date('d-M-Y')))
							{
								$push = $this->bookings_model->add_push_notifications($push_fields);
							}
							$tablet = $this->tablets_model->get_tablet_by_zone_tabid($toid);
							$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
							if($tablet)
							{
								if(strtotime($service_dates) <= strtotime( date('d-M-Y')))
								{
									$deviceid = $tablet->google_reg_id;
									// optional payload
									$payload = array();
									$payload['isfeedback'] = false;
									if(isset($push) && $push > 0)
									{
										$payload['pushid'] = $push;
									} else {
										$payload['pushid'] = 0;
									}


									$title = "Booking Transfered";
									$message = 'Booking transferred. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
									$res = array();
									$res['title'] = $title;
									$res['is_background'] = false;
									$res['body'] = $message;
									$res['image'] = "";
									$res['payload'] = $payload;
									$res['customer'] = $booking->customer_name;
									$res['maid'] = $booking->maid_name;
									$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
									$res['timestamp'] = date('Y-m-d G:i:s');
									$regId = $deviceid;
									$fields = array(
										'to' => $regId,
										'notification' => $res,
										'data' => $res,
									);
									$return = android_customer_app_push($fields);
									if($getmaiddeviceid->device_id != "")
									{
										$maid_fields = array(
											'to' => $getmaiddeviceid->device_id,
											'notification' => $res,
											'data' => $res,
										);
										android_customer_app_push($maid_fields);
									}
								}
							}
						}
						echo 'success';
						exit();
					} else {
						echo 'error';
						exit();
					}
				}
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'update-driver-to-booking')
			{
				if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 )
				{
					$booking_id = $this->input->post('booking_id');
					$tablet_id = $this->input->post('tablet_id');
					$booking = $this->bookings_model->get_booking_by_id($booking_id);
					$users_id = user_authenticate();
					if(user_authenticate() != 1)
					{
						if($booking->is_locked == 1 && $booking->booked_by != $users_id)
						{
							echo 'locked';
							exit();
						}
					}
					$update_fields = array();
					$update_fields['tabletid'] = $tablet_id;
					$update_book = $this->bookings_model->update_booking($booking_id, $update_fields);
					
					$service_dates = $this->input->post('servicedate');
					$service_date_new = date('Y-m-d', strtotime($service_dates));
					
					if($update_book)
					{
						if($tablet_id > 0)
						{
							$push_fields = array();
							$push_fields['tab_id'] = $tablet_id;
							$push_fields['type'] = 1;
							$push_fields['message'] = 'Booking transferred. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
							$push_fields['title'] = "Booking Transfered";
							$push_fields['customer_name'] = $booking->customer_name;
							$push_fields['maid_name'] = $booking->maid_name;
							$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
							if(strtotime($service_date_new) <= strtotime( date('Y-m-d')))
							{
								$push = $this->bookings_model->add_push_notifications($push_fields);
							}
							$tablet = $this->tablets_model->get_tablet_by_zone_tabid($tablet_id);
							if($tablet)
							{
								if(strtotime($service_date_new) <= strtotime( date('Y-m-d')))
								{
									$deviceid = $tablet->google_reg_id;
									// optional payload
									$payload = array();
									$payload['isfeedback'] = false;
									if(isset($push) && $push > 0)
									{
										$payload['pushid'] = $push;
									} else {
										$payload['pushid'] = 0;
									}

									$title = "Booking Transfered";
									$message = 'Booking transferred. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
									$res = array();
									$res['title'] = $title;
									$res['is_background'] = false;
									$res['body'] = $message;
									$res['image'] = "";
									$res['payload'] = $payload;
									$res['customer'] = $booking->customer_name;
									$res['maid'] = $booking->maid_name;
									$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
									$res['timestamp'] = date('Y-m-d G:i:s');
									$regId = $deviceid;
									$fields = array(
										'to' => $regId,
										'notification' => $res,
										'data' => $res,
									);
									$return = android_customer_app_push($fields);
								}
								$response = array();
								$response['status'] = 'success';
								$response['tablet_id'] = $tablet_id;
								$response['driver_name'] = $tablet->tablet_driver_name;
								echo json_encode($response);
								exit();
							}
						}
					} else {
						echo 'error';
						exit();
					}
				} else {
					echo 'error';
					exit();
				}
			}
                        // End
			if($this->input->post('action') && $this->input->post('action') == 'update-booking')
			{
				if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('update_type') && ($this->input->post('update_type') == 'permanent' || $this->input->post('update_type') == 'one-day'))
				{
					$booking_id =trim($this->input->post('booking_id'));
					$booking_idnewww =trim($this->input->post('booking_id'));
					$get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_idnewww,$service_date);
					if(count($get_dayservice_details) > 0)
					{
						if($get_dayservice_details->odoo_package_activity_status == 1)
						{
							echo 'odoorefresh';
							exit();
						}
					}
                                        $update_date = date("Y-m-d");
					$update_type = trim($this->input->post('update_type'));
					
					$time_from =  date('H:i', trim($this->input->post('time_from')));
					$time_to = date('H:i', trim($this->input->post('time_to')));
					$is_locked = $this->input->post('is_locked') ? 1 : 0;
					$cleaning_material = $this->input->post('cleaning_material');
					$pending_amount = $this->input->post('pending_amount');
					$discount = $this->input->post('discount');
					$priceperhr = $this->input->post('priceperhr');
					$discountpriceperhr = $this->input->post('discountpriceperhr');
					$netamtcost = $this->input->post('netamtcost');
					$total_amt = $this->input->post('total_amt');
					$booking_note = $this->input->post('booking_note') ? trim($this->input->post('booking_note')) : '';
					$email_notifications = $this->input->post('email_notifications');
					$sms_notifications = $this->input->post('sms_notifications');
					$new_payment_mode = $this->input->post('payment_mode');
					$reference_no = $this->input->post('reference_no');
					//$tabletiddval = $this->input->post('tabletidval');
					//$activetabletiddval = $this->input->post('hiddentabletidval');
                                        
					//if(preg_match("/([01]?[0-9]|2[0-3]):[0][0]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					if(preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
					{
						echo 'refresh';
						exit();
					}
					
					$booking = $this->bookings_model->get_booking_by_id($booking_id);
                                        
                                        //Email and SMS Notifications
                                        if($booking->booking_type == "WE"){
                                            $last_sunday = strtotime('last Sunday');
                                            $service_start_date_for_not = date('l', strtotime('+'.$booking->service_week_day.' day', $last_sunday));
                                        } else if($booking->booking_type == "OD"){
                                            $service_start_date_for_not = date('d/m/Y', strtotime($booking->service_start_date));
                                        }
                                        $no_of_hourss = $this->get_time_difference($time_from, $time_to);
                                        $time_from_not =  date('g:i A', strtotime(html_escape($time_from)));
                                        //$booking_not = $this->bookings_model->get_booking_by_id($booking_id);
                                        $email_address_not = $booking->email_address;
                                        $mobile = $booking->mobile_number_1;
                                        $content = "Thanks for booking with us! Your scheduled booking on ".$service_start_date_for_not." for ".$no_of_hourss." hrs  from ".$time_from_not." has been updated.There might be chance of delay in dropping the crew due to the traffic, thanks for understanding in advance.";
                                        $contentEmail = "Your scheduled booking on ".$service_start_date_for_not." for ".$no_of_hourss." hrs  from ".$time_from_not." has been updated.There might be chance of delay in dropping the crew due to the traffic, thanks for understanding in advance.";
                                        if($email_notifications == "Y")
                                        { 
                                            if($email_address_not!=""){ 
                                            $this->send_email_notifications($contentEmail,$email_address_not);
                                            $action_type="email_notification";
                                            $action_by=(user_authenticate()==1 ? 'Admin User':'User');
                                            $notify_content="Email has been sent to ". $email_address_not . " regarding job by " . $action_by;
                                            $data_activity = array();
                                            $data['added_user'] = user_authenticate();
                                            $data['booking_type'] = $booking->booking_type == 'OD' ? 'One Day' : 'Every Week';
                                            $data['shift'] = $booking->time_from . '-' . $booking->time_to;
                                            $data['action_type'] = $action_type;
                                            $data['action_content'] = $notify_content;
                                            $data['addeddate'] = date('Y-m-d H:i:s');
                                            $this->db->set($data);
		                            $this->db->insert('user_activity');
                                            }
                                        }
                                        if($sms_notifications == 'Y')
                                        {
											//hided for crystal blu
                                            $sms_send = $this->send_sms_notifications($content,$mobile);
                                            $action_type="sms_notification";
                                            $action_by=(user_authenticate()==1 ? 'Admin User':'User');
                                            $notify_content="SMS has been sent to ". $mobile . " regarding job by " . $action_by;
                                            $data_activity = array();
                                            $data['added_user'] = user_authenticate();
                                            $data['booking_type'] = $booking->booking_type == 'OD' ? 'One Day' : 'Every Week';
                                            $data['shift'] = $booking->time_from . '-' . $booking->time_to;
                                            $data['action_type'] = $action_type;
                                            $data['action_content'] = $notify_content;
                                            $data['addeddate'] = date('Y-m-d H:i:s');
                                            $this->db->set($data);
		                                    $this->db->insert('user_activity');
									//ends
                                        }
                                        
                                        //ends
                                        
                                        
                                        
                                        if(user_authenticate() != 1){
                                        if($booking->is_locked == 1 && $booking->booked_by != user_authenticate())
                                        {
                                            echo 'locked';
                                            exit();
                                        }
										}
					/*if(!isset($booking->booking_id) || ($booking->service_end == 1 && strtotime($booking->service_actual_end_date) < strtotime(date('Y-m-d'))))
					{
						echo 'refresh';
						exit();
					}*/
					
					if(is_numeric(trim($this->input->post('customer_address_id'))) && trim($this->input->post('customer_address_id')) > 0 && $booking->customer_address_id != trim($this->input->post('customer_address_id')))
					{
						$customer_address_id = trim($this->input->post('customer_address_id'));
					}
					else
					{
						$customer_address_id = $booking->customer_address_id;
					}
					
					$time_from_stamp = trim($this->input->post('time_from'));
					$time_to_stamp = trim($this->input->post('time_to'));
			
					if($booking->booking_type == 'OD')
					{
						
						if(isset($booked_slots[$booking->maid_id]['time']))
						{
							foreach($booked_slots[$booking->maid_id]['time'] as $f_time => $t_time)
							{
								if(strtotime($booking->time_from) != $f_time && strtotime($booking->time_to) != $t_time)
								{
									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
									{
										$return = array();
										$return['status'] = 'error';
										$return['message'] = 'The selected time slot is not available for this maid';

										echo json_encode($return);
										exit();
									}
								}
							}
						}
						
						$update_fields = array();
						$update_fields['customer_address_id'] = $customer_address_id;
						$update_fields['time_from'] = $time_from;
						$update_fields['time_to'] = $time_to;
						$update_fields['pending_amount'] = $pending_amount;
						$update_fields['price_per_hr'] = $priceperhr;
						$update_fields['discount_price_per_hr'] = $discountpriceperhr;
						$update_fields['net_service_cost'] = $netamtcost;
						$update_fields['discount'] = $discount;
						$update_fields['total_amount'] = $total_amt;
						$update_fields['is_locked'] = $is_locked;
						$update_fields['cleaning_material'] = $cleaning_material;
						$update_fields['pay_by'] = $this->input->post("payment_mode");						
						$update_fields['booking_note'] = $booking_note;
						//$update_fields['tabletid'] = $tabletiddval;
					
						$this->bookings_model->update_booking($booking_id, $update_fields);
						
						// if($tabletiddval != $activetabletiddval)
						// {
							// $datetimes = date('Y-m-d H:i:s');
							// $driver_chng_array = array();
							// $driver_chng_array['booking_id'] = $booking_id;
							// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
							// $driver_chng_array['service_date'] = $service_date;
							// $driver_chng_array['transferred_date_time'] = $datetimes;
							// $driver_chng_array['transferred_from'] = 'B';
							// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
							// $driver_chng_array['transferred_by'] = user_authenticate();
							// $this->bookings_model->insert_driverchnage($driver_chng_array,'One Day');
						// }
						
						if(count($get_dayservice_details) > 0)
						{
							if($get_dayservice_details->odoo_package_activity_status == 1)
							{
								echo 'odoorefresh';
								exit();
							} else {
								$updatearry = array();
								$updatearry['total_fee'] = $total_amt;
								$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
							}
						}
						
						// $justmopfields=array();
						// $justmopfields['booking_id']=$booking_id;
						// $justmopfields['mop']=ucfirst($this->input->post("payment_mode"));
						// $justmopfields['just_mop_ref']=$reference_no;
						// $justmopfields['service_date']=$booking->service_start_date;
						$payment_mode=$this->bookings_model->get_payment_mode($booking_id,$booking->service_start_date);
						if(!empty($payment_mode)){
						//$this->bookings_model->update_payment_justmop($booking_id,$booking->service_start_date,$justmopfields);
						}
						else{
						//$this->bookings_model->add_justmop_ref($justmopfields);	
						}
						if($booking->service_start_date == date('Y-m-d'))
						{
							if($booking->tabletid > 0)
							{
								$tablet = $this->tablets_model->get_tablet_by_zone_tabid($booking->tabletid);
								if($tablet)
								{
									$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
									$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
									$push_fields = array();
									$push_fields['tab_id'] = $tablet->tablet_id;
									$push_fields['type'] = 5;
									$push_fields['maid_id'] = $upd_booking->maid_id;
									$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
									$push_fields['title'] = "Booking Updated.";
									$push_fields['customer_name'] = $upd_booking->customer_name;
									$push_fields['maid_name'] = $upd_booking->maid_name;
									$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
									if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
										$push = $this->bookings_model->add_push_notifications($push_fields);
									
									if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
									{
										$deviceid = $tablet->google_reg_id;
										$payload = array();
										$payload['isfeedback'] = false;
										if(isset($push) && $push > 0)
										{
											$payload['pushid'] = $push;
										} else {
											$payload['pushid'] = 0;
										}

										$title = "Booking Updated";
										$message = $push_fields['message'];
										$res = array();
										$res['title'] = $title;
										$res['is_background'] = false;
										$res['body'] = $message;
										$res['image'] = "";
										$res['payload'] = $payload;
										$res['customer'] = $upd_booking->customer_name;
										$res['maid'] = $upd_booking->maid_name;
										$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
										$res['timestamp'] = date('Y-m-d G:i:s');
										$regId = $deviceid;
										$fields = array(
											'to' => $regId,
											'notification' => $res,
											'data' => $res,
										);
										$return = android_customer_app_push($fields);
										if($getmaiddeviceid->device_id != "")
										{
											$maid_fields = array(
												'to' => $getmaiddeviceid->device_id,
												'notification' => $res,
												'data' => $res,
											);
											android_customer_app_push($maid_fields);
										}
										//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
									}
								}
							} else {
								$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
								if(empty($transfered_booking))
								{                 
									$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
									$zoneID=$c_address->zone_id;
								} else {
									$zoneID= $transfered_booking->zone_id;	
								}
								if(isset($zoneID) &&  $zoneID!="")
								{
									$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
									if($tablet)
									{
										$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
										$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
										$push_fields = array();
										$push_fields['tab_id'] = $tablet->tablet_id;
										$push_fields['type'] = 5;
										$push_fields['maid_id'] = $upd_booking->maid_id;
										$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
										$push_fields['title'] = "Booking Updated.";
										$push_fields['customer_name'] = $upd_booking->customer_name;
										$push_fields['maid_name'] = $upd_booking->maid_name;
										$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
										if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
											$push = $this->bookings_model->add_push_notifications($push_fields);
										
										if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
										{
											$deviceid = $tablet->google_reg_id;
											$payload = array();
											$payload['isfeedback'] = false;
											if(isset($push) && $push > 0)
											{
												$payload['pushid'] = $push;
											} else {
												$payload['pushid'] = 0;
											}

											$title = "Booking Updated";
											$message = $push_fields['message'];
											$res = array();
											$res['title'] = $title;
											$res['is_background'] = false;
											$res['body'] = $message;
											$res['image'] = "";
											$res['payload'] = $payload;
											$res['customer'] = $upd_booking->customer_name;
											$res['maid'] = $upd_booking->maid_name;
											$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
											$res['timestamp'] = date('Y-m-d G:i:s');
											$regId = $deviceid;
											$fields = array(
												'to' => $regId,
												'notification' => $res,
												'data' => $res,
											);
											$return = android_customer_app_push($fields);
											if($getmaiddeviceid->device_id != "")
											{
												$maid_fields = array(
													'to' => $getmaiddeviceid->device_id,
													'notification' => $res,
													'data' => $res,
												);
												android_customer_app_push($maid_fields);
											}
											//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
										}
									}
								}
							}
                            
						}
						
						$return = array();
						$return['status'] = 'success';
						$return['maid_id'] = $booking->maid_id;
						$return['customer_id'] = $booking->customer_id;
						$return['time_from'] = $time_from;
						$return['time_to'] = $time_to;

						echo json_encode($return);
						exit();
					}
					else
					{
                                          
						if($this->input->post('repeat_end') && (trim($this->input->post('repeat_end')) == 'never' ||  trim($this->input->post('repeat_end')) == 'ondate'))
						{
							$repeat_end = $this->input->post('repeat_end');
							
							if($repeat_end == 'ondate')
							{
								if($this->input->post('repeat_end_date'))
								{
									$repeat_end_date = $this->input->post('repeat_end_date');
									
									$repeat_end_date_split = explode('/', $repeat_end_date);
									if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
									{
										$s_date = new DateTime($service_date);
										$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
										//$diff = $s_date->diff($e_date);
										
                                                                                $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                                                                $years = floor($diff / (365*60*60*24));
                                                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
										if($days < 0)//if($diff->days < 0)
										{
											echo 'refresh';
											exit();
										}
										
										$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
									}
									else 
									{
										echo 'refresh';
										exit();
									}
								}
								else
								{
									echo 'refresh';
									exit();
								}
							}
							else
							{
								$service_end_date = $service_date;
							}
														
							$today_week_day = date('w', strtotime($service_date));
							$service_start_date = $service_date;
					
							if($booking->booking_type == 'WE')
							{
                                                           
								//$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($booking->maid_id, $today_week_day);
                                                                $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_dayupdate($booking->maid_id, $today_week_day, $update_date);
								foreach($bookings_on_day as $booking_on_day)
								{
									if($booking_on_day->booking_id != $booking->booking_id)
									{
										$s_date_stamp = strtotime($service_start_date);
										$e_date_stamp = strtotime($service_end_date);
										$bs_date_stamp = strtotime($booking_on_day->service_start_date);
										$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
										
										if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										//if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										//latest hideif(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
										{
											$f_time = strtotime($booking_on_day->time_from);
											$t_time = strtotime($booking_on_day->time_to);

											if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
											{
												$return = array();
												$return['status'] = 'error';
												$return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$today_week_day] . 's';

												echo json_encode($return);
												exit();
											}
										}
									}
								}
								
								if($update_type == 'permanent')
								{
                                                                   
									$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;
									
									// End current booking and add new booking if address id or time changes
									if($booking->customer_address_id != $customer_address_id || $booking->time_from != $time_from || $booking->time_to != $time_to)
									{
										// End current booking
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($service_start_date . ' - 1 day')), 'service_end' => 1));

										$booking_fields = array();
										$booking_fields['customer_id'] = $booking->customer_id;
										$booking_fields['customer_address_id'] = $customer_address_id;
										$booking_fields['maid_id'] = $booking->maid_id;
										$booking_fields['service_type_id'] = $booking->service_type_id;
										$booking_fields['service_start_date'] = $service_start_date;
										$booking_fields['service_week_day'] = $today_week_day;
										$booking_fields['time_from'] = $time_from;
										$booking_fields['time_to'] = $time_to;
										$booking_fields['booking_type'] = $booking->booking_type;
                                                                                //$booking_fields['cleaning_material'] = $booking->cleaning_material;
										$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$booking_fields['service_end_date'] = $service_end_date;				
										$booking_fields['booking_note'] = $booking_note;
                                                                                $booking_fields['is_locked'] = $is_locked;
                                                                                $booking_fields['cleaning_material'] = $cleaning_material;
										$booking_fields['pending_amount'] = $pending_amount;
										$booking_fields['total_amount'] = $total_amt;
										$booking_fields['price_per_hr'] = $priceperhr;
										$booking_fields['discount_price_per_hr'] = $discountpriceperhr;
										$booking_fields['net_service_cost'] = $netamtcost;
										$booking_fields['discount'] = $discount;
										$booking_fields['booked_by'] = user_authenticate();
										$booking_fields['booking_status'] = 1;
										$booking_fields['pay_by'] = $this->input->post("payment_mode");
										$booking_fields['booking_category'] = 'C';
										$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
										$booking_fields['tabletid'] = $booking->tabletid;

										$booking_id = $this->bookings_model->add_booking($booking_fields);
										
										if($booking_id)
										{
											if(count($get_dayservice_details) > 0)
											{
												if($get_dayservice_details->odoo_package_activity_status == 1)
												{
													echo 'odoorefresh';
													exit();
												} else {
													$updatearry = array();
													$updatearry['total_fee'] = $total_amt;
													$updatearry['booking_id'] = $booking_id;
													$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
												}
											}
											
											// if($tabletiddval != $activetabletiddval)
											// {
												// $datetimes = date('Y-m-d H:i:s');
												// $driver_chng_array = array();
												// $driver_chng_array['booking_id'] = $booking_id;
												// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
												// $driver_chng_array['service_date'] = $service_date;
												// $driver_chng_array['transferred_date_time'] = $datetimes;
												// $driver_chng_array['transferred_from'] = 'B';
												// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
												// $driver_chng_array['transferred_by'] = user_authenticate();
												// $this->bookings_model->insert_driverchnage($driver_chng_array,'One Day');
											// }
											// $justmop=array();
											// $justmop['booking_id']=$booking_id;
											// $justmop['mop']=ucfirst($this->input->post('payment_mode'));
											// $justmop['service_date']=$service_start_date;
											// $justmop['just_mop_ref']=$reference_no;
											// $this->bookings_model->add_justmop_ref($justmop);
											if($service_start_date == date('Y-m-d'))
											{
												if($booking->tabletid > 0)
												{
													$tablet = $this->tablets_model->get_tablet_by_zone_tabid($booking->tabletid);
													if($tablet)
													{
														$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
														$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 5;
														$push_fields['maid_id'] = $upd_booking->maid_id;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $upd_booking->customer_name;
														$push_fields['maid_name'] = $upd_booking->maid_name;
														$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;
															if(isset($push) && $push > 0)
															{
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['title'] = $title;
															$res['is_background'] = false;
															$res['body'] = $message;
															$res['image'] = "";
															$res['payload'] = $payload;
															$res['customer'] = $upd_booking->customer_name;
															$res['maid'] = $upd_booking->maid_name;
															$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
															$res['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'notification' => $res,
																'data' => $res,
															);
															$return = android_customer_app_push($fields);
															if($getmaiddeviceid->device_id != "")
															{
																$maid_fields = array(
																	'to' => $getmaiddeviceid->device_id,
																	'notification' => $res,
																	'data' => $res,
																);
																android_customer_app_push($maid_fields);
															}
															//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
													}
												} else {
													$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
													if(empty($transfered_booking)){ 
													$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
													$zoneID=$c_address->zone_id;
													}
													else{
													$zoneID= $transfered_booking->zone_id;	
													  }
								
													if(isset($zoneID) &&  $zoneID!="")
													{
														$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
														if($tablet)
														{
															$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
															$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
															$push_fields = array();
															$push_fields['tab_id'] = $tablet->tablet_id;
															$push_fields['type'] = 5;
															$push_fields['maid_id'] = $upd_booking->maid_id;
															$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
															$push_fields['title'] = "Booking Updated";
															$push_fields['customer_name'] = $upd_booking->customer_name;
															$push_fields['maid_name'] = $upd_booking->maid_name;
															$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
															if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
																$push = $this->bookings_model->add_push_notifications($push_fields);
															if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															{
																$deviceid = $tablet->google_reg_id;
																// optional payload
																$payload = array();
																$payload['isfeedback'] = false;
																if(isset($push) && $push > 0)
																{
																	$payload['pushid'] = $push;
																} else {
																	$payload['pushid'] = 0;
																}

																$title = "Booking Updated";
																$message = $push_fields['message'];
																$res = array();
																$res['title'] = $title;
																$res['is_background'] = false;
																$res['body'] = $message;
																$res['image'] = "";
																$res['payload'] = $payload;
																$res['customer'] = $upd_booking->customer_name;
																$res['maid'] = $upd_booking->maid_name;
																$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
																$res['timestamp'] = date('Y-m-d G:i:s');
																$regId = $deviceid;
																$fields = array(
																	'to' => $regId,
																	'notification' => $res,
																	'data' => $res,
																);
																$return = android_customer_app_push($fields);
																if($getmaiddeviceid->device_id != "")
																{
																	$maid_fields = array(
																		'to' => $getmaiddeviceid->device_id,
																		'notification' => $res,
																		'data' => $res,
																	);
																	android_customer_app_push($maid_fields);
																}
																//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
															}
															$booking = $this->bookings_model->get_booking_by_id($booking_id);
															
															
														}
													}
												
												}
											}
					
											$return = array();
											$return['status'] = 'success';
											$return['maid_id'] = $booking->maid_id;
											$return['customer_id'] = $booking->customer_id;
											$return['time_from'] = $time_from;
											$return['time_to'] = $time_to;

											echo json_encode($return);
											exit();
										}
									}
									else
									{
                                                                            
										$update_fields = array();
										$update_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$update_fields['service_end_date'] = $service_end_date;
										$update_fields['service_actual_end_date'] = $service_end_date;
										$update_fields['pending_amount'] = $pending_amount;
										$update_fields['total_amount'] = $total_amt;
										$update_fields['discount'] = $discount;
										$update_fields['price_per_hr'] = $priceperhr;
										$update_fields['discount_price_per_hr'] = $discountpriceperhr;
										$update_fields['net_service_cost'] = $netamtcost;
										$update_fields['is_locked'] = $is_locked;
										$update_fields['cleaning_material'] = $cleaning_material;
										$update_fields['booking_note'] = $booking_note;
										//$update_fields['tabletid'] = $tabletiddval;

										$this->bookings_model->update_booking($booking_id, $update_fields);
										
										if(count($get_dayservice_details) > 0)
										{
											if($get_dayservice_details->odoo_package_activity_status == 1)
											{
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										
										// if($tabletiddval != $activetabletiddval)
										// {
											// $datetimes = date('Y-m-d H:i:s');
											// $driver_chng_array = array();
											// $driver_chng_array['booking_id'] = $booking_id;
											// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
											// $driver_chng_array['service_date'] = $service_date;
											// $driver_chng_array['transferred_date_time'] = $datetimes;
											// $driver_chng_array['transferred_from'] = 'B';
											// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
											// $driver_chng_array['transferred_by'] = user_authenticate();
											// $this->bookings_model->insert_driverchnage($driver_chng_array,'Every Week');
										// }
										
										$get_book_detail_new = $this->bookings_model->get_booking_by_id($booking_id);
										
										$payment_mode_new = $this->bookings_model->get_payment_mode($booking_id,$get_book_detail_new->service_start_date);
										
										// $justmopfields=array();
										// $justmopfields['booking_id']=$booking_id;
										// $justmopfields['mop']=ucfirst($this->input->post('payment_mode'));
										// $justmopfields['service_date']=$get_book_detail_new->service_start_date;
										// $justmopfields['just_mop_ref']=$reference_no;
										
										if(!empty($payment_mode_new)){
										//$this->bookings_model->update_payment_justmop($booking_id,$get_book_detail_new->service_start_date,$justmopfields);
										}
										else{
										//$this->bookings_model->add_justmop_ref($justmopfields);	
										}
										
										if($booking->service_start_date == date('Y-m-d'))
										{
											if($get_book_detail_new->tabletid > 0)
											{
												$tablet = $this->tablets_model->get_tablet_by_zone_tabid($get_book_detail_new->tabletid);
												if($tablet)
												{
													$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
													$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 5;
													$push_fields['maid_id'] = $upd_booking->maid_id;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $upd_booking->customer_name;
													$push_fields['maid_name'] = $upd_booking->maid_name;
													$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														$push = $this->bookings_model->add_push_notifications($push_fields);
													
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
													{
														$deviceid = $tablet->google_reg_id;
														// optional payload
														$payload = array();
														$payload['isfeedback'] = false;
														if(isset($push) && $push > 0)
														{
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}


														$title = "Booking Updated";
														$message = $push_fields['message'];
														$res = array();
														$res['title'] = $title;
														$res['is_background'] = false;
														$res['body'] = $message;
														$res['image'] = "";
														$res['payload'] = $payload;
														$res['customer'] = $upd_booking->customer_name;
														$res['maid'] = $upd_booking->maid_name;
														$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
														$res['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'notification' => $res,
															'data' => $res,
														);
														$return = android_customer_app_push($fields);
														if($getmaiddeviceid->device_id != "")
														{
															$maid_fields = array(
																'to' => $getmaiddeviceid->device_id,
																'notification' => $res,
																'data' => $res,
															);
															android_customer_app_push($maid_fields);
														}
														//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
													}
													$booking = $this->bookings_model->get_booking_by_id($booking_id);
												}
											} else {
												$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
												if(empty($transfered_booking)){ 
													$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
													$zoneID=$c_address->zone_id;
												} else {
													$zoneID= $transfered_booking->zone_id;	
												}
												if(isset($zoneID) &&  $zoneID!="")
												{
													$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
													if($tablet)
													{
														$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
														$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 5;
														$push_fields['maid_id'] = $upd_booking->maid_id;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $upd_booking->customer_name;
														$push_fields['maid_name'] = $upd_booking->maid_name;
														$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;
															if(isset($push) && $push > 0)
															{
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['title'] = $title;
															$res['is_background'] = false;
															$res['body'] = $message;
															$res['image'] = "";
															$res['payload'] = $payload;
															$res['customer'] = $upd_booking->customer_name;
															$res['maid'] = $upd_booking->maid_name;
															$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
															$res['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'notification' => $res,
																'data' => $res,
															);
															$return = android_customer_app_push($fields);
															if($getmaiddeviceid->device_id != "")
															{
																$maid_fields = array(
																	'to' => $getmaiddeviceid->device_id,
																	'notification' => $res,
																	'data' => $res,
																);
																android_customer_app_push($maid_fields);
															}
															//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
													}
												}
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
								else if($update_type == 'one-day')
								{
                                                                    
                                                            
                                                                     
									/* Delete booking one day */
									$delete_b_fields = array();
									$delete_b_fields['booking_id'] = $booking->booking_id;
									$delete_b_fields['service_date'] = $service_date;
                                                                        $delete_b_fields['deleted_by'] = user_authenticate();

									$this->bookings_model->add_booking_delete($delete_b_fields);
									
									/* Add one day booking */
									$booking_fields = array();
									$booking_fields['customer_id'] = $booking->customer_id;
									$booking_fields['customer_address_id'] = $customer_address_id;
									$booking_fields['maid_id'] = $booking->maid_id;
									$booking_fields['service_type_id'] = $booking->service_type_id;
									$booking_fields['service_start_date'] = $service_date;
									$booking_fields['service_week_day'] = $today_week_day;
									$booking_fields['time_from'] = $time_from;
									$booking_fields['time_to'] = $time_to;
									$booking_fields['booking_type'] = 'OD';
									$booking_fields['service_end'] = 1;
									$booking_fields['service_end_date'] = $service_date;				
									$booking_fields['booking_note'] = $booking_note;
									//$booking_fields['cleaning_material'] = $booking->cleaning_material;
									$booking_fields['is_locked'] = $is_locked;
									$booking_fields['cleaning_material'] = $cleaning_material;
									$booking_fields['pending_amount'] = $pending_amount;
									$booking_fields['total_amount'] = $total_amt;
									$booking_fields['discount'] = $discount;
									$booking_fields['price_per_hr'] = $priceperhr;
									$booking_fields['discount_price_per_hr'] = $discountpriceperhr;
									$booking_fields['net_service_cost'] = $netamtcost;
									$booking_fields['booking_category'] = 'C';
									$booking_fields['booked_by'] = user_authenticate();
									$booking_fields['booking_status'] = 1;
									$booking_fields['pay_by'] = $this->input->post('payment_mode');
									$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
									$booking_fields['tabletid'] = $booking->tabletid;

									$booking_id = $this->bookings_model->add_booking($booking_fields);
                                                                          
                                                                                       
									if($booking_id)
									{
										if(count($get_dayservice_details) > 0)
										{
											if($get_dayservice_details->odoo_package_activity_status == 1)
											{
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												$updatearry['booking_id'] = $booking_id;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										
										// if($tabletiddval != $activetabletiddval)
										// {
											// $datetimes = date('Y-m-d H:i:s');
											// $driver_chng_array = array();
											// $driver_chng_array['booking_id'] = $booking_id;
											// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
											// $driver_chng_array['service_date'] = $service_date;
											// $driver_chng_array['transferred_date_time'] = $datetimes;
											// $driver_chng_array['transferred_from'] = 'B';
											// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
											// $driver_chng_array['transferred_by'] = user_authenticate();
											// $this->bookings_model->insert_driverchnage($driver_chng_array,'One Day');
										// }
										// $justmop=array();
										// $justmop['booking_id']=$booking_id;
										// $justmop['mop']=ucfirst($this->input->post('payment_mode'));
										// $justmop['service_date']=$service_date;
										// $justmop['just_mop_ref']=$reference_no;
                                                                                // $this->bookings_model->add_justmop_ref($justmop);
										if($service_date == date('Y-m-d'))
										{
										   // Edited by sandeep
										
										   /* check bookingID in dayservice table and update new bookingId    */
										
											$day_service_row = $this->bookings_model->check_day_service_booking($booking->booking_id);
											
											if($day_service_row)
											{  
											   $day_service_id= $day_service_row[0]->day_service_id;
											   
											   $day_service_fields = array(); 
											   
											   $day_service_fields['booking_id']=$booking_id;
										 
											   //$day_service_fields['start_time']=$time_from;
											   
											   //$day_service_fields['end_time']=$time_to;
											   
											   $day_service_fields['total_fee']=$this->get_price_booking($booking_id);
											   
											   $this->bookings_model->update_day_service_booking($day_service_id,$day_service_fields);
												
											}
											
											if($booking->tabletid > 0)
											{
												$tablet = $this->tablets_model->get_tablet_by_zone_tabid($booking->tabletid);
												$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
												if($tablet)
												{
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 3;
													$push_fields['maid_id'] = $booking->maid_id;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A",strtotime($time_from)) . '-' . date("h:i A",strtotime($time_to));
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $booking->customer_name;
													$push_fields['maid_name'] = $booking->maid_name;
													$push_fields['booking_time'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														$push = $this->bookings_model->add_push_notifications($push_fields);
													
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
													{
														$deviceid = $tablet->google_reg_id;
														// optional payload
														$payload = array();
														$payload['isfeedback'] = false;
														if(isset($push) && $push > 0)
														{
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}

														$title = "Booking Updated";
														$message = $push_fields['message'];
														$res = array();
														$res['title'] = $title;
														$res['is_background'] = false;
														$res['body'] = $message;
														$res['image'] = "";
														$res['payload'] = $payload;
														$res['customer'] = $booking->customer_name;
														$res['maid'] = $booking->maid_name;
														$res['bookingTime'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
														$res['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'notification' => $res,
															'data' => $res,
														);
														$return = android_customer_app_push($fields);
														if($getmaiddeviceid->device_id != "")
														{
															$maid_fields = array(
																'to' => $getmaiddeviceid->device_id,
																'notification' => $res,
																'data' => $res,
															);
															android_customer_app_push($maid_fields);
														}
														//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
													}
													$booking = $this->bookings_model->get_booking_by_id($booking_id);
												}
											} else {
												$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
												if(empty($transfered_booking))
												{ 
													$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
													$zoneID=$c_address->zone_id;
												} else {
													$zoneID= $transfered_booking->zone_id;	
												}
							
												if(isset($zoneID) &&  $zoneID!="")
												{
													$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
													$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
													if($tablet)
													{
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 3;
														$push_fields['maid_id'] = $booking->maid_id;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A",strtotime($time_from)) . '-' . date("h:i A",strtotime($time_to));
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $booking->customer_name;
														$push_fields['maid_name'] = $booking->maid_name;
														$push_fields['booking_time'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;
															if(isset($push) && $push > 0)
															{
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['title'] = $title;
															$res['is_background'] = false;
															$res['body'] = $message;
															$res['image'] = "";
															$res['payload'] = $payload;
															$res['customer'] = $booking->customer_name;
															$res['maid'] = $booking->maid_name;
															$res['bookingTime'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
															$res['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'notification' => $res,
																'data' => $res,
															);
															$return = android_customer_app_push($fields);
															if($getmaiddeviceid->device_id != "")
															{
																$maid_fields = array(
																	'to' => $getmaiddeviceid->device_id,
																	'notification' => $res,
																	'data' => $res,
																);
																android_customer_app_push($maid_fields);
															}
															//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
													}
												}
											}
										}
											
										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
							} else if($booking->booking_type == 'BW')
							{
                                                           
								//$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($booking->maid_id, $today_week_day);
								$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_dayupdate($booking->maid_id, $today_week_day, $update_date);
								foreach($bookings_on_day as $booking_on_day)
								{
									if($booking_on_day->booking_id != $booking->booking_id)
									{
										$s_date_stamp = strtotime($service_start_date);
										$e_date_stamp = strtotime($service_end_date);
										$bs_date_stamp = strtotime($booking_on_day->service_start_date);
										$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
                                                                                
                                                                                if($booking_on_day->booking_type == "BW")
                                                                                {
                                                                                    $now = strtotime($service_start_date); // or your date as well
                                                                                    $your_date = strtotime($booking_on_day->service_start_date);
                                                                                    $datediff = $now - $your_date;

                                                                                    $week_diff = round($datediff / (60 * 60 * 24));
                                                                                    $week_difference = fmod($week_diff,14);

                                                                                    if($week_difference == 0 || $week_difference == '-0')
                                                                                    {
                                                                                        $f_time = strtotime($booking_on_day->time_from);
                                                                                        $t_time = strtotime($booking_on_day->time_to);

                                                                                        if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                                                                        {
																							// echo"<pre>";print_r($booking_on_day);die;
                                                                                                $return = array();
                                                                                                $return['status'] = 'error';
                                                                                                $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                                                                                echo json_encode($return);
                                                                                                exit();
                                                                                        }
                                                                                    }
                                                                                } else { 
																					if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                                                    //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                                                    //latest hideif(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                                                                    {
                                                                                            $f_time = strtotime($booking_on_day->time_from);
                                                                                            $t_time = strtotime($booking_on_day->time_to);

                                                                                            if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                                                                            {
                                                                                                    $return = array();
                                                                                                    $return['status'] = 'error';
                                                                                                    $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$today_week_day] . 's';

                                                                                                    echo json_encode($return);
                                                                                                    exit();
                                                                                            }
                                                                                    }
                                                                                }
									}
								}
								
								if($update_type == 'permanent')
								{
                                                                   
									$service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;
									
									// End current booking and add new booking if address id or time changes
									if($booking->customer_address_id != $customer_address_id || $booking->time_from != $time_from || $booking->time_to != $time_to)
									{
										// End current booking
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => date('Y-m-d', strtotime($service_start_date . ' - 1 day')), 'service_end' => 1));

										$booking_fields = array();
										$booking_fields['customer_id'] = $booking->customer_id;
										$booking_fields['customer_address_id'] = $customer_address_id;
										$booking_fields['maid_id'] = $booking->maid_id;
										$booking_fields['service_type_id'] = $booking->service_type_id;
										$booking_fields['service_start_date'] = $service_start_date;
										$booking_fields['service_week_day'] = $today_week_day;
										$booking_fields['time_from'] = $time_from;
										$booking_fields['time_to'] = $time_to;
										$booking_fields['booking_type'] = $booking->booking_type;
										$booking_fields['cleaning_material'] = $booking->cleaning_material;
										$booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$booking_fields['service_end_date'] = $service_end_date;				
										$booking_fields['booking_note'] = $booking_note;
										$booking_fields['is_locked'] = $is_locked;
										$booking_fields['cleaning_material'] = $cleaning_material;
										$booking_fields['pending_amount'] = $pending_amount;
										$booking_fields['total_amount'] = $total_amt;
										$booking_fields['discount'] = $discount;
										$booking_fields['price_per_hr'] = $priceperhr;
										$booking_fields['discount_price_per_hr'] = $discountpriceperhr;
										$booking_fields['net_service_cost'] = $netamtcost;
										$booking_fields['booking_category'] = 'C';
										$booking_fields['booked_by'] = user_authenticate();
										$booking_fields['booking_status'] = 1;
										$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
										$booking_fields['tabletid'] = $booking->tabletid;

										$booking_id = $this->bookings_model->add_booking($booking_fields);

										if($booking_id)
										{
											if(count($get_dayservice_details) > 0)
											{
												if($get_dayservice_details->odoo_package_activity_status == 1)
												{
													echo 'odoorefresh';
													exit();
												} else {
													$updatearry = array();
													$updatearry['total_fee'] = $total_amt;
													$updatearry['booking_id'] = $booking_id;
													$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
												}
											}
											// if($tabletiddval != $activetabletiddval)
											// {
												// $datetimes = date('Y-m-d H:i:s');
												// $driver_chng_array = array();
												// $driver_chng_array['booking_id'] = $booking_id;
												// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
												// $driver_chng_array['service_date'] = $service_date;
												// $driver_chng_array['transferred_date_time'] = $datetimes;
												// $driver_chng_array['transferred_from'] = 'B';
												// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
												// $driver_chng_array['transferred_by'] = user_authenticate();
												// $this->bookings_model->insert_driverchnage($driver_chng_array,'Bi-weekly');
											// }
											// $justmop=array();
											// $justmop['booking_id']=$booking_id;
											// $justmop['mop']=ucfirst($this->input->post('payment_mode'));
											// $justmop['service_date']=$service_start_date;
											// $justmop['just_mop_ref']=$reference_no;
											// $this->bookings_model->add_justmop_ref($justmop);
											if($service_start_date == date('Y-m-d'))
											{
												if($booking->tabletid > 0)
												{
													$tablet = $this->tablets_model->get_tablet_by_zone_tabid($booking->tabletid);
													if($tablet)
													{
														$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
														$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 5;
														$push_fields['maid_id'] = $upd_booking->maid_id;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $upd_booking->customer_name;
														$push_fields['maid_name'] = $upd_booking->maid_name;
														$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;
															if(isset($push) && $push > 0)
															{
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['title'] = $title;
															$res['is_background'] = false;
															$res['body'] = $message;
															$res['image'] = "";
															$res['payload'] = $payload;
															$res['customer'] = $upd_booking->customer_name;
															$res['maid'] = $upd_booking->maid_name;
															$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
															$res['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'notification' => $res,
																'data' => $res,
															);
															$return = android_customer_app_push($fields);
															if($getmaiddeviceid->device_id != "")
															{
																$maid_fields = array(
																	'to' => $getmaiddeviceid->device_id,
																	'notification' => $res,
																	'data' => $res,
																);
																android_customer_app_push($maid_fields);
															}
															//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
													}
												} else {
													$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
													if(empty($transfered_booking)){ 
														$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
														$zoneID=$c_address->zone_id;
													} else {
														$zoneID= $transfered_booking->zone_id;	
													}
								
													if(isset($zoneID) &&  $zoneID!="")
													{
														$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
														if($tablet)
														{
															$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
															$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
															$push_fields = array();
															$push_fields['tab_id'] = $tablet->tablet_id;
															$push_fields['type'] = 5;
															$push_fields['maid_id'] = $upd_booking->maid_id;
															$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
															$push_fields['title'] = "Booking Updated";
															$push_fields['customer_name'] = $upd_booking->customer_name;
															$push_fields['maid_name'] = $upd_booking->maid_name;
															$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
															if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
																$push = $this->bookings_model->add_push_notifications($push_fields);
															if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															{
																$deviceid = $tablet->google_reg_id;
																// optional payload
																$payload = array();
																$payload['isfeedback'] = false;
																if(isset($push) && $push > 0)
																{
																	$payload['pushid'] = $push;
																} else {
																	$payload['pushid'] = 0;
																}

																$title = "Booking Updated";
																$message = $push_fields['message'];
																$res = array();
																$res['title'] = $title;
																$res['is_background'] = false;
																$res['body'] = $message;
																$res['image'] = "";
																$res['payload'] = $payload;
																$res['customer'] = $upd_booking->customer_name;
																$res['maid'] = $upd_booking->maid_name;
																$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
																$res['timestamp'] = date('Y-m-d G:i:s');
																$regId = $deviceid;
																$fields = array(
																	'to' => $regId,
																	'notification' => $res,
																	'data' => $res,
																);
																$return = android_customer_app_push($fields);
																if($getmaiddeviceid->device_id != "")
																{
																	$maid_fields = array(
																		'to' => $getmaiddeviceid->device_id,
																		'notification' => $res,
																		'data' => $res,
																	);
																	android_customer_app_push($maid_fields);
																}
																//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
															}
															$booking = $this->bookings_model->get_booking_by_id($booking_id);
															
															
														}
													}
												}
											}
					
											$return = array();
											$return['status'] = 'success';
											$return['maid_id'] = $booking->maid_id;
											$return['customer_id'] = $booking->customer_id;
											$return['time_from'] = $time_from;
											$return['time_to'] = $time_to;

											echo json_encode($return);
											exit();
										}
									}
									else
									{
                                                                            
										$update_fields = array();
										$update_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
										$update_fields['service_end_date'] = $service_end_date;
										$update_fields['service_actual_end_date'] = $service_end_date;
										$update_fields['pending_amount'] = $pending_amount;
										$update_fields['total_amount'] = $total_amt;
										$update_fields['discount'] = $discount;
										$update_fields['price_per_hr'] = $priceperhr;
										$update_fields['discount_price_per_hr'] = $discountpriceperhr;
										$update_fields['net_service_cost'] = $netamtcost;
										$update_fields['is_locked'] = $is_locked;
										$update_fields['cleaning_material'] = $cleaning_material;
										$update_fields['booking_note'] = $booking_note;
										$update_fields['tabletid'] = $booking->tabletid;

										$this->bookings_model->update_booking($booking_id, $update_fields);
										
										if(count($get_dayservice_details) > 0)
										{
											if($get_dayservice_details->odoo_package_activity_status == 1)
											{
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												//$updatearry['booking_id'] = $booking_id;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										
										// if($tabletiddval != $activetabletiddval)
										// {
											// $datetimes = date('Y-m-d H:i:s');
											// $driver_chng_array = array();
											// $driver_chng_array['booking_id'] = $booking_id;
											// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
											// $driver_chng_array['service_date'] = $service_date;
											// $driver_chng_array['transferred_date_time'] = $datetimes;
											// $driver_chng_array['transferred_from'] = 'B';
											// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
											// $driver_chng_array['transferred_by'] = user_authenticate();
											// $this->bookings_model->insert_driverchnage($driver_chng_array,'Bi-weekly');
										// }
										
										$payment_mode_new = $this->bookings_model->get_payment_mode($booking_id,$booking->service_start_date);
										// $justmopfields=array();
										// $justmopfields['booking_id']=$booking_id;
										// $justmopfields['mop']=ucfirst($this->input->post('payment_mode'));
										// $justmopfields['service_date']=$booking->service_start_date;
										// $justmopfields['just_mop_ref']=$reference_no;
										
										if(!empty($payment_mode_new)){
										//$this->bookings_model->update_payment_justmop($booking_id,$booking->service_start_date,$justmopfields);
										}
										else{
										//$this->bookings_model->add_justmop_ref($justmopfields);	
										}
										
										
										if($booking->service_start_date == date('Y-m-d'))
										{
											if($booking->tabletid > 0)
											{
												$tablet = $this->tablets_model->get_tablet_by_zone_tabid($booking->tabletid);
												if($tablet)
												{
													$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
													$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 5;
													$push_fields['maid_id'] = $upd_booking->maid_id;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $upd_booking->customer_name;
													$push_fields['maid_name'] = $upd_booking->maid_name;
													$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														$push = $this->bookings_model->add_push_notifications($push_fields);
													
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
													{
														$deviceid = $tablet->google_reg_id;
														// optional payload
														$payload = array();
														$payload['isfeedback'] = false;
														if(isset($push) && $push > 0)
														{
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}

														$title = "Booking Updated";
														$message = $push_fields['message'];
														$res = array();
														$res['title'] = $title;
														$res['is_background'] = false;
														$res['body'] = $message;
														$res['image'] = "";
														$res['payload'] = $payload;
														$res['customer'] = $upd_booking->customer_name;
														$res['maid'] = $upd_booking->maid_name;
														$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
														$res['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'notification' => $res,
															'data' => $res,
														);
														$return = android_customer_app_push($fields);
														if($getmaiddeviceid->device_id != "")
														{
															$maid_fields = array(
																'to' => $getmaiddeviceid->device_id,
																'notification' => $res,
																'data' => $res,
															);
															android_customer_app_push($maid_fields);
														}
														//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
													}
													$booking = $this->bookings_model->get_booking_by_id($booking_id);
												}
											} else {
												$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
												if(empty($transfered_booking)){ 
													$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
													$zoneID=$c_address->zone_id;
												} else {
													$zoneID= $transfered_booking->zone_id;	
												}
												if(isset($zoneID) &&  $zoneID!="")
												{
													$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
													if($tablet)
													{
														$upd_booking = $this->bookings_model->get_booking_by_id($booking_id);
														$getmaiddeviceid = $this->maids_model->get_maid_by_id($upd_booking->maid_id);
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 5;
														$push_fields['maid_id'] = $upd_booking->maid_id;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $upd_booking->maid_name . ', Customer : ' . $upd_booking->customer_name . ', Shift : ' . $upd_booking->newtime_from . '-' . $upd_booking->newtime_to;
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $upd_booking->customer_name;
														$push_fields['maid_name'] = $upd_booking->maid_name;
														$push_fields['booking_time'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;

															if(isset($push) && $push > 0)
															{
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['title'] = $title;
															$res['is_background'] = false;
															$res['body'] = $message;
															$res['image'] = "";
															$res['payload'] = $payload;
															$res['customer'] = $upd_booking->customer_name;
															$res['maid'] = $upd_booking->maid_name;
															$res['bookingTime'] = $upd_booking->newtime_from.' - '.$upd_booking->newtime_to;
															$res['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'notification' => $res,
																'data' => $res,
															);
															$return = android_customer_app_push($fields);
															if($getmaiddeviceid->device_id != "")
															{
																$maid_fields = array(
																	'to' => $getmaiddeviceid->device_id,
																	'notification' => $res,
																	'data' => $res,
																);
																android_customer_app_push($maid_fields);
															}
															//android_push(array($tablet->google_reg_id), array('action' => 'update-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
													}
												}
											}
										}

										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
								else if($update_type == 'one-day')
								{                                 
									/* Delete booking one day */
									$delete_b_fields = array();
									$delete_b_fields['booking_id'] = $booking->booking_id;
									$delete_b_fields['service_date'] = $service_date;
									$delete_b_fields['deleted_by'] = user_authenticate();

									$this->bookings_model->add_booking_delete($delete_b_fields);
									
									/* Add one day booking */
									$booking_fields = array();
									$booking_fields['customer_id'] = $booking->customer_id;
									$booking_fields['customer_address_id'] = $customer_address_id;
									$booking_fields['maid_id'] = $booking->maid_id;
									$booking_fields['service_type_id'] = $booking->service_type_id;
									$booking_fields['service_start_date'] = $service_date;
									$booking_fields['service_week_day'] = $today_week_day;
									$booking_fields['time_from'] = $time_from;
									$booking_fields['time_to'] = $time_to;
									$booking_fields['booking_type'] = 'OD';
									$booking_fields['service_end'] = 1;
									$booking_fields['service_end_date'] = $service_date;				
									$booking_fields['booking_note'] = $booking_note;
									$booking_fields['cleaning_material'] = $booking->cleaning_material;;
									$booking_fields['is_locked'] = $is_locked;
									$booking_fields['cleaning_material'] = $cleaning_material;
									$booking_fields['pending_amount'] = $pending_amount;
									$booking_fields['total_amount'] = $total_amt;
									$booking_fields['booking_category'] = 'C';
									$booking_fields['discount'] = $discount;
									$booking_fields['price_per_hr'] = $priceperhr;
									$booking_fields['discount_price_per_hr'] = $discountpriceperhr;
									$booking_fields['net_service_cost'] = $netamtcost;
									$booking_fields['booked_by'] = user_authenticate();
									$booking_fields['booking_status'] = 1;
									$booking_fields['pay_by'] = $this->input->post('payment_mode');
									$booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
									$booking_fields['tabletid'] = $booking->tabletid;

									$booking_id = $this->bookings_model->add_booking($booking_fields);
                                                                          
                                                                                    
									if($booking_id)
									{
										if(count($get_dayservice_details) > 0)
										{
											if($get_dayservice_details->odoo_package_activity_status == 1)
											{
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['total_fee'] = $total_amt;
												$updatearry['booking_id'] = $booking_id;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										
										// if($tabletiddval != $activetabletiddval)
										// {
											// $datetimes = date('Y-m-d H:i:s');
											// $driver_chng_array = array();
											// $driver_chng_array['booking_id'] = $booking_id;
											// $driver_chng_array['transfering_to_tablet'] = $tabletiddval;
											// $driver_chng_array['service_date'] = $service_date;
											// $driver_chng_array['transferred_date_time'] = $datetimes;
											// $driver_chng_array['transferred_from'] = 'B';
											// $driver_chng_array['transferred_from_id'] = $activetabletiddval;
											// $driver_chng_array['transferred_by'] = user_authenticate();
											// $this->bookings_model->insert_driverchnage($driver_chng_array,'One Day');
										// }
										// $justmop=array();
										// $justmop['booking_id']=$booking_id;
										// $justmop['mop']=ucfirst($this->input->post('payment_mode'));
										// $justmop['service_date']=$service_date;
										// $justmop['just_mop_ref']=$reference_no;
							            // $this->bookings_model->add_justmop_ref($justmop);
										
									
										if($service_date == date('Y-m-d'))
										{
										   // Edited by sandeep
										
										   /* check bookingID in dayservice table and update new bookingId    */
										
											$day_service_row = $this->bookings_model->check_day_service_booking($booking->booking_id);
											
											if($day_service_row)
											{  
											   $day_service_id= $day_service_row[0]->day_service_id;
											   
											   $day_service_fields = array(); 
											   
											   $day_service_fields['booking_id']=$booking_id;
										 
											   //$day_service_fields['start_time']=$time_from;
											   
											   //$day_service_fields['end_time']=$time_to;
											   
											   $day_service_fields['total_fee']=$this->get_price_booking($booking_id);
											   
											   $this->bookings_model->update_day_service_booking($day_service_id,$day_service_fields);
												
											}
											
											if($booking->tabletid > 0)
											{
												$tablet = $this->tablets_model->get_tablet_by_zone_tabid($booking->tabletid);
												$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
												if($tablet)
												{
													$push_fields = array();
													$push_fields['tab_id'] = $tablet->tablet_id;
													$push_fields['type'] = 3;
													$push_fields['maid_id'] = $booking->maid_id;
													$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A",strtotime($time_from)) . '-' . date("h:i A",strtotime($time_to));
													$push_fields['title'] = "Booking Updated";
													$push_fields['customer_name'] = $booking->customer_name;
													$push_fields['maid_name'] = $booking->maid_name;
													$push_fields['booking_time'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														$push = $this->bookings_model->add_push_notifications($push_fields);
													
													if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
													{
														$deviceid = $tablet->google_reg_id;
														// optional payload
														$payload = array();
														$payload['isfeedback'] = false;
														if(isset($push) && $push > 0)
														{
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}

														$title = "Booking Updated";
														$message = $push_fields['message'];
														$res = array();
														$res['title'] = $title;
														$res['is_background'] = false;
														$res['body'] = $message;
														$res['image'] = "";
														$res['payload'] = $payload;
														$res['customer'] = $booking->customer_name;
														$res['maid'] = $booking->maid_name;
														$res['bookingTime'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
														$res['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'notification' => $res,
															'data' => $res,
														);
														$return = android_customer_app_push($fields);
														if($getmaiddeviceid->device_id != "")
														{
															$maid_fields = array(
																'to' => $getmaiddeviceid->device_id,
																'notification' => $res,
																'data' => $res,
															);
															android_customer_app_push($maid_fields);
														}
														//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
													}
													$booking = $this->bookings_model->get_booking_by_id($booking_id);
															
													
												}
											} else {
												$transfered_booking=$this->bookings_model->transfered_booking($booking_id, date('Y-m-d'));
												if(empty($transfered_booking))
												{ 
													$c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
													$zoneID=$c_address->zone_id;
												} else {
													$zoneID= $transfered_booking->zone_id;	
												}
								
												if(isset($zoneID) &&  $zoneID!="")
												{
													$tablet = $this->tablets_model->get_tablet_by_zone($zoneID);
													$getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
													if($tablet)
													{
														$push_fields = array();
														$push_fields['tab_id'] = $tablet->tablet_id;
														$push_fields['type'] = 3;
														$push_fields['maid_id'] = $booking->maid_id;
														$push_fields['message'] = 'Booking Updated. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . date("h:i A",strtotime($time_from)) . '-' . date("h:i A",strtotime($time_to));
														$push_fields['title'] = "Booking Updated";
														$push_fields['customer_name'] = $booking->customer_name;
														$push_fields['maid_name'] = $booking->maid_name;
														$push_fields['booking_time'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
															$push = $this->bookings_model->add_push_notifications($push_fields);
														
														if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
														{
															$deviceid = $tablet->google_reg_id;
															// optional payload
															$payload = array();
															$payload['isfeedback'] = false;
															if(isset($push) && $push > 0)
															{
																$payload['pushid'] = $push;
															} else {
																$payload['pushid'] = 0;
															}

															$title = "Booking Updated";
															$message = $push_fields['message'];
															$res = array();
															$res['title'] = $title;
															$res['is_background'] = false;
															$res['body'] = $message;
															$res['image'] = "";
															$res['payload'] = $payload;
															$res['customer'] = $booking->customer_name;
															$res['maid'] = $booking->maid_name;
															$res['bookingTime'] = date("h:i A",strtotime($time_from)).' - '.date("h:i A",strtotime($time_to));
															$res['timestamp'] = date('Y-m-d G:i:s');
															$regId = $deviceid;
															$fields = array(
																'to' => $regId,
																'notification' => $res,
																'data' => $res,
															);
															$return = android_customer_app_push($fields);
															if($getmaiddeviceid->device_id != "")
															{
																$maid_fields = array(
																	'to' => $getmaiddeviceid->device_id,
																	'notification' => $res,
																	'data' => $res,
																);
																android_customer_app_push($maid_fields);
															}
															//android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
														}
														$booking = $this->bookings_model->get_booking_by_id($booking_id);
																
														
													}
												}
											}
										}
											
										$return = array();
										$return['status'] = 'success';
										$return['maid_id'] = $booking->maid_id;
										$return['customer_id'] = $booking->customer_id;
										$return['time_from'] = $time_from;
										$return['time_to'] = $time_to;

										echo json_encode($return);
										exit();
									}
								}
							}
						}
						else
						{
							echo 'refresh';
							exit();
						}
					}
										
					echo 'refresh';
					exit();
				}
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'refresh-grid')
			{
				$data = array();
				$data['maids'] = $maids;
				$data['maid_bookings'] = $maid_bookings;
                                $data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
                                $data['leave_maid_types'] = $leave_maid_types; // Maid Leave
				$data['all_bookings'] = $all_bookings;
				$data['times'] = $times;
				$data['booking_allowed'] = 1; // Enable past bookings by Geethu
				//$data['booking_allowed'] = strtotime($schedule_date) >= strtotime( date('d-M-Y')) ? 1 : 0;
				$schedule_day = date('d F Y, l', strtotime($schedule_date));
				$data['schedule_day'] = $schedule_day;
				$data['service_datesss'] = $service_date;
				$data['current_hour_index'] = $current_hour_index;
				$schedule_grid = $this->load->view('partials/schedule_grid', $data, TRUE);
				$schedule_report = $this->load->view('partials/schedule_report', $data, TRUE);
				
				echo json_encode(array('grid' => $schedule_grid, 'report' => $schedule_report));
				exit();
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'get-all-drivers' && $this->input->post('hiddentabletid') && is_numeric($this->input->post('hiddentabletid')) && $this->input->post('hiddentabletid') > 0)
			{
				$hiddentabletid = trim($this->input->post('hiddentabletid'));
				$getdrivers = $this->tablets_model->get_all_drivers($hiddentabletid);
				if(!empty($getdrivers))
				{
					$html = '<select name="transferdriverselect" id="transferdriverselect"><option value="">Select Driver</option>';
					foreach($getdrivers as $driverval)
					{
						$html .= '<option value="'.$driverval->tablet_id.'">'.$driverval->tablet_driver_name.'</option>';
					}
					$html .= '</select>';
									
									
					$returnval = array();
					$returnval['status'] = 'success';
					$returnval['html'] = $html;
					echo json_encode($returnval);
					exit();
				} else {
					echo 'refresh';
					exit();
				}
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
			{
				$customer_id = trim($this->input->post('customer_id'));
				
				$customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                                
                                
				echo json_encode($customer_addresses); 
				exit();
			}
                        if($this->input->post('action') && $this->input->post('action') == 'get-customer-pending-amount' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
			{
				$customer_id = trim($this->input->post('customer_id'));
				
				$pending_amount = $this->customers_model->get_customer_details($customer_id);
                                
                                
				echo json_encode($pending_amount); 
				exit();
			}
                        if($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
			{
				$customer_id = trim($this->input->post('customer_id'));
				
				$customer_addresses = $this->customers_model->get_customer_addresses($customer_id);
                                
                                
				echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses))); 
				exit();
			}
                        if($this->input->post('action') && $this->input->post('action') == 'get-details-customer' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
			{
				$customer_id = trim($this->input->post('customer_id'));
				
				$customer_details = $this->customers_model->get_customer_by_id($customer_id);
                                
                                
				echo json_encode($customer_details); 
				exit();
			}
			// Edited byt Geethu
			if($this->input->post('action') && $this->input->post('action') == 'get-free-maids' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
				$booking_id = trim($this->input->post('booking_id'));
				$booking = $this->bookings_model->get_booking_by_id($booking_id);
				$same_zone = $this->input->post('same_zone');
				$service_dates = $this->input->post('service_date');
				$service_date_new = date('Y-m-d', strtotime($service_dates));
				$service_end_date = $service_date_new;
				
				
				$search_maids = $this->maids_model->get_all_maid_new();
				//$search_maids = $this->maids_model->get_all_maid_new($filter);
				$time_from_stamp = strtotime(trim($booking->time_from));
				$time_to_stamp = strtotime(trim($booking->time_to));
				
				$maid_array =array();
				$nf_maids = array();
				
				foreach ($search_maids as $s_maids)
				{
					$today_week_day = date('w', strtotime($service_date_new));
					$repeat_days = $today_week_day;
					$repeat_end = $booking->service_end;
					
					if($repeat_end == '1')
					{
						$repeat_end_date = $booking->service_end_date;
							
						$repeat_end_date_split = explode('-', $repeat_end_date);
						if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
						{
							$s_date = new DateTime($service_date_new);
							$e_date = new DateTime($repeat_end_date);
							//$diff = $s_date->diff($e_date);
							
							$diff = abs(strtotime($repeat_end_date) - strtotime($service_date_new));

							$years = floor($diff / (365*60*60*24));
							$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
							$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
																	
							if($days < 7)//if($diff->days < 7)
							{
								//$service_end_date = "";
							}
							
							$service_end_date = $repeat_end_date;
							
						}
						else 
						{
							//$service_end_date = "";
						}
					}
					
					if($booking->booking_type == 'OD')
					{
						//$time_from_stamp = strtotime(trim($booking->time_from));
						//$time_to_stamp = strtotime(trim($booking->time_to));
						
						$today_bookingss = $this->bookings_model->get_schedule_by_date_avail_new($service_date_new,$s_maids->maid_id);
						
						
						foreach ($today_bookingss as $t_booking)
						{
							$booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);
							
						}
						
						//$maids_s = $this->maids_model->get_maids();
						
						//foreach($maids_s as $smaid)
						//{
						if(isset($booked_slots[$s_maids->maid_id]['time']))
						{
							foreach($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time)
							{
								if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
								{
									//array_push($nf_maids, $smaid->maid_id);
									array_push($nf_maids, $s_maids->maid_id);
								}
							}
						}
						//}
					}
					
					// echo '<pre>';
					// print_r($nf_maids);
					// exit();
					
					if($booking->booking_type == 'WE')
					{
						//foreach($repeat_days as $repeat_day)
						//{
							//echo $repeat_day;
							if($repeat_days < $today_week_day)
							{
								$day_diff = (6 - $today_week_day + $repeat_days + 1);
							}
							else
							{
								$day_diff = $repeat_days - $today_week_day;
							}
							$service_start_date = date('Y-m-d', strtotime($service_date_new . ' + ' . $day_diff . ' day'));
							
							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_forfree($s_maids->maid_id, $repeat_days, $service_date_new);
							// echo '<pre>';
							// print_r($bookings_on_day);
							// exit();
					
							if(!empty($bookings_on_day))
							{	
								foreach($bookings_on_day as $booking_on_day)
								{
									if($booking_on_day->booking_type == "OD")
									{
										if($booking_on_day->service_start_date == $service_date_new)
										{
											$f_time = strtotime($booking_on_day->time_from);
											$t_time = strtotime($booking_on_day->time_to);
											if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
											{
												//array_push($nf_maids, $smaid->maid_id);
												array_push($nf_maids, $s_maids->maid_id);
											}
										}
									} else {
									// if($service_end_date == "")
									// {
										// $service_end_date = $service_start_date;
									// } else {
										// $service_end_date = $service_end_date;
									// }
									$s_date_stamp = strtotime($service_start_date);
									$e_date_stamp = strtotime($service_end_date);
									$bs_date_stamp = strtotime($booking_on_day->service_start_date);
									$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
							
									if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 0) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 0) || ($repeat_end == 1 && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
									//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
									{
										$f_time = strtotime($booking_on_day->time_from);
										$t_time = strtotime($booking_on_day->time_to);

										if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) )//|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
										{
											array_push($nf_maids, $s_maids->maid_id);
										} else {
											
										}								
									}
									}
								}
							} else {
                        
							}
						//}
					}
					
					if($booking->booking_type == 'BW')
					{
						//foreach($repeat_days as $repeat_day)
						//{
							if($repeat_days < $today_week_day)
							{
								$day_diff = (6 - $today_week_day + $repeat_days + 1);
							}
							else
							{
								$day_diff = $repeat_days - $today_week_day;
							}
							$service_start_date = date('Y-m-d', strtotime($service_date_new . ' + ' . $day_diff . ' day'));
							$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_forfree($s_maids->maid_id, $repeat_days, $service_date_new);
							
							if(!empty($bookings_on_day))
							{
								foreach($bookings_on_day as $booking_on_day)
								{
									// if($service_end_date == "")
									// {
										// $service_end_date = $service_start_date;
									// } else {
										// $service_end_date = $service_end_date;
									// }
									$s_date_stamp = strtotime($service_start_date);
									$e_date_stamp = strtotime($service_end_date);
									$bs_date_stamp = strtotime($booking_on_day->service_start_date);
									$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
							
									if($booking_on_day->booking_type == "BW")
									{
										$now = strtotime($service_start_date); // or your date as well
										$your_date = strtotime($booking_on_day->service_start_date);
										$datediff = $now - $your_date;

										$week_diff = round($datediff / (60 * 60 * 24));
										$week_difference = fmod($week_diff,14);
															
										if($week_difference == 0 || $week_difference == '-0')
										{
											$f_time = strtotime($booking_on_day->time_from);
											$t_time = strtotime($booking_on_day->time_to);
											
											if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
											{
												array_push($nf_maids, $s_maids->maid_id);
											} else {

											}
										}
									} else if($booking_on_day->booking_type == "WE"){
								
										if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 0) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 0) || ($repeat_end == 1 && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										//latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
										//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
										{
												$f_time = strtotime($booking_on_day->time_from);
												$t_time = strtotime($booking_on_day->time_to);

												if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
												{
														array_push($nf_maids, $s_maids->maid_id);
												}
										}
									} else if($booking_on_day->booking_type == "OD"){
										if($booking_on_day->service_start_date == $service_date_new)
										{
											$f_time = strtotime($booking_on_day->time_from);
											$t_time = strtotime($booking_on_day->time_to);
											if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
											{
												//array_push($nf_maids, $smaid->maid_id);
												array_push($nf_maids, $s_maids->maid_id);
											}
										}
										
									}
								}
							}  else {
                        
							}
						//}
					}
				}
				$all_maid_list = array();
				$all_maids = $this->maids_model->get_maids();
				foreach ($all_maids as $maid){
					array_push($all_maid_list, $maid->maid_id);
				}
				$free_maids = array_diff($all_maid_list, $nf_maids);
                                 
				if(!empty($free_maids))
				{
					//$f_maids = array_diff($free_maids, $not_free_maids);
					$free_maid_dtls = array();
					$free_maid_ids = array();
					foreach ($free_maids as $f_maid){
						$maid = $this->maids_model->get_maid_by_id($f_maid);
						array_push($free_maid_dtls, $maid);
						array_push($free_maid_ids, $f_maid);
					}
					if($same_zone == 1)
					{
						$zone = $this->customers_model->get_customer_zone_by_address_id($booking->customer_address_id);
						$same_zone_maids = $this->bookings_model->get_same_zone_maids($zone->zone_id, $free_maid_ids, $booking->service_week_day);
						$free_maid_dtls = array();
						foreach ($same_zone_maids as $same_zone_maid){
							$maid = $this->maids_model->get_maid_by_id($same_zone_maid->maid_id);
							array_push($free_maid_dtls, $maid);                                            
						}
						if(empty($free_maid_dtls))
						{                                           
							$return = array();
							$return['status'] = 'error';
							$return['message'] = 'There are no same zone maids available';

							echo json_encode($return);
							exit();
							
						}
					}
					echo json_encode($free_maid_dtls);
					exit();
				}
				else
				{
					$return = array();
					$return['status'] = 'error';
					$return['message'] = 'There are no maids available for the selected time slot';

					echo json_encode($return);
					exit();
				}
			}
                        // End 
						
						//for services
                        if($this->input->post('action') && $this->input->post('action') == 'get-related-services' && $this->input->post('service_id') && is_numeric($this->input->post('service_id')) && $this->input->post('service_id') > 0)
			{
                            $service_id = trim($this->input->post('service_id'));
                            $service_detail = $this->service_types_model->get_servicedetail_id($service_id);
                            $html = '<table id="da-ex-datatable-numberpaging" class="table da-table dataTable no-footer">';
                            $i = 1;
                            foreach ($service_detail as $detail)
                            {
                                $html .= '<tr>';
                                $html .= '<td><span id=""><strong>' .$i. '</strong></span></td>';
                                //if($detail->category_id != 0)
                                //{
                                $html .= '<td><span id=""><strong>' .$detail->service_category_name. '</strong></span></td>';
                                //}
                                //if($detail->sub_category_id != 0)
                                //{
                                    $html .= '<td><span id=""><strong>' .$detail->sub_category_name. '</strong></span></td>';
                                //}
                                //if($detail->service_sub_funish_id != 0)
                                //{
                                    $html .= '<td><span id=""><strong>' .$detail->service_furnish_name. '</strong></span></td>';
                                //}
                                if($detail->enable_scrubbing == 'Y')
                                {
                                    if($detail->is_scrubbing == 1){
                                        $scrubval = "Yes";
                                    } else {
                                        $scrubval = "No";
                                    }
                                    $html .= '<td><span id=""><strong>' .$scrubval. '</strong></span></td>';
                                }
                                $html .= '<td><span id=""><strong>' .$detail->name. '</strong></span></td>';
                                $html .= '<td><span id=""><strong>' .$detail->service_cost. '</strong></span></td>';
                                $html .= '<td><input data-flat="'.$detail->flat_rate.'" data-catid="'.$detail->category_id.'" data-subcatid="'.$detail->sub_category_id.'" data-furnishid="'.$detail->service_sub_funish_id.'" data-scrubid="'.$detail->is_scrubbing.'" data-name="'.$detail->name.'" data-cost="'.$detail->service_cost.'"  type="button" value="Select &raquo;" class="copy_service"  /></td>';
                                $html .= '</tr>';
                                $i++;
                            }
                            $html .= '</table>';
                            echo $html;
                            exit();
			}
                        //ends
						
			if($this->input->post('action') && $this->input->post('action') == 'delete-booking-permanent' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
				$booking_id = trim($this->input->post('booking_id'));
				$booking_idnewww =trim($this->input->post('booking_id'));
				$get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_idnewww,$service_date);
				if(count($get_dayservice_details) > 0)
				{
					if(user_authenticate_isadmin() != 'Y' && $get_dayservice_details->odoo_package_activity_status == 1)
					{
						echo 'odoorefresh';
						exit();
					}
				}
				
				$remarks = $this->input->post('remarks');
                                
				$d_booking = $this->bookings_model->get_booking_by_id($booking_id);
				if(user_authenticate() != 1)
				{              
					if($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate())
					{
						echo 'locked';
						exit();
					}
				}
				if(isset($d_booking->booking_id))
				{
					if($d_booking->booking_type == 'OD')
					{
						$this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
                                                                                                
					}
					else
					{
						$newsearchdate = date('Y-m-d', strtotime($schedule_date));
						$enddate = $d_booking->service_start_date;
						if($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate)
						{
							$this->bookings_model->update_booking($booking_id, array('booking_status' => 2), 'Delete');
						} else {
							if($d_booking->booking_type == 'WE')
							{
								$current = strtotime($newsearchdate);
								$last = strtotime($enddate); 
								while( $current >= $last ) {
									$current = strtotime('-7 days', $current);
									$deletedatee = date('Y-m-d', $current);
									$check_buk_delete=$this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id,$deletedatee);
									if(count($check_buk_delete)>0)
									{   
										
									} else {
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
										break;
									}
								}
							} else if($d_booking->booking_type == 'BW')
							{
								$current = strtotime($newsearchdate);
								$last = strtotime($enddate); 
								while( $current >= $last ) {
									$current = strtotime('-14 days', $current);
									$deletedatee = date('Y-m-d', $current);
									$check_buk_delete=$this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id,$deletedatee);
									if(count($check_buk_delete)>0)
									{   
										
									} else {
										$this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
										break;
									}
								}
							}
						}
						
						// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 1 day'));
						// if($d_booking->booking_type == 'WE')
						// {
							// $last_repeat_date=date('Y-m-d', strtotime($schedule_date . ' - 7 day'));
							// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 6 day'));
							// $check_buk_delete=$this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id,$last_repeat_date);
							// if(count($check_buk_delete)>0)
							// {   
								// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 8 day'));
							// }
						// }
						// else if($d_booking->booking_type == 'BW')
						// {
							// $last_repeat_date=date('Y-m-d', strtotime($schedule_date . ' - 14 day'));
							// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 13 day'));
							// $check_buk_delete=$this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id,$last_repeat_date);
							// if(count($check_buk_delete)>0)
							// {   
								// $ser_ac_end_date=date('Y-m-d', strtotime($schedule_date . ' - 15 day'));
							// }
						// }
						// $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $ser_ac_end_date, 'service_end' => 1), 'Delete');    
					}
                                        
                                        $delete_b_fields = array();
                                        $delete_b_fields['booking_id'] = $booking_id;
                                        $delete_b_fields['service_date'] = $service_date;
                                        $delete_b_fields['deleted_by'] = user_authenticate();

                                        $this->bookings_model->add_booking_cancel($delete_b_fields);
                                        
                                        $delete_b_fields_new = array();
                                        $delete_b_fields_new['booking_id'] = $booking_id;
                                        $delete_b_fields_new['service_date'] = $service_date;
                                        $delete_b_fields_new['remarks'] = $remarks;
                                        $delete_b_fields_new['deleted_by'] = user_authenticate();
                                        $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');
                                        
                                        $this->bookings_model->add_booking_remarks($delete_b_fields_new);
										
										//adding for odoo issue
										if(count($get_dayservice_details) > 0)
										{
											if($get_dayservice_details->odoo_package_activity_status == 1)
											{
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['service_status'] = 3;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										//odoo issue ends
										

					//if(($d_booking->booking_type == 'WE' && $d_booking->service_start_date == date('Y-m-d')) || ($d_booking->service_week_day == date('w') && strtotime(date('Y-m-d')) >= strtotime($d_booking->service_start_date) && strtotime(date('Y-m-d')) < strtotime($schedule_date)))
					//{
						$c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
						if(isset($c_address->zone_id))
						{
							$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
							$getmaiddeviceid = $this->maids_model->get_maid_by_id($d_booking->maid_id);
                                                        if($tablet)
							{
                                                            $push_fields = array();
                                                            $push_fields['tab_id'] = $tablet->tablet_id;
                                                            $push_fields['type'] = 2;
                                                            $push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                                                            $push_fields['maid_id'] = $d_booking->maid_id;
															$push_fields['title'] = "Booking Cancelled";
															$push_fields['customer_name'] = $d_booking->customer_name;
															$push_fields['maid_name'] = $d_booking->maid_name;
															$push_fields['booking_time'] = $d_booking->newtime_from.' - '.$d_booking->newtime_to;
                                                            if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
                                                                $push = $this->bookings_model->add_push_notifications($push_fields);
                                                            
                                                            if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
                                                            {
                                                                $deviceid = $tablet->google_reg_id;
                                                                // optional payload
                                                                $payload = array();
                                                                $payload['isfeedback'] = false;
																if(isset($push) && $push > 0)
																{
																	$payload['pushid'] = $push;
																} else {
																	$payload['pushid'] = 0;
																}

                                                                $title = "Booking Cancelled";
                                                                $message = $push_fields['message'];
                                                                $res = array();
                                                                $res['title'] = $title;
																$res['is_background'] = false;
																$res['body'] = $message;
																$res['image'] = "";
																$res['payload'] = $payload;
																$res['customer'] = $d_booking->customer_name;
																$res['maid'] = $d_booking->maid_name;
																$res['bookingTime'] = $d_booking->newtime_from.' - '.$d_booking->newtime_to;
																$res['timestamp'] = date('Y-m-d G:i:s');
																$regId = $deviceid;
																$fields = array(
																	'to' => $regId,
																	'notification' => $res,
																	'data' => $res,
																);
																$return = android_customer_app_push($fields);
																if($getmaiddeviceid->device_id != "")
																{
																	$maid_fields = array(
																		'to' => $getmaiddeviceid->device_id,
																		'notification' => $res,
																		'data' => $res,
																	);
																	android_customer_app_push($maid_fields);
																}
                                                                //$return = android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                                            }
                                                        }
                                                        //print_r($return);exit;
						}
					//}
				
					echo 'success';
					exit();
				}
				
				echo 'refresh';
				exit();
			}
			
			if($this->input->post('action') && $this->input->post('action') == 'delete-booking-one-day' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
				$booking_id = trim($this->input->post('booking_id'));
				$booking_idnewww =trim($this->input->post('booking_id'));
				$get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_idnewww,$service_date);
				if(count($get_dayservice_details) > 0)
				{
					if(user_authenticate_isadmin() != 'Y' && $get_dayservice_details->odoo_package_activity_status == 1)
					{
						echo 'odoorefresh';
						exit();
					}
				}
				$remarks = $this->input->post('remarks');
                                
				$d_booking = $this->bookings_model->get_booking_by_id($booking_id);
								if(user_authenticate() != 1){
                                if($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate())
                                {
                                    echo 'locked';
                                    exit();
                                }
								}
				if(isset($d_booking->booking_id))
				{
					if($d_booking->booking_type != 'OD')
					{
						$delete_b_fields = array();
						$delete_b_fields['booking_id'] = $booking_id;
						$delete_b_fields['service_date'] = $service_date;
                                                $delete_b_fields['deleted_by'] = user_authenticate();
                                                
						$this->bookings_model->add_booking_delete($delete_b_fields);
                                                
                                                $delete_b_fields_new = array();
                                                $delete_b_fields_new['booking_id'] = $booking_id;
                                                $delete_b_fields_new['service_date'] = $service_date;
                                                $delete_b_fields_new['remarks'] = $remarks;
                                                $delete_b_fields_new['deleted_by'] = user_authenticate();
                                                $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');

                                                $this->bookings_model->add_booking_remarks($delete_b_fields_new);
												
												//adding for odoo issue
										if(count($get_dayservice_details) > 0)
										{
											if($get_dayservice_details->odoo_package_activity_status == 1)
											{
												echo 'odoorefresh';
												exit();
											} else {
												$updatearry = array();
												$updatearry['service_status'] = 3;
												$this->bookings_model->update_day_service_booking($get_dayservice_details->day_service_id, $updatearry);
											}
										}
										//odoo issue ends
					}
				}
				
				//if($service_date == date('Y-m-d'))
				//{
					$c_address = $this->customers_model->get_customer_address_by_id($d_booking->customer_address_id);
					if(isset($c_address->zone_id))
					{
						$tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
						$getmaiddeviceid = $this->maids_model->get_maid_by_id($d_booking->maid_id);
                                                if($tablet)
                                                {
                                                    $push_fields = array();
                                                    $push_fields['tab_id'] = $tablet->tablet_id;
                                                    $push_fields['type'] = 2;
													$push_fields['message'] = 'Booking Cancelled. Maid : ' . $d_booking->maid_name . ', Customer : ' . $d_booking->customer_name . ', Shift : ' . $d_booking->newtime_from . '-' . $d_booking->newtime_to;
                                                    $push_fields['maid_id'] = $d_booking->maid_id;
													$push_fields['title'] = "Booking Cancelled";
													$push_fields['customer_name'] = $d_booking->customer_name;
													$push_fields['maid_name'] = $d_booking->maid_name;
													$push_fields['booking_time'] = $d_booking->newtime_from.' - '.$d_booking->newtime_to;
                                                    if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
                                                        $push = $this->bookings_model->add_push_notifications($push_fields);
                                                    
                                                    if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
                                                    {
                                                        $deviceid = $tablet->google_reg_id;
                                                        // optional payload
                                                        $payload = array();
                                                        $payload['isfeedback'] = false;
														if(isset($push) && $push > 0)
														{
															$payload['pushid'] = $push;
														} else {
															$payload['pushid'] = 0;
														}

                                                        $title = "Booking Cancelled";
                                                        $message = $push_fields['message'];
                                                        $res = array();
                                                        $res['title'] = $title;
														$res['is_background'] = false;
														$res['body'] = $message;
														$res['image'] = "";
														$res['payload'] = $payload;
														$res['customer'] = $d_booking->customer_name;
														$res['maid'] = $d_booking->maid_name;
														$res['bookingTime'] = $d_booking->newtime_from.' - '.$d_booking->newtime_to;
														$res['timestamp'] = date('Y-m-d G:i:s');
														$regId = $deviceid;
														$fields = array(
															'to' => $regId,
															'notification' => $res,
															'data' => $res,
														);
														$return = android_customer_app_push($fields);
														if($getmaiddeviceid->device_id != "")
														{
															$maid_fields = array(
																'to' => $getmaiddeviceid->device_id,
																'notification' => $res,
																'data' => $res,
															);
															android_customer_app_push($maid_fields);
														}
                                                        //android_push(array($tablet->google_reg_id), array('action' => 'delete-booking', 'booking-ids' => array($booking_id), 'message' => $push_fields['message']));
                                                    }
                                                }
					}
				//}
				
				echo 'success';
				exit();
			}
                        if($this->input->post('action') && $this->input->post('action') == 'check_booking_status' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
                           $booking_id = $this->input->post('booking_id');
                           if($this->uri->segment(2) && strlen(trim($this->uri->segment(2) > 0))){
                           $scheduled_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
                           }
                           else
                           {
                            $scheduled_date = date("Y-m-d");   
                           }
                           $job_service=$this->bookings_model->job_start_or_finish($booking_id,$scheduled_date);
                           $resp_service=array();
                           if(!empty($job_service)){
                               
                    
                               echo json_encode($job_service);
                               exit;
                           }
                           else{
                               echo 0;
                               exit;   
                               
                           }
                            
                        }
			
			echo 'refresh';
			exit();
		}
		
		$schedule_day = date('l d / M / Y', strtotime($schedule_date));
		$next_day = date('d-m-Y', strtotime($schedule_date . ' + 1 day'));
		$prev_day = date('d-m-Y', strtotime($schedule_date . ' - 1 day'));
		$day_number = date('w', strtotime($schedule_date));
		
                $servicesssdate = date('d-m-Y', strtotime($schedule_date));
		//$customers = $this->customers_model->get_customers();
                
		$service_types = $this->service_types_model->get_service_types();

		$all_maids = array();
		foreach($maids as $maid)
		{
			$all_maids['m-' . $maid->maid_id] = $maid->maid_name;
		}
		
		$data = array();
		$data['schedule_day'] = $schedule_day;
		$data['all_bookings'] = $all_bookings;
                $data['freemaid_ids'] = $freemaid_id;
                $data['morning_avail'] = $morning_avail;
                $data['eve_avail'] = $eve_avail;
                $data['tot_avail'] = $tot_avail;
                $data['services_date'] = $servicesssdate;
		$data['schedule_day_c'] = date('d-m-Y', strtotime($schedule_date));
		$data['repeate_end_start_c'] = date('Y, m, d', strtotime($schedule_date . ' + 7 day'));
		$data['next_day'] = $next_day;
		$data['prev_day'] = $prev_day;
		$data['service_datesss'] = $service_date; 
		$data['day_number'] = $day_number;
		$data['times'] = $times;                           
		$data['maid_bookings'] = $maid_bookings;
		$data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
		$data['leave_maid_types'] = $leave_maid_types;
		$data['maids'] = $maids;
		$data['all_maids'] = json_encode($all_maids);
		//$data['customers'] = $customers;
		$data['service_types'] = $service_types;
		$data['booking_allowed'] = strtotime($schedule_date) >= strtotime( date('d-M-Y')) ? 1 : 0;
		$data['current_hour_index'] = $current_hour_index;
                //$data['teams'] = $this->settings_model->get_teams();
                //$data['teamss'] = $this->settings_model->get_teams_for_count($team_id);
                $data['maidsss'] = $this->maids_model->get_maids();
                //$data['team_ids'] = $team_id;
		$data['tablets'] = $this->settings_model->get_tablet_list();
		$data['schedule_grid'] = $this->load->view('partials/schedule_grid', $data, TRUE);
		$data['areas'] = $this->settings_model->get_areas();
		$layout_data = array();
		$layout_data['content_body'] = $this->load->view('schedule', $data, TRUE);		
		$layout_data['page_title'] = 'Bookings';
		$layout_data['meta_description'] = 'Bookings';
                $layout_data['bookings_active'] = '1';
		$layout_data['css_files'] = array('jquery.fancybox.css', 'datepicker.css', 'bootstrap-datetimepicker.min.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('jquery.fancybox.pack.js', 'bootstrap-datetimepicker.min.js', 'bootstrap-datepicker.js', 'booking.js');
		
		$this->load->view('layouts/default', $layout_data);
	}
	
	public function testsql()
	{
		$this->load->model('customerappapi_model');
		$this->customerappapi_model->get_extra_service_details();
		exit();
	}
	
	function testinsert()
	{
		$row = 1;
if (($handle = fopen("/home/bookingspectrums/public_html/csv/justmopregularclient.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        //echo $data[0];echo '</br>';
        //echo $name = $data[1];
		$name = $data[1]; echo '<br>';
		$address = $data[2];
		$area = $data[3];
		$paymode = $data[11];
		$date = date('Y-m-d H:i:s');
		
		$data = array(
			'customer_type' => 'HO',
			'customer_name' => $name,
			'customer_nick_name' => $name,
			'is_company' => 'Y',
			'company_name' => 'Justmop',
			'mobile_number_1' => '045578370',
			'contact_person' => $name,
			'payment_type' => 'D',
			'payment_mode' => $paymode,
			'key_given' => 'N',
			'price_hourly' => '35',
			'price_extra' => '35',
			'price_weekend' => '35',
			'customer_source' => 'Justmop',
			'customer_added_datetime' => $date,
			'customer_booktype' => 1,
			'customer_status' => 1,
		);
			
			$res = $this->customers_model->add_customers($data);
			
			echo 'customer'.$res;
			echo '<br/>';
			if($res > 0)
			{
				$datas = array(
					'customer_id' => $res,
					'area_id' => $area,
					'customer_address' => $address,
				);
				$this->customers_model->add_customer_address($datas);
				echo 'address';
			}
		
        
    }
    fclose($handle);
}
exit();
	}
        function online()
        {
            $week_day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            $online_bookings = $this->bookings_model->get_online_bookings();
		
            $all_online_bookings = array();
            
            $i = 0;
            foreach($online_bookings as $booking)
            {
                    $all_online_bookings[$i] = new stdClass();
                    $all_online_bookings[$i]->id = $booking->booking_id;
                    $all_online_bookings[$i]->start_date = $booking->service_start_date;
                    $all_online_bookings[$i]->week_day = $week_day_names[$booking->service_week_day-1];
                    $all_online_bookings[$i]->shift = $booking->shift;
                    $all_online_bookings[$i]->type = $booking->booking_type;
                    $all_online_bookings[$i]->type_name = $booking->service_type_name;
                    $all_online_bookings[$i]->customer_id = $booking->customer_id;
                    $all_online_bookings[$i]->customer = html_escape($booking->customer_name);  
                    $all_online_bookings[$i]->contact_number = html_escape($booking->mobile_number_1);
                    $all_online_bookings[$i]->area = html_escape($booking->zone_name . ' - ' . $booking->area_name);
                    $all_online_bookings[$i]->address = str_replace(array("\r\n", "\n\r", "\n", "\r"), ', ', (html_escape($booking->customer_address)));
                    
                    $i++;
            }
            
            
            
            $data = array();
            $data['online_bookings'] = $all_online_bookings;
            $layout_data['content_body'] = $this->load->view('list_online_bookings', $data, TRUE);

            $layout_data['page_title'] = 'Online Bookings';
            $layout_data['meta_description'] = 'Online Bookings';
            $layout_data['css_files'] = array('bootstrap.css', 'datepicker.css');
            $layout_data['external_js_files'] = array();
            $layout_data['js_files'] = array('jquery.twbsPagination.min.js', 'customer.js','bootstrap-datepicker.js');

            $this->load->view('layouts/default', $layout_data);
        }
        
        public function onedaycanceldelete()
        {
            $booking_delete_id = $this->input->post('id');
            echo $this->bookings_model->undo_booking_delete($booking_delete_id);
        }
        public function appointment_bill($booking_id) {
            
            $this->load->helper('bill_cronjob_helper');
            $this->load->model('bill_model');
            
            $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
            
            $customer = $this->customers_model->get_customer_by_id($d_booking->customer_id);            

            $report = $this->bill_model->get_service_by_booking_id($booking_id);

            $file = generate_bill($customer, $report, 0);
            
            if($file){
                send_mail($customer->email_address, $customer, $file, 0);
            }
                
            
        }
        
        function send_email_notifications($content,$emailid)
        {
            $this->load->library('email');
            $c_message = 'Thanks for booking with us!';
            $c_message .= $content;
            $b_message = $c_message;
            $html = '<html>
	<head>
	</head>
	<body>
		<div style="margin:0;padding:0;background-color:#ffffff;min-height:100%!important;width:100%!important">
			<center>
				<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#ffffff;height:100%!important;width:100%!important">
					<tbody>
						<tr>
							<td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important"><table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;border:0">
								<tbody>
									<tr>
										<td align="center" valign="top">
											<table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
												<tbody>
												  <tr>
													<td valign="top"></td>
												  </tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td align="center" valign="top">
											<table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
													<tr>
														<td valign="top">
														<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
															<tbody>
																<tr>
																	<td valign="top" style="padding:0px">
																	<table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
																		<tbody>
																			<tr>
																				<td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0"><img align="left" alt="" src="'. base_url() .'images/elitemaid_emaid_banner.jpg" width="800" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
																					<div class="a6S" dir="ltr" style="opacity: 0.01; left: 632px; top: 304px;">
																					<div id=":1dx" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
																						<div class="aSK J-J5-Ji aYr"></div>
																					</div>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																	</td>
																</tr>
															</tbody>
														</table>
														<table width="800" cellspacing="0" cellpadding="0" border="0" align="left" style="border-collapse:collapse">
															<tbody>
																<tr>
																	<td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
																   <br>
																	Dear Customer,<br>
																	<br>
																	<br> ' . $b_message . '<br>

																	<br>
																		Thanks & Regards<br/>
																		Elitemaids Team<br />
																	</td>
																</tr>
															</tbody>
														</table>
														<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
															<tbody>
															<tr>
															  <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
																  <tbody>
																	<tr>
																	  <td valign="top" style="padding-top:40px;padding-right:18px;padding-bottom:20px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left;"><div style="text-align:center"><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">Now you can easily book, modify or cancel your appointment<br>Download the Elitemaids app in the following stores:</span><br>
																		  <br style="color:#606060;text-align:center;font-family:helvetica;font-size:15px;line-height:22.5px">
																		  <a href="https://apps.apple.com/in/app/elite-maids/id1515735490" style="word-wrap:break-word;color:#606060;text-align:center;font-family:helvetica;font-size:15px;line-height:22.5px;font-weight:normal;text-decoration:underline" target="_blank"><img align="none" height="44" src="'. base_url().'images/ios.png" style="border:0px;outline:none;text-decoration:none;min-height:44px;width:150px" width="150" class="CToWUd"></a><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">&nbsp;</span>
																		  <a href="https://play.google.com/store/apps/details?id=com.emaid.elitemaids" style="word-wrap:break-word;color:#606060;text-align:center;font-family:helvetica;font-size:15px;line-height:22.5px;font-weight:normal;text-decoration:underline" target="_blank"><img align="none" height="44" src="'. base_url().'images/android.png" style="border:0px;outline:none;text-decoration:none;min-height:44px;width:150px" width="150" class="CToWUd"></a><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">&nbsp;</span><span style="color:#606060;font-family:helvetica;font-size:15px;line-height:22.5px;text-align:center">&nbsp;</span></div></td>
																	</tr>
																  </tbody>
																</table></td>
															</tr>
															</tbody>
														</table>
														<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
														  <tbody>
															<tr>
															  <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
																  <tbody>
																	<tr>
																	  <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"><div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright &copy; 2021 Elitemaids.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
																		  <span style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">You are receiving this email because you signed up to Elitemaids</span></div></td>
																	</tr>
																  </tbody>
																</table></td>
															</tr>
														  </tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
				</center>
			</div>
		</body>
            </html>';
			// $config = array(
			// 	'protocol'  => 'smtp',
			// 	'smtp_host' => 'ssl://smtp.googlemail.com',
			// 	'smtp_port' => 465,
			// 	'smtp_user' => 'info@elitemaids.emaid.info',
			// 	'smtp_pass' => 'Advance1234',
			// 	'mailtype'  => 'html',
			// 	'charset'   => 'utf-8'
			// );
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'smtp.sendgrid.net',
				'smtp_user' => 'apikey',
				// 'smtp_pass' => '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',
				'smtp_pass' => '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',
				'smtp_port' => 587,
				'mailtype'  => 'html',
				'crlf' => "\r\n",
				'newline' => "\r\n"
			  );
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
			// $this->email->from('online@elitemaids.emaid.info', 'ELITEMAIDS');
			$this->email->to($emailid);
			$this->email->subject('Booking Confirmation Mail');
			$this->email->message($html);
			$this->email->send();
        }
        
        function send_sms_notifications($content,$mobile)
        {
			//echo '<pre>';
            $smsdetails = $this->settings_model->get_sms_info();
			//print_r($smsdetails);die;

            //Please Enter Your Details
            $user=$smsdetails[0]['user'];//"mymaidapi"; //your username
            $password=$smsdetails[0]['pass'];//"Knkmz7ig"; //your password
            //$m_number = "56456654645";
            $final_number = substr(trim(preg_replace('/[^0-9]*/', '', $mobile)), -9);
            $mobilenumbers = "971".$final_number; //enter Mobile numbers comma seperated
            $message = $content; //enter Your Message
            $senderid=$smsdetails[0]['sender_id'];//"800 MyMaid"; //Your senderid
            $messagetype="N"; //Type Of Your Message
            $DReports="Y"; //Delivery Reports
            $url=$smsdetails[0]['api_url'];
            $message = urlencode($message);
            $ch = curl_init();
            if (!$ch){die("Couldn't initialize a cURL handle");}
            $ret = curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,"User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
            $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            //If you are behind proxy then please uncomment below line and provide your proxy ip with port.
            // $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
            $curlresponse = curl_exec($ch); // execute
            if(curl_errno($ch))
            echo 'curl error : '. curl_error($ch);
            if (empty($ret)) {
                // some kind of an error happened
                die(curl_error($ch));
                curl_close($ch); // close cURL handler
            } else {
                $info = curl_getinfo($ch);
                curl_close($ch); // close cURL handler  
                return $curlresponse; //echo "Message Sent Succesfully" ;
            }
        }
        
        function approvallist() 
        {
                ini_set('memory_limit','-1');
                if($this->input->is_ajax_request())
		{
			if($this->input->post('action') && $this->input->post('action') == 'reject-booking')
			{
                            if($this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
                            {
                                $booking_id = $this->input->post('booking_id');
                                
                                $delete_affected = $this->bookings_model->reject_booking($booking_id);
                                //$delete_affected = 1;
                                if($delete_affected > 0)
                                {
                                    $response = array();
                                    $response['status'] = 'success';
                                    
                                    //$this->sent_booking_confirmation_mail($booking_id, 'Rejected');
                                    echo json_encode($response);
                                    exit();
                                }
                                else
                                {
                                    $response = array();
                                    $response['status'] = 'error';
                                    $response['message'] = 'Invalid booking id';
                                    
                                    echo json_encode($response);
                                    exit();
                                }
                            }
                            else
                            {
                                $response = array();
                                $response['status'] = 'error';
                                $response['message'] = 'Unexpected error!';

                                echo json_encode($response);
                                exit();
                            }
                        } 
                        if($this->input->post('action') && $this->input->post('action') == 'get-free-maids' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0)
			{
				$booking_id = trim($this->input->post('booking_id'));
				$booking = $this->bookings_model->get_booking_by_id($booking_id);
                                $same_zone = $this->input->post('same_zone');
                                
                                $not_free_maids = $this->bookings_model->get_not_free_maids_by_booking_id($booking_id, $booking->service_week_day, /*$booking->service_start_date . ' ' .*/$booking->time_from, /*$booking->service_start_date . ' ' .*/ $booking->time_to, $booking->service_start_date);
                                $all_maids = $this->maids_model->get_maids();
                                
                                
                                $time_from_stamp = strtotime(trim($booking->time_from));
                                $time_to_stamp = strtotime(trim($booking->time_to));
                                $booking_type = $booking->booking_type;
                                $service_date = $booking->service_start_date;
                                $service_end_date = $booking->service_end_date;
                                
                                $nf_maids = array();
                                $all_maid_list = array();    
                                
                                foreach ($not_free_maids as $nmaid)
                                {
                                    
                                    $maid_id = $nmaid->maid_id;
                                    $f_time = strtotime($nmaid->time_from);
                                    $t_time = strtotime($nmaid->time_to);
                                    if($booking_type == 'OD')
                                    {
                                            
                                            //if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                            //{
                                                    //continue;
                                                   
                                                
                                            //}
                                            //else
                                            //{
                                                if(($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp > $t_time) || ($time_from_stamp < $f_time && $time_to_stamp > $f_time && $time_to_stamp < $t_time/**/) || ($time_from_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp <= $t_time))
                                                {
                                                    array_push($nf_maids, $nmaid->maid_id);
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                                
                                            //}
                                                    
                                    }

                                    $today_week_day = date('w', strtotime(date('Y-m-d')));                                    
                                    $repeat_days = array(date('w', strtotime($service_date)));
                                    
                                    if($booking_type == 'WE')
                                    {
                                            
                                            foreach($repeat_days as $repeat_day)
                                            {
                                                    if($repeat_day < $today_week_day)
                                                    {
                                                            $day_diff = (6 - $today_week_day + $repeat_day + 1);
                                                    }
                                                    else
                                                    {
                                                            $day_diff = $repeat_day - $today_week_day;
                                                    }

                                                    $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

                                                    $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
                                                    foreach($bookings_on_day as $booking_on_day)
                                                    {
                                                            $s_date_stamp = strtotime($service_start_date);
                                                            $e_date_stamp = strtotime($service_end_date);
                                                            $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                                            $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);

                                                            //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                                            if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                                            {
                                                                    $f_time = strtotime($booking_on_day->time_from);
                                                                    $t_time = strtotime($booking_on_day->time_to);

                                                                    if(($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp > $t_time) || ($time_from_stamp < $f_time && $time_to_stamp > $f_time && $time_to_stamp < $t_time/**/) || ($time_from_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp > $f_time && $time_from_stamp < $t_time && $time_to_stamp <= $t_time))
                                                                    {
                                                                        array_push($nf_maids, $nmaid->maid_id);
                                                                    }
                                                                    else
                                                                    {
                                                                        continue;
                                                                    }
                                                            }
                                                    }
                                            }
                                    }
                                }
                                
                                foreach ($all_maids as $maid){
                                    array_push($all_maid_list, $maid->maid_id);
                                }
                                $free_maids = array_diff($all_maid_list, $nf_maids);     
                                
                                 
                                if(!empty($free_maids))
                                {
                                    //$f_maids = array_diff($free_maids, $not_free_maids);
                                    $free_maid_dtls = array();
                                    $free_maid_ids = array();
                                    foreach ($free_maids as $f_maid){
                                        $maid = $this->maids_model->get_maid_by_id($f_maid);
                                        array_push($free_maid_dtls, $maid);
                                        array_push($free_maid_ids, $f_maid);
                                    }
                                    if($same_zone == 1)
                                    {
                                        $zone = $this->customers_model->get_customer_zone_by_address_id($booking->customer_address_id);
                                        $same_zone_maids = $this->bookings_model->get_same_zone_maids($zone->zone_id, $free_maid_ids, $booking->service_week_day);
                                        $free_maid_dtls = array();
                                        foreach ($same_zone_maids as $same_zone_maid){
                                            $maid = $this->maids_model->get_maid_by_id($same_zone_maid->maid_id);
                                            array_push($free_maid_dtls, $maid);                                            
                                        }
                                        if(empty($free_maid_dtls))
                                        {                                           
                                            $return = array();
                                            $return['status'] = 'error';
                                            $return['message'] = 'There are no same zone maids available';

                                            echo json_encode($return);
                                            exit();
                                            
                                        }
                                    }
                                    echo json_encode($free_maid_dtls);
                                    exit();
                                }
                                else
                                {
                                    $return = array();
                                    $return['status'] = 'error';
                                    $return['message'] = 'There are no maids available for the selected time slot';

                                    echo json_encode($return);
                                    exit();
                                }
			}
                        if($this->input->post('action') && $this->input->post('action') == 'assign-maid-status'  )
                        {
                            
                                $booking_id = $this->input->post('booking_id');
                                $update_booking_fields = array();
                                $update_booking_fields['booked_by'] = user_authenticate();
                                $update_booking_fields['booking_status'] = 1; 
                                
                                $affected_rows = $this->bookings_model->update_booking($booking_id, $update_booking_fields);
                                $response = array();
                                $response['status'] = 'success';
                                    
                                echo json_encode($response);
                                exit();
                            
                        }
                        if($this->input->post('action') && $this->input->post('action') == 'delete-assign-maid-status'  )
                        { 
                            $booking_id = $this->input->post('booking_id');
                            if($this->input->post('remarks') != "")
                            {
                                $remarks = $this->input->post('remarks');
                            } else {
                                $remarks = "";
                            }
                            $datefordelete = date('d-M-Y');
                            $service_datefordelete = date('Y-m-d', strtotime($datefordelete));

                            $d_booking1 = $this->bookings_model->get_booking_by_id($booking_id);
							if(user_authenticate() != 1){
                            if($d_booking1->is_locked == 1 && $d_booking1->booked_by != user_authenticate())
                            {
                                echo 'locked';
                                exit();
                            }
							}
                            if(isset($d_booking1->booking_id))
                            {
                                if($d_booking1->booking_type == 'OD')
                                {
                                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'delete_remarks' => $remarks), 'Delete');
                                    $delete_b_fields = array();
                                    $delete_b_fields['booking_id'] = $booking_id;
                                    $delete_b_fields['service_date'] = $service_datefordelete;
                                    $delete_b_fields['deleted_by'] = user_authenticate();

                                    $affectedrows_del = $this->bookings_model->add_booking_cancel($delete_b_fields);
                                    $response = array();
                                    $response['status'] = 'success';
                                }
                                else
                                {
                                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2, 'service_actual_end_date' => date('Y-m-d', strtotime($datefordelete . ' - 1 day')), 'service_end' => 1,'delete_remarks' => $remarks), 'Delete');
                                    $delete_b_fields = array();
                                    $delete_b_fields['booking_id'] = $booking_id;
                                    $delete_b_fields['service_date'] = $service_datefordelete;
                                    $delete_b_fields['deleted_by'] = user_authenticate();

                                    $affectedrows_del = $this->bookings_model->add_booking_cancel($delete_b_fields);
                                    $response = array();
                                    $response['status'] = 'success';
                                }
                                echo json_encode($response);
                                exit();
                            }
                            
                        }
                       
                        if($this->input->post('action') && $this->input->post('action') == 'assign-maid' && $this->input->post('booking_id') && is_numeric($this->input->post('booking_id')) && $this->input->post('booking_id') > 0 && $this->input->post('maid_id') )//&& is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0
                        {
                            
                                $booking_id = $this->input->post('booking_id');
                                $booking = $this->bookings_model->get_booking_by_id($booking_id);
                                $maid_id = explode(",", trim($this->input->post('maid_id')));                               
                                
                                $update_booking_fields = array();
                                $update_booking_fields['maid_id'] = $maid_id[0];
                                $update_booking_fields['booked_by'] = user_authenticate();
                                $update_booking_fields['booking_status'] = 1;
                                
                                $affected_rows = $this->bookings_model->update_booking($booking_id, $update_booking_fields);
                                
                                array_shift($maid_id);
                                
                                if(is_array($maid_id))
                                {
                                        
                                        foreach ($maid_id as $m_id)
                                        {
                                            $booking_fields = array();
                                            $booking_fields['customer_id'] = $booking->customer_id;
                                            $booking_fields['customer_address_id'] = $booking->customer_address_id;
                                            $booking_fields['maid_id'] = $m_id;
                                            $booking_fields['service_type_id'] = $booking->service_type_id;
                                            $booking_fields['service_start_date'] = $booking->service_start_date;
                                            $booking_fields['service_week_day'] = $booking->service_week_day;
                                            $booking_fields['time_from'] = $booking->time_from;
                                            $booking_fields['time_to'] = $booking->time_to;
                                            $booking_fields['booking_type'] = $booking->booking_type;
                                            $booking_fields['service_end'] = $booking->service_end;
                                            $booking_fields['service_end_date'] = $booking->service_end_date;					
                                            $booking_fields['booking_note'] = $booking->booking_note;					
                                            $booking_fields['pending_amount'] = $booking->pending_amount;                                        
                                            $booking_fields['discount'] = $booking->discount;
                                            $booking_fields['booked_from'] = 'W';
                                            $booking_fields['booked_by'] = user_authenticate();
                                            $booking_fields['booking_status'] = 1;
											$booking_fields['booking_category'] = 'C';
                                            $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
                                            if($booking->booking_type == 'WE')
                                            {
                                                $updatefield = array();
                                                $updatefield['customer_booktype'] = 1;
                                                $updatess = $this->customers_model->update_booktype($updatefield,$customer_id);
                                            }
                                            $b_id = $this->bookings_model->add_booking($booking_fields);
                                            
                                        }
                                }
                                
                                
                                //$this->sent_booking_confirmation_mail($booking_id, 'Approved');
                                //if($affected_rows > 0)
                                //{
                                    $response = array();
                                    $response['status'] = 'success';
                                    
                                    echo json_encode($response);
                                    exit();
                                //}
                                
                        }
                        else
                        {
                            $response = array();
                            $response['status'] = 'error';
                            $response['message'] = 'Unexpected error!';

                            echo json_encode($response);
                            exit();
                        }
                }
                
                if($this->input->post('filter') && strlen($this->input->post('filter')) > 0)
                {
                    $filter = $this->input->post('filter');
                }
                else
                {
                    $filter = 'Pending';
                }
                
                $data = array();
                $data['approval_list'] = $this->bookings_model->get_all_booking_approval_list($filter);
                $data['filter'] = $filter;
                $book_source=array("M"=>"Mobile","W"=>"Web","A"=>"Admin");
                $data['source']=$book_source;
                $layout_data['content_body'] = $this->load->view('booking_approval_list', $data, TRUE);
                $layout_data['page_title'] = 'Booking Approval List';
                $layout_data['meta_description'] = 'Booking Approval List';
                $layout_data['css_files'] = array('jquery.fancybox.css','demo.css');
                $layout_data['external_js_files'] = array();
                 $layout_data['bookings_active'] = '1';
                $layout_data['js_files'] = array('jquery.fancybox.pack.js','ajaxupload.3.5.js','bootstrap-datepicker.js','base.js', 'mymaids.js', 'moment.min.js','jquery.dataTables.min.js', 'bootstrap.js'); //'hm.js');
                $this->load->view('layouts/default', $layout_data);
                
        }
        function sent_booking_confirmation_mail($booking_id,$approved_status)
        {
            //$booking_id=18777;$approved_status='Approved';
            $booking = $this->bookings_model->get_booking_by_id($booking_id);
            
            $service_start_date = date("d/m/Y", strtotime($booking->service_start_date));
            $customer_name = $booking->customer_name;
            $customer_mobile = $booking->mobile_number_1;
            $email_id = $booking->email_address;
            $area = $booking->area_name;
            $address = $booking->customer_address;
            $start_time = date("h:i a", strtotime($booking->time_from)); 
            $end_time =date("h:i a", strtotime($booking->time_to)); 
            $booking_type= $booking->booking_type == 'OD' ? 'One Day' : 'Every Week'; 
            
            $message = '';
            if($approved_status == 'Approved')
            {
                $message = "Thank you for booking with us. We are pleased to inform you that your booking has been approved. ";
            }
            else
            {
                $message = "Thank you for booking with us. We regret that we can't process your request because of the unavailabilty of maids on that time slot. ";
            }

                //$this->load->library('email');

                $html = '<html>
                        <head>
                        </head>
                        <body>
                            <div style="margin:0;padding:0;background-color:#ffffff;min-height:100%!important;width:100%!important">
                                <center>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#ffffff;">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important">

                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;border:0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top" style="padding-top:9px"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td valign="top" style="padding:0px">
                                                                                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0">


                                                                                                                    <img align="left" alt="" src="'. base_url() . 'images/elitemaid_emaid_banner.jpg" width="800" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 1; left: 629px; top: 304px;"><div id=":1z9" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>


                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td valign="top">

                                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                                        <tbody>
                                                                                                            <tr>

                                                                                                                <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                                                                                                                    <br>
                                                                                                                    Dear ' . $customer_name . ',<br>
                                                                                                                    <br>
                                                                                                                    ' . $message . '<br>
                                                                                                                    <br>
                                                                                                                    <u>Booking ID - <span style="color:#C33F4A;">' . 'EM' . $booking_id . '</span>&nbsp;</u><br>
                                                                                                                    <br>
                                                                                                                    Address - <i>' . nl2br($address) . '</i><br>
                                                                                                                    Area - <i>' . $area . '</i>&nbsp;<br>
                                                                                                                    Service Date - <i>' . $service_start_date . ' ('. $booking_type .')' . '</i>&nbsp;<br>
                                                                                                                    Shift - <i>' . $start_time . ' - ' . $end_time . '</i>&nbsp;<br>
                                                                                                                    <br>
                                                                                                                    
                                                                                                                    <br>
                                                                                                                    Keep booking with us<br /><br />
                                                                                                                    
                                                                                                                    Elitemaids<br>
                                                                                                                    Office 201B, Prime Business Center,<br>
                                                                                                                    Jumeirah Village Circle,<br>
                                                                                                                    Dubai, U.A.E.<br>
                                                                                                                    For Bookings : 800 258 / +971 58 286 4783 <br>
                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td valign="top">

                                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                                        <tbody>
                                                                                                            <tr>

                                                                                                                <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                                                                                                                    <div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright © 2020 Elitemaids.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                                                                                                    </div>



                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td valign="top">

                                                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse">
                                                                                                        <tbody><tr>

                                                                                                                <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">


                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top" width="50%">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td valign="top"></td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                                <td align="left" valign="top" width="50%">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td valign="top"></td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" valign="top">

                                                                    <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td valign="top" style="padding-bottom:9px"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                        </tbody></table>
                                </center>
                            </div>
                        </body>
                    </html>';
                
				// $config = array(
				// 	'protocol'  => 'smtp',
				// 	'smtp_host' => 'ssl://smtp.googlemail.com',
				// 	'smtp_port' => 465,
				// 	'smtp_user' => 'info@elitemaids.emaid.info',
				// 	'smtp_pass' => 'Advance1234',
				// 	'mailtype'  => 'html',
				// 	'charset'   => 'utf-8'
				// );
				// $this->email->initialize($config);
				// $this->email->set_mailtype("html");
				// $this->email->set_newline("\r\n");
				// $this->email->from('info@elitemaids.emaid.info', 'ELITEMAIDS');
				// $this->email->to($email_id);
				// $this->email->subject('Elitemaids - Booking Status');
				// $this->email->message($html);
				// $this->email->send();

            $this->load->library('email');
            $this->email->initialize(array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'smtp.sendgrid.net',
			  'smtp_user' => 'apikey',
			//   'smtp_pass' => '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',
			'smtp_pass' => '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',
			'smtp_port' => 587,
			  'mailtype'  => 'html',
			  'crlf' => "\r\n",
			  'newline' => "\r\n"
			));
            //$email_id='jose.robert@azinova.info';
            //$this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->initialize($config);
            $this->email->from('info@elitemaids.emaid.info', 'Elitemaids');
            $this->email->to($email_id);

            $this->email->subject('Elitemaids - Booking Status');

            $this->email->message($html);

            $mailstat=$this->email->send();
        }
        
        public function register_contact_exists()
        {
            if (array_key_exists('phone',$_POST))
            {
                if ( $this->bookings_model->phone_exists($this->input->post('phone')) == TRUE )
                {
                    echo json_encode(FALSE);
                } else {
                        echo json_encode(TRUE);
                }
            }
        }
        
        function get_price_booking($booking_id)
        {
          
           $booking = $this->bookings_model->get_booking_by_id($booking_id);
           $normal_hours = 0;
           $extra_hours = 0;
           $weekend_hours = 0;
           $normal_from = strtotime('08:00:00');
           $normal_to = strtotime('18:00:00');

           $shift_from = strtotime($booking->time_from . ':00');
           $shift_to = strtotime($booking->time_to . ':00');

            if(date('w') >= 0 && date('w') < 5)
               {					
                  if($shift_from < $normal_from)
                    {
                      if($shift_to <= $normal_from)
                        {
                           $extra_hours = ($shift_to - $shift_from) / 3600;
                        }

                        if($shift_to > $normal_from && $shift_to <= $normal_to)
                           {
                             $extra_hours = ($normal_from - $shift_from) / 3600;
                             $normal_hours = ($shift_to - $normal_from) / 3600;
                           }

                        if($shift_to > $normal_to)
                           {
                             $extra_hours = ($normal_from - $shift_from) / 3600;
                             $extra_hours += ($shift_to - $normal_to) / 3600;
                             $normal_hours = ($normal_to - $normal_from) / 3600;
                           }
                    }

                    if($shift_from >= $normal_from && $shift_from < $normal_to)
                           {
                              if($shift_to <= $normal_to)
                                {
                                   $normal_hours = ($shift_to - $shift_from) / 3600;
                                }

                                if($shift_to > $normal_to)
                                {
                                   $normal_hours = ($normal_to - $shift_from) / 3600;
                                   $extra_hours = ($shift_to - $normal_to) / 3600;
                                 }
                            }

                     if($shift_from > $normal_to)
                                 {
                                    $extra_hours = ($shift_to - $shift_from) / 3600;
                                 }
                    }
                    else
                        {
                            $weekend_hours = ($shift_to - $shift_from) / 3600;
                        }

                                $service_description = array();

                                $service_description['normal'] = new stdClass();
                                $service_description['normal']->hours = $normal_hours;
                                $service_description['normal']->fees = $normal_hours * $booking->price_hourly;

                                $service_description['extra'] = new stdClass();
                                $service_description['extra']->hours = $extra_hours;
                                $service_description['extra']->fees = $extra_hours * $booking->price_extra;

                                $service_description['weekend'] = new stdClass();
                                $service_description['weekend']->hours = $weekend_hours;
                                $service_description['weekend']->fees = $weekend_hours * $booking->price_weekend;

                                $location_charge = 0;
                               

                                $total_fee = $service_description['normal']->fees + $service_description['extra']->fees + $service_description['weekend']->fees+ $location_charge - $booking->discount; 
            
                                return $total_fee;
        }
	
        
       public function add_customer_popup()
       {
         $customer_name = $this->input->post('customer_name');
         $customer_email = $this->input->post('customer_email'); 
         $area = $this->input->post('area');
         $apartmentno = $this->input->post('apartmentnos');
         $address = $this->input->post('address');
         $customer_phone = $this->input->post('mobile_number1');
         $user_name = $customer_email;
         $password=$customer_phone;
         $added = date('Y-m-d h:i:s');
         $data = array(
                'customer_password' => $password,
                'customer_username' => $user_name,
                'customer_name' => $customer_name,
                'customer_nick_name' => $customer_name,
                'mobile_number_1' => $customer_phone,
                'email_address' => $customer_email,
                'customer_type' => 'HO',
                'customer_booktype' =>0,
                'payment_type' =>'D',
                'payment_mode' =>'Cash',
                'price_hourly' => 40,
                'price_extra' =>40,
                'price_weekend' =>45,
                'customer_status' => 1,
                'customer_added_datetime' => $added,
            );
         $res = $this->customers_model->add_customers($data);
         $datas = array(
                    'customer_id' => $res,
                    'building' => $apartmentno,
                    'area_id' => $area,
                    'customer_address' =>$address,
                );
         $this->customers_model->add_customer_address($datas);
         $customers = $this->customers_model->get_customers_byid($res);
         $options = "";
          foreach($customers as $customer)
							{  
                                                                if($customer->payment_type == "D")
                                    {
                                                                    $paytype = "(Daily)";
                                                                } else if($customer->payment_type == "W")
                                                                {
                                                                    $paytype = "(Weekly)";
                                                                } else if($customer->payment_type == "M")
                                                                {
                                                                    $paytype = "(Monthly)";
                                                                } else
                                                                {
                                                                    $paytype = "";
                                    } 
                                                                $p_number = array();
                                                                if($customer->phone_number != NULL)
                                                                    array_push ($p_number, $customer->phone_number);
                                                                if($customer->mobile_number_1 != NULL)
                                                                    array_push ($p_number, $customer->mobile_number_1);
                                                                if($customer->mobile_number_2 != NULL)
                                                                    array_push ($p_number, $customer->mobile_number_2);
                                                                if(@$customer->mobile_numebr_3 != NULL)
                                                                    array_push ($p_number, @$customer->mobile_number_3);
                                                                
                                                                $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';
								$options.= '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
							}
         
         
         $response = array();
            $response['html'] = $options;
            $response['cust_id'] = $res;

            echo json_encode($response);
         //echo $options;
         exit;
       }
       function get_time_difference($time1, $time2) {
	$time1 = strtotime("1/1/1980 $time1");
	$time2 = strtotime("1/1/1980 $time2");
	if ($time2 < $time1) {
		$time2 = $time2 + 86400;
	}
	return ($time2 - $time1) / 3600;
}



    /*
     * author : Jiby
     * Purpose : calculating  booked weekly hours for customers while booking for price calculation
     * Date : 12-10-17
     */
    public function customer_weekly_booking_hrs(){
         $custId    = $this->input->post('customer_id');
            $bookings = $this->customers_model->get_bookings_data_by_customer_id($custId);
            $hrs = 0;
            foreach($bookings as $booking){

                $hr_diff =  (substr($booking->time_to,0,2)-substr($booking->time_from,0,2));
                $hrs = $hrs + intVal($hr_diff);
            }
            echo $hrs;exit;
    //        echo '<pre>';print_r($bookings);
    }
    public function locations()
    {
        //$current_date = date('Y-m-d');
        if($this->input->post('service_date'))
        {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        }
        else
        {                
            $date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        
        
        
        $get_today_job_details = $this->bookings_model->get_today_job_details($date);
        
        
        $data = array();
        $data['formatted_date'] = $date;
        $data['service_date'] = date('d/m/Y', strtotime($date));
        $data['dataarray'] = $get_today_job_details;//\array_map('array_values', $get_today_job_details);
        //$job_json = json_encode($dataarray);
        
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('booking_locations', $data, TRUE);		
        $layout_data['page_title'] = 'Maps';
        $layout_data['meta_description'] = 'Maps';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['maps_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js','dataTables.responsive.min.js','dataTables.checkboxes.min.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    public function closest_locations_job()
    {
        $location =json_decode( preg_replace('/\\\"/',"\"",$_POST['data']));
        $current_date = $location->servicedate;
        $get_today_job_details = $this->bookings_model->get_today_job_details($current_date);
        $dataarray = array_map('array_values', $get_today_job_details);
        echo json_encode($dataarray);
    }
    
    public function new_booking()
    {
        if($this->input->is_ajax_request())
        {
            if($this->input->post('customer_name'))
            {
                $customer_name = $this->input->post('customer_name');
                $customer_email = $this->input->post('customer_email'); 
                $area = $this->input->post('area');
                $apartmentno = $this->input->post('apartmentnos');
                $address = $this->input->post('address');
                $customer_phone = $this->input->post('mobile_number1');
                $user_name = $customer_email;
                $password=$customer_phone;
                $added = date('Y-m-d h:i:s');
                $data = array(
                    'customer_password' => $password,
                    'customer_username' => $user_name,
                    'customer_name' => $customer_name,
                    'customer_nick_name' => $customer_name,
                    'mobile_number_1' => $customer_phone,
                    'email_address' => $customer_email,
                    'customer_type' => 'HO',
                    'customer_booktype' =>0,
                    'payment_type' =>'D',
                    'payment_mode' =>'Cash',
                    'price_hourly' => 40,
                    'price_extra' =>40,
                    'price_weekend' =>45,
                    'customer_status' => 1,
                    'customer_added_datetime' => $added,
                );
                $res = $this->customers_model->add_customers($data);
                $datas = array(
                    'customer_id' => $res,
                    'building' => $apartmentno,
                    'area_id' => $area,
                    'customer_address' =>$address,
                );
                $this->customers_model->add_customer_address($datas);
                $customers = $this->customers_model->get_customers();
                $options = "";
                foreach($customers as $customer)
                {  
                    if($customer->payment_type == "D")
                    {
                        $paytype = "(Daily)";
                    } else if($customer->payment_type == "W")
                    {
                        $paytype = "(Weekly)";
                    } else if($customer->payment_type == "M")
                    {
                        $paytype = "(Monthly)";
                    } else
                    {
                        $paytype = "";
                    } 
                    $p_number = array();
                    if($customer->phone_number != NULL)
                        array_push ($p_number, $customer->phone_number);
                    if($customer->mobile_number_1 != NULL)
                        array_push ($p_number, $customer->mobile_number_1);
                    if($customer->mobile_number_2 != NULL)
                        array_push ($p_number, $customer->mobile_number_2);
                    if(@$customer->mobile_numebr_3 != NULL)
                        array_push ($p_number, @$customer->mobile_number_3);

                    $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';
                    $options.= '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
                }
                $response = array();
                $response['html'] = $options;
                $response['cust_id'] = $res;

                echo json_encode($response);
                //echo $options;
                exit;
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-no-of-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode(array("address" => $customer_addresses, "no_of_address" => count($customer_addresses))); 
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-details-customer' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_details = $this->customers_model->get_customer_by_id($customer_id);


                echo json_encode($customer_details); 
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'get-customer-address' && $this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0)
            {
                $customer_id = trim($this->input->post('customer_id'));

                $customer_addresses = $this->customers_model->get_customer_addresses($customer_id);


                echo json_encode($customer_addresses); 
                exit();
            }
            
            if($this->input->post('action') && $this->input->post('action') == 'book-maid-new')
            {
                if($this->input->post('customer_id') && is_numeric($this->input->post('customer_id')) && $this->input->post('customer_id') > 0 && $this->input->post('customer_address_id') && is_numeric($this->input->post('customer_address_id')) && $this->input->post('customer_address_id') > 0 && $this->input->post('maid_id') && is_numeric($this->input->post('maid_id')) && $this->input->post('maid_id') > 0 && $this->input->post('service_type_id') && is_numeric($this->input->post('service_type_id')) && $this->input->post('service_type_id') > 0 && $this->input->post('time_from') && is_numeric($this->input->post('time_from')) && $this->input->post('time_from') > 0 && $this->input->post('time_to') && is_numeric($this->input->post('time_to')) && $this->input->post('time_to') > 0 && $this->input->post('time_from') < $this->input->post('time_to') && $this->input->post('booking_type') && ($this->input->post('booking_type') == 'OD' || $this->input->post('booking_type') == 'WE' || $this->input->post('booking_type') == 'BW'))
                { 		
                    $customer_id = trim($this->input->post('customer_id'));
                    $customer_address_id = trim($this->input->post('customer_address_id'));
                    $maid_id = trim($this->input->post('maid_id'));
                    $service_type_id = trim($this->input->post('service_type_id'));
                    
                    $s_date = explode('/', trim($this->input->post('service_date')));
                    if(count($s_date) == 3 && checkdate($s_date[1], $s_date[0], $s_date[2]) && $s_date[2] >= 2014 && $s_date[2] <= date('Y') + 2)
                    {
                            $service_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
                    }
                    $service_week_day = date('w', strtotime($service_date));
                    
                    $time_from =  date('H:i:s', trim($this->input->post('time_from')));
                    $time_to = date('H:i:s', trim($this->input->post('time_to')));
                    $booking_type = trim($this->input->post('booking_type'));
                    $is_locked = $this->input->post('is_locked') ? 1 : 0;
                    $repeat_days = array($service_week_day);
                    $repeat_end = 'ondate';
                    $service_end_date = $service_date;
                    $cleaning_material = $this->input->post('cleaning_mat');
                    $total_amt = trim($this->input->post('total_amt'));
                    $booking_note = $this->input->post('notes') ? trim($this->input->post('notes')) : '';
					
                    if(preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_from) == FALSE || preg_match("/([01]?[0-9]|2[0-3]):[0-5][0-9]/", $time_to) == FALSE)
                    {
                            echo 'refresh';
                            exit();
                    }
					
                    if($booking_type != 'OD')
                    {
                        if($this->input->post('repeat_days') && is_array($this->input->post('repeat_days')) && count($this->input->post('repeat_days')) > 0 &&  count($this->input->post('repeat_days')) <= 7 && $this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate'))
                        {
                            $repeat_days = $this->input->post('repeat_days');
                            $repeat_end = $this->input->post('repeat_end');
							
                            if($repeat_end == 'ondate')
                            {
                                if($this->input->post('repeat_end_date'))
                                {
                                    $repeat_end_date = $this->input->post('repeat_end_date');
									
                                    $repeat_end_date_split = explode('/', $repeat_end_date);
                                    if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
                                    {
                                        $s_date = new DateTime($service_date);
                                        $e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
                                        //$diff = $s_date->diff($e_date);

                                        $diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

                                        $years = floor($diff / (365*60*60*24));
                                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                                                                
                                        if($days < 7)//if($diff->days < 7)
                                        {
                                                echo 'refresh';
                                                exit();
                                        }
										
                                        $service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
                                    }
                                    else 
                                    {
                                        echo 'refresh';
                                        exit();
                                    }
                                }
                                else
                                {
                                    echo 'refresh';
                                    exit();
                                }
                            }
                        }
                        else 
                        {
                                echo 'refresh';
                                exit();
                        }
                    }
					
                    $time_from_stamp = trim($this->input->post('time_from'));
                    $time_to_stamp = trim($this->input->post('time_to'));
					
                    $today_week_day = date('w', strtotime($service_date));
					
                    if($booking_type == 'WE')
                    {
                        foreach($repeat_days as $repeat_day)
                        {
                            if($repeat_day < $today_week_day)
                            {
                                $day_diff = (6 - $today_week_day + $repeat_day + 1);
                            } else
                            {
                                    $day_diff = $repeat_day - $today_week_day;
                            }

                            $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
                                                        
                                                  
                            foreach($bookings_on_day as $booking_on_day)
                            {
                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_end_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
								
                                if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                {
                                    $f_time = strtotime($booking_on_day->time_from);
                                    $t_time = strtotime($booking_on_day->time_to);
                                    if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) )//|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
                                    {
                                        //echo "<br>".$booking_on_day->booking_id;
                                        $return = array();
                                        $return['status'] = 'error';
                                        $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                        echo json_encode($return);
                                        exit();
                                    }                                   
                                }                               
                            }
                        }
                    }
                    else if($booking_type == 'BW')
                    {
                        foreach($repeat_days as $repeat_day)
                        {
                            if($repeat_day < $today_week_day)
                            {
                                $day_diff = (6 - $today_week_day + $repeat_day + 1);
                            }
                            else
                            {
                                    $day_diff = $repeat_day - $today_week_day;
                            }

                            $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));

                            //$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day_new($service_start_date,$maid_id, $repeat_day);
                            $bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($maid_id, $repeat_day);
//                                                echo '<pre>';
//                                                print_r($bookings_on_day);
//                                                exit();
                            foreach($bookings_on_day as $booking_on_day)
                            {
                                $s_date_stamp = strtotime($service_start_date);
                                $e_date_stamp = strtotime($service_end_date);
                                $bs_date_stamp = strtotime($booking_on_day->service_start_date);
                                $be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
                                                    
                                if($booking_on_day->booking_type == "BW")
                                {
                                    $now = strtotime($service_start_date); // or your date as well
                                    $your_date = strtotime($booking_on_day->service_start_date);
                                    $datediff = $now - $your_date;

                                    $week_diff = round($datediff / (60 * 60 * 24));
                                    $week_difference = fmod($week_diff,14);
                                                        
                                    if($week_difference == 0 || $week_difference == '-0')
                                    {
                                        $f_time = strtotime($booking_on_day->time_from);
                                        $t_time = strtotime($booking_on_day->time_to);

                                        if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                        {
                                            $return = array();
                                            $return['status'] = 'error';
                                            $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                            echo json_encode($return);
                                            exit();
                                        }
                                    }
                                } else {
                                    //if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                    //edited by vishnu
                                    if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                    //latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
                                    //if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
                                    {
                                        $f_time = strtotime($booking_on_day->time_from);
                                        $t_time = strtotime($booking_on_day->time_to);

                                        if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
                                        {
                                            $return = array();
                                            $return['status'] = 'error';
                                            $return['message'] = 'The selected time slot is not available for this maid on ' . $week_day_names[$repeat_day] . 's';

                                            echo json_encode($return);
                                            exit();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo 'refresh';
                    exit();
                }
				
                $todays_new_booking = array();
                foreach($repeat_days as $repeat_day)
                {
                    if($repeat_day < $today_week_day)
                    {
                        $day_diff = (6 - $today_week_day + $repeat_day + 1);
                    }
                    else
                    {
                        $day_diff = $repeat_day - $today_week_day;
                    }
					
                    $service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
                    $service_end_date = $repeat_end == 'never' ? $service_start_date : $service_end_date;

                    $booking_fields = array();
                    $booking_fields['customer_id'] = $customer_id;
                    $booking_fields['customer_address_id'] = $customer_address_id;
                    $booking_fields['maid_id'] = $maid_id;
                    $booking_fields['service_type_id'] = $service_type_id;
                    $booking_fields['service_start_date'] = $service_start_date;
                    $booking_fields['service_week_day'] = $repeat_day;
                    $booking_fields['time_from'] = $time_from;
                    $booking_fields['time_to'] = $time_to;
                    $booking_fields['booking_type'] = $booking_type;
                    $booking_fields['service_end'] = $repeat_end == 'ondate' ? 1 : 0;
                    $booking_fields['service_end_date'] = $service_end_date;					
                    $booking_fields['booking_note'] = $booking_note;
                    $booking_fields['is_locked'] = $is_locked;
                    if($cleaning_material == 'Y')
                    {
                        $booking_fields['cleaning_material'] = 'Y';
                    } else {
                        $booking_fields['cleaning_material'] = 'N';
                    }
                    $booking_fields['total_amount'] = $total_amt;
                    $booking_fields['booked_by'] = user_authenticate();
                    $booking_fields['booking_status'] = 1;
					$booking_fields['booking_category'] = 'C';
                    $booking_fields['booked_datetime'] = date('Y-m-d H:i:s');
                    if($booking_type == 'WE')
                    {
                        $updatefield = array();
                        $updatefield['customer_booktype'] = 1;
                        $updatess = $this->customers_model->update_booktype($updatefield,$customer_id);
                    }
                    $booking_id = $this->bookings_model->add_booking($booking_fields);
					
                    if($booking_id)
                    {
                        //Email and SMS Notifications
//                        if($booking_type == "WE"){
//                            $last_sunday = strtotime('last Sunday');
//                            $service_start_date_for_not = date('l', strtotime('+'.$repeat_day.' day', $last_sunday));
//                        } else if($booking_type == "OD"){
//                            $service_start_date_for_not = date('d/m/Y', strtotime($service_start_date));
//                        }
//                        $no_of_hourss = $this->get_time_difference($time_from, $time_to);
//                        $time_from_not =  date('g:i A', strtotime(html_escape($time_from)));
//                        $booking_not = $this->bookings_model->get_booking_by_id($booking_id);
//                        $email_address_not = $booking_not->email_address;
//                        $mobile = $booking_not->mobile_number_1;
//                        $content = "Your scheduled booking on ".$service_start_date_for_not." for ".$no_of_hourss." hrs  from ".$time_from_not." has been confirmed.";
//                        if($email_notifications == "Y")
//                        {
//                            $this->send_email_notifications($content,$email_address_not);
//                        }
//                        if($sms_notifications == 'Y')
//                        {
//                            $sms_send = $this->send_sms_notifications($content,$mobile);
////                                                $sms_val = $this->input->post('sms_val');
////                                                if($sms_val == 'N')
////                                                {
////                                                    //$this->send_sms_notifications($content,$mobile);
////                                                } else {
////                                                    echo $sms_send_date = $this->input->post('sms_send_date');
////                                                    exit();
////                                                    $this->send_sms_notifications_later($content,$mobile);
////                                                }
//                        }
                        $booking_done = TRUE;
                                                
						//$this->appointment_bill($booking_id);
                                                
                        if($service_start_date == date('Y-m-d'))
                        {
                                $todays_new_booking[] = $booking_id;
                        }
                        $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                        if(isset($c_address->zone_id))
                        {
                            $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                            if($tablet)
                            {
                                $booking = $this->bookings_model->get_booking_by_id($booking_id);

                                $push_fields = array();
                                $push_fields['tab_id'] = $tablet->tablet_id;
                                $push_fields['type'] = 1;
								$push_fields['message'] = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                $push_fields['maid_id'] = $booking->maid_id;
								$push_fields['title'] = "New Booking";
								$push_fields['customer_name'] = $booking->customer_name;
								$push_fields['maid_name'] = $booking->maid_name;
								$push_fields['booking_time'] = $booking->newtime_from.' - '.$booking->newtime_to;
                                if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
                                {
                                    $push = $this->bookings_model->add_push_notifications($push_fields);
                                }
                            }
                        }
						
                    }
                }
				
                if(isset($booking_done))
                {
                   if(count($todays_new_booking) > 0 && isset($customer_address_id))
                   {
                       $c_address = $this->customers_model->get_customer_address_by_id($customer_address_id);
                       if(isset($c_address->zone_id))
                       {
                           $tablet = $this->tablets_model->get_tablet_by_zone($c_address->zone_id);
                           $booking = $this->bookings_model->get_booking_by_id($booking_id);
						   $getmaiddeviceid = $this->maids_model->get_maid_by_id($booking->maid_id);
                           if($tablet)
                           {
                               if(strtotime($schedule_date) <= strtotime( date('d-M-Y')))
                               {
                                   $deviceid = $tablet->google_reg_id;
                                   // optional payload
                                   $payload = array();
                                   $payload['isfeedback'] = false;
								   if(isset($push) && $push > 0)
									{
										$payload['pushid'] = $push;
									} else {
										$payload['pushid'] = 0;
									}

                                   $title = "New Booking";
                                   $message = 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->newtime_from . '-' . $booking->newtime_to;
                                   $res = array();
                                   $res['title'] = $title;
									$res['is_background'] = false;
									$res['body'] = $message;
									$res['image'] = "";
									$res['payload'] = $payload;
									$res['customer'] = $booking->customer_name;
									$res['maid'] = $booking->maid_name;
									$res['bookingTime'] = $booking->newtime_from.' - '.$booking->newtime_to;
									$res['timestamp'] = date('Y-m-d G:i:s');
									$regId = $deviceid;
									$fields = array(
										'to' => $regId,
										'notification' => $res,
										'data' => $res,
									);
									$return = android_customer_app_push($fields);
									if($getmaiddeviceid->device_id != "")
									{
										$maid_fields = array(
											'to' => $getmaiddeviceid->device_id,
											'notification' => $res,
											'data' => $res,
										);
										android_customer_app_push($maid_fields);
									}
                                   //$return = android_push(array($tablet->google_reg_id), array('action' => 'new-booking', 'booking-ids' => $todays_new_booking, 'message' => 'New Booking. Maid : ' . $booking->maid_name . ', Customer : ' . $booking->customer_name . ', Shift : ' . $booking->time_from . '-' . $booking->time_to));
                               }
                           }
                                                       //print_r($return);exit;
                       }
                   }
					
                    $return = array();
                    $return['status'] = 'success';
                    $return['maid_id'] = $maid_id;
                    $return['customer_id'] = $customer_id;
                    $return['time_from'] = $time_from;
                    $return['time_to'] = $time_to;
					//$user_logged_in_session = $CI->session->userdata('user_logged_in');
                                        //$return['user'] = $user_logged_in_session['user_fullname'];
                                        
                    echo json_encode($return);
                    exit();
                }
            }
        }
        
        $times = array();
        $current_hour_index = 0;
        $time = '04:00 am';  
        $time_stamp = strtotime($time);

        //for ($i = 0; $i < 24; $i++) 
        for ($i = 0; $i < 48; $i++)
        {
                if(!isset($times['t-' . $i]))
                {
                        $times['t-' . $i] = new stdClass();
                }

                $times['t-' . $i]->stamp = $time_stamp;
                $times['t-' . $i]->display = $time;

                if(date('H') == $i)
                //if(date('H') == $i && $service_date == date('Y-m-d'))
                {
                        $current_hour_index = 't-' . (($i *2) - 1);
                    //$current_hour_index = 't-' . ($i - 1);
                }
                //For 1 hour
                //$time_stamp = strtotime('+60mins', strtotime($time));
                //For 30 min
                $time_stamp = strtotime('+30mins', strtotime($time));
                $time = date('g:i a', $time_stamp);
        }
                
                
        
        $data = array();
        //$data['customers'] = $this->customers_model->get_customers();
        $data['service_types'] = $this->service_types_model->get_cleaning_service_types();
        $data['times'] = $times; 
        $data['areas'] = $this->settings_model->get_areas();
        $layout_data['content_body'] = $this->load->view('new_booking_form', $data, TRUE);
        $layout_data['page_title'] = 'Bookings';
        $layout_data['meta_description'] = 'Bookings';
        $layout_data['css_files'] = array('jquery.fancybox.css','datepicker.css', 'bootstrap-datetimepicker.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','bootstrap-datepicker.js','base.js','jquery.validate.min.js', 'normalform.js', 'moment.min.js','jquery.dataTables.min.js', 'bootstrap.js'); //'hm.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    
    public function maids_booking()
    {
        $schedule_date  = $this->input->post('book_date') ;
        $from_time      = $this->input->post('from_time') ;
        $to_time        = $this->input->post('to_time') ;
        $book_type      = $this->input->post('book_type') ;
        $customer_id    = $this->input->post('customer_id') ;
        $address_id   = $this->input->post('cust_address_id') ;
        $repeat_end   = $this->input->post('repeat_end') ;
        $repeat_end_date   = $this->input->post('repeat_end_date') ;
        $location_type  = $this->input->post('location_type') ;
        $location_val   = $this->input->post('location_value') ;
           
        $filter = array();
        $filter['customer_id'] = $customer_id;
        $filter['location_type'] = $location_type;
        $filter['location_val'] = $location_val;
            
        $service_date   =  date('Y-m-d', strtotime(str_replace('/', '-', $schedule_date)));
        $service_week_day   =  date('w', strtotime(str_replace('/', '-', $schedule_date)));
		$repeat_days = array($service_week_day);
		$service_end_date = $service_date;
        $filter['service_date']   = $service_date;
        $filter['from_time']      = $from_time;
        $filter['to_time']        = $to_time;
          
        $data['service_date']   = $service_date;
        $data['from_time']      = $from_time;
        $data['to_time']        = $to_time;
        $data['customer_id']    = $customer_id;
        $data['location_type']  = $location_type;
        $data['new_book_type']      = $book_type;
        //$data['maids']          = $this->maids_model->get_all_maid($filter);
        $search_maids          = $this->maids_model->get_all_maid_new($filter);
            //print_r($search_maids);
            //exit();
		$maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($service_date);
                
		$leave_maid_ids = array();
		foreach ($maids_leave_on_date as $leave)
		{
			array_push($leave_maid_ids, $leave->maid_id);
		}
            
        $time_from_stamp = $from_time;
        $time_to_stamp = $to_time;
        $maid_array =array();
        $maids_det = array();
            
        foreach ($search_maids as $s_maids)
        {
            $today_week_day = date('w', strtotime($service_date));
			
			if($this->input->post('repeat_days') && is_array($this->input->post('repeat_days')) && count($this->input->post('repeat_days')) > 0 &&  count($this->input->post('repeat_days')) <= 7 && $this->input->post('repeat_end') && ($this->input->post('repeat_end') == 'never' || $this->input->post('repeat_end') == 'ondate'))
			{
				$repeat_days = $this->input->post('repeat_days');
				$repeat_end = $this->input->post('repeat_end');
				if($repeat_end == 'ondate')
				{
					if($this->input->post('repeat_end_date'))
					{
						$repeat_end_date = $this->input->post('repeat_end_date');
						
						$repeat_end_date_split = explode('/', $repeat_end_date);
						if(count($repeat_end_date_split) == 3 && checkdate($repeat_end_date_split[1] , $repeat_end_date_split[0], $repeat_end_date_split[2]))
						{
							$s_date = new DateTime($service_date);
							$e_date = new DateTime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]);
							//$diff = $s_date->diff($e_date);
							
							$diff = abs(strtotime($repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0]) - strtotime($service_date));

							$years = floor($diff / (365*60*60*24));
							$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
							$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
																	
							if($days < 7)//if($diff->days < 7)
							{
								//$service_end_date = "";
							}
							
							$service_end_date = $repeat_end_date_split[2] . '-' . $repeat_end_date_split[1] . '-' . $repeat_end_date_split[0];
							
						}
						else 
						{
							//$service_end_date = "";
						}
					}
					else
					{
						//$service_end_date = "";
					}
				}
			}
			if($book_type == 'OD')
			{
				//$time_from_stamp = strtotime(trim($booking->time_from));
				//$time_to_stamp = strtotime(trim($booking->time_to));
				
				$today_bookingss = $this->bookings_model->get_schedule_by_date_avail_new($service_date,$s_maids->maid_id);
				 // echo '<pre>';
			   // print_r($today_bookingss);
				// exit();
				
				foreach ($today_bookingss as $t_booking)
				{
					$booked_slots[$t_booking->maid_id]['time'][strtotime($t_booking->time_from)] = strtotime($t_booking->time_to);
					
				}
				
				//$maids_s = $this->maids_model->get_maids();
				
				//foreach($maids_s as $smaid)
				//{
					if(isset($booked_slots[$s_maids->maid_id]['time']))
					{
						foreach($booked_slots[$s_maids->maid_id]['time'] as $f_time => $t_time)
						{
							if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
							{
								//array_push($nf_maids, $smaid->maid_id);
								array_push($maids_det, $s_maids->maid_id);
							}
						}
					}
				//}
			}
			
			if($book_type == 'WE')
            {
				foreach($repeat_days as $repeat_day)
				{
					//echo $repeat_day;
					if($repeat_day < $today_week_day)
					{
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					}
					else
					{
						$day_diff = $repeat_day - $today_week_day;
					}
					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					
					$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day);
					//echo '<pre>';
					//print_r($bookings_on_day);
					
					if(!empty($bookings_on_day))
					{	
						foreach($bookings_on_day as $booking_on_day)
						{
							// if($service_end_date == "")
							// {
								// $service_end_date = $service_start_date;
							// } else {
								// $service_end_date = $service_end_date;
							// }
							$s_date_stamp = strtotime($service_start_date);
							$e_date_stamp = strtotime($service_end_date);
							$bs_date_stamp = strtotime($booking_on_day->service_start_date);
							$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
							
							if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
							//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
							{
								$f_time = strtotime($booking_on_day->time_from);
								$t_time = strtotime($booking_on_day->time_to);
																	
																   

								if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time) )//|| ($time_from_stamp <= $f_time && $time_to_stamp <= $t_time)
								{
									array_push($maids_det, $s_maids->maid_id);
								} else {
									
								}								
							}							
						}
					} else {
                        
					}
				}
			}
			
			
			
                
            if($book_type == 'BW')
            {
				foreach($repeat_days as $repeat_day)
				{
					echo $repeat_day;
					if($repeat_day < $today_week_day)
					{
						$day_diff = (6 - $today_week_day + $repeat_day + 1);
					}
					else
					{
						$day_diff = $repeat_day - $today_week_day;
					}
					$service_start_date = date('Y-m-d', strtotime($service_date . ' + ' . $day_diff . ' day'));
					$bookings_on_day = $this->bookings_model->get_maid_booking_by_week_day($s_maids->maid_id, $repeat_day);
					// echo '<pre>';
					// print_r($bookings_on_day);
					if(!empty($bookings_on_day))
					{
						foreach($bookings_on_day as $booking_on_day)
						{
							// if($service_end_date == "")
							// {
								// $service_end_date = $service_start_date;
							// } else {
								// $service_end_date = $service_end_date;
							// }
							$s_date_stamp = strtotime($service_start_date);
							$e_date_stamp = strtotime($service_end_date);
							$bs_date_stamp = strtotime($booking_on_day->service_start_date);
							$be_date_stamp = strtotime($booking_on_day->service_actual_end_date);
							
							if($booking_on_day->booking_type == "BW")
							{
								$now = strtotime($service_start_date); // or your date as well
								$your_date = strtotime($booking_on_day->service_start_date);
								$datediff = $now - $your_date;

								$week_diff = round($datediff / (60 * 60 * 24));
								$week_difference = fmod($week_diff,14);
															
								if($week_difference == 0 || $week_difference == '-0')
								{
									$f_time = strtotime($booking_on_day->time_from);
									$t_time = strtotime($booking_on_day->time_to);
									
									if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
									{
										array_push($maids_det, $s_maids->maid_id);
									} else {

									}
								}
							} else if($booking_on_day->booking_type == "WE"){
								
								if(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $booking_on_day->service_end == '0' && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//latestif(($s_date_stamp >= $bs_date_stamp && $s_date_stamp <= $be_date_stamp) || ($s_date_stamp > $bs_date_stamp && $repeat_end == 'never') || ($s_date_stamp < $bs_date_stamp && $repeat_end == 'never') || ($repeat_end == 'ondate' && $e_date_stamp >= $bs_date_stamp && $e_date_stamp <= $be_date_stamp))
								//if(($s_date_stamp >= $bs_date_stamp && ($booking_on_day->service_end == 0 || ($booking_on_day->service_end == 1 && $s_date_stamp <= $be_date_stamp))))
								{
										$f_time = strtotime($booking_on_day->time_from);
										$t_time = strtotime($booking_on_day->time_to);

										if(($time_from_stamp > $f_time && $time_from_stamp < $t_time) || ($time_to_stamp > $f_time && $time_to_stamp < $t_time) || ($time_from_stamp <= $f_time && $time_to_stamp >= $t_time))
										{
												array_push($maids_det, $s_maids->maid_id);
										}
								}
							}
						}
					}  else {
                        
					}
				}
            }
        }
            
            //$result = array_diff((array)$search_maids, (array)$maids_det);
           // echo '<pre>';
           // print_r($maids_det);
           // echo '</pre>';
           // exit();





        $data['maids'] = $search_maids;
        $data['n_maids'] = $maids_det;
		$data['leave_maid_ids'] = $leave_maid_ids;

//          echo $this->db->last_query();
        $maids  = $this->load->view('view_maid_booking_new', $data, TRUE);	
        echo $maids;
        exit();	
    }
	
	public function booking_reports()
	{
		$data = array();
		$performance_report = array();               
            
		if($this->input->post('schedule_date_from'))
		{                    
			$f_s_date = explode('/', trim($this->input->post('schedule_date_from')));
			$schedule_date_from = $f_s_date[0] . '/' . $f_s_date[1] . '/' . $f_s_date[2];
                        list($day, $month, $year) = explode("/", $schedule_date_from);
                        $st_date = "$year-$month-$day";

		}
		else 
		{
			$schedule_date_from = date('d/m/Y');
                        list($day, $month, $year) = explode("/", $schedule_date_from);
                        $st_date = "$year-$month-$day";
		}
		 if ($this->input->post('schedule_date_to')) 
		 {
			 $t_s_date = explode('/', trim($this->input->post('schedule_date_to')));
			 $schedule_date_to = $t_s_date[0] . '/' . $t_s_date[1] . '/' . $t_s_date[2];
                         list($day, $month, $year) = explode("/", $schedule_date_to);
                         $fr_date = "$year-$month-$day";
		 }
		 else 
		 {                
			 $schedule_date_to = date('d/m/Y');
                         list($day, $month, $year) = explode("/", $schedule_date_to);
                         $fr_date = "$year-$month-$day";
		 }
			
		// if($this->input->post('maid_company'))
		// {
			// $company_id = $this->input->post('maid_company');
		// }
		// else
		// {
			// $company_id = '';
		// }
			
		if($this->input->post('zones'))
		{
			$zone_id = $this->input->post('zones');
			$zone_names = $this->zones_model->get_zone_by_id($zone_id);
		} else {
			$zone_id = "";
			$zone_names = "";
		}
            
		$f_sr_date = explode('/',$schedule_date_from);
		$service_date_from = $f_sr_date[2] . '-' . $f_sr_date[1] . '-' . $f_sr_date[0];
		
		//$getmaids = $this->maids_model->get_maids();
		$getmaids = $this->maids_model->get_full_maids();
		
		$per_report = array();
                
                $loop_start_date=$st_date;
                $loop_end_date=$fr_date;

                while (strtotime($loop_start_date) <= strtotime($loop_end_date)) {

			$newDate = date("d/m/Y", strtotime($loop_start_date));
                        foreach ($getmaids as $getmaid)
                        {
                                $maid_ids = $getmaid->maid_id;
                                $per_report = $this->bookings_model->get_maid_performance($maid_ids, $loop_start_date,$zone_id);
				foreach ($per_report as $key=>$per_repor) {
	                            $per_report[$key]->date=$newDate;
	                        }
                                array_push($performance_report, $per_report);
                        }

                        $loop_start_date = date ('Y-m-d', strtotime("+1 day", strtotime($loop_start_date)));

                }
            
		
		$maid_id = 0;
		$maid_name = '';
				
		// $t_sr_date = explode('/',$schedule_date_to);
		// $service_date_to = $t_sr_date[2] . '-' . $t_sr_date[1] . '-' . $t_sr_date[0];
            
		// if($this->input->post('maid_id'))
		// {
			// $maid_id = $this->input->post('maid_id');
			
			// $selected_maid = $this->maids_model->get_maid_by_id($maid_id);
			// $maid_name = $selected_maid->maid_name;
			// $performance_report = $this->day_services_model->get_maid_performance($maid_id, $service_date_from, $service_date_to, $company_id);
		// }
		// else
		// {
			// $getmaids = $this->maids_model->get_maids();
			// $per_report = array();
			// foreach ($getmaids as $getmaid)
			// {
				// $maid_ids = $getmaid->maid_id;
				// $per_report = $this->day_services_model->get_maid_performance($maid_ids, $service_date_from, $service_date_to, $company_id);
				// array_push($performance_report, $per_report);
			// }
			// $maid_id = 0;
			// $maid_name = '';
		// }
            
           // echo '<pre>';
           // print_r($performance_report);
           // echo '</pre>';
		   // exit();
		$zones = $this->zones_model->get_zones();
		$data['zones'] = $zones;
		$data['zone_id'] = $zone_id;
		$data['zone_names'] = $zone_names;
		$data['performance_report'] = $performance_report;              
		$data['schedule_date_from'] = $schedule_date_from;
		$data['schedule_date_to'] = $schedule_date_to;
		//$data['maid_id'] = $maid_id;
		//$data['maid_name'] = $maid_name;
		$data['maids'] = $this->maids_model->get_maids();
		//$data['maidcompany'] = $this->maids_model->get_maids_company();
		//$data['company_id'] = $company_id;

		$layout_data['content_body'] = $this->load->view('maid_performance_report', $data, TRUE);

		$layout_data['page_title'] = 'Performance Report';
		$layout_data['meta_description'] = 'Performance Report';
		$layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
		$layout_data['external_js_files'] = array();
		$layout_data['accounts_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
		
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function block_timing()
    {
		$data = array();
        $data['availabletimings_maint'] =  $this->bookings_model->get_available_cleaning_times();
		
		
        $layout_data['content_body'] = $this->load->view('scheduler_clean', $data, TRUE);
        $layout_data['page_title'] = 'Block Timings';
        $layout_data['meta_description'] = 'Block Timings'; 
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['blocks_active'] = '1'; 
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default_dashboard_block', $layout_data);
    }
	
	public function addscheduletime()
    {
        $date = $this->input->post('select_date');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        
        $time_fields = array();
        $time_fields['date']        = $date;
        $time_fields['from_time']   = $start; 
        $time_fields['to_time']     = $end; 
        $time_fields['status']      = 1; 
        $time_fields['added_datetime']      = date('Y-m-d H:i:s');
        $time_add = $this->bookings_model->add_booking_times($time_fields);   
        
        echo 'success';
        exit();
    }
	
	public function deletescheduletime()
    {
        $timeid = $this->input->post('timeid');
        
        $deletetime = $this->bookings_model->delete_booking_times($timeid);
        
        echo 'success';
        exit();
    }
    
    public function delayed_report()
    {
		
		if($this->input->post('action') && $this->input->post('action') == 'sms_send')
			{
				
			  	$mobile=$this->input->post("mobile");
				$message=$this->input->post("message");
				$this->_send_sms($mobile,$message);
				echo 1;
				exit;
				
				
				
			}
        $today_date = date('Y-m-d');
        $current_time_old = date('H:i:s');
        $current_time = date('H:i:s',strtotime($current_time_old . ' +15 minutes'));
		
        $data=array();
        $data['currentdate'] = date('d/m/Y', strtotime($today_date));
        if($this->input->post())
        {
            $data['currentdate'] = $this->input->post('cstatemnet-date-from');
            $s_date = explode("/", $this->input->post('cstatemnet-date-from'));
            $today_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        }
        if($today_date == date('Y-m-d'))
        {
            $current_booking = $this->bookings_model->get_today_schedule_by_date($today_date,$current_time);
        } else {
            $current_booking = $this->bookings_model->get_previous_schedule_by_date($today_date);
        }

        $bookings = array();
        $i = 0;
        foreach ($current_booking as $c_booking)
        {
        	// $checkbooking_transferred = $this->bookings_model->check_booking_transferred($c_booking->booking_id,$today_date);
        	$checkbooking_transferred = $this->bookings_model->check_booking_transferred_new($c_booking->booking_id,$today_date);
            if(count($checkbooking_transferred) > 0)
			{
				// $t_zone_id = $checkbooking_transferred->zone_id;
				$tablet_id = $checkbooking_transferred->transfering_to_tablet;
				$transdrivername = $checkbooking_transferred->tablet_driver_name;
			} else {
				// $t_zone_id = $c_booking->zone_id;
				$tablet_id = $c_booking->tabletid;
				$transdrivername = '';
			}
			
			//$get_tablet_id = $this->bookings_model->get_tablet_id($t_zone_id);
			
			//$tablet_id = $get_tablet_id->tablet_id;
			
			$get_tablet_loc = $this->bookings_model->get_tablet_loc($tablet_id);



            $checkdayservice = $this->bookings_model->get_day_schedule_by_date($today_date,$c_booking->starttime,$c_booking->booking_id);
			// echo '<pre>';
			// print_r($checkdayservice);
			// exit();
			
			if(!empty($checkdayservice))
			{
				if($checkdayservice->start_time != "")
				{
					$starttime = date('g:i:s a', strtotime($checkdayservice->start_time));
					$checktimee = $checkdayservice->start_time;
					$newtime = date('H:i:s',strtotime($c_booking->starttime . ' -15 minutes'));
					if($checkdayservice->end_time != '00:00:00')
					{
						$endtime = ' - ' .date('g:i:s a', strtotime($checkdayservice->end_time));
					} else {
						$endtime = "";
					}
					if($checkdayservice->start_time <= $newtime)
					{
						$checked = 1;
					} else {
						$checked = 0;
					}
					//echo $checked.','.$checkdayservice->start_time.','.$newtime;exit();
				} else {
					$starttime = "";
					$checked = 0;
					$checktimee = "";
				}
			} else {
				$starttime = "";
				$endtime = "";
				$checked = 0;
				$checktimee = "";
				$newtime = "";
			}
            if($checked==0)
            {
			
            $bookings[$i]['customer_name'] = $c_booking->customer_name;
            $bookings[$i]['company_name'] = $c_booking->company_name!="" ? $c_booking->company_name:"EliteMaids" ;
            $bookings[$i]['maid_name'] = $c_booking->maid_name;
            $bookings[$i]['zone_name'] = $c_booking->zone_name;
            $bookings[$i]['driver_name'] = $c_booking->driver_name;
            $bookings[$i]['trans_driver_name'] = $transdrivername;
            $bookings[$i]['from_time'] = $c_booking->time_from;
            $bookings[$i]['starttime'] = $newtime;
			$bookings[$i]['starttimenew'] = $c_booking->starttime;
            $bookings[$i]['to_time'] = $c_booking->time_to;
            $bookings[$i]['drop_time'] = $starttime;
            $bookings[$i]['drop_time_new'] = $checktimee;
            $bookings[$i]['pick_time'] = $endtime;
            $bookings[$i]['total_amount'] = $c_booking->total_amount;
			$bookings[$i]['cust_latitude'] = $c_booking->lat;
            $bookings[$i]['cust_longitude'] = $c_booking->longt;
            $bookings[$i]['tab_latitude'] = $get_tablet_loc->latitude;
            $bookings[$i]['tab_longitude'] = $get_tablet_loc->longitude;
            $bookings[$i]['customer_address'] = $c_booking->customer_address;
            $bookings[$i]['mobile_number_1'] = $c_booking->mobile_number_1;
            
            $i++;
            }
        }
//        echo '<pre>';
//        print_r($bookings);
        
//        $data['current_booking'] = $this->bookings_model->get_today_schedule_by_date($today_date,$current_time);
        $data['current_booking'] = $bookings;
		$data['currenttime'] = $current_time;
		$data['currenttimeold'] = $current_time_old;
       //echo $current_time;
       // echo '<pre>';
       // print_r($data['current_booking']);
       // exit();
        $layout_data['content_body'] = $this->load->view('delayed_report', $data, TRUE);
        $layout_data['page_title'] = 'Delayed Report';
        $layout_data['meta_description'] = 'Delayed Report'; 
        $layout_data['css_files'] = array('datepicker.css', 'demo.css','jquery.fancybox.css');
        $layout_data['accounts_active'] = '1'; 
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.fancybox.pack.js','base.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js','mymaids.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }
    function delayed_report_download(){
		if($this->input->post('action') && $this->input->post('action') == 'sms_send')
			{
				
			  	$mobile=$this->input->post("mobile");
				$message=$this->input->post("message");
				$this->_send_sms($mobile,$message);
				echo 1;
				exit;
				
				
				
			}
        $today_date = date('Y-m-d');
        $current_time_old = date('H:i:s');
        $current_time = date('H:i:s',strtotime($current_time_old . ' +15 minutes'));
		
        $data=array();
        $data['currentdate'] = date('d/m/Y', strtotime($today_date));
        if($this->input->post())
        {
            $data['currentdate'] = $this->input->post('cstatemnet-date-from');
            $s_date = explode("/", $this->input->post('cstatemnet-date-from'));
            $today_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        }
        if($today_date == date('Y-m-d'))
        {
            $current_booking = $this->bookings_model->get_today_schedule_by_date($today_date,$current_time);
        } else {
            $current_booking = $this->bookings_model->get_previous_schedule_by_date($today_date);
        }

        $bookings = array();
        $i = 0;
        foreach ($current_booking as $c_booking)
        {
        	// $checkbooking_transferred = $this->bookings_model->check_booking_transferred($c_booking->booking_id,$today_date);
        	$checkbooking_transferred = $this->bookings_model->check_booking_transferred_new($c_booking->booking_id,$today_date);
            if(count($checkbooking_transferred) > 0)
			{
				// $t_zone_id = $checkbooking_transferred->zone_id;
				$tablet_id = $checkbooking_transferred->transfering_to_tablet;
				$transdrivername = $checkbooking_transferred->tablet_driver_name;
			} else {
				// $t_zone_id = $c_booking->zone_id;
				$tablet_id = $c_booking->tabletid;
				$transdrivername = '';
			}
			
			//$get_tablet_id = $this->bookings_model->get_tablet_id($t_zone_id);
			
			//$tablet_id = $get_tablet_id->tablet_id;
			
			$get_tablet_loc = $this->bookings_model->get_tablet_loc($tablet_id);



            $checkdayservice = $this->bookings_model->get_day_schedule_by_date($today_date,$c_booking->starttime,$c_booking->booking_id);
			// echo '<pre>';
			// print_r($checkdayservice);
			// exit();
			
			if(!empty($checkdayservice))
			{
				if($checkdayservice->start_time != "")
				{
					$starttime = date('g:i:s a', strtotime($checkdayservice->start_time));
					$checktimee = $checkdayservice->start_time;
					$newtime = date('H:i:s',strtotime($c_booking->starttime . ' -15 minutes'));
					if($checkdayservice->end_time != '00:00:00')
					{
						$endtime = ' - ' .date('g:i:s a', strtotime($checkdayservice->end_time));
					} else {
						$endtime = "";
					}
					if($checkdayservice->start_time <= $newtime)
					{
						$checked = 1;
					} else {
						$checked = 0;
					}
					//echo $checked.','.$checkdayservice->start_time.','.$newtime;exit();
				} else {
					$starttime = "";
					$checked = 0;
					$checktimee = "";
				}
			} else {
				$starttime = "";
				$endtime = "";
				$checked = 0;
				$checktimee = "";
				$newtime = "";
			}
            if($checked==0)
            {
			
            $bookings[$i]['customer_name'] = $c_booking->customer_name;
            $bookings[$i]['company_name'] = $c_booking->company_name!="" ? $c_booking->company_name:"EliteMaids" ;
            $bookings[$i]['maid_name'] = $c_booking->maid_name;
            $bookings[$i]['zone_name'] = $c_booking->zone_name;
            $bookings[$i]['driver_name'] = $c_booking->driver_name;
            $bookings[$i]['trans_driver_name'] = $transdrivername;
            $bookings[$i]['from_time'] = $c_booking->time_from;
            $bookings[$i]['starttime'] = $newtime;
			$bookings[$i]['starttimenew'] = $c_booking->starttime;
            $bookings[$i]['to_time'] = $c_booking->time_to;
            $bookings[$i]['drop_time'] = $starttime;
            $bookings[$i]['drop_time_new'] = $checktimee;
            $bookings[$i]['pick_time'] = $endtime;
            $bookings[$i]['total_amount'] = $c_booking->total_amount;
			$bookings[$i]['cust_latitude'] = $c_booking->lat;
            $bookings[$i]['cust_longitude'] = $c_booking->longt;
            $bookings[$i]['tab_latitude'] = $get_tablet_loc->latitude;
            $bookings[$i]['tab_longitude'] = $get_tablet_loc->longitude;
            $bookings[$i]['customer_address'] = $c_booking->customer_address;
            $bookings[$i]['mobile_number_1'] = $c_booking->mobile_number_1;
            
            $i++;
            }
        }
//        echo '<pre>';
//        print_r($bookings);
        
//        $data['current_booking'] = $this->bookings_model->get_today_schedule_by_date($today_date,$current_time);
        $data['current_booking'] = $bookings;
		$data['currenttime'] = $current_time;
		$data['currenttimeold'] = $current_time_old;
       //echo $current_time;
       // echo '<pre>';
       // print_r($data['current_booking']);
       // exit();
	   $this->load->view('delayed_report_download', $data);
	}
    public function delayed_booking()
    { 
        $today_date = date('Y-m-d');
        $current_time = date('H:i:s');
        $data=array();
        $data['currentdate'] = date('d/m/Y', strtotime($today_date));
        
        if($this->input->post())
        {
            $data['currentdate'] = $this->input->post('cstatemnet-date-from');
            $s_date = explode("/", $this->input->post('cstatemnet-date-from'));
            $today_date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        }
        
        
        $current_booking = $this->bookings_model->get_delayed_booking_by_date($today_date);
        
        $bookings = array();
        $i = 0;
        foreach ($current_booking as $c_booking)
        {
            if($c_booking->time_from < $c_booking->start_time)
            {
            $bookings[$i]['customer_name'] = $c_booking->customer_name;
            $bookings[$i]['maid_name'] = $c_booking->maid_name;
            $bookings[$i]['zone_name'] = $c_booking->zone_name;
            $bookings[$i]['from_time'] = date('g:i:s a', strtotime($c_booking->time_from));
            $bookings[$i]['drop_time'] = date('g:i:s a', strtotime($c_booking->start_time));
            $bookings[$i]['total_amount'] = $c_booking->total_amount;
            $i++;
            }
        }

        $data['current_booking'] = $bookings;

        $layout_data['content_body'] = $this->load->view('delayed_booking', $data, TRUE);
        $layout_data['page_title'] = 'Delayed Report';
        $layout_data['meta_description'] = 'Delayed Report'; 
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['accounts_active'] = '1'; 
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js','mymaids.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
        
    }
    private function _send_sms($mobile_number, $message)
    {
        $num = substr($mobile_number, -9);
        
        //$sms_url = 'https://api.smsglobal.com/http-api.php?action=sendsms&user=jhh1a51c&password=LXoAEzMF&&from=TestJC&to=971' . urlencode($num) . '&text=' . urlencode($message) . '';
        $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=elitemaids&passwd=emaid@123&mobilenumber=971'.$num.'&message='.urlencode($message).'&sid=EliteMaids&mtype=N';
        $sms_url = str_replace(" ", '%20', $sms_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $res = curl_exec($ch);
        curl_close ($ch);
    }
    
    public function sent_booking_test()
   {
       $this->load->library('email');
       
       
       $html = "hi sandy";
       $email="azinova123@hotmail.com";
       $config['mailtype'] = 'html';
       $config['charset'] = 'iso-8859-1';
       $this->email->initialize($config);

       $this->email->from('booking@spectrumservices.ae', 'Spectrum');
       $this->email->to($email);
       //$this->email->cc("vishnu.m@azinova.info");
       $this->email->subject('Booking Confirmation');

       $this->email->message($html);

       if($this->email->send())
       { echo "success";}
       else
       { echo "fail";}
   }
   public function get_payment_mode()
   {

   $booking_id=$this->input->post("booking_id");
   $service_date=date("Y-m-d",strtotime($this->input->post("service_date")));
   $payment_type=$this->bookings_model->get_payment_mode($booking_id,$service_date);
   if(!empty($payment_type)){
   echo json_encode($payment_type);
    }
   else{
   $payment_type=$booking = $this->bookings_model->get_booking_by_id($booking_id);
   if(!empty($payment_type)){
   echo json_encode($payment_type);
    }

   } 
   exit;

   }
   
   public function available_booking_list()
   {
	   $service_date = date('Y-m-d');
	   $get_avilable_before_noon = $this->bookings_model->get_avilable_before_noon($service_date,92);
	   $count = count($get_avilable_before_noon);
	   $hour_array = array();
	   for($i=0; $i<$count; $i++)
	   {
		   if($i==0)
		   { 
			   $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime('08:00:00')) / 3600;
			   array_push($hour_array,$gap);
			   if($count == 1)
			   {
				   $gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
				   array_push($hour_array,$gap2);
			   }
		   } else if($i == ($count - 1))
		   {
			  $gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
			  array_push($hour_array,$gap); 
			   $gap2 = (strtotime('13:00:00') - strtotime($get_avilable_before_noon[$i]->time_to)) / 3600;
			   array_push($hour_array,$gap2);
			   
		   } else {
				$gap = (strtotime($get_avilable_before_noon[$i]->time_from) - strtotime($get_avilable_before_noon[$i-1]->time_to)) / 3600;
				array_push($hour_array,$gap);
		   }
	   }
	   
	   foreach($hour_array as $list)
	   {
		   echo $list;
		   echo '<br/>';
	   }
	   
	 
   }
   public function testmail()
	{
		$this->load->library('email');
		$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_user' => 'apikey',
		//   'smtp_pass' => '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',
		'smtp_pass' => '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',
		'smtp_port' => 587,
			'mailtype'  => 'html',
			'crlf' => "\r\n",
			'newline' => "\r\n"
		));
		//$email_id='jose.robert@azinova.info';
		//$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->initialize($config);
		$this->email->from('info@elitemaids.emaid.info', 'Elitemaids');
		$this->email->to('karthika.prasad@azinova.info');

		$this->email->subject('Elitemaids - Booking Status');

		$this->email->message('Hi');

		$mailstat=$this->email->send();
	}
	function printsession(){
		// echo "<pre>"; print_r($this->session->userdata());die;
		echo "<pre>"; print_r(user_authenticate_isadmin());die;
	}
   
}

/* End of file booking.php */
/* Location: ./application/controllers/booking.php */
