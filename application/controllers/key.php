<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Key extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('settings_model');
        $this->load->model('bookings_model');
        $this->load->helper('google_api_helper');
        $this->load->helper('curl_helper');
        $this->load->model('keys_model');

    }

    public function index()
    {
        // echo 'God is love';

        if ($this->input->post('key_sub')) {
            $key = $this->input->post('key');

            $data = array(
                'code' => $key,
                'customer_access_type' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_by' => 0,

            );

            $key_id = $this->keys_model->add_keys($data);

        } else if ($this->input->post('key_edit')) {
            $key_id = $this->input->post('edit_keyid');
            $key_name = $this->input->post('edit_key');

            $data = array(
                'code' => $key_name,
                'updated_at' => date('Y-m-d H:i:s'),

            );
            $this->keys_model->update_keys($data, $key_id);

        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['keys'] = $this->keys_model->get_keys();
        // print_r($data['keys']);die();
        $layout_data['content_body'] = $this->load->view('key_list', $data, true);
        $layout_data['page_title'] = 'Key List';
        $layout_data['meta_description'] = 'key';
        $layout_data['css_files'] = array('toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);

    }

    public function edit_key()
    {
        $key_id = $this->input->post('id');
        $result = $this->keys_model->get_key_details($key_id);
        echo json_encode($result);
    }

    public function remove_key()
    {
        // $key_id = $this->input->post('key_id');
        // $data = $this->keys_model->delete_key($key_id);

        $key_id = $this->input->post('key_id');
        $access_owned_data = $this->keys_model->get_access_owned_data($key_id);

        if ($access_owned_data !== null) {
            if ($access_owned_data->return_added_by !== null && $access_owned_data->returned_at !== null) {
                $this->keys_model->delete_key($key_id);
                $response = array('status' => 'success');
            } else {
                $response = array('status' => 'error');
            }
        } else {
            $this->keys_model->delete_key($key_id);
            $response = array('status' => 'not_found');
        }

        echo json_encode($response);

    }

    public function cards()
    {

        if ($this->input->post('card_sub')) {
            $card = $this->input->post('card');

            $data = array(
                'code' => $card,
                'customer_access_type' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_by' => 0,

            );

            $card_id = $this->keys_model->add_cards($data);

        } else if ($this->input->post('card_edit')) {
            $key_id = $this->input->post('edit_cardid');
            $card_name = $this->input->post('edit_card');

            $data = array(
                'code' => $card_name,
                'updated_at' => date('Y-m-d H:i:s'),

            );
            $this->keys_model->update_keys($data, $key_id);

        }
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['cards'] = $this->keys_model->get_cards();
        // print_r($data['cards']);die();
        $layout_data['content_body'] = $this->load->view('card_list', $data, true);
        $layout_data['page_title'] = 'Card List';
        $layout_data['meta_description'] = 'card';
        $layout_data['css_files'] = array('toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('mymaids.js', 'bootstrap-datepicker.js', 'ajaxupload.3.5.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);

    }

    public function get_last_code_old()
    {
        $this->db->select('code');
        $this->db->from('customer_access');
        $this->db->where('deleted_at', null);
        $this->db->where('customer_access_type', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);

        $query = $this->db->get();

        $result = $query->row();

        $response = array();
        if ($result) {
            $response['success'] = true;
            $response['lastCode'] = $result->code;
        } else {
            $response['success'] = false;
            $response['lastCode'] = 'MM-K-000';
        }

        echo json_encode($response);
    }

    public function get_card_last_code()
    {

        $this->db->select('count(id) as count');
        $this->db->from('customer_access as ca');
        $this->db->where('customer_access_type', 2);
        $query = $this->db->get();
        $count = $query->row()->count + 1;
        $response = array();
        $response['success'] = true;
        $response['code'] = 'MM-C-' . sprintf('%03d', $count);
        echo json_encode($response);

    }

    public function check_returned()
    {
        $key_id = $this->input->post('key_id');
        $access_owned_data = $this->keys_model->get_access_owned_data($key_id);

        if ($access_owned_data !== null) {
            if ($access_owned_data->return_added_by !== null && $access_owned_data->returned_at !== null) {
                $response = array('status' => 'not_null');
            } else {
                $response = array('status' => 'null');
            }
        } else {
            $response = array('status' => 'not_found');
        }

        echo json_encode($response);

    }

    public function get_last_code()
    {
        $this->db->select('count(id) as count');
        $this->db->from('customer_access as ca');
        $this->db->where('customer_access_type', 1);
        $query = $this->db->get();
        $count = $query->row()->count + 1;
        $response = array();
        $response['success'] = true;
        $response['code'] = 'MM-K-' . sprintf('%03d', $count);
        echo json_encode($response);
    }

    public function check_key_code()
    {
        $key = $this->input->post('key');

        $exists = $this->keys_model->check_key_exists($key);
        echo json_encode(array('exists' => $exists));
    }

    public function save_key()
    {
        $key = $this->input->post('key');
        $data = array(
            'code' => $key,
            'customer_access_type' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => user_authenticate(),

        );
        $key_id = $this->keys_model->add_keys($data);
        echo json_encode(array('status' => "success"));
    }

    public function check_key_code_edit()
    {
        $key = $this->input->post('key');
        $keyid = $this->input->post('edit_keyid');
        $exists = $this->keys_model->check_key_exists_edit($key, $keyid);
        echo json_encode(array('exists' => $exists));
    }

    public function edit_key_new()
    {

        $key_id = $this->input->post('edit_keyid');
        $key_name = $this->input->post('edit_key');

        $data = array(
            'code' => $key_name,
            'updated_at' => date('Y-m-d H:i:s'),

        );
        $this->keys_model->update_keys($data, $key_id);
        echo json_encode(array('success' => true));
    }

    public function save_card()
    {
        $card = $this->input->post('card');
        $data = array(
            'code' => $card,
            'customer_access_type' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => user_authenticate()
        );
        $card_id = $this->keys_model->add_cards($data);
        echo json_encode(array('success' => true));
    }

    public function edit_card_new()
    {
        $key_id = $this->input->post('edit_cardid');
        $card_name = $this->input->post('edit_card');
        $data = array(
            'code' => $card_name,
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $this->keys_model->update_keys($data, $key_id);
        echo json_encode(array('success' => true));
    }
}
