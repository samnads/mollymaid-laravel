<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        
        /**/if(!is_user_loggedin())
        {
            if($this->input->is_ajax_request())
            {
                echo 'refresh';
                exit();
            }
            else
            {
                redirect('logout');
            }
        }
            
        if(!user_permission(user_authenticate(), 2))
                {
                    show_error('You may not have the appropriate permissions to access the file. <br /><a href="'. base_url() .'dashboard">Go to Dashboard</a>', 401, 'Access denied');
                }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('zones_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
                $this->load->model('settings_model');
        
        $this->load->helper('google_api_helper');
    }

    public function index()
    {
        //$current_date = date('Y-m-d');
        if($this->input->post('service_date'))
        {
            $s_date = $this->input->post('service_date');
            $s_date = explode("/", $s_date);
            $date = $s_date[2] . '-' . $s_date[1] . '-' . $s_date[0];
        }
        else
        {                
            $date = date('Y-m-d');
            //$service_date = '2015-06-17';
        }
        
        
        
        //$get_today_job_details = $this->bookings_model->get_today_job_details($date);
        
        
        $data = array();
        $data['formatted_date'] = $date;
        $data['service_date'] = date('d/m/Y', strtotime($date));
        $data['dataarray'] = array();
        //$job_json = json_encode($dataarray);
        
        $layout_data = array();
        $layout_data['content_body'] = $this->load->view('driver_location', $data, TRUE);       
        $layout_data['page_title'] = 'Maps';
        $layout_data['meta_description'] = 'Maps';
        $layout_data['css_files'] = array('demo.css');
        $layout_data['maps_active'] = '1';
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'bootstrap-datepicker.js','moment.min.js','jquery.dataTables.min.js','dataTables.responsive.min.js','dataTables.checkboxes.min.js');

        $this->load->view('layouts/default_dashboard', $layout_data);
    }
}