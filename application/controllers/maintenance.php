<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if(!is_user_loggedin())
		{
			redirect('logout');
			
		}
	}
	
	public function index()
	{
		$this->load->model('maintenance_model');
		$data = array();
		$service_date   = date('Y-m-d');
                $data['total_booking_count']= $this->maintenance_model->total_booking_count();
                $data['schedule_count']     = $this->maintenance_model->get_schedule_by_date_counts($service_date);
                
                $total_todays_bookng_hrs    = $this->maintenance_model->get_schedule_hrs_by_date($service_date);
                $todaybookinghrs            = 0;
                foreach($total_todays_bookng_hrs as $totalhrs){
                    $todaybookinghrs        = $todaybookinghrs + ($totalhrs->time_to - $totalhrs->time_from);
                }
                $data['customer_count']     = $this->maintenance_model->get_total_customers_counts();
                
                // daily booking summary calculation starts
                $data['maid_count']         = $maidcount = $this->maintenance_model->get_schedule_by_date_counts($service_date);
                $data['dailySummaryperc']   = ($maidcount > 0) ? ($todaybookinghrs / ($maidcount*8)) * 100 : 0;
                // daily booking summary calculation ends
                 
                // Weekly booking summary calculation starts
                $lastSatday                 = new DateTime('last saturday');
                $lastweekend_service_date   = $lastSatday->format('Y-m-d');
               
                $begin      = new DateTime($lastweekend_service_date);
                $today      =  new DateTime(date('Y-m-d')+1);
                $daterange  = new DatePeriod($begin, new DateInterval('P1D'), $today);
                $weeklyTotalBookingHRs = $weeklymaidcount = 0;
                $weekdaysCount = 1 ;
                foreach($daterange as $date){
                    $serviceDateLoop            = $date->format("Y-m-d") ;
                    $weeklymaidcount            = $weeklymaidcount + $this->maintenance_model->get_schedule_by_date_counts($serviceDateLoop);
                    $total_weekly_bookng_hrs    = $this->maintenance_model->get_schedule_hrs_by_date($serviceDateLoop);
                    foreach($total_weekly_bookng_hrs as $totalhrs){
                        $weeklyTotalBookingHRs  = $weeklyTotalBookingHRs + ($totalhrs->time_to - $totalhrs->time_from);
                    }
                    $weekdaysCount ++ ;
                }
                $data['weeklySummaryperc']  = ($weeklymaidcount >0) ? ($weeklyTotalBookingHRs / ($weeklymaidcount*8 * $weekdaysCount)) * 100 : 0;
                // Weekly booking summary calculation ends
                
                
                
//                Monthly booking summary calculation starts
                
                $beginMonth      = new DateTime(date('Y-m-01'));
                $today      =  new DateTime(date('Y-m-d')+1);
                $mnthdaterange  = new DatePeriod($beginMonth, new DateInterval('P1D'), $today);
                $monthlyTotalBookingHRs = $mnthlymaidcount = 0;
                $mnthdaysCount = 1 ;
                foreach($mnthdaterange as $date){
                    $serviceDateLoop            = $date->format("Y-m-d") ;
                    $mnthlymaidcount            = $mnthlymaidcount + $this->maintenance_model->get_schedule_by_date_counts($serviceDateLoop);
                    $total_weekly_bookng_hrs    = $this->maintenance_model->get_schedule_hrs_by_date($serviceDateLoop);
                    foreach($total_weekly_bookng_hrs as $totalhrs){
                        $monthlyTotalBookingHRs  = $monthlyTotalBookingHRs + ($totalhrs->time_to - $totalhrs->time_from);
                    }
                    $mnthdaysCount ++ ;
                }
                $data['mnthlySummaryperc']  =  ($monthlyTotalBookingHRs / ($mnthlymaidcount*8 * $mnthdaysCount)) * 100;
                
                
//                Monthly booking summary calculation ends
                
//                 Complaints count starts
                    //$data['total_daily_complaint_count']    = $this->bookings_model->get_complaints_count();
                    //$data['total_weekly_complaint_count']   = $this->bookings_model->get_complaints_count($lastweekend_service_date);
                    //$data['total_monthly_complaint_count']  = $this->bookings_model->get_complaints_count(date('Y-m-01'));
 
//                 Complaints count ends
//                Job in transit count
                    ///$data['total_job_in_transit_count']    = $this->bookings_model->get_transit_jobs_count();
//                     Daily billed invoices starts
                     //$data['billed_daily_invoice_count']   = $this->bookings_model->get_invoice_count('0');
                     //$data['billed_weekly_invoice_count']  = $this->bookings_model->get_invoice_count('0',$lastweekend_service_date);
                     //$data['billed_monthly_invoice_count'] = $this->bookings_model->get_invoice_count('0',date('Y-m-01'));
//                     Daily billed invoices ends
//                     Daily collected invoices starts
                     //$data['collected_daily_invoice_count']     = $this->bookings_model->get_invoice_count('1');
                     //$data['collected_weekly_invoice_count']    = $this->bookings_model->get_invoice_count('1',$lastweekend_service_date);
                     //$data['collected_monthly_invoice_count']   = $this->bookings_model->get_invoice_count('1',date('Y-m-01'));
//                     Daily collected invoices ends
                //Get week dates
                $saturday = strtotime("last saturday");
                $saturday = date('w', $saturday)==date('w') ? $saturday+6*86400 : $saturday;

                $friday = strtotime(date("Y-m-d",$saturday)." +5 days");

                $this_week_sd = date("Y-m-d",$saturday);
                $this_week_ed = date("Y-m-d",$friday);
                
                $days = ((strtotime($this_week_ed) - strtotime($this_week_sd)) / (60*60*24));
                for ($i = 0; $i <= $days; ++$i) {
                    $date = date('Y-m-d', strtotime("$this_week_sd +$i day"));
                    $year = date('Y',strtotime($date));
                    $month = date('n',strtotime($date)) - 1;
                    $day = date('d',strtotime($date));
                    $count = $this->maintenance_model->get_schedule_by_date_counts($date);
                    //$jsarray = array();
                    $element = "new Date($year,$month,$day),$count,'$count bookings'";
                    $jsarray[$i] = $element;
                    //array_push($jsarray, $element);
                }

                //echo "Current week range from $this_week_sd to $this_week_ed ";
                //exit();
                //Ends
//                echo '<pre>';
//                print_r($jsarray);  
//                echo '</pre>';
                $data['grapharray'] = $jsarray;
                //$data['total_invoices']     = $this->bookings_model->total_invoices_count();
                $data['recent_activity']    = $this->maintenance_model->get_user_activity();
                
		$layout_data['content_body']= $this->load->view('maintenance_dashboard', $data, TRUE);
		$layout_data['page_title']  = 'Dashboard';
		$layout_data['dashboard_active'] = '1';
		$layout_data['meta_description'] = 'Dashboard';
		$layout_data['css_files']   = array();
		$layout_data['js_files']    = array();
		$layout_data['external_js_files'] = array();
		
		$this->load->view('layouts/default_maint_dashboard', $layout_data);
	}
	
	public function booking()
    {
		$this->load->model('maintenance_model');
		//$date = date('Y-m-d');
        $data = array();
        // $data['post_invoice_date'] = '';
        // if($this->input->post('invoice_date')!=""){
            // $data['post_invoice_date']=$this->input->post('invoice_date');   
            // $date = DateTime::createFromFormat('d/m/Y', $this->input->post('invoice_date'));   
            // $date=$date->format('Y-m-d');
            
        // }
        
        
        //$data['invoice_date'] =$date;
		
        $data['maint_report']=$this->maintenance_model->get_maintenance_booking();
       // echo '<pre>';
       // print_r($data['maint_report']);
       // echo '</pre>';
        //exit();
        $layout_data['content_body'] = $this->load->view('maint_booking', $data, TRUE);
        $layout_data['page_title'] = 'Bookings';
        $layout_data['meta_description'] = 'Bookings'; 
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['bookings_active'] = '1'; 
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default_maint_dashboard', $layout_data);
    }
	
	public function block_timing()
    {
		$this->load->model('maintenance_model');
		$data = array();
        $data['availabletimings_maint'] =  $this->maintenance_model->get_available_cleaning_times_maint();
		
		
        $layout_data['content_body'] = $this->load->view('scheduler_maint', $data, TRUE);
        $layout_data['page_title'] = 'Block Timings';
        $layout_data['meta_description'] = 'Block Timings'; 
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['blocks_active'] = '1'; 
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'mymaids.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default_maint_dashboard', $layout_data);
    }
	
	public function addscheduletime_maint()
    {
		$this->load->model('maintenance_model');
        $date = $this->input->post('select_date');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        
        $time_fields = array();
        $time_fields['date']        = $date;
        $time_fields['from_time']   = $start; 
        $time_fields['to_time']     = $end; 
        $time_fields['status']      = 1; 
        $time_add = $this->maintenance_model->add_booking_times_maint($time_fields);   
        
        echo 'success';
        exit();
    }
	
	public function deletescheduletime_maint()
    {
		$this->load->model('maintenance_model');
        $timeid = $this->input->post('timeid');
        $deletetime = $this->maintenance_model->delete_booking_times_maint($timeid);
        
        echo 'success';
        exit();
    }
	
	
}
	
	