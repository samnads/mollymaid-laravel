<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Schedule extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 2)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('zones_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('driver_model');
        $this->load->model('tablets_model');
        $this->load->model('settings_model');
        $this->load->model('justmop_model');
        $this->load->helper('google_api_helper');
        $this->load->model('employee_model');
        $this->load->model('schedule_model');
        $this->load->helper('time_helper');
        $this->load->helper('booking_helper');
    }
    public function week_view()
    {
        $date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $data['filter_start_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $date = $this->input->get('filter_end_date') ?: date('Y-m-d');
        $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +7 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +7 days"));
        //    print_r($data['filter_end_date']);die();
        $filter_start_date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $filter_end_date = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +7 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +7 days"));
        // print_r($filter_end_date);die();
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date_we($filter_start_date, $filter_end_date);
        $get_leave_maids = $this->maids_model->get_maids_leave_by_date_we($filter_start_date, $filter_end_date);
      

        $leave_maid_types = array();
        foreach ($get_leave_maids as $leave) {
            $leavetypearay = array();
            $leavetypearay['maid_id'] = $leave->maid_id;
            $leavetypearay['service_week_day'] = date('w', strtotime($leave->leave_date));
            $leavetypearay['maid_type'] = $leave->typeleaves;
            array_push($leave_maid_types, $leavetypearay);
        }

        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }
        $data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
        $data['leave_maid_types'] = $leave_maid_types; // Maid Leave
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['cancel_reasons'] = $this->schedule_model->get_cancel_resons();
        $data['services'] = $this->settings_model->get_services_maid();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_week_view', $data, true);
        $layout_data['page_title'] = 'Schedule Week View';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css','schedule.week-view.css?v='.time());
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.week-view.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function day_view()
    {
        $date = $this->input->get('service_date') ?: date('Y-m-d');
        $data['service_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $data['service_week_day'] = date('w', strtotime($date));
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['cancel_reasons'] = $this->schedule_model->get_cancel_resons();
        $data['suspend_reasons'] = $this->schedule_model->get_suspend_resons();
        $data['services'] = $this->settings_model->get_services_maid();
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date($date);
        $get_leave_maids = $this->maids_model->get_maids_leave_by_date($date);

        // $leave_maid_ids = array();
        // foreach ($get_leave_maids as $leave) {
        //     array_push($leave_maid_ids, $leave->maid_id);
        // }

        $leave_maid_types = array();
        foreach ($get_leave_maids as $leave) {
            $leavetypearay = array();
            $leavetypearay['maid_id'] = $leave->maid_id;
            $leavetypearay['maid_type'] = $leave->typeleaves;
            array_push($leave_maid_types, $leavetypearay);
        }

        $leave_maid_ids = array();
        foreach ($maids_leave_on_date as $leave) {
            array_push($leave_maid_ids, $leave->maid_id);
        }
        $data['leave_maid_ids'] = $leave_maid_ids; // Maid Leave
        $data['leave_maid_types'] = $leave_maid_types; // Maid Leave
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_day_view', $data, true);
        $layout_data['page_title'] = 'Schedule Day View';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css', 'schedule.day-view.css?v='.time());
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.day-view.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function customers_search()
    {
        error_reporting(E_ALL);
        $search_result = $this->schedule_model->customers_search($this->input->post('query'));
        $data = array();
        foreach ($search_result as $user) {
            //$userdetail = $user->customer_name . ',  ' . $user->mobile_number_1 . ', ' . $user->mobile_number_2;
            $userdetail = $user->customer_name;
            $data[] = array("id" => $user->customer_id, "text" => nl2br($userdetail));
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
    public function save_booking()
    {
        try {
            // log_message('error','post save booking'.json_encode($this->input->post()));
            header('Content-Type: application/json; charset=utf-8');

            // Check if maid is flagged
            $maid_id = $this->input->post('maid_id');
            $customer_id = $this->input->post('customer_id');
            $is_maidflagged = $this->schedule_model->is_maidflagged($maid_id, $customer_id);

            if ($is_maidflagged) {
                $response['status'] = "failed";
                $response['message']='This maid is blocked by this customer.';
                echo json_encode($response);
                return;
            }
            $settings = $this->settings_model->get_settings();
            $time_from = strtotime($this->input->post('time_from'));
            $time_to = strtotime($this->input->post('time_to'));
            $working_minutes = ($time_to - $time_from) / 60;
            $working_hours = $working_minutes / 60;
            $booking_data['customer_id'] = $this->input->post('customer_id');
            /***************************************************************** */
            $customer = $this->db->select("c.default_booking_address")
                ->from('customers as c')
                ->where('c.customer_id', $booking_data['customer_id'])->get()->row();
            if ($customer->default_booking_address == "") {
                $response['status'] = "failed";
                $response['message'] = "Please fill the customer address";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            /***************************************************************** */
            $booking_data['service_start_date'] = $this->input->post('service_start_date');
            if ($this->input->post('booking_type') == "OD") {
                $booking_data['booking_type'] = "OD";
                $booking_data['service_end_date'] = $booking_data['service_start_date'];
                $booking_data['service_actual_end_date'] = $booking_data['service_start_date'];
                $booking_data['service_end'] = 1;
            } else {
                $booking_data['booking_type'] = "WE";
                if ($this->input->post('service_end_date') == "") {
                    $booking_data['service_end_date'] = $this->input->post('service_start_date');
                    $booking_data['service_actual_end_date'] = $this->input->post('service_start_date');
                    $booking_data['service_end'] = 0;
                } else {
                    $booking_data['service_end_date'] = $this->input->post('service_end_date');
                    $booking_data['service_actual_end_date'] = $this->input->post('service_end_date');
                    $booking_data['service_end'] = 1;
                }
            }
            /***************************************************************** */
            $booking_data['charge_type'] = $this->input->post('charge_type1') ?: 0;
            $booking_data['customer_address_id'] = $this->input->post('customer_address_id') ?: $customer->default_booking_address;
            $booking_data['maid_id'] = $this->input->post('maid_id');
            $booking_data['service_type_id'] = $this->input->post('service_type_id');
            $booking_data['time_from'] = $this->input->post('time_from');
            $booking_data['time_to'] = $this->input->post('time_to');
            $booking_data['price_per_hr'] = $this->input->post('service_rate_per_hour');
            $booking_data['discount_price_per_hr'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['discount'] = $this->input->post('service_discount');
            $booking_data['vat_charge'] = $this->input->post('service_vat_amount');
            $booking_data['net_service_charge'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['service_charge'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['net_service_cost'] = $booking_data['discount_price_per_hr'] * $working_hours;
            $booking_data['service_week_day'] = $this->input->post('service_week_day');
            $booking_data['total_amount'] = $this->input->post('taxed_total');
            $booking_data['booking_status'] = 1;
            $booking_data['booked_datetime'] = date('Y-m-d H:i:s');
            /***************************************************************** */
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['cleaning_material'] = "Y";
                $booking_data['cleaning_material_fee'] = $this->input->post('material_fee');
            } else {
                $booking_data['cleaning_material'] = "N";
                $booking_data['cleaning_material_fee'] = 0;
            }
            $booking_data['booking_category'] = "C";
            /************************************************ */
            // some understandable new fields
            $booking_data['_service_hours'] = $working_hours;
            // $booking_data['_cleaning_material_rate_per_hour'] = 10;
            $booking_data['_cleaning_material_rate_per_hour'] = $this->input->post('cleaning_material_rate_per_hour1') ?: 0;
            $booking_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
            $booking_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['_service_amount'] = $booking_data['_service_rate_per_hour'] * $working_hours;
            $booking_data['_service_discount'] = $booking_data['_service_amount'] - ($booking_data['_service_discount_rate_per_hour'] * $working_hours);
            $booking_data['_net_service_amount'] = $booking_data['_service_amount'] - $booking_data['_service_discount'];
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['material_cost'] = $this->input->post('material_cost');
                $booking_data['_cleaning_material'] = "Y";
                $booking_data['_net_cleaning_material_amount'] = $booking_data['_cleaning_material_rate_per_hour'] * $working_hours;
            } else {
                $booking_data['_cleaning_material'] = "N";
                $booking_data['material_cost'] = "";
                $booking_data['_net_cleaning_material_amount'] = 0;
            }
            $booking_data['_total_discount'] = $booking_data['_service_discount'];
            $booking_data['_taxable_amount'] = $booking_data['_net_service_amount'] + $booking_data['_net_cleaning_material_amount'];
            $booking_data['_vat_percentage'] = $settings->service_vat_percentage;
            $booking_data['_vat_amount'] = ($booking_data['_vat_percentage'] / 100) * $booking_data['_taxable_amount'];
            $booking_data['_total_amount'] = $booking_data['_taxable_amount'] + $booking_data['_vat_amount'];
            $booking_data['created_at'] = date('Y-m-d H:i:s');
            $booking_data['updated_at'] = date('Y-m-d H:i:s');
            /***************************************************************** */
            // check if it's overlapping other bookings
            $booking = new stdClass();
            $booking->service_start_date = $booking_data['service_start_date'];
            $booking->service_week_day = $booking_data['service_week_day'];
            $booking->service_actual_end_date = $booking_data['service_actual_end_date'];
            $booking->time_from = $booking_data['time_from'];
            $booking->time_to = $booking_data['time_to'];
            $booking->service_end = $booking_data['service_end'];
            $booking->booking_type = $booking_data['booking_type'];
            $timed_bookings = get_same_timed_bookings($booking, null);
            $booking_ids = array_column($timed_bookings,'booking_id');
            $deleted_booking_data = get_deleted_booking($booking_data);
          
            $book_id = array_column($deleted_booking_data,'day_service_booking_id');
            $result = array_diff($booking_ids,$book_id);
            if(sizeof($result) > 0)
            {
                $busy_maid_ids = array_column($timed_bookings,'maid_id');
                //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                throw new Exception('Selected time slot not available.', 100);
                }
            }  
            $day_services = new stdClass();
            $day_services->time_from = $booking_data['time_from'];
            $day_services->time_to =  $booking_data['time_from'];
            $day_services->service_date = $booking_data['service_start_date'];
            
            $timed_day_service = get_same_timed_day_service($day_services, null);
            $busy_maid_id = array_column($timed_day_service,'maid_id');
            // echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
            // if (in_array($booking_data['maid_id'], $busy_maid_id)) {
            //     throw new Exception('Selected time slot not available.', 100);
            // }
            $day_service_id = array_column($deleted_booking_data,'day_service_id');
            $day_service_ids = array_column($timed_day_service,'day_service_id');
            $resultarray = array_diff($day_service_ids,$day_service_id);
            if(sizeof($resultarray) > 0)
            {
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                if (in_array($day_service->maid_id, $busy_maid_id)) {
                    throw new Exception('Selected time slot not available.', 100);
                }
            } 
            // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
            // $booking_ids = array_column($timed_bookings,'booking_id');
            // // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
            // // $result = array_diff($book_id,$booking_ids);
            // if(sizeof($booking_ids) > 0)
            // {
            //     $busy_maid_ids = array_column($timed_bookings,'maid_id');
            //     if (in_array($booking->maid_id, $busy_maid_ids)) {
            //         throw new Exception('Selected time slot not available.', 100);
            //     }
            // }  
            /***************************************************************** */
            $this->db->trans_begin();
            $this->db->insert('bookings', $booking_data);
            $booking_id = $this->db->insert_id();
            if ($booking_id > 0) {
                $response['status'] = "success";
                if ($this->input->post('booking_type') == "OD") {
                    $response['message'] = "Schedule created successfully.";
                } else {
                    $response['message'] = "New booking created successfully.";
                }
                // add more data
                $booking_data['reference_id'] = "MM-" . $booking_data['booking_type'] . "-" . sprintf("%06d", $booking_id);
                $this->db->set($booking_data)
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                $this->db->trans_commit();
            } else {
                $response['status'] = "failed";
                throw new Exception($this->db->_error_message(), 100);
            }
            
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function update_booking()
    {
        try {
            header('Content-Type: application/json; charset=utf-8');
            $settings = $this->settings_model->get_settings();
            $time_from = strtotime($this->input->post('time_from'));
            $time_to = strtotime($this->input->post('time_to'));
            $working_minutes = ($time_to - $time_from) / 60;
            $working_hours = $working_minutes / 60;
            /***************************************************************** */
            //$booking_data['customer_address_id'] = $this->input->post('customer_address_id') ?: 1;
            $booking_data['service_type_id'] = $this->input->post('service_type_id');
            $booking_data['time_from'] = $this->input->post('time_from');
            $booking_data['time_to'] = $this->input->post('time_to');
            $booking_data['price_per_hr'] = $this->input->post('service_rate_per_hour');
            $booking_data['discount_price_per_hr'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['discount'] = $this->input->post('service_discount');
            $booking_data['vat_charge'] = $this->input->post('service_vat_amount');
            $booking_data['total_amount'] = $this->input->post('taxed_total');
            $booking_data['booking_type'] = "WE";
            /***************************************************************** */
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['cleaning_material'] = "Y";
                $booking_data['cleaning_material_fee'] = $this->input->post('material_fee');
            } else {
                $booking_data['cleaning_material'] = "N";
                $booking_data['cleaning_material_fee'] = 0;
            }
            /***************************************************************** */
            if ($this->input->post('service_end_date') == "") {
                $booking_data['service_end_date'] = null;
                $booking_data['service_end'] = 0;
            } else {
                $booking_data['service_end_date'] = $this->input->post('service_end_date');
                $booking_data['service_end'] = 1;
            }
            /***************************************************************** */
            // check if it's overlapping other bookings
            $booking = $this->db->select("b.*")
                ->from('bookings as b')
                ->where('b.booking_id', $this->input->post('booking_id'))->get()->row();
            $booking->time_from = $this->input->post('time_from');
            $booking->time_to = $this->input->post('time_to');
            $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
            $busy_maid_ids = array_column($timed_bookings,'maid_id');
            //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
            if (in_array($booking->maid_id, $busy_maid_ids)) {
                throw new Exception('Selected time slot not available.', 100);
            }
            /************************************************ */
            // some understandable new fields
            $booking_data['_service_hours'] = $working_hours;
            $booking_data['_cleaning_material_rate_per_hour'] = $this->input->post('cleaning_material_rate_per_hour');
            $booking_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
            $booking_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
            $booking_data['_service_amount'] = $booking_data['_service_rate_per_hour'] * $working_hours;
            $booking_data['_service_discount'] = $booking_data['_service_amount'] - ($booking_data['_service_discount_rate_per_hour'] * $working_hours);
            $booking_data['_net_service_amount'] = $booking_data['_service_amount'] - $booking_data['_service_discount'];
            if ($this->input->post('cleaning_materials') == "Y") {
                $booking_data['_cleaning_material'] = "Y";
                $booking_data['_net_cleaning_material_amount'] = $booking_data['_cleaning_material_rate_per_hour'] * $working_hours;
            } else {
                $booking_data['_cleaning_material'] = "N";
                $booking_data['_net_cleaning_material_amount'] = 0;
            }
            $booking_data['_total_discount'] = $booking_data['_service_discount'];
            $booking_data['_taxable_amount'] = $booking_data['_net_service_amount'] + $booking_data['_net_cleaning_material_amount'];
            $booking_data['_vat_percentage'] = $settings->service_vat_percentage;
            $booking_data['_vat_amount'] = ($booking_data['_vat_percentage'] / 100) * $booking_data['_taxable_amount'];
            $booking_data['_total_amount'] = $booking_data['_taxable_amount'] + $booking_data['_vat_amount'];
            $booking_data['updated_at'] = date('Y-m-d H:i:s');
            //var_dump($booking_data);
            /************************************************ */
            $this->db->trans_begin();
            $this->db->set($booking_data)
                ->where('booking_id', $this->input->post('booking_id'))
                ->update('bookings');
            if ($this->input->post('booking_id') > 0) {
                $response['status'] = "success";
                $response['message'] = "Booking updated successfully.";
                $this->db->trans_commit();
            } else {
                $response['status'] = "failed";
                throw new Exception($this->db->_error_message(), 100);
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function save_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            $service_date = $this->input->post('service_date');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*,bd.booking_delete_id")
                ->from('day_services as ds')
                ->join('booking_deletes as bd', 'ds.day_service_id = bd.day_service_id', 'left')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {

                // day service entry exist
                // if ($day_service->dispatch_status == 2) {
                //     $response['status'] = "failed";
                //     $response['message'] = "Dispatched bookings is can't be changed.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                
                // set update data
                $day_service->time_from = $this->input->post('time_from');
                $day_service->time_to = $this->input->post('time_to');
                $time_from = strtotime($day_service->time_from);
                $time_to = strtotime($day_service->time_to);
                $this->db->select("b.*")
                ->from('bookings as b')
                ->where('b.booking_id', $this->input->post('booking_id'));
                $query = $this->db->get();
                $booking = $query->row();
                $booking->time_from = $this->input->post('time_from');
                $booking->time_to = $this->input->post('time_to');
                $booking_data['service_start_date'] = $booking->service_start_date;
                $booking_data['time_from'] = $booking->time_from;
                $booking_data['time_to'] = $booking->time_to;
                $deleted_booking_data = get_deleted_booking($booking_data);
                $day_service_id = array_column($deleted_booking_data,'day_service_id');
                $timed_day_service = get_same_timed_day_service($day_service, [$day_service->day_service_id]);
                $day_service_ids = array_column($timed_day_service,'day_service_id');
                $resultarray = array_diff($day_service_ids,$day_service_id);
                if(sizeof($resultarray) > 0)
                {
                    $busy_maid_id = array_column($timed_day_service,'maid_id');
                    //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    if (in_array($day_service->maid_id, $busy_maid_id)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }
                } else {
                    $busy_maid_id = array_column($timed_day_service,'maid_id');
                    if (in_array($day_service->maid_id, $busy_maid_id)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }

                }
                $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                $booking_ids = array_column($timed_bookings,'booking_id');
                // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
                // $result = array_diff($book_id,$booking_ids);
                if(sizeof($booking_ids) > 0)
                {
                    $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    if (in_array($booking->maid_id, $busy_maid_ids)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }
                }  
                //  print_r($timed_bookings);die();

                // $timed_bookings_day = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids_day = array_column($timed_bookings_day,'maid_id');
                // if (in_array($booking->maid_id, $busy_maid_ids_day)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                /************************************************ */
                $day_service_data['time_from'] = $this->input->post('time_from');
                $day_service_data['time_to'] = $this->input->post('time_to');
                $day_service_data['service_date'] = $this->input->post('service_date');

                // print_r($day_service_data);die();
                $day_service_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
                $day_service_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = $this->input->post('cleaning_material_rate_per_hour');
                $day_service_data['_net_cleaning_material_amount'] = 0;
                if ($this->input->post('cleaning_materials') == "Y") {
                    $day_service_data['_cleaning_material'] = "Y";
                    $day_service_data['material_cost'] = $this->input->post('material_cost1');
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                } else {
                    $day_service_data['_cleaning_material'] = "N";
                    $day_service_data['material_cost'] = "";
                    $day_service_data['_net_cleaning_material_amount'] = 0;
                }
                $day_service_data['_total_discount'] = $this->input->post('service_discount');
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $this->input->post('service_vat_percentage');
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['cleaning_material'] = $day_service_data['_cleaning_material'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                $day_service_data['charge_type'] = $this->input->post('charge_type') ?: 0;
                /************************************************ */
                $this->db->set($day_service_data)
                    ->where('day_service_id', $day_service->day_service_id)
                    ->update('day_services');
                    if ($this->input->post('cleaning_materials') == "Y") {
                        $cleaning_material = "Y";
                        $material_cost = $this->input->post('material_cost1');
                        $net_cleaning_material_amount = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    } else {
                        $cleaning_material = "N";
                        $material_cost = "";
                        $net_cleaning_material_amount = 0;
                    }
                    $time_from1 = $this->input->post('time_from') ? $this->input->post('time_from'): null;
                    $time_to1 = $this->input->post('time_to') ? $this->input->post('time_to'): null;
                    $service_rate_per_hour = $this->input->post('service_rate_per_hour') ?  $this->input->post('service_rate_per_hour') : null;
                    $service_discount_rate_per_hour = $this->input->post('service_discount_rate_per_hour') ? $this->input->post('service_discount_rate_per_hour') : $booking->_service_discount_rate_per_hour;
                    $charge_type = $this->input->post('charge_type') ?: 0;
                    $this->db->set(array('time_from' => $time_from1,'charge_type' => $charge_type,
                    'time_to' => $time_to1, '_service_rate_per_hour' => $service_rate_per_hour, 'price_per_hr' => $service_rate_per_hour,
                    '_service_discount_rate_per_hour' => $service_discount_rate_per_hour, '_net_cleaning_material_amount' => $net_cleaning_material_amount,
                    'material_cost'=> $material_cost, '_cleaning_material' => $cleaning_material))
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                $response['status'] = "success";
                $response['message'] = "Schedule updated successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                $booking->time_from = $this->input->post('time_from');
                $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // print_r($timed_bookings);die();
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                $booking_data[] = $booking;
                $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                $booking_ids = array_column($timed_bookings,'booking_id');
                $deleted_booking_data = get_deleted_booking($booking_data);
                $book_id = array_column($deleted_booking_data,'day_service_booking_id');
                $result = array_diff($book_id,$booking_ids);
                if(sizeof($result) > 0)
                {
                    $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                    if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                    throw new Exception('Selected time slot not available.', 100);
                    }
                }  
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $this->input->post('time_from') ? $this->input->post('time_from'): $booking->time_from;
                $day_service_data['time_to'] = $this->input->post('time_to') ? $this->input->post('time_to'): $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $service_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $this->input->post('cleaning_materials') == "Y" ? "Y" : "N";
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
                $day_service_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                if ($this->input->post('cleaning_materials') == "Y") {
                    $day_service_data['_cleaning_material'] = "Y";
                    $material_cost = $this->input->post('material_cost1');
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                } else {
                    $day_service_data['_cleaning_material'] = "N";
                    $material_cost = "";
                    $day_service_data['_net_cleaning_material_amount'] = 0;
                }
                $day_service_data['charge_type'] = $this->input->post('charge_type') ?: 0;
                $day_service_data['_total_discount'] = $this->input->post('service_discount');
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];

                $day_services = new stdClass();
                $day_services->time_from = $day_service_data['time_from'];
                $day_services->time_to = $day_service_data['time_to'];
                $day_services->service_date = $day_service_data['service_date'];
                $timed_day_service = get_same_timed_day_service($day_services, null);
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                // echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($day_service_data['maid_id'], $busy_maid_id)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                ->where('day_service_id', $day_service_id)
                ->update('day_services');
                    $time_from = $this->input->post('time_from') ? $this->input->post('time_from'): $booking->time_from;
                    $time_to = $this->input->post('time_to') ? $this->input->post('time_to'): $booking->time_to;
                    $service_rate_per_hour = $this->input->post('service_rate_per_hour') ?  $this->input->post('service_rate_per_hour') : $booking->_service_rate_per_hour;;
                    $service_discount_rate_per_hour = $this->input->post('service_discount_rate_per_hour') ? $this->input->post('service_discount_rate_per_hour') : $booking->_service_discount_rate_per_hour;
                    $charge_type = $this->input->post('charge_type') ?: 0;
                    $this->db->set(array('time_from' => $time_from, 'charge_type' => $charge_type,
                    'time_to' => $time_to, '_service_rate_per_hour' => $service_rate_per_hour, 'price_per_hr' => $service_rate_per_hour,
                   '_service_discount_rate_per_hour' => $service_discount_rate_per_hour, '_net_cleaning_material_amount' => $net_cleaning_material_amount,
                    'material_cost'=> $material_cost, '_cleaning_material' => $cleaning_material))
                    ->where('booking_id', $booking_id)
                    ->update('bookings');
                // update reference id END
                $response['status'] = "success";
                $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function update_one_day()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            $service_date = $this->input->post('service_date');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*,bd.booking_delete_id")
                ->from('day_services as ds')
                ->join('booking_deletes as bd', 'ds.day_service_id = bd.day_service_id', 'left')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {

                // day service entry exist
                // if ($day_service->dispatch_status == 2) {
                //     $response['status'] = "failed";
                //     $response['message'] = "Dispatched bookings is can't be changed.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                
                // set update data
                $day_service->time_from = $this->input->post('time_from');
                $day_service->time_to = $this->input->post('time_to');
                $time_from = strtotime($day_service->time_from);
                $time_to = strtotime($day_service->time_to);
                $this->db->select("b.*")
                ->from('bookings as b')
                ->where('b.booking_id', $this->input->post('booking_id'));
                $query = $this->db->get();
                $booking = $query->row();
                $booking->time_from = $this->input->post('time_from');
                $booking->time_to = $this->input->post('time_to');
                $booking_data['service_start_date'] = $booking->service_start_date;
                $booking_data['time_from'] = $booking->time_from;
                $booking_data['time_to'] = $booking->time_to;
                $deleted_booking_data = get_deleted_booking($booking_data);
                $day_service_id = array_column($deleted_booking_data,'day_service_id');
                $timed_day_service = get_same_timed_day_service($day_service, [$day_service->day_service_id]);
                $day_service_ids = array_column($timed_day_service,'day_service_id');
                $resultarray = array_diff($day_service_ids,$day_service_id);
                if(sizeof($resultarray) > 0)
                {
                    $busy_maid_id = array_column($timed_day_service,'maid_id');
                    //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    if (in_array($day_service->maid_id, $busy_maid_id)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }
                } else {
                    $busy_maid_id = array_column($timed_day_service,'maid_id');
                    if (in_array($day_service->maid_id, $busy_maid_id)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }

                }
                $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                $booking_ids = array_column($timed_bookings,'booking_id');
                // $booking_delete_id = array_column($booking_delete_id,'booking_delete_id');
                // $result = array_diff($book_id,$booking_ids);
                if(sizeof($booking_ids) > 0)
                {
                    $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    if (in_array($booking->maid_id, $busy_maid_ids)) {
                        throw new Exception('Selected time slot not available.', 100);
                    }
                }  
                //  print_r($timed_bookings);die();

                // $timed_bookings_day = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids_day = array_column($timed_bookings_day,'maid_id');
                // if (in_array($booking->maid_id, $busy_maid_ids_day)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                /************************************************ */
                $day_service_data['time_from'] = $this->input->post('time_from');
                $day_service_data['time_to'] = $this->input->post('time_to');
                $day_service_data['service_date'] = $this->input->post('service_date');

                // print_r($day_service_data);die();
                $day_service_data['charge_type'] = $this->input->post('charge_type') ?: 0;
                // print_r($booking_data);die();
                $day_service_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
                $day_service_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = $this->input->post('cleaning_material_rate_per_hour');
                $day_service_data['_net_cleaning_material_amount'] = 0;
                if ($this->input->post('cleaning_materials') == "Y") {
                    $day_service_data['_cleaning_material'] = "Y";
                    $day_service_data['material_cost'] = $this->input->post('material_cost1');
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                } else {
                    $day_service_data['_cleaning_material'] = "N";
                    $day_service_data['_net_cleaning_material_amount'] = 0;
                    $day_service_data['material_cost'] = "";

                }
                $day_service_data['_total_discount'] = $this->input->post('service_discount');
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $this->input->post('service_vat_percentage');
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['cleaning_material'] = $day_service_data['_cleaning_material'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                /************************************************ */
                $this->db->set($day_service_data)
                    ->where('day_service_id', $day_service->day_service_id)
                    ->update('day_services');
                    // print_r($day_service_data);die();
                    // $time_from1 = $this->input->post('time_from') ? $this->input->post('time_from'): null;
                    // $time_to1 = $this->input->post('time_to') ? $this->input->post('time_to'): null;
                    // $this->db->set(array('time_from' => $time_from1,
                    // 'time_to' => $time_to1))
                    // ->where('booking_id', $booking_id)
                    // ->update('bookings');
                $response['status'] = "success";
                $response['message'] = "Schedule updated successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                $booking->time_from = $this->input->post('time_from');
                $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // print_r($timed_bookings);die();
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                $booking_data[] = $booking;
                $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                $booking_ids = array_column($timed_bookings,'booking_id');
                $deleted_booking_data = get_deleted_booking($booking_data);
                $book_id = array_column($deleted_booking_data,'day_service_booking_id');
                $result = array_diff($book_id,$booking_ids);
                if(sizeof($result) > 0)
                {
                    $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    //echo '<pre>';var_dump($timed_bookings);echo '</pre>';die();
                    if (in_array($booking_data['maid_id'], $busy_maid_ids)) {
                    throw new Exception('Selected time slot not available.', 100);
                    }
                }  
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $this->input->post('time_from') ? $this->input->post('time_from'): $booking->time_from;
                $day_service_data['time_to'] = $this->input->post('time_to') ? $this->input->post('time_to'): $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $service_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $this->input->post('cleaning_materials') == "Y" ? "Y" : "N";
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $this->input->post('service_rate_per_hour');
                $day_service_data['_service_discount_rate_per_hour'] = $this->input->post('service_discount_rate_per_hour');
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                if ($this->input->post('cleaning_materials') == "Y") {
                    $day_service_data['_cleaning_material'] = "Y";
                    $day_service_data['material_cost'] = $this->input->post('material_cost1');
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                } else {
                    $day_service_data['_cleaning_material'] = "N";
                    $day_service_data['material_cost'] = "";
                    $day_service_data['_net_cleaning_material_amount'] = 0;
                }
                $day_service_data['_total_discount'] = $this->input->post('service_discount');
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                $day_service_data['charge_type'] = $this->input->post('charge_type') ?: 0;
                $day_services = new stdClass();
                $day_services->time_from = $day_service_data['time_from'];
                $day_services->time_to = $day_service_data['time_to'];
                $day_services->service_date = $day_service_data['service_date'];
                $timed_day_service = get_same_timed_day_service($day_services, null);
                $busy_maid_id = array_column($timed_day_service,'maid_id');
                // echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($day_service_data['maid_id'], $busy_maid_id)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                // print_r($day_service_data);die();
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                ->where('day_service_id', $day_service_id)
                ->update('day_services');
                    // $time_from = $this->input->post('time_from') ? $this->input->post('time_from'): $booking->time_from;
                    // $time_to = $this->input->post('time_to') ? $this->input->post('time_to'): $booking->time_to;
                    // $this->db->set(array('time_from' => $time_from,
                    // 'time_to' => $time_to))
                    // ->where('booking_id', $booking_id)
                    // ->update('bookings');
                // update reference id END
                $response['status'] = "success";
                $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function delete_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*")
                ->from('day_services as ds')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
        //  print_r($day_service);die();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {
                // print_r("helloo");die();
                // day service entry exist
                if ($day_service->service_status == 1) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                if ($day_service->service_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                // if ($day_service->service_status == 3) {
                //     $response['status'] = "failed";
                //     $response['message'] = "schedule can't be deleted.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                if ($day_service->dispatch_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "Dispatched bookings is can't be changed.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                
                // set update data
                $date_range = $service_date .' - '. $service_date;
                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] = $this->input->post('service_date');
                $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Suspend';
                // $booking_deleted_data['delete_date_range'] = $date_range;
                
                /***************************************************************** */
            //    print_r($booking_deleted_data);die();
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                
               
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                // echo'<pre>';
                // print_r($booking);die();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                // $booking->time_from = $this->input->post('time_from');
                // $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $booking->time_from;
                $day_service_data['time_to'] = $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $booking->service_start_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $booking->cleaning_material;
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                $day_service_data['_total_discount'] = $booking->_total_discount;
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                ->where('day_service_id', $day_service_id)
                ->update('day_services');
                $date_range = $service_date .' - '. $service_date;
                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                
                $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                $booking_deleted_data['day_service_id'] = $day_service_id;
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Suspend';
                // $booking_deleted_data['delete_date_range'] = $date_range;
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                    
                   
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
                // // update reference id END
                // $response['status'] = "success";
                // $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
    public function delete_permeant_booking_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remarks');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            $moved_to_booking_id = $booking_id;
            $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
            if (user_authenticate() != 1) {
                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
            }
            if (isset($d_booking->booking_id)) {
                $newsearchdate = date('Y-m-d', strtotime($schedule_date));
                $enddate = $d_booking->service_start_date;
                if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');
                } else {
                    if ($d_booking->booking_type == 'WE') {

                        if ($d_booking->service_start_date > $newsearchdate)
                        {
                            $current_date = strtotime($d_booking->service_start_date);
                            $current_date = strtotime('-7 days', $current_date);
                            $delete_datee = date('Y-m-d', $current_date);
                            // print_r($deletedatee);die();
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $delete_datee);
                            if (count($check_buk_delete) > 0) {

                            } else {
                                $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $delete_datee, 'service_end_date' => $delete_datee, 'service_end' => 1), 'Delete');
                                
                            }
                        } else{
                            $current = strtotime($newsearchdate);
                            $last = strtotime($enddate);
    
                            
                            while ($current >= $last) {
                                $current = strtotime('-7 days', $current);
                                $deletedatee = date('Y-m-d', $current);
                                //  print_r($deletedatee);die();
                                $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                if (count($check_buk_delete) > 0) {
    
                                } else {
                                    $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                    break;
                                }
                            }
                        }
                       
                    }
                }
                $delete_b_fields = array();
                $delete_b_fields['booking_id'] = $booking_id;
                $delete_b_fields['remarks'] = $remarks;
                $delete_b_fields['service_date'] = $service_date;
                // $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_from_date'] = $service_date;
                $delete_b_fields['delete_to_date'] = $service_date;
                $delete_b_fields['added_datetime'] = date('Y-m-d H:i:s');
                $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_booking_type'] = 'Suspend';
                $this->bookings_model->add_booking_delete($delete_b_fields);

                // $delete_b_fields_new = array();
                // $delete_b_fields_new['booking_id'] = $booking_id;
                // $delete_b_fields_new['service_date'] = $service_date;
                // $delete_b_fields_new['remarks'] = $remarks;
                // $delete_b_fields_new['deleted_by'] = user_authenticate();
                // $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');
                // $this->bookings_model->add_booking_remarks($delete_b_fields_new);

                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function delete_booking_one_day()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remarks');
            $delete_date_from = $this->input->post('delete_date_from');
            // print_r($delete_date_from);de();
            $delete_date_from = DateTime::createFromFormat('d/m/Y', $delete_date_from)->format('Y-m-d');
            // print_r($delete_date_from);die();
            $delete_date_to = $this->input->post('delete_date_to');
            $delete_date_to = DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            // $this->db->select("ds.*")
            //     ->from('day_services as ds')
            //     ->where('ds.booking_id', $booking_id)
            //     ->where('ds.service_date', $service_date);
            // $query = $this->db->get();
            // $day_service = $query->row();
            // $start = strtotime($delete_date_from);
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings->service_end == 1 && $delete_date_from > $bookings->service_end_date  )
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if($bookings->service_end == 1 && $delete_date_to > $bookings->service_end_date)
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_from) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_to) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            
            $start = strtotime($delete_date_from);
            $weekday = $bookings->service_week_day;
            $end = strtotime($delete_date_to);
            $weekdays = array();
            // $weekdays = array();
            while (date("w", $start) != $weekday) {
                $start += 86400;
            }
            while ($start <= $end)
            {
                $weekdays[] = date('Y-m-d', $start);
                $start += 604800;
            }
            // print_r($weekdays);die();
            $day_service = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
            ->where_in('ds.service_date', $weekdays)->get()->result();
            // print_r($day_service);die();
            /***************************************************************** */
            
            $response['day_service'] = $day_service;
            // if(sizeof($result) > 0)
            if (sizeof($day_service) > 0){
                $delete_b_fields = [];
                foreach ($day_service as $key => $dayservice) {
                    if ($daservice->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($dayservice->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($dayservice->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if(is_any_ongoing_day_service_for_booking_by_service_date($booking_id,$service_date)){
                    //     exit('ongoing');
                    // }
                    // $get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_id, $service_date);
                    // if (count($get_dayservice_details) > 0) {
                    //     if (user_authenticate_isadmin() != 'Y' && $get_dayservice_details->odoo_package_activity_status == 1) {
                    //         echo 'odoorefresh';
                    //         exit();

                    //     }
                    // }
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                    if (user_authenticate() != 1) {
                        if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                            echo 'locked';
                            exit();
                        }
                    }

                    $date_range = $delete_date_from .' - '. $delete_date_to ;
                   $delete_from_date =  current($weekdays);
                   $delete_to_date =  end($weekdays);
                    if (isset($d_booking->booking_id)) {
                        if ($d_booking->booking_type != 'OD') {
                            $delete_b_fields[$key]['booking_id'] = $booking_id;
                            $delete_b_fields[$key]['remarks'] = $remarks;
                            $delete_b_fields[$key]['service_date'] = $dayservice->service_date;
                            $delete_b_fields[$key]['day_service_id'] = $dayservice->day_service_id;
                            $delete_b_fields[$key]['delete_from_date'] = $delete_from_date;
                            $delete_b_fields[$key]['delete_to_date'] = $delete_to_date;
                            $delete_b_fields[$key]['added_datetime'] = date('Y-m-d H:i:s');
                            $delete_b_fields[$key]['delete_date_range'] = $date_range;
                            $delete_b_fields[$key]['deleted_by'] = user_authenticate();
                            $delete_b_fields[$key]['delete_booking_type'] = 'Suspend';
                        }
                    }
                }
            //  print_r($delete_b_fields);die();
                $this->db->insert_batch('booking_deletes', $delete_b_fields);
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
           
            else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;

                $start = strtotime($delete_date_from);
                $weekday = $booking->service_week_day;
                // print_r($start);die();
                $end = strtotime($delete_date_to);
                $weekdays = array();
                while (date("w", $start) != $weekday) {
                    $start += 86400;
                }
                while ($start <= $end)
                {
                    $weekdays[] = date('Y-m-d', $start);
                    $start += 604800;  
                }
                $day_service_data = [];
                foreach ($weekdays as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $booking->customer_id;
                    $day_service_data[$key]['maid_id'] = $booking->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $booking->time_from;
                    $day_service_data[$key]['time_to'] = $booking->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $booking->service_type_id;
                    $day_service_data[$key]['customer_name'] = $booking->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $booking->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
                // print_r($day_service_data[$key]['service_date']);die();

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

            $date_range = $delete_date_from .' - '. $delete_date_to ;
            $booking_deleted_data = [];
            $delete_from_date =  current($weekdays);
            $delete_to_date =  end($weekdays);
            foreach ($day_services as $key => $day_service) {
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $date_range;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';

            }
                
               
                // $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                // $booking_deleted_data['service_date'] = $service_date;
                // $booking_deleted_data['day_service_id'] = $day_service_id;
                // $booking_deleted_data['remarks'] = $remarks;
                // $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                // $booking_deleted_data['deleted_by'] = user_authenticate();
                // print_r($booking_deleted_data);die();
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();   
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspended_view()
    {
        $date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $data['filter_start_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $date = $this->input->get('filter_end_date') ?: date('Y-m-d');
        $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +30 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +30 days"));
        $filter_start_date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $filter_end_date = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +30 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +30 days"));
        $maids_leave_on_date = $this->maids_model->get_maids_leave_by_date_we($filter_start_date, $filter_end_date);
        //    print_r($data['filter_end_date']);die();
        $data['maids_leave_on_date'] = $maids_leave_on_date;
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['services'] = $this->settings_model->get_services_maid();
        $data['suspend_reasons'] = $this->schedule_model->get_suspend_resons();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_suspended_view', $data, true);
        $layout_data['page_title'] = 'Suspended Schedule';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.suspended-view.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function cancel_view()
    {
        $date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $data['filter_start_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $date = $this->input->get('filter_end_date') ?: date('Y-m-d');
        $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +30 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +30 days"));
    //    print_r($data['filter_end_date']);die();
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['services'] = $this->settings_model->get_services_maid();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_cancel_view', $data, true);
        $layout_data['page_title'] = 'Cancelled Schedule';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.cancelled-view.js');
        $this->load->view('layouts/default', $layout_data);
    }

    // Cancel Schedule

    public function cancel_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*")
                ->from('day_services as ds')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
        //  print_r($day_service);die();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {
                // print_r("helloo");die();
                // day service entry exist
                if ($day_service->service_status == 1) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                if ($day_service->service_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                // if ($day_service->service_status == 3) {
                //     $response['status'] = "failed";
                //     $response['message'] = "schedule can't be deleted.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                if ($day_service->dispatch_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "Dispatched bookings is can't be changed.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                
                // set update data
                $date_range = $service_date .' - '. $service_date;
                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] = $this->input->post('service_date');
                $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_date_range'] = $date_range;
                $booking_deleted_data['delete_booking_type'] = 'Cancel';
                 
                /***************************************************************** */
            //    print_r($booking_deleted_data);die();
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                
               
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                // echo'<pre>';
                // print_r($booking);die();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                // $booking->time_from = $this->input->post('time_from');
                // $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $booking->time_from;
                $day_service_data['time_to'] = $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $booking->service_start_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $booking->cleaning_material;
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                $day_service_data['_total_discount'] = $booking->_total_discount;
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                ->where('day_service_id', $day_service_id)
                ->update('day_services');
                $date_range = $service_date .' - '. $service_date;
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                
                $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                $booking_deleted_data['day_service_id'] = $day_service_id;
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Cancel';
                $booking_deleted_data['delete_date_range'] = $date_range;
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                    
                   
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
                // // update reference id END
                // $response['status'] = "success";
                // $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function cancel_booking_one_day()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remarks');
            $delete_date_from = $this->input->post('delete_date_from');
            // print_r($delete_date_from);de();
            $delete_date_from = DateTime::createFromFormat('d/m/Y', $delete_date_from)->format('Y-m-d');
            // print_r($delete_date_from);die();
            $delete_date_to = $this->input->post('delete_date_to');
            $delete_date_to = DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            // $this->db->select("ds.*")
            //     ->from('day_services as ds')
            //     ->where('ds.booking_id', $booking_id)
            //     ->where('ds.service_date', $service_date);
            // $query = $this->db->get();
            // $day_service = $query->row();
            // $start = strtotime($delete_date_from);
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings->service_end == 1 && $delete_date_from > $bookings->service_end_date  )
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if($bookings->service_end == 1 && $delete_date_to > $bookings->service_end_date)
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_from) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_to) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            
            $start = strtotime($delete_date_from);
            $weekday = $bookings->service_week_day;
            $end = strtotime($delete_date_to);
            $weekdays = array();
            // $weekdays = array();
            while (date("w", $start) != $weekday) {
                $start += 86400;
            }
            while ($start <= $end)
            {
                $weekdays[] = date('Y-m-d', $start);
                $start += 604800;
            }
            // print_r($weekdays);die();
            $day_service = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
            ->where_in('ds.service_date', $weekdays)->get()->result();
            // print_r($day_service);die();
            /***************************************************************** */
            
            $response['day_service'] = $day_service;
            // if(sizeof($result) > 0)
            if (sizeof($day_service) > 0){
                $delete_b_fields = [];
                foreach ($day_service as $key => $dayservice) {
                    if ($daservice->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($dayservice->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($dayservice->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if(is_any_ongoing_day_service_for_booking_by_service_date($booking_id,$service_date)){
                    //     exit('ongoing');
                    // }
                    // $get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_id, $service_date);
                    // if (count($get_dayservice_details) > 0) {
                    //     if (user_authenticate_isadmin() != 'Y' && $get_dayservice_details->odoo_package_activity_status == 1) {
                    //         echo 'odoorefresh';
                    //         exit();

                    //     }
                    // }
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                    if (user_authenticate() != 1) {
                        if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                            echo 'locked';
                            exit();
                        }
                    }

                    $date_range = $delete_date_from .' - '. $delete_date_to ;
                   $delete_from_date =  current($weekdays);
                   $delete_to_date =  end($weekdays);
                    if (isset($d_booking->booking_id)) {
                        if ($d_booking->booking_type != 'OD') {
                            $delete_b_fields[$key]['booking_id'] = $booking_id;
                            $delete_b_fields[$key]['remarks'] = $remarks;
                            $delete_b_fields[$key]['service_date'] = $dayservice->service_date;
                            $delete_b_fields[$key]['day_service_id'] = $dayservice->day_service_id;
                            $delete_b_fields[$key]['delete_from_date'] = $delete_from_date;
                            $delete_b_fields[$key]['delete_to_date'] = $delete_to_date;
                            $delete_b_fields[$key]['added_datetime'] = date('Y-m-d H:i:s');
                            $delete_b_fields[$key]['delete_date_range'] = $date_range;
                            $delete_b_fields[$key]['deleted_by'] = user_authenticate();
                            $delete_b_fields[$key]['delete_booking_type'] = 'Cancel';
                        }
                    }
                }
            //  print_r($delete_b_fields);die();
                $this->db->insert_batch('booking_deletes', $delete_b_fields);
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
           
            else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;

                $start = strtotime($delete_date_from);
                $weekday = $booking->service_week_day;
                // print_r($start);die();
                $end = strtotime($delete_date_to);
                $weekdays = array();
                while (date("w", $start) != $weekday) {
                    $start += 86400;
                }
                while ($start <= $end)
                {
                    $weekdays[] = date('Y-m-d', $start);
                    $start += 604800;  
                }
                $day_service_data = [];
                foreach ($weekdays as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $booking->customer_id;
                    $day_service_data[$key]['maid_id'] = $booking->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $booking->time_from;
                    $day_service_data[$key]['time_to'] = $booking->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $booking->service_type_id;
                    $day_service_data[$key]['customer_name'] = $booking->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $booking->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
                // print_r($day_service_data[$key]['service_date']);die();

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

            $date_range = $delete_date_from .' - '. $delete_date_to ;
            $booking_deleted_data = [];
            $delete_from_date =  current($weekdays);
            $delete_to_date =  end($weekdays);
            foreach ($day_services as $key => $day_service) {
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $date_range;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['delete_booking_type'] = 'Cancel';
            }
                
               
                // $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                // $booking_deleted_data['service_date'] = $service_date;
                // $booking_deleted_data['day_service_id'] = $day_service_id;
                // $booking_deleted_data['remarks'] = $remarks;
                // $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                // $booking_deleted_data['deleted_by'] = user_authenticate();
                // print_r($booking_deleted_data);die();
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();   
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function cancel_permeant_booking_schedule()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remarks');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            $moved_to_booking_id = $booking_id;
            $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
            if (user_authenticate() != 1) {
                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
            }
            if (isset($d_booking->booking_id)) {
                $newsearchdate = date('Y-m-d', strtotime($schedule_date));
                $enddate = $d_booking->service_start_date;
                if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');
                } else {
                    if ($d_booking->booking_type == 'WE') {

                        if ($d_booking->service_start_date > $newsearchdate)
                        {
                            $current_date = strtotime($d_booking->service_start_date);
                            $current_date = strtotime('-7 days', $current_date);
                            $delete_datee = date('Y-m-d', $current_date);
                            // print_r($deletedatee);die();
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $delete_datee);
                            if (count($check_buk_delete) > 0) {

                            } else {
                                $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $delete_datee, 'service_end_date' => $delete_datee, 'service_end' => 1), 'Delete');
                                
                            }
                        } else{
                            $current = strtotime($newsearchdate);
                            $last = strtotime($enddate);
    
                            
                            while ($current >= $last) {
                                $current = strtotime('-7 days', $current);
                                $deletedatee = date('Y-m-d', $current);
                                //  print_r($deletedatee);die();
                                $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                if (count($check_buk_delete) > 0) {
    
                                } else {
                                    $this->bookings_model->update_booking($booking_id, array('service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                    break;
                                }
                            }
                        }
                       
                    }
                }
                $delete_b_fields = array();
                $delete_b_fields['booking_id'] = $booking_id;
                $delete_b_fields['remarks'] = $remarks;
                $delete_b_fields['service_date'] = $service_date;
                // $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_from_date'] = $service_date;
                $delete_b_fields['delete_to_date'] = $service_date;
                $delete_b_fields['added_datetime'] = date('Y-m-d H:i:s');
                $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_booking_type'] = 'Cancel';
                $this->bookings_model->add_booking_delete($delete_b_fields);

                // $delete_b_fields_new = array();
                // $delete_b_fields_new['booking_id'] = $booking_id;
                // $delete_b_fields_new['service_date'] = $service_date;
                // $delete_b_fields_new['remarks'] = $remarks;
                // $delete_b_fields_new['deleted_by'] = user_authenticate();
                // $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');
                // $this->bookings_model->add_booking_remarks($delete_b_fields_new);

                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function day_view_list()
    {
        
        $date = $this->input->get('service_date') ?: date('Y-m-d');
        $data['service_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['services'] = $this->settings_model->get_services_maid();
        $data['cancel_reasons'] = $this->schedule_model->get_cancel_resons();
        $data['suspend_reasons'] = $this->schedule_model->get_suspend_resons();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_day_view_list', $data, true);
        $layout_data['page_title'] = 'Schedule Day View List';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('demo.css','jquery.fancybox.css', 'demo.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        // $layout_data['js_files'] = array('base.js', 'moment.js','ajaxupload.3.5.js', 'jquery.dataTables.min.js','jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.day-view-list.js');
        $layout_data['js_files'] = array('base.js', 'moment.js','ajaxupload.3.5.js', 'jquery.dataTables.min.js','jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.day-view-list.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function week_view_list()
    {
        $date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $data['filter_start_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $date = $this->input->get('filter_end_date') ?: date('Y-m-d');
        $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +7 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +7 days"));
        // $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +30 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +30 days"));
    //    print_r($data['filter_end_date']);die();
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['cancel_reasons'] = $this->schedule_model->get_cancel_resons();
        $data['services'] = $this->settings_model->get_services_maid();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_week_view_list', $data, true);
        $layout_data['page_title'] = 'Schedule Week View List';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('demo.css','jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'moment.js','ajaxupload.3.5.js', 'jquery.dataTables.min.js','jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.week-view-list.js');
        // $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.week-view-list.js');
        $this->load->view('layouts/default', $layout_data);
    }

    public function suspended_view_list()
    {
        $date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $data['filter_start_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $date = $this->input->get('filter_end_date') ?: date('Y-m-d');
        $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +30 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +30 days"));
     //    print_r($data['filter_end_date']);die();
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['services'] = $this->settings_model->get_services_maid();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_suspended_view_list', $data, true);
        $layout_data['page_title'] = 'Suspended Schedule';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('demo.css','jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'moment.js','ajaxupload.3.5.js', 'jquery.dataTables.min.js','jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.suspended-view-list.js');
        // $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.suspended-view.js');
        $this->load->view('layouts/default', $layout_data);
    }
    
    public function cancel_view_list()
    {
        $date = $this->input->get('filter_start_date') ?: date('Y-m-d');
        $data['filter_start_date'] = isValidDate($date) ? $date : date('Y-m-d');
        $date = $this->input->get('filter_end_date') ?: date('Y-m-d');
        $data['filter_end_date'] = $this->input->get('filter_end_date') ? (isValidDate($date) ? date("Y-m-d", strtotime("$date +30 days")) : date('Y-m-d')) : date("Y-m-d", strtotime("$date +30 days"));
     //    print_r($data['filter_end_date']);die();
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data = array();
        $data['maids'] = $this->schedule_model->get_maids(1, []);
        $data['week_days'] = $this->schedule_model->get_week_days();
        $data['services'] = $this->settings_model->get_services_maid();
        //var_dump($data['week_days']);die();
        $layout_data['content_body'] = $this->load->view('schedule/schedule_cancel_view_list', $data, true);
        $layout_data['page_title'] = 'Cancelled Schedule';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('demo.css','jquery.fancybox.css', 'mollymaid.css', 'datepicker.css', 'toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('base.js', 'moment.js','ajaxupload.3.5.js', 'jquery.dataTables.min.js','jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.cancelled-view-list.js');
        // $layout_data['js_files'] = array('moment.js', 'jquery.fancybox.pack.js', 'bootstrap-datepicker.js', 'schedule.cancelled-view.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function suspend_one_day_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $service_date)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Dispatched bookings is can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    
                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    $this->db->trans_commit();
                    // print_r($booking_delete_id);die();
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspend_one_day_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('service_date');
            $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_week_day = date('w', strtotime($service_date));
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_week_day', $service_week_day)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                if ($bookings->service_end == 1 && $service_date > $bookings->service_end_date  )
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                // if($bookings->service_end == 1 && $service_date > $bookings->service_end_date)
                // {
                //     $response['status'] = "failed";
                //     $response['message'] = "Booking not available in this date.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }

                if ($bookings->service_start_date > $service_date) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not started in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Dispatched bookings is can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    
                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $service_date;
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // print_r($day_service_id);die();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    
                    $booking_deleted_data['service_date'] =  $service_date;
                    // $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    // print_r($booking_deleted_data);die();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";

                }
                $this->db->trans_commit();
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspend_date_range_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('delete_date_from');
            $delete_date_from = $this->input->post('delete_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('delete_date_from'))->format('Y-m-d') : null;
            $delete_date_to = $this->input->post('delete_date_to') ? DateTime::createFromFormat('d/m/Y', $this->input->post('delete_date_to'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $delete_date_from)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
              
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $delete_date_from);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Dispatched bookings is can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    
                    // set update data
                    $date_range = $delete_date_from .' - '. $delete_date_to;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $delete_date_from;
                    $booking_deleted_data['delete_to_date'] =  $delete_date_to;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_date';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $delete_date_from .' - '. $delete_date_to;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $delete_date_from;
                    $booking_deleted_data['delete_to_date'] =  $delete_date_to;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Suspend';
                    $booking_deleted_data['delete_status'] = 'suspend_date';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function suspend_date_range_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remark');
            $delete_date_from = $this->input->post('delete_date_from');
            // print_r($delete_date_from);de();
            $delete_date_from = DateTime::createFromFormat('d/m/Y', $delete_date_from)->format('Y-m-d');
            // print_r($delete_date_from);die();
            $delete_date_to = $this->input->post('delete_date_to');
            $delete_date_to = DateTime::createFromFormat('d/m/Y', $delete_date_to)->format('Y-m-d');
            //    print_r($delete_date_from);
            // print_r($delete_date_to);die();
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            // $this->db->select("ds.*")
            //     ->from('day_services as ds')
            //     ->where('ds.booking_id', $booking_id)
            //     ->where('ds.service_date', $service_date);
            // $query = $this->db->get();
            // $day_service = $query->row();
            // $start = strtotime($delete_date_from);
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings->service_end == 1 && $delete_date_from > $bookings->service_end_date  )
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if($bookings->service_end == 1 && $delete_date_to > $bookings->service_end_date)
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_from) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            if ($bookings->service_start_date > $delete_date_to) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not started in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            
            $start = strtotime($delete_date_from);
            $weekday = $bookings->service_week_day;
            $end = strtotime($delete_date_to);
            $weekdays = array();
            // $weekdays = array();
            while (date("w", $start) != $weekday) {
                $start += 86400;
            }
            while ($start <= $end)
            {
                $weekdays[] = date('Y-m-d', $start);
                $start += 604800;
            }
            if (empty($weekdays)) 
            {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
            // print_r($weekdays);die();
            $day_service = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
            ->where_in('ds.service_date', $weekdays)->get()->result();
            // print_r($day_service);die();
            /***************************************************************** */
            
            $response['day_service'] = $day_service;
            // if(sizeof($result) > 0)
            if (sizeof($day_service) > 0){
                // print_r('dsfgdshgfdhhg');die();
                $delete_b_fields = [];
                foreach ($day_service as $key => $dayservice) {
                    if ($daservice->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($dayservice->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($dayservice->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($dayservice->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Confirmed schedule can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if(is_any_ongoing_day_service_for_booking_by_service_date($booking_id,$service_date)){
                    //     exit('ongoing');
                    // }
                    // $get_dayservice_details = $this->bookings_model->check_day_service_booking_new($booking_id, $service_date);
                    // if (count($get_dayservice_details) > 0) {
                    //     if (user_authenticate_isadmin() != 'Y' && $get_dayservice_details->odoo_package_activity_status == 1) {
                    //         echo 'odoorefresh';
                    //         exit();

                    //     }
                    // }
                    $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                    if (user_authenticate() != 1) {
                        if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                            echo 'locked';
                            exit();
                        }
                    }
                    $this->db->select('bd.*')
                    ->from('booking_deletes as bd');
                    // $this->db->where("DATE(bd.delete_from_date) BETWEEN '$delete_date_from1' AND '$delete_to_date1'"); 
                    $this->db->where("bd.booking_id", $booking_id);
                    $get_booking_deletes_qry = $this->db->get()->result();
                    $existing_booking_delete_from_date  = array();
                    foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                        $existing_booking_delete_from_date[] = date('Y-m-d', strtotime($get_booking_data->service_date));
                        $booking_delete_range = $get_booking_data->delete_date_range;
                        // $booking_delete_from_date[] = $get_booking_data->delete_from_date;
                        // $booking_delete_to_date[] = $get_booking_data->delete_to_date;

                    }
                    $dates = explode(' - ', $booking_delete_range);
                    $startDate_range = '';
                    $endDate_range = '';
                    if (count($dates) === 2) {
                        $startDate_range = trim($dates[0]);
                        $endDate_range = trim($dates[1]);
                    } 
                    $date_range = $delete_date_from .' - '. $delete_date_to ;
                    $delete_from_date =  current($weekdays);
                    $delete_to_date =  end($weekdays);
                    if (sizeof($get_booking_deletes_qry) > 0){
                        if (isset($d_booking->booking_id)) {
                            if ($d_booking->booking_type != 'OD') {
                                $delete_b_fields[$key]['booking_id'] = $booking_id;
                                $delete_b_fields[$key]['remarks'] = $remarks;
                                $delete_b_fields[$key]['service_date'] = $dayservice->service_date;
                                $delete_b_fields[$key]['day_service_id'] = $dayservice->day_service_id;
                                $delete_b_fields[$key]['delete_from_date'] = $delete_from_date;
                                $delete_b_fields[$key]['delete_to_date'] = $delete_to_date;
                                $delete_b_fields[$key]['added_datetime'] = date('Y-m-d H:i:s');
                                $delete_b_fields[$key]['delete_date_range'] = $startDate_range .' - '. $delete_to_date;
                                $delete_b_fields[$key]['deleted_by'] = user_authenticate();
                                $delete_b_fields[$key]['delete_booking_type'] = 'Suspend';
                                $delete_b_fields[$key]['delete_status'] = 'suspend_date';
                                $delete_b_fields[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null ;
                            }
                        }
                    }else {
                        if (isset($d_booking->booking_id)) {
                            if ($d_booking->booking_type != 'OD') {
                                $delete_b_fields[$key]['booking_id'] = $booking_id;
                                $delete_b_fields[$key]['remarks'] = $remarks;
                                $delete_b_fields[$key]['service_date'] = $dayservice->service_date;
                                $delete_b_fields[$key]['day_service_id'] = $dayservice->day_service_id;
                                $delete_b_fields[$key]['delete_from_date'] = $delete_from_date;
                                $delete_b_fields[$key]['delete_to_date'] = $delete_to_date;
                                $delete_b_fields[$key]['added_datetime'] = date('Y-m-d H:i:s');
                                $delete_b_fields[$key]['delete_date_range'] = $date_range;
                                $delete_b_fields[$key]['deleted_by'] = user_authenticate();
                                $delete_b_fields[$key]['delete_booking_type'] = 'Suspend';
                                $delete_b_fields[$key]['delete_status'] = 'suspend_date';
                                $delete_b_fields[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null ;
                            }
                        }
                    }
                }
            //  print_r($delete_b_fields);die();
                $this->db->insert_batch('booking_deletes', $delete_b_fields);
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
           
            else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
           
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;

                $start = strtotime($delete_date_from);
                $weekday = $booking->service_week_day;
                // print_r($start);die();
                $end = strtotime($delete_date_to);
                $weekdays = array();
                while (date("w", $start) != $weekday) {
                    $start += 86400;
                }
                while ($start <= $end)
                {
                    $weekdays[] = date('Y-m-d', $start);
                    $start += 604800;  
                }
                if (empty($weekdays)) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                $day_service_data = [];
                foreach ($weekdays as $key => $date) {
                    $day_service_data[$key]['service_date'] = $date;
                    $day_service_data[$key]['booking_id'] = $booking_id;
                    $day_service_data[$key]['customer_id'] = $booking->customer_id;
                    $day_service_data[$key]['maid_id'] = $booking->maid_id;
                    $day_service_data[$key]['driver_id'] = null;
                    $day_service_data[$key]['is_driver_confirmed'] = null;
                    $day_service_data[$key]['time_from'] = $booking->time_from;
                    $day_service_data[$key]['time_to'] = $booking->time_to;
                    $day_service_data[$key]['customer_name'] = "";
                    $day_service_data[$key]['customer_address'] = "";
                    $day_service_data[$key]['customer_payment_type'] = "D";
                    $day_service_data[$key]['service_description'] = null;
                    // $day_service_data[$key]['service_date'] = $service_date;
                    $day_service_data[$key]['start_time'] = null;
                    $day_service_data[$key]['end_time'] = null;
                    $day_service_data[$key]['service_status'] = null;
                    $day_service_data[$key]['payment_status'] = 0;
                    $day_service_data[$key]['service_added_by'] = "B";
                    $day_service_data[$key]['service_added_by_id'] = user_authenticate();
                    $day_service_data[$key]['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data[$key]['dispatch_status'] = null;
                    $day_service_data[$key]['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data[$key]['booking_service_id'] = $booking->service_type_id;
                    $day_service_data[$key]['customer_name'] = $booking->b_customer_name;
                    $day_service_data[$key]['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data[$key]['_service_hours'] = $working_hours;
                    $day_service_data[$key]['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data[$key]['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data[$key]['_service_amount'] = $day_service_data[$key]['_service_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_service_discount'] = ($day_service_data[$key]['_service_rate_per_hour'] * $working_hours) - ($day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data[$key]['_net_service_amount'] = $day_service_data[$key]['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data[$key]['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data[$key]['_net_cleaning_material_amount'] = $day_service_data[$key]['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data[$key]['_total_discount'] = $booking->_total_discount;
                    $day_service_data[$key]['_taxable_amount'] = $day_service_data[$key]['_net_service_amount'] + $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data[$key]['_vat_amount'] = ($day_service_data[$key]['_vat_percentage'] / 100) * $day_service_data[$key]['_taxable_amount'];
                    $day_service_data[$key]['_total_amount'] = $day_service_data[$key]['_taxable_amount'] + $day_service_data[$key]['_vat_amount'];
                    /************************************************ */
                    $day_service_data[$key]['service_rate_per_hour'] = $day_service_data[$key]['_service_rate_per_hour'];
                    $day_service_data[$key]['service_discount_rate_per_hour'] = $day_service_data[$key]['_service_discount_rate_per_hour'];
                    $day_service_data[$key]['material_fee'] = $day_service_data[$key]['_net_cleaning_material_amount'];
                    $day_service_data[$key]['serviceamount'] = $day_service_data[$key]['_net_service_amount'];
                    $day_service_data[$key]['vatamount'] = $day_service_data[$key]['_vat_amount'];
                    $day_service_data[$key]['total_fee'] = $day_service_data[$key]['_total_amount'];
                }
                // print_r($day_service_data[$key]['service_date']);die();

                $this->db->insert_batch('day_services', $day_service_data);
                $this->db->trans_commit();
                $day_services = $this->db->select("ds.*")->from('day_services as ds')->where('ds.booking_id', $booking_id)
                ->where_in('ds.service_date', $weekdays)->get()->result();

            $date_range = $delete_date_from .' - '. $delete_date_to ;
            $booking_deleted_data = [];
            $delete_from_date =  current($weekdays);
            $delete_to_date =  end($weekdays);
            $this->db->select('bd.*')
            ->from('booking_deletes as bd');
            // $this->db->where("DATE(bd.delete_from_date) BETWEEN '$delete_date_from1' AND '$delete_to_date1'"); 
            $this->db->where("bd.booking_id", $booking_id);
            $get_booking_deletes_qry = $this->db->get()->result();
            $existing_booking_delete_from_date  = array();
            foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                $existing_booking_delete_from_date[] = date('Y-m-d', strtotime($get_booking_data->service_date));
                $booking_delete_range = $get_booking_data->delete_date_range;
                // $booking_delete_from_date[] = $get_booking_data->delete_from_date;
                // $booking_delete_to_date[] = $get_booking_data->delete_to_date;

            }
            $dates = explode(' - ', $booking_delete_range);
            $startDate_range = '';
            $endDate_range = '';
            if (count($dates) === 2) {
                $startDate_range = trim($dates[0]);
                $endDate_range = trim($dates[1]);
            } 
            if (sizeof($get_booking_deletes_qry) > 0){
            foreach ($day_services as $key => $day_service) {
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $startDate_range .' - '. $delete_to_date;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';
                $booking_deleted_data[$key]['delete_status'] = 'suspend_date';
                $booking_deleted_data[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
            }
        }
        else {
            foreach ($day_services as $key => $day_service) {
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service->day_service_id))))
                // ->where_in('day_service_id', $day_service->day_service_id)
                // ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service->day_service_id))))
                ->where('day_service_id', $day_service->day_service_id)
                ->update('day_services');
                $booking_deleted_data[$key]['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data[$key]['day_service_id'] = $day_service->day_service_id;
                $booking_deleted_data[$key]['service_date'] = $day_service->service_date;
                $booking_deleted_data[$key]['delete_from_date'] =  $delete_from_date;
                $booking_deleted_data[$key]['delete_to_date'] = $delete_to_date;
                $booking_deleted_data[$key]['remarks'] = $remarks;
                $booking_deleted_data[$key]['delete_date_range'] = $date_range;
                $booking_deleted_data[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data[$key]['deleted_by'] = user_authenticate();
                $booking_deleted_data[$key]['delete_booking_type'] = 'Suspend';
                $booking_deleted_data[$key]['delete_status'] = 'suspend_date';
                $booking_deleted_data[$key]['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
            }
        }
               
                // $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                // $booking_deleted_data['service_date'] = $service_date;
                // $booking_deleted_data['day_service_id'] = $day_service_id;
                // $booking_deleted_data['remarks'] = $remarks;
                // $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                // $booking_deleted_data['deleted_by'] = user_authenticate();
                // print_r($booking_deleted_data);die();
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert_batch('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();   
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function cancel_permeantly_od()
    {
        

        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("ds.*")
                ->from('day_services as ds')
                ->where('ds.booking_id', $booking_id)
                ->where('ds.service_date', $service_date);
            $query = $this->db->get();
            $day_service = $query->row();
        //  print_r($day_service);die();
            /***************************************************************** */
            $response['day_service'] = $day_service;
            if ($day_service) {
                // print_r("helloo");die();
                // day service entry exist
                if ($day_service->service_status == 1) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                if ($day_service->service_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "schedule can't be deleted.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                // if ($day_service->service_status == 3) {
                //     $response['status'] = "failed";
                //     $response['message'] = "schedule can't be deleted.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }
                if ($day_service->dispatch_status == 2) {
                    $response['status'] = "failed";
                    $response['message'] = "Dispatched bookings is can't be changed.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
                
                $moved_to_booking_id = $booking_id;
                $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');

                // set update data
                $date_range = $service_date .' - '. $service_date;
                
                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] = $this->input->post('service_date');
                $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Cancel';
                $booking_deleted_data['delete_status'] = 'cancel_permeantly';
                $booking_deleted_data['delete_date_range'] = $date_range;
                $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                
                /***************************************************************** */
            //    print_r($booking_deleted_data);die();
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                
               
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            } else {

                // get data from booking and insert to day service
                /*************************************************************************************************************************************************************************************************** */
                $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                    ->from('bookings as b')
                    ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                    ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                    ->where('b.booking_id', $booking_id)
                    ->where('b.booking_status', 1);
                $query = $this->db->get();
                $booking = $query->row();
                // echo'<pre>';
                // print_r($booking);die();
                /************************************************ */
                $time_from = strtotime($booking->time_from);
                $time_to = strtotime($booking->time_to);
                $working_minutes = ($time_to - $time_from) / 60;
                $working_hours = $working_minutes / 60;
                // $booking->time_from = $this->input->post('time_from');
                // $booking->time_to = $this->input->post('time_to');
                // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                // if (in_array($booking->maid_id, $busy_maid_ids)) {
                //     throw new Exception('Selected time slot not available.', 100);
                // }
                /************************************************ */
                $day_service_data['booking_id'] = $booking_id;
                $day_service_data['customer_id'] = $booking->customer_id;
                $day_service_data['maid_id'] = $booking->maid_id;
                $day_service_data['driver_id'] = null;
                $day_service_data['is_driver_confirmed'] = null;
                $day_service_data['time_from'] = $booking->time_from;
                $day_service_data['time_to'] = $booking->time_to;
                $day_service_data['customer_name'] = "";
                $day_service_data['customer_address'] = "";
                $day_service_data['customer_payment_type'] = "D";
                $day_service_data['service_description'] = null;
                $day_service_data['service_date'] = $booking->service_start_date;
                $day_service_data['start_time'] = null;
                $day_service_data['end_time'] = null;
                $day_service_data['service_status'] = null;
                $day_service_data['payment_status'] = 0;
                $day_service_data['service_added_by'] = "B";
                $day_service_data['service_added_by_id'] = user_authenticate();
                $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                $day_service_data['dispatch_status'] = null;
                $day_service_data['cleaning_material'] = $booking->cleaning_material;
                $day_service_data['booking_service_id'] = $booking->service_type_id;
                $day_service_data['customer_name'] = $booking->b_customer_name;
                $day_service_data['customer_address'] = $booking->b_customer_address;
                /************************************************ */
                // some understandable new fields
                $day_service_data['_service_hours'] = $working_hours;
                $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                $day_service_data['_total_discount'] = $booking->_total_discount;
                $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                /************************************************ */
                $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                $this->db->insert('day_services', $day_service_data);
                $day_service_id = $this->db->insert_id();
                // update reference id
                // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                //     ->where('day_service_id', $day_service_id)
                //     ->update('day_services');
                $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                ->where('day_service_id', $day_service_id)
                ->update('day_services');
                $date_range = $service_date .' - '. $service_date;
                
                $moved_to_booking_id = $booking_id;
                $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');

                $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                $booking_deleted_data['day_service_id'] = $day_service_id;
                $booking_deleted_data['remarks'] = $this->input->post('remark');
                $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                $booking_deleted_data['deleted_by'] = user_authenticate();
                $booking_deleted_data['delete_booking_type'] = 'Cancel';
                $booking_deleted_data['delete_status'] = 'cancel_permeantly';
                $booking_deleted_data['delete_date_range'] = $date_range;
                $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                /***************************************************************** */
                // $this->db->trans_begin();
                $this->db->insert('booking_deletes', $booking_deleted_data);
                $booking_delete_id = $this->db->insert_id();
                    
                   
                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
                // // update reference id END
                // $response['status'] = "success";
                // $response['message'] = "Schedule entry added successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function cancel_permeantly_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin(); 
            $booking_id = $this->input->post('booking_id');
            $remarks = $this->input->post('remark');
            $schedule_date = date('d-M-Y');
            $service_date = date('Y-m-d', strtotime($schedule_date));
            $moved_to_booking_id = $booking_id;
            $d_booking = $this->bookings_model->get_booking_by_id($booking_id);
            if (user_authenticate() != 1) {
                if ($d_booking->is_locked == 1 && $d_booking->booked_by != user_authenticate()) {
                    echo 'locked';
                    exit();
                }
            }
            if (isset($d_booking->booking_id)) {
                $newsearchdate = date('Y-m-d', strtotime($schedule_date));
                $enddate = $d_booking->service_start_date;
                if ($d_booking->service_start_date == $newsearchdate && $d_booking->service_end_date == $newsearchdate && $d_booking->service_actual_end_date == $newsearchdate) {
                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'moved_to_booking_id' => $moved_to_booking_id), 'Delete');
                } else {
                    if ($d_booking->booking_type == 'WE') {

                        if ($d_booking->service_start_date > $newsearchdate)
                        {
                            $current_date = strtotime($d_booking->service_start_date);
                            $current_date = strtotime('-7 days', $current_date);
                            $delete_datee = date('Y-m-d', $current_date);
                            // print_r($deletedatee);die();
                            $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $delete_datee);
                            if (count($check_buk_delete) > 0) {

                            } else {
                                $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'service_actual_end_date' => $delete_datee, 'service_end_date' => $delete_datee, 'service_end' => 1), 'Delete');
                                
                            }
                        } else{
                            $current = strtotime($newsearchdate);
                            $last = strtotime($enddate);
    
                            
                            while ($current >= $last) {
                                $current = strtotime('-7 days', $current);
                                $deletedatee = date('Y-m-d', $current);
                                //  print_r($deletedatee);die();
                                $check_buk_delete = $this->bookings_model->check_booking_deletes_by_id_and_date($d_booking->booking_id, $deletedatee);
                                if (count($check_buk_delete) > 0) {
    
                                } else {
                                    $this->bookings_model->update_booking($booking_id, array('booking_status' => 2,'service_actual_end_date' => $deletedatee, 'service_end_date' => $deletedatee, 'service_end' => 1), 'Delete');
                                    break;
                                }
                            }
                        }
                       
                    }
                }
                $date_range = $service_date .' - '. $service_date;
                $delete_b_fields = array();
                $delete_b_fields['booking_id'] = $booking_id;
                $delete_b_fields['remarks'] = $remarks;
                $delete_b_fields['service_date'] = $service_date;
                // $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_from_date'] = $service_date;
                $delete_b_fields['delete_to_date'] = $service_date;
                $delete_b_fields['added_datetime'] = date('Y-m-d H:i:s');
                $delete_b_fields['deleted_by'] = user_authenticate();
                $delete_b_fields['delete_booking_type'] = 'Cancel';
                $delete_b_fields['delete_status'] = 'cancel_permeantly';
                $delete_b_fields['delete_date_range'] = $date_range;
                $delete_b_fields['cancel_reason_id'] = $this->input->post('cancel_reason_id') ?: null;
                $this->bookings_model->add_booking_delete($delete_b_fields);

                // $delete_b_fields_new = array();
                // $delete_b_fields_new['booking_id'] = $booking_id;
                // $delete_b_fields_new['service_date'] = $service_date;
                // $delete_b_fields_new['remarks'] = $remarks;
                // $delete_b_fields_new['deleted_by'] = user_authenticate();
                // $delete_b_fields_new['deleted_date_time'] = date('Y-m-d H:i:s');
                // $this->bookings_model->add_booking_remarks($delete_b_fields_new);

                $response['status'] = "success";
                $response['message'] = "Schedule deleted successfully.";
            }
            $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function get_customer_address_details()
    {
        $customer_id = $this->input->post('customer_id');
        $data['customer_address'] = $this->bookings_model->get_customer_address($customer_id);
        echo json_encode($data);

    }

    
    public function cancel_one_day_schedule_od()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            $service_date = $this->input->post('service_date');
            // $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_start_date', $service_date)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
           
            // print_r($time_difference_minutes);die();
            if ($bookings)
            {
                $time_from = $bookings->time_from;
                $time_to = $bookings->time_to;
                $timestamp_from = strtotime($time_from);
                $timestamp_to = strtotime($time_to);
                // Calculate the difference in seconds
                $time_difference_seconds = ($timestamp_to - $timestamp_from) / 60;
                // Convert seconds to minutes
                $time_difference_minutes = $time_difference_seconds / 60;
                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Dispatched bookings is can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    $pay_customer1 = $this->input->post('pay_customer1');
                    if (isset($pay_customer1) && $pay_customer1 == "Y") {
                        $this->db->set(array('charge_customer' => 1))
                        ->where('day_service_id', $day_service->day_service_id)
                        ->update('day_services');
                        // $day_service_data['charge_customer'] = 1;
                    } 
                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;  
                    $customer_id = $bookings->customer_id;  
                    $pay_empoyee = $this->input->post('pay_employee1');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $customer_id;
                        $maid_hour_data['service_date'] = $this->input->post('service_date');
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $time_difference_minutes;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }
                    
                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $this->input->post('service_date');
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id');
                    
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    $this->db->trans_commit();
                    // print_r($booking_delete_id);die();
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $pay_customer1 = $this->input->post('pay_customer1');
                    if (isset($pay_customer1) && $pay_customer1 == "Y") {
                        $day_service_data['charge_customer'] = 1;
                    } 
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $service_date .' - '. $service_date;

                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;  
                    $customer_id  = $bookings->customer_id;  
                    $pay_empoyee = $this->input->post('pay_employee1');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $customer_id;
                        $maid_hour_data['service_date'] = $this->input->post('service_date');
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $time_difference_minutes;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    
                    $booking_deleted_data['service_date'] =  $booking->service_start_date;;
                    $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['delete_to_date'] =  $this->input->post('service_date');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id');
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";
                    $this->db->trans_commit();

                }
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function cancel_one_day_schedule_we()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');
            // print_r($booking_id);die();
            // $service_date = $this->input->post('service_date');
            $service_date = $this->input->post('suspend_date_from') ? DateTime::createFromFormat('d/m/Y', $this->input->post('suspend_date_from'))->format('Y-m-d') : null;
            // print_r($service_date);die();
            $service_week_day = date('w', strtotime($service_date));
            $service_status = $this->input->post('day_service_status');
            $day_service_id = $this->input->post('day_service_id');
            /***************************************************************** */
            $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
            ->where('b.booking_id', $booking_id)
            ->where('b.service_week_day', $service_week_day)
            ->where('b.booking_status', 1);
            $query = $this->db->get();
            $bookings = $query->row();
            if ($bookings)
            {
                $time_from = $bookings->time_from;
                $time_to = $bookings->time_to;
                $timestamp_from = strtotime($time_from);
                $timestamp_to = strtotime($time_to);
                // Calculate the difference in seconds
                $time_difference_seconds = ($timestamp_to - $timestamp_from) / 60;
                // Convert seconds to minutes
                $time_difference_minutes = $time_difference_seconds / 60;
              
                if ($bookings->service_end == 1 && $service_date > $bookings->service_end_date  )
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not available in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                // if($bookings->service_end == 1 && $service_date > $bookings->service_end_date)
                // {
                //     $response['status'] = "failed";
                //     $response['message'] = "Booking not available in this date.";
                //     die(json_encode($response, JSON_PRETTY_PRINT));
                // }

                if ($bookings->service_start_date > $service_date) 
                {
                    $response['status'] = "failed";
                    $response['message'] = "Booking not started in this date.";
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }

                $this->db->select("ds.*")
                    ->from('day_services as ds')
                    ->where('ds.booking_id', $booking_id)
                    ->where('ds.service_date', $service_date);
                $query = $this->db->get();
                $day_service = $query->row();
                //  print_r($day_service);die();
                /***************************************************************** */
                $response['day_service'] = $day_service;
                if ($day_service) {
                    // print_r("helloo");die();
                    // day service entry exist
                    if ($day_service->service_status == 1) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    if ($day_service->service_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "schedule can't be deleted.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    // if ($day_service->service_status == 3) {
                    //     $response['status'] = "failed";
                    //     $response['message'] = "schedule can't be deleted.";
                    //     die(json_encode($response, JSON_PRETTY_PRINT));
                    // }
                    if ($day_service->dispatch_status == 2) {
                        $response['status'] = "failed";
                        $response['message'] = "Dispatched bookings is can't be changed.";
                        die(json_encode($response, JSON_PRETTY_PRINT));
                    }
                    $pay_customer = $this->input->post('pay_customer');
                    if (isset($pay_customer) && $pay_customer == "Y") {
                        $this->db->set(array('charge_customer' => 1))
                        ->where('day_service_id', $day_service->day_service_id)
                        ->update('day_services');
                        // $day_service_data['charge_customer'] = 1;
                    } 
                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;   
                    $customer_id  = $bookings->customer_id;   
                    $pay_empoyee = $this->input->post('pay_employee');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $customer_id;
                        $maid_hour_data['service_date'] = $service_date;
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $time_difference_minutes;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }

                    // set update data
                    $date_range = $service_date .' - '. $service_date;
                    
                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    $booking_deleted_data['service_date'] = $service_date;
                    $booking_deleted_data['day_service_id'] = $this->input->post('day_service_id');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id');
                    /***************************************************************** */
                    //    print_r($booking_deleted_data);die();
                    // $this->db->trans_begin();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                    
                    $this->db->trans_commit();
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                } else {

                    // get data from booking and insert to day service
                    /*************************************************************************************************************************************************************************************************** */
                    $this->db->select("b.*,c.customer_name as b_customer_name,ca.customer_address as b_customer_address")
                        ->from('bookings as b')
                        ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
                        ->join('customer_addresses as ca', 'b.customer_address_id = ca.customer_address_id', 'left')
                        ->where('b.booking_id', $booking_id)
                        ->where('b.booking_status', 1);
                    $query = $this->db->get();
                    $booking = $query->row();
                    // echo'<pre>';
                    // print_r($booking);die();
                    /************************************************ */
                    $time_from = strtotime($booking->time_from);
                    $time_to = strtotime($booking->time_to);
                    $working_minutes = ($time_to - $time_from) / 60;
                    $working_hours = $working_minutes / 60;
                    // $booking->time_from = $this->input->post('time_from');
                    // $booking->time_to = $this->input->post('time_to');
                    // $timed_bookings = get_same_timed_bookings($booking, [$booking->booking_id]);
                    // $busy_maid_ids = array_column($timed_bookings,'maid_id');
                    // //echo '<pre>';var_dump($busy_maid_ids);echo '</pre>';die();
                    // if (in_array($booking->maid_id, $busy_maid_ids)) {
                    //     throw new Exception('Selected time slot not available.', 100);
                    // }
                    /************************************************ */
                    $day_service_data['booking_id'] = $booking_id;
                    $day_service_data['customer_id'] = $booking->customer_id;
                    $day_service_data['maid_id'] = $booking->maid_id;
                    $day_service_data['driver_id'] = null;
                    $day_service_data['is_driver_confirmed'] = null;
                    $day_service_data['time_from'] = $booking->time_from;
                    $day_service_data['time_to'] = $booking->time_to;
                    $day_service_data['customer_name'] = "";
                    $day_service_data['customer_address'] = "";
                    $day_service_data['customer_payment_type'] = "D";
                    $day_service_data['service_description'] = null;
                    $day_service_data['service_date'] = $booking->service_start_date;
                    $day_service_data['start_time'] = null;
                    $day_service_data['end_time'] = null;
                    $day_service_data['service_status'] = null;
                    $day_service_data['payment_status'] = 0;
                    $day_service_data['service_added_by'] = "B";
                    $day_service_data['service_added_by_id'] = user_authenticate();
                    $day_service_data['service_added_datetime'] = date('Y-m-d H:i:s');
                    $day_service_data['dispatch_status'] = null;
                    $day_service_data['cleaning_material'] = $booking->cleaning_material;
                    $day_service_data['booking_service_id'] = $booking->service_type_id;
                    $day_service_data['customer_name'] = $booking->b_customer_name;
                    $day_service_data['customer_address'] = $booking->b_customer_address;
                    /************************************************ */
                    // some understandable new fields
                    $day_service_data['_service_hours'] = $working_hours;
                    $day_service_data['_service_rate_per_hour'] = $booking->_service_rate_per_hour;
                    $day_service_data['_service_discount_rate_per_hour'] = $booking->_service_discount_rate_per_hour;
                    $day_service_data['_service_amount'] = $day_service_data['_service_rate_per_hour'] * $working_hours;
                    $day_service_data['_service_discount'] = ($day_service_data['_service_rate_per_hour'] * $working_hours) - ($day_service_data['_service_discount_rate_per_hour'] * $working_hours);
                    $day_service_data['_net_service_amount'] = $day_service_data['_service_discount_rate_per_hour'] * $working_hours;
                    $day_service_data['_cleaning_material_rate_per_hour'] = 10;
                    $day_service_data['_cleaning_material'] = $booking->_cleaning_material;
                    $day_service_data['_net_cleaning_material_amount'] = $day_service_data['_cleaning_material_rate_per_hour'] * $working_hours;
                    $day_service_data['_total_discount'] = $booking->_total_discount;
                    $day_service_data['_taxable_amount'] = $day_service_data['_net_service_amount'] + $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['_vat_percentage'] = $booking->_vat_percentage;
                    $day_service_data['_vat_amount'] = ($day_service_data['_vat_percentage'] / 100) * $day_service_data['_taxable_amount'];
                    $day_service_data['_total_amount'] = $day_service_data['_taxable_amount'] + $day_service_data['_vat_amount'];
                    /************************************************ */
                    $day_service_data['service_rate_per_hour'] = $day_service_data['_service_rate_per_hour'];
                    $day_service_data['service_discount_rate_per_hour'] = $day_service_data['_service_discount_rate_per_hour'];
                    $day_service_data['material_fee'] = $day_service_data['_net_cleaning_material_amount'];
                    $day_service_data['serviceamount'] = $day_service_data['_net_service_amount'];
                    $day_service_data['vatamount'] = $day_service_data['_vat_amount'];
                    $day_service_data['total_fee'] = $day_service_data['_total_amount'];
                    $pay_customer1 = $this->input->post('pay_customer');
                    if (isset($pay_customer1) && $pay_customer1 == "Y") {
                        $day_service_data['charge_customer'] = 1;
                    } 
                    $this->db->insert('day_services', $day_service_data);
                    $day_service_id = $this->db->insert_id();
                    // print_r($day_service_id);die();
                    // update reference id
                    // $this->db->set(array('day_service_reference_id' => createReference("MM-SERV-", sprintf("%07s", $day_service_id))))
                    //     ->where('day_service_id', $day_service_id)
                    //     ->update('day_services');
                    $this->db->set(array('day_service_reference_id' => createReference("MM-". $booking->booking_type . "-", sprintf("%04s", $day_service_id))))
                    ->where('day_service_id', $day_service_id)
                    ->update('day_services');
                    $date_range = $service_date .' - '. $service_date;
                    
                    //maid cancellation hour
                    $maid_id = $bookings->maid_id;
                    $total_amount  = $bookings->_total_amount;   
                    $customer_id  = $bookings->customer_id;   
                    $pay_empoyee = $this->input->post('pay_employee');
                    if (isset($pay_empoyee) && $pay_empoyee == "Y") {
                        $maid_hour_data['booking_id'] = $this->input->post('booking_id');
                        $maid_hour_data['customer_id'] = $customer_id;
                        $maid_hour_data['service_date'] = $service_date;
                        $maid_hour_data['maid_id'] = $maid_id;
                        $maid_hour_data['total_hours'] = $working_hours;
                        $maid_hour_data['total_amount'] = $total_amount;
                        $maid_hour_data['added_date_time'] = date('Y-m-d H:i:s');
                        $maid_hour_data['deleted_by'] = user_authenticate();
                        $this->db->insert('maid_hour_cancellation', $maid_hour_data);
                    }

                    $booking_deleted_data['booking_id'] = $this->input->post('booking_id');
                    
                    $booking_deleted_data['service_date'] =  $service_date;
                    // $booking_deleted_data['day_service_id'] = $day_service_id;
                    $booking_deleted_data['remarks'] = $this->input->post('remark');
                    $booking_deleted_data['delete_from_date'] =  $service_date;
                    $booking_deleted_data['delete_to_date'] =  $service_date;
                    $booking_deleted_data['added_datetime'] = date('Y-m-d H:i:s');
                    $booking_deleted_data['deleted_by'] = user_authenticate();
                    $booking_deleted_data['delete_booking_type'] = 'Cancel';
                    $booking_deleted_data['delete_status'] = 'cancel_one_day';
                    $booking_deleted_data['delete_date_range'] = $date_range;
                    $booking_deleted_data['cancel_reason_id'] = $this->input->post('cancel_reason_id');
                    /***************************************************************** */
                    // $this->db->trans_begin();
                    // print_r($booking_deleted_data);die();
                    $this->db->insert('booking_deletes', $booking_deleted_data);
                    $booking_delete_id = $this->db->insert_id();
                        
                    
                    $response['status'] = "success";
                    $response['message'] = "Schedule deleted successfully.";
                    // // update reference id END
                    // $response['status'] = "success";
                    // $response['message'] = "Schedule entry added successfully.";

                }
                $this->db->trans_commit();
            } else {
                $response['status'] = "failed";
                $response['message'] = "Booking not available in this date.";
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }

    public function booking_schedule_re_activate()
    {
        header('Content-Type: application/json; charset=utf-8');
        try {
            $this->db->trans_begin();
            $settings = $this->settings_model->get_settings();
            $booking_id = $this->input->post('booking_id');

           
            $this->db->select('bd.*')
            ->from('booking_deletes as bd');
            // $this->db->where('bd.delete_from_date >='. $this->db->escape($service_date));
            $this->db->where("bd.booking_id", $booking_id);
            $get_booking_deletes_qry = $this->db->get()->result();

            $this->db->select('b.*,c.customer_id as customerid, c.mobile_number_1')
            ->from('bookings as b')
            ->join('customers c', 'b.customer_id = c.customer_id');
            $this->db->where("b.booking_id", $booking_id);
            $get_booking_qry = $this->db->get()->row();
            
            
            foreach ($get_booking_deletes_qry as $key => $get_booking_data) {
                $booking_re_activate[$key]['booking_id'] = $booking_id;
                $booking_re_activate[$key]['service_date'] = $get_booking_data->service_date;
                $booking_re_activate[$key]['re_activate_date_from'] = $get_booking_data->delete_from_date;
                $booking_re_activate[$key]['re_activate_date_to'] = $get_booking_data->delete_to_date;
                $booking_re_activate[$key]['added_datetime'] = date('Y-m-d H:i:s');
                $booking_re_activate[$key]['re_activated_by'] = user_authenticate();
                $booking_delete_ids[] = $get_booking_data->booking_delete_id;
            }
            $this->db->insert_batch('booking_re_activate', $booking_re_activate);

            $this->db->where_in("booking_delete_id", $booking_delete_ids);
   
      
            $this->db->delete('booking_deletes');
            $response['status'] = "success";
            $response['message'] = "Schedule activated successfully.";
        
        $this->db->trans_commit();
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
        }
        die(json_encode($response, JSON_PRETTY_PRINT));
    }
}
