<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// Error_reporting(E_ALL);
class Security_pass extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 3)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('security_pass_model');
        $this->load->model('settings_model');
    }
    // ****************************************************************************************************************
    public function index($id = '')
    {
        $pass_id = $id;
        // log_message('error', 'pass_id' . $pass_id);
        if ($pass_id != '') {
            $data['passdetails'] = $this->security_pass_model->getpass_details_by_id($pass_id);
            $data['customers'] = $this->security_pass_model->get_customers();
            $data['maids'] = $this->security_pass_model->get_all_maid();
            // print_r($data['passdetails']);exit();
        } else {
            $data['customers'] = $this->security_pass_model->get_customers();
            $data['maids'] = $this->security_pass_model->get_all_maid();
            // print_r($data['maids']);exit();
        }
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('security_pass', $data, true);
        $layout_data['page_title'] = 'Security Pass';
        $layout_data['meta_description'] = 'Security Pass';
        $layout_data['css_files'] = array('jquery.flexdatalist.css', 'jquery-ui.min-v1.13.2.css',  'bootstrap.min.css', 'datepicker.css', 'toastr.min.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.flexdatalist.js', 'jquery-ui.min-v1.3.2.js', 'bootstrap-datepicker.js', 'cropper.js', 'security_pass.js');
        $this->load->view('layouts/default', $layout_data);
    }
    // ***********************************************************************************************************
    public function save_pass()
    {
        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post(NULL, TRUE);
        $filename = $_FILES['security_pass_image']['name'];
        log_message('error', 'post datas' . json_encode($post));
        log_message('error', 'post datas' . json_encode($filename));

        try {
            $this->db->trans_begin();

            $config['upload_path'] = './uploads/gate_pass/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|doc|docx|pdf';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('security_pass_image')) {
                $file_data = $this->upload->data();
                $current_date = date('y-m-d');
                $original_filename = $file_data['file_name'];
                $new_filename = "pass_image_" . $current_date . "_" . $original_filename;
                rename($config['upload_path'] . $original_filename, $config['upload_path'] . $new_filename);
                $data['file_name'] = $new_filename;
            } else {
                $error = $this->upload->display_errors();
                $response['status'] = "failed";
                $response['message'] = "Error uploading file: " . $error;
                die(json_encode($response, JSON_PRETTY_PRINT));
            }

            $data['maid_id'] = $post['pass_maid_name'] ?: null;
            $data['customer_id'] = $post['pass_customer_name'] ?: null;
            $data['customer_name'] = $post['customer_name'] ?: null;
            $data['maid_name'] = $post['employee_name'] ?: null;
            $data['pass_type'] = $post['pass_type'] ?: null;
            $data['service_date'] = !empty($post['service_date']) ? date('Y-m-d', strtotime($post['service_date'])) : null;
            $data['service_start_date'] = !empty($post['service_start_date']) ? date('Y-m-d', strtotime($post['service_start_date'])) : null;
            $data['service_end_date'] = !empty($post['service_end_date']) ? date('Y-m-d', strtotime($post['service_end_date'])) : null;
            $data['created_by_user'] = user_authenticate();
            $data['created_at'] = $data['updated_at']  = date('Y-m-d H:i:s');
            $pass_id = $this->security_pass_model->create_new_pass($data);

            if ($pass_id > 0) {
                $this->db->trans_commit();

                $response['status'] = "success";
                $response['message'] = "pass created successfully";
                die(json_encode($response, JSON_PRETTY_PRINT));
            } else {
                $response['status'] = false;
                $response['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }
    }
    // ***************************************************************************
    public function update_pass()
    {

        header('Content-Type: application/json; charset=utf-8');
        $post = $this->input->post(NULL, TRUE);
        // log_message('error', 'post update ' . json_encode($post));
        $id = $post['pass_id'];
        try {
            $this->db->trans_begin();

            $config['upload_path'] = './uploads/gate_pass/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|doc|docx|pdf';
            $this->load->library('upload', $config);

            if (!empty($_FILES['security_pass_image']['name'])) {
                if ($this->upload->do_upload('security_pass_image')) {
                    $file_data = $this->upload->data();
                    $current_date = date('y-m-d');
                    $original_filename = $file_data['file_name'];
                    $new_filename = "pass_image_" . $current_date . "_" . $original_filename;
                    rename($config['upload_path'] . $original_filename, $config['upload_path'] . $new_filename);
                    $data['file_name'] = $new_filename;
                } else {
                    $error = $this->upload->display_errors();
                    $response['status'] = "failed";
                    $response['message'] = "Error uploading file: " . $error;
                    die(json_encode($response, JSON_PRETTY_PRINT));
                }
            }

            $data['maid_id'] = $post['pass_maid_name'] ?: null;
            $data['customer_id'] = $post['pass_customer_name'] ?: null;
            $data['customer_name'] = $post['customer_name'] ?: null;
            $data['maid_name'] = $post['employee_name'] ?: null;
            $data['pass_type'] = $post['pass_type'] ?: null;

            if ($data['pass_type'] == 'Daily') {
                $data['service_start_date'] = null;
                $data['service_end_date'] = null;
                $data['service_date'] = !empty($post['service_date']) ? date('Y-m-d', strtotime($post['service_date'])) : null;
            } elseif ($data['pass_type'] == 'Yearly') {
                $data['service_date'] = null;
                $data['service_start_date'] = !empty($post['service_start_date']) ? date('Y-m-d', strtotime($post['service_start_date'])) : null;
                $data['service_end_date'] = !empty($post['service_end_date']) ? date('Y-m-d', strtotime($post['service_end_date'])) : null;
            }
            // $data['service_date'] = !empty($post['service_date']) ? date('Y-m-d', strtotime($post['service_date'])) : null;
            // $data['service_start_date'] = !empty($post['service_start_date']) ? date('Y-m-d', strtotime($post['service_start_date'])) : null;
            // $data['service_end_date'] = !empty($post['service_end_date']) ? date('Y-m-d', strtotime($post['service_end_date'])) : null;
            $data['updated_at']  = date('Y-m-d H:i:s');
            $pass_id = $this->security_pass_model->update_pass($data, $id);

            if ($pass_id == true) {
                $this->db->trans_commit();

                $response['status'] = "success";
                $response['message'] = "pass Updated successfully";
                die(json_encode($response, JSON_PRETTY_PRINT));
            } else {
                $response['status'] = false;
                $response['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
                die(json_encode($response, JSON_PRETTY_PRINT));
            }
        } catch (Exception $e) {
            $response['status'] = "failed";
            $response['message'] = $e->getMessage();
            $this->db->trans_rollback();
            die(json_encode($response, JSON_PRETTY_PRINT));
        }
    }

    // ********************************************************************************************************************
    function pass_list()
    {
        $data = array();
        $data['gate_pass'] = $this->security_pass_model->get_all_pass();
        // print_r($data['gate_pass']);exit;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('pass_list', $data, TRUE);
        $layout_data['page_title'] = 'Gate Pass';
        $layout_data['meta_description'] = 'Gate Pass';
        $layout_data['css_files'] = array('jquery.flexdatalist.css', 'jquery-ui.min-v1.13.2.css', 'bootstrap.min.css', 'datepicker.css', 'toastr.min.css', 'cropper.css');
        $layout_data['external_js_files'] = array();
        $layout_data['maids_active'] = '1';
        $layout_data['js_files'] = array('base.js', 'security_pass.js', 'jquery-ui.min-v1.3.2.js', 'jquery.flexdatalist.js', 'ajaxupload.3.5.js', 'bootstrap-datepicker.js', 'cropper.js', 'moment.min.js', 'jquery.dataTables.min.js');
        $this->load->view('layouts/default', $layout_data);
    }

    // ******************************************************************************************
    public function get_pass_details()
    {
        header('Content-Type: application/json');
        $passId = $this->input->post('pass_id');
        // log_message('error', 'get pass_details pass id' . $passId);
        $sanitizedPassId = filter_var($passId, FILTER_SANITIZE_NUMBER_INT);
        if (!is_numeric($sanitizedPassId)) {
            echo json_encode(['error' => 'Invalid pass_id']);
            return;
        }

        $passdetails = $this->security_pass_model->getpass_details_by_id($sanitizedPassId);
        if (empty($passdetails)) {

            echo json_encode(['error' => 'Pass not found']);
        } else {

            echo json_encode($passdetails);
        }
    }
    // *************************************************************************

}
