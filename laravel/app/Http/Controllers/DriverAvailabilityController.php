<?php

namespace App\Http\Controllers;

use App\Models\DriverAvailability;
use App\Models\Drivers;
use Illuminate\Http\Request;
use Response;

class DriverAvailabilityController extends Controller
{
    public function mapping_data_all_under_driver(Request $request)
    {
        // $from_date = (isset($_REQUEST['from_date']) ? $_REQUEST['from_date'] : date('Y-m-d'));
        // $to_date = (isset($_REQUEST['to_date']) ? $_REQUEST['to_date'] : NULL);
        $drivers = Drivers::select(
            'maid_id as driver_id',
            'maid_name as driver_name')
            ->where([['employee_type_id', '=', 2], ['maid_status', '=', 1]])
            ->get();
        $driver_availabilies = DriverAvailability::
            select(
            'driver_availability.driver_availability_id',
            'driver_availability.driver_id',
            'z.zone_id',
            'z.zone_name',
            'driver_availability.area_id',
            'a.area_name',
            'driver_availability.date_from',
            'driver_availability.date_to',
            'm.maid_name as driver_name')
            ->leftJoin('areas as a', 'driver_availability.area_id', 'a.area_id')
            ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
            ->leftJoin('maids as m', 'driver_availability.driver_id', 'm.maid_id')
            ->where([['a.deleted_at', '=', null], ['driver_availability.deleted_at', '=', null], 
            ['date_from', '<=', $request->from_date],
            ['date_to', '>=', $request->to_date]
            ])
            // ->where(array('da.date_from <=' => $service_date, 'da.date_to >=' => $service_date));
            // ->whereBetween('date_to', [$request->from_date, $request->to_date])
            ->when($request->driver_id, function ($query) use ($request) {
                return $query->where('driver_availability.driver_id', $request->driver_id);
            })
            ->when($request->zone_id, function ($query) use ($request) {
                return $query->where('z.zone_id', $request->zone_id);
            })
            ->get();
        foreach ($drivers as $driver) {
            $response['data'][$driver->driver_id]['driver'] = $driver;
            $response['data'][$driver->driver_id]['availabilities'] = [];
            foreach ($driver_availabilies as $driver_availability) {
                if ($driver_availability->driver_id == $driver->driver_id) {
                    $response['data'][$driver->driver_id]['availabilities'][] = $driver_availability;
                }
            }
        }
        $response['data'] = array_values($response['data']);
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }
}
