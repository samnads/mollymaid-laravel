<?php

namespace App\Http\Controllers;

use App\Models\Bookings;
use App\Models\settings_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class BookingsController extends Controller
{
    public function regular_schedules(Request $request)
    {
        $filter_start_date = (isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m-d'));
        $end_date = (isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : NULL);
        $response['bookings'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.customer_id',
                'c.customer_name',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'b.service_start_date',
                'b.service_actual_end_date',
                'b.service_end',
                'b.service_week_day',
                'b.maid_id',
                'b.booking_reassign_reason',
                'cl.location_name as booking_location',
                'ar.area_id',
                'z.zone_id',
                'z.zone_name as booking_zone',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.delete_booking_type',
                'bd.remarks',
                'bd.delete_status',
                'b.parent_booking_id',
                'b.parent_maid_name',
                'cr.cancel_reason_id',
                'cr.cancel_reason',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('areas as ar', 'ca.area_id', 'ar.area_id')
            ->leftJoin('zones as z', 'ar.zone_id', 'z.zone_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->leftJoin('cancel_resons as cr', 'bd.cancel_reason_id', 'cr.cancel_reason_id')
            ->where([['b.booking_type', '=', 'WE'], ['b.deleted_at', '=', null]])
        //    ->whereRaw('(b.service_actual_end_date >= '.$service_date2. ' and service_end = 1) or ( service_end = 0)')
        //    ->whereRaw('(( b.service_start_date <= ' . $service_date2 . ' and b.booking_type = "WE"))')
            ->orderBy('b.time_from', 'ASC')
            ->where(function ($query) use ($filter_start_date,$end_date) {
                $query->where([['b.service_end', '=', 0]])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $end_date]])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $filter_start_date]]);
            })
            ->get();
        $booking_service_start_date = array();
        $booking_data = array();
        $booking_data1 = array();
        $permeant_deletes = array();
        $booking_data2 = array();
        $booking_data3 = array();
        $booking_data4= array();
        foreach ($response['bookings'] as $key=>$booking) {
          
            $date = date("Y-m-d", strtotime($filter_start_date));
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date2 = date('Y-m-d', strtotime($days[$booking->service_week_day], strtotime($date)));
            // if (($booking->service_start_date == $date2) || ($booking->service_end_date == $date2) || ($booking->service_actual_end_date == $date2)) {
            //     // $booking_data1[$key]['booking_id'] = $booking->booking_id;
            //     unset($response['bookings'][$key]);
            // }
            if ($booking->service_actual_end_date <= $date2)
            {
                // if ($booking->booking_delete_id != null && $booking->delete_date_range == "") {
                if ($booking->booking_delete_id != null && $booking->delete_status  == "cancel_permeantly") {
                    unset($response['bookings'][$key]);
                } 
                // else if ($booking->booking_delete_id != null && $booking->delete_from_date  == $filter_start_date) {
                //     unset($response['bookings'][$key]);
                // } 
                else {
                    $working_minutes = $booking->working_minutes / 60;
                    $booking_data1[$key]['maid_id']  = $booking->maid_id;
                    $booking_data1[$key]['working_minutes']  = $working_minutes;
                    $booking_data2[$key]['working_minutes']  = $working_minutes;
                    // $ids = $booking->service_week_day;
                    // $maid_id = $booking->maid_id;
                    // $booking_data3[$maid_id][$ids][] =  $working_minutes;
                    // $booking_data3[$maid_id][$ids][]=  $maid_id;
                    $booking_data4[$key]['maid_id']  = $booking->maid_id;
                    $booking_data4[$key]['service_week_day']  =  $booking->service_week_day;;
                    $booking_data4[$key]['working_minutes']  = $working_minutes;
                }
            }

            if ($date <= $date2) {
                // $working_minutes = $booking->working_minutes / 60;
                $booking_data[$key]['booking_service_start_date'] = $date2;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
                $booking_data3[$key]['booking_service_start_date'] = $date2;
                $booking_data3[$key]['booking_id'] = $booking->booking_id;
                // $booking_data1[$key]['maid_id']  = $booking->maid_id;
                // $booking_data1[$key]['working_minutes']  = $working_minutes;
                // $booking_data2[$key]['working_minutes']  = $working_minutes;
                // $booking_data4[$key]['maid_id']  = $booking->maid_id;
                //     $booking_data4[$key]['service_week_day']  =  $booking->service_week_day;;
                //     $booking_data4[$key]['working_minutes']  = $working_minutes; 
            }
            if ($date > $date2) {
                $working_minutes = $booking->working_minutes / 60;
                $booking_data[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
                $booking_data3[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data3[$key]['booking_id'] = $booking->booking_id;
                // $booking_data4[$key]['maid_id']  = $booking->maid_id;
                // $booking_data4[$key]['service_week_day']  =  $booking->service_week_day;;
                // $booking_data4[$key]['working_minutes']  = $working_minutes;
                // $booking_service_start_date[] = $booking->service_start_date;
            }
        }  
        $booking_ids = array();
        foreach ($booking_data3 as $key => $value) {
            $booking_ids[] = $value['booking_id'];
            // $deletedata = $deletedata->orWhere(function ($query) use ($value) {
            //     $query->where('bd.booking_id', '=', $value['booking_id'])
            //           ->where('bd.service_date', '=', $value['booking_service_start_date'])
            //          ->where('bd.delete_date_range', '!=', Null);
            //         // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            // });
        }
        // print_r($booking_ids);

        $deletedata = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks');
        foreach ($booking_data3 as $key => $value) {
            $deletedata = $deletedata->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletedata = $deletedata->get();
        $deletedata = json_decode(json_encode($deletedata));
        // print_r($deletedata);
        $deleted_bookings = array();
        foreach ($deletedata as $value) {   
            $deleted_bookings[] = $value->booking_id;
        }
        // print_r($booking_ids);
        // print_r($deleted_bookings);
        $book_ids = array_diff($booking_ids,$deleted_bookings);
        // print_r($book_ids);


        $booking_data5 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        // ->whereNotIn('b.booking_id', $deleted_bookings)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data5 = json_decode(json_encode($booking_data5, true), true);

        $booking_data9 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'), 'b._service_amount')
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        // ->whereNotIn('b.booking_id', $deleted_bookings)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data9 = json_decode(json_encode($booking_data9, true), true);
        $total_sales = 0;
        foreach ($booking_data9 as $value) {   
            $total_sales += $value['_service_amount'];    
        
        }
        
        $booking_data10 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'), 'b._total_amount')
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->whereIn('b.booking_id', $deleted_bookings)
        ->get();
        $booking_data10 = json_decode(json_encode($booking_data10, true), true);
        $lost_sales = 0;
        foreach ($booking_data10 as $value) {   
            $lost_sales += $value['_total_amount'];    
        
        }

        // print_r($booking_data10);
        // $res = array_reduce (
        //     $booking_data5,
        //     function($res, $el) {
        //         if (isset($res[$el['maid_id'].$el['service_week_day']])) {
        //             $res[$el['maid_id'].$el['service_week_day']]['working_minutes'] += $el['working_minutes']; 
        //         } else {
        //             $res[$el['maid_id'].$el['service_week_day']] = $el;
        //         }
                
        //         return $res;
        //     },
        //     [] // initial result - empty array
        // );
        // output result values without keys

        $booking_data11 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->where('b.charge_type', '=', 2)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data11 = json_decode(json_encode($booking_data11, true), true);
        $training_hours = 0;
        foreach ($booking_data11 as $value) {   
            $training_hours += $value['working_minutes'];    
        }

        $booking_data12 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->where('b.charge_type', '=', 1)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data12 = json_decode(json_encode($booking_data12, true), true);
        $free_hours = 0;
        foreach ($booking_data12 as $value) {   
            $free_hours += $value['working_minutes'];    
        }

        $res = array_reduce (
            $booking_data5,
            function($res, $el) {
                if (isset($res[$el['maid_id'].$el['service_week_day']])) {
                    $res[$el['maid_id'].$el['service_week_day']]['working_minutes'] += $el['working_minutes']; 
                } else {
                    $res[$el['maid_id'].$el['service_week_day']] = $el;
                }
                
                return $res;
            },
            [] // initial result - empty array
        );
       $total_hour_week = array_values($res);

            
        $grand_total = 0;
        foreach ($booking_data5 as $value) {   
            $grand_total += $value['working_minutes'];    
        
        }
        // foreach ($booking_data2 as $value) {   
        //     $grand_total += $value['working_minutes'];    
        // }
        $result = array();
        foreach($booking_data5 as $k => $v) {
            $id = $v['maid_id'];
            $result[$id][] = $v['working_minutes'];
        }
        // foreach($booking_data1 as $k => $v) {
        //     $id = $v['maid_id'];
        //     $result[$id][] = $v['working_minutes'];
        // }

        $total_hours = array();
        foreach($result as $key => $value) {
            $total_hours[] = array('maid_id' => $key, 'working_minutes' => array_sum($value));
        }
        // Lost hours
        $deletedata1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.delete_booking_type');
        foreach ($booking_data3 as $key => $value) {
            $deletedata1 = $deletedata1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.delete_booking_type', '=', 'Suspend');
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletedata1 = $deletedata1->get();
        $deletedata1 = json_decode(json_encode($deletedata1));
        // print_r($deletedata1);
        $deleted_bookings = array();
        foreach ($deletedata1 as $value) {   
            $deleted_bookings[] = $value->booking_id;
        }
        $booking_data6 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->whereIn('b.booking_id', $deleted_bookings)
        ->get();
        $booking_data6 = json_decode(json_encode($booking_data6, true), true);
        $grand_suspend_total = 0;
        foreach ($booking_data6 as $value) {   
            $grand_suspend_total += $value['working_minutes'];    
        
        }
        // print_r($grand_suspend_total);
        $booking_details = json_decode(json_encode($response['bookings']), true);
        $deletes = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks');
        foreach ($booking_data as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes = $deletes->get();

        $maids = DB::table('maids as m')->select('m.maid_id', 'm.maid_name')
        ->where('m.employee_type_id', 1)
        ->where('m.maid_status', 1)
        ->orderBy('m.maid_name', 'ASC')
        ->get();

        //maid emergency leaves
        $emergency_leave = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves', 'm.employee_type_id')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'emergency_leave')
        ->groupBy('ml.maid_id')
        ->get();
        // print_r($emergency_leave);
        $emergency_leave_count = count($emergency_leave);

        //maid vacations
        $vacations = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('ml.typeleaves','=', 'vacations')
        ->where('m.employee_type_id', 1)
        ->groupBy('ml.maid_id')
        ->get();
        $vacations_count = count($vacations);

        //maid holidays
        $holidays = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'holidays')
        ->groupBy('ml.maid_id')
        ->get();
        $holidays_count = count($holidays);

        //maid medical_leaves
        $medical_leaves = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('ml.typeleaves','=', 'medical_leaves')
        ->where('m.employee_type_id', 1)
        ->groupBy('ml.maid_id')
        ->get();
        $medical_leaves_count = count($medical_leaves);
        
        $leaveData = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'leave')
        ->groupBy('ml.maid_id')
        ->get();
        // print_r($emergency_leave);
        $leaveData_count = count($leaveData);

        $maid_count = count($maids);
        // Absent maid count 
        $total_maid_leave_count = $leaveData_count + $emergency_leave_count + $vacations_count + $holidays_count + $medical_leaves_count;
        //present maid count
        $present_maid_count = $maid_count -  $total_maid_leave_count;

        //vaccation total count 
        $vacations_week = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'vacations')
        ->get();
        $vacations_week_count = count($vacations_week);

        //maid emergency leaves total count 
        $emergency_leave_week  = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'emergency_leave')
        ->get();
        $emergency_leave_week_count = count($emergency_leave_week);
        //maid holidays total count 
        $holidays_week = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'holidays')
        ->get();
        $holidays_week_count = count($holidays_week);

        //maid medical_leaves total count 
        $medical_leaves_week = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'medical_leaves')
        ->get();
        $medical_leaves_week_count = count($medical_leaves_week);

        $maid_leaveData = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->get();

        $leave_maid_types = array();
        foreach ($maid_leaveData as $leave) {
            $leavetypearay = array();
            $leavetypearay['maid_id'] = $leave->maid_id;
            $leavetypearay['service_week_day'] = date('w', strtotime($leave->leave_date));
            $leavetypearay['maid_type'] = $leave->typeleaves;
            $leavetypearay['leave_date'] = $leave->leave_date;
            array_push($leave_maid_types, $leavetypearay);
        }
        $response['booking_delete_data'] = $deletes;
        $response['total_hours'] = $total_hours;
        $response['total_hour_week'] = $total_hour_week;
        $response['maid_leaveData'] = $leave_maid_types;
        $response['grand_total'] = $grand_total;
        $response['free_hours'] = $free_hours;
        $response['training_hours'] = $training_hours;
        $response['total_sales'] = $total_sales;
        $response['lost_sales'] = $lost_sales;
        $response['grand_suspend_total'] = $grand_suspend_total;
        $response['maid_count'] = $maid_count;
        $response['emergency_leave_count'] =  $emergency_leave_count;
        $response['vacations_count'] =  $vacations_count;
        $response['vacations_week_count'] =  $vacations_week_count;
        $response['emergency_leave_week_count'] =  $emergency_leave_week_count;
        $response['medical_leaves_week_count'] =  $medical_leaves_week_count;
        $response['holidays_week_count'] =  $holidays_week_count;
        $response['holidays_count'] =  $holidays_count;
        $response['medical_leaves_count'] =  $medical_leaves_count;
        $response['total_maid_leave_count'] =  $total_maid_leave_count;
        $response['present_maid_count'] =  $present_maid_count;
        
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }
    public function get_booking_data(Request $request)
    {
        $response['booking_data'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.reference_id',
                'b.customer_id',
                'b.charge_type',
                'b.material_cost',
                'c.customer_name',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'b.service_week_day',
                'b.service_start_date',
                DB::raw('IF(b.service_end = 1, service_end_date, null) as service_end_date'),
                'b.price_per_hr',
                'b.discount_price_per_hr',
                'b.cleaning_material',
                'b.maid_id',
                'm.maid_name',
                'b.service_type_id',
                'cl.location_name as booking_location',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'),
                'b._service_hours',
                'b._service_rate_per_hour',
                'b._service_discount_rate_per_hour',
                'b._service_amount',
                'b._service_discount',
                'b._net_service_amount',
                'b._cleaning_material',
                'b._cleaning_material_rate_per_hour',
                'b._net_cleaning_material_amount',
                'b._total_discount',
                'b._taxable_amount',
                'b._vat_percentage',
                'b._vat_amount',
                'b._total_amount')
            ->leftJoin('maids as m', 'm.maid_id', 'b.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->where([['b.booking_id', '=', $request->booking_id], ['b.deleted_at', '=', null]])
            ->first();
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

    public function suspended_schedules(Request $request)
    {
        $filter_start_date = (isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m-d'));
        $end_date = (isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : NULL);
        $response['bookings'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.customer_id',
                'b.booking_type',
                'c.customer_name',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'b.service_start_date',
                'b.service_actual_end_date',
                'b.service_end',
                'b.service_week_day',
                'b.maid_id',
                'cl.location_name as booking_location',
                'ar.area_id',
                'z.zone_id',
                'z.zone_name as booking_zone',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.remarks',
                'bd.reassign_status',
                'bd.new_parent_maid_name',
                'cr.cancel_reason_id',
                'cr.cancel_reason',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('areas as ar', 'ca.area_id', 'ar.area_id')
            ->leftJoin('zones as z', 'ar.zone_id', 'z.zone_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->leftJoin('cancel_resons as cr', 'bd.cancel_reason_id', 'cr.cancel_reason_id')
            ->where([['b.deleted_at', '=', null], ['bd.delete_booking_type', '=', 'Suspend']])
            // ->where([['b.booking_type', '=', 'WE'], ['b.deleted_at', '=', null], ['bd.delete_booking_type', '=', 'Suspend']])
            // ->where([['b.booking_type', '=', 'WE'], ['b.deleted_at', '=', null], ['bd.delete_booking_type', '=', 'Suspend']])
        //    ->whereRaw('(b.service_actual_end_date >= '.$service_date2. ' and service_end = 1) or ( service_end = 0)')
        //    ->whereRaw('(( b.service_start_date <= ' . $service_date2 . ' and b.booking_type = "WE"))')
            ->orderBy('b.time_from', 'ASC')
            ->where(function ($query) use ($filter_start_date,$end_date) {
                $query->where([['b.service_end', '=', 0]])
                      ->orWhere([['b.service_end', '=', 1], ['b.booking_type', '=', 'OD']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $end_date], ['b.booking_type', '=', 'WE']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $filter_start_date], ['b.booking_type', '=', 'WE']]);

            })
            ->get();
        $booking_service_start_date = array();
        $booking_data = array();
         $booking_data1 = array();
         $permeant_deletes = array();
        foreach ($response['bookings'] as $key=>$booking) {

            $date = date("Y-m-d", strtotime($filter_start_date));
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date2 = date('Y-m-d', strtotime($days[$booking->service_week_day], strtotime($date)));
            // if (($booking->service_start_date == $date2) || ($booking->service_end_date == $date2) || ($booking->service_actual_end_date == $date2)) {
            //     // $booking_data1[$key]['booking_id'] = $booking->booking_id;
            //     unset($response['bookings'][$key]);
            // }
            if ($booking->service_actual_end_date <= $date2)
            {
                if ($booking->booking_delete_id != null && $booking->delete_date_range == "") {
                    unset($response['bookings'][$key]);
                }
            }

            if ($date <= $date2) {
                $booking_data[$key]['booking_service_start_date'] = $date2;
                $booking_data[$key]['booking_id'] = $booking->booking_id;

            }
            if ($date > $date2) {
                $booking_data[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
                // $booking_service_start_date[] = $booking->service_start_date;
            }
        }
        $booking_details = json_decode(json_encode($response['bookings']), true);
        $deletes = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
                    //  ->where('bd.reassign_status', '=', Null);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes = $deletes->get();
        $suspend_count = count($deletes);
       
        // suspend maid on leave 
        $deletes1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes1 = $deletes1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.cancel_reason_id', '=', 1);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes1 = $deletes1->get();
        $suspend_maid_absent_count = count($deletes1);

        // Suspended by Customer
        $deletes2 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes2 = $deletes2->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.cancel_reason_id', '=', 2);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes2 = $deletes2->get();
        $suspend_by_customer_count = count($deletes2);

        // Suspended by Office
        $deletes3 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes3 = $deletes3->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 3);
            });
        }
        $deletes3 = $deletes3->get();
        $suspend_by_office_count = count($deletes3);

       // Unassigned Schedules
        $deletes4 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
           $deletes4 = $deletes4->orWhere(function ($query) use ($value) {
               $query->where('bd.booking_id', '=', $value['booking_id'])
                     ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '=', Null)
                    ->where('bd.cancel_reason_id', '=', 1);
           });
        }
       $deletes4 = $deletes4->get();
       $unassigned_count = count($deletes4);

        // Reassigned Schedules
        $deletes5 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes5 = $deletes5->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '!=', Null);
                    // ->where('bd.cancel_reason_id', '=', 1);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }

        $maid_leaveData = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        // ->where('ml.leave_date','=', $filter_start_date)
        ->whereBetween('ml.leave_date', [$filter_start_date, $end_date])
        ->where('ml.leave_status', 1)
        ->get();

        $deletes5 = $deletes5->get();
        $reassigned_count = count($deletes5);
        $response['booking_delete_data'] = $deletes;
        $response['maid_leaveData'] = $maid_leaveData;
        $response['unassigned_count'] = $unassigned_count;
        $response['reassigned_count'] = $reassigned_count;
        // $response['assigned_hours'] = $assigned_hours;
        // $response['unassigned_hours'] = $unassigned_hours;
        
        $response['suspend_count'] = $suspend_count;
        $response['suspend_maid_absent_count'] = $suspend_maid_absent_count;
        $response['suspend_by_customer_count'] = $suspend_by_customer_count;
        $response['suspend_by_office_count'] = $suspend_by_office_count;
        // $booking_array = array_merge($booking_details,$booking_data);
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

    public function get_suspended_booking_data(Request $request)
    {
        $response['booking_data'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.reference_id',
                'b.customer_id',
                'c.customer_name',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'b.service_week_day',
                'b.service_start_date',
                DB::raw('IF(b.service_end = 1, service_end_date, null) as service_end_date'),
                'b.price_per_hr',
                'b.discount_price_per_hr',
                'b.cleaning_material',
                'b.maid_id',
                'm.maid_name',
                'b.service_type_id',
                'cl.location_name as booking_location',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'),
                'b._service_hours',
                'b._service_rate_per_hour',
                'b._service_discount_rate_per_hour',
                'b._service_amount',
                'b._service_discount',
                'b._net_service_amount',
                'b._cleaning_material',
                'b._cleaning_material_rate_per_hour',
                'b._net_cleaning_material_amount',
                'b._total_discount',
                'b._taxable_amount',
                'b._vat_percentage',
                'b._vat_amount',
                'b._total_amount',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.remarks')
            ->leftJoin('maids as m', 'm.maid_id', 'b.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->where([['b.booking_id', '=', $request->booking_id], ['b.deleted_at', '=', null]])
            ->first();
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

    public function cancelled_schedules(Request $request)
    {
        $filter_start_date = (isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m-d'));
        $end_date = (isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : NULL);
        $response['bookings'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.customer_id',
                'b.booking_type',
                'c.customer_name',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'b.service_start_date',
                'b.service_actual_end_date',
                'b.service_end',
                'b.service_week_day',
                'b.maid_id',
                'cl.location_name as booking_location',
                'ar.area_id',
                'z.zone_id',
                'z.zone_name as booking_zone',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.remarks',
                'bd.reassign_status',
                'bd.delete_status',
                'cr.cancel_reason_id',
                'cr.cancel_reason',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('areas as ar', 'ca.area_id', 'ar.area_id')
            ->leftJoin('zones as z', 'ar.zone_id', 'z.zone_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->leftJoin('cancel_resons as cr', 'bd.cancel_reason_id', 'cr.cancel_reason_id')
            // ->where([['b.booking_type', '=', 'WE'], ['b.deleted_at', '=', null], ['bd.delete_booking_type', '=', 'Suspend']])
            ->where([['b.deleted_at', '=', null], ['bd.delete_status', '=', 'cancel_one_day']])
        //    ->whereRaw('(b.service_actual_end_date >= '.$service_date2. ' and service_end = 1) or ( service_end = 0)')
        //    ->whereRaw('(( b.service_start_date <= ' . $service_date2 . ' and b.booking_type = "WE"))')
            ->orderBy('b.time_from', 'ASC')
            ->where(function ($query) use ($filter_start_date,$end_date) {
                $query->where([['b.service_end', '=', 0]])
                      ->orWhere([['b.service_end', '=', 1], ['b.booking_type', '=', 'OD']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $end_date], ['b.booking_type', '=', 'WE']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $filter_start_date], ['b.booking_type', '=', 'WE']]);

            })
           
            ->get();
        $booking_service_start_date = array();
        $booking_data = array();
         $booking_data1 = array();
         $permeant_deletes = array();
        foreach ($response['bookings'] as $key=>$booking) {

            $date = date("Y-m-d", strtotime($filter_start_date));
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date2 = date('Y-m-d', strtotime($days[$booking->service_week_day], strtotime($date)));
            // if (($booking->service_start_date == $date2) || ($booking->service_end_date == $date2) || ($booking->service_actual_end_date == $date2)) {
            //     // $booking_data1[$key]['booking_id'] = $booking->booking_id;
            //     unset($response['bookings'][$key]);
            // }
            if ($booking->service_actual_end_date <= $date2)
            {
                if ($booking->booking_delete_id != null && $booking->delete_date_range == "") {
                    unset($response['bookings'][$key]);
                }
            }

            if ($date <= $date2) {
                $booking_data[$key]['booking_service_start_date'] = $date2;
                $booking_data[$key]['booking_id'] = $booking->booking_id;

            }
            if ($date > $date2) {
                $booking_data[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
                // $booking_service_start_date[] = $booking->service_start_date;
            }
        }
        $booking_details = json_decode(json_encode($response['bookings']), true);
        $deletes = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null);
            });
        }
        $deletes = $deletes->get();

        // cancelled schedule count
        $deletes1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes1 = $deletes1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
            });
        }
        $deletes1 = $deletes1->get();
        $cancelled_count = count($deletes1);

        //cancel total hours
        $deletedata1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.delete_booking_type');
        foreach ($booking_data as $key => $value) {
            $deletedata1 = $deletedata1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.delete_booking_type', '=', 'Suspend');
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletedata1 = $deletedata1->get();
        $deletedata1 = json_decode(json_encode($deletedata1));
        $deleted_bookings = array();
        foreach ($deletedata1 as $value) {   
            $deleted_bookings[] = $value->booking_id;
        }
        $booking_data6 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->whereIn('b.booking_id', $deleted_bookings)
        ->get();
        $booking_data6 = json_decode(json_encode($booking_data6, true), true);
        $grand_cancelled_total = 0;
        foreach ($booking_data6 as $value) {   
            $grand_cancelled_total += $value['working_minutes'];    
        
        }

        // Cancel maid on leave 
        $deletes2 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes2 = $deletes2->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 1);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes2 = $deletes2->get();
        $deletes2 = json_decode(json_encode($deletes2));
        $cancelled_maid_absent_count = count($deletes2);

        // Cancel by Customer
        $deletes3 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes3 = $deletes3->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 2);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes3 = $deletes3->get();
        $deletes3 = json_decode(json_encode($deletes3));
        $cancelled_by_customer_count = count($deletes3);

        // Cancelled by Office
        $deletes4 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes4 = $deletes4->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 3);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes4 = $deletes4->get();
        $deletes4 = json_decode(json_encode($deletes4));
        $cancelled_by_office_count = count($deletes4);

        // Cancelled by Driver mistake
        $deletes5 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes5 = $deletes5->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 4);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes5 = $deletes5->get();
        $deletes5 = json_decode(json_encode($deletes5));
        $cancelled_by_driver_mistake_count = count($deletes5);

        // Unassigned Schedules
        $deletes6 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
           $deletes6 = $deletes6->orWhere(function ($query) use ($value) {
               $query->where('bd.booking_id', '=', $value['booking_id'])
                     ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '=', Null)
                    ->where('bd.cancel_reason_id', '=', 1);
                   // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
           });
        }
       $deletes6 = $deletes6->get();
       $unassigned_count = count($deletes6);

        // Reassigned Schedules
        $deletes7 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes7 = $deletes7->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '!=', Null);
            });
        }
        $deletes7 = $deletes7->get();
        $reassigned_count = count($deletes7);

        $response['booking_delete_data'] = $deletes;
        $response['cancelled_count'] = $cancelled_count;
        $response['grand_cancelled_total'] = $grand_cancelled_total;
        $response['cancelled_maid_absent_count'] = $cancelled_maid_absent_count;
        $response['cancelled_by_customer_count'] = $cancelled_by_customer_count;
        $response['cancelled_by_office_count'] = $cancelled_by_office_count;
        $response['cancelled_by_driver_mistake_count'] = $cancelled_by_driver_mistake_count;
        $response['unassigned_count'] = $unassigned_count;
        $response['reassigned_count'] = $reassigned_count;
        // $response['assigned_hours'] = $assigned_hours;
        // $response['unassigned_hours'] = $unassigned_hours;
        // $booking_array = array_merge($booking_details,$booking_data);
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

    public function get_cancelled_booking_data(Request $request)
    {
        $response['booking_data'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.reference_id',
                'b.customer_id',
                'c.customer_name',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'b.service_week_day',
                'b.service_start_date',
                DB::raw('IF(b.service_end = 1, service_end_date, null) as service_end_date'),
                'b.price_per_hr',
                'b.discount_price_per_hr',
                'b.cleaning_material',
                'b.maid_id',
                'm.maid_name',
                'b.service_type_id',
                'cl.location_name as booking_location',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'),
                'b._service_hours',
                'b._service_rate_per_hour',
                'b._service_discount_rate_per_hour',
                'b._service_amount',
                'b._service_discount',
                'b._net_service_amount',
                'b._cleaning_material',
                'b._cleaning_material_rate_per_hour',
                'b._net_cleaning_material_amount',
                'b._total_discount',
                'b._taxable_amount',
                'b._vat_percentage',
                'b._vat_amount',
                'b._total_amount',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.remarks')
            ->leftJoin('maids as m', 'm.maid_id', 'b.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->where([['b.booking_id', '=', $request->booking_id], ['b.deleted_at', '=', null]])
            ->first();
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

    // list view regular schedule
    public function regular_schedules_list(Request $request)
    {
        $filter_start_date = (isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m-d'));
        $end_date = (isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : NULL);
        $response['bookings'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.customer_id',
                'c.customer_name',
                'c.customer_code',
                'c.customer_booktype',
                'b.customer_address_id',
                'b.booking_type',
                'b.time_from',
                'b._service_rate_per_hour',
                'b._total_amount',
                'b.time_to',
                'b.service_start_date',
                'b.service_actual_end_date',
                'b.service_end',
                'b.service_week_day',
                'b.maid_id',
                'm.maid_name',
                'b.booking_reassign_reason',
                'cl.location_name as booking_location',
                'ar.area_id',
                'ar.area_name',
                'we.week_day_id',
                'we.week_name',
                'z.zone_id',
                'z.zone_name as booking_zone',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.delete_booking_type',
                'bd.remarks',
                'bd.delete_status',
                'b.parent_booking_id',
                'b.parent_maid_name',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('areas as ar', 'ca.area_id', 'ar.area_id')
            ->leftJoin('zones as z', 'ar.zone_id', 'z.zone_id')
            ->leftJoin('week_days as we', 'b.service_week_day', 'we.week_day_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->where([['b.booking_type', '=', 'WE'], ['b.deleted_at', '=', null]])
            // ->groupBy('bd.booking_id')
            ->distinct()
            ->orderBy('b.time_from', 'ASC')
            // ->groupBy('bd.booking_id')
            ->where(function ($query) use ($filter_start_date,$end_date) {
                $query->where([['b.service_end', '=', 0]])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $end_date]])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $filter_start_date]]);
            })
            ->get();
        $booking_service_start_date = array();
        $booking_data = array();
        $booking_data1 = array();
        $permeant_deletes = array();
        $booking_data2 = array();
        $booking_data3 = array();
        $booking_data4= array();
        foreach ($response['bookings'] as $key=>$booking) {
          
            $date = date("Y-m-d", strtotime($filter_start_date));
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date2 = date('Y-m-d', strtotime($days[$booking->service_week_day], strtotime($date)));
            
            if ($booking->service_actual_end_date <= $date2)
            {
                if ($booking->booking_delete_id != null && $booking->delete_status  == "cancel_permeantly") {
                    unset($response['bookings'][$key]);
                } 
                else if ($booking->booking_delete_id != null && $booking->delete_from_date  == $filter_start_date) {
                    unset($response['bookings'][$key]);
                } 
                else if ($booking->delete_from_date  != $filter_start_date) {
                    $response['bookings'][$key]->delete_status = null;
                }
                else {
                    $working_minutes = $booking->working_minutes / 60;
                    $booking_data1[$key]['maid_id']  = $booking->maid_id;
                    $booking_data1[$key]['working_minutes']  = $working_minutes;
                    $booking_data2[$key]['working_minutes']  = $working_minutes;
                   
                    $booking_data4[$key]['maid_id']  = $booking->maid_id;
                    $booking_data4[$key]['service_week_day']  =  $booking->service_week_day;;
                    $booking_data4[$key]['working_minutes']  = $working_minutes;
                }
            }

            if ($date <= $date2) {
                // $working_minutes = $booking->working_minutes / 60;
                $booking_data[$key]['booking_service_start_date'] = $date2;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
                $booking_data3[$key]['booking_service_start_date'] = $date2;
                $booking_data3[$key]['booking_id'] = $booking->booking_id;
              
            }
            if ($date > $date2) {
                $working_minutes = $booking->working_minutes / 60;
                $booking_data[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
                $booking_data3[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data3[$key]['booking_id'] = $booking->booking_id;
                
            }
        }  
        $booking_ids = array();
        foreach ($booking_data3 as $key => $value) {
            $booking_ids[] = $value['booking_id'];
        }
        $deletedata = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks');
        foreach ($booking_data3 as $key => $value) {
            $deletedata = $deletedata->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
            });
        }
        $deletedata = $deletedata->get();
        $deletedata = json_decode(json_encode($deletedata));
        $deleted_bookings = array();
        foreach ($deletedata as $value) {   
            $deleted_bookings[] = $value->booking_id;
        }
        $book_ids = array_diff($booking_ids,$deleted_bookings);
        $booking_data5 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        // ->whereNotIn('b.booking_id', $deleted_bookings)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data5 = json_decode(json_encode($booking_data5, true), true);
        
        $booking_data9 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'), 'b._service_amount')
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        // ->whereNotIn('b.booking_id', $deleted_bookings)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data9 = json_decode(json_encode($booking_data9, true), true);
        $total_sales = 0;
        foreach ($booking_data9 as $value) {   
            $total_sales += $value['_service_amount'];    
        
        }
        
        $booking_data10 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'), 'b._total_amount')
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->whereIn('b.booking_id', $deleted_bookings)
        ->get();
        $booking_data10 = json_decode(json_encode($booking_data10, true), true);
        $lost_sales = 0;
        foreach ($booking_data10 as $value) {   
            $lost_sales += $value['_total_amount'];    
        
        }

         $res = array_reduce (
            $booking_data5,
            function($res, $el) {
                if (isset($res[$el['maid_id'].$el['service_week_day']])) {
                    $res[$el['maid_id'].$el['service_week_day']]['working_minutes'] += $el['working_minutes']; 
                } else {
                    $res[$el['maid_id'].$el['service_week_day']] = $el;
                }
                
                return $res;
            },
            [] // initial result - empty array
        );
       $total_hour_week = array_values($res);

            
        $grand_total = 0;
        foreach ($booking_data5 as $value) {   
            $grand_total += $value['working_minutes'];    
        
        }

        $booking_data11 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->where('b.charge_type', '=', 2)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data11 = json_decode(json_encode($booking_data11, true), true);
        $training_hours = 0;
        foreach ($booking_data11 as $value) {   
            $training_hours += $value['working_minutes'];    
        }

        $booking_data12 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->where('b.charge_type', '=', 1)
        ->whereIn('b.booking_id', $book_ids)
        ->get();
        $booking_data12 = json_decode(json_encode($booking_data12, true), true);
        $free_hours = 0;
        foreach ($booking_data12 as $value) {   
            $free_hours += $value['working_minutes'];    
        }
        
        $result = array();
        foreach($booking_data5 as $k => $v) {
            $id = $v['maid_id'];
            $result[$id][] = $v['working_minutes'];
        }

        $total_hours = array();
        foreach($result as $key => $value) {
            $total_hours[] = array('maid_id' => $key, 'working_minutes' => array_sum($value));
        }
        // Lost hours
        $deletedata1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.delete_booking_type');
        foreach ($booking_data3 as $key => $value) {
            $deletedata1 = $deletedata1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.delete_booking_type', '=', 'Suspend');
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletedata1 = $deletedata1->get();
        $deletedata1 = json_decode(json_encode($deletedata1));
        $deleted_bookings = array();
        foreach ($deletedata1 as $value) {   
            $deleted_bookings[] = $value->booking_id;
        }
        $booking_data6 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->whereIn('b.booking_id', $deleted_bookings)
        ->get();
        $booking_data6 = json_decode(json_encode($booking_data6, true), true);
        $grand_suspend_total = 0;
        foreach ($booking_data6 as $value) {   
            $grand_suspend_total += $value['working_minutes'];    
        
        }
        $booking_details = json_decode(json_encode($response['bookings']), true);
        $deletes = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks');
        foreach ($booking_data as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
            });
        }
        $deletes = $deletes->get();

        $maids = DB::table('maids as m')->select('m.maid_id', 'm.maid_name')
        ->where('m.employee_type_id', 1)
        ->where('m.maid_status', 1)
        ->orderBy('m.maid_name', 'ASC')
        ->get();

        //maid emergency leaves
        $emergency_leave = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->where('ml.leave_date','=', $filter_start_date)
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'emergency_leave')
        ->get();
        $emergency_leave_count = count($emergency_leave);

        //maid vacations
        $vacations = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->where('ml.leave_date','=', $filter_start_date)
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'vacations')
        ->get();
        $vacations_count = count($vacations);

        //maid holidays
        $holidays = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->where('ml.leave_date','=', $filter_start_date)
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'holidays')
        ->get();
        $holidays_count = count($holidays);

        //maid medical_leaves
        $medical_leaves = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->where('ml.leave_date','=', $filter_start_date)
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'medical_leaves')
        ->get();
        $medical_leaves_count = count($medical_leaves);
        
        $leaveData = DB::table('maid_leave as ml')->select('ml.maid_id', 'ml.leave_date', 'ml.typeleaves')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->where('ml.leave_date','=', $filter_start_date)
        ->where('ml.leave_status', 1)
        ->where('m.employee_type_id', 1)
        ->where('ml.typeleaves','=', 'leave')
        ->get();
        // print_r($emergency_leave);
        $leaveData_count = count($leaveData);

        $maid_count = count($maids);
        // Absent maid count 
        $total_maid_leave_count =  $leaveData_count + $emergency_leave_count + $vacations_count + $holidays_count + $medical_leaves_count;
        //present maid count
        $present_maid_count = $maid_count -  $total_maid_leave_count;

        $response['booking_delete_data'] = $deletes;
        $response['total_hours'] = $total_hours;
        $response['total_hour_week'] = $total_hour_week;
        $response['grand_total'] = $grand_total;
        $response['grand_suspend_total'] = $grand_suspend_total;
        $response['maid_count'] = $maid_count;
        $response['free_hours'] = $free_hours;
        $response['training_hours'] = $training_hours;
        $response['total_sales'] = $total_sales;
        $response['lost_sales'] = $lost_sales;
        $response['emergency_leave_count'] =  $emergency_leave_count;
        $response['vacations_count'] =  $vacations_count;
        $response['holidays_count'] =  $holidays_count;
        $response['medical_leaves_count'] =  $medical_leaves_count;
        $response['total_maid_leave_count'] =  $total_maid_leave_count;
        $response['present_maid_count'] =  $present_maid_count;
        
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

    public function suspended_schedules_list(Request $request)
    {
        $filter_start_date = (isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m-d'));
        $end_date = (isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : NULL);
        $response['bookings'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.customer_id',
                'b.booking_type',
                'c.customer_name',
                'c.customer_booktype',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'c.customer_code',
                'b._service_rate_per_hour',
                'b._total_amount',
                'b.service_start_date',
                'b.service_actual_end_date',
                'b.service_end',
                'b.service_week_day',
                'b.maid_id',
                'm.maid_name',
                'cl.location_name as booking_location',
                'ar.area_id',
                'ar.area_name',
                'we.week_day_id',
                'we.week_name',
                'z.zone_id',
                'z.zone_name as booking_zone',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.delete_booking_type',
                'bd.remarks',
                'bd.delete_status',
                'bd.service_date',
                'bd.reassign_status',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('week_days as we', 'b.service_week_day', 'we.week_day_id')
            ->leftJoin('areas as ar', 'ca.area_id', 'ar.area_id')
            ->leftJoin('zones as z', 'ar.zone_id', 'z.zone_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->leftJoin('cancel_resons as cr', 'bd.cancel_reason_id', 'cr.cancel_reason_id')
            ->where([['b.deleted_at', '=', null], ['bd.delete_booking_type', '=', 'Suspend']])
            ->groupBy('bd.booking_id')
            ->orderBy('b.time_from', 'ASC')
            ->where(function ($query) use ($filter_start_date,$end_date) {
                $query->where([['b.service_end', '=', 0]])
                      ->orWhere([['b.service_end', '=', 1], ['b.booking_type', '=', 'OD']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $end_date], ['b.booking_type', '=', 'WE']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $filter_start_date], ['b.booking_type', '=', 'WE']]);

            })
            ->get();
        $booking_service_start_date = array();
        $booking_data = array();
         $booking_data1 = array();
         $permeant_deletes = array();
        foreach ($response['bookings'] as $key=>$booking) {

            $date = date("Y-m-d", strtotime($filter_start_date));
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date2 = date('Y-m-d', strtotime($days[$booking->service_week_day], strtotime($date)));
            if ($booking->service_actual_end_date <= $date2)
            {
                if ($booking->booking_delete_id != null && $booking->delete_date_range == "") {
                    unset($response['bookings'][$key]);
                }
            }

            if ($date <= $date2) {
                $booking_data[$key]['booking_service_start_date'] = $date2;
                $booking_data[$key]['booking_id'] = $booking->booking_id;

            }
            if ($date > $date2) {
                $booking_data[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
            }
        }
        $booking_details = json_decode(json_encode($response['bookings']), true);
        $deletes = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status', 'bd.service_date',);
        foreach ($booking_data as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
                   
            });
        }
        $deletes = $deletes->get();
        $suspend_count = count($deletes);
       
        // suspend maid on leave 
        $deletes1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes1 = $deletes1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.cancel_reason_id', '=', 1);
            });
        }
        $deletes1 = $deletes1->get();
        $suspend_maid_absent_count = count($deletes1);

        // Suspended by Customer
        $deletes2 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes2 = $deletes2->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.cancel_reason_id', '=', 2);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes2 = $deletes2->get();
        $suspend_by_customer_count = count($deletes2);

        // Suspended by Office
        $deletes3 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes3 = $deletes3->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 3);
            });
        }
        $deletes3 = $deletes3->get();
        $suspend_by_office_count = count($deletes3);

       // Unassigned Schedules
        $deletes4 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
           $deletes4 = $deletes4->orWhere(function ($query) use ($value) {
               $query->where('bd.booking_id', '=', $value['booking_id'])
                     ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '=', Null)
                    ->where('bd.cancel_reason_id', '=', 1);
           });
        }
       $deletes4 = $deletes4->get();
       $unassigned_count = count($deletes4);

        // Reassigned Schedules
        $deletes5 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes5 = $deletes5->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '!=', Null);
            });
        }
        $deletes5 = $deletes5->get();
        $reassigned_count = count($deletes5);
        $response['booking_delete_data'] = $deletes;
        $response['unassigned_count'] = $unassigned_count;
        $response['reassigned_count'] = $reassigned_count;
        // $response['assigned_hours'] = $assigned_hours;
        // $response['unassigned_hours'] = $unassigned_hours;
        
        $response['suspend_count'] = $suspend_count;
        $response['suspend_maid_absent_count'] = $suspend_maid_absent_count;
        $response['suspend_by_customer_count'] = $suspend_by_customer_count;
        $response['suspend_by_office_count'] = $suspend_by_office_count;
        // $booking_array = array_merge($booking_details,$booking_data);
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }
    public function cancelled_schedules_list(Request $request)
    {
        $filter_start_date = (isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : date('Y-m-d'));
        $end_date = (isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : NULL);
        $response['bookings'] = Bookings::from('bookings as b')
            ->select(
                'b.booking_id',
                'b.customer_id',
                'b.booking_type',
                'c.customer_name',
                'c.customer_booktype',
                'b.customer_address_id',
                'b.time_from',
                'b.time_to',
                'c.customer_code',
                'b._service_rate_per_hour',
                'b._total_amount',
                'b.service_start_date',
                'b.service_actual_end_date',
                'b.service_end',
                'b.service_week_day',
                'b.maid_id',
                'm.maid_name',
                'cl.location_name as booking_location',
                'ar.area_id',
                'ar.area_name',
                'we.week_day_id',
                'we.week_name',
                'z.zone_id',
                'z.zone_name as booking_zone',
                'bd.booking_delete_id',
                'bd.delete_from_date',
                'bd.delete_to_date',
                'bd.delete_date_range',
                'bd.remarks',
                'bd.reassign_status',
                'bd.delete_booking_type',
                'bd.delete_status',
                'cr.cancel_reason_id',
                'cr.cancel_reason',
                DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('locations as cl', 'ca.location_id', 'cl.location_id')
            ->leftJoin('areas as ar', 'ca.area_id', 'ar.area_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('week_days as we', 'b.service_week_day', 'we.week_day_id')
            ->leftJoin('zones as z', 'ar.zone_id', 'z.zone_id')
            ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
            ->leftJoin('cancel_resons as cr', 'bd.cancel_reason_id', 'cr.cancel_reason_id')
            ->where([['b.deleted_at', '=', null], ['bd.delete_status', '=', 'cancel_one_day']])
            ->groupBy('bd.booking_id')
            ->orderBy('b.time_from', 'ASC')
            ->where(function ($query) use ($filter_start_date,$end_date) {
                $query->where([['b.service_end', '=', 0]])
                      ->orWhere([['b.service_end', '=', 1], ['b.booking_type', '=', 'OD']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $end_date], ['b.booking_type', '=', 'WE']])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $filter_start_date], ['b.booking_type', '=', 'WE']]);

            })
           
            ->get();
        $booking_service_start_date = array();
        $booking_data = array();
         $booking_data1 = array();
         $permeant_deletes = array();
        foreach ($response['bookings'] as $key=>$booking) {

            $date = date("Y-m-d", strtotime($filter_start_date));
            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
            $date2 = date('Y-m-d', strtotime($days[$booking->service_week_day], strtotime($date)));
           
            if ($booking->service_actual_end_date <= $date2)
            {
                if ($booking->booking_delete_id != null && $booking->delete_date_range == "") {
                    unset($response['bookings'][$key]);
                }
            }

            if ($date <= $date2) {
                $booking_data[$key]['booking_service_start_date'] = $date2;
                $booking_data[$key]['booking_id'] = $booking->booking_id;

            }
            if ($date > $date2) {
                $booking_data[$key]['booking_service_start_date'] = $booking->service_start_date;
                $booking_data[$key]['booking_id'] = $booking->booking_id;
            }
        }
        $booking_details = json_decode(json_encode($response['bookings']), true);
        $deletes = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes = $deletes->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null);
            });
        }
        $deletes = $deletes->get();

        // cancelled schedule count
        $deletes1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes1 = $deletes1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null);
            });
        }
        $deletes1 = $deletes1->get();
        $cancelled_count = count($deletes1);

        //cancel total hours
        $deletedata1 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.delete_booking_type');
        foreach ($booking_data as $key => $value) {
            $deletedata1 = $deletedata1->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                      ->where('bd.service_date', '=', $value['booking_service_start_date'])
                     ->where('bd.delete_date_range', '!=', Null)
                     ->where('bd.delete_booking_type', '=', 'Suspend');
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletedata1 = $deletedata1->get();
        $deletedata1 = json_decode(json_encode($deletedata1));
        $deleted_bookings = array();
        foreach ($deletedata1 as $value) {   
            $deleted_bookings[] = $value->booking_id;
        }
        $booking_data6 = DB::table('bookings as b')
        ->select('b.booking_id','b.maid_id', 'b.service_week_day', DB::raw('TIMESTAMPDIFF(MINUTE,b.time_from ,b.time_to) as working_minutes'))
        ->where('b.booking_type', '=', 'WE')
        ->where('b.booking_status', '!=', 2)
        ->whereIn('b.booking_id', $deleted_bookings)
        ->get();
        $booking_data6 = json_decode(json_encode($booking_data6, true), true);
        $grand_cancelled_total = 0;
        foreach ($booking_data6 as $value) {   
            $grand_cancelled_total += $value['working_minutes'];    
        
        }

        // Cancel maid on leave 
        $deletes2 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes2 = $deletes2->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 1);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes2 = $deletes2->get();
        $deletes2 = json_decode(json_encode($deletes2));
        $cancelled_maid_absent_count = count($deletes2);

        // Cancel by Customer
        $deletes3 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes3 = $deletes3->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 2);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes3 = $deletes3->get();
        $deletes3 = json_decode(json_encode($deletes3));
        $cancelled_by_customer_count = count($deletes3);

        // Cancelled by Office
        $deletes4 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes4 = $deletes4->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 3);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes4 = $deletes4->get();
        $deletes4 = json_decode(json_encode($deletes4));
        $cancelled_by_office_count = count($deletes4);

        // Cancelled by Driver mistake
        $deletes5 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes5 = $deletes5->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.cancel_reason_id', '=', 4);
                    // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
            });
        }
        $deletes5 = $deletes5->get();
        $deletes5 = json_decode(json_encode($deletes5));
        $cancelled_by_driver_mistake_count = count($deletes5);

        // Unassigned Schedules
        $deletes6 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
           $deletes6 = $deletes6->orWhere(function ($query) use ($value) {
               $query->where('bd.booking_id', '=', $value['booking_id'])
                     ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '=', Null)
                    ->where('bd.cancel_reason_id', '=', 1);
                   // ->where('bd.service_date', '!=', $value['booking_service_start_date'])
           });
        }
       $deletes6 = $deletes6->get();
       $unassigned_count = count($deletes6);

        // Reassigned Schedules
        $deletes7 = DB::table('booking_deletes as bd')->select('bd.booking_delete_id', 'bd.service_date', 'bd.booking_id', 'bd.delete_date_range','bd.remarks', 'bd.reassign_status');
        foreach ($booking_data as $key => $value) {
            $deletes7 = $deletes7->orWhere(function ($query) use ($value) {
                $query->where('bd.booking_id', '=', $value['booking_id'])
                    ->where('bd.service_date', '=', $value['booking_service_start_date'])
                    ->where('bd.delete_date_range', '!=', Null)
                    ->where('bd.reassign_status', '!=', Null);
            });
        }
        $deletes7 = $deletes7->get();
        $reassigned_count = count($deletes7);

        $response['booking_delete_data'] = $deletes;
        $response['cancelled_count'] = $cancelled_count;
        $response['grand_cancelled_total'] = $grand_cancelled_total;
        $response['cancelled_maid_absent_count'] = $cancelled_maid_absent_count;
        $response['cancelled_by_customer_count'] = $cancelled_by_customer_count;
        $response['cancelled_by_office_count'] = $cancelled_by_office_count;
        $response['cancelled_by_driver_mistake_count'] = $cancelled_by_driver_mistake_count;
        $response['unassigned_count'] = $unassigned_count;
        $response['reassigned_count'] = $reassigned_count;
        // $response['assigned_hours'] = $assigned_hours;
        // $response['unassigned_hours'] = $unassigned_hours;
        // $booking_array = array_merge($booking_details,$booking_data);
        return Response::json($response, 200, array(), JSON_PRETTY_PRINT);
    }

}
