<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverAvailability extends Model
{
    use HasFactory;
    protected $table = 'driver_availability';
    public $timestamps = true;
    protected $primaryKey = 'driver_availability_id';
}
