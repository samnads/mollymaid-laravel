<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DriverAvailabilityController;
use App\Http\Controllers\BookingsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::prefix('driver')->group(function () {
    Route::get('mapping_data_all_under_driver', [DriverAvailabilityController::class, 'mapping_data_all_under_driver']);
});
Route::prefix('booking')->group(function () {
    Route::get('get_booking_data', [BookingsController::class, 'get_booking_data']);
    Route::get('get_suspended_booking_data', [BookingsController::class, 'get_suspended_booking_data']);    
    Route::get('regular_schedules', [BookingsController::class, 'regular_schedules']);
    Route::get('regular_schedules_list', [BookingsController::class, 'regular_schedules_list']);
    Route::get('suspended_schedules', [BookingsController::class, 'suspended_schedules']);
    Route::get('suspended_schedules_list', [BookingsController::class, 'suspended_schedules_list']);
    Route::get('cancelled_schedules', [BookingsController::class, 'cancelled_schedules']);
    Route::get('cancelled_schedules_list', [BookingsController::class, 'cancelled_schedules_list']);
    Route::get('get_cancelled_booking_data', [BookingsController::class, 'get_cancelled_booking_data']);
});
Route::get('/', function () {
    return view('welcome');
});
